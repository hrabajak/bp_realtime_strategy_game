@startuml

interface ITileCollector {
	MapTile get_tile(x, y)
	MapPlace get_place(x, y)
}

class MoveableItem {
}

class MoveablePath {
	+void follow_next()
}

class GotoGroup {
}

class PathFinder {
	MapPlace[] find_path(MapPlace from, MapPlace to)
}

class PathFinderFlow {
	+void prepare(MapPlace from, MapPlace to)
	+void update_step(int max_steps)
	+void update_obstacles(...)
}

class PathFlowMap {
	PathFlowMapField costs[]
	+void update_costs(int map_steps)
}

class PathFlowEntity {
	MapPlace get_next_place()
}

MoveableItem "1" --* "1" MoveablePath
MoveableItem "*" --* "1" GotoGroup
MoveablePath "1" --* "1" PathFinder
MoveablePath "1" --* "1" PathFlowEntity

GotoGroup "1" --* "1" PathFinderFlow

PathFinderFlow "1" --* "1" PathFlowMap
PathFlowEntity "1" --* "1" PathFlowMap
PathFinderFlow "1" --* "*" PathFlowEntity

PathFinder "1" --* "1" ITileCollector
PathFlowMap "1" --* "1" ITileCollector

@enduml
