#!/bin/bash
plantuml "$1".txt -tsvg
plantuml "$1".txt
cat "$1".svg | inkscape --pipe --export-filename="$1".pdf
