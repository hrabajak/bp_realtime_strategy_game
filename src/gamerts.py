#import cProfile
import sys
import ctypes
import os
if os.name != 'nt':
    sys.setdlopenflags(sys.getdlopenflags() | ctypes.RTLD_GLOBAL)

from game.main import Main


def main():
    #pr = cProfile.Profile()

    m = Main()
    #m.set_prof(pr)
    m.init()


if __name__ == '__main__':
    #pr = cProfile.run('main()', sort="tottime")
    main()
