#!/bin/bash

for current in `find build -type f -name "*.o"` 
do
	echo "FILE: "$current
	objdump -t $current | grep " main$"
done
