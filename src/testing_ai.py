import time

import pyglet
from pyglet import image

from game.action.actionitemspec import ActionItemSpec
from game.ai.aicollector import AiCollector
from game.build.buildcollector import BuildCollector
from game.build.buildspec import BuildSpec
from game.build.buildturretspec import BuildTurretSpec
from game.config import Config
from game.gamestat.gameplayerspec import GamePlayerSpec
from game.gui.guibasedummy import GuiBaseDummy
from game.helper.discrete import build_discrete_map
from game.helper.singleton import Singleton
from game.image.imagecollector import ImageCollector
from game.image.imagecollectordummy import ImageCollectorDummy
from game.map.mapbase import MapBase
from game.map.mapsnapshot import MapSnapshot
from game.moveable.gotogroup import GotoGroup
from game.moveable.moveablecollector import MoveableCollector
from game.moveable.unitspec import UnitSpec
from game.particle.particlecollector import ParticleCollector
from game.player.playersidecollector import PlayerSideCollector
from game.shot.shotcollector import ShotCollector
from game.shot.shotspec import ShotSpec
from game.sound.soundcollectordummy import SoundCollectorDummy
from game.workerdummy import WorkerDummy


def make_snap(mp, tick):
    snap = MapSnapshot(2)
    snap.update_map(mp)
    snap.get_img().save('temp/maptest_' + str(tick // 100).zfill(7) + '.png')


def get_snap(mp):
    snap = MapSnapshot(2)
    snap.update_map(mp)
    return snap.get_img().get_texture()


def do_loop(dt, wnd, spr):

    worker = WorkerDummy()
    worker.set_quick_build(True)
    worker.set_quick_progress(True)
    worker.set_quick_mine(True)

    shots = ShotCollector()
    particles = ParticleCollector()

    mp = MapBase()
    mp.set_fog_type(Config.FOG_NONE) # fog disabled
    mp.build(80, 80)
    mp.regenerate_b(10, 4) # seed, players
    #mp.build(62, 62)
    #mp.regenerate_b(10, 2) # seed, players

    sides = PlayerSideCollector()
    ais = AiCollector()

    moveables = MoveableCollector()
    moveables.set_refs(mp, sides, None)

    builds = BuildCollector()
    builds.set_refs(mp, sides, None)

    sides.set_refs(moveables, builds, mp, worker)
    sides.add_side(GamePlayerSpec('s1', 'ai', 2000))
    sides.add_side(GamePlayerSpec('s2', 'ai_medium', 2000))
    sides.add_side(GamePlayerSpec('s1', 'ai_medium', 2000))
    sides.add_side(GamePlayerSpec('s2', 'ai_easy', 2000))

    sides.build_units()
    mp.update_sides(sides)

    ais.start()
    tick = 0

    while True:
        shots.update()
        particles.update()
        GotoGroup.update(tick, tick % 10 == 0)
        moveables.update(tick)
        builds.update(tick)
        sides.update(tick)
        ais.update(tick)

        if tick % 100 == 0:
            make_snap(mp, tick)
            spr.image = get_snap(mp)
            wnd.clear()
            spr.draw()
            #print(tick)

        tick += 1


def main():
    build_discrete_map()

    Singleton.set_cls_override("ImageCollector", ImageCollectorDummy)
    Singleton.set_cls_override("SoundCollector", SoundCollectorDummy)
    Singleton.set_cls_override("GuiBase", GuiBaseDummy)

    conf = Config.get()

    GuiBaseDummy()

    ShotSpec.init_specs(ImageCollector())
    UnitSpec.init_specs(ImageCollector())
    BuildTurretSpec.init_specs(ImageCollector())
    BuildSpec.init_specs(ImageCollector())
    ActionItemSpec.init_specs()

    config = pyglet.gl.Config(sample_buffers=1, samples=8, double_buffer=False)
    wnd = pyglet.window.Window(600, 600, resizable=False, config=config)
    spr = pyglet.sprite.Sprite(img=image.load('resources/tex/darkgrass_00.png'), x=300, y=300)
    spr.scale = 3

    @wnd.event
    def on_draw():
        wnd.clear()
        spr.draw()

    pyglet.clock.schedule_once(do_loop, 0.1, wnd, spr)
    pyglet.app.run()


if __name__ == '__main__':
    main()
