import importlib
import sys
import ctypes
sys.setdlopenflags(sys.getdlopenflags() | ctypes.RTLD_GLOBAL)


class MyModuleSpec:
    def __init__(self, name, loader, *, origin=None, loader_state=None, is_package=None):
        self.name = name
        self.loader = loader
        self.origin = origin
        self.loader_state = loader_state
        self.submodule_search_locations = [] if is_package else None

        # file-location attributes
        self._set_fileattr = False
        self._cached = None

    def __repr__(self):
        args = ['name={!r}'.format(self.name), 'loader={!r}'.format(self.loader)]
        if self.origin is not None:
            args.append('origin={!r}'.format(self.origin))
        if self.submodule_search_locations is not None:
            args.append('submodule_search_locations={}'.format(self.submodule_search_locations))
        return '{}({})'.format(self.__class__.__name__, ', '.join(args))

    def __eq__(self, other):
        smsl = self.submodule_search_locations
        try:
            return (self.name == other.name and
                    self.loader == other.loader and
                    self.origin == other.origin and
                    smsl == other.submodule_search_locations and
                    self.cached == other.cached and
                    self.has_location == other.has_location)
        except AttributeError:
            return False

    @property
    def cached(self):
        return self._cached

    @cached.setter
    def cached(self, cached):
        self._cached = cached

    @property
    def parent(self):
        """The name of the module's parent."""
        if self.submodule_search_locations is None:
            return self.name.rpartition('.')[0]
        else:
            return self.name

    @property
    def has_location(self):
        return self._set_fileattr

    @has_location.setter
    def has_location(self, value):
        self._set_fileattr = bool(value)


class CythonPackageLoader(importlib.abc.Loader):

    path = ""

    def __init__(self, init_function):
        super(CythonPackageLoader, self).__init__()
        self.init_module = init_function

    def load_module(self, fullname):
        if fullname not in sys.modules:
            sys.modules[fullname] = self.init_module()
            print("initialized: " + fullname)
        return sys.modules[fullname]


class CythonPackageMetaPathFinder(importlib.abc.MetaPathFinder):
    def __init__(self, init_dict):
        super(CythonPackageMetaPathFinder, self).__init__()
        self.init_dict = init_dict

    def find_module(self, fullname, path):
        try:
            return CythonPackageLoader(self.init_dict[fullname])
        except KeyError:
            return None

    def find_spec(self, fullname, path, target=None):
        if fullname in self.init_dict:
            return MyModuleSpec(fullname, CythonPackageLoader(self.init_dict[fullname]))
        return None


cdef extern object PyInit_gam_main()
cdef extern object PyInit_servermain()
cdef extern object PyInit_imagecollectordummy()
cdef extern object PyInit_imagecollector()
cdef extern object PyInit_imagetransform()
cdef extern object PyInit_image()
cdef extern object PyInit_texturebuilder()
cdef extern object PyInit_types()
cdef extern object PyInit_pathflowmap()
cdef extern object PyInit_primitive()
cdef extern object PyInit_quadcollector()
cdef extern object PyInit_graphic()
cdef extern object PyInit_groundcollector()
cdef extern object PyInit_rect()
cdef extern object PyInit_graphbatches()
cdef extern object PyInit_groundrect()
cdef extern object PyInit_rectcollector()
cdef extern object PyInit_gridarea()
cdef extern object PyInit_drawableblank()
cdef extern object PyInit_primitivecollector()
cdef extern object PyInit_rockettrajectory()
cdef extern object PyInit_vector()
cdef extern object PyInit_distangle()
cdef extern object PyInit_line()
cdef extern object PyInit_linetrajectory()
cdef extern object PyInit_helper()
cdef extern object PyInit_discretemath()
cdef extern object PyInit_heapqueue()
cdef extern object PyInit_simpleprofile()
cdef extern object PyInit_vehicletrajectoryacc()
cdef extern object PyInit_frametimer()
cdef extern object PyInit_angletrajectory()
cdef extern object PyInit_vehicletrajectory()
cdef extern object PyInit_singleton()
cdef extern object PyInit_trooptrajectory()
cdef extern object PyInit_guimessagebox()
cdef extern object PyInit_gui()
cdef extern object PyInit_guimenu()
cdef extern object PyInit_guibase()
cdef extern object PyInit_buildaction()
cdef extern object PyInit_build()
cdef extern object PyInit_buildcollector()
cdef extern object PyInit_buildplace()
cdef extern object PyInit_buildplaceanalyze()
cdef extern object PyInit_buildspec()
cdef extern object PyInit_builditem()
cdef extern object PyInit_logger()
cdef extern object PyInit_game()
cdef extern object PyInit_shotcollector()
cdef extern object PyInit_shot()
cdef extern object PyInit_shotspec()
cdef extern object PyInit_shotline()
cdef extern object PyInit_shotrocket()
cdef extern object PyInit_shotbase()
cdef extern object PyInit_shotsplash()
cdef extern object PyInit_shotrect()
cdef extern object PyInit_shotminirocket()
cdef extern object PyInit_player()
cdef extern object PyInit_playersidecollector()
cdef extern object PyInit_playerside()
cdef extern object PyInit_itrajectory()
cdef extern object PyInit_ievents()
cdef extern object PyInit_iparticles()
cdef extern object PyInit_icoordsnap()
cdef extern object PyInit_igui()
cdef extern object PyInit_iworker()
cdef extern object PyInit_interface()
cdef extern object PyInit_iplayersides()
cdef extern object PyInit_iai()
cdef extern object PyInit_iview()
cdef extern object PyInit_iparticle()
cdef extern object PyInit_iselectable()
cdef extern object PyInit_idrawable()
cdef extern object PyInit_ibuilds()
cdef extern object PyInit_isounds()
cdef extern object PyInit_iimages()
cdef extern object PyInit_itargetable()
cdef extern object PyInit_imoveables()
cdef extern object PyInit_ishots()
cdef extern object PyInit_itilecollector()
cdef extern object PyInit_moveableitem()
cdef extern object PyInit_moveableminer()
cdef extern object PyInit_unitspec()
cdef extern object PyInit_moveablepath()
cdef extern object PyInit_moveable()
cdef extern object PyInit_gotogroup()
cdef extern object PyInit_moveabletarget()
cdef extern object PyInit_moveabletravel()
cdef extern object PyInit_moveablecollector()
cdef extern object PyInit_main()
cdef extern object PyInit_control()
cdef extern object PyInit_serverconnection()
cdef extern object PyInit_network()
cdef extern object PyInit_recvbuffer()
cdef extern object PyInit_SendBuffer()
cdef extern object PyInit_client()
cdef extern object PyInit_command()
cdef extern object PyInit_server()
cdef extern object PyInit_specialflags()
cdef extern object PyInit_const()
cdef extern object PyInit_worker()
cdef extern object PyInit_action()
cdef extern object PyInit_actionitem()
cdef extern object PyInit_actionqueue()
cdef extern object PyInit_actionitemspec()
cdef extern object PyInit_mapwave()
cdef extern object PyInit_mapplace()
cdef extern object PyInit_pathfinder()
cdef extern object PyInit_maptileorganize()
cdef extern object PyInit_map()
cdef extern object PyInit_pathfinderflow()
cdef extern object PyInit_placeside()
cdef extern object PyInit_maptile()
cdef extern object PyInit_flowentity()
cdef extern object PyInit_mapscanner()
cdef extern object PyInit_mapbase()
cdef extern object PyInit_maptilearea()
cdef extern object PyInit_mapedge()
cdef extern object PyInit_mapgenerator()
cdef extern object PyInit_maptiletexture()
cdef extern object PyInit_sound()
cdef extern object PyInit_soundcollector()
cdef extern object PyInit_soundplayer()
cdef extern object PyInit_aiecodefenddynamic()
cdef extern object PyInit_ai()
cdef extern object PyInit_aicollector()
cdef extern object PyInit_aiecobuild()
cdef extern object PyInit_aiecolevel()
cdef extern object PyInit_aiecodefend()
cdef extern object PyInit_aicontainer()
cdef extern object PyInit_aigroup()
cdef extern object PyInit_aiecominer()
cdef extern object PyInit_gamemapspec()
cdef extern object PyInit_gamestat()
cdef extern object PyInit_gamecontext()
cdef extern object PyInit_gameplayerspec()
cdef extern object PyInit_gamespec()
cdef extern object PyInit_tstmod()
cdef extern object PyInit_particle()
cdef extern object PyInit_light()
cdef extern object PyInit_particlecollector()
cdef extern object PyInit_particleitem()
cdef extern object PyInit_config()
cdef extern object PyInit_button()
cdef extern object PyInit_window()
cdef extern object PyInit_textbox()
cdef extern object PyInit_buttonaction()
cdef extern object PyInit_label()
cdef extern object PyInit_windowelement()
cdef extern object PyInit_listbox()
cdef extern object PyInit_listboxitem()
cdef extern object PyInit_panel()
cdef extern object PyInit_windowmanager()
cdef extern object PyInit_position()
def init_gam_main():
    return PyInit_gam_main()
def init_servermain():
    return PyInit_servermain()
def init_imagecollectordummy():
    return PyInit_imagecollectordummy()
def init_imagecollector():
    return PyInit_imagecollector()
def init_imagetransform():
    return PyInit_imagetransform()
def init_image():
    return PyInit_image()
def init_texturebuilder():
    return PyInit_texturebuilder()
def init_types():
    return PyInit_types()
def init_pathflowmap():
    return PyInit_pathflowmap()
def init_primitive():
    return PyInit_primitive()
def init_quadcollector():
    return PyInit_quadcollector()
def init_graphic():
    return PyInit_graphic()
def init_groundcollector():
    return PyInit_groundcollector()
def init_rect():
    return PyInit_rect()
def init_graphbatches():
    return PyInit_graphbatches()
def init_groundrect():
    return PyInit_groundrect()
def init_rectcollector():
    return PyInit_rectcollector()
def init_gridarea():
    return PyInit_gridarea()
def init_drawableblank():
    return PyInit_drawableblank()
def init_primitivecollector():
    return PyInit_primitivecollector()
def init_rockettrajectory():
    return PyInit_rockettrajectory()
def init_vector():
    return PyInit_vector()
def init_distangle():
    return PyInit_distangle()
def init_line():
    return PyInit_line()
def init_linetrajectory():
    return PyInit_linetrajectory()
def init_helper():
    return PyInit_helper()
def init_discretemath():
    return PyInit_discretemath()
def init_heapqueue():
    return PyInit_heapqueue()
def init_simpleprofile():
    return PyInit_simpleprofile()
def init_vehicletrajectoryacc():
    return PyInit_vehicletrajectoryacc()
def init_frametimer():
    return PyInit_frametimer()
def init_angletrajectory():
    return PyInit_angletrajectory()
def init_vehicletrajectory():
    return PyInit_vehicletrajectory()
def init_singleton():
    return PyInit_singleton()
def init_trooptrajectory():
    return PyInit_trooptrajectory()
def init_guimessagebox():
    return PyInit_guimessagebox()
def init_gui():
    return PyInit_gui()
def init_guimenu():
    return PyInit_guimenu()
def init_guibase():
    return PyInit_guibase()
def init_buildaction():
    return PyInit_buildaction()
def init_build():
    return PyInit_build()
def init_buildcollector():
    return PyInit_buildcollector()
def init_buildplace():
    return PyInit_buildplace()
def init_buildplaceanalyze():
    return PyInit_buildplaceanalyze()
def init_buildspec():
    return PyInit_buildspec()
def init_builditem():
    return PyInit_builditem()
def init_logger():
    return PyInit_logger()
def init_game():
    return PyInit_game()
def init_shotcollector():
    return PyInit_shotcollector()
def init_shot():
    return PyInit_shot()
def init_shotspec():
    return PyInit_shotspec()
def init_shotline():
    return PyInit_shotline()
def init_shotrocket():
    return PyInit_shotrocket()
def init_shotbase():
    return PyInit_shotbase()
def init_shotsplash():
    return PyInit_shotsplash()
def init_shotrect():
    return PyInit_shotrect()
def init_shotminirocket():
    return PyInit_shotminirocket()
def init_player():
    return PyInit_player()
def init_playersidecollector():
    return PyInit_playersidecollector()
def init_playerside():
    return PyInit_playerside()
def init_itrajectory():
    return PyInit_itrajectory()
def init_ievents():
    return PyInit_ievents()
def init_iparticles():
    return PyInit_iparticles()
def init_icoordsnap():
    return PyInit_icoordsnap()
def init_igui():
    return PyInit_igui()
def init_iworker():
    return PyInit_iworker()
def init_interface():
    return PyInit_interface()
def init_iplayersides():
    return PyInit_iplayersides()
def init_iai():
    return PyInit_iai()
def init_iview():
    return PyInit_iview()
def init_iparticle():
    return PyInit_iparticle()
def init_iselectable():
    return PyInit_iselectable()
def init_idrawable():
    return PyInit_idrawable()
def init_ibuilds():
    return PyInit_ibuilds()
def init_isounds():
    return PyInit_isounds()
def init_iimages():
    return PyInit_iimages()
def init_itargetable():
    return PyInit_itargetable()
def init_imoveables():
    return PyInit_imoveables()
def init_ishots():
    return PyInit_ishots()
def init_itilecollector():
    return PyInit_itilecollector()
def init_moveableitem():
    return PyInit_moveableitem()
def init_moveableminer():
    return PyInit_moveableminer()
def init_unitspec():
    return PyInit_unitspec()
def init_moveablepath():
    return PyInit_moveablepath()
def init_moveable():
    return PyInit_moveable()
def init_gotogroup():
    return PyInit_gotogroup()
def init_moveabletarget():
    return PyInit_moveabletarget()
def init_moveabletravel():
    return PyInit_moveabletravel()
def init_moveablecollector():
    return PyInit_moveablecollector()
def init_main():
    return PyInit_main()
def init_control():
    return PyInit_control()
def init_serverconnection():
    return PyInit_serverconnection()
def init_network():
    return PyInit_network()
def init_recvbuffer():
    return PyInit_recvbuffer()
def init_SendBuffer():
    return PyInit_SendBuffer()
def init_client():
    return PyInit_client()
def init_command():
    return PyInit_command()
def init_server():
    return PyInit_server()
def init_specialflags():
    return PyInit_specialflags()
def init_const():
    return PyInit_const()
def init_worker():
    return PyInit_worker()
def init_action():
    return PyInit_action()
def init_actionitem():
    return PyInit_actionitem()
def init_actionqueue():
    return PyInit_actionqueue()
def init_actionitemspec():
    return PyInit_actionitemspec()
def init_mapwave():
    return PyInit_mapwave()
def init_mapplace():
    return PyInit_mapplace()
def init_pathfinder():
    return PyInit_pathfinder()
def init_maptileorganize():
    return PyInit_maptileorganize()
def init_map():
    return PyInit_map()
def init_pathfinderflow():
    return PyInit_pathfinderflow()
def init_placeside():
    return PyInit_placeside()
def init_maptile():
    return PyInit_maptile()
def init_flowentity():
    return PyInit_flowentity()
def init_mapscanner():
    return PyInit_mapscanner()
def init_mapbase():
    return PyInit_mapbase()
def init_maptilearea():
    return PyInit_maptilearea()
def init_mapedge():
    return PyInit_mapedge()
def init_mapgenerator():
    return PyInit_mapgenerator()
def init_maptiletexture():
    return PyInit_maptiletexture()
def init_sound():
    return PyInit_sound()
def init_soundcollector():
    return PyInit_soundcollector()
def init_soundplayer():
    return PyInit_soundplayer()
def init_aiecodefenddynamic():
    return PyInit_aiecodefenddynamic()
def init_ai():
    return PyInit_ai()
def init_aicollector():
    return PyInit_aicollector()
def init_aiecobuild():
    return PyInit_aiecobuild()
def init_aiecolevel():
    return PyInit_aiecolevel()
def init_aiecodefend():
    return PyInit_aiecodefend()
def init_aicontainer():
    return PyInit_aicontainer()
def init_aigroup():
    return PyInit_aigroup()
def init_aiecominer():
    return PyInit_aiecominer()
def init_gamemapspec():
    return PyInit_gamemapspec()
def init_gamestat():
    return PyInit_gamestat()
def init_gamecontext():
    return PyInit_gamecontext()
def init_gameplayerspec():
    return PyInit_gameplayerspec()
def init_gamespec():
    return PyInit_gamespec()
def init_tstmod():
    return PyInit_tstmod()
def init_particle():
    return PyInit_particle()
def init_light():
    return PyInit_light()
def init_particlecollector():
    return PyInit_particlecollector()
def init_particleitem():
    return PyInit_particleitem()
def init_config():
    return PyInit_config()
def init_button():
    return PyInit_button()
def init_window():
    return PyInit_window()
def init_textbox():
    return PyInit_textbox()
def init_buttonaction():
    return PyInit_buttonaction()
def init_label():
    return PyInit_label()
def init_windowelement():
    return PyInit_windowelement()
def init_listbox():
    return PyInit_listbox()
def init_listboxitem():
    return PyInit_listboxitem()
def init_panel():
    return PyInit_panel()
def init_windowmanager():
    return PyInit_windowmanager()
def init_position():
    return PyInit_position()
init_dict = {
    "gam_main": init_gam_main,
    "game.servermain": init_servermain,
    "game.image.imagecollectordummy": init_imagecollectordummy,
    "game.image.imagecollector": init_imagecollector,
    "game.image.imagetransform": init_imagetransform,
    "game.image": init_image,
    "game.image.texturebuilder": init_texturebuilder,
    "game.types": init_types,
    "game.types.pathflowmap": init_pathflowmap,
    "game.graphic.primitive": init_primitive,
    "game.graphic.quadcollector": init_quadcollector,
    "game.graphic": init_graphic,
    "game.graphic.groundcollector": init_groundcollector,
    "game.graphic.rect": init_rect,
    "game.graphic.graphbatches": init_graphbatches,
    "game.graphic.groundrect": init_groundrect,
    "game.graphic.rectcollector": init_rectcollector,
    "game.graphic.gridarea": init_gridarea,
    "game.graphic.drawableblank": init_drawableblank,
    "game.graphic.primitivecollector": init_primitivecollector,
    "game.helper.rockettrajectory": init_rockettrajectory,
    "game.helper.vector": init_vector,
    "game.helper.distangle": init_distangle,
    "game.helper.line": init_line,
    "game.helper.linetrajectory": init_linetrajectory,
    "game.helper": init_helper,
    "game.helper.discretemath": init_discretemath,
    "game.helper.heapqueue": init_heapqueue,
    "game.helper.simpleprofile": init_simpleprofile,
    "game.helper.vehicletrajectoryacc": init_vehicletrajectoryacc,
    "game.helper.frametimer": init_frametimer,
    "game.helper.angletrajectory": init_angletrajectory,
    "game.helper.vehicletrajectory": init_vehicletrajectory,
    "game.helper.singleton": init_singleton,
    "game.helper.trooptrajectory": init_trooptrajectory,
    "game.gui.guimessagebox": init_guimessagebox,
    "game.gui": init_gui,
    "game.gui.guimenu": init_guimenu,
    "game.gui.guibase": init_guibase,
    "game.build.buildaction": init_buildaction,
    "game.build": init_build,
    "game.build.buildcollector": init_buildcollector,
    "game.build.buildplace": init_buildplace,
    "game.build.buildplaceanalyze": init_buildplaceanalyze,
    "game.build.buildspec": init_buildspec,
    "game.build.builditem": init_builditem,
    "game.logger": init_logger,
    "game": init_game,
    "game.shot.shotcollector": init_shotcollector,
    "game.shot": init_shot,
    "game.shot.shotspec": init_shotspec,
    "game.shot.shotline": init_shotline,
    "game.shot.shotrocket": init_shotrocket,
    "game.shot.shotbase": init_shotbase,
    "game.shot.shotsplash": init_shotsplash,
    "game.shot.shotrect": init_shotrect,
    "game.shot.shotminirocket": init_shotminirocket,
    "game.player": init_player,
    "game.player.playersidecollector": init_playersidecollector,
    "game.player.playerside": init_playerside,
    "game.interface.itrajectory": init_itrajectory,
    "game.interface.ievents": init_ievents,
    "game.interface.iparticles": init_iparticles,
    "game.interface.icoordsnap": init_icoordsnap,
    "game.interface.igui": init_igui,
    "game.interface.iworker": init_iworker,
    "game.interface": init_interface,
    "game.interface.iplayersides": init_iplayersides,
    "game.interface.iai": init_iai,
    "game.interface.iview": init_iview,
    "game.interface.iparticle": init_iparticle,
    "game.interface.iselectable": init_iselectable,
    "game.interface.idrawable": init_idrawable,
    "game.interface.ibuilds": init_ibuilds,
    "game.interface.isounds": init_isounds,
    "game.interface.iimages": init_iimages,
    "game.interface.itargetable": init_itargetable,
    "game.interface.imoveables": init_imoveables,
    "game.interface.ishots": init_ishots,
    "game.interface.itilecollector": init_itilecollector,
    "game.moveable.moveableitem": init_moveableitem,
    "game.moveable.moveableminer": init_moveableminer,
    "game.moveable.unitspec": init_unitspec,
    "game.moveable.moveablepath": init_moveablepath,
    "game.moveable": init_moveable,
    "game.moveable.gotogroup": init_gotogroup,
    "game.moveable.moveabletarget": init_moveabletarget,
    "game.moveable.moveabletravel": init_moveabletravel,
    "game.moveable.moveablecollector": init_moveablecollector,
    "game.main": init_main,
    "game.control": init_control,
    "game.network.serverconnection": init_serverconnection,
    "game.network": init_network,
    "game.network.recvbuffer": init_recvbuffer,
    "game.network.SendBuffer": init_SendBuffer,
    "game.network.client": init_client,
    "game.network.command": init_command,
    "game.network.server": init_server,
    "game.const.specialflags": init_specialflags,
    "game.const": init_const,
    "game.worker": init_worker,
    "game.action": init_action,
    "game.action.actionitem": init_actionitem,
    "game.action.actionqueue": init_actionqueue,
    "game.action.actionitemspec": init_actionitemspec,
    "game.map.mapwave": init_mapwave,
    "game.map.mapplace": init_mapplace,
    "game.map.pathfinder": init_pathfinder,
    "game.map.maptileorganize": init_maptileorganize,
    "game.map": init_map,
    "game.map.pathfinderflow": init_pathfinderflow,
    "game.map.placeside": init_placeside,
    "game.map.maptile": init_maptile,
    "game.map.flowentity": init_flowentity,
    "game.map.mapscanner": init_mapscanner,
    "game.map.mapbase": init_mapbase,
    "game.map.maptilearea": init_maptilearea,
    "game.map.mapedge": init_mapedge,
    "game.map.mapgenerator": init_mapgenerator,
    "game.map.maptiletexture": init_maptiletexture,
    "game.sound": init_sound,
    "game.sound.soundcollector": init_soundcollector,
    "game.sound.soundplayer": init_soundplayer,
    "game.ai.aiecodefenddynamic": init_aiecodefenddynamic,
    "game.ai": init_ai,
    "game.ai.aicollector": init_aicollector,
    "game.ai.aiecobuild": init_aiecobuild,
    "game.ai.aiecolevel": init_aiecolevel,
    "game.ai.aiecodefend": init_aiecodefend,
    "game.ai.aicontainer": init_aicontainer,
    "game.ai.aigroup": init_aigroup,
    "game.ai.aiecominer": init_aiecominer,
    "game.gamestat.gamemapspec": init_gamemapspec,
    "game.gamestat": init_gamestat,
    "game.gamestat.gamecontext": init_gamecontext,
    "game.gamestat.gameplayerspec": init_gameplayerspec,
    "game.gamestat.gamespec": init_gamespec,
    "game.tstmod": init_tstmod,
    "game.particle": init_particle,
    "game.particle.light": init_light,
    "game.particle.particlecollector": init_particlecollector,
    "game.particle.particleitem": init_particleitem,
    "game.config": init_config,
    "game.window.button": init_button,
    "game.window": init_window,
    "game.window.textbox": init_textbox,
    "game.window.buttonaction": init_buttonaction,
    "game.window.label": init_label,
    "game.window.windowelement": init_windowelement,
    "game.window.listbox": init_listbox,
    "game.window.listboxitem": init_listboxitem,
    "game.window.panel": init_panel,
    "game.window.windowmanager": init_windowmanager,
    "game.window.position": init_position,
}


sys.meta_path = [CythonPackageMetaPathFinder(init_dict)] + sys.meta_path

#import cProfile
from game.main import Main
from game.tstmod import TstMod


def main():
    #pr = cProfile.Profile()

    m = Main()
    #m.set_prof(pr)
    m.init()

    #smt = TstMod()
    #smt.do_smt()


if __name__ == '__main__':
    #pr = cProfile.run('main()', sort="tottime")
    main()
