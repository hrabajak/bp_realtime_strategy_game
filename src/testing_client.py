from game.network.client import Client


class Main:
    def __init__(self):
        pass

    def logic(self):
        c = Client()
        c.connect()


def main():
    m = Main()
    m.logic()


if __name__ == '__main__':
    main()
