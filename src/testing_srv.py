from game.network.server import Server


class Main:
    def __init__(self):
        pass

    def logic(self):
        s = Server()
        s.start()


def main():
    m = Main()
    m.logic()


if __name__ == '__main__':
    main()
