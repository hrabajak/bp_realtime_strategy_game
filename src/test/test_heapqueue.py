from game.helper.heapqueue import HeapQueue


def test_sortedset():

    hp = HeapQueue(lambda x: x[0])
    hp.push([5, "first"])
    hp.push([6, "second"])
    hp.push([7, "third"])
    hp.push([1, "fourth"])
    hp.push([2, "fifth"])

    assert hp.pop()[1] == "fourth"
    assert hp.pop()[1] == "fifth"
    assert hp.pop()[1] == "first"
