import pyglet
import pytest
import flexmock

from game.image.imagetransform import ImageTransform
from game.map.mapbase import MapBase


@pytest.fixture
def prep():
    flexmock(ImageTransform).should_receive('image_to_monocrhome').replace_with(lambda im_ref: im_ref)
    flexmock(pyglet).should_receive('resource.image').and_return(flexmock(width=50, height=50, anchor_x=25, anchor_y=25))


@pytest.fixture(scope="session")
def prep_map():
    m = MapBase()
    m.build(20, 20)

    return m


def test_build(prep, prep_map):
    assert len(prep_map.get_tiles()) == 400


def test_generate_blank(prep, prep_map):
    prep_map.generate_blank(2)
    assert True


@pytest.mark.parametrize(
    'cx, cy',
    [
        (0, 0),
        (19, 0),
        (0, 19),
        (19, 19)
    ]
)
def test_edges(prep, prep_map, cx, cy):
    assert prep_map.get_tile(cx, cy) is not None


def test_neighbors(prep, prep_map):
    assert prep_map.get_tile(0, 0).get_right().get_top() is prep_map.get_tile(1, 1)
    assert prep_map.get_tile(5, 5).get_bottom() is prep_map.get_tile(5, 4)
    assert prep_map.get_tile(1, 1).get_left().get_bottom() is prep_map.get_tile(0, 0)
