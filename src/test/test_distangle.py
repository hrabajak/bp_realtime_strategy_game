import math
import pytest

from game.helper.discrete import build_map, get_angle_from, get_diff_from_angle
from game.helper.distangle import DistAngle
from game.helper.vector import angle_conv_realdeg_to_deg


@pytest.mark.parametrize(
    'coords_a, coords_b, angle',
    [
        ((0, 0, 20, 20), (0, 0, 0, 20), math.pi / 4),
        ((0, 0, 0, 20), (0, 0, 0, -20), math.pi),
        ((0, 0, 20, 20), (0, 0, 20, -20), math.pi / 2)
    ]
)
def test_dist_angle_a(coords_a, coords_b, angle):

    d1 = DistAngle()
    d1.set(coords_a[0], coords_a[1], coords_a[2], coords_a[3])

    d2 = DistAngle()
    d2.set(coords_b[0], coords_b[1], coords_b[2], coords_b[3])

    assert abs(d1.angle_btw(d2) - math.cos(angle)) < 0.00001


@pytest.mark.parametrize(
    'diff_x, diff_y, angle',
    [
        (2, 2, 45),
        (500, 0, 90),
        (0, 10, 0),
        (1, 10, 6),
        (-10, 0, 270),
        (-10, 5, 297)
    ]
)
def test_dist_angle_b(diff_x, diff_y, angle):

    build_map()

    assert get_angle_from(diff_x, diff_y) == angle_conv_realdeg_to_deg(angle)


@pytest.mark.parametrize(
    'dist',
    [
        5, 10, 20, 30, 40, 50, 60, 70, 80, 90
    ]
)
def test_dist_angle_c(dist):

    build_map()

    for c in DistAngle._map_rev[dist]:
        tmp = get_diff_from_angle(c[0], dist)
        assert tmp[0] == c[0]
