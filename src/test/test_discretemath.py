import pytest

from game.helper.discretemath import int_sqrt


@pytest.mark.parametrize(
    'value, result',
    [
        (9, 3),
        (25, 5),
        (35, 5),
        (36, 6),
        (1, 1),
        (2, 1),
        (3, 1),
        (4, 2),
        (99999999, 9999),
        (65025, 255),
        (32769, 181)
    ]
)
def test_int_sqrt(value, result):
    assert int_sqrt(value) == result
