from game.network.recvbuffer import RecvBuffer


def test_recvbuffer_a():

    rb = RecvBuffer()
    rb.append(RecvBuffer.encode('neco').decode('ascii') + RecvBuffer.get_sep())

    assert rb.pop() == b'neco'
    assert rb.pop() is None


def test_recvbuffer_b():

    rb = RecvBuffer()
    rb.append(RecvBuffer.encode('jedna').decode('ascii') + RecvBuffer.get_sep() + RecvBuffer.encode('dva').decode('ascii') + RecvBuffer.get_sep())
    rb.append(RecvBuffer.encode('tri').decode('ascii'))
    rb.append(RecvBuffer.get_sep())

    assert rb.pop() == b'jedna'
    assert rb.pop() == b'dva'

    rb.append(RecvBuffer.encode('ctyri').decode('ascii') + RecvBuffer.get_sep())

    assert rb.pop() == b'tri'
    assert rb.pop() == b'ctyri'
    assert rb.pop() is None
