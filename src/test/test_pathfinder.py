import pyglet
import pytest
import flexmock

from game.image.imagetransform import ImageTransform
from game.map.mapbase import MapBase
from game.map.pathfinder import PathFinder


@pytest.fixture
def prep():
    flexmock(ImageTransform).should_receive('image_to_monocrhome').replace_with(lambda im_ref: im_ref)
    flexmock(pyglet).should_receive('resource.image').and_return(flexmock(width=50, height=50, anchor_x=25, anchor_y=25))


@pytest.fixture
def prep_map(lines):
    m = MapBase()
    m.build(5, 5)
    m.set_lines(lines)

    return m


@pytest.mark.parametrize(
    'c_from, c_to, c_len, lines',
    [
        ((0, 0), (4, 4), 13, 'ooooo|xxxxo|ooooo|oxxxx|ooooo'),
        ((0, 0), (4, 0), 5, 'ooooo|xxxxo|ooooo|oxxxx|ooooo'),
        ((0, 0), (4, 4), 5, 'ooooo|ooooo|ooooo|ooooo|ooooo'),
        ((0, 0), (1, 0), 2, 'ooooo|ooooo|ooooo|ooooo|ooooo')
    ]
)
def test_path_a(prep, prep_map, c_from, c_to, c_len):

    tile_from = prep_map.get_tile(*c_from)
    tile_to = prep_map.get_tile(*c_to)

    fnd = PathFinder(prep_map)

    fnd.path_find_tile(tile_from, lambda tl: tl is tile_to, lambda tl: tl.is_accessible_simple())
    path = fnd.back_list(tile_to, tile_from)

    assert len(path) == c_len
