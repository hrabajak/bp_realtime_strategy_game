from collections import deque


def test_queue():
    q = deque()

    q.append(5)
    q.append(6)
    q.append(3)
    q.append(2)

    assert q.popleft() == 5

    q.append(45)

    assert q.popleft() == 6

    q.append(12)

    assert q.popleft() == 3
    assert q.popleft() == 2
    assert q.popleft() == 45
    assert q.popleft() == 12
