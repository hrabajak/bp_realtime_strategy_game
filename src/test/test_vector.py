import pytest

from game.helper.vector import angle_to, angle_conv, line_in_circle, angle_dist_side_deg


@pytest.mark.parametrize(
    'points, angle',
    [
        ((1, 1, 4, 4), 45),
        ((1, 1, 5, 3), 63.44),
        ((1, 1, -1, -4), 201.81),
        ((1, 1, -3, 2), 284.04)
    ]
)
def test_angle_to(points, angle):
    ang = angle_conv(angle_to(*points))

    assert pytest.approx(ang, 0.1) == angle


@pytest.mark.parametrize(
    'params, flag',
    [
        ((5, 4, 2, 3, 1, 8, 6), True),
        ((5, 4, 2, -1, 2, 6, 9), False),
        ((5, 4, 2, -1, 4, 10, 4), True)
    ]
)
def test_line_in_circle(params, flag):
    assert line_in_circle(*params) == flag


@pytest.mark.parametrize(
    'deg_a, deg_b, fin',
    [
        (350, 10, 20),
        (360, 180, 180),
        (270, 350, 80),
        (275, 85, 170)
    ]
)
def test_angle_dist_side_deg(deg_a, deg_b, fin):
    assert min(angle_dist_side_deg(deg_a, deg_b)) == fin
