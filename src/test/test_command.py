from game.network.command import Command


def test_command_a():

    c = Command()
    c.append(Command.T_SYNC, 1, ticks=56, time=1634384633.130825, latency=1.1)
    c.append(Command.T_COORDS, 2, x=10, y=25)
    c.append(Command.T_COORDS, 3, x=33, y=72)

    b = Command(c.get_buffer())
    assert b.pop() == (Command.T_SYNC, 1, 56, 1634384633.130825, 1.1)
    assert b.pop() == (Command.T_COORDS, 2, 10, 25)
    assert b.pop() == (Command.T_COORDS, 3, 33, 72)
