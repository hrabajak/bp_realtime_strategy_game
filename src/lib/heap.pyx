# -*- python -*-

"""Cython implementation of a binary min heap.

Original author: Almar Klein
Modified by: Zachary Pincus

License: BSD

Copyright 2009 Almar Klein

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# cython specific imports
import cython
from libc.stdlib cimport malloc, free
from cpython.ref cimport PyObject,Py_INCREF,Py_DECREF

cdef extern from "pyport.h":
  double Py_HUGE_VAL

ctypedef double VALUE_T
ctypedef void * REFERENCE_T
ctypedef Py_ssize_t INDEX_T
ctypedef unsigned char BOOL_T
ctypedef unsigned char LEVELS_T

cdef VALUE_T inf = Py_HUGE_VAL

# this is handy
cdef inline INDEX_T index_min(INDEX_T a, INDEX_T b):
    return a if a <= b else b


cdef class BinaryHeap:
    """BinaryHeap(initial_capacity=128)

    A binary heap class that can store values and an integer reference.

    A binary heap is an object to store values in, optimized in such a way
    that the minimum (or maximum, but a minimum in this implementation)
    value can be found in O(log2(N)) time. In this implementation, a reference
    value (a single integer) can also be stored with each value.

    Use the methods push() and pop() to put in or extract values.
    In C, use the corresponding push_fast() and pop_fast().

    Parameters
    ----------
    initial_capacity : int
        Estimate of the size of the heap, if known in advance. (In any case,
        the heap will dynamically grow and shrink as required, though never
        below the `initial_capacity`.)

    Attributes
    ----------
    count : int
        The number of values in the heap
    levels : int
        The number of levels in the binary heap (see Notes below). The values
        are stored in the last level, so 2**levels is the capacity of the
        heap before another resize is required.
    min_levels : int
        The minimum number of levels in the heap (relates to the
        `initial_capacity` parameter.)

    Notes
    -----
    This implementation stores the binary heap in an array twice as long as
    the number of elements in the heap. The array is structured in levels,
    starting at level 0 with a single value, doubling the amount of values in
    each level. The final level contains the actual values, the level before
    it contains the pairwise minimum values. The level before that contains
    the pairwise minimum values of that level, etc. Take a look at this
    illustration:

    level: 0 11 2222 33333333 4444444444444444
    index: 0 12 3456 78901234 5678901234567890
                        1          2         3

     The actual values are stored in level 4. The minimum value of position 15
    and 16 is stored in position 7. min(17,18)->8, min(7,8)->3, min(3,4)->1.
    When adding a value, only the path to the top has to be updated, which
    takesO(log2(N)) time.

     The advantage of this implementation relative to more common
    implementations that swap values when pushing to the array is that data
    only needs to be swapped once when an element is removed. This means that
    keeping an array of references along with the values is very inexpensive.
    Th disadvantage is that if you pop the minimum value, the tree has to be
    traced from top to bottom and back. So if you only want values and no
    references, this implementation will probably be slower. If you need
    references (and maybe cross references to be kept up to date) this
    implementation will be faster.

    """
    cdef readonly INDEX_T count
    cdef readonly LEVELS_T levels, min_levels
    cdef VALUE_T *_values
    cdef REFERENCE_T *_references
    cdef REFERENCE_T _popped_ref

    ## Basic methods
    # The following lines are always "inlined", but documented here for
    # clarity:
    #
    # To calculate the start index of a certain level:
    # 2**l-1 # LevelStart
    # Note that in inner loops, this may also be represented as (1<<l)-1,
    # because code of the form x**y goes via the python pow operations and
    # can thus be a bit slower.
    #
    # To calculate the corresponding ABSOLUTE index at the next level:
    # i*2+1 # CalcNextAbs
    #
    # To calculate the corresponding ABSOLUTE index at the previous level:
    # (i-1)/2 # CalcPrevAbs
    #
    # To calculate the capacity at a certain level:
    # 2**l

    def __cinit__(self, INDEX_T initial_capacity=128, *args, **kws):
        # calc levels from the default capacity
        cdef LEVELS_T levels = 0
        while 2**levels < initial_capacity:
            levels += 1
        # set levels
        self.min_levels = self.levels = levels

        # we start with 0 values
        self.count = 0

        # allocate arrays
        cdef INDEX_T number = 2**self.levels
        self._values = <VALUE_T *>malloc(2 * number * sizeof(VALUE_T))
        self._references = <REFERENCE_T *>malloc(number * sizeof(REFERENCE_T))

    def __init__(self, INDEX_T initial_capacity=128):
        """__init__(initial_capacity=128)

        Class constructor.

        Takes an optional parameter 'initial_capacity' so that
        if the required heap capacity is known or can be estimated in advance,
        there will need to be fewer resize operations on the heap.

        """
        if self._values is NULL or self._references is NULL:
            raise MemoryError()
        self.reset()

    def __len__(self):
        return self.count

    def clear(self):
        self.reset()

    def reset(self):
        """reset()

        Reset the heap to default, empty state.

        """
        cdef INDEX_T number = 2**self.levels
        cdef INDEX_T i
        cdef VALUE_T *values = self._values
        for i in range(number * 2):
            values[i] = inf

    def __dealloc__(self):
        free(self._values)
        free(self._references)

    def __str__(self):
        s = ''
        for level in range(1, self.levels + 1):
            i0 = 2**level - 1  # LevelStart
            s += 'level %i: ' % level
            for i in range(i0, i0 + 2**level):
                s += '%g, ' % self._values[i]
            s = s[:-1] + '\n'
        return s

    ## C Maintenance methods

    cdef void _add_or_remove_level(self, LEVELS_T add_or_remove):
        # init indexing ints
        cdef INDEX_T i, i1, i2, n

        # new amount of levels
        cdef LEVELS_T new_levels = self.levels + add_or_remove

        # allocate new arrays
        cdef INDEX_T number = 2**new_levels
        cdef VALUE_T *values
        cdef REFERENCE_T *references
        values = <VALUE_T *>malloc(number * 2 * sizeof(VALUE_T))
        references = <REFERENCE_T *>malloc(number * sizeof(REFERENCE_T))
        if values is NULL or references is NULL:
            free(values)
            free(references)
            raise MemoryError()

        # init arrays
        for i in range(number * 2):
            values[i] = inf
        for i in range(number):
            #references[i] = -1
            references[i] = NULL

        # copy data
        cdef VALUE_T *old_values = self._values
        cdef REFERENCE_T *old_references = self._references
        if self.count:
            i1 = 2**new_levels - 1  # LevelStart
            i2 = 2**self.levels - 1  # LevelStart
            n = index_min(2**new_levels, 2**self.levels)
            for i in range(n):
                values[i1+i] = old_values[i2+i]
            for i in range(n):
                references[i] = old_references[i]

        # make current
        free(self._values)
        free(self._references)
        self._values = values
        self._references = references

        # we need a full update
        self.levels = new_levels
        self._update()

    cdef void _update(self):
        """Update the full tree from the bottom up.

        This should be done after resizing.

        """
        # shorter name for values
        cdef VALUE_T *values = self._values

        # Note that i represents an absolute index here
        cdef INDEX_T i0, i, ii, n
        cdef LEVELS_T level

        # track tree
        for level in range(self.levels, 1, -1):
            i0 = (1 << level) - 1  # 2**level-1 = LevelStart
            n = i0 + 1  # 2**level
            for i in range(i0, i0+n, 2):
                ii = (i-1) // 2  # CalcPrevAbs
                if values[i] < values[i+1]:
                    values[ii] = values[i]
                else:
                    values[ii] = values[i+1]

    cdef void _update_one(self, INDEX_T i):
        """Update the tree for one value."""
        # shorter name for values
        cdef VALUE_T *values = self._values

        # make index uneven
        if i % 2 == 0:
            i -= 1

        # track tree
        cdef INDEX_T ii
        cdef LEVELS_T level
        for level in range(self.levels, 1, -1):
            ii = (i-1) // 2  # CalcPrevAbs

            # test
            if values[i] < values[i+1]:
                values[ii] = values[i]
            else:
                values[ii] = values[i+1]
            # next
            if ii % 2:
                i = ii
            else:
                i = ii - 1

    cdef void _remove(self, INDEX_T i1):
        """Remove a value from the heap. By index."""
        cdef LEVELS_T levels = self.levels
        cdef INDEX_T count = self.count
        # get indices
        cdef INDEX_T i0 = (1 << levels) - 1  # 2**self.levels - 1 # LevelStart
        cdef INDEX_T i2 = i0 + count - 1

        # get relative indices
        cdef INDEX_T r1 = i1 - i0
        cdef INDEX_T r2 = count - 1

        cdef VALUE_T *values = self._values
        cdef REFERENCE_T *references = self._references

        # swap with last
        values[i1] = values[i2]
        references[r1] = references[r2]

        # make last Null
        values[i2] = inf

        # update
        self.count -= 1
        count -= 1
        if (levels > self.min_levels) and (count < (1 << (levels-2))):
            self._add_or_remove_level(-1)
        else:
            self._update_one(i1)
            self._update_one(i2)

    ## C Public methods

    cdef INDEX_T push_fast(self, VALUE_T value, REFERENCE_T reference):
        """The c-method for fast pushing.

        Returns the index relative to the start of the last level in the heap.

        """
        # We need to resize if currently it just fits.
        cdef LEVELS_T levels = self.levels
        cdef INDEX_T count = self.count
        if count >= (1 << levels):  # 2**self.levels:
            self._add_or_remove_level(1)
            levels += 1

        # insert new value
        cdef INDEX_T i = ((1 << levels) - 1) + count  # LevelStart + n
        self._values[i] = value
        self._references[count] = reference

        # update
        self.count += 1
        self._update_one(i)

        # return
        return count

    cdef VALUE_T pop_fast(self):
        """The c-method for fast popping.

        Returns the minimum value. The reference is put in self._popped_ref.

        """
        # shorter name for values
        cdef VALUE_T *values = self._values

        # init index. start at 1 because we start in level 1
        cdef LEVELS_T level
        cdef INDEX_T i = 1
        cdef LEVELS_T levels = self.levels
        # search tree (using absolute indices)
        for level in range(1, levels):
            if values[i] <= values[i+1]:
                i = i * 2 + 1  # CalcNextAbs
            else:
                i = (i+1) * 2 + 1  # CalcNextAbs

        # select best one in last level
        if values[i] <= values[i+1]:
            i = i
        else:
            i += 1

        # get values
        cdef INDEX_T ir = i - ((1 << levels) - 1) # (2**self.levels-1)
                                                  # LevelStart
        cdef VALUE_T value = values[i]
        self._popped_ref = self._references[ir]

        # remove it
        if self.count:
            self._remove(i)

        # return
        return value

    ## Python Public methods (that do not need to be VERY fast)

    #def push(self, VALUE_T value, REFERENCE_T reference=-1):
    def push(self, VALUE_T value, object reference):
        """push(value, reference=-1)

        Append a value to the heap, with optional reference.

        Parameters
        ----------
        value : float
            Value to push onto the heap
        reference : int, optional
            Reference to associate with the given value.

        """
        # je treba udelat incref
        Py_INCREF(reference)

        self.push_fast(value, <void *>reference)

    def min_val(self):
        """min_val()

        Get the minimum value on the heap.

        Returns only the value, and does not remove it from the heap.

        """
        # shorter name for values
        cdef VALUE_T *values = self._values

        # select best one in last level
        if values[1] < values[2]:
            return values[1]
        else:
            return values[2]

    def values(self):
        """values()

        Get the values in the heap as a list.

        """
        cdef INDEX_T i0 = 2**self.levels - 1  # LevelStart
        return [self._values[i] for i in range(i0, i0+self.count)]

    #def references(self):
        """references()

        Get the references in the heap as a list.

        """
        #return [self._references[i] for i in range(self.count)]

    def pop(self):
        """pop()

        Get the minimum value and remove it from the list.

        Returns
        -------
        value : float
        reference : int
            If no reference was provided, -1 is returned here.

        Raises
        ------
        IndexError
            On attempt to pop from an empty heap

        """
        value = self.pop_fast()

        # zase decref
        ref = <object>self._popped_ref
        Py_DECREF(ref)

        return value, ref

    def pop_ref(self):
        self.pop_fast()

        # zase decref
        ref = <object> self._popped_ref
        Py_DECREF(ref)

        return ref
