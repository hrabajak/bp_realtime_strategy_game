import os
import random
import pyglet

from game.image.imagecollector import ImageCollector


class Main:
    _wnd = None
    _rect = None
    _vlist = None
    _spr = None
    _tex3d = None
    _tex = None

    _area_x = 0
    _area_y = 0
    _angle = 0
    _scale = 0

    def __init__(self):
        self._wnd = pyglet.window.Window(1200, 1200, resizable=True, vsync=False)

        @self._wnd.event
        def on_draw(): self._draw(None)

        #self._rect = pyglet.shapes.Rectangle(50, 50, 100, 100, color=(200, 0, 0))

        pyglet.resource.path = ['../resources']
        pyglet.resource.reindex()

        imgs = ImageCollector()

        idxs = []
        pts = []
        cols = []
        uv = []
        num = 0

        off_x = 0
        off_y = 0

        sz = 50

        grid_width = 15
        grid_height = 15

        """
        for fy in range(0, grid_height + 1):
            for fx in range(0, grid_width + 1):

                pt_x = fx * sz + off_x
                pt_y = fy * sz + off_y

                pts.extend([pt_x, pt_y])
                uv.extend([0.2, 0.2, 0.0])

                num += 1
        """

        if False:
            for fy in range(0, grid_height):
                for fx in range(0, grid_width):

                    #idxs.append((fy + 1) * (grid_width + 1) + fx)
                    #idxs.append((fy + 1) * (grid_width + 1) + fx + 1)
                    #idxs.append(fy * (grid_width + 1) + fx + 1)
                    #idxs.append(fy * (grid_width + 1) + fx)

                    st_x = fx * sz + off_x
                    st_y = fy * sz + sz + off_y
                    en_x = fx * sz + sz + off_x
                    en_y = fy * sz + off_y

                    pts.extend([st_x, en_y])
                    idxs.append(len(idxs))

                    pts.extend([en_x, en_y])
                    idxs.append(len(idxs))

                    pts.extend([en_x, st_y])
                    idxs.append(len(idxs))

                    pts.extend([st_x, st_y])
                    idxs.append(len(idxs))

                    #idx = random.randint(0, len(self._regs) - 1)
                    #uv.extend(self._regs[idx].tex_coords)

                    num += 4

            # indexed je
            self._vlist = pyglet.graphics.vertex_list_indexed(num, idxs, ('v2i', pts), ('t3f', uv))

        if True:
            for fy in range(0, grid_height):
                for fx in range(0, grid_width):

                    st_x = float(fx * sz + off_x)
                    st_y = float(fy * sz + sz + off_y)
                    en_x = float(fx * sz + sz + off_x)
                    en_y = float(fy * sz + off_y)

                    pts.extend([st_x, en_y])
                    pts.extend([en_x, en_y])
                    pts.extend([en_x, st_y])
                    pts.extend([st_x, st_y])

                    cols.extend([100 + random.randint(100, 150), 0, 0])
                    cols.extend([100 + random.randint(100, 150), 0, 0])
                    cols.extend([100 + random.randint(100, 150), 0, 0])
                    cols.extend([100 + random.randint(100, 150), 0, 0])

                    #idx = random.randint(0, len(self._imgs) - 1)
                    #uv.extend(self._imgs[idx].get_texture().tex_coords)

                    #idx = random.randint(0, len(self._tex3d.get_texture_sequence()) - 1)
                    #uv.extend(self._tex3d.get_texture_sequence()[idx].tex_coords)

                    #idx = random.randint(0, len(self._regs) - 1)
                    #uv.extend(self._regs[idx].tex_coords)

                    #im = imgs.get_atlas_tex("field_10")
                    im = imgs.get_atlas_tex_random()
                    uv.extend(im.tex_coords)

                    num += 4

            self._vlist = pyglet.graphics.vertex_list(num, ('v2f', pts), ('t3f', uv)) # ('c3B', cols),

        self._area_x = 0
        self._area_y = 0
        self._angle = 0
        self._scale = 1.0

        self._fps_display = pyglet.window.FPSDisplay(window=self._wnd)

        pyglet.clock.schedule(self.update_logic, 1.0 / 60)
        pyglet.app.run()

    def _draw(self, dt):
        imgs = ImageCollector()

        self._wnd.clear()
        #self._rect.draw()

        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslatef(self._area_x, self._area_y, 0.0)
        pyglet.gl.glRotatef(self._angle, 0.0, 0.0, 1.0)
        pyglet.gl.glScalef(self._scale, self._scale, self._scale)

        pyglet.gl.glEnable(pyglet.gl.GL_TEXTURE_2D)
        #pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MAG_FILTER, pyglet.gl.GL_NEAREST)
        pyglet.gl.glBindTexture(pyglet.gl.GL_TEXTURE_2D, imgs.get_atlas_tex_id())

        self._vlist.draw(pyglet.gl.GL_QUADS)
        #self._vlist.draw(pyglet.gl.GL_POINTS)

        pyglet.gl.glDisable(pyglet.gl.GL_TEXTURE_2D)
        pyglet.gl.glPopMatrix()

        """
        for idx in range(0, len(self._vlist.vertices)):
            self._vlist.vertices[idx] += 0.1
            pass
        """

        self._fps_display.draw()

    def update_logic(self, dt, dtb):
        #self._area_x += 0.1
        #self._area_y += 0.1
        #self._angle += 0.01
        #self._scale += 0.0001
        pass


def main():
    m = Main()


if __name__ == '__main__':
    main()
