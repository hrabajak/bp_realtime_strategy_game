#!/bin/bash

if [[ -z $1 ]];
then
  cnt=1
else
  cnt=$1
fi

for i in $(seq 1 $cnt);
do
  /usr/bin/python3.6 gamerts.py &
done
