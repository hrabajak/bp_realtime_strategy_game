import pyglet
from pyglet.math import create_orthogonal

from lib import pyshaders



class Main(pyglet.window.Window):
    _wnd = None
    _rect = None
    _vlist = None
    _spr = None
    _tex3d = None
    _tex = None
    _shad = None
    _mat = None
    _pts = None
    _dists = None
    _cols = None

    _area_x = 0
    _area_y = 0
    _angle = 0
    _scale = 0

    def __init__(self):
        super(Main, self).__init__(width=1200, height=1200, resizable=True, vsync=False)

        #self._wnd = pyglet.window.Window(1200, 1200, resizable=True, vsync=False)
        #self._wnd.projection = pyglet.math.Mat4()
        #self._wnd.view = pyglet.math.Mat4()

        #self._rect = pyglet.shapes.Rectangle(50, 50, 100, 100, color=(200, 0, 0))

        #for attr in super().__dir__():
            #print(attr)

        #elf._mat = pyglet.math.Mat4()
        #self._mat.create_orthogonal(0, self.width, 0, self.height, -255, 255)
        self._mat = create_orthogonal(0, self.width, 0, self.height, -255, 255)
        #self._mat = self._projection_matrix

        pts = []
        cols = []
        uv = []
        dists = []
        num = 0

        off_x = 0
        off_y = 0

        sz = 50

        grid_width = 15
        grid_height = 15

        # vertex shader can manipulate the attributes of vertices
        # 1 vertex shader
        # 2 fragment shader

        #layout(location = 0) in vec4 position;
        #layout(location = 1) in vec4 colors;

        vertex_shader = """
        #version 150
        in vec4 vert;
        in vec3 colors;
        in vec2 dist;
        
        out vec3 vertex_colors;
        out vec2 vertex_dist;
        
        uniform mat4 projection = mat4(1.0, 0, 0, 0,  0, 1.0, 0, 0,  0, 0, 1.0, 0,  0, 0, 0, 1.0);
        
        void main()
        {
          gl_Position = projection * vert;
          
          vertex_colors = colors;
          vertex_dist = dist;
        }
        """
        #gl_Position = window.projection * window.view * position;

        fragment_shader = """
        #version 150
        
        in vec3 vertex_colors;
        in vec2 vertex_dist;
        
        out vec4 color_frag;
        
        void main()
        {
          gl_FragCoord.xy;
          //color_frag = vec4(vertex_dist.x, 1.0, 0.0, 1.0);
          //color_frag = vec4(vertex_colors, distance(vertex_dist, gl_FragCoord.xy) / 45.0);
          color_frag = vec4(vertex_colors, distance(gl_FragCoord.xy, vec2(0.5)) / 300.0 - 0.5);
          //color_frag = vec4(1.0, 0, 0, 0.5);
        }
        """
        # outColor = vec4(newColor, 1.0f);

        pyshaders.transpose_matrices(False)
        self._shad = pyshaders.from_string(vertex_shader, fragment_shader)

        #pyglet.gl.glBindAttribLocation(self._shad.pid.value, 0, b'dist')

        if True:
            for fy in range(0, grid_height):
                for fx in range(0, grid_width):

                    st_x = float(fx * sz + off_x)
                    st_y = float(fy * sz + sz + off_y)
                    en_x = float(fx * sz + sz + off_x) - 5
                    en_y = float(fy * sz + off_y) + 5

                    pts.extend([st_x, en_y])
                    pts.extend([en_x, en_y])
                    pts.extend([en_x, st_y])
                    pts.extend([st_x, st_y])

                    #pts.extend([0.0, 0.95])
                    #pts.extend([0.95, -0.95])
                    #pts.extend([-0.95, -0.95])

                    #pts.extend([0.0, 95])
                    #pts.extend([95, -95])
                    #pts.extend([-95, -95])

                    #cols.extend([100 + random.randint(100, 150), 0, 0])
                    #cols.extend([100 + random.randint(100, 150), 0, 0])
                    #cols.extend([100 + random.randint(100, 150), 0, 0])
                    #cols.extend([100 + random.randint(100, 150), 0, 0])

                    cols.extend([1.0, 1.0, 1.0])
                    cols.extend([1.0, 1.0, 1.0])
                    cols.extend([1.0, 1.0, 1.0])
                    cols.extend([1.0, 1.0, 1.0])

                    dists.extend([en_x, en_y])
                    dists.extend([en_x, en_y])
                    dists.extend([en_x, en_y])
                    dists.extend([en_x, en_y])

                    num += 4
                    #num += 3

            #self._vlist = pyglet.graphics.vertex_list(num, ('v2f', pts)) # ('1g3f', cols)
            #print(self._vlist.__getattr__("1g"))

        #print(pyglet.version)

        self._area_x = 0
        self._area_y = 0
        self._angle = 0
        self._scale = 1.0

        self._pts = (pyglet.gl.GLfloat * len(pts))(*pts)
        self._dists = (pyglet.gl.GLfloat * len(dists))(*dists)
        self._cols = (pyglet.gl.GLfloat * len(cols))(*cols)

        #pyglet.gl.glEnableClientState(pyglet.gl.GL_VERTEX_ARRAY)
        #pyglet.gl.glEnableClientState(pyglet.gl.GL_COLOR_ARRAY)

        #print(self._shad.attributes)

        self._fps_display = pyglet.window.FPSDisplay(window=self)

        pyglet.clock.schedule(self.update_logic, 1.0 / 60)
        pyglet.app.run()

    def on_draw(self):
        self.clear()

        """
        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslatef(self._area_x, self._area_y, 0.0)
        pyglet.gl.glRotatef(self._angle, 0.0, 0.0, 1.0)
        pyglet.gl.glScalef(self._scale, self._scale, self._scale)
        """

        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
        pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)

        with self._shad.using():

            #self._shad.uniforms.color = (1.0, 0.5, 0.5)
            self._shad.uniforms.projection = [self._mat]

            #pyglet.gl.glBindVertexArray(self._pts)

            self._shad.enable_all_attributes()

            if 'vert' in self._shad.attributes:
                self._shad.attributes['vert'].point_to(self._pts, pyglet.gl.GL_FLOAT, 2)

            if 'colors' in self._shad.attributes:
                self._shad.attributes['colors'].point_to(self._cols, pyglet.gl.GL_FLOAT, 3)

            if 'dist' in self._shad.attributes:
                self._shad.attributes['dist'].point_to(self._dists, pyglet.gl.GL_FLOAT, 2)

            #pyglet.gl.glVertexPointer(2, pyglet.gl.GL_FLOAT, 0, self._pts)
            #pyglet.gl.glColorPointer(4, pyglet.gl.GL_FLOAT, 0, self._cols)
            #pyglet.gl.glVertexAttribPointer(0, 1, pyglet.gl.GL_FLOAT, pyglet.gl.GL_FALSE, 0, self._dists)
            pyglet.gl.glDrawArrays(pyglet.gl.GL_QUADS, 0, len(self._pts) // 2)

            self._shad.disable_all_attributes()

            #self._vlist.draw(pyglet.gl.GL_QUADS)
            #self._vlist.draw(pyglet.gl.GL_TRIANGLES)
            #self._vlist.draw(pyglet.gl.GL_POINTS)

        #pyglet.gl.glPopMatrix()

        pyglet.gl.glDisable(pyglet.gl.GL_BLEND)

        """
        for idx in range(0, len(self._vlist.vertices)):
            self._vlist.vertices[idx] += 0.1
            pass
        """

        self._fps_display.draw()

    def on_resize(self, width, height):
        super().on_resize(width, height)
        #self._mat = self._projection_matrix
        self._mat = create_orthogonal(0, width, 0, height, -255, 255)

    def update_logic(self, dt, dtb):
        #for i in range(0, len(self._pts)):
            #self._pts[i] += 0.1

        #self._area_x += 0.1
        #self._area_y += 0.1
        #self._angle += 0.01
        #self._scale += 0.0001
        pass


def main():
    m = Main()


if __name__ == '__main__':
    main()
