import os
import sys
import ctypes
from game.servermain import ServerMain

if os.name == 'posix':
    sys.setdlopenflags(sys.getdlopenflags() | ctypes.RTLD_GLOBAL)


def main():
    m = ServerMain()
    m.init()


if __name__ == '__main__':
    main()
