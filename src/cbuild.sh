#!/bin/bash
case $1 in
	"build_dis")
		echo "disabled"
		exit

		incFiles=("gam_main.py")

		for current in `find game -type f -name "*.py" ! -name "__*"`
		do
			incFiles+=($current)
		done

		for cc in ${incFiles[@]}
		do
			echo "building $cc"
			cython -t -X "language_level=3" $cc
		done

		echo "freezing"

		incFiles=("gam_main.py")

		for current in `find game -type f -name "*.py" ! -name "__*"`
		do
			incFiles+=($current)
		done

		cython_freeze ${incFiles[@]} -o gam_main_final.c
		;;

	"build")
		python3.6 setup_gam.py build

		incFiles=("gam_main")

		# ! -name "__*"
		for current in `find game -type f -name "*.py"`
		do
			current=${current%.*}
			current=`echo $current | sed -r 's/\//./g'`
			incFiles+=($current)
		done

		#incFiles=("gam_main")
		#incFiles+=("game")
		#incFiles+=("game.tstmod")
		
		echo ${incFiles[@]}

		./freeze_self ${incFiles[@]} -o gam_main_final.c
		;;

	"compile")
		incFiles=()
		incFiles+=("gam_main_final.c")
		incFiles+=("gam_main.c")

		for current in `find game -type f -name "*.c"`
		do
			incFiles+=($current)
		done

		#incFiles=("gam_main_final.c")
		#incFiles+=("gam_main.c")
		#incFiles+=("game/__init__.c")
		#incFiles+=("game/tstmod.c")

		for cc in ${incFiles[@]}
		do
			bname=${cc%.*}
			bname=`echo $bname | sed -r 's/\//__/g'`
			bname="build/"${bname%.*}".o"
			gcc -DCYTHON_PEP489_MULTI_PHASE_INIT=0 -c -I/usr/include/python3.6m -lpython3.6m $cc -o $bname
			#gcc -c -I/usr/include/python3.6m -lpython3.6m $cc -o $bname
			echo "compiling $cc"
		done

		incFiles=()
		#incFiles+=("build/gam_main_final.o")
		#incFiles+=("build/gam_main.o")

		# ! -name "__*"
		#for current in `find game -type f -name "*.py"`
		for current in `find build -type f -name "*.o"`
		do
			current=${current##*/}
			current="build/"${current%.*}".o"
			incFiles+=($current)
		done
		
		#incFiles=()
		#incFiles+=("build/gam_main_final.o")
		#incFiles+=("build/gam_main.o")
		#incFiles+=("build/game____init__.o")
		#incFiles+=("build/game__tstmod.o")

		echo "linking"
		echo ${incFiles[@]}

		gcc -DCYTHON_PEP489_MULTI_PHASE_INIT=0 ${incFiles[@]} -I/usr/include/python3.6m -lpython3.6m -o binary
		#gcc ${incFiles[@]} -I/usr/include/python3.6m -lpython3.6m -o binary

		#gcc -DCYTHON_PEP489_MULTI_PHASE_INIT=0 -I/usr/include/python3.6m -lpython3.6m -E __init__.c -o __tst.c
		;;

	"defs")
		incFiles=()
		incFiles+=("gam_main.c")

		for current in `find game -type f -name "*.c"`
		do
			incFiles+=($current)
		done

		useItems=()

		for cc in ${incFiles[@]}
		do
			tt=`basename $cc`
			tt=${tt%.*}

			if [[ $tt == "__init__" ]]
			then
				tt=$(dirname "$cc")
				tt=${tt##*/}
			fi

			useItems+=("$tt")
		done

		for cc in ${useItems[@]}
		do
			echo "cdef extern object PyInit_$cc()"
		done

		for cc in ${useItems[@]}
		do
			echo "def init_$cc():"
			echo "    return PyInit_$cc()"
		done

		echo "init_dict = {"
		for cc in ${incFiles[@]}
		do
			tt=`basename $cc`
			tt=${tt%.*}

			if [[ $tt == "__init__" ]]
			then
				tt=$(dirname "$cc")
				tt=${tt##*/}

				bname=$(dirname "$cc")
				bname=`echo $bname | sed -r 's/\//./g'`
			else
				bname=${cc%.*}
				bname=`echo $bname | sed -r 's/\//./g'`
			fi

			echo "    \"$bname\": init_$tt,"
		done
		echo "}"
		;;

	"pyinstaller")
		incFiles=()

		for current in `find game -type f -name "*.pyx" -o -name "*.py"`
		do
			incFiles+=($current)
		done

		> pyinstaller_win.txt
		> pyinstaller_linux.txt

		echo -n "pyinstaller gamerts.py -F --hidden-import cython --hidden-import deepdiff --hidden-import lib.pyshaders " >> pyinstaller_win.txt
		echo -n "pyinstaller gamerts.py -F --hidden-import cython --hidden-import deepdiff --hidden-import lib.pyshaders " >> pyinstaller_linux.txt

		for cc in ${incFiles[@]}
		do
			bname=${cc%.*}
			bname=`echo $bname | sed -r 's/\//./g'`

			echo "^" >> pyinstaller_win.txt
			echo -n "--hidden-import $bname " >> pyinstaller_win.txt
			echo -n "--collect-binaries $bname " >> pyinstaller_win.txt

			echo -n "--hidden-import $bname " >> pyinstaller_linux.txt
			echo -n "--collect-binaries $bname " >> pyinstaller_linux.txt
		done
		;;

	"dist")
		rm -rf "dist"
		mkdir "dist"
		mkdir "dist/resources"
		cp -rfv "resources/eff" "dist/resources/eff"
		cp -rfv "resources/tex" "dist/resources/tex"
		cp -rfv "resources/units" "dist/resources/units"
		cp -rfv "resources/sound" "dist/resources/sound"
		cp -rfv "discrete_map.dat" "dist/discrete_map.dat"
		;;

	"compile_b")
		gcc -c -I/usr/include/python3.6m -lpython3.6m "gam_main_final.c" -o "build/gam_main_final.o"

		incFiles=()

		for current in `find build -type f -name "*.o"`
		do
			incFiles+=($current)
		done

		echo "linking"

		gcc ${incFiles[@]} -I/usr/include/python3.6m -lpython3.6m -o binary
		;;

	"clean")
		for cc in `find game -type f -name "*.c"`
		do
			echo "removing $cc"
			rm $cc
		done

		for cc in `find game -type f -name "*.so"`
		do
			echo "removing $cc"
			rm $cc
		done

		for cc in `find game -type f -name "*.html"`
		do
			echo "removing $cc"
			rm $cc
		done

		for cc in `find build -type f -name "*.o"`
		do
			echo "removing $cc"
			rm $cc
		done

		rm -rf build/*
		;;

	*)
		echo "invalid"
		;;
esac
