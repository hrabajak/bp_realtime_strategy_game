import os

from Cython.Build import cythonize
from distutils.core import setup, Extension
import Cython.Compiler.Options


Cython.Compiler.Options.annotate = True

exts = []

for cdir in ["game", "lib"]:
    for root, dirs, files in os.walk(cdir):
        path = root.split(os.sep)

        for filename in files:
            ext = os.path.splitext(filename)[1]
            full_path = root + "/" + filename

            if ext == ".pyx":
                if os.name == 'nt':
                    module_name = root.replace("\\", ".") + "." + os.path.splitext(filename)[0]
                else:
                    module_name = root.replace("/", ".") + "." + os.path.splitext(filename)[0]

                print("module=" + module_name + ", source=" + full_path)

                if os.name != 'nt':
                    e = Extension(module_name, sources=[full_path])
                    e.cython_directives = {"language_level": "3"}
                    exts.append(e)
                else:
                    e = Extension(module_name, sources=[full_path], language="c++", extra_compile_args=["-std=c++17"])
                    e.cython_directives = {"language_level": "3"}
                    exts.append(e)

setup(
    name="Ctest",
    ext_modules=cythonize(exts, compiler_directives={"language_level": "3"}, annotate=True)
    #cmdclass={'build_ext': build_ext},
    #ext_modules=cythonize(exts, compiler_directives={"language_level": "3"})
    #embed="main",
    #package_dir={'game': 'game'},
    #ext_modules=cythonize(["gam_main.py"], annotate=True)
    #ext_modules=cythonize(["gam_main.py", "game/*.py"])
)
