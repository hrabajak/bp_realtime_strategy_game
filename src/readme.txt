## Počítačová hra - realtime strategie inspirovaná hrou Starcraft.

V tomto adresáři je obsažen zdrojový kód bakalářské práce.

Autor: Jakub Hrabálek (j-hrb@seznam.cz)

Další informace dostupné na http://fit.mazec.org/rts/ a především v textu bakalářské práce https://projects.fit.cvut.cz/theses/4177


## Příprava prostředí Python a spuštění

Hra je vyvíjena a odladěna na verzi Python 3.6. Jako první krok je třeba vytvořit virtuální prostředí: 

; /usr/bin/python3.6 -m venv rts
; source rts/bin/activate

Poté nainstalovat potřebné balíčky:

; python -m pip install wheel
; python -m pip install pyglet==1.5.17
; python -m pip install pyshaders==1.4.1
; python -m pip install ordered_set
; python -m pip install cython
; python -m pip install deepdiff

Spustit kompilaci nástrojem Cython:

; python setup_gam_b.py build_ext --inplace

Spustit hru:

; python gamerts.py

Nebo spustit server:

; python server.py


## Další soubory

=> cbuild.sh : skript, který mimo další experimenty připraví příkaz pro pyinstaller

=> run_game.sh / run_server.sh / kill_game.sh : skripty pro spouštění více instancí hry / serveru / vypínání (nepočítá s venv)

=> cyt.sh / cytw.bat : skripty pro spuštění Cython build (nepočítá s venv)

=> testing_*.py : testovací experimenty

=> freeze_self : převzatý starý freeze skript s experimentálními úpravami

=> gamerts_entry.pyx : vstupní bod související s experimentováním se stavbou vlastního balíčku

=> discrete_map.dat : binární soubor s předdefinovanými celočíselnými výpočty (společně s adresářem resources musí být přítomen při spuštění hry nebo serveru)


## Adresáře

=> game : zdrojové soubory s komponentami hry (Python a Cython)

=> lib : zdrojové kódy několika málo převzatých knihoven / částí knihoven, které nebyly snadno dostupné jako balíčky a v některých bylo potřeba provést úpravu

=> test : další testy

=> resources : grafický a zvukový obsah (včetně zdrojových souborů)

