import json
import random
import threading
import time

import pyglet
import math

#from pyglet.math import create_orthogonal
from game.action.actionitemspec import ActionItemSpec
from game.ai.aicollector import AiCollector
from game.build.buildcollector import BuildCollector
from game.build.buildspec import BuildSpec
from game.build.buildturretspec import BuildTurretSpec
from game.config import Config
from game.graphic.envrect import EnvRect
from game.graphic.graphbatches import GraphBatches
from game.graphic.primitivecollector import PrimitiveCollector
from game.gui.guibase import GuiBase
from game.gui.guichatbox import GuiChatBox
from game.gui.guipromptbox import GuiPromptBox
from game.helper.discrete import build_discrete_map
from game.helper.frametimer import FrameTimer
from game.helper.singleton import Singleton
from game.image.imagecollector import ImageCollector
from game.interface.iview import IView
from game.logger import Logger
from game.map.mapbase import MapBase
from game.map.maptiletexture import MapTileTexture
from game.moveable.moveablecollector import MoveableCollector
from game.particle.particlecollector import ParticleCollector
from game.player.playersidecollector import PlayerSideCollector
from game.shot.shotcollector import ShotCollector
from game.shot.shotspec import ShotSpec
from game.sound.soundcollector import SoundCollector
from game.moveable.unitspec import UnitSpec
from game.window.windowmanager import WindowManager
from game.worker import Worker
from lib.mmat import create_orthogonal


class Main(IView, metaclass=Singleton):
    _wnd_width = 0
    _wnd_height = 0
    _wnd = None
    _mat = None
    _trans_mat = None
    _map: MapBase = None
    _fr_timer = None
    _worker = None
    _images: ImageCollector = None
    _sounds: SoundCollector = None
    _moveables: MoveableCollector = None
    _builds: BuildCollector = None
    _particles: ParticleCollector = None
    _shots: ShotCollector = None
    _sides: PlayerSideCollector = None
    _primitives: PrimitiveCollector = None
    _windows: WindowManager = None
    _batches: GraphBatches = None
    _ais: AiCollector = None
    _gui: GuiBase = None
    _keys = set()

    _zoom: float = 1.0
    _zoom_use: float = 1.0
    _zoom_sc_max: int = 30
    _zoom_scalar: int = _zoom_sc_max
    _tile_size: float = 50.0
    _scX: float = 0
    _scY: float = 0

    _fps_display = None
    _tick = 0
    _prof = None
    _prof_flag = 0
    _coords = []
    _tmp = None
    _tmp_ang = 0

    def __init__(self):
        IView.__init__(self)
        IView.set(self)

    def init(self):
        conf = Config.get()

        conf.test_a()
        conf.load()

        self._wnd_width, self._wnd_height = Config.get().get_resolution()
        self._mat = create_orthogonal(0, self._wnd_width, 0, self._wnd_height, -255, 255)
        self._trans_mat = pyglet.math.Mat4().translate(self._scX, self._scY, 0.0)

        self._batches = GraphBatches()

        self._windows = WindowManager()
        self._windows.set_size(self._wnd_width, self._wnd_height)
        self._windows.event_mouse = self._event_mouse
        self._windows.event_click = self._event_click
        self._windows.event_double_click = self._event_double_click
        self._windows.event_drag_area = self._event_drag_area
        self._windows.event_drag_move = self._event_drag_move

        #config = pyglet.gl.Config(sample_buffers=1, samples=8, double_buffer=False)

        self._wnd = pyglet.window.Window(self._wnd_width, self._wnd_height, resizable=True, vsync=Config.get().get_vsync()) # , config=config
        self._fps_display = pyglet.window.FPSDisplay(window=self._wnd)
        self._fps_display.label.font_size = 8
        self._fps_display.label.bold = False

        self._worker = Worker()
        self._worker.set_parent(self)

        Logger.get().logm("Initializing gui ...")

        self._gui = GuiBase()
        self._gui.init()
        self._gui.focus_remove()
        self._gui.create_modal_message("Loading ...")

        @self._wnd.event
        def on_draw(): self._draw_simple(None)

        pyglet.clock.schedule_once(self.init_do, 0.1)
        pyglet.app.run()

    def wnd_update(self, set_pos=False):
        self._wnd.set_vsync(Config.get().get_vsync())

        if Config.get().get_fullscreen():
            self._wnd.set_fullscreen(True)
            self._wnd_width = self._wnd.screen.width
            self._wnd_height = self._wnd.screen.height

        else:
            self._wnd.set_fullscreen(False)
            self._wnd.set_size(*Config.get().get_resolution())
            self._wnd_width, self._wnd_height = Config.get().get_resolution()

            if set_pos:
                wx = self._wnd.screen.x + int((self._wnd.screen.width - self._wnd_width) // 2)
                wy = self._wnd.screen.y + int((self._wnd.screen.height - self._wnd_height) // 2)

                self._wnd.set_location(wx, wy)

        self._on_resize(self._wnd_width, self._wnd_height)

    def init_do(self, dt):
        Logger.get().logm("Loading resources ...")

        pyglet.resource.path = ['resources']
        pyglet.resource.reindex()

        build_discrete_map()

        self._primitives = PrimitiveCollector()
        self._ais = AiCollector()

        self._images = ImageCollector()
        self._images.init()
        self._batches.init(self)

        Logger.get().logm("Initializing game content ...")

        ShotSpec.init_specs(self._images)
        UnitSpec.init_specs(self._images)
        BuildTurretSpec.init_specs(self._images)
        BuildSpec.init_specs(self._images)
        ActionItemSpec.init_specs()

        Logger.get().logm("Generating and preparing map ...")

        self._map = MapBase()
        self._map.init_grid_area()

        self._particles = ParticleCollector()
        self._shots = ShotCollector()
        self._moveables = MoveableCollector()
        self._builds = BuildCollector()
        self._sides = PlayerSideCollector()

        Logger.get().logm("Loading sounds ...")

        self._sounds = SoundCollector()
        self._sounds.init()

        Logger.get().logm("Preparing player sides ...")

        self._builds.set_refs(self._map, self._sides, self)
        self._moveables.set_refs(self._map, self._sides, self)
        self._sides.set_refs(self._moveables, self._builds, self._map, self._worker)

        Logger.get().logm("Starting game ...")

        random.seed()

        self._fr_timer = FrameTimer(ticks_qa=50)

        pyglet.clock.schedule_once(self.update_draw, 1.0 / self._fr_timer.get_time_ticks_qa())
        pyglet.clock.schedule_once(self.update_logic, 1.0 / 40)
        pyglet.clock.schedule(self.do_draw, 1.0 / 60)

        self._worker.start()

        @self._wnd.event
        def on_draw(): self._draw(None)

        @self._wnd.event
        def on_mouse_motion(x, y, dx, dy): self._mouse_motion(x, y, dx, dy)

        @self._wnd.event
        def on_mouse_drag(x, y, dx, dy, button, modifiers): self._mouse_motion(x, y, dx, dy)

        @self._wnd.event
        def on_mouse_press(x, y, button, modifiers): self._mouse_press(x, y, button, modifiers)

        @self._wnd.event
        def on_mouse_release(x, y, button, modifiers): self._mouse_release(x, y, button, modifiers)

        @self._wnd.event
        def on_mouse_scroll(x, y, scroll_x, scroll_y): self._mouse_scroll(x, y, scroll_x, scroll_y)

        @self._wnd.event
        def on_resize(width, height): self._on_resize(width, height)

        @self._wnd.event
        def on_show(): self._on_show()

        @self._wnd.event
        def on_text(text): self._on_text(text)

        @self._wnd.event
        def on_text_motion(motion): self._on_text_motion(motion)

        @self._wnd.event
        def on_key_press(symbol, modifiers): return self._on_keypress(symbol, modifiers)

        @self._wnd.event
        def on_key_release(symbol, modifiers): self._on_keyrelease(symbol, modifiers)

        @self._wnd.event
        def on_close(): self._on_close()

        self._gui.create_menu("menu")

        self._tick = 0
        self.wnd_update(set_pos=False)

        # UnitSpec.log_table()

    def set_prof(self, prof):
        self._prof = prof
        self._prof_flag = 0

    def get_x(self) -> float:
        return self._scX

    def get_y(self) -> float:
        return self._scY

    def get_width(self) -> int:
        return self._wnd_width

    def get_height(self) -> int:
        return self._wnd_height

    def get_view_rect(self):
        return self._scX, self._scY, self._wnd_width, self._wnd_height, self._zoom_use

    def get_view_vals(self):
        return self._scX, self._scY, self._zoom_use

    def get_zoom(self) -> float:
        return self._zoom_use

    def get_cur_tilesize(self) -> float:
        return self._tile_size

    def get_wnd(self):
        return self._wnd

    def get_mat(self):
        return self._mat

    def get_trans_mat(self):
        return self._trans_mat

    def update_work_side(self, x, y):
        if Config.get().get_allow_cheats():
            mref = next((c for c in [self._moveables.get_moveable_at_coord(x, y), self._builds.get_build_at_coord(x, y)] if c is not None), None)

            if mref is not None and (Config.get().get_allow_cheats() or not AiCollector().get_have_ai(mref.get_side())):
                self._gui.set_side(mref.get_side())

    def sc_fix(self, inc_x=0, inc_y=0):
        self._scX += inc_x
        self._scY += inc_y

        max_x = self._wnd_width / 2
        max_y = self._wnd_height / 2
        min_x = - self._map.get_width() * self.get_cur_tilesize() + self._wnd_width - self._wnd_width / 2
        min_y = - self._map.get_height() * self.get_cur_tilesize() + self._wnd_height - self._wnd_height / 2

        if self._map.get_width() * self.get_cur_tilesize() < self._wnd_width and False:
            self._scX = (min_x + max_x) / 2

        else:
            if self._scX > max_x:
                self._scX = max_x
            if self._scX < min_x:
                self._scX = min_x

        if self._map.get_height() * self.get_cur_tilesize() < self._wnd_height and False:
            self._scY = (min_y + max_y) / 2

        else:
            if self._scY > max_y:
                self._scY = max_y
            if self._scY < min_y:
                self._scY = min_y

        self._gui.scroll_update()

    def sc_to(self, x, y, zoom=-1):
        if zoom != -1:
            self._zoom = zoom
            self._zoom_use = zoom
            self._tile_size = 50.0 * self._zoom_use
            self._zoom_scalar = self._zoom_sc_max

        trg_x = - x * self.get_zoom() + self._wnd_width / 2
        trg_y = - y * self.get_zoom() + self._wnd_height / 2

        self._scX = trg_x
        self._scY = trg_y
        self.sc_fix()

    def update_draw(self, dt):
        self._fr_timer.start()

        self._trans_mat = pyglet.math.Mat4().translate(self._scX, self._scY, 0.0)
        self._trans_mat = self._trans_mat.scale(self._zoom_use, self._zoom_use, self._zoom_use)

        if self._worker.get_is_stopped():
            self._wnd.close()
            return False

        elif self._worker.get_game_state() == 0:
            self._draw_simple(dt)
            self._sounds.update()
            self._gui.update()
            self._batches.draw_logic()

            self._tick += 1

        elif self._worker.get_game_state() == 1:
            self._map.load_images()
            self._sides.load_images()
            self._worker.game_prepare()

            self._map.draw_update()
            self._particles.draw()
            self._shots.draw()
            self._moveables.draw()
            self._builds.draw()
            self._batches.draw_logic()

        elif self._worker.get_game_state() == 2:
            self._worker.game_run()

            self._map.draw_update()
            self._particles.draw()
            self._shots.draw()
            self._moveables.draw()
            self._builds.draw()
            self._batches.draw_logic()

        else:
            self._map.draw_update()
            self._particles.draw()
            self._shots.draw()
            self._moveables.draw()
            self._builds.draw()
            self._batches.draw_logic()

            self._sounds.update()
            self._gui.update()

            if self._tick % 120 == 0:
                vside = self._worker.check_victory_side()

                if vside is not None:
                    pb = GuiPromptBox("The " + vside.get_color().get_name() + " player won", vside.get_color().get_name().capitalize() + " player won, do you want to end the game?", "Keep playing", "Quit game")

                    @pb.event_ok(self)
                    def _(this):
                        if this._worker.game_clear():
                            this._gui.create_menu("menu")

            self._tick += 1

        #use_int = self._fr_timer.end()
        #pyglet.clock.schedule_once(self.update_draw, max(1.0 / 100, use_int))
        pyglet.clock.schedule_once(self.update_draw, 1.0 / 60)

    def update_logic(self, dt):
        if self._worker.get_is_stopped():
            self._wnd.close()
            return False

        elif self._worker.get_game_state() >= 2:
            self.update_view()

        pyglet.clock.schedule_once(self.update_logic, 1 / 40)

    def do_draw(self, dt, dtb):
        self._draw(dt)

    def update_view(self):
        #if self._windows.get_is_pan():
            #c = self._windows.pop_drag_coords()

            #self._scX += c[0]
            #self._scY += c[1]
            #self.sc_fix()

        if abs(self._zoom - self._zoom_use) > 0.0001:
            z_diff = (self._zoom - self._zoom_use) / 4

            dx = - self._scX + self._wnd_width / 2
            dy = - self._scY + self._wnd_height / 2
            tsz = self.get_cur_tilesize()

            if abs(z_diff) < 0.001:
                self._zoom_use = self._zoom
            else:
                self._zoom_use += z_diff

            if self._map.get_width() * 50.0 * self._zoom_use < self._wnd_width:
                sz = self._wnd_width / self._map.get_width()
                #self._zoom_use = sz / 50.0

            if self._map.get_height() * 50.0 * self._zoom_use < self._wnd_height:
                sz = self._wnd_height / self._map.get_height()
                #self._zoom_use = sz / 50.0

            self._tile_size = 50.0 * self._zoom_use
            diff_zoom = self._tile_size / tsz

            dx = dx * diff_zoom
            dy = dy * diff_zoom

            self._scX = self._wnd_width / 2 - dx
            self._scY = self._wnd_height / 2 - dy
            self.sc_fix()

        if self._windows.get_focused() is None:
            if self._windows.get_is_key(pyglet.window.key.LEFT):
                self.sc_fix(inc_x=2 if self._windows.get_is_shift() else 20)

            if self._windows.get_is_key(pyglet.window.key.RIGHT):
                self.sc_fix(inc_x=-2 if self._windows.get_is_shift() else -20)

            if self._windows.get_is_key(pyglet.window.key.UP):
                self.sc_fix(inc_y=-2 if self._windows.get_is_shift() else -20)

            if self._windows.get_is_key(pyglet.window.key.DOWN):
                self.sc_fix(inc_y=2 if self._windows.get_is_shift() else 20)

    def _draw_simple(self, dt):
        self._wnd.clear()

        self._batches.draw_simple()
        self._windows.draw()
        self._batches.draw_gui()
        self._fps_display.draw()

    def _draw(self, dt):
        self._wnd.clear()

        self._map.draw()
        self._primitives.draw()
        self._batches.draw()
        self._map.draw_black()
        self._windows.draw()
        self._batches.draw_gui()
        self._fps_display.draw()

    def _event_mouse(self, x, y):
        if self._worker.get_in_game() and self._gui.get_state() in [GuiBase.STATE_BUILDING, GuiBase.STATE_UPGRADE]:
            self._gui.building_set_tile(self._map.get_tile_by_coords_scroll(x, y))

    def _event_click(self, x, y, button):
        if not self._worker.get_in_game():
            return False

        work_side = self._gui.get_side()

        if work_side is None:
            self.update_work_side(x, y)

        elif self._gui.get_state() in [GuiBase.STATE_BUILDING, GuiBase.STATE_UPGRADE]:

            if button == 0:
                self._gui.building_finish()
            elif button == 1:
                self._gui.focus_remove()

        elif self._gui.get_state() == GuiBase.STATE_TARGET:

            if self._gui.get_focus_target_type() == "go":
                work_side.selected_goto(self._map.get_place_by_coords_scroll(x, y))

            elif self._gui.get_focus_target_type() == "attack":
                work_side.selected_goto_attack(work_side.get_targetable_at_coords(x, y))

            elif self._gui.get_focus_target_type() == "attack_move":
                work_side.selected_attack_move(self._map.get_place_by_coords_scroll(x, y))

            self._gui.focus_moveables_back()

        elif button == 1:
            # click right

            if self._gui.get_state() == GuiBase.STATE_FOCUS_BUILDS:
                work_side.selected_place_travel(self._map.get_place_by_coords_scroll(x, y))

            else:
                mref = work_side.get_targetable_at_coords(x, y)

                if self._windows.get_is_key(pyglet.window.key.LCTRL) or self._windows.get_is_key(pyglet.window.key.RCTRL):
                    work_side.selected_attack_move(self._map.get_place_by_coords_scroll(x, y))
                elif mref is not None:
                    work_side.selected_goto_attack(mref)
                else:
                    work_side.selected_goto(self._map.get_place_by_coords_scroll(x, y))

        elif button == 0:
            # click left
            if not self._windows.get_is_shift():
                self._gui.focus_remove()
                work_side.clear_selected()

                mref = next((c for c in [self._moveables.get_moveable_at_coord(x, y), self._builds.get_build_at_coord(x, y)] if c is not None), None)

                if Config.get().get_allow_cheats() and mref is not None and (Config.get().get_allow_cheats() or not AiCollector().get_have_ai(mref.get_side())):
                    self._gui.set_side(mref.get_side())

            mm = self._sides.select_one(work_side, x, y)

            if mm is not None and (self._windows.get_is_key(pyglet.window.key.LCTRL) or self._windows.get_is_key(pyglet.window.key.RCTRL)):
                self._sides.select_at_range(work_side, 0, 0, self.get_width(), self.get_height(), mm.get_spec().get_key())

            self._gui.focus(work_side)

            if mm is not None:
                print(json.dumps(mm.serialize(), indent=4))

    def _event_double_click(self, x, y, button):
        if not self._worker.get_in_game():
            return False

        work_side = self._gui.get_side()

        if work_side is None:
            self.update_work_side(x, y)

        if self._gui.get_state() in [GuiBase.STATE_BUILDING, GuiBase.STATE_UPGRADE]:

            if button == 0:
                self._gui.building_finish()
            elif button == 1:
                self._gui.focus_remove()

        elif self._gui.get_state() == GuiBase.STATE_TARGET:

            if self._gui.get_focus_target_type() == "go":
                work_side.selected_goto(self._map.get_place_by_coords_scroll(x, y))

            elif self._gui.get_focus_target_type() == "attack":
                work_side.selected_goto_attack(work_side.get_targetable_at_coords(x, y))

            elif self._gui.get_focus_target_type() == "attack_move":
                work_side.selected_attack_move(self._map.get_place_by_coords_scroll(x, y))

            self._gui.focus_moveables_back()

        elif button == 0:
            mref = self._moveables.get_moveable_at_coord(x, y)

            if not self._windows.get_is_shift():
                self._gui.focus_remove()
                work_side.clear_selected()

            if mref is not None and mref.get_side() is work_side:
                self._sides.select_at_range(work_side, 0, 0, self.get_width(), self.get_height(), mref.get_spec().get_key())
                self._gui.focus(work_side)
            else:
                bref = self._builds.get_build_at_coord(x, y)

                if bref is not None and bref.get_side() is work_side:
                    self._sides.select_builds_at_range(work_side, 0, 0, self.get_width(), self.get_height(), bref.get_spec().get_key())
                    self._gui.focus(work_side)

    def _event_drag_area(self, sx, sy, ex, ey, button):
        if not self._worker.get_in_game():
            return False

        work_side = self._gui.get_side()

        if button == 0 and self._gui.get_state() in [GuiBase.STATE_BUILDING, GuiBase.STATE_UPGRADE]:
            pass

        elif button == 0 and work_side is not None:
            if not self._windows.get_is_shift():
                self._gui.focus_remove()
                work_side.clear_selected()

            self._sides.select_at_range(work_side, sx, sy, ex, ey)
            self._gui.focus(work_side)

    def _event_drag_move(self, dx, dy):
        self._scX += dx
        self._scY += dy
        self.sc_fix()

    def _mouse_motion(self, x, y, dx, dy):
        self._windows.mouse_motion(x, y)

    def _mouse_press(self, x, y, button, modifiers):
        self._windows.mouse_press(x, y, button)

    def _mouse_release(self, x, y, button, modifiers):
        self._windows.mouse_release(x, y, button)

    def _mouse_scroll(self, x, y, scroll_x, scroll_y):
        if scroll_y > 0:
            # kolecko nahoru
            self._zoom_scalar = min(self._zoom_scalar + 1, 35)

        elif scroll_y < 0:
            # kolecko dolu
            self._zoom_scalar = max(self._zoom_scalar - 1, 27)

        if self._zoom_scalar < 1:
            self._zoom_scalar = 1

        if self._zoom_scalar < self._zoom_sc_max:
            # zoom out
            self._zoom = 0.9 ** (self._zoom_sc_max - self._zoom_scalar)
        else:
            # zoom in
            self._zoom = 1 / 1.1 ** (self._zoom_sc_max - self._zoom_scalar)

        Logger.get().log("zoom: " + str(self._zoom) + " | s: " + str(self._zoom_scalar))

    def _on_resize(self, width, height):
        self._wnd_width = width
        self._wnd_height = height

        self._windows.set_size(self._wnd_width, self._wnd_height)
        self._mat = create_orthogonal(0, self._wnd_width, 0, self._wnd_height, -255, 255)
        self.sc_fix()

    def _on_show(self):
        self._wnd_width = self._wnd.width
        self._wnd_height = self._wnd_height
        self._windows.set_size(self._wnd_width, self._wnd_height)
        self.sc_fix()

    def _on_text(self, text):
        self._windows.on_text(text)

    def _on_text_motion(self, motion):
        self._windows.on_text_motion(motion)

    def _on_keypress(self, symbol, modifiers):

        if self._worker.get_game_state() == 3 and self._windows.get_focused() is None:
            work_side = self._gui.get_side()

            if work_side is not None:
                x, y = self._windows.get_mouse_x(), self._windows.get_mouse_y()

                if symbol == pyglet.window.key.A: # go
                    work_side.selected_attack_move(self._map.get_place_by_coords_scroll(x, y))

                elif symbol == pyglet.window.key.G:
                    work_side.selected_goto(self._map.get_place_by_coords_scroll(x, y))

                elif symbol == pyglet.window.key.H:
                    work_side.selected_hold()

                elif symbol == pyglet.window.key.S:
                    work_side.selected_stop()

                elif symbol == pyglet.window.key.S:
                    #work_side.selected_goto_attack(work_side.get_targetable_at_coords(x, y))
                    pass

                elif symbol == pyglet.window.key.D:
                    # attack move
                    pass

        if self._worker.get_game_state() == 3 and Config.get().get_allow_cheats() and self._windows.get_focused() is None:
            if symbol == pyglet.window.key._1:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_lighttank"))

            if symbol == pyglet.window.key._2:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_medtank"))

            if symbol == pyglet.window.key._3:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_jeep"))

            if symbol == pyglet.window.key._4:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_plate"))

            if symbol == pyglet.window.key._5:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_rocket"))

            if symbol == pyglet.window.key._6:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_troop"))

            if symbol == pyglet.window.key._7:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_troop_cannon"))

            if symbol == pyglet.window.key._8:
                self._sides.get_sides()[0].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s1_sniper"))

            if symbol == pyglet.window.key.NUM_1:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_basic"))

            if symbol == pyglet.window.key.NUM_2:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_mini"))

            if symbol == pyglet.window.key.NUM_3:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_plasma"))

            if symbol == pyglet.window.key.NUM_4:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_rock"))

            if symbol == pyglet.window.key.NUM_5:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_troop"))

            if symbol == pyglet.window.key.NUM_6:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_troop_beam"))

            if symbol == pyglet.window.key.NUM_7:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_troop_rocket"))

            if symbol == pyglet.window.key.NUM_8:
                self._sides.get_sides()[1].add_unit_coord_command(self._windows.get_mouse_x(), self._windows.get_mouse_y(), UnitSpec.get("s2_partilery"))

        if not self._windows.keypress(symbol):
            return pyglet.event.EVENT_HANDLED
        else:
            return pyglet.event.EVENT_HANDLED

    def _on_keyrelease(self, symbol, modifiers):
        if not self._windows.keyrelease(symbol):
            return False

        if symbol == pyglet.window.key.ESCAPE:
            pb = GuiPromptBox("Quit game", "Do you want to quit game?", "No", "Yes, quit")

            @pb.event_ok(self)
            def _(this):
                if this._worker.game_clear():
                    this._gui.create_menu("menu")
                else:
                    this._worker.game_quit()

        if symbol == pyglet.window.key.ENTER and self._worker.get_game_state() >= 2:

            if GuiChatBox.glob_ref is None:
                cbx = GuiChatBox("Enter chat message", "Cancel", "Send")

                @cbx.event_ok(self)
                def _(this, message):
                    this._worker.chat_message(message)

            else:
                self._worker.chat_message(GuiChatBox.glob_ref.get_message())
                GuiChatBox.glob_ref.destroy()

        if self._worker.get_game_state() == 3 and Config.get().get_allow_cheats() and self._windows.get_focused() is None and False:
            if symbol == pyglet.window.key.R:

                px = (self._windows.get_mouse_x() - self.get_x()) / self.get_zoom()
                py = (self._windows.get_mouse_y() - self.get_y()) / self.get_zoom()

                tile = self._map.get_tile_by_coords(px, py)

                if tile is not None:
                    print("tile [" + str(tile.get_pos_x()) + ", " + str(tile.get_pos_y()) + "]")
                    tile.set_type_update(MapTileTexture.T_CONCGROUND)

            if symbol == pyglet.window.key.T:

                px = (self._windows.get_mouse_x() - self.get_x()) / self.get_zoom()
                py = (self._windows.get_mouse_y() - self.get_y()) / self.get_zoom()

                tile = self._map.get_tile_by_coords(px, py)

                if tile is not None:
                    tile.get_tex().print_overview()
                    tile.set_type_update(MapTileTexture.T_GRASS)

            if symbol == pyglet.window.key.Y:

                px = (self._windows.get_mouse_x() - self.get_x()) / self.get_zoom()
                py = (self._windows.get_mouse_y() - self.get_y()) / self.get_zoom()

                tile = self._map.get_tile_by_coords(px, py)

                if tile is not None:
                    tile.get_tex().print_overview()
                    tile.set_type_update(MapTileTexture.T_FIELD)

            if symbol == pyglet.window.key.U:

                px = (self._windows.get_mouse_x() - self.get_x()) / self.get_zoom()
                py = (self._windows.get_mouse_y() - self.get_y()) / self.get_zoom()

                tile = self._map.get_tile_by_coords(px, py)

                if tile is not None:
                    tile.get_tex().print_overview()
                    tile.set_type_update(MapTileTexture.T_TREE)

            if symbol == pyglet.window.key.F:
                if Config.get().get_fog_type() == Config.FOG_ALL:
                    Config.get().set_fog_type(Config.FOG_ALLOW)
                    self._sides.set_playable_sides([self._sides.get_sides()[0]])
                    self._map.update_sides(self._sides)
                    self._map.force_update()
                    print("FOG_ALLOW")

                else:
                    Config.get().set_fog_type(Config.FOG_ALL)
                    self._sides.set_playable_sides(self._sides.get_sides())
                    self._map.update_sides(self._sides)
                    self._map.force_update()
                    print("FOG_ALL")

            if symbol == pyglet.window.key.E:
                self._primitives.clear()

            if symbol == pyglet.window.key.Q:

                px = round(math.floor((self._windows.get_mouse_x() - self.get_x()) / 1) / self.get_zoom())
                py = round(math.floor((self._windows.get_mouse_y() - self.get_y()) / 1) / self.get_zoom())
                pl = self._map.get_place_by_coords(px, py)

                rc = self._batches.get_envs().create(EnvRect.TYPE_TREE)
                rc.set_pos(px, py)
                rc.create_sub()

            if symbol == pyglet.window.key.P:
                if self._prof is None:
                    pass

                elif self._prof_flag == 0:
                    self._prof.enable()
                    self._prof_flag = 1
                    print("Profiler enabled")

                else:
                    self._prof.print_stats(sort="tottime")
                    self._prof.disable()
                    self._prof_flag = 0
                    print("Profiler disabled")

    def _on_close(self):
        self._worker.stop()
