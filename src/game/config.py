import json

from game.gamestat.gamespec import GameSpec


class Config:

    RES_SQUARE = 1100, 1100
    RES_HD = 1280, 720
    RES_HD_PLUS = 1600, 900
    RES_FULLHD = 1920, 1080
    RES_QHD = 2560, 1440

    FOG_ALLOW = 0
    FOG_ALL = 1
    FOG_NONE = 2

    _conf_file = "settings.json"
    _ref = None
    _map_generator_seed = None
    _map_blank = False
    _fog_type = True
    _quick_tick = False
    _allow_cheats = False
    _resolution = None, None
    _fullscreen = False
    _play_sounds = True
    #_master_volume = 0.05
    _master_volume = 0.2
    _game_spec_single = None
    _game_spec_multi = None
    _port = 65111
    _listen_host = '0.0.0.0'
    #_connect_host = '127.0.0.1'
    _connect_host = '10.0.0.200'
    #_connect_host = '95.168.217.251'
    _client_full_state_check = False

    def __init__(self):
        self._map_generator_seed = 92
        self._map_blank = False
        self._fog_type = Config.FOG_ALLOW
        self._quick_tick = False
        self._allow_cheats = False
        self._play_sounds = True
        self._game_spec_single = None
        self._game_spec_multi = None
        self._resolution = Config.RES_FULLHD
        #self._resolution = Config.RES_SQUARE
        self._fullscreen = False
        self._vsync = True

    def get_map_generator_seed(self, update=True):
        seed = self._map_generator_seed

        if update:
            self._map_generator_seed += 1

        return seed

    def get_map_blank(self):
        return self._map_blank

    def get_fog_type(self):
        return self._fog_type

    def get_is_fog_enabled(self):
        return self._fog_type in [Config.FOG_ALLOW, Config.FOG_ALL]

    def get_quick_tick(self):
        return self._quick_tick

    def get_allow_cheats(self):
        return self._allow_cheats

    def get_play_sounds(self):
        return self._play_sounds

    def get_master_volume(self):
        return self._master_volume

    def get_resolution(self):
        return self._resolution

    def get_fullscreen(self):
        return self._fullscreen

    def get_vsync(self):
        return self._vsync

    def get_game_spec_single(self):
        return self._game_spec_single

    def get_game_spec_multi(self):
        return self._game_spec_multi

    def get_port(self):
        return self._port

    def get_listen_host(self):
        return self._listen_host

    def get_connect_host(self):
        return self._connect_host

    def get_client_full_state_check(self):
        return self._client_full_state_check

    def set_fog_type(self, fog_type):
        self._fog_type = fog_type

    def set_allow_cheats(self, allow_cheats):
        self._allow_cheats = allow_cheats

    def set_resolution(self, resolution):
        self._resolution = resolution

    def set_master_volume(self, master_volume):
        self._master_volume = master_volume

    def set_fullscreen(self, fullscreen):
        self._fullscreen = fullscreen

    def set_vsync(self, vsync):
        self._vsync = vsync

    def set_game_spec_single(self, game_spec_single):
        self._game_spec_single = game_spec_single

    def set_game_spec_multi(self, game_spec_multi):
        self._game_spec_multi = game_spec_multi

    def set_connect_host(self, connect_host):
        self._connect_host = connect_host

    def set_listen_host(self, listen_host):
        self._listen_host = listen_host

    @staticmethod
    def get_resolution_values(capt=True, find_value=None):
        out = {
            "square": "SQUARE" if capt else Config.RES_SQUARE,
            "hd": "HD" if capt else Config.RES_HD,
            "hdplus": "HD+" if capt else Config.RES_HD_PLUS,
            "fullhd": "FULLHD" if capt else Config.RES_FULLHD,
            "qhd": "QHD" if capt else Config.RES_QHD
        }

        if find_value is None:
            return out
        else:
            for key in out.keys():
                if out[key][0] == find_value[0] and out[key][1] == find_value[1]:
                    return key

            return ""

    @staticmethod
    def get_fog_type_values(capt=True, find_value=None):
        out = {
            "allow": "enabled" if capt else Config.FOG_ALLOW,
            "all": "show all players" if capt else Config.FOG_ALL,
            "none": "disabled" if capt else Config.FOG_NONE
        }

        if find_value is None:
            return out
        else:
            for key in out.keys():
                if out[key] == find_value:
                    return key

            return ""

    def test_a(self):
        self._quick_tick = False
        self._allow_cheats = False
        self._vsync = True
        self._fog_type = Config.FOG_ALL
        #self._fog_type = Config.FOG_ALLOW
        #self._fog_type = Config.FOG_NONE
        self._map_blank = False
        #self._map_generator_seed = 107

        return True

    def test_srv(self):
        self._quick_tick = False
        self._allow_cheats = False
        self._vsync = True
        self._fog_type = Config.FOG_ALL
        self._map_blank = False

        return True

    def set_standalone(self):
        pass

    def set_client(self):
        pass

    def test_only_map(self):
        self._vsync = True
        self._fog_type = Config.FOG_NONE

        return True

    def server(self):
        self._play_sounds = False

    def save(self):
        dt = {
            'map_generator_seed': self._map_generator_seed,
            'map_blank': self._map_blank,
            'fog_type': self._fog_type,
            'allow_cheats': self._allow_cheats,
            'resolution': self._resolution,
            'fullscreen': self._fullscreen,
            'vsync': self._vsync,
            'play_sounds': self._play_sounds,
            'master_volume': self._master_volume,
            'game_spec_single': None if self._game_spec_single is None else self._game_spec_single.serialize(),
            'game_spec_multi': None if self._game_spec_multi is None else self._game_spec_multi.serialize(),
            'port': self._port,
            'listen_host': self._listen_host,
            'connect_host': self._connect_host
        }

        jstr = json.dumps(dt, indent=4)

        try:
            with open(Config._conf_file, 'w') as fl:
                fl.write(jstr)

        except IOError:
            pass

    def load(self):
        try:
            with open(Config._conf_file, 'r') as fl:
                jstr = fl.read()

            dt = json.loads(jstr)

            self._map_generator_seed = int(dt['map_generator_seed'])
            self._map_blank = bool(dt['map_blank'])
            self._fog_type = int(dt['fog_type'])
            self._allow_cheats = int(dt['allow_cheats'])
            self._fullscreen = bool(dt['fullscreen'])
            self._vsync = bool(dt['vsync'])
            self._play_sounds = bool(dt['play_sounds'])
            self._master_volume = float(dt['master_volume'])
            self._port = int(dt['port'])
            self._listen_host = str(dt['listen_host'])
            self._connect_host = str(dt['connect_host'])

            if isinstance(dt['resolution'], list) and len(dt['resolution']) == 2:
                self._resolution = int(dt['resolution'][0]), int(dt['resolution'][1])

            self._game_spec_single = None
            self._game_spec_multi = None

            if dt['game_spec_single']:
                self._game_spec_single = GameSpec().unserialize(dt['game_spec_single'])
            if dt['game_spec_multi']:
                self._game_spec_multi = GameSpec().unserialize(dt['game_spec_multi'])

        except KeyError:
            pass
        except ValueError:
            pass
        except FileNotFoundError:
            pass
        except IOError:
            pass

    @staticmethod
    def get():
        if Config._ref is None:
            Config._ref = Config()

        return Config._ref
