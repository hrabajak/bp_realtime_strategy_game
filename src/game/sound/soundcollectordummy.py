from game.helper.singleton import Singleton
from game.interface.isounds import ISounds


class SoundCollectorDummy(ISounds, metaclass=Singleton):

    def __init__(self):
        ISounds.__init__(self)

    def play_sound(self, media, x, y, pitch):
        pass
