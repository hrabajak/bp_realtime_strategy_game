import math
import pyglet

from game.config import Config
from game.interface.iview import IView


class SoundPlayer:

    _view: IView = None
    _parent = None
    _player = None
    _media = None
    _play_flag = False
    _avail = False
    _x = 0
    _y = 0
    _pitch = 0
    _use_coords = True
    _master_volume = 1.0

    def __init__(self, parent):
        self._view = IView.get()
        self._parent = parent
        self._player = pyglet.media.Player()
        self._player.play()
        self._avail = True
        self._use_coords = True
        self._master_volume = Config.get().get_master_volume()

        @self._player.event
        def on_eos(): self._on_player_end()

    def destroy(self):
        self._player.pause()

    def get_is_avail(self):
        return self._avail

    def play(self, media, x, y, pitch, use_coords=True):
        """
        if (not self._avail):
            self._player.delete()
            self._player = pyglet.media.Player()

            @self._player.event
            def on_eos(): self._on_player_end()
        """

        self._media = media
        self._avail = False
        self._master_volume = Config.get().get_master_volume()
        self._x = x
        self._y = y
        self._pitch = pitch
        self._use_coords = use_coords
        self._play_flag = True

    def _on_player_end(self):
        self._parent.play_stop(self)

    def stop(self):
        self._player.delete()
        self._player = pyglet.media.Player()
        self._player.play()

        @self._player.event
        def on_eos(): self._on_player_end()

        self._avail = True

    def update(self):
        if not self._avail:
            if self._use_coords:
                dist = math.sqrt((self._view.get_width() / 2 - self._view.get_x() - self._x * self._view.get_zoom()) ** 2 + (self._view.get_height() / 2 - self._view.get_y() - self._y * self._view.get_zoom()) ** 2)
                area = max(self._view.get_width(), self._view.get_height())

                if dist <= area / 4:
                    koef = 1.0
                else:
                    koef = 1.0 - (dist - area / 4) / area
                    if koef < 0:
                        koef = 0
            else:
                koef = 1.0

            self._player.volume = koef * self._master_volume

            if self._play_flag:
                self._play_flag = False

                # zvuk se musi poustet taktez ve hlavnim vlakne
                self._player.queue(self._media)
                self._player.pitch = self._pitch
