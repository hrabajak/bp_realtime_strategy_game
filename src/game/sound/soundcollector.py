from threading import Lock

import pyglet

from game.config import Config
from game.helper.singleton import Singleton
from game.interface.isounds import ISounds
from game.sound.soundplayer import SoundPlayer


class SoundCollector(ISounds, metaclass=Singleton):

    _lock = None
    _sounds = {}
    _players = None
    _in_play = None
    _max = 30

    def __init__(self):
        pass

    def init(self):
        ISounds.__init__(self)

        self._lock = Lock()
        self._players = []
        self._in_play = []

        while len(self._players) < self._max:
            self._players.append(SoundPlayer(self))

        self.add_sound("resources/sound/explosion0.wav")
        self.add_sound("resources/sound/human_die.wav")
        self.add_sound("resources/sound/human_hit1.wav")
        self.add_sound("resources/sound/human_hit2.wav")
        self.add_sound("resources/sound/human_hit3.wav")
        self.add_sound("resources/sound/laser_shot.wav")
        self.add_sound("resources/sound/laser_shot2.wav")
        self.add_sound("resources/sound/launch.wav")
        self.add_sound("resources/sound/minigun.wav")
        self.add_sound("resources/sound/machinegun.wav")
        self.add_sound("resources/sound/mini_hit.wav")
        self.add_sound("resources/sound/mini_rocket.wav")
        self.add_sound("resources/sound/plasma.wav")
        self.add_sound("resources/sound/plasma_small.wav")
        self.add_sound("resources/sound/rocket.wav")
        self.add_sound("resources/sound/rocket_load.wav")
        self.add_sound("resources/sound/sniper.wav")
        self.add_sound("resources/sound/tank_fire.wav")
        self.add_sound("resources/sound/hit.wav")
        self.add_sound("resources/sound/hit2.wav")
        self.add_sound("resources/sound/xpl1.wav")
        self.add_sound("resources/sound/ex1.wav")
        self.add_sound("resources/sound/ex2.wav")
        self.add_sound("resources/sound/burst.wav")
        self.add_sound("resources/sound/sniper.wav")
        self.add_sound("resources/sound/upgrade.wav")
        self.add_sound("resources/sound/const.wav")
        self.add_sound("resources/sound/create1.wav")
        self.add_sound("resources/sound/create2.wav")
        self.add_sound("resources/sound/blueshot.wav")
        self.add_sound("resources/sound/blueshot_hit.wav")
        self.add_sound("resources/sound/message.wav")
        self.add_sound("resources/sound/error.wav")
        self.add_sound("resources/sound/ready.wav")
        self.add_sound("resources/sound/place.wav")
        self.add_sound("resources/sound/add.wav")
        self.add_sound("resources/sound/undo.wav")

        self.play_sound("explosion0", 0, 0, 1.0, use_coords=False)

    def destroy(self):
        for pla in self._players:
            pla.destroy()

    def add_sound(self, path):
        idx = path.rindex("/")

        if idx >= 0:
            key = path[idx + 1:]
        else:
            key = path

        idx = key.rindex(".")

        if idx >= 0:
            key = key[:idx]

        self._sounds[key] = {
            "media": None
        }

        snd = pyglet.media.StaticSource(pyglet.media.load(path, streaming=False))
        self._sounds[key]["media"] = snd

    def play_sound(self, key, x, y, pitch, use_coords=True):
        if Config.get().get_play_sounds():
            self._lock.acquire()

            snd = self._sounds[key]["media"]
            idx = 0

            while idx < len(self._players) and not self._players[idx].get_is_avail():
                idx += 1

            if idx < len(self._players):
                pla = self._players[idx]
                pla.play(snd, x, y, pitch, use_coords)
                self._in_play.append(pla)

            # elif (len(self._in_play) > 0):
                # pla = self._in_play[0]
                # pla.play(snd, x, y, pitch)

            self._lock.release()

    def play_stop(self, pla):
        self._lock.acquire()
        self._in_play.remove(pla)
        pla.stop()
        self._lock.release()

    def update(self):
        for pla in self._players:
            if not pla.get_is_avail():
                pla.update()
