import random
from collections import OrderedDict

from lib.orderedset import OrderedSet

from game.interface.iselectable import ISelectable
from game.map.pathfinderflow import PathFinderFlow


class GotoGroup:

    _refs = OrderedSet()

    _childs = None
    _child_numbers = None
    _max_speed = None
    _key = 0
    _keys = None
    _change = False
    _sort_flag = False
    _path_flow = None
    _rel_object = None

    def __init__(self):
        self._childs = []
        self._child_numbers = OrderedSet()
        self._key = 0
        self._keys = OrderedDict()
        self._rel_object = None

    def get_group_keys(self):
        return self._keys.keys()

    def get_group_moveables(self, group_key):
        return self._keys[group_key]["set"]

    def get_path_flow(self):
        return self._path_flow

    def get_rel_object(self):
        return self._rel_object

    def add(self, mref):
        self._childs.append(mref)
        self._child_numbers.add(mref.number)
        self._init_key(mref)
        self._change = True
        self._sort_flag = True

        if self._max_speed is None or mref.get_spec().get_speed() < self._max_speed:
            self._max_speed = mref.get_spec().get_speed()

    def rem(self, mref):
        if self._path_flow is not None:
            self._path_flow.rem_moveable(mref)

        self._keys[mref.get_goto_key()]["set"].remove(mref)

        if len(self._keys[mref.get_goto_key()]["set"]) == 0:
            del self._keys[mref.get_goto_key()]

        self._childs.remove(mref)
        self._child_numbers.remove(mref.number)

        if len(self._childs) == 0:
            GotoGroup._refs.remove(self)
        else:
            self._update_speed()
            self._change = True

    def sort_by_place(self, place_to):
        self._childs = sorted(self._childs, key=lambda mref: place_to.dist_to_moveable(mref) / mref.get_max_speed(), reverse=True)

    def sort_childs(self):
        self._sort_flag = True

    def prepare(self, path_flow=None, rel_object=None):
        for mref in self._childs:
            mref.set_max_speed(self._max_speed)

        self._path_flow = path_flow
        self._rel_object = rel_object

    def get_childs(self):
        return self._childs

    def get_mref_score(self, mref):
        flow = self._path_flow
        ent = None if flow is None else flow.get_entity(mref)
        return 999999 if ent is None else ent.get_score()

    def get_childs_iterate(self):
        if self._sort_flag:
            self._childs = sorted(self._childs, key=lambda mref: self.get_mref_score(mref), reverse=False)
            self._sort_flag = False

        return self._childs

    def _init_key(self, mref):
        self._key += 1
        self._keys[self._key] = {
            "set": OrderedSet(),
            "dist": 0
        }
        self._keys[self._key]["set"].add(mref)
        mref.set_goto_key(self._key)
        self._change = True

        return self._key

    def generate_key(self, mref):
        self._keys[mref.get_goto_key()]["set"].remove(mref)

        if len(self._keys[mref.get_goto_key()]["set"]) == 0:
            del self._keys[mref.get_goto_key()]

        self._init_key(mref)

    def merge_keys(self, key_a, key_b):
        if key_a == key_b:
            return False

        else:
            if len(self._keys[key_a]["set"]) > len(self._keys[key_b]["set"]):
                key_keep = key_a
                key_merge = key_b
            else:
                key_keep = key_b
                key_merge = key_a

            for mm in self._keys[key_merge]["set"]:
                mm.set_goto_key(key_keep)
                self._keys[key_keep]["set"].add(mm)

            del self._keys[key_merge]
            self._change = True

            return True

    def _update_speed(self):
        if len(self._childs) > 0:
            speeds = list([c.get_spec().get_speed() for c in self._childs])
            self._max_speed = min(speeds) if len(speeds) > 0 else 0

    def get_is_relevant(self):
        return len(self._childs) > 1 and self._max_speed > 0

    def get_max_speed(self):
        return self._max_speed

    def get_is_match_childs(self, child_numbers):
        return self._child_numbers == child_numbers

    def update_item(self, tick, force=False):

        if self._path_flow is not None:
            self._path_flow.update(tick)

        if self._change or force:
            self._change = False

            if len(self._keys) == 1:
                for mref in self._childs:
                    mref.set_max_speed(self._max_speed)

            else:
                max_dist = 0

                for key in self._keys.keys():
                    dist = sum([mref.get_path_list_len() for mref in self._keys[key]["set"]]) / len(self._keys[key]["set"])
                    self._keys[key]["dist"] = dist

                    if dist > max_dist:
                        max_dist = dist

                for key in self._keys.keys():
                    if self._keys[key]["dist"] + 2 < max_dist:
                        [mref.set_max_speed(self._max_speed, True) for mref in self._keys[key]["set"]]

                    elif self._keys[key]["dist"] - 2 > max_dist:
                        [mref.set_max_speed(mref.get_spec().get_speed()) for mref in self._keys[key]["set"]]

                    else:
                        [mref.set_max_speed(self._max_speed) for mref in self._keys[key]["set"]]

    @staticmethod
    def create():
        grp = GotoGroup()
        GotoGroup._refs.add(grp)
        return grp

    @staticmethod
    def update(tick, force=False):
        for grp in GotoGroup._refs:
            grp.update_item(tick, force)

    @staticmethod
    def clear():
        GotoGroup._refs = OrderedSet()

    @staticmethod
    def find_existing(childs):
        nums = OrderedSet()

        for mref in childs:
            if mref.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
                nums.add(mref.number)

        for grp in GotoGroup._refs:
            if grp.get_is_match_childs(nums):
                return grp

        return None
