import random
import math
import pyglet

from game.graphic.graphbatches import GraphBatches
from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.discrete cimport angle_coords, get_diff_from_angle, get_angle_from
from game.helper.discretemath cimport int_sqrt
from game.helper.distangle import DistAngle
from game.image.imagecollector import ImageCollector
from game.interface.icoordsnap import ICoordSnap
from game.interface.idrawable import IDrawable
from game.interface.imoveables import IMoveables
from game.interface.iparticles import IParticles
from game.interface.iselectable import ISelectable
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.map.mapscanner import MapScanner
from game.map.maptilearea import MapTileArea
from game.moveable.gotogroup import GotoGroup
from game.moveable.moveablebuilder import MoveableBuilder
from game.moveable.moveableminer import MoveableMiner
from game.moveable.moveablepath import MoveablePath
from game.moveable.moveabletarget import MoveableTarget
from game.map.pathfinder import PathFinder
from game.moveable.moveableupgrade import MoveableUpgrade
from game.particle.particlecollector import ParticleCollector
from game.player.playerside import PlayerSide
from game.const.specialflags import SpecialFlags
from game.moveable.unitspec import UnitSpec
from game.helper.vector cimport angle_dist_side, angle_inc_border_deg, angle_dist_side_deg, angle_conv_deg, angle_conv_realdeg
from game.shot.shotcollector import ShotCollector
from game.sound.soundcollector import SoundCollector


class MoveableItem(ITargetable, ISelectable, IDrawable, ICoordSnap):

    _moveables: IMoveables = None
    _shots: IShots = None
    _particles: IParticles = None
    _sounds: ISounds = None
    _batches = None

    _allow_lines = False

    _side: PlayerSide = None
    _spec: UnitSpec = None
    _path: MoveablePath = None
    _target: MoveableTarget = None
    _miner: MoveableMiner = None
    _upgrade: MoveableUpgrade = None
    _builder: MoveableBuilder = None
    _map_scr: MapScanner = None
    _visible = False

    _sprite_init = False
    _sprite_body = None
    _sprite_turret = None
    _sprite_canon = None
    _sprite_select = None
    _sprite_up = 0
    _sprite_up_mined = 0
    _sprite_up_upgraded = 0
    _sprite_up_walk_next = 0
    _sprite_up_shot = 0
    _sprite_up_damage = 0

    _line = None
    _line_to = None
    _selected = None
    _attack_place = None
    _action_item = None
    _tile_area = None
    _goto_group = None
    _goto_key = None
    _callback_dead = None
    _callback_damage = None
    _callback_kill = None
    _x = 0
    _y = 0
    _dec = 0
    _angle = 0
    _angle_deg = 0
    _angle_dist = None
    _health = 0
    _walk_frame = 0
    _is_dead = False

    _idle_value = 0
    _idle_angle = 0
    _idle_dir = 0

    _born = SpecialFlags.BORN_READY

    _look_angle = 0
    _look_angle_deg = 0
    _look_trg_angle_deg = 0
    _look_focus = True
    _look_snap = None

    _force_turret = 0
    _force_canon = 0
    _dmg_fire_tick = 0
    _dmg_smoke_tick = 0
    _dmg_hit_timeout = 0

    _note = None

    dist = 0
    number = 0
    number_total = 0
    path_update = False
    group_ai = None
    flag_ai = False

    focus_ai = None
    focus_ai_dist = -1
    focus_ai_perc = 0
    focus_ai_perc_min = 0

    def __init__(self, side: PlayerSide, spec: UnitSpec, path_fnd: PathFinder, map_scr: MapScanner, set_number=-1):
        ITargetable.__init__(self)
        ISelectable.__init__(self)
        IDrawable.__init__(self)
        ICoordSnap.__init__(self)

        self._moveables = IMoveables.get()
        self._shots: IShots = ShotCollector()
        self._particles: IParticles = ParticleCollector()
        self._sounds: ISounds = SoundCollector()
        self._batches = GraphBatches()

        MoveableItem.calc_number = 0

        if set_number == -1:
            self.number = MoveableItem.number_total
            MoveableItem.number_total += 1
        else:
            self.number = set_number
            MoveableItem.number_total = max(MoveableItem.number_total, set_number) + 1

        self._born = SpecialFlags.BORN_READY
        self._side = side
        self._spec = spec
        self._map_scr = map_scr
        self._goto_group = GotoGroup.create()
        self._goto_group.add(self)
        self._path = MoveablePath(self, path_fnd, self._map_scr)
        self._target = MoveableTarget(self)

        if SpecialFlags.SPECIAL_MINER in self._spec.get_specials():
            self._miner = MoveableMiner(self, self._map_scr)

        elif SpecialFlags.SPECIAL_UPGRADE in self._spec.get_specials():
            self._upgrade = MoveableUpgrade(self, self._map_scr)

        elif SpecialFlags.SPECIAL_BUILD in self._spec.get_specials():
            self._builder = MoveableBuilder(self, self._map_scr)

        if self._spec.get_size() == 2:
            self._dec = 12

        self._tile_area = MapTileArea(self, self._spec.get_range_sight())
        self._health = self._spec.get_health()
        self._selected = False
        self._is_dead = False

        self._angle = 0
        self._angle_deg = 0
        self._angle_dist = DistAngle()

        self._look_angle = 0
        self._look_angle_deg = 0
        self._look_trg_angle_deg = 0

    def get_side(self):
        return self._side

    def get_target_type(self):
        return 0

    def get_spec(self):
        return self._spec

    def get_selected_type(self):
        if self._upgrade is not None or self._builder is not None or not self.get_is_alive():
            return ISelectable.SELECTED_NONE
        else:
            return ISelectable.SELECTED_MOVEABLE if self._born == SpecialFlags.BORN_READY else ISelectable.SELECTED_NONE

    def get_targetable(self):
        return self

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_eff_x(self):
        return self._x / DistAngle.ZOOM_VALUE

    def get_eff_y(self):
        return self._y / DistAngle.ZOOM_VALUE

    def get_use_x(self):
        return self._x // DistAngle.ZOOM_VALUE + self._dec

    def get_use_y(self):
        return self._y // DistAngle.ZOOM_VALUE - self._dec

    def get_angle(self):
        return self._angle

    def get_angle_deg(self):
        return self._angle_deg

    def get_angle_dist(self):
        return self._angle_dist
    
    def get_sprite_x(self):
        return (self._x / DistAngle.ZOOM_VALUE + self._dec) * self._moveables.get_zoom() + self._moveables.get_x()

    def get_sprite_y(self):
        return (self._y / DistAngle.ZOOM_VALUE - self._dec) * self._moveables.get_zoom() + self._moveables.get_y()

    def get_sprite_size(self):
        return 25.0 * self._spec.get_size() * self._moveables.get_zoom()

    def get_born(self):
        return self._born

    def get_path(self):
        return self._path

    def get_target_entity(self):
        return self._target

    def get_miner(self):
        return self._miner

    def get_upgrade(self):
        return self._upgrade

    def get_builder(self):
        return self._builder

    def get_tile(self):
        return self._path.get_place().get_tile()

    def get_place(self):
        return self._path.get_place()

    def get_places(self):
        return self._path.get_places()

    def get_places_around(self):
        return self._path.get_places_around()

    def get_is_on_place(self, place):
        return self._path.get_is_on_place(place)

    def get_focus_place(self):
        return self._path.get_focus_place()

    def get_focus_places(self):
        return self._path.get_focus_places()

    def get_nearest_place(self, from_x, from_y):
        return self._path.get_place()

    def get_path_list(self):
        return self._path.get_path_list()

    def get_path_list_len(self):
        return self._path.get_path_list_len()

    def get_target(self):
        return self._target.get_target()

    def get_tile_area(self):
        return self._tile_area

    def get_goto_group(self):
        return self._goto_group

    def get_goto_key(self):
        return self._goto_key

    def get_is_working(self):
        if self._miner is not None:
            return self._miner.get_is_working()
        elif self._upgrade is not None:
            return self._upgrade.get_is_working()
        elif self._builder is not None:
            return self._builder.get_is_working()

        return False

    def get_is_targeting(self):
        return self._target.get_is_targeting()

    def get_is_wait(self, find_mref=None):
        return self._path.get_is_wait(find_mref)

    def get_wait_mref(self):
        return self._path.get_wait_mref()

    def unset_path_timeout(self):
        self._path.unset_timeouts()

    def dist_to_moveable(self, ref):
        return int_sqrt((self.get_use_x() - ref.get_use_x()) ** 2 + (self.get_use_y() - ref.get_use_y()) ** 2)

    def dist_to_tile(self, tile):
        return int_sqrt((self.get_use_x() - tile.get_x()) ** 2 + (self.get_use_y() - tile.get_y()) ** 2)

    def get_selected(self):
        return self._selected

    def get_is_look_focus(self):
        if self._spec.get_look_when_stand() and self._path.get_current_speed() > 0:
            return False
        return self._look_focus

    def get_look_angle(self):
        return self._angle if self._spec.get_is_troop() else self._look_angle

    def get_look_angle_deg(self):
        return self._angle_deg if self._spec.get_is_troop() else self._look_angle_deg

    def get_look_trg_angle_deg(self):
        return self._look_trg_angle_deg

    def get_look_snap(self):
        return self._look_snap

    def get_current_speed(self):
        return self._path.get_current_speed()

    def get_max_speed(self):
        return self._path.get_max_speed()

    def get_is_moving(self):
        return self._path.get_is_moving()

    def get_is_moving_atm(self):
        return self._path.get_is_moving_atm()

    def get_is_stuck(self):
        return self._path.get_is_stuck() or (self._miner is not None and self._miner.get_is_stuck())

    def get_is_attacking(self):
        return self._target.get_target() is not None and self._target.get_target_focus() is not None

    def get_is_alive(self):
        return self._health > 0 and self._born == SpecialFlags.BORN_READY

    def get_is_dead(self):
        return self._is_dead

    def get_is_heavy_damaged(self):
        return self._health / self._spec.get_health() <= 0.4

    def get_is_human(self):
        return self._spec.get_is_troop()

    def get_health_koef(self):
        return self._health / self._spec.get_health()

    def get_look_coords_angle_inc(self, value, rand=0):
        # TODO udelat diskretne
        return (
            (self._x / DistAngle.ZOOM_VALUE + self._dec) + math.sin(self._angle if self._spec.get_is_troop() else self._look_angle) * value + random.random() * rand - rand / 2,
            (self._y / DistAngle.ZOOM_VALUE - self._dec) + math.cos(self._angle if self._spec.get_is_troop() else self._look_angle) * value + random.random() * rand - rand / 2
        )

    def get_coords(self, rand=0):
        return (
            (self._x / DistAngle.ZOOM_VALUE + self._dec) + random.random() * rand - rand / 2,
            (self._y / DistAngle.ZOOM_VALUE - self._dec) + random.random() * rand - rand / 2
        )

    def get_use_coords(self):
        return (
            self._x // DistAngle.ZOOM_VALUE + self._dec,
            self._y // DistAngle.ZOOM_VALUE - self._dec
        )

    def get_coords_on_place(self):
        return (
            int(self.get_place().get_x()) + int((self._spec.get_size() - 1) * 12.5),
            int(self.get_place().get_y()) - int((self._spec.get_size() - 1) * 12.5)
        )

    def get_two_points(self, size, inc):

        ang = angle_conv_deg(self._angle_deg)
        x1 = (self._x / DistAngle.ZOOM_VALUE + self._dec) + math.sin(ang) * inc - math.cos(ang) * size
        y1 = (self._y / DistAngle.ZOOM_VALUE - self._dec) + math.cos(ang) * inc + math.sin(ang) * size

        x2 = (self._x / DistAngle.ZOOM_VALUE + self._dec) + math.sin(ang) * inc - math.cos(ang) * (- size)
        y2 = (self._y / DistAngle.ZOOM_VALUE - self._dec) + math.cos(ang) * inc + math.sin(ang) * (- size)

        return x1, y1, x2, y2

    def get_target_place(self, from_x, from_y):
        return min(self._path.get_places(), key=lambda tl: tl.dist_to_coord(from_x, from_y))

    def get_target_coords(self, from_x, from_y):
        return self.get_use_x(), self.get_use_y()

    def get_target_colide_coords(self, ref):
        return self.get_coords(self._spec.get_collide_coords_range())

    def get_target_colide_area(self):
        return self._spec.get_collide_area_range()

    def get_target_priority(self):
        return self._spec.get_target_priority()

    def get_target_colide_check(self, cx, cy):
        cdef long diff_x = (self._x // DistAngle.ZOOM_VALUE + self._dec) - cx
        cdef long diff_y = (self._y // DistAngle.ZOOM_VALUE - self._dec) - cy

        return int_sqrt(diff_x * diff_x + diff_y * diff_y) <= self._spec.get_mass()

    def get_is_attack_focus(self):
        return self._target.get_target_focus() is not None

    def get_target_focus(self):
        return self._target.get_target_focus()

    def get_attack_place(self):
        return self._attack_place

    def get_trg_place(self):
        return self._path.get_trg_place()

    def get_may_move(self):
        return not self._spec.get_is_troop() or not self._target.get_shots_lock()

    def set_selected(self, selected):
        self._selected = selected

    def set_born(self, born):
        self._born = born

        if self._born == SpecialFlags.BORN_SPAWNING and self._action_item is not None:
            self._action_item.callback("finish", {"ref": self})

    def set_look_snap(self, look_snap):
        self._look_snap = look_snap

    def set_force_turret(self, value):
        self._force_turret = value

    def set_force_canon(self, value):
        self._force_canon = value

    def set_action_item(self, action_item):
        self._action_item = action_item

    def set_goto_group(self, goto_group):
        if goto_group is not self._goto_group:
            self._goto_group.rem(self)
            self._goto_group = goto_group
            self._goto_group.add(self)

    def set_goto_key(self, key):
        self._goto_key = key

    def set_max_speed(self, max_speed, wait=False):
        self._path.set_max_speed(max_speed, wait)

    def set_callback_dead(self, callback):
        self._callback_dead = callback

    def set_callback_damage(self, callback):
        self._callback_damage = callback

    def set_callback_kill(self, callback):
        self._callback_kill = callback

    def get_state_map(self):
        sm = {}
        sm.update(self._path.get_state_map())

        return sm

    def stop(self, reflow=False):
        if self._target.set_is_hold(False):
            self._sprite_up = 1

        self._path.stop(reflow=reflow)

    def hold(self):
        if self._target.set_is_hold(True):
            self._sprite_up = 1

        self._path.stop()

    def set_angle_deg(self, value):
        self._angle_deg = value
        self._angle = angle_conv_deg(value)

    def move(self, value):
        cdef angle_coords c
        c = get_diff_from_angle(self._angle_deg, value)

        self._x += c.ox
        self._y += c.oy
        self._tile_area.set_coords(self._x // DistAngle.ZOOM_VALUE, self._y // DistAngle.ZOOM_VALUE)

    def move_set(self, x, y):
        self._x = x
        self._y = y
        self._tile_area.set_coords(self._x // DistAngle.ZOOM_VALUE, self._y // DistAngle.ZOOM_VALUE)

    def rotate_step_to(self, trg_angle_deg):
        ad = angle_dist_side_deg(self._angle_deg, trg_angle_deg)

        if ad.left < ad.right:
            self._angle_deg = angle_inc_border_deg(self._angle_deg, - self._spec.get_turn_speed(), trg_angle_deg, self._spec.get_turn_speed())
            self._angle = angle_conv_deg(self._angle_deg)
            return ad.left
        else:
            self._angle_deg = angle_inc_border_deg(self._angle_deg, self._spec.get_turn_speed(), trg_angle_deg, self._spec.get_turn_speed())
            self._angle = angle_conv_deg(self._angle_deg)
            return ad.right

    def rotate_step_set(self, trg_angle_deg):
        self._angle_deg = trg_angle_deg
        self._angle = angle_conv_deg(trg_angle_deg)

    def rotate_to_diff(self, trg_angle_deg):
        ad = angle_dist_side_deg(self._angle_deg, trg_angle_deg)
        return min(ad.left, ad.right)

    def set_look_to_place(self, place):
        self._look_trg_angle_deg = get_angle_from(place.get_pos_x() - self.get_place().get_pos_x(), place.get_pos_y() - self.get_place().get_pos_y())
        self._look_focus = abs(self._look_angle_deg - self._look_trg_angle_deg) < self._spec.get_look_speed()

    def set_look_to_coords(self, cx, cy):
        self._look_trg_angle_deg = get_angle_from(cx - self.get_use_x(), cy - self.get_use_y())
        self._look_focus = abs(self._look_angle_deg - self._look_trg_angle_deg) < self._spec.get_look_speed()

    def set_look_to_target(self, ref):
        c = ref.get_target_coords(self.get_use_x(), self.get_use_y())
        self._look_trg_angle_deg = get_angle_from(c[0] - self.get_use_x(), c[1] - self.get_use_y())
        self._look_focus = abs(self._look_angle_deg - self._look_trg_angle_deg) < self._spec.get_look_speed()

    def idle_reset(self):
        self._angle = angle_conv_deg(self._angle_deg)
        self._idle_value = 0
        self._idle_angle = self._idle_angle
        self._idle_dir = - 1 if (random.random() - 0.5) < 0 else 1

    def idle_work(self):
        if self._spec.get_is_troop() and not self._target.get_shots_lock():

            if self._idle_value == 0:
                self._idle_angle = self._angle

            if self.get_target() is None:
                self._angle = self._idle_angle + math.sin(self._idle_value / 20) * 0.5
                self._idle_value += random.random() * self._idle_dir

            else:
                self._angle = self._idle_angle + math.sin(self._idle_value / 10) * 0.2
                self._idle_value += random.random() * self._idle_dir

    def may_dmg_hit(self, timeout, shot_spec):
        if shot_spec.get_key() in ["mini_vehicle", "mini_jeep", "mini_troop"]:

            if not self._spec.get_is_troop() and self._dmg_hit_timeout == 0:
                self._dmg_hit_timeout = timeout
                return True
            else:
                return False

        else:
            return True

    def deal_damage(self, dx, dy, owner, shot_spec, angle, koef=1000):

        #if self._spec.get_side_type() == "s1":
            #return False

        dmg = (shot_spec.get_damage() * koef) // 1000

        if shot_spec in self._spec.get_armor():
            dmg = (dmg * self._spec.get_armor()[shot_spec]) // 1000

        # dmg = 0 # !!!

        self._health -= dmg

        #print(dmg, self._health)

        if dmg > 80 and not self._spec.get_is_troop():
            pt = self._particles.add_particle(dx, dy, 'destroy', -1, scale=0.7 - random.random() * 0.1)
            pt.set_force(angle + math.pi + (math.pi / 2) * (0.5 - random.random()), 3 + random.random() * 2, 0.4)
            pt.set_force_rotate(math.pi / 4 + random.random(), 0.2)
            pt.set_fadeout(40, 10)

        if self._spec.get_is_troop():
            c = self.get_target_colide_coords(owner.get_targetable())
            self._particles.add_particle(c[0], c[1], "blood_hit1", -1, scale=1.1 - random.random() * 0.3)
            
        if self._health <= 0:
            self._is_dead = True

            if self._callback_dead is not None:
                self._callback_dead(self, owner.get_targetable())

            if owner.get_targetable()._callback_kill is not None:
                owner.get_targetable()._callback_kill(owner.get_targetable(), self)

            self._side.event_destroy_unit(self)

            self._path.stop()
            self._path.unset_place(clear_refs=False)
            self._side.rem_from_selected(self)

            eff_x = self.get_eff_x()
            eff_y = self.get_eff_y()

            if self._spec.get_is_troop():
                self._particles.add_particle(eff_x + self._dec, eff_y - self._dec, "die1", -1, scale=1.1 - random.random() * 0.3)

            else:
                self._sounds.play_sound("xpl1", eff_x + self._dec, eff_y - self._dec, 0.9 + random.random() * 0.2)
                self._particles.add_particle(eff_x + self._dec, eff_y - self._dec, "exp1", -1, scale=1.1 - random.random() * 0.2)
                self._particles.add_light(eff_x + self._dec, eff_y - self._dec, opacity=70, tick_value=(40, 20, 35), color=(255, 255, 122), size=40 + random.random() * 6 - 3)

            h_koef = min((800 / self._spec.get_health()), 1.0)
            force = min(dmg, 150)

            ba = self._batches

            to_add = []

            img_dmg = self._spec.get_tex_pad_dead()

            if img_dmg is None:
                img_dmg = self._spec.get_tex_pad_dmg(1)

            if self._spec.get_is_troop():
                # to_add.append({'img': img_dmg, 'angle': self._angle, 'group': ba.get_group_below()}) # ne
                pass

            else:
                to_add.append({'img': img_dmg, 'angle': self._angle, 'force': 1, 'force_rotate': 1, 'group': ba.get_group_below()})

            if self._spec.get_tex_canon() is not None:
                to_add.append({'img': self._spec.get_tex_canon(1), 'angle': self._look_angle, 'force': 1 + random.random() * 0.1, 'force_rotate': 1 + random.random() * 0.5, 'group': ba.get_group_below_up()})

            if self._spec.get_tex_turret() is not None:
                to_add.append({'img': self._spec.get_tex_turret(1), 'angle': self._look_angle, 'force': 1 + random.random() * 0.2, 'force_rotate': 1 + random.random() * 0.8, 'group': ba.get_group_below_up()})

            for c in to_add:
                pt = self._particles.add_particle(eff_x + self._dec, eff_y - self._dec, c['img'], c['angle'], group=c['group'])
                pt.set_fadeout(250, 50)

                if 'force' in c:
                    pt.set_force(angle, 6 * (force / 150) * h_koef * c['force'], 0.1)

                    ad = angle_dist_side(c['angle'], angle)

                    if ad.left < ad.right:
                        pt.set_force_rotate(-0.6 * (force / 150) * h_koef * c['force_rotate'], -0.015)
                    else:
                        pt.set_force_rotate(0.6 * (force / 150) * h_koef * c['force_rotate'], 0.015)

            return True

        elif self.get_is_heavy_damaged():
            self._sprite_up_damage = 1
            self._sprite_up = 1

        self._target.add_damaged_by(owner.get_targetable())

        if self._callback_damage is not None:
            self._callback_damage(self, owner.get_targetable())

        return False

    def init(self, place):
        self._path.set_place(place)
        self.move_set(self._path.get_place().get_x() * DistAngle.ZOOM_VALUE, self._path.get_place().get_y() * DistAngle.ZOOM_VALUE)
        self._sprite_init = True
        self.may_draw = 1
        self._batches.add_drawable(self)

        if self._miner is not None:
            self._miner.init()

    def update_body(self, mined=0, upgraded=0, walk_next=0, shot=0):
        self._sprite_up_mined = mined
        self._sprite_up_upgraded = upgraded

        if self._spec.get_is_troop():
            self._sprite_up_walk_next = walk_next
            self._sprite_up_shot = shot

            if walk_next == 1:
                self._walk_frame = 0
            elif walk_next == 2:
                self._walk_frame += 1

        self._sprite_up = 1

    def draw_destroy(self):
        pass

    def destroy_full(self):
        self._path.stop()
        self._path.unset_place()
        self._side.rem_from_selected(self)
        self._is_dead = True

    def destroy(self):
        self.may_draw = 2
        self.path_update = False
        self._tile_area.destroy()
        self._goto_group.rem(self)
        self._side.rem_unit(self)
        self._goto_group = None

        if self._miner is not None:
            self._miner.destroy()
        elif self._upgrade is not None:
            self._upgrade.destroy()
        elif self._builder is not None:
            self._builder.destroy()

        self._batches.add_sprite_delete([self._sprite_body, self._sprite_turret, self._sprite_canon, self._sprite_select])
        GraphBatches().kill_drawable(self)

    def serialize(self):
        out = {
            'number': self.number,
            'side_number': self._side.number,
            'spec': self._spec.get_key(),
            'place_x': -1 if self.get_place() is None else self.get_place().get_pos_x(),
            'place_y': -1 if self.get_place() is None else self.get_place().get_pos_y(),
            'ang_x': self._angle_dist.get_diff_x(),
            'ang_y': self._angle_dist.get_diff_y(),
            'look_angle_deg': self._look_angle_deg,
            'path': self._path.serialize(),
            'target': self._target.serialize(),
            'ai_group': '-' if self.group_ai is None else self.group_ai.get_caption(),
            'ai_group_incl': False
        }

        if self.group_ai is not None:
            out['ai_group_incl'] = self in self.group_ai.get_childs()

        return out

    def update(self, tick):

        if self.get_is_dead():
            return 0

        if self._dmg_hit_timeout > 0:
            self._dmg_hit_timeout -= 1

        path_flag = self._path.update(tick)

        if self.get_is_alive():
            self._target.update(tick)

        if self._miner is not None:
            self._miner.update(tick)

        elif self._upgrade is not None:
            self._upgrade.update(tick)

        elif self._builder is not None:
            self._builder.update(tick)

        if (self._look_angle_deg - self._look_trg_angle_deg) != 0:
            btw = angle_dist_side_deg(self._look_trg_angle_deg, self._look_angle_deg)

            if min(btw.left, btw.right) <= self._spec.get_look_speed():
                self._look_angle_deg = self._look_trg_angle_deg
                self._look_focus = True

            if btw.left < btw.right:
                self._look_angle_deg = angle_inc_border_deg(self._look_angle_deg, self._spec.get_look_speed(), self._look_trg_angle_deg, self._spec.get_look_speed())
                self._look_focus = False

            else:
                self._look_angle_deg = angle_inc_border_deg(self._look_angle_deg, -self._spec.get_look_speed(), self._look_trg_angle_deg, self._spec.get_look_speed())
                self._look_focus = False

            self._look_angle = angle_conv_deg(self._look_angle_deg)

        if self._force_canon != 0:
            self._force_canon += 0.25
            if self._force_canon >= 0:
                self._force_canon = 0

        if self._force_turret != 0:
            self._force_turret += 0.1
            if self._force_turret >= 0:
                self._force_turret = 0

        if self.get_is_heavy_damaged() and not self._spec.get_is_troop():
            if self._dmg_smoke_tick == 0:
                c = self.get_coords(15)
                self._particles.add_particle(c[0], c[1], "smoke", -1, scale=1.1 - random.random() * 0.5)

                self._dmg_smoke_tick = 40 + random.randint(0, 50)

            else:
                self._dmg_smoke_tick -= 1

            if self._dmg_fire_tick == 0:
                flg = "fire1" if random.randint(0, 1) == 0 else "fire2"
                c = self.get_coords(25)
                self._particles.add_particle(c[0], c[1], flg, -1, scale=1.1 - random.random() * 0.4)

                self._dmg_fire_tick = 20 + random.randint(0, 10)

            else:
                self._dmg_fire_tick -= 1

        return 2 if path_flag else 1

    def unset_actions(self):
        self._target.set_target_focus(None)
        self._attack_place = None

    def goto_pre(self, place_to):
        self._path.goto_pre(place_to)
        self._path.update_goto_group()

    def goto(self, place_to, create_goto_group=True, strict=False, path_flow=None):
        if create_goto_group:
            self.set_goto_group(GotoGroup.create())

        if not self._spec.get_free_look():
            self._target.release_target()
        else:
            self._target.release_target_focus()

        self._attack_place = None
        self._target.set_target_back_place(None)
        self._target.set_target_type(MoveableTarget.TARGET_DEFAULT)
        self._target.release_lock()

        if self._target.set_is_hold(False):
            self._sprite_up = 1

        return self._path.goto(place_to, strict=strict, path_flow=path_flow)

    def goto_nearest(self, exc=None, cross_path=None, force_strict=False):
        return self._path.goto_nearest(exc, cross_path, force_strict)

    def goto_enforce_places(self, path_list, goto_place_after=None, force_step=False):
        self._path.goto_enforce_places(path_list, goto_place_after=goto_place_after, force_step=force_step)

    def goto_attack(self, mref, target_type=MoveableTarget.TARGET_DEFAULT, keep_attack_place=False, create_goto_group=True, path_flow=None, strict=False):
        if self._spec.get_attack_able() and mref.get_is_alive():
            if not keep_attack_place:
                self._attack_place = None

            if create_goto_group:
                self.set_goto_group(GotoGroup.create())

            if self._target.set_is_hold(False):
                self._sprite_up = 1

            if mref is not self._target.get_target():
                self._target.release_target()
                self._target.release_lock()
                self._target.set_target_focus(mref, target_type)
                self._target.scan_targets()

            else:
                self._target.release_shots_lock()
                self._target.set_target_focus(mref, target_type)

            self._path.goto(mref.get_place(), path_flow=path_flow, strict=strict, check_target=True)

            """
            if not self._target.scan_targets():
                place = self._map_scr.scan_avail_place(self, mref.get_nearest_place(self.get_use_x(), self.get_use_y()))

                if place is not None:
                    self._path.goto(place, path_flow=path_flow)
            """

    def attack_move(self, place_to, create_goto_group=True, path_flow=None):
        if self._spec.get_attack_able():

            if create_goto_group:
                self.set_goto_group(GotoGroup.create())

            if self._target.set_is_hold(False):
                self._sprite_up = 1

            self._attack_place = place_to
            self._target.scan_targets()

            if self._target.get_target() is None:
                self._target.scan_attack_targets()

            if self._target.get_target() is None:
                self._path.goto(place_to, path_flow=path_flow, check_target=True)
                self._target.set_target_focus(None)
            else:
                self.stop()

    def draw(self):
        if self.may_draw != 1:
            return False

        visible_flag = self._path.get_is_visible_place()

        if visible_flag != self._visible:
            self._visible = visible_flag

            if self._visible:
                self._sprite_init = True
            else:
                self._batches.add_sprite_delete([self._sprite_body, self._sprite_turret, self._sprite_canon, self._sprite_select])

                self._sprite_body = None
                self._sprite_turret = None
                self._sprite_canon = None
                self._sprite_select = None

        if self._visible:
            if self._sprite_init:
                self._sprite_init = False
                self._sprite_body = self._batches.create_sprite(img=self._spec.get_tex_pad(self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_first(), batch=self._batches.get_batch())
                self._sprite_body.visible = self._visible

                if self._spec.get_tex_turret() is not None:
                    self._sprite_turret = self._batches.create_sprite(img=self._spec.get_tex_turret(0, self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_third(), batch=self._batches.get_batch())
                    self._sprite_turret.visible = self._visible

                if self._spec.get_tex_canon() is not None:
                    self._sprite_canon = self._batches.create_sprite(img=self._spec.get_tex_canon(0, self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_second(), batch=self._batches.get_batch())
                    self._sprite_canon.visible = self._visible

                self._sprite_select = self._batches.create_sprite(img=self._spec.get_select_tex(self._target.get_is_hold()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_particle(), batch=self._batches.get_batch())
                self._sprite_select.visible = self._visible and self._selected

                if self._allow_lines:
                    self._line = pyglet.shapes.Line(self.get_sprite_x(), self.get_sprite_y(), self.get_sprite_x(), self.get_sprite_y(), width=1, color=(255, 0, 0), group=self._batches.get_group_gui(), batch=self._batches.get_batch())
                    self._line_to = pyglet.shapes.Line(self.get_sprite_x(), self.get_sprite_y(), self.get_sprite_x(), self.get_sprite_y(), width=1, color=(0, 0, 255), group=self._batches.get_group_gui(), batch=self._batches.get_batch())

            if self._sprite_up:
                self._sprite_up = 0

                if self._sprite_up_mined == 2:
                    self._sprite_body.image = self._spec.get_tex_pad_full(self._side.get_color())
                    self._sprite_up_mined = 0

                elif self._sprite_up_mined == 1:
                    self._sprite_body.image = self._spec.get_tex_pad(self._side.get_color())
                    self._sprite_up_mined = 0

                if self._sprite_up_upgraded == 2:
                    self._sprite_body.image = self._spec.get_tex_pad_full(self._side.get_color())
                    self._sprite_up_upgraded = 0

                elif self._sprite_up_upgraded == 1:
                    self._sprite_body.image = self._spec.get_tex_pad(self._side.get_color())
                    self._sprite_up_upgraded = 0

                if self._sprite_up_walk_next == 1:
                    self._sprite_body.image = self._spec.get_tex_pad(self._side.get_color())
                    self._sprite_up_walk_next = 0

                elif self._sprite_up_walk_next == 2:
                    self._sprite_body.image = self._spec.get_tex_troop_walk(self._walk_frame % 4, self._side.get_color())
                    self._sprite_up_walk_next = 0

                if self._sprite_up_shot == 1:
                    self._sprite_body.image = self._spec.get_tex_pad(self._side.get_color())
                    self._sprite_up_shot = 0

                elif self._sprite_up_shot == 2:
                    self._sprite_body.image = self._spec.get_tex_troop_shot(self._side.get_color())
                    self._sprite_up_shot = 0

                if self._sprite_up_damage == 1:
                    if self._spec.get_tex_pad_dmg(0, self._side.get_color()) is not None:
                        self._sprite_body.image = self._spec.get_tex_pad_dmg(0, self._side.get_color())
                    self._sprite_up_damage = 0

                self._sprite_select.image = self._spec.get_select_tex(self._target.get_is_hold())

            """
            visible_flag = self._path.get_is_visible_place()
    
            if visible_flag != self._visible:
                self._visible = visible_flag
                self._sprite_select.visible = self._visible and self._selected
    
                if self._sprite_canon is not None:
                    self._sprite_canon.visible = self._visible
    
                if self._sprite_turret is not None:
                    self._sprite_turret.visible = self._visible
    
                self._sprite_body.visible = self._visible
                self._sprite_select.visible = False
            """

            self._sprite_body.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._moveables.get_zoom(), rotation=angle_conv_realdeg(self._angle))

            if self._sprite_canon is not None:
                self._sprite_canon.update(x=self.get_sprite_x() + math.sin(self._look_angle) * self._force_canon * self._moveables.get_zoom(),
                                          y=self.get_sprite_y() + math.cos(self._look_angle) * self._force_canon * self._moveables.get_zoom(),
                                          scale=self._moveables.get_zoom(),
                                          rotation=angle_conv_realdeg(self._look_angle))

            if self._sprite_turret is not None:
                self._sprite_turret.update(x=self.get_sprite_x() + math.sin(self._look_angle) * self._force_turret * self._moveables.get_zoom(),
                                           y=self.get_sprite_y() + math.cos(self._look_angle) * self._force_turret * self._moveables.get_zoom(),
                                           scale=self._moveables.get_zoom(),
                                           rotation=angle_conv_realdeg(self._look_angle))

            if self._selected:
                self._sprite_select.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._moveables.get_zoom())

            if self._sprite_select.visible != self._selected:
                self._sprite_select.visible = self._selected

            if self._allow_lines:
                self._line.x = self.get_sprite_x() - self._dec
                self._line.y = self.get_sprite_y() + self._dec
                self._line.opacity = 100 if self._path.get_is_moving() else 0

                if self._path.get_focus_place() is not None:
                    self._line.x2 = self._path.get_focus_place().get_x() * self._moveables.get_zoom() + self._moveables.get_x()
                    self._line.y2 = self._path.get_focus_place().get_y() * self._moveables.get_zoom() + self._moveables.get_y()

                self._line_to.x = self.get_sprite_x() - self._dec
                self._line_to.y = self.get_sprite_y() + self._dec
                self._line_to.opacity = 100 if self._path.get_is_moving() else 0

                if self._path.get_current_trg_place() is not None:
                    self._line_to.x2 = self._path.get_current_trg_place().get_x() * self._moveables.get_zoom() + self._moveables.get_x()
                    self._line_to.y2 = self._path.get_current_trg_place().get_y() * self._moveables.get_zoom() + self._moveables.get_y()

            """
            if self._note is None:
                self._note = primitives.get().add_text(self.get_use_x(), self.get_use_y(), "")

            self._note.set_coords(self.get_use_x(), self.get_use_y())

            pt = str(self._goto_key)
            if self._path.get_flow_entity() is not None:
                pt += "|" + str(self._path.get_flow_entity().get_stacked_timeout())

            self._note.set_data({"text": pt})
            """

            if self._path.get_is_track_draw():
                rect = self._batches.get_grounds().create(self._spec.get_tex_track())
                rect.set_coords_list(self._path.get_track_draw_pts())
                rect.set_opacity_kf(self._spec.get_track_opacity())
                rect.set_decay(100, self._spec.get_track_opacity())
