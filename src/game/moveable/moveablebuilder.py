import math
import random

from game.const.specialflags import SpecialFlags
from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.distangle import DistAngle
from game.interface.itilecollector import ITileCollector
from game.map.mapscanner import MapScanner
from game.map.pathfinder import PathFinder
from game.moveable.unitspec import UnitSpec
from game.particle.particlecollector import ParticleCollector
from game.sound.soundcollector import SoundCollector


class MoveableBuilder:

    TASK_NONE = 0
    TASK_FIND = 1
    TASK_MOVE = 2
    TASK_BUILD = 3
    TASK_RETURN = 4

    _map_scr: MapScanner = None

    _parent = None
    _spec: UnitSpec = None
    _build_ref = None
    _build_trg_ref = None
    _state_tiles = None
    _timeout = 0
    _task = None
    _angle_dist = None

    def __init__(self, parent, map_scr: MapScanner):
        self._map_scr = map_scr

        self._parent = parent
        self._spec = parent.get_spec()
        self._build_ref = None
        self._build_trg_ref = None
        self._angle_dist = DistAngle()
        self._task = MoveableBuilder.TASK_NONE

    def get_is_working(self):
        return self._task in [MoveableBuilder.TASK_BUILD]

    def get_is_stuck(self):
        return False

    def set_build_refs(self, build_ref, build_trg_ref):
        self._build_ref = build_ref
        self._build_trg_ref = build_trg_ref
        self._task = MoveableBuilder.TASK_FIND

    def destroy(self):
        pass

    def update(self, tick):

        if tick % 30 == 0:
            if self._task == MoveableBuilder.TASK_FIND and not self._parent.get_is_moving() and self._build_trg_ref is not None and self._build_trg_ref.get_is_alive():
                self._state_tiles = self._build_trg_ref.get_state_tiles().copy()
                self._task = MoveableBuilder.TASK_MOVE

            elif self._task == MoveableBuilder.TASK_MOVE and not (self._parent.get_is_moving() or self._parent.get_is_stuck()) and self._build_trg_ref is not None and self._build_trg_ref.get_is_alive():

                if len(self._state_tiles) == 0:
                    self._task = MoveableBuilder.TASK_RETURN

                elif self._parent.get_place() is self._state_tiles[0].get_place():
                    self._task = MoveableBuilder.TASK_BUILD
                    self._timeout = 30
                    self._parent.set_look_to_coords(self._parent.get_use_x(), self._parent.get_use_y() - 20)
                    SoundCollector().play_sound("const", self._state_tiles[0].get_x(), self._state_tiles[0].get_y(), 1.1 + random.random() * 0.2)

                else:
                    self._parent.goto(self._state_tiles[0].get_place(), strict=True)

            elif self._task == MoveableBuilder.TASK_RETURN and not self._parent.get_is_moving() and self._build_ref is not None and self._build_ref.get_is_alive():
                pls = self._build_ref.get_places_special(SpecialFlags.SPECIAL_CREATE_FINAL)

                if pls is not None and pls[0][0] is self._parent.get_place():

                    places_create = [c[0] for c in self._build_ref.get_places_special(SpecialFlags.SPECIAL_CREATE)]
                    places_move = [c[0] for c in self._build_ref.get_places_special(SpecialFlags.SPECIAL_CREATE_MOVE)]

                    path_fnd = PathFinder(ITileCollector.get())
                    place_list = path_fnd.path_places_follow(places_move + places_create)

                    self._parent.set_born(SpecialFlags.BORN_VAPORIZE)
                    self._parent.goto_enforce_places(place_list, force_step=True)

                elif pls is not None:
                    self._parent.goto(pls[0][0], strict=True)

            elif self._task == MoveableBuilder.TASK_RETURN and self._build_trg_ref is not None and self._build_trg_ref.get_is_alive() and not self._build_trg_ref.get_is_ready():
                any_in = False

                for place in self._build_trg_ref.get_places():
                    if place in self._parent.get_places():
                        any_in = True
                        break

                if not any_in:
                    self._build_trg_ref.set_ready()

        if self._task == MoveableBuilder.TASK_BUILD:
            diff = self._parent.rotate_step_to(self._angle_dist.get_angle_deg())
            self._parent.get_angle_dist().set_angle_dist(self._angle_dist)

            pt = ParticleCollector().add_particle(self._state_tiles[0].get_x() + random.random() * 20 - 10, self._state_tiles[0].get_y() + random.random() * 20 - 10, 'spark4', 0, scale=0.4)
            pt.set_random_angle()
            pt.set_force(math.pi * random.random() * 2.0, 4 + random.random() * 4, 1)
            pt.set_force_rotate(math.pi / 10, 0)
            pt.set_fadeout(15, 3)

            self._timeout = max(self._timeout - 1, 0)

            if self._timeout % 5 == 0:
                ParticleCollector().add_light(self._state_tiles[0].get_x() + random.random() * 25 - 12.5, self._state_tiles[0].get_y() + random.random() * 25 - 12.5, opacity=30, tick_value=(15, 4, 11), color=(255, 255, 255), size=20)

            #ParticleCollector().add_particle(self._state_tiles[0].get_x(), self._state_tiles[0].get_y(), "smoke2", -1, random.randint(0, 8), scale=1.1 - random.random() * 0.5)

            if diff == 0 and self._timeout == 0:
                #ParticleCollector().add_light(self._state_tiles[0].get_x(), self._state_tiles[0].get_y(), opacity=50, tick_value=(20, 15, 18), color=(255, 255, 0), size=40)
                self._state_tiles[0].set_build()
                del self._state_tiles[0]
                self._task = MoveableBuilder.TASK_MOVE
