from game.const.specialflags import SpecialFlags
from game.helper.distangle import DistAngle
from game.interface.itilecollector import ITileCollector
from game.map.mapscanner import MapScanner
from game.map.pathfinder import PathFinder
from game.moveable.unitspec import UnitSpec


class MoveableUpgrade:

    TASK_NONE = 0
    TASK_MOVE = 1
    TASK_TURN = 3
    TASK_UPGRADE = 4
    TASK_RETURN = 5

    _map_scr: MapScanner = None

    _parent = None
    _spec: UnitSpec = None
    _build_ref = None
    _build_trg_ref = None
    _timeout = 0
    _task = None
    _angle_dist = None

    def __init__(self, parent, map_scr: MapScanner):
        self._map_scr = map_scr

        self._parent = parent
        self._spec = parent.get_spec()
        self._build_ref = None
        self._build_trg_ref = None
        self._task = MoveableUpgrade.TASK_NONE

    def get_is_working(self):
        return self._task in [MoveableUpgrade.TASK_TURN, MoveableUpgrade.TASK_TURN]

    def get_is_stuck(self):
        return False

    def set_build_refs(self, build_ref, build_trg_ref):
        self._build_ref = build_ref
        self._build_trg_ref = build_trg_ref
        self._build_trg_ref.set_upgrade_mref(self._parent)
        self._task = MoveableUpgrade.TASK_MOVE

    def destroy(self):
        if self._build_trg_ref is not None:
            self._build_trg_ref.unset_upgrade_mref(self._parent)

    def update(self, tick):

        if self._timeout > 0:
            self._timeout -= 1

        elif tick % 30 == 0:
            if self._task == MoveableUpgrade.TASK_UPGRADE and self._build_trg_ref.get_is_alive():
                self._parent.update_body(upgraded=2)
                self._build_trg_ref.unset_upgrade_mref(self._parent)
                self._build_trg_ref.add_upgrade()
                self._task = MoveableUpgrade.TASK_RETURN

            elif self._task == MoveableUpgrade.TASK_MOVE and not self._parent.get_is_moving() and self._build_trg_ref is not None and self._build_trg_ref.get_is_alive():
                pls = self._build_trg_ref.get_places_special(SpecialFlags.SPECIAL_UPGRADE)

                if pls is not None and pls[0][0] is self._parent.get_place():
                    self._task = MoveableUpgrade.TASK_TURN
                    self._angle_dist = pls[0][1]["angle_dist"]

                elif pls is not None:
                    self._parent.goto(pls[0][0], strict=True)

                else:
                    min_dist = -1
                    min_place = None

                    for tl in [c.get_tile() for c in self._build_trg_ref.get_places_around() if c.get_tile().is_accessible_simple()]:
                        dist = tl.dist_to_moveable(self._parent)

                        if min_dist == -1 or dist < min_dist:
                            min_dist = dist
                            min_place = tl.get_place()

                    if min_place is not None and min_place is self._parent.get_place():
                        self._task = MoveableUpgrade.TASK_TURN
                        self._angle_dist = DistAngle().set_places(self._build_trg_ref.get_place(), self._parent.get_place())

                    elif min_place is not None:
                        self._parent.goto(min_place, strict=True)

            elif self._task == MoveableUpgrade.TASK_RETURN and not self._parent.get_is_moving() and self._build_ref is not None and self._build_ref.get_is_alive():
                pls = self._build_ref.get_places_special(SpecialFlags.SPECIAL_CREATE_FINAL)

                if pls is not None and pls[0][0] is self._parent.get_place():

                    places_create = [c[0] for c in self._build_ref.get_places_special(SpecialFlags.SPECIAL_CREATE)]
                    places_move = [c[0] for c in self._build_ref.get_places_special(SpecialFlags.SPECIAL_CREATE_MOVE)]

                    path_fnd = PathFinder(ITileCollector.get())
                    place_list = path_fnd.path_places_follow(places_move + places_create)

                    self._parent.set_born(SpecialFlags.BORN_VAPORIZE)
                    self._parent.goto_enforce_places(place_list, force_step=True)

                elif pls is not None:
                    self._parent.goto(pls[0][0], strict=True)

            elif self._task == MoveableUpgrade.TASK_MOVE and not self._parent.get_is_moving():
                self._task = MoveableUpgrade.TASK_RETURN

        if self._task == MoveableUpgrade.TASK_TURN:
            diff = self._parent.rotate_step_to(self._angle_dist.get_angle_deg())
            self._parent.get_angle_dist().set_angle_dist(self._angle_dist)

            if diff == 0:
                self._task = MoveableUpgrade.TASK_UPGRADE
                self._timeout = 30
