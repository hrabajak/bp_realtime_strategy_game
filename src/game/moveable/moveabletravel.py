import math

from game.graphic.graphbatches import GraphBatches
from game.helper.simpleprofile import SimpleProfile
from game.interface.igui import IGui
from game.interface.imoveables import IMoveables
from game.interface.iselectable import ISelectable
from game.interface.itilecollector import ITileCollector
from game.map.mapscanner import MapScanner
from game.map.pathfinderflow import PathFinderFlow
from game.moveable.gotogroup import GotoGroup
from game.particle.particlecollector import ParticleCollector


class MoveableTravel:

    _moveables: IMoveables = None
    _map_scr: MapScanner = None

    _childs = None

    skip_when_targetting = False

    def __init__(self):
        self._moveables = IMoveables.get()
        self._map_scr = MapScanner(ITileCollector.get())

        self._childs = []

    def add(self, ref):
        if isinstance(ref, list):
            for s_ref in ref:
                if s_ref is not None and s_ref.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
                    self._childs.append(s_ref)

        elif ref is not None and ref.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
            self._childs.append(ref)

    def goto(self, place_to):
        if place_to is not None and len(self._childs) > 0:
            gref = GotoGroup.find_existing(self._childs)
            gref_new = False

            if gref is None:
                gref = GotoGroup.create()
                gref_new = True

                for mref in self._childs:
                    if mref.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
                        mref.set_goto_group(gref)

            for mref in self._childs:
                if not self.skip_when_targetting or not mref.get_is_targeting():
                    mref.goto_pre(place_to)

            flw = gref.get_path_flow()

            if gref_new or flw is None:
                flw = PathFinderFlow(ITileCollector.get(), gref)
                flw.set_moveables(self._childs)

            if flw.get_is_ready():
                flw.invalidate_finish()
                flw.prepare(place_to)

                gref.prepare(path_flow=flw, rel_object=place_to)
                flw.clear_targetables()
                flw.realize(PathFinderFlow.REALIZE_GOTO, place_to, True)

            else:
                gref.prepare(path_flow=flw, rel_object=place_to)
                flw.clear_targetables()
                flw.realize_light(PathFinderFlow.REALIZE_GOTO, place_to, True)

            for mref in self._childs:
                mref.goto(place_to, create_goto_group=False, path_flow=flw)

            if IGui.get().get_side() is self._childs[0].get_side():
                pt = ParticleCollector().add_particle(place_to.get_x(), place_to.get_y(), 'goeff', -1, scale=1, group=GraphBatches().get_group_gui(), batch=GraphBatches().get_batch_black())
                pt.set_force_rotate(math.pi / 40, 0)

    def goto_attack(self, attack_mref):
        if attack_mref is not None and attack_mref.get_is_alive() and len(self._childs) > 0:
            gref = GotoGroup.find_existing(self._childs)
            gref_new = False

            if gref is None:
                gref = GotoGroup.create()
                gref_new = True

                for mref in self._childs:
                    if mref.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
                        mref.set_goto_group(gref)

            for mref in self._childs:
                if not self.skip_when_targetting or not mref.get_is_targeting():
                    mref.goto_pre(attack_mref.get_place())

            flw = gref.get_path_flow()

            if gref_new or flw is None:
                flw = PathFinderFlow(ITileCollector.get(), gref)
                flw.set_moveables(self._childs)

            if flw.get_is_ready():
                flw.invalidate_finish()
                flw.prepare(None)
                flw.clear_targetables()
                flw.add_targetable(attack_mref)

                gref.prepare(path_flow=flw, rel_object=attack_mref)
                flw.realize(PathFinderFlow.REALIZE_ATTACK, attack_mref, True)

            else:
                gref.prepare(path_flow=flw, rel_object=attack_mref)
                flw.clear_targetables()
                flw.add_targetable(attack_mref)
                flw.realize_light(PathFinderFlow.REALIZE_ATTACK, attack_mref, True)

            for mref in self._childs:
                mref.goto_attack(attack_mref, create_goto_group=False, path_flow=flw)

            if IGui.get().get_side() is self._childs[0].get_side():
                pt = ParticleCollector().add_particle(attack_mref.get_use_x(), attack_mref.get_use_y(), 'ateff', -1, scale=1, group=GraphBatches().get_group_gui(), batch=GraphBatches().get_batch_black())
                pt.set_force_rotate(math.pi / 40, 0)

    def attack_move(self, place_to):
        if place_to is not None and len(self._childs) > 0:
            gref = GotoGroup.find_existing(self._childs)
            gref_new = False

            if gref is None:
                gref = GotoGroup.create()
                gref_new = True

                for mref in self._childs:
                    if mref.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
                        mref.set_goto_group(gref)

            for mref in self._childs:
                if not self.skip_when_targetting or not mref.get_is_targeting():
                    mref.goto_pre(place_to)

            flw = gref.get_path_flow()

            if gref_new or flw is None:
                flw = PathFinderFlow(ITileCollector.get(), gref)
                flw.set_moveables(self._childs)

            if flw.get_is_ready():
                flw.invalidate_finish()
                flw.prepare(place_to)

                gref.prepare(path_flow=flw, rel_object=place_to)
                flw.realize(PathFinderFlow.REALIZE_ATTACK_MOVE, place_to, True)

            else:
                gref.prepare(path_flow=flw, rel_object=place_to)
                flw.realize_light(PathFinderFlow.REALIZE_ATTACK_MOVE, place_to, True)

            for mref in self._childs:
                mref.attack_move(place_to, create_goto_group=False, path_flow=flw)

            if IGui.get().get_side() is self._childs[0].get_side():
                pt = ParticleCollector().add_particle(place_to.get_x(), place_to.get_y(), 'atmeff', -1, scale=1, group=GraphBatches().get_group_gui(), batch=GraphBatches().get_batch_black())
                pt.set_force_rotate(math.pi / 40, 0)

    def __place_callback(self, place_to, callback):
        places = self._map_scr.scan_avail_places(place_to, len(self._childs))

        moveables = (mm for mm in self._childs if mm.get_selected_type() == ISelectable.SELECTED_MOVEABLE)
        moveables = sorted(moveables, key=lambda mref: place_to.dist_to_moveable(mref) / mref.get_max_speed(), reverse=True)

        for mref in moveables:
            use_place = None
            use_dist = 0

            for tl in places:
                dist = tl.dist_to_moveable(mref)

                if use_place is None or dist < use_dist:
                    use_place = tl
                    use_dist = dist

            if not self.skip_when_targetting or not mref.get_is_targeting():
                use_place.test_flag = 1
                callback(mref, use_place)

            places.remove(use_place)

    def _place_callback(self, place_to, callback):

        tile_col = ITileCollector.get()
        mvs = {
            1: list([mm for mm in self._childs if mm.get_selected_type() == ISelectable.SELECTED_MOVEABLE and mm.get_spec().get_size() == 1]),
            2: list([mm for mm in self._childs if mm.get_selected_type() == ISelectable.SELECTED_MOVEABLE and mm.get_spec().get_size() == 2])
        }
        mvs[1] = sorted(mvs[1], key=lambda mref: place_to.dist_to_moveable(mref) / mref.get_max_speed(), reverse=True)
        mvs[2] = sorted(mvs[2], key=lambda mref: place_to.dist_to_moveable(mref) / mref.get_max_speed(), reverse=True)
        mvs_fin = []
        mvs_idx = {
            1: 0,
            2: 0
        }
        mvs_count = 0

        pls = {
            1: [],
            2: []
        }

        for size in mvs.keys():
            mvs_count += len(mvs[size])

        use_flag = tile_col.get_find_flag()
        sec_flag = tile_col.get_find_flag(1)

        stk = [place_to]

        for tt in stk:
            tt.back_steps = 0

        while len(stk) > 0 and mvs_count > 0:
            place = stk[0]
            del stk[0]

            is_acc = {
                1: True,
                2: True
            }

            for size in is_acc.keys():
                for pl in place.get_neighbors_size(size):
                    if not pl.is_accessible_simple() or pl.to_flag == sec_flag:
                        is_acc[size] = False

            min_size = -1
            min_dist = -1

            for size in is_acc.keys():
                if is_acc[size] and mvs_idx[size] < len(mvs[size]):
                    is_acc[size] = mvs[size][0]

                    dist = place.dist_to_moveable(mvs[size][0]) / mvs[size][0].get_max_speed()

                    if min_dist == -1 or min_dist < dist:
                        min_size = size
                        min_dist = dist

            if min_size != -1:
                mvs_count -= 1
                pls[min_size].append(place)
                mvs_fin.append(mvs[min_size][mvs_idx[min_size]])
                mvs_idx[min_size] += 1

                for pl in place.get_neighbors_size(min_size):
                    pl.to_flag = sec_flag

            # print("mvs count: " + str(mvs_count))

            place.flag = use_flag

            for sub_place in place.get_neighbors_around():
                if sub_place.flag != use_flag:
                    sub_place.flag = use_flag
                    stk.append(sub_place)

        tile_col.move_find_flag()
        tile_col.move_find_flag(1)

        for mref in mvs_fin:
            size = mref.get_spec().get_size()
            use_place = None
            use_dist = 0

            for tl in pls[size]:
                dist = tl.dist_to_moveable(mref)

                if use_place is None or dist < use_dist:
                    use_place = tl
                    use_dist = dist

            if not self.skip_when_targetting or not mref.get_is_targeting():
                # use_place.test_flag = 1
                callback(mref, use_place)

            pls[size].remove(use_place)
