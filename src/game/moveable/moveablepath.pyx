import math
import random

from lib.orderedset import OrderedSet

from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.discrete import get_angle_from
from game.helper.discretemath cimport int_sqrt
from game.helper.distangle import DistAngle
from game.interface.itilecollector import ITileCollector
from game.map.flowentity import FlowEntity
from game.map.mapscanner import MapScanner
from game.map.pathfinder import PathFinder
from game.const.specialflags import SpecialFlags
from game.moveable.gotogroup import GotoGroup
from game.moveable.unitspec import UnitSpec
from game.helper.vector cimport angle_dist_side_deg


class MoveablePath:

    FOCUS_NONE = 0
    FOCUS_TURN = 1
    FOCUS_MOVE = 2
    FOCUS_SEEK = 3

    _parent = None
    _spec: UnitSpec = None
    _path_fnd: PathFinder = None
    _map_scr: MapScanner = None
    _path_list = None
    _path_flow = None
    _flow_entity = None
    _trg_enforce = False
    _place = None
    _places = None
    _places_around = None
    _is_visible_place = False
    _is_visible_timeout = 0

    _focus_place = None
    _focus_next_place = None
    _focus_state_bef = False
    _focus_state = False

    _goto_place_after = None
    _wait_mref = None

    _speed = 0
    _max_speed = 0
    _walk_koef = 0
    _stuck_type = None
    _strict = False
    _evade_signal = True
    _tick = 0
    _trg_x = 0
    _trg_y = 0
    _trg_diff_x = 0
    _trg_diff_y = 0
    _trg_angle = 0
    _trg_angle_dist = None
    _trg_dist = 0
    _trg_timeout = 0
    _trg_time_count = 0
    _trg_range_expand = 0
    _trg_len_expand = 0
    _trg_place = None
    _trg_force_step = False
    _exclude_place = None
    _goto_nbs = False

    _track_step = 0
    _track_dist = 0
    _track_pts = None
    _track_angle = 0
    _track_draw = False

    def __init__(self, parent, path_fnd: PathFinder, map_scr: MapScanner):
        self._parent = parent
        self._spec = parent.get_spec()
        self._path_fnd = path_fnd
        self._map_scr = map_scr
        self._track_step = self._spec.get_height() / 2 if self._spec.get_track_opacity() > 0 else 0
        self._place = None
        self._places = OrderedSet()
        self._wait_mref = OrderedSet()
        self._places_around = []
        self._exclude_place = []
        self._focus_state = MoveablePath.FOCUS_NONE
        self._focus_state_bef = MoveablePath.FOCUS_NONE
        self._trg_angle_dist = DistAngle()
        self._trg_dist = 0
        self._flow_entity = FlowEntity(self._parent)

        MoveablePath.update_number = 0

    def get_place(self):
        return self._place

    def get_places(self):
        return self._places

    def get_places_around(self):
        return self._places_around

    def get_is_on_place(self, place):
        return place in self._places

    def get_is_moving(self):
        # !! return self._trg_place is not None or self._focus_state != MoveablePath.FOCUS_NONE
        return self._focus_state != MoveablePath.FOCUS_NONE

    def get_is_moving_atm(self):
        return self._focus_state == MoveablePath.FOCUS_MOVE

    def get_is_stuck(self):
        return self._trg_timeout > 0 or self._trg_time_count > 0

    def get_is_wait(self, find_mref=None):
        if find_mref is None:
            return len(self._wait_mref) > 0
        else:
            stk = []
            stk.extend(self._wait_mref)

            while len(stk) > 0:
                if stk[0][0] is find_mref:
                    return True
                else:
                    stk.extend(stk[0][0].get_wait_mref())
                    del stk[0]

            return False

    def get_wait_mref(self):
        return self._wait_mref

    def get_flow_entity(self):
        return self._flow_entity

    def get_current_speed(self):
        return self._speed

    def get_max_speed(self):
        return self._max_speed

    def set_max_speed(self, max_speed, wait):
        self._max_speed = self._spec.get_speed() if max_speed == 0 else max_speed

    def get_focus_place(self):
        return self._focus_place

    def get_focus_places(self):
        return [] if self._focus_place is None else self._focus_place.get_neighbors_size(self._spec.get_size())

    def get_trg_place(self):
        return self._trg_place

    def get_current_trg_place(self):
        if self._path_flow is not None:
            return self._path_flow.get_place_to()
        else:
            return self._path_list[0] if self._path_list is not None and len(self._path_list) > 0 else None

    def get_path_list(self):
        return self._path_list

    def get_path_list_len(self):
        return 0 if self._path_list is None else len(self._path_list)

    def get_is_visible_place(self):
        return self._is_visible_place

    def get_path_flow(self):
        return self._path_flow

    def get_state_map(self):
        sm = {
            "get_is_moving": self.get_is_moving(),
            "_focus_state": self._focus_state,
            "_stuck_type": self._stuck_type,
            "_trg_place": self._trg_place,
            "_trg_timeout": self._trg_timeout,
            "_trg_time_count": self._trg_time_count
        }

        return sm

    def set_focus_state(self, focus_state):
        if focus_state != self._focus_state:
            self._focus_state_bef = self._focus_state
            self._focus_state = focus_state

    def set_path_flow(self, path_flow):
        self._path_flow = path_flow

    def unset_timeouts(self):
        self._trg_timeout = 0
        self._trg_time_count = 0

    def _deceleration(self):
        if self._speed > 0:
            self._speed = max(0, self._speed - self._spec.get_dec())

            self._parent.move(self._speed)
            self._move_track(self._speed / DistAngle.ZOOM_VALUE, self._speed == 0)

            if self._focus_state != MoveablePath.FOCUS_NONE and self._trg_time_count == 0 and self._trg_timeout == 0:
                self._trg_angle = get_angle_from(self._trg_x - self._parent.get_x(), self._trg_y - self._parent.get_y())
                self._parent.rotate_step_to(self._trg_angle)

    def _move_track(self, dist, force_track=False):
        # track pts pro efekt stopy jizdy

        if self._track_step > 0 and (self._track_dist == 0 or force_track):
            if self._track_pts is None:
                pt1 = self._parent.get_two_points(self._spec.get_width() / 2, 0)
                pt2 = self._parent.get_two_points(self._spec.get_width() / 2, - self._spec.get_height() / 2)

                self._track_pts = [pt2, pt1]
                self._track_angle = self._parent.get_angle()

            else:
                ad = angle_dist_side_deg(self._track_angle, self._parent.get_angle())
                self._track_angle = self._parent.get_angle()

                if min(ad.left, ad.right) <= 200: # math.pi * 0.4:
                    self._track_pts[0] = self._track_pts[1]
                    self._track_pts[1] = self._parent.get_two_points(self._spec.get_width() / 2, 0)
                    self._track_draw = True

                else:
                    self._track_pts = None
                    self._track_dist = 100

        self._track_dist += dist

        if self._track_dist >= self._track_step:
            self._track_dist = 0

    def get_is_track_draw(self):
        return self._track_draw and self._track_pts is not None

    def get_track_draw_pts(self):
        self._track_draw = False

        return (
            self._track_pts[0][0], self._track_pts[0][1], self._track_pts[0][2], self._track_pts[0][3],
            self._track_pts[1][2], self._track_pts[1][3], self._track_pts[1][0], self._track_pts[1][1]
        )

    def serialize(self):
        out = {
            'trg_time_count': self._trg_time_count,
            'trg_timeout': self._trg_timeout,
            'focus_state': self._focus_state,
            'path_list_len': 0,
            'flow_realize_type': -1,
            'is_moving': self.get_is_moving(),
            'is_stuck': self.get_is_stuck()
        }

        if self._path_flow is not None:
            out['flow_realize_type'] = self._path_flow.get_realize_type()

        if self._path_list is not None:
            out['path_list_len'] = len(self._path_list)

        return out

    def update_seek(self, tick):
        self._tick = tick

        if self._focus_state == MoveablePath.FOCUS_SEEK:
            self._update_focus_seek()

    def update(self, tick):

        self._tick = tick
        self._is_visible_place = False

        for tl in self._places_around:
            if tl.get_visible():
                self._is_visible_place = True
                break

        if self._focus_state == MoveablePath.FOCUS_TURN:
            self._update_focus_turn()

        elif self._focus_state == MoveablePath.FOCUS_MOVE:
            self._update_focus_move()

        elif self._focus_state == MoveablePath.FOCUS_SEEK:

            if self._speed > 0:
                self._deceleration()

            return False

        else:
            if self._speed > 0:
                self._deceleration()

            self._parent.idle_work()

            if self._trg_timeout > 0:
                self._trg_timeout -= 1

                if self._trg_timeout == 0 and self._trg_place is not None:
                    self._goto_continue(self._trg_place, None)

        return True

    def _update_focus_turn(self):
        # zpomalovani a otaceni za novym cilem

        self._trg_angle = get_angle_from(self._trg_x - self._parent.get_x(), self._trg_y - self._parent.get_y())
        rotate_diff = self._parent.rotate_to_diff(self._trg_angle)

        if rotate_diff > 150:
            self._parent.rotate_step_to(self._trg_angle)
            self._move_track(0, True)
            self._deceleration()

        elif self._set_focus_place():
            # ok jede se dal
            self.set_focus_state(MoveablePath.FOCUS_MOVE)
            self._trg_dist = 10000
            self._update_focus_move()

        else:
            # nelze jet dal
            self.set_focus_state(MoveablePath.FOCUS_SEEK)

    def _update_focus_move(self):
        # jizda za cilem

        cdef long ang_diff, diff_x, diff_y, dist

        self._parent.move(self._speed)
        self._move_track(self._speed / DistAngle.ZOOM_VALUE)

        ang_diff = self._parent.rotate_to_diff(self._trg_angle)

        if self._spec.get_is_troop():
            self._walk_koef += 2

            if self._walk_koef > self._max_speed + self._max_speed // 2:
                self._walk_koef = self._max_speed // 4

            self._speed = self._walk_koef

        elif ang_diff < 125:
            self._speed = min(self._speed + self._spec.get_acc(), self._max_speed)

        else:
            self._speed = max(0, self._speed - self._spec.get_dec())

        if self._tick % 10 == 0:
            self._parent.update_body(walk_next=2)

        diff_x = self._trg_x - self._parent.get_x()
        diff_y = self._trg_y - self._parent.get_y()
        dist = int_sqrt(diff_x ** 2 + diff_y ** 2)

        if dist < self._trg_dist and dist > DistAngle.PLACE_SIZE_HALF:
            self._trg_angle = get_angle_from(self._trg_x - self._parent.get_x(), self._trg_y - self._parent.get_y())
            self._parent.rotate_step_to(self._trg_angle)

            self._trg_dist = dist + 2

        else:
            self.set_focus_state(MoveablePath.FOCUS_SEEK)

    def _update_focus_seek(self):
        # hledani kam jet

        if self._trg_timeout > 0:
            if self._speed >= 0:
                self._deceleration()
            else:
                self._parent.idle_work()

            self._trg_timeout -= 1

            if self._trg_timeout == 0:
                self._follow_next_seek()

        else:
            self._follow_next_seek()

            if self._focus_state == MoveablePath.FOCUS_SEEK:
                if self._speed >= 0:
                    self._deceleration()
                else:
                    self._parent.idle_work()

    def _follow_next_go(self):
        # realizace pohybu na dalsi policko (nejdrive natoceni)

        if self._focus_place is not None:
            self._focus_place.unset_moveable_focus(self._parent)

        if self._path_flow is not None:
            self._focus_place = self._path_flow.get_focus_place(self._parent)
            self._focus_next_place = self._path_flow.get_focus_place(self._parent)

        else:
            self._focus_place = self._path_list[len(self._path_list) - 1]
            self._focus_next_place = None if len(self._path_list) == 1 else self._path_list[len(self._path_list) - 2]

        if self._focus_place is None:
            self.set_focus_state(MoveablePath.FOCUS_SEEK)

        else:
            self._focus_place.set_moveable_focus(self._parent)

            self._trg_x, self._trg_y = self._focus_place.get_trg_coords(self._parent)
            self._trg_angle = get_angle_from(self._trg_x - self._parent.get_x(), self._trg_y - self._parent.get_y())

            if self._place is not self._focus_place:
                self._trg_angle_dist.set_places(self._place, self._focus_place)

            if self._parent.get_look_snap() is None:
                if self._spec.get_free_look():
                    trg_place = self.get_current_trg_place()

                    if trg_place is not None:
                        self._parent.set_look_to_place(trg_place)

                else:
                    self._parent.set_look_to_place(self._focus_place)

            self.set_focus_state(MoveablePath.FOCUS_TURN)

            self._trg_timeout = 0
            self._trg_time_count = 0
            self._stuck_type = None
            self._parent.idle_reset()

    def _follow_next_seek(self):
        # hledani dalsiho mozneho policka

        if self._path_flow is not None:
            if self._path_flow.get_is_finish(self._parent):
                self._trg_place = None
                self._stopped()
                return False

            to_focus = self._path_flow.get_focus_place(self._parent)

        else:
            if self._path_list is None or len(self._path_list) == 0:
                self._trg_place = None
                self._stopped()
                return False

            to_focus = self._path_list[len(self._path_list) - 1]

        if to_focus is None:
            self.set_focus_state(MoveablePath.FOCUS_SEEK)
            self._stuck_type = "no_place"
            self._trg_time_count += 1
            self._trg_timeout = 5
            return False

        self._trg_x, self._trg_y = to_focus.get_trg_coords(self._parent)
        self._trg_angle = get_angle_from(self._trg_x - self._parent.get_x(), self._trg_y - self._parent.get_y())
        if self._place is not to_focus:
            self._trg_angle_dist.set_places(self._place, to_focus)

        if self._trg_enforce:
            # vyjezd jednotky

            if self._trg_force_step:
                self._follow_next_go()

            elif to_focus.is_moveable_any(self._parent):
                for mref in to_focus.get_moveables():
                    if (mref.get_is_stuck() or not mref.get_is_moving()) and not mref.get_is_working():
                        mref.goto_nearest(self._path_list, self._path_list, force_strict=True)

                self.set_focus_state(MoveablePath.FOCUS_SEEK)
                self._stuck_type = "enforce"
                self._trg_timeout = 30
                self._trg_time_count += 1

            else:
                self._follow_next_go()

        elif len(self._wait_mref) > 0:

            self.set_focus_state(MoveablePath.FOCUS_SEEK)
            self._stuck_type = "wait_mref"
            self._trg_time_count = 1
            self._trg_timeout = 5

            for mm in [c for c in self._wait_mref]:
                if mm[0].get_is_wait(self._parent):
                    self._wait_mref.remove(mm)
                elif mm[0].get_place() is not mm[1]:
                    self._wait_mref.remove(mm)
                elif mm[0].get_is_alive() and mm[0].get_is_moving():
                    pass
                else:
                    self._wait_mref.remove(mm)

        elif not self._parent.get_may_move():
            self.set_focus_state(MoveablePath.FOCUS_SEEK)
            self._stuck_type = "not_may_move"
            self._trg_time_count = 1
            self._trg_timeout = 5

        elif to_focus.is_moveable_goto_group(self._parent, self._parent.get_goto_group()) and not to_focus.is_moveable_slower(self._parent) and self._trg_time_count < 20:

            if self._parent.rotate_to_diff(self._trg_angle) > 150:
                # nejdrive se natocit na policko
                self._follow_next_go()

            else:
                self.set_focus_state(MoveablePath.FOCUS_SEEK)
                self._stuck_type = "goto_group"
                self._trg_timeout = 5
                self._trg_time_count += 1

        elif to_focus.is_accessible(self._parent):
            self._follow_next_go()

        elif to_focus.is_moveable_standing(self._parent, self._parent.get_side()) and (self._trg_time_count >= 0 or to_focus is self._trg_place or self._strict) and self._evade_signal:

            grp = None
            h = set()
            mvs = []
            self._wait_mref.clear()

            for place in to_focus.get_neighbors_size(self._spec.get_size()):
                for mref in place.get_moveables_size(self._spec.get_size()):
                    if mref not in h and not mref.get_is_wait():
                        h.add(mref)

                        if mref.get_goto_group() is self._parent.get_goto_group():
                            #mvs.append(mref)
                            # !! radeji ne
                            pass

                        elif not mref.get_is_working() or (mref.get_is_stuck() and SpecialFlags.SPECIAL_MINER in mref.get_spec().get_specials()):

                            if mref.get_spec().get_size() < self._spec.get_size() or len(mvs) == 0:
                                # staci aby odjela jedna jednotka
                                mvs.append(mref)

            for mref in mvs:
                if grp is None:
                    grp = GotoGroup.create()

                mref.set_goto_group(grp)

                if mref.goto_nearest(self._place.get_neighbors_size(self._spec.get_size()), self._path_list):
                    self._wait_mref.add((mref, to_focus))

                    self.set_focus_state(MoveablePath.FOCUS_SEEK)
                    self._stuck_type = "standing"
                    self._trg_timeout = 5
                    self._trg_time_count += 1
                    self._trg_range_expand = 0

            if len(self._wait_mref) == 0:
                # neni koho signalizovat - najit jinou cestu

                if to_focus not in self._exclude_place:
                    self._exclude_place.append(to_focus)

                self.set_focus_state(MoveablePath.FOCUS_SEEK)
                self._goto_continue(self._trg_place, to_focus)

        elif self._strict and self._trg_time_count > 10 and to_focus.is_moveable_standing_stuck(self._parent, self._parent.get_side()):

            # !!
            self.set_focus_state(MoveablePath.FOCUS_SEEK)
            self._stuck_type = "strict_stuck"
            self._trg_time_count = 1
            self._trg_timeout = 30

            for mref in to_focus.get_moveables():
                if (mref.get_is_stuck() or not mref.get_is_moving()) and not mref.get_is_working():
                    mref.goto_nearest(self._place.get_neighbors_size(self._spec.get_size()), self._path_list)

        elif self._trg_place is not None:

            if to_focus not in self._exclude_place:
                self._exclude_place.append(to_focus)

            self.set_focus_state(MoveablePath.FOCUS_SEEK)
            self._goto_continue(self._trg_place, to_focus)

        if self._trg_time_count > 0:
            #print("stucked: " + self._stuck_type + ", cnt: " + str(self._trg_time_count))
            pass

    def _stopped(self):
        self.set_focus_state(MoveablePath.FOCUS_NONE)
        self._parent.update_body(walk_next=1)
        #self._move_track(0, True)

    def goto_pre(self, place_to):
        self._trg_place = place_to

    def goto_nearest(self, exc=None, cross_path=None, force_strict=False):

        av_places = []
        #PrimitiveCollector().clear()

        for tls in self._place.get_neighbors_around_backs(self._parent, get_all=True):
            tl = tls[0]
            allow = True

            if exc is not None:
                allow = next((False for c in exc if c in tls), True)

            if allow and self._focus_place is not None:
                allow = next((False for c in self.get_focus_places() if c in tls), True)

            if allow and self._trg_place is not None:
                allow = next((False for c in self._trg_place.get_neighbors_size(self._spec.get_size()) if c in tls), True)

            if allow:
                cost_add = 0

                if cross_path is not None and tl in cross_path:
                    cost_add += 100

                for sub_tl in tls:
                    if not sub_tl.is_accessible(self._parent):
                        cost_add += 50

                    #PrimitiveCollector().add_circle(sub_tl.get_x(), sub_tl.get_y(), 2 + cost_add // 10)
                    #PrimitiveCollector().add_line(self._place.get_x(), self._place.get_y(), sub_tl.get_x(), sub_tl.get_y(), color=(0, 0, 255))

                av_places.append({
                    'cost': (self._parent.number + len(av_places)) % 8 + cost_add,
                    'place': tl,
                    'places': tls.copy() + [self._place]
                })

                """
                if tl.is_moveable_standing(self._parent):
                    av_places.append({
                        'cost': 2 + cost_add, # random.random() +
                        'place': tl
                    })

                elif tl.is_moveable_standing_stuck(self._parent):
                    av_places.append({
                        'cost': 4 + cost_add, # random.random() +
                        'place': tl
                    })

                elif len(tl.get_moveables()) > 0:
                    av_places.append({
                        'cost': 6 + cost_add, # random.random() +
                        'place': tl
                    })

                else:
                    av_places.append({
                        'cost': 0 + cost_add, # random.random() +
                        'place': tl
                    })
                """

        av_places.sort(key=lambda c: c['cost'], reverse=False)

        if len(av_places) > 0:

            #PrimitiveCollector().add_circle(av_places[0]['place'].get_x(), av_places[0]['place'].get_y(), 2 + av_places[0]['cost'] // 10, color=(255, 255, 0))

            # !! self.goto(av_places[0]['place'], strict=av_places[0]['cost'] >= 1.5 or force_strict)
            self.goto_place_list(av_places[0]['places'], strict=av_places[0]['cost'] >= 1.5 or force_strict)
            return True

        else:
            return False

    def goto(self, place_to, strict=False, path_flow=None, check_target=False, evade_signal=True):
        self._trg_enforce = False
        self._trg_place = place_to
        self._strict = strict
        self._evade_signal = evade_signal
        self._trg_len_expand = 0
        self._trg_timeout = 0
        self._trg_time_count = 0
        self._trg_range_expand = 0
        self._trg_force_step = False
        self._path_list = None
        self._path_flow = path_flow
        self._goto_place_after = None

        if check_target and self._parent.get_target() is not None:
            self.stop()
            return False

        if self._path_flow is not None:

            if self._focus_state != MoveablePath.FOCUS_MOVE:
                if self._focus_place is not None:
                    self._focus_place.unset_moveable_focus(self._parent)
                    self._focus_place = None

            self._focus_next_place = self._path_flow.get_focus_place(self._parent)

            if self._path_flow.get_is_finish(self._parent):
                self._path_flow = None
                self._trg_place = None
                self.update_goto_group()

                return False

            else:
                if self._focus_state == MoveablePath.FOCUS_NONE:
                    self.set_focus_state(MoveablePath.FOCUS_SEEK)

                return True

        else:
            max_len_edge = max(ITileCollector.get().get_tiles_width() * 2, ITileCollector.get().get_tiles_height() * 2)

            place_found = self._path_fnd.path_find_place_for_moveable(self._parent, self._place, place_to, 0, max_len_edge, 250, strict=self._strict)

            if place_found is None:
                place_found = self._path_fnd.path_find_place_for_moveable(self._parent, self._place, place_to, 0, max_len_edge, 0, strict=self._strict)

            if place_found is not None:
                self._path_list = self._path_fnd.back_list(place_found, self._place)
                self._focus_next_place = None

            if place_found is not None:
                del self._path_list[len(self._path_list) - 1]

                if self._focus_state != MoveablePath.FOCUS_MOVE:
                    if self._focus_place is not None:
                        self._focus_place.unset_moveable_focus(self._parent)
                        self._focus_place = None

                if len(self._path_list) == 0:
                    self._path_list = None
                    self._trg_place = None
                    # !! self.set_focus_state(MoveablePath.FOCUS_NONE) !! NE !!
                    self.update_goto_group()

                    return False

                else:
                    self._focus_next_place = self._path_list[len(self._path_list) - 1]
                    # PrimitiveCollector().add_line(self._parent.get_x(), self._parent.get_y(), self._path_list[0].get_x(), self._path_list[0].get_y())

                    if self._focus_state == MoveablePath.FOCUS_NONE:
                        self.set_focus_state(MoveablePath.FOCUS_SEEK)

                    return True

            else:
                self._stuck_type = "cannot_goto"
                self._trg_time_count += 1
                self._trg_timeout = 60

        return False

    def goto_place_list(self, path_list, strict=False):
        self._trg_enforce = False
        self._trg_place = path_list[0]
        self._strict = strict
        self._trg_len_expand = 0
        self._trg_timeout = 0
        self._trg_time_count = 0
        self._trg_range_expand = 0
        self._trg_force_step = False
        self._path_list = path_list
        self._path_flow = None
        self._goto_place_after = None

        del self._path_list[len(self._path_list) - 1]

        self.set_focus_state(MoveablePath.FOCUS_SEEK)

    def goto_enforce_places(self, path_list, goto_place_after=None, force_step=False):
        self._trg_enforce = True
        self._trg_place = path_list[0]
        self._trg_force_step = force_step
        self._path_list = path_list
        self._path_flow = None
        self._goto_place_after = goto_place_after

        del self._path_list[len(self._path_list) - 1]

        self.set_focus_state(MoveablePath.FOCUS_SEEK)

    def stop(self, reflow=False):
        self._path_list = None
        self._path_flow = None
        self._trg_timeout = 0
        self._trg_time_count = 0
        self._trg_place = None
        self._trg_force_step = False
        self._stuck_type = None
        self.update_goto_group()

        if self._focus_state != MoveablePath.FOCUS_MOVE:
            self.set_focus_state(MoveablePath.FOCUS_NONE)

        if reflow and self._parent.get_goto_group().get_path_flow() is not None:
            self._parent.get_goto_group().get_path_flow().set_realize_force()

    def _goto_continue(self, place_to, collide_place):
        self._trg_place = place_to

        if self._path_flow is not None:
            self._trg_timeout = 0
            self._trg_time_count = 0
            self.set_focus_state(MoveablePath.FOCUS_SEEK)
            return True
            pass

        max_len_edge = max(ITileCollector.get().get_tiles_width() * 2, ITileCollector.get().get_tiles_height() * 2)

        if self._path_list is None:
            max_len = max_len_edge

        elif collide_place is None:
            max_len = len(self._path_list) + 5 + (self._trg_range_expand + self._trg_len_expand) * self._spec.get_size()

        elif collide_place.is_moveable_attacking(self._parent):
            max_len = len(self._path_list) * 2 + self._trg_len_expand * self._spec.get_size()

        elif collide_place.is_moveable_slower(self._parent):
            # jednotka na puvodnim policku je pomalejsi - ma smysl hledat lepsi delsi cestu
            max_len = len(self._path_list) * 2 + self._trg_len_expand * self._spec.get_size()

        else:
            max_len = len(self._path_list) + 5 + (self._trg_range_expand + self._trg_len_expand) * self._spec.get_size()
            # max_len = math.ceil(len(self._path_list) * 1.3) + self._trg_range_expand

        max_len = min(max_len_edge, max_len)

        to_range = (self._trg_range_expand * self._spec.get_size()) / 2
        place_found = self._path_fnd.path_find_place_for_moveable(self._parent, self._place, place_to, to_range, max_len, 125 + 125 * self._trg_time_count * self._spec.get_size(), strict=self._strict, exclude=self._exclude_place)
        #place_found = self._path_fnd.path_find_place_for_moveable(self._parent, self._place, place_to, to_range, max_len, 0, strict=self._strict, exclude=self._exclude_place)

        # print("GOTO: " + str(125 + 125 * self._trg_time_count * self._spec.get_size()) + " | exp: " + str(self._trg_range_expand) + " | " + str(random.random()) + " | exc: " + str(len(self._exclude_place)))

        if place_found is self._place:
            # lepsi stat na miste
            self._path_list = None
            self._path_flow = None
            self._stopped()
            self._trg_place = None
            self._trg_timeout = 0
            self._trg_time_count = 0
            self._trg_range_expand = 0

            self._parent.unset_actions()
            self.update_goto_group()

            return False

        if place_found is None and len(self._wait_mref) > 0:
            # pokud se ceka na uvolneni cesty jednotkou, pak hledani cesty s ignorovanim okolnich jednotek
            place_found = self._path_fnd.path_find_place_for_moveable(self._parent, self._place, place_to, to_range, max_len, 0, strict=self._strict, exclude=self._exclude_place)

        if place_found is not None:
            # pohyb nekam
            self._path_list = self._path_fnd.back_list(place_found, self._place)
            self._path_flow = None
            self._trg_range_expand = 0

            #for pl in self._path_list: pl.test_flag = 1

            del self._path_list[len(self._path_list) - 1]

            if len(self._path_list) == 0:
                self._path_list = None
                self._trg_place = None
                self._stopped()
                self.update_goto_group()

                return False

            else:
                #PrimitiveCollector().add_line(self._parent.get_x(), self._parent.get_y(), self._path_list[0].get_x(), self._path_list[0].get_y())
                pass

            self.update_goto_group()
            self._trg_timeout = 0
            self._trg_time_count = 0

            return True

        else:
            # rozumnou cestu nelze najit
            self._trg_time_count += 1
            self.set_focus_state(MoveablePath.FOCUS_SEEK)

            if self._trg_time_count > 5:
                self._trg_len_expand += 2

            self._trg_range_expand += 1

            if self._trg_place.is_moveable_standing_stuck(self._parent):
                # nema smysl cekat dlouho
                self._trg_timeout = 15
                self._stuck_type = "goto_stuck_15"

            elif self._trg_time_count > 100:
                self._trg_timeout = 150
                self._stuck_type = "goto_stuck_150"

            elif self._trg_time_count > 30:
                self._trg_timeout = 80
                self._stuck_type = "goto_stuck_80"

            else:
                # ceka se dele
                self._trg_timeout = 30
                self._stuck_type = "goto_stuck_30"

            return False

    def update_goto_group(self):
        grp = self._parent.get_goto_group()

        if grp.get_is_relevant():
            found = False

            for tl in self._places_around:
                mm = tl.get_moveable_goto_group_simple(self._parent, grp)

                if mm is not None:
                    grp.merge_keys(self._parent.get_goto_key(), mm.get_goto_key())

                    self._goto_nbs = True
                    found = True

            if not found and self._goto_nbs:
                self._goto_nbs = False
                grp.generate_key(self._parent)

        else:
            self._max_speed = self._spec.get_speed()

    def _set_focus_place(self):
        if self._focus_place is not None and (self._focus_place.is_accessible(self._parent) or self._trg_enforce or self._trg_force_step):

            before_place = self._place

            self.set_place(self._focus_place)
            self._exclude_place.clear()
            self._focus_place.unset_moveable_focus(self._parent)
            self._focus_place = None

            #PrimitiveCollector().add_line(before_place.get_x(), before_place.get_y(), self._place.get_x(), self._place.get_y())

            if self._path_flow is not None:

                self._path_flow.map_focus_place(self._parent, self._place, before_place)
                self.update_goto_group()
                self._parent.get_goto_group().sort_childs()

                if self._path_flow.get_is_finish(self._parent):
                    self._stopped()
                    self._trg_place = None

            elif self._path_list is None:
                self._stopped()
                self._trg_place = None

            else:
                del self._path_list[len(self._path_list) - 1]

                if len(self._path_list) == 0:
                    self._path_list = None
                    self.update_goto_group()

                    self._trg_place = None
                    # !! self.set_focus_state(MoveablePath.FOCUS_NONE) !! NE !!

                    if self._parent.get_born() == SpecialFlags.BORN_VAPORIZE:
                        self._parent.destroy_full()

                    elif self._parent.get_born() == SpecialFlags.BORN_SPAWNING:
                        self._parent.set_born(SpecialFlags.BORN_READY)
                        self._parent.get_side().event_finish_unit(self._parent)

                        if self._parent.get_upgrade() is not None or self._parent.get_builder() is not None:
                            pass
                        elif self._goto_place_after is not None:
                            self.goto(self._goto_place_after, strict=True, evade_signal=False)
                        else:
                            self._parent.goto_nearest()

                else:
                    self.update_goto_group()

            return True

        else:
            return False

    def set_place(self, place):
        self._place = place

        new_places = self._place.get_neighbors_size(self._spec.get_size())
        rem_places = OrderedSet()

        for pl in [p for p in self._places]:
            if pl not in new_places:
                self._places.remove(pl)
                pl.moveable_rem(self._parent)
                rem_places.add(pl)

        for pl in new_places:
            if pl in self._places:
                pass
            else:
                self._places.add(pl)
                pl.moveable_map(self._parent)

        [p.update_acc_spread() for p in self._places]
        [p.update_acc_spread() for p in rem_places]

        self._places_around.clear()

        for fy in range(- self._spec.get_size(), 2):
            for fx in range(-1, self._spec.get_size() + 1):
                if fy == - self._spec.get_size() or fy == 1 or fx == -1 or fx == self._spec.get_size():
                    pl = self._place.get_place_add_xy(fx, fy)

                    if pl is not None:
                        self._places_around.append(pl)

    def unset_place(self, clear_refs=True):
        if self._focus_place is not None:
            self._focus_place.unset_moveable_focus(self._parent)

        if self._place is not None:
            [p.moveable_rem(self._parent) for p in self._places]
            [p.update_acc_spread() for p in self._places]

            if clear_refs:
                self._place = None
                self._places = OrderedSet()
                self._places_around.clear()
