from game.helper.distangle import DistAngle
from game.interface.iworker import IWorker
from game.map.mapscanner import MapScanner
from game.const.specialflags import SpecialFlags
from game.map.maptiletexture import MapTileTexture
from game.moveable.unitspec import UnitSpec


class MoveableMiner:

    TASK_NONE = 0
    TASK_MINE = 1
    TASK_DUMP = 2
    TASK_STUCK = 3

    _map_scr: MapScanner = None

    _parent = None
    _spec: UnitSpec = None
    _tile_sweep = None
    _tile_sweep_last = None
    _builds = None
    _build_place = None
    _mined_resources = 0
    _timeout = 0
    _timeout_task = None

    def __init__(self, parent, map_scr: MapScanner):
        self._map_scr = map_scr

        self._parent = parent
        self._spec = parent.get_spec()
        self._mined_resources = 0
        self._builds = []
        self._timeout_task = MoveableMiner.TASK_NONE

    def get_is_working(self):
        return self._timeout_task != MoveableMiner.TASK_NONE or self._timeout_task == MoveableMiner.TASK_STUCK

    def get_is_stuck(self):
        return self._timeout_task == MoveableMiner.TASK_STUCK

    def init(self):
        self._refresh_builds()

    def destroy(self):
        pass

    def update(self, tick):

        if self._timeout > 0:
            # provadeni akce

            if self._timeout_task == MoveableMiner.TASK_MINE:
                ang = DistAngle().set_diff(0, 1)
                adiff = self._parent.rotate_step_to(ang.get_angle_deg())
                self._parent.get_angle_dist().set_angle_dist(ang)

                if adiff == 0:
                    self._timeout -= 1

            elif self._timeout_task == MoveableMiner.TASK_DUMP:

                if self._build_place.get_special() == SpecialFlags.SPECIAL_MINER:
                    ang = self._build_place.get_tile().get_special_data()["angle_dist"]
                    adiff = self._parent.rotate_step_to(ang.get_angle_deg())
                    self._parent.get_angle_dist().set_angle_dist(ang)

                    if adiff == 0:
                        self._timeout -= 1

                else:
                    self._build_place = None
                    self._timeout = 0
                    self._timeout_task = MoveableMiner.TASK_NONE

            else:
                self._timeout -= 1

            if self._timeout == 0:
                if self._timeout_task == MoveableMiner.TASK_MINE:
                    self._parent.update_body(mined=2)

                    self._tile_sweep_last = self._tile_sweep
                    self._tile_sweep.set_moveable_focus(None)
                    self._mined_resources = self._tile_sweep.mine_resources(350)

                    self._tile_sweep = None

                elif self._timeout_task == MoveableMiner.TASK_DUMP:
                    self._parent.get_side().money_add(self._mined_resources, add_stack=True)
                    self._parent.update_body(mined=1)
                    self._mined_resources = 0

                elif self._timeout_task == MoveableMiner.TASK_STUCK:
                    pass

                self._timeout_task = MoveableMiner.TASK_NONE

        elif tick % 201 == 0:
            self._refresh_builds()

        elif len(self._builds) == 0:
            # neni prirazena budova

            if tick % 100 == 0:
                self._refresh_builds()

        elif (not self._parent.get_is_moving() and tick % 10 == 0) or (self._parent.get_is_stuck() and tick % 100 == 0):

            if self._mined_resources > 0:

                if self._parent.get_place() is self._build_place:
                    self._timeout = 10 if IWorker.get().get_quick_mine() else 50
                    self._timeout_task = MoveableMiner.TASK_DUMP

                else:
                    self._build_place = self._find_dump_place(True)

                    if self._build_place is None:
                        self._build_place = self._find_dump_place(False)

                    if self._build_place is None or not self._parent.goto(self._build_place, True):
                        # nejde dojet

                        self._build_place = None
                        self._timeout = 50
                        self._timeout_task = MoveableMiner.TASK_STUCK

            elif self._tile_sweep is not None:

                if self._parent.get_tile() is self._tile_sweep:
                    # vytezeni obsahu

                    if self._tile_sweep.get_type() != MapTileTexture.T_FIELD:
                        self._tile_sweep.set_moveable_focus(None)
                        self._tile_sweep = None

                    else:
                        self._timeout = 10 if IWorker.get().get_quick_mine() else 130
                        self._timeout_task = MoveableMiner.TASK_MINE

                elif not self._parent.goto(self._tile_sweep.get_place(), True, strict=True):
                    # nejde dojet - hledani dalsiho

                    self._parent.stop()
                    self._tile_sweep.set_moveable_focus(None)
                    self._tile_sweep = None

            elif tick % 90 == 0:
                # hledani policka k vytezeni

                c = self._map_scr.scan_tiles(self._parent.get_tile(), self._spec.get_range_scan_tiles(), lambda tl, result: MoveableMiner._find_field(self._parent, tl, result), lambda tl: tl.is_accessible_no_build_only(), True)

                if c is not None:
                    self._tile_sweep = c['ref']
                    self._tile_sweep.set_moveable_focus(self._parent)

                elif self._tile_sweep_last is None:
                    # neni co vytezit
                    pass

                else:
                    self._parent.goto(self._tile_sweep_last.get_place(), True)
                    self._tile_sweep_last = None

        elif tick % 30 == 0 and self._tile_sweep is not None and (self._tile_sweep.get_type() != MapTileTexture.T_FIELD or not self._tile_sweep.check_moveable_focus(self._parent) or self._tile_sweep.is_moveable_spec(self._parent, self._spec)):
            self._tile_sweep.set_moveable_focus(None)
            self._tile_sweep = None
            self._parent.stop()

        elif tick % 30 == 0 and self._build_place is not None and not self._build_place.is_accessible(self._parent):
            self._build_place = self._find_dump_place(True)

            if self._build_place is None:
                self._build_place = self._find_dump_place(False)

            if self._build_place is None or not self._parent.goto(self._build_place, True):
                # nejde dojet

                self._build_place = None
                self._timeout = 50
                self._timeout_task = MoveableMiner.TASK_STUCK

    def _find_dump_place(self, accessible_only=False):
        m_dist = -1
        dump_place = None

        for bl in self._builds:
            if bl.get_is_alive():
                test_place = bl.get_places_special(SpecialFlags.SPECIAL_MINER)[0][0]

                if (accessible_only and test_place.is_accessible(self._parent)) or not accessible_only:
                    dist = self._parent.get_place().dist_to(test_place)

                    if m_dist == -1 or dist < m_dist:
                        dump_place = test_place
                        m_dist = dist

        return dump_place

    def _refresh_builds(self):
        for bl in [c for c in self._builds]:
            if not bl.get_is_alive():
                self._builds.remove(bl)

        scn = self._map_scr.scan_tiles(self._parent.get_tile(), self._spec.get_range_scan_tiles(),
                                       lambda tl, result: MoveableMiner._find_building(self._parent, tl, result),
                                       lambda tl: tl.is_accessible_tolerance(self._parent, 0))

        for c in scn:
            if not c in self._builds:
                self._builds.append(c)

    @staticmethod
    def _find_field(self, tile_loop, result):
        if tile_loop.get_type() == MapTileTexture.T_FIELD and tile_loop.is_accessible_no_build_only() and tile_loop.check_moveable_focus(self) and not tile_loop.is_moveable_spec(self, self.get_spec()):

            if result is None or tile_loop.back_steps // 10 < result['steps']:
                result = {
                    'ref': tile_loop,
                    'steps': tile_loop.back_steps // 10
                }

        return result

    @staticmethod
    def _find_building(self, tile_loop, result):
        if result is None:
            result = []

        if tile_loop.is_accessible_tolerance(self, 0) and tile_loop.get_special() == SpecialFlags.SPECIAL_MINER:

            bref = tile_loop.get_build_ref()

            if bref is not None and bref.get_is_alive() and bref.get_side() is self.get_side():
                result.append(bref)

        return result
