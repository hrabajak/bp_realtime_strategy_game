import random

from game.const.specialflags import SpecialFlags
from game.image.imagecollector import ImageCollector
from game.interface.iimages import IImages
from game.logger import Logger
from game.shot.shotspec import ShotSpec


class UnitSpec:

    _specs = {}
    _specs_by_num = {}
    _key = None
    _key_base = None
    _side_type = None
    _name = None
    _tex_pad = None
    _tex_pad_full = None
    _tex_pad_dmg = None
    _tex_pad_dead = None
    _tex_turret = None
    _tex_canon = None
    _tex_track = None
    _tex_troop_shot = None
    _tex_troop_walk = None
    _target_priority = 2
    _size = 2
    _width = 20
    _height = 20
    _mass = 30
    _speed = 1
    _acc = 0.1
    _dec = 0.1
    _turn_speed = 0.05
    _look_speed = 0.05
    _free_look = True
    _look_when_stand = False
    _is_troop = False
    _is_scout = False
    _specials = []
    _armor = {}
    _health = 1000
    _range = 150
    _range_scan = 150
    _range_sight = 150
    _shot_count = 1
    _shot_effect = "boom"
    _shot_spec = None
    _shot_reload = 60
    _shot_mid_reload = 15
    _shot_cooldown = 0
    _shot_force_canon = -10
    _shot_force_turret = -4
    _shot_canon_dist = 25
    _collide_coords_range = 25
    _collide_area_range = 20
    _max_quantity = 0
    _track_opacity = 0
    _build_money_cost = 0
    _build_delay = 0

    #_troop_kf = 3
    _troop_kf = 1

    number = 0
    _number_total = 0

    def __init__(self, key):
        self._key = key
        self._key_base = key[3:]

        self.number = UnitSpec._number_total
        UnitSpec._number_total += 1

    def get_key(self):
        return self._key

    def get_key_base(self):
        return self._key_base

    def get_side_type(self):
        return self._side_type

    def get_name(self):
        return self._name

    def get_tex_pad(self, color=None):
        return ImageCollector().get_image_color(self._tex_pad, color)

    def get_tex_pad_full(self, color=None):
        return ImageCollector().get_image_color(self._tex_pad_full, color)

    def get_tex_pad_dmg(self, key=0, color=None):
        return ImageCollector().get_image_color(self._tex_pad_dmg[key], color)

    def get_tex_pad_dead(self):
        return ImageCollector().get_image_color(self._tex_pad_dead)

    def get_tex_turret(self, key=0, color=None):
        return ImageCollector().get_image_color(self._tex_turret[key], color)

    def get_tex_canon(self, key=0, color=None):
        return ImageCollector().get_image_color(self._tex_canon[key], color)

    def get_tex_track(self):
        return ImageCollector().get_image_color(self._tex_track)

    def get_tex_troop_shot(self, color=None):
        return ImageCollector().get_image_color(self._tex_troop_shot, color)

    def get_tex_troop_walk(self, key=0, color=None):
        return ImageCollector().get_image_color(self._tex_troop_walk[key], color)

    def get_tex_load(self, color):
        self.get_tex_pad(color)
        self.get_tex_pad_full(color)
        self.get_tex_pad_dmg(0, color)
        self.get_tex_pad_dmg(1, color)
        self.get_tex_pad_dead()
        self.get_tex_turret(0, color)
        self.get_tex_turret(1, color)
        self.get_tex_canon(0, color)
        self.get_tex_canon(1, color)
        self.get_tex_track()

        if self.get_is_troop():
            self.get_tex_troop_shot(color)
            self.get_tex_troop_walk(0, color)
            self.get_tex_troop_walk(1, color)
            self.get_tex_troop_walk(2, color)
            self.get_tex_troop_walk(3, color)

    def get_target_priority(self):
        return self._target_priority

    def get_size(self):
        return self._size

    def get_width(self):
        return self._width

    def get_height(self):
        return self._height

    def get_mass(self):
        return self._mass

    def get_mass_sec(self):
        return 1 if self._size == 1 else self._size // 2

    def get_mass_third(self):
        return self._size ** 2

    def get_speed(self):
        return self._speed

    def get_acc(self):
        return self._acc

    def get_dec(self):
        return self._dec

    def get_turn_speed(self):
        return self._turn_speed

    def get_look_speed(self):
        return self._look_speed

    def get_free_look(self):
        return self._free_look

    def get_look_when_stand(self):
        return self._look_when_stand

    def get_specials(self):
        return self._specials

    def get_health(self):
        return self._health

    def get_range(self):
        return self._range

    def get_range_scan(self):
        return self._range_scan

    def get_range_sight(self):
        return self._range_sight

    def get_range_tiles(self):
        return self._range // 50

    def get_range_scan_tiles(self):
        return self._range_scan // 50

    def get_attack_able(self):
        #return False
        #return self._side_type == "s1"
        return self._shot_count > 0

    def get_shot_count(self):
        return self._shot_count

    def get_armor(self):
        return self._armor

    def get_shot_effect(self):
        return self._shot_effect

    def get_shot_spec(self):
        return self._shot_spec

    def get_shot_reload(self):
        return self._shot_reload

    def get_shot_mid_reload(self):
        return self._shot_mid_reload

    def get_shot_cooldown(self):
        return self._shot_cooldown

    def get_shot_force_canon(self):
        return self._shot_force_canon

    def get_shot_force_turret(self):
        return self._shot_force_turret

    def get_shot_canon_dist(self):
        return self._shot_canon_dist

    def get_shot_canon_dist_rnd(self, rand):
        return self._shot_canon_dist + random.random() * rand - rand / 2

    def get_is_troop(self):
        return self._is_troop

    def get_is_scout(self):
        return self._is_scout

    def get_collide_coords_range(self):
        return self._collide_coords_range

    def get_collide_area_range(self):
        return self._collide_area_range

    def get_max_quantity(self):
        return self._max_quantity

    def get_track_opacity(self):
        return self._track_opacity

    def get_damage_per_second(self):
        if self._shot_spec is None:
            return 0

        else:
            dmg = self._shot_count * self._shot_spec.get_damage()
            delay = self._shot_mid_reload * self._shot_count + self._shot_reload + self._shot_cooldown

            if self._is_troop:
                dmg *= UnitSpec._troop_kf

            return (50.0 * dmg) / delay

    def get_damage_till_die(self):
        if self._shot_spec is None:
            return 0

        else:
            if self._is_troop:
                mass_per_second = (self._health * UnitSpec._troop_kf) / 50.0
            else:
                mass_per_second = self._health / 50.0

            return mass_per_second * self.get_damage_per_second()

    def get_damage_per_cost(self):
        if self._shot_spec is None:
            return 0

        else:
            if self._is_troop:
                return self.get_damage_till_die() / (self._build_money_cost * UnitSpec._troop_kf)
            else:
                return self.get_damage_till_die() / self._build_money_cost

    def get_endurance(self, dmg_spec):
        if dmg_spec.get_shot_spec() is None:
            return 0
        else:
            armor = 1000.0

            if dmg_spec.get_shot_spec() in self._armor:
                armor = self._armor[dmg_spec.get_shot_spec()]

            if self._is_troop:
                return (self._health * UnitSpec._troop_kf) / (dmg_spec.get_damage_per_second() * (armor / 1000.0))
            else:
                return self._health / (dmg_spec.get_damage_per_second() * (armor / 1000.0))

    def get_endurance_neutral(self):
        armor = 1000.0

        if self._is_troop:
            return (self._health * UnitSpec._troop_kf) / (25 * (armor / 1000.0))
        else:
            return self._health / (25 * (armor / 1000.0))

    def get_bonus_perc(self, dmg_spec):
        if dmg_spec.get_shot_spec() is None:
            return 0
        else:
            armor = 1000.0

            if dmg_spec.get_shot_spec() in self._armor:
                armor = self._armor[dmg_spec.get_shot_spec()]

            return armor / 1000.0

    def get_ar(self, dmg_spec):
        if dmg_spec.get_shot_spec() is None:
            return 0
        else:
            armor = 1000.0

            if dmg_spec.get_shot_spec() in self._armor:
                armor = self._armor[dmg_spec.get_shot_spec()]

            return armor

    def get_max_dealt_damage(self, to_spec):
        if self._shot_spec is None:
            return 0

        dmg = self._shot_spec.get_damage()

        if self._shot_spec in to_spec.get_armor():
            dmg = (dmg * to_spec.get_armor()[self._shot_spec]) // 1000

        return min(to_spec.get_health(), dmg)

    def get_delay_per_health(self):
        return self.get_health() / self._build_delay

    def get_select_tex(self, is_hold):
        if self._size == 1:
            return ImageCollector().get_image("select_hold_small" if is_hold else "select_small")
        else:
            return ImageCollector().get_image("select_hold" if is_hold else "select")

    def set_build_money_cost(self, value):
        self._build_money_cost = value

    def set_build_delay(self, value):
        self._build_delay = value

    @staticmethod
    def init_specs(imgs: IImages):

        troop_armor = {
            ShotSpec.get("mini_vehicle"): 3000,
            ShotSpec.get("mini_jeep"): 3500,
            ShotSpec.get("mini_troop"): 2500,
            ShotSpec.get("sniper"): 4000,
            ShotSpec.get("beam_shot"): 2000,
            ShotSpec.get("laser"): 3000,
            ShotSpec.get("laser_beam"): 3000
        }

        # === side 1 ===

        s = UnitSpec("s1_troop")
        s._side_type = "s1"
        s._name = "Troop"
        s._tex_pad = "s1_troop0"
        s._tex_pad_dmg = None, "s1_troop0"
        s._tex_pad_dead = None #"troop_dead"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_troop_walk = "s1_troop1", "s1_troop0", "s1_troop2", "s1_troop0"
        s._tex_troop_shot = "s1_troop3"
        s._width = 17
        s._height = 10
        s._mass = 23
        s._size = 1
        s._speed = 30
        s._acc = 5
        s._dec = 10
        s._turn_speed = 32
        s._look_speed = 32
        s._free_look = False
        s._is_troop = True
        s._health = 400
        s._range = 300
        s._range_scan = 400
        s._range_sight = 300
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("mini_troop")
        s._armor = {}
        s._armor = troop_armor.copy()
        s._shot_effect = "mini_troop"
        s._shot_count = 4
        s._shot_reload = 35
        s._shot_mid_reload = 7
        s._shot_cooldown = 35
        s._shot_force_canon = 0
        s._shot_force_turret = 0
        s._collide_coords_range = 12
        s._collide_area_range = 10

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_troop_cannon")
        s._side_type = "s1"
        s._name = "Cannon troop"
        s._tex_pad = "s1_troop_cannon0"
        s._tex_pad_dmg = None, "s1_troop_cannon0"
        s._tex_pad_dead = None #"troop_dead"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_troop_walk = "s1_troop_cannon1", "s1_troop_cannon0", "s1_troop_cannon2", "s1_troop_cannon0"
        s._tex_troop_shot = "s1_troop_cannon3"
        s._width = 17
        s._height = 10
        s._mass = 23
        s._size = 1
        s._speed = 22
        s._acc = 5
        s._dec = 10
        s._turn_speed = 32
        s._look_speed = 32
        s._free_look = False
        s._is_troop = True
        s._health = 400
        s._range = 300
        s._range_scan = 450
        s._range_sight = 450
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("tank")
        s._armor = {}
        s._armor = troop_armor.copy()
        s._shot_effect = "tank"
        s._shot_count = 1
        s._shot_reload = 80
        s._shot_mid_reload = 0
        s._shot_cooldown = 50
        s._shot_force_canon = 0
        s._shot_force_turret = 0
        s._collide_coords_range = 12
        s._collide_area_range = 10

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_sniper")
        s._side_type = "s1"
        s._name = "Sniper"
        s._tex_pad = "s1_sniper0"
        s._tex_pad_dmg = None, "s1_sniper0"
        s._tex_pad_dead = None #"troop_dead"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_troop_walk = "s1_sniper1", "s1_sniper0", "s1_sniper2", "s1_sniper0"
        s._tex_troop_shot = "s1_sniper3"
        s._width = 17
        s._height = 10
        s._mass = 23
        s._size = 1
        s._speed = 22
        s._acc = 5
        s._dec = 10
        s._turn_speed = 32
        s._look_speed = 32
        s._free_look = False
        s._is_troop = True
        s._health = 400
        s._range = 500
        s._range_scan = 0
        s._range_sight = 500
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("sniper")
        s._armor = {}
        s._armor = troop_armor.copy()
        s._armor[ShotSpec.get("mini_vehicle")] = 4000
        s._armor[ShotSpec.get("mini_jeep")] = 4000
        s._shot_effect = "sniper"
        s._shot_count = 1
        s._shot_reload = 75
        s._shot_mid_reload = 0
        s._shot_cooldown = 75
        s._shot_force_canon = 0
        s._shot_force_turret = 0
        s._collide_coords_range = 12
        s._collide_area_range = 10

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_jeep")
        s._side_type = "s1"
        s._name = "Jeep"
        s._tex_pad = "jeep_pad"
        s._tex_pad_dmg = "jeep_damage", "jeep_damage_mono"
        s._tex_turret = "jeep_turret", "jeep_turret_mono"
        s._tex_canon = "jeep_cannon", "jeep_cannon_mono"
        s._tex_track = "track"
        s._width = 22
        s._height = 35
        s._mass = 30
        s._speed = 60
        s._acc = 6
        s._dec = 4
        s._turn_speed = 12
        s._look_speed = 32
        s._is_scout = True
        #s._free_look = False
        #s._look_when_stand = True
        s._health = 850
        s._range = 250
        s._range_scan = 400
        s._range_sight = 400
        s._armor = {
            ShotSpec.get("minirocket"): 1300,
            ShotSpec.get("beam_shot"): 1500,
            ShotSpec.get("beam_shot_small"): 1300
        }
        s._shot_effect = "mini_jeep"
        s._shot_spec = ShotSpec.get("mini_jeep")
        s._shot_count = 5
        s._shot_canon_dist = 15
        s._shot_reload = 23 * 5 - 4 * 5
        s._shot_mid_reload = 5
        s._shot_force_canon = -2
        s._shot_force_turret = -1
        s._track_opacity = 0.05

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_plate")
        s._side_type = "s1"
        s._name = "Plated tank"
        s._tex_pad = "plate_pad"
        s._tex_pad_dmg = "plate_damage", "plate_damage_mono"
        s._tex_turret = "plate_turret", "plate_turret_mono"
        s._tex_canon = "plate_cannon", "plate_cannon_mono"
        s._tex_track = "track2"
        s._width = 20
        s._height = 38
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 2
        s._turn_speed = 8
        s._look_speed = 16
        s._health = 1300
        s._range = 350
        s._range_scan = 450
        s._range_sight = 450
        s._shot_reload = 135
        s._shot_mid_reload = 30
        s._shot_count = 3
        s._shot_force_canon = -6
        s._shot_force_turret = -3
        s._shot_canon_dist = 18
        s._armor = {
            ShotSpec.get("laser"): 1400,
            ShotSpec.get("laser_beam"): 1400,
            ShotSpec.get("beam_shot"): 1400,
            ShotSpec.get("beam_shot_small"): 1200,
            ShotSpec.get("mini_vehicle"): 1400,
            ShotSpec.get("mini_jeep"): 1300,
            ShotSpec.get("minirocket"): 500,
            ShotSpec.get("rocket"): 800,
            ShotSpec.get("tank"): 800,
            ShotSpec.get("sniper"): 300
        }
        s._shot_effect = "miniboom"
        s._shot_spec = ShotSpec.get("heat_shot")
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_lighttank")
        s._side_type = "s1"
        s._name = "Light tank"
        s._tex_pad = "lighttank_pad"
        s._tex_pad_dmg = "lighttank_damage", "lighttank_damage_mono"
        s._tex_turret = "lighttank_turret", "lighttank_turret_mono"
        s._tex_canon = "lighttank_cannon", "lighttank_cannon_mono"
        s._tex_track = "track"
        s._width = 23
        s._height = 37
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 2
        s._turn_speed = 8
        s._look_speed = 12
        s._health = 1500
        s._range = 300
        s._range_scan = 450
        s._range_sight = 450
        s._armor = {
            ShotSpec.get("laser"): 800,
            ShotSpec.get("laser_beam"): 800,
            ShotSpec.get("minirocket"): 1200,
            ShotSpec.get("beam_shot"): 800,
            ShotSpec.get("beam_shot_small"): 800,
            ShotSpec.get("sniper"): 500,
            ShotSpec.get("rocket"): 1500,

            ShotSpec.get("mini_jeep"): 800,
            ShotSpec.get("mini_troop"): 800
        }
        s._shot_spec = ShotSpec.get("tank")
        s._shot_effect = "tank"
        s._shot_reload = 60
        s._shot_mid_reload = 15
        s._shot_force_canon = -8
        s._shot_force_turret = -4
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_medtank")
        s._side_type = "s1"
        s._name = "Medium tank"
        s._tex_pad = "medtank_pad"
        s._tex_pad_dmg = "medtank_damage", "medtank_damage_mono"
        s._tex_turret = "medtank_turret", "medtank_turret_mono"
        s._tex_canon = "medtank_cannon", "medtank_cannon_mono"
        s._tex_track = "track"
        s._width = 25
        s._height = 38
        s._mass = 35
        s._speed = 26
        s._acc = 1
        s._dec = 1
        s._turn_speed = 6
        s._look_speed = 7
        s._health = 2300
        s._range = 300
        s._range_scan = 450
        s._range_sight = 450
        s._armor = {
            ShotSpec.get("laser"): 600,
            ShotSpec.get("laser_beam"): 800,
            ShotSpec.get("minirocket"): 1200,
            ShotSpec.get("beam_shot"): 800,
            ShotSpec.get("beam_shot_small"): 800,
            ShotSpec.get("sniper"): 500,
            ShotSpec.get("rocket"): 1500,

            ShotSpec.get("mini_vehicle"): 800,
            ShotSpec.get("mini_jeep"): 700,
            ShotSpec.get("mini_troop"): 600
        }
        s._shot_spec = ShotSpec.get("tank")
        s._shot_effect = "tank"
        s._shot_reload = 85
        s._shot_mid_reload = 15
        s._shot_count = 2
        s._shot_force_canon = -10
        s._shot_force_turret = -3
        s._track_opacity = 0.2

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_rocket")
        s._side_type = "s1"
        s._name = "Rocket launcher"
        s._tex_pad = "rocket_pad"
        s._tex_pad_dmg = "rocket_damage", "rocket_damage_mono"
        s._tex_turret = "rocket_turret", "rocket_turret_mono"
        s._tex_canon = "rocket_cannon", "rocket_cannon_mono"
        s._tex_track = "track"
        s._width = 22
        s._height = 45
        s._mass = 35
        s._speed = 30
        s._acc = 1
        s._dec = 2
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._health = 800
        s._range = 500
        s._range_scan = 0
        s._range_sight = 500
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("rocket")
        s._armor = {
            ShotSpec.get("laser"): 1500,
            ShotSpec.get("laser_beam"): 1500,
            ShotSpec.get("beam_shot"): 2000,
            ShotSpec.get("beam_shot_small"): 1500,
            ShotSpec.get("minirocket"): 1300
        }
        s._shot_effect = "rocket"
        s._shot_count = 1
        s._shot_reload = 300
        s._shot_mid_reload = 0
        s._shot_force_canon = -8
        s._shot_force_turret = -6
        s._track_opacity = 0.2

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_miner")
        s._side_type = "s1"
        s._name = "Harvester"
        s._tex_pad = "s1_miner_pad"
        s._tex_pad_full = "s1_miner_pad_full"
        s._tex_pad_dmg = None, "s1_miner_pad_mono"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_track = "track"
        s._target_priority = 1
        s._width = 24
        s._height = 39
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 4
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._specials = [SpecialFlags.SPECIAL_MINER]
        s._health = 3000
        s._range = 400
        s._range_scan = 800
        s._range_sight = 400
        s._shot_spec = None
        s._shot_effect = None
        s._shot_count = 0
        s._track_opacity = 0.15
        s._max_quantity = 5

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_upgrade")
        s._side_type = "s1"
        s._name = "Upgrade vehicle"
        s._tex_pad = "s1_upgrade_pad"
        s._tex_pad_full = "s1_upgrade_pad_full"
        s._tex_pad_dmg = None, "s1_upgrade_pad_mono"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_track = "track"
        s._target_priority = 1
        s._width = 35
        s._height = 44
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 4
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._specials = [SpecialFlags.SPECIAL_UPGRADE]
        s._health = 1500
        s._range = 400
        s._range_scan = 400
        s._range_sight = 400
        s._shot_spec = None
        s._shot_effect = None
        s._shot_count = 0
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s1_builder")
        s._side_type = "s1"
        s._name = "Build vehicle"
        s._tex_pad = "s1_builder_pad"
        s._tex_pad_dmg = None, "s1_upgrade_pad_mono"
        s._tex_turret = "s1_builder_turret", "s1_builder_turret_mono"
        s._tex_canon = None, None
        s._tex_track = "track"
        s._target_priority = 1
        s._width = 20
        s._height = 40
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 4
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._specials = [SpecialFlags.SPECIAL_BUILD]
        s._health = 1500
        s._range = 400
        s._range_scan = 400
        s._range_sight = 400
        s._shot_spec = None
        s._shot_effect = None
        s._shot_count = 0
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        # === side 2 ===

        s = UnitSpec("s2_troop")
        s._side_type = "s2"
        s._name = "Troop"
        s._tex_pad = "s2_troop0"
        s._tex_pad_dmg = None, "s2_troop0"
        s._tex_pad_dead = None #"troop_dead"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_troop_walk = "s2_troop1", "s2_troop0", "s2_troop2", "s2_troop0"
        s._tex_troop_shot = "s2_troop3"
        s._width = 17
        s._height = 10
        s._mass = 23
        s._size = 1
        s._speed = 30
        s._acc = 10
        s._dec = 10
        s._turn_speed = 32
        s._look_speed = 32
        s._free_look = False
        s._is_troop = True
        s._health = 400
        s._range = 300
        s._range_scan = 400
        s._range_sight = 300
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("laser_beam")
        s._armor = troop_armor.copy()
        s._armor[ShotSpec.get("tank")] = 1100
        s._shot_effect = "laser_beam"
        s._shot_count = 2
        s._shot_reload = 45
        s._shot_mid_reload = 25
        s._shot_cooldown = 25
        s._shot_force_canon = 0
        s._shot_force_turret = 0
        s._collide_coords_range = 12
        s._collide_area_range = 10

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_troop_rocket")
        s._side_type = "s2"
        s._name = "Troop rocket"
        s._tex_pad = "s2_troop_rocket0"
        s._tex_pad_dmg = None, "s2_troop_rocket0"
        s._tex_pad_dead = None #"troop_dead"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_troop_walk = "s2_troop_rocket1", "s2_troop_rocket0", "s2_troop_rocket2", "s2_troop_rocket0"
        s._tex_troop_shot = "s2_troop_rocket3"
        s._width = 17
        s._height = 10
        s._mass = 23
        s._size = 1
        s._speed = 22
        s._acc = 10
        s._dec = 10
        s._turn_speed = 32
        s._look_speed = 32
        s._free_look = False
        s._is_troop = True
        s._health = 400
        s._range = 350
        s._range_scan = 450
        s._range_sight = 450
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("minirocket")
        s._armor = troop_armor.copy()
        s._armor[ShotSpec.get("heat_shot")] = 2000
        s._shot_effect = "minirocket"
        s._shot_count = 1
        s._shot_reload = 200
        s._shot_mid_reload = 0
        s._shot_cooldown = 50
        s._shot_force_canon = 0
        s._shot_force_turret = 0
        s._collide_coords_range = 12
        s._collide_area_range = 10

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_troop_beam")
        s._side_type = "s2"
        s._name = "Troop beam"
        s._tex_pad = "s2_troop_beam0"
        s._tex_pad_dmg = None, "s2_troop_beam0"
        s._tex_pad_dead = None #"troop_dead"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_troop_walk = "s2_troop_beam1", "s2_troop_beam0", "s2_troop_beam2", "s2_troop_beam0"
        s._tex_troop_shot = "s2_troop_beam3"
        s._width = 17
        s._height = 10
        s._mass = 23
        s._size = 1
        s._speed = 22
        s._acc = 10
        s._dec = 10
        s._turn_speed = 32
        s._look_speed = 32
        s._free_look = False
        s._is_troop = True
        s._health = 400
        s._range = 350
        s._range_scan = 450
        s._range_sight = 450
        s._shot_canon_dist = 10
        s._shot_spec = ShotSpec.get("beam_shot_small")
        s._armor = troop_armor.copy()
        s._armor[ShotSpec.get("tank")] = 1500
        s._shot_effect = "bigboom_small"
        s._shot_count = 1
        s._shot_reload = 150
        s._shot_mid_reload = 0
        s._shot_cooldown = 50
        s._shot_force_canon = 0
        s._shot_force_turret = 0
        s._collide_coords_range = 12
        s._collide_area_range = 10

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_basic")
        s._side_type = "s2"
        s._name = "Basic"
        s._tex_pad = "basic_pad"
        s._tex_pad_dmg = "basic_damage", "basic_damage_mono"
        s._tex_turret = "basic_turret", "basic_turret_mono"
        s._tex_canon = "basic_cannon", "basic_cannon_mono"
        s._tex_track = "track2"
        s._width = 18
        s._height = 27
        s._mass = 30
        s._speed = 60
        s._acc = 6
        s._dec = 4
        s._turn_speed = 12
        s._look_speed = 32
        s._is_scout = True
        s._health = 600
        s._range = 250
        s._range_scan = 400
        s._range_sight = 400
        #s._shot_count = 6
        #s._shot_reload = 60
        #s._shot_mid_reload = 10
        s._shot_count = 2
        s._shot_reload = 40
        s._shot_mid_reload = 0
        s._shot_force_canon = -4
        s._shot_force_turret = -2
        s._shot_canon_dist = 15
        s._armor = {
            ShotSpec.get("heat_shot"): 1500
        }
        #s._shot_effect = "lasereff"
        #s._shot_spec = ShotSpec.get("laser")
        s._shot_effect = "laser_beam"
        s._shot_spec = ShotSpec.get("laser_beam")
        s._track_opacity = 0.1

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_mini")
        s._side_type = "s2"
        s._name = "Minigun vehicle"
        s._tex_pad = "mini_pad"
        s._tex_pad_dmg = "mini_damage", "mini_damage_mono"
        s._tex_turret = "mini_turret", "mini_turret_mono"
        s._tex_canon = "mini_cannon", "mini_cannon_mono"
        s._tex_track = "track"
        s._width = 24
        s._height = 30
        s._mass = 30
        s._speed = 40
        s._acc = 2
        s._dec = 2
        s._turn_speed = 13
        s._look_speed = 13
        s._health = 900
        s._range = 250
        s._range_scan = 400
        s._range_sight = 400
        s._armor = {
            ShotSpec.get("mini_vehicle"): 700,
            ShotSpec.get("mini_jeep"): 700,
            ShotSpec.get("mini_troop"): 700,
            ShotSpec.get("heat_shot"): 1300,
            ShotSpec.get("sniper"): 500
        }
        s._shot_effect = "mini_vehicle"
        s._shot_spec = ShotSpec.get("mini_vehicle")
        s._shot_count = 10
        s._shot_reload = 75
        s._shot_mid_reload = 5
        s._shot_force_canon = -4
        s._shot_force_turret = -2
        s._shot_canon_dist = 17
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_rock")
        s._side_type = "s2"
        s._name = "Rocket vehicle"
        s._tex_pad = "rock_pad"
        s._tex_pad_dmg = "rock_damage", "rock_damage_mono"
        s._tex_turret = "rock_turret", "rock_turret_mono"
        s._tex_canon = "rock_cannon", "rock_cannon_mono"
        s._tex_track = "track"
        s._width = 24
        s._height = 32
        s._mass = 32
        s._speed = 30
        s._acc = 1
        s._dec = 1
        s._turn_speed = 10
        s._look_speed = 16
        s._health = 1300
        s._range = 350
        s._range_scan = 450
        s._range_sight = 450
        s._armor = {
            ShotSpec.get("tank"): 800,
            ShotSpec.get("mini_vehicle"): 1400,
            ShotSpec.get("mini_jeep"): 1400,
            ShotSpec.get("mini_troop"): 1400,
            ShotSpec.get("heat_shot"): 1400,
            ShotSpec.get("sniper"): 1200
        }
        s._shot_effect = "minirocket"
        s._shot_spec = ShotSpec.get("minirocket")
        s._shot_count = 2
        s._shot_reload = 121
        s._shot_mid_reload = 10
        s._shot_force_canon = -5
        s._shot_force_turret = -2
        s._shot_canon_dist = 4
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_plasma")
        s._side_type = "s2"
        s._name = "Plasma tank"
        s._tex_pad = "plasma_pad"
        s._tex_pad_dmg = "plasma_damage", "plasma_damage_mono"
        s._tex_turret = "plasma_turret", "plasma_turret_mono"
        s._tex_canon = "plasma_cannon", "plasma_cannon_mono"
        s._tex_track = "track"
        s._width = 26
        s._height = 39
        s._mass = 38
        s._speed = 20
        s._acc = 1
        s._dec = 1
        s._turn_speed = 5
        s._look_speed = 7
        s._health = 1800
        s._range = 350
        s._range_scan = 450
        s._range_sight = 450
        s._armor = {
            ShotSpec.get("mini_vehicle"): 500,
            ShotSpec.get("mini_jeep"): 500,
            ShotSpec.get("mini_troop"): 500,
            ShotSpec.get("heat_shot"): 500,
            ShotSpec.get("tank"): 1200,
            ShotSpec.get("sniper"): 500
        }
        s._shot_effect = "bigboom"
        s._shot_spec = ShotSpec.get("beam_shot")
        s._shot_reload = 150
        s._shot_mid_reload = 0
        s._shot_force_canon = -14
        s._shot_force_turret = -5
        s._track_opacity = 0.2

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_partilery")
        s._side_type = "s2"
        s._name = "Artilery tank"
        s._tex_pad = "partilery_pad"
        s._tex_pad_dmg = "partilery_damage", "partilery_damage_mono"
        s._tex_turret = "partilery_turret", "partilery_turret_mono"
        s._tex_canon = "partilery_cannon", "partilery_cannon_mono"
        s._tex_track = "track2"
        s._width = 28
        s._height = 39
        s._mass = 35
        s._speed = 30
        s._acc = 1
        s._dec = 2
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._health = 800
        s._range = 500
        s._range_scan = 0
        s._range_sight = 500
        s._armor = {
            ShotSpec.get("mini_vehicle"): 500,
            ShotSpec.get("mini_jeep"): 500,
            ShotSpec.get("mini_troop"): 500,
            ShotSpec.get("heat_shot"): 500,
            ShotSpec.get("tank"): 1200,
            ShotSpec.get("sniper"): 500
        }
        s._shot_canon_dist = 20
        s._shot_effect = "blueboom"
        s._shot_spec = ShotSpec.get("blueshot")
        s._shot_reload = 150
        s._shot_mid_reload = 0
        s._shot_force_canon = -24
        s._shot_force_turret = -10
        s._track_opacity = 0.2

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_miner")
        s._side_type = "s2"
        s._name = "Harvester"
        s._tex_pad = "s2_miner_pad"
        s._tex_pad_full = "s2_miner_pad_full"
        s._tex_pad_dmg = "s2_miner_pad", "s2_miner_pad_mono"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_track = "track"
        s._target_priority = 1
        s._width = 24
        s._height = 39
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 4
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._specials = [SpecialFlags.SPECIAL_MINER]
        s._health = 3000
        s._range = 400
        s._range_scan = 800
        s._range_sight = 400
        s._shot_spec = None
        s._shot_effect = None
        s._shot_count = 0
        s._track_opacity = 0.15
        s._max_quantity = 5

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_upgrade")
        s._side_type = "s2"
        s._name = "Upgrade vehicle"
        s._tex_pad = "s2_upgrade_pad"
        s._tex_pad_full = "s2_upgrade_pad_full"
        s._tex_pad_dmg = None, "s2_upgrade_pad_mono"
        s._tex_turret = None, None
        s._tex_canon = None, None
        s._tex_track = "track"
        s._target_priority = 1
        s._width = 35
        s._height = 44
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 4
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._specials = [SpecialFlags.SPECIAL_UPGRADE]
        s._health = 1500
        s._range = 400
        s._range_scan = 400
        s._range_sight = 400
        s._shot_spec = None
        s._shot_effect = None
        s._shot_count = 0
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

        s = UnitSpec("s2_builder")
        s._side_type = "s2"
        s._name = "Build vehicle"
        s._tex_pad = "s2_builder_pad"
        s._tex_pad_dmg = None, "s2_upgrade_pad_mono"
        s._tex_turret = "s2_builder_turret", "s2_builder_turret_mono"
        s._tex_canon = None, None
        s._tex_track = "track"
        s._target_priority = 1
        s._width = 20
        s._height = 40
        s._mass = 35
        s._speed = 40
        s._acc = 2
        s._dec = 4
        s._turn_speed = 8
        s._look_speed = 8
        s._free_look = False
        s._look_when_stand = True
        s._specials = [SpecialFlags.SPECIAL_BUILD]
        s._health = 1500
        s._range = 400
        s._range_scan = 400
        s._range_sight = 400
        s._shot_spec = None
        s._shot_effect = None
        s._shot_count = 0
        s._track_opacity = 0.15

        UnitSpec._specs[s.get_key()] = s
        UnitSpec._specs_by_num[s.number] = s

    def to_string(self):
        out = ""
        out += self._key + "\t"
        out += str(self._health) + "\t"
        out += str(self._build_money_cost) + "\t"
        out += str(self._build_delay) + "\t"
        out += str(self._speed) + "\t"
        out += str(self._range) + "\t"
        out += str(self._shot_spec.get_damage()) + "\t"
        out += str(self._shot_spec.get_key()) + "\t"
        out += str(self._shot_count) + "\t"
        out += str(self._shot_mid_reload) + "\t"
        out += str(self._shot_reload) + "\t"
        out += str(self.get_damage_per_second()) + "\t"
        out += str(self.get_damage_till_die()) + "\t"
        out += str(self.get_damage_per_cost()) + "\t"
        out += str(self.get_delay_per_health()) + "\t"

        return out

    @staticmethod
    def header_to_string():
        out = ""
        out += "\t"
        out += "health" + "\t"
        out += "build_money_cost" + "\t"
        out += "build_delay" + "\t"
        out += "speed" + "\t"
        out += "range" + "\t"
        out += "damage" + "\t"
        out += "shot_type" + "\t"
        out += "shot_count" + "\t"
        out += "shot_mid_reload" + "\t"
        out += "shot_reload" + "\t"
        out += "damage_per_second" + "\t"
        out += "damage_til_die" + "\t"
        out += "damage_per_cost" + "\t"
        out += "delay_per_health" + "\t"

        return out

    @staticmethod
    def disable_attack():
        for key in UnitSpec._specs.keys():
            UnitSpec._specs[key]._shot_count = 0

    @staticmethod
    def get(key):
        return UnitSpec._specs[key]

    @staticmethod
    def get_by_number(num):
        return UnitSpec._specs_by_num[num]

    @staticmethod
    def get_all():
        list = []

        for key in UnitSpec._specs.keys():
            list.append(UnitSpec.get(key))

        return list

    @staticmethod
    def get_all_by_type(tp):
        list = []

        for key in UnitSpec._specs.keys():
            spec = UnitSpec.get(key)

            if spec.get_side_type() == tp:
                list.append(spec)

        return list

    @staticmethod
    def log_table():
        Logger.get().log(UnitSpec.header_to_string())

        for c in UnitSpec.get_all():
            Logger.get().log(c.to_string())
