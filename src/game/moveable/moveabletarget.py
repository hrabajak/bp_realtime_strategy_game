import random

from game.helper.distangle import DistAngle
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.iview import IView
from game.map.pathfinderflow import PathFinderFlow
from game.moveable.unitspec import UnitSpec
from game.particle.particlecollector import ParticleCollector
from game.shot.shotcollector import ShotCollector
from game.shot.shotrealize import ShotRealize
from game.sound.soundcollector import SoundCollector


class MoveableTarget:

    TARGET_DEFAULT = 0
    TARGET_AUTO = 1
    TARGET_DAMAGED = 2

    _view: IView = None
    _shots: IShots = None
    _particles: IParticles = None
    _sounds: ISounds = None

    _parent = None
    _spec: UnitSpec = None
    _target: ITargetable = None
    _target_fallback: ITargetable = None
    _target_focus = None
    _target_goto = None
    _target_goto_tick = 0
    _target_type = 0
    _target_back_place = None
    _target_angle_dist = None
    _target_idle = 0

    _is_hold = False
    _damaged_by = None

    _shots_lock = False
    _shots_count = 0
    _shots_timeout = 0
    _shots_wait_timeout = 0
    _sound_timeout = 0

    def __init__(self, parent):
        self._view = IView.get()
        self._shots: IShots = ShotCollector()
        self._particles: IParticles = ParticleCollector()
        self._sounds: ISounds = SoundCollector()

        self._parent = parent
        self._spec = parent.get_spec()
        self._shots_lock = False
        self._shots_count = self._spec.get_shot_count()
        self._target_angle_dist = DistAngle()
        self._target_type = MoveableTarget.TARGET_DEFAULT
        self._target_idle = 0
        self._damaged_by = []
        self._is_hold = self._parent.get_side().get_is_ai()

    def set_target_focus(self, target_focus: ITargetable, target_type=TARGET_DEFAULT):
        self._target_focus = target_focus
        self._target_type = target_type

        if self._target_type == MoveableTarget.TARGET_AUTO and self._target_back_place is None:
            # !! self._target_back_place = self._parent.get_place() !! radeji nic !!
            self._target_back_place = None

        elif self._target_type != MoveableTarget.TARGET_AUTO:
            self._target_back_place = None

    def set_target_fallback(self, target_fallback):
        self._target_fallback = target_fallback

    def set_target_back_place(self, place):
        self._target_back_place = place

    def set_target_type(self, target_type):
        self._target_type = target_type

    def set_is_hold(self, is_hold):
        if self._parent.get_side().get_is_ai():
            self._is_hold = True
            return False

        elif self._is_hold != is_hold:
            self._is_hold = is_hold
            return True

        else:
            return False

    def add_damaged_by(self, ref):
        if ref not in self._damaged_by:
            self._damaged_by.append(ref)

    def get_shots_lock(self):
        return self._shots_lock

    def get_target(self) -> ITargetable:
        return self._target

    def get_target_focus(self) -> ITargetable:
        return self._target_focus

    def get_target_type(self):
        return self._target_type

    def get_is_targeting(self):
        return self._target is not None or self._target_focus is not None

    def get_is_hold(self):
        return self._is_hold

    def release_target_focus(self):
        self._target_focus = None

    def release_target(self):
        self._target = None
        self._target_focus = None
        self._target_goto = None

    def release_shots_lock(self):
        self._shots_lock = False

    def release_lock(self):
        self._shots_lock = False

        if not self._spec.get_free_look():
            self._target = None

    def serialize(self):
        out = {
            'shots_lock': self._shots_lock,
            'shots_count': self._shots_count,
            'shots_timeout': self._shots_timeout,
            'shots_wait_timeout': self._shots_wait_timeout,
            'target_number': self._target.number if self._target is not None else -1,
            'target_focus_number': self._target_focus.number if self._target_focus is not None else -1,
            'is_targeting': self.get_is_targeting()
        }

        return out

    def update(self, tick):

        use_tick = tick + self._parent.number

        if self._shots_wait_timeout > 0:
            self._shots_wait_timeout -= 1

            if self._shots_wait_timeout == 0:
                self._shots_lock = False
                self._parent.idle_reset()
                self._parent.update_body(shot=1)

        elif self._shots_timeout > 0:
            self._shots_timeout -= 1

        if self._sound_timeout > 0:
            self._sound_timeout -= 1

        if self._target is not None:

            bef_target = self._target

            if not self._target.get_is_alive() or (self._target_type == MoveableTarget.TARGET_DAMAGED and tick % 15 == 0):
                self._target = None
                self.scan_targets()

            if self._target is not None and self._target.dist_to_moveable(self._parent) <= self._spec.get_range():

                if self._spec.get_free_look() or self._spec.get_look_when_stand():
                    # je mozne se volne rozhlizet

                    if self._spec.get_look_when_stand() and self._parent.get_is_moving():
                        pass
                    
                    else:
                        if self._parent.get_look_snap() is not None:
                            self._parent.set_look_to_coords(*self._parent.get_look_snap())
                        else:
                            self._parent.set_look_to_target(self._target)

                        if self._parent.get_is_look_focus() and self._shots_count > 0 and self._shots_timeout == 0 and self._shots_wait_timeout == 0 and not self._parent.get_look_snap():
                            self._parent.update_body(shot=2)
                            self._shot_to_target()

                elif not self._parent.get_is_moving() and not self._spec.get_free_look():
                    # otaceni za cilem vcetne podstavce

                    self._target_angle_dist.set_places(self._parent.get_place(), self._target.get_target_place(*self._parent.get_coords_on_place()))
                    btw = self._parent.rotate_to_diff(self._target_angle_dist.get_angle_deg())

                    if btw > 0:
                        self._parent.rotate_step_to(self._target_angle_dist.get_angle_deg())

                    elif self._shots_count > 0 and self._shots_timeout == 0 and self._shots_wait_timeout == 0:
                        self._parent.idle_reset()
                        self._parent.update_body(shot=2)
                        self._shot_to_target()

            else:
                self._target = None
                self._target_goto = None
                self._target_idle = 60
                self._shots_wait_timeout = self._spec.get_shot_cooldown()

                # hledani dalsich cilu
                bef_back_place = self._target_back_place

                if self._target is None:
                    self.scan_attack_targets()

                self._target_back_place = bef_back_place

                if self._target is None and bef_target is not None and self._parent.get_attack_place() is not None:

                    grp = self._parent.get_goto_group()
                    flw = grp.get_path_flow()

                    if flw is not None:
                        grp.prepare(path_flow=flw, rel_object=self._parent.get_attack_place())
                        flw.rem_targetable(bef_target)
                        flw.realize_light(PathFinderFlow.REALIZE_ATTACK_MOVE, self._parent.get_attack_place(), True)
                    else:
                        self._parent.attack_move(self._parent.get_attack_place(), create_goto_group=False)

        elif self._target_focus is not None:
            if self._target_focus.get_is_alive():

                grp = self._parent.get_goto_group()
                flw = grp.get_path_flow()

                if flw is not None and flw.check_targetables_change() and flw.get_realize_type() in [PathFinderFlow.REALIZE_ATTACK, PathFinderFlow.REALIZE_ATTACK_MOVE]:
                    grp.prepare(path_flow=flw, rel_object=self._target_focus)
                    flw.realize_light(-1, flw.get_realize_rel_object(), True)

            else:
                self._target_focus = None
                self._target_goto = None

                if self._target_back_place is not None:
                    self._parent.stop(reflow=True)
                else:
                    self._parent.stop()

        if use_tick % self._get_tick_scan() == 0:

            if self._target_focus is not None or self._parent.get_attack_place() is not None:
                # zadan prikaz k utoku
                self.scan_targets()

            elif self._spec.get_free_look():
                # jednotka utoci za pohybu
                self.scan_targets()

            elif not self._parent.get_is_moving():
                # jednotka utoci pouze kdyz stoji
                self.scan_targets()

        if not self._is_hold and self._spec.get_range_scan() > 0 and self._target_focus is None and self._target is None and self._parent.get_attack_place() is None:
            if use_tick % self._get_tick_scan_long() == 0:
                self.scan_targets_damaged()

                if self._target is None and self._target_focus is None:
                    self.scan_goto_attack_targets()

        """
        if self._target is None and not self._parent.get_is_moving() and self._target_back_place is not None:
            if use_tick % 80 == 0:
                # zpatky na puvodni policko z automatickeho utoku

                self.scan_attack_targets()

                if self._target_focus is not None:
                    pass
                elif self._target_back_place is self._parent.get_place():
                    self._target_back_place = None
                #else:
                    #self._parent.goto(self._target_back_place, create_goto_group=False)
        """

        if self._target_idle > 0 and not self._parent.get_is_moving() and self._target is None:
            self._target_idle -= 1

            if self._target_idle == 0:
                self._parent.unset_actions()

    def _get_tick_scan(self):
        if self._target_focus is None and self._parent.get_attack_place() is None:
            return 30
        else:
            return 10

    def _get_tick_scan_long(self):
        if self._parent.get_attack_place() is None:
            return 80
        else:
            return 10

    def _shot_to_target(self):
        sr = ShotRealize(self._parent, self._target, self._spec)
        c = sr.make(self._shots_count)

        if "sound_timeout" in c:
            self._sound_timeout = c["sound_timeout"]

        self._parent.set_force_canon(self._spec.get_shot_force_canon())
        self._parent.set_force_turret(self._spec.get_shot_force_turret())
        self._shots_count -= 1

        if self._spec.get_shot_cooldown() > 0:
            self._shots_lock = True

        if self._shots_count == 0:
            self._shots_timeout = self._spec.get_shot_reload() + self._spec.get_shot_mid_reload()
            self._shots_count = self._spec.get_shot_count()
            self._shots_wait_timeout = self._spec.get_shot_cooldown()

        else:
            self._shots_timeout = self._spec.get_shot_mid_reload()

    @staticmethod
    def _find_moveable(mm_self, mm_loop: ITargetable, mm_found: ITargetable):
        if mm_loop is not mm_self and mm_loop.get_is_alive() and mm_self.get_side() is not mm_loop.get_side():

            dist = mm_loop.dist_to_moveable(mm_self)
            pri = mm_loop.get_target_priority()

            if mm_found is None or (mm_found['dist'] > dist and mm_found['pri'] == pri) or mm_found['pri'] < pri:
                mm_found = {
                    'ref': mm_loop,
                    'dist': dist,
                    'pri': pri
                }

        return mm_found

    def scan_targets_damaged(self):
        if not self._parent.get_is_moving():
            min_dist = -1
            min_trg = None

            for trg in self._damaged_by:
                dist = trg.dist_to_moveable(self._parent)

                if trg.get_is_alive() and (min_dist == -1 or dist < min_dist):
                    min_trg = trg
                    min_dist = dist

            if min_trg is not None:
                self._parent.goto_attack(min_trg, target_type=MoveableTarget.TARGET_DAMAGED, create_goto_group=False, keep_attack_place=True, strict=True)

        self._damaged_by.clear()

    def scan_targets(self):
        if not self._spec.get_attack_able():
            pass

        elif self._target_focus is not None:
            trg = self._target_focus

            if trg.get_is_alive() and trg.dist_to_moveable(self._parent) <= self._spec.get_range():
                self._target = trg
                self._target_idle = 0
                self._parent.idle_reset()

                if self._parent.get_is_moving():
                    self._parent.stop(reflow=True)

        elif self._target is None or self._target.get_target_priority() in [0, 1]:
            c = self._parent.get_tile_area().scan_targetables_on_tiles(self._spec.get_range_tiles() + 1, lambda mm, result: MoveableTarget._find_moveable(self._parent, mm, result))

            if c is not None and c['ref'].dist_to_moveable(self._parent) <= self._spec.get_range():
                self._target = c['ref']
                self._target_type = MoveableTarget.TARGET_DEFAULT
                self._target_idle = 0
                self._parent.idle_reset()

                if self._parent.get_attack_place() is not None and self._parent.get_is_moving():
                    self._parent.stop(reflow=True)

                if self._parent.get_attack_place() is not None:
                    flw = self._parent.get_goto_group().get_path_flow()

                    if flw is not None and flw.add_targetable(c['ref']):
                        flw.set_realize_force()

        return self._target is not None

    def scan_attack_targets(self):
        if not self._spec.get_attack_able():
            pass

        elif self._parent.get_is_moving() and self._parent.get_attack_place() is None:
            pass

        elif (self._target is None or self._target.get_target_priority() in [0, 1]) and self._parent.get_attack_place() is not None:
            c = self._parent.get_tile_area().scan_targetables_on_tiles(self._spec.get_range_scan_tiles() + 1, lambda mm, result: MoveableTarget._find_moveable(self._parent, mm, result))

            if c is not None:
                dst = c['ref'].dist_to_moveable(self._parent)

                if dst <= self._spec.get_range_scan() and dst >= self._spec.get_range():
                    flw = self._parent.get_goto_group().get_path_flow()

                    if flw is not None and flw.add_targetable(c['ref']):
                        flw.set_realize_force()

        """
        if self._parent.get_attack_place() is None:
            if not self._parent.get_is_moving():
                self._parent.goto_attack(c['ref'], create_goto_group=False, is_auto_attack=True, keep_attack_place=True, strict=True)
        else:
        """

    def scan_goto_attack_targets(self):
        if not self._spec.get_attack_able():
            pass

        elif not self._parent.get_is_moving() and self._parent.get_attack_place() is None:

            if self._target is None or self._target.get_target_priority() in [0, 1]:
                c = self._parent.get_tile_area().scan_targetables_on_tiles(self._spec.get_range_scan_tiles() + 1, lambda mm, result: MoveableTarget._find_moveable(self._parent, mm, result))

                if c is not None:
                    dst = c['ref'].dist_to_moveable(self._parent)
                    attack_do = False

                    if c['ref'].get_target() is not None:
                        # nepratelska jednotka utoci - reakce hned
                        attack_do = True

                    elif dst > self._spec.get_range_scan():
                        self._target_goto = None

                    elif self._target_goto is None or c['ref'] is not self._target_goto:
                        self._target_goto = c['ref']
                        self._target_goto_tick = 2

                    elif self._target_goto_tick > 0:
                        self._target_goto_tick -= 1

                    else:
                        attack_do = True

                    if attack_do:
                        grp = self._parent.get_goto_group()
                        flw = grp.get_path_flow()

                        if flw is not None and False:
                            pass
                            """
                            # !! BLBOST !!
                            grp.prepare(path_flow=flw, rel_object=c['ref'])

                            if flw.get_is_ready():
                                flw.invalidate_finish()
                                flw.prepare(None)
                                flw.clear_targetables()
                                flw.add_targetable(c['ref'])
                                flw.realize(PathFinderFlow.REALIZE_ATTACK, c['ref'])

                            else:
                                flw.clear_targetables()
                                flw.add_targetable(c['ref'])
                                flw.realize_light(PathFinderFlow.REALIZE_ATTACK, c['ref'])

                            self._parent.goto_attack(c['ref'], MoveableTarget.TARGET_AUTO, create_goto_group=False, keep_attack_place=True, path_flow=flw)
                            """

                        else:
                            self._parent.goto_attack(c['ref'], MoveableTarget.TARGET_AUTO, create_goto_group=False, keep_attack_place=True, strict=True)

                else:
                    self._target_goto = None

    def scan_target_check(self):
        c = self._parent.get_tile_area().scan_targetables_on_tiles(self._spec.get_range_scan_tiles() + 1, lambda mm, result: MoveableTarget._find_moveable(self._parent, mm, result))

        if c is not None and c['ref'].dist_to_moveable(self._parent) <= self._spec.get_range_scan():
            return c['ref']
        else:
            return None
