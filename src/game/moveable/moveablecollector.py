from collections import deque, OrderedDict

from game.helper.heapqueue import HeapQueue
from game.helper.singleton import Singleton
from game.interface.imoveables import IMoveables
from game.interface.iplayersides import IPlayerSides
from game.interface.itilecollector import ITileCollector
from game.interface.iview import IView
from game.map.mapscanner import MapScanner
from game.moveable.gotogroup import GotoGroup
from game.moveable.moveableitem import MoveableItem
from game.map.pathfinder import PathFinder
from game.moveable.moveablepath import MoveablePath
from game.moveable.unitspec import UnitSpec
from lib.heap import BinaryHeap


class MoveableCollector(IMoveables, metaclass=Singleton):

    _tiles: ITileCollector = None
    _sides: IPlayerSides = None
    _view: IView = None
    _map_scr: MapScanner = None
    _path_fnd: PathFinder = None

    _childs = None
    _childs_hash = None
    _childs_hp = None
    _flows_hp = None
    _path_max = 0

    _x = 0
    _y = 0
    _zoom = 1.0

    def __init__(self):
        IMoveables.__init__(self)
        IMoveables.set(self)

        self._childs = []
        self._childs_hash = OrderedDict()
        self._childs_hp = BinaryHeap()
        self._flows_hp = deque()
        self._path_max = 0

    def set_refs(self, tiles, sides, view):
        self._tiles = tiles
        self._sides = sides
        self._view = view
        self._map_scr = MapScanner(self._tiles)
        self._path_fnd = PathFinder(self._tiles)

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_zoom(self):
        return self._zoom

    def add_moveable(self, side, spec: UnitSpec, x: int, y: int, set_number=-1):
        return self.add_moveable_place(side, spec, self._tiles.get_tile(x, y).get_place(), set_number)

    def add_moveable_place(self, side, spec: UnitSpec, place, set_number=-1):
        mm = MoveableItem(side, spec, self._path_fnd, self._map_scr, set_number)
        mm.init(place)

        self._childs.append(mm)
        self._childs_hash[mm.number] = mm

        return mm

    def get_moveable_at_coord(self, x: float, y: float, target_side=None):
        for mm in self._childs:
            sz = mm.get_sprite_size() / 2

            if x >= mm.get_sprite_x() - sz and x <= mm.get_sprite_x() + sz and y <= mm.get_sprite_y() + sz and y >= mm.get_sprite_y() - sz:
                if target_side is None or mm.get_side() is target_side:
                    return mm

        return None

    def get_moveable_at_range(self, sx: float, sy: float, ex: float, ey: float, target_side=None):
        out = []

        for mm in self._childs:
            sz = mm.get_sprite_size() / 2

            if mm.get_sprite_x() + sz >= sx and mm.get_sprite_x() - sz <= ex and mm.get_sprite_y() + sz >= sy and mm.get_sprite_y() - sz <= ey:
                if target_side is None or mm.get_side() is target_side:
                    out += [mm]

        return out

    def get_moveable_by_number(self, number: int):
        if number in self._childs_hash:
            return self._childs_hash[number]

        return None

    def get_childs(self):
        return self._childs

    def serialize(self):
        out = {
            'number_total': MoveableItem.number_total,
            'list': []
        }

        for mm in self._childs:
            out['list'].append(mm.serialize())

        return out

    def unserialize(self, dt):
        for c in dt['list']:
            side = self._sides.get_side_by_number(c['side_number'])
            side.add_unit(self._tiles.get_place(c['place_x'], c['place_y']), UnitSpec.get(c['spec']), c['number'])

        MoveableItem.number_total = dt['number_total']

    def clear(self):
        for ch in self._childs:
            ch.destroy()

        while self._flows_hp:
            self._flows_hp.popleft()

        self._childs = []
        MoveableItem.number_total = 0

    def update(self, tick):
        rem = []

        for flw in [g.get_path_flow() for g in GotoGroup._refs if g.get_path_flow() is not None]:
            if not flw.get_is_ready() and flw not in self._flows_hp:
                self._flows_hp.append(flw)

        #max_steps = 2000
        #max_steps = 400
        #max_steps = 5
        #max_steps = 800
        max_steps = 600

        while self._flows_hp and max_steps > 0:
            flw = self._flows_hp.popleft()
            max_steps = flw.update_realize(max_steps)

        #max_num = 40
        max_num = 30

        while self._childs_hp and max_num > 0:
            ch = self._childs_hp.pop()

            if ch[1].path_update:
                ch[1].get_path().update_seek(tick)
                ch[1].path_update = False
                max_num -= 1

        for grp in [g for g in GotoGroup._refs]:
            for ch in grp.get_childs_iterate():
                flag = ch.update(tick)

                if flag == 0:
                    rem.append(ch)

                elif flag == 1 and not ch.path_update:
                    self._childs_hp.push(self._path_max, ch)
                    self._path_max += 1
                    ch.path_update = True

        if len(rem) > 0:
            for ch in rem:
                ch.destroy()
                ch.path_update = False
                self._childs.remove(ch)
                del self._childs_hash[ch.number]

    def draw(self):
        self._x, self._y, self._zoom = self._view.get_view_vals()
