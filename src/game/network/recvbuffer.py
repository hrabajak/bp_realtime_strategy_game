import base64
import errno
import socket
import struct

from game.logger import Logger
from game.network.command import Command


class RecvBuffer:

    _buff = None
    _sep = chr(3).encode('ascii')

    def __init__(self):
        self._buff = ''.encode('ascii')

    @staticmethod
    def get_sep():
        return RecvBuffer._sep

    @staticmethod
    def encode(input):
        return base64.b64encode(input.encode('ascii'))

    @staticmethod
    def encode_sep(input):
        b = base64.b64encode(input.encode('ascii'))
        b += RecvBuffer._sep

        return b

    @staticmethod
    def encode_sep_bytes(input):
        b = base64.b64encode(input)
        b += RecvBuffer._sep

        return b

    @staticmethod
    def decode(input):
        return base64.b64decode(input)

    def recv_from_socket(self, sck):
        recv_len = 0
        to_recv_len = struct.calcsize('!I')

        while recv_len == 0:
            try:
                tmp = sck.recv(to_recv_len)

                if not tmp:
                    # ocekavaji se data
                    return False

                elif len(tmp) == to_recv_len:
                    recv_len = struct.unpack('!I', tmp)[0]
                else:
                    recv_len = 0

            except socket.error as err:
                if err.errno == errno.EWOULDBLOCK or err.errno == errno.EAGAIN:
                    pass
                else:
                    Logger.get().logm("(error) Recv error: " + str(err))
                    return False

        if recv_len > 0:
            recv_data = b''

            while len(recv_data) < recv_len:
                try:
                    tmp = sck.recv(min(recv_len - len(recv_data), 1024))
                    recv_data += tmp

                except socket.error as err:
                    if err.errno == errno.EWOULDBLOCK or err.errno == errno.EAGAIN:
                        pass
                    else:
                        Logger.get().logm("(error) Recv error: " + str(err))
                        return False

            self.append(recv_data)

        return True

    def append(self, inp):
        self._buff += inp

    def pop(self):
        idx = self._buff.find(RecvBuffer._sep)

        if idx != -1:
            to_dec = self._buff[:idx]
            #print("DEC LEN: " + str(len(to_dec)) + " | " + str(len(self._buff)))

            frag = RecvBuffer.decode(to_dec)
            self._buff = self._buff[idx + 1:]
            return frag
        else:
            return None

    def pop_command_all(self):
        out = []

        while True:
            frag = self.pop()

            if frag is not None:
                cm = Command(frag)
                [out.append(c) for c in cm.pop_all()]
            else:
                break

        out.sort(key=lambda x: x[1])

        return out
