import errno
import struct

from game.logger import Logger
from game.network.recvbuffer import RecvBuffer


class SendBuffer:

    _buff = None

    def __init__(self):
        self._buff = b''

    def append_command(self, cm):
        self._buff += RecvBuffer.encode_sep_bytes(cm.get_buffer())

    def send_to_socket(self, sck):
        if len(self._buff) > 0:
            to_send = struct.pack('!I', len(self._buff)) + self._buff
            send_qa = 0

            while send_qa < len(to_send):
                try:
                    send_qa += sck.send(to_send[send_qa:])

                except IOError as err:
                    if err.errno == errno.EWOULDBLOCK or err.errno == errno.EAGAIN:
                        pass
                    else:
                        Logger.get().logm("(error) Send error: " + str(err))
                        break

            self._buff = b''
