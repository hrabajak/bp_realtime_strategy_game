import pickle
import struct


class Command:

    T_NONE = 0
    T_SYNC = 1
    T_ENDSYNC = 2
    T_ERROR = 3
    T_CHAT = 4
    T_GAME_START = 5
    T_GAME_START_SINGLE = 6
    T_GAME_GET = 7
    T_GAME_CONTEXT = 8
    T_GAME_LIST = 9
    T_GAME_READY = 10
    T_STATE_CHECK = 11
    T_ACK = 12
    T_ENDACK = 13
    T_QUIT = 14
    T_ACTION = 15
    T_COORDS = 16
    T_NUMBER = 17
    T_MV_GOTO = 18
    T_MV_ATTGOTO = 19
    T_MV_ATTACK = 20
    T_MV_HOLD = 21
    T_MV_STOP = 22
    T_BUILD_DESTROY = 23
    T_TRAVEL_PLACE = 24
    T_ADD_UNIT_COORD = 25
    T_ACTION_ENQUEUE = 26
    T_ACTION_BUILD = 27
    T_ACTION_REFUND = 28
    T_ACTION_CANCEL = 29
    T_MV_CHECK = 30

    sort_num = 0

    _cmds = {
        T_SYNC: ('Idd', ['ticks', 'time', 'latency']),
        T_ENDSYNC: ('I', ['ticks']),
        T_ACK: ('d', ['time']),
        T_ENDACK: ('', []),
        T_ERROR: ('Is', ['length', 'bytes']),
        T_CHAT: ('Is', ['length', 'bytes']),
        T_GAME_START: ('Is', ['length', 'bytes']),
        T_GAME_START_SINGLE: ('Is', ['length', 'bytes']),
        T_GAME_GET: ('I', ['number']),
        T_GAME_CONTEXT: ('Is', ['length', 'bytes']),
        T_GAME_LIST: ('Is', ['length', 'bytes']),
        T_GAME_READY: ('II', ['ctx', 'waiting_count']),
        T_STATE_CHECK: ('IIs', ['tick', 'length', 'bytes']),
        T_QUIT: ('', []),
        T_ACTION: ('II', ['ctx', 'ticks']),
        T_COORDS: ('II', ['x', 'y']),
        T_NUMBER: ('II', ['type', 'number']),
        T_MV_GOTO: ('IIII', ['ctx', 'x', 'y', 'count']),
        T_MV_ATTGOTO: ('IIII', ['ctx', 'x', 'y', 'count']),
        T_MV_ATTACK: ('IIII', ['ctx', 'type', 'number', 'count']),
        T_MV_HOLD: ('II', ['ctx', 'count']),
        T_MV_STOP: ('II', ['ctx', 'count']),
        T_BUILD_DESTROY: ('II', ['ctx', 'number']),
        T_TRAVEL_PLACE: ('IIII', ['ctx', 'x', 'y', 'count']),
        T_ADD_UNIT_COORD: ('IIIII', ['ctx', 'x', 'y', 'spec_number', 'side_number']),
        T_ACTION_ENQUEUE: ('IIIIIi', ['ctx', 'side_number', 'action_number', 'build_number', 'build_trg_number', 'money_slot']),
        T_ACTION_BUILD: ('IIIIII', ['ctx', 'side_number', 'action_item_number', 'build_spec_number', 'x', 'y']),
        T_ACTION_REFUND: ('III', ['ctx', 'side_number', 'action_item_number']),
        T_ACTION_CANCEL: ('III', ['ctx', 'side_number', 'action_item_number']),
        T_MV_CHECK: ('IIIII', ['ctx', 'tick', 'number', 'x', 'y'])
    }
    _buffer = None

    def __init__(self, buffer=b''):
        self._buffer = buffer

    def get_buffer(self):
        return self._buffer

    @staticmethod
    def get_sort_num():
        s = Command.sort_num
        Command.sort_num += 1
        return s

    def append_error(self, sort_num, message):
        dump = pickle.dumps(message)
        self.append(Command.T_ERROR, sort_num, length=len(dump), bytes=dump)
        return self

    def append(self, tp, sort_num, **kwargs):
        if tp in [Command.T_ERROR, Command.T_CHAT]:
            self._buffer += struct.pack('!BII' + 'B' * kwargs['length'], tp, sort_num, kwargs['length'], *kwargs['bytes'])

        elif tp in [Command.T_GAME_START, Command.T_GAME_START_SINGLE, Command.T_GAME_CONTEXT, Command.T_GAME_LIST]:
            self._buffer += struct.pack('!BII' + 'B' * kwargs['length'], tp, sort_num, kwargs['length'], *kwargs['bytes'])

        elif tp == Command.T_STATE_CHECK:
            self._buffer += struct.pack('!BIII' + 'B' * kwargs['length'], tp, sort_num, kwargs['length'], kwargs['tick'], *kwargs['bytes'])

        elif tp in self._cmds:
            self._buffer += struct.pack('!BI' + self._cmds[tp][0], tp, sort_num, *[kwargs[k] for k in self._cmds[tp][1]])

        return self

    def append_tuple(self, item):
        if item[0] in [Command.T_ERROR, Command.T_CHAT]:
            self._buffer += struct.pack('!BII' + 'B' * item[1], *item)

        elif item[0] in [Command.T_GAME_START, Command.T_GAME_START_SINGLE, Command.T_GAME_CONTEXT, Command.T_GAME_LIST]:
            self._buffer += struct.pack('!BII' + 'B' * item[1], *item)

        elif item[0] == Command.T_STATE_CHECK:
            self._buffer += struct.pack('!BIII' + 'B' * item[1], *item)

        elif item[0] in self._cmds:
            self._buffer += struct.pack('!BI' + self._cmds[item[0]][0], *item)

        return self

    def append_buffer(self, buffer):
        self._buffer += buffer

    def pop(self):
        out = None

        if len(self._buffer) > 0:
            ln = 1
            tp = struct.unpack('!B', self._buffer[:ln])[0]

            if tp in [Command.T_ERROR, Command.T_CHAT]:
                ln = struct.calcsize('!BII')
                tmp = struct.unpack('!BII', self._buffer[:ln])
                frm = '!BII' + 'B' * tmp[2]
                ln = struct.calcsize(frm)
                out = struct.unpack(frm, self._buffer[:ln])
                out = (tp, tmp[1], pickle.loads(bytes(out[3:])))

            elif tp in [Command.T_GAME_START, Command.T_GAME_START_SINGLE, Command.T_GAME_CONTEXT, Command.T_GAME_LIST]:
                ln = struct.calcsize('!BII')
                tmp = struct.unpack('!BII', self._buffer[:ln])
                frm = '!BII' + 'B' * tmp[2]
                ln = struct.calcsize(frm)
                out = struct.unpack(frm, self._buffer[:ln])

            elif tp == Command.T_STATE_CHECK:
                ln = struct.calcsize('!BII')
                tmp = struct.unpack('!BII', self._buffer[:ln])
                frm = '!BIII' + 'B' * tmp[2]
                ln = struct.calcsize(frm)
                out = struct.unpack(frm, self._buffer[:ln])

            elif tp in self._cmds:
                ln = struct.calcsize('!BI' + self._cmds[tp][0])
                out = struct.unpack('!BI' + self._cmds[tp][0], self._buffer[:ln])

            self._buffer = self._buffer[ln:]

        return out

    def pop_all(self):
        out = []

        while True:
            c = self.pop()

            if c is not None:
                out.append(c)
            else:
                break

        out.sort(key=lambda x: x[1])
        #print([(c[0], c[1]) for c in out])

        return out
