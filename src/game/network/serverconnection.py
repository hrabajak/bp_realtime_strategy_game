import pickle
import random
import select
import threading
import time

from game.gamestat.gamecontext import GameContext
from game.gamestat.gamespec import GameSpec
from game.interface.ievents import IEvents
from game.logger import Logger
from game.network.sendbuffer import SendBuffer
from game.network.command import Command
from game.network.recvbuffer import RecvBuffer


class ServerConnection(IEvents):

    _thread = None
    _parent = None
    _addr = None
    _sck = None
    _ctx = None
    _ctx_corrupt = False

    _cond_ntf = False
    _cond = None

    _send_time = 0
    _send_latency = 0

    _commands = None
    _events = None
    _logic_loop = False
    _cmds = None

    def __init__(self, parent, sck, addr):
        IEvents.__init__(self)

        self._parent = parent
        self._sck = sck
        self._addr = addr
        self._ctx = None
        self._ctx_corrupt = False

        self._cond_ntf = False
        self._cond = threading.Condition(threading.Lock())

        self._commands = []
        self._events = []
        self._cmds = []

    def start_wait(self):

        self._thread = threading.Thread(target=ServerConnection.connection_logic, args=(self,))
        self._thread.start()

        time.sleep(1 / 30)

        while not self._logic_loop:
            time.sleep(1 / 30)

    def get_send_latency(self):
        return self._send_latency

    def get_ctx_number(self):
        return -1 if self._ctx is None else self._ctx.number

    def get_is_alive(self):
        return self._logic_loop

    def set_game_context(self, ctx):
        with self._cond:
            self._ctx = ctx
            self._ctx_corrupt = False

    def add_command(self, c: Command):
        with self._cond:
            self._commands.append(c)

    def add_command_rest(self):
        with self._cond:
            self._cond_ntf = True
            self._cond.notify()

    def event_add(self, evt_type, evt_data):
        with self._cond:
            self._events.append((evt_type, evt_data))

    def event_get(self):
        with self._cond:
            if len(self._events) > 0:
                evt = self._events[0]
                self._events = self._events[1:]
                yield evt

        return None

    def connection_logic(self):
        Logger.get().logm('(server-conn) Client connected ' + str(self._addr))

        self._sck.setblocking(False)

        sb = SendBuffer()
        rb = RecvBuffer()

        lst_read = [self._sck]
        lst_write = []
        lst_except = [self._sck]
        timeout = 0

        self._logic_loop = True

        while self._logic_loop:
            with self._cond:
                self._cond.wait(timeout=2)

                if self._cond_ntf:
                    self._send_time = time.time()
                    [sb.append_command(c) for c in self._commands]
                    self._commands.clear()
                    self._cond_ntf = False

            # odeslani prikazu na klienta
            sb.send_to_socket(self._sck)

            # cekani max 5 sec na reply
            try:
                r = select.select(lst_read, lst_write, lst_except, 5)
            except select.error:
                self._logic_loop = False
                break

            if r[2]:
                self._logic_loop = False

            elif r[0]:
                # prijem response od klienta
                if not rb.recv_from_socket(self._sck):
                    self._logic_loop = False

                else:
                    # zpracovani response
                    timeout = 0

                    for item in rb.pop_command_all():
                        if item[0] == Command.T_ACK:
                            self._cmds.append(item)

                        elif item[0] == Command.T_ENDACK:
                            self._cmds.append(item)
                            self._realize_commands()
                            self._cmds.clear()

                        else:
                            self._cmds.append(item)

                    if self._ctx is not None and not self._ctx_corrupt and self._ctx.get_is_corrupted():
                        if self._ctx.get_is_desync():
                            self._commands.append(Command().append_error(Command.get_sort_num(), 'Out of sync error!'))
                        else:
                            self._commands.append(Command().append_error(Command.get_sort_num(), 'The connection is lost - one of the players has disconnected!'))

                        self._ctx_corrupt = True

                    for evt in self.event_get():
                        if evt[0] == Command.T_GAME_START:
                            ctx = self._parent.game_context_get(evt[1]['ctx_number'])

                            if ctx is not None:
                                dump = {'ctx_number': ctx.number}
                                dump = pickle.dumps(dump)

                                cmd = Command()
                                cmd.append(Command.T_GAME_START, Command.get_sort_num(), length=len(dump), bytes=dump)

                                self._commands.append(cmd)

                                dump = {'game_context_list': self._parent.game_context_list_get()}
                                dump = pickle.dumps(dump)

                                cmd = Command()
                                cmd.append(Command.T_GAME_LIST, Command.get_sort_num(), length=len(dump), bytes=dump)

                                self._commands.append(cmd)

                            else:
                                self._commands.append(Command().append_error(Command.get_sort_num(), 'Cannot get context `' + str(evt[1]['ctx_number']) + '`!'))

            else:
                timeout += 0.5

                if timeout > 5:
                    self._logic_loop = False

        Logger.get().logm('(server-conn) Client disconnected: ' + str(self._addr))
        self._parent.connection_drop(self)

    def _realize_commands(self):
        cn_list = []
        mv_check_list = []

        for item in self._cmds:
            if item[0] == Command.T_ACK:
                self._send_latency = time.time() - self._send_time

            elif item[0] == Command.T_ENDACK:
                self._parent.add_commands_with_ack(self.get_ctx_number(), cn_list)
                cn_list.clear()

            elif item[0] == Command.T_QUIT:
                Logger.get().logm('(server-conn) Client quit triggered')
                self._logic_loop = False
                break

            elif item[0] == Command.T_GAME_GET:

                self._ctx = self._parent.game_context_get(item[2], assign_conn=self)
                self._ctx_corrupt = False

                if self._ctx is None:
                    self._commands.append(Command().append_error(Command.get_sort_num(), 'No such game context `' + str(item[2]) + '`!'))

                elif self._ctx.get_side_by_connection(self) is None:
                    self._commands.append(Command().append_error(Command.get_sort_num(), 'Cannot pick player side!'))

                else:
                    Logger.get().logm('(server-conn) Game context ' + str(item[2]) + ' get')

                    dump = pickle.dumps(self._ctx.serialize(self))

                    cmd = Command()
                    cmd.append(Command.T_GAME_CONTEXT, Command.get_sort_num(), length=len(dump), bytes=dump)

                    self._commands.append(cmd)

            elif item[0] == Command.T_GAME_READY:

                ctx = self._parent.game_context_get(item[2])

                if ctx is None:
                    self._commands.append(Command().append_error(Command.get_sort_num(), 'No such game context `' + str(item[2]) + '`!'))

                elif ctx.get_side_by_connection(self) is None:
                    self._commands.append(Command().append_error(Command.get_sort_num(), 'Cannot pick player side!'))

                elif not ctx.connection_set_ready(self):
                    self._commands.append(Command().append_error(Command.get_sort_num(), 'Cannot set connection as ready!'))

                else:
                    Logger.get().logm('(server-conn) Game context ' + str(item[2]) + ' waiting count ' + str(ctx.get_waiting_count()))

                    cn_list.append(Command().append(Command.T_GAME_READY, Command.get_sort_num(), ctx=ctx.number, waiting_count=ctx.get_waiting_count()))

            elif item[0] == Command.T_CHAT:
                self._parent.add_chat_message(item[2])

            elif item[0] == Command.T_GAME_START:

                dump = bytes(item[3:])
                dump = pickle.loads(dump)

                ctx = self._parent.game_context_create(GameSpec().unserialize(dump['game_spec']))
                ctx.init(self)

            elif item[0] == Command.T_GAME_LIST:

                dump = {'game_context_list': self._parent.game_context_list_get()}
                dump = pickle.dumps(dump)

                cmd = Command()
                cmd.append(Command.T_GAME_LIST, Command.get_sort_num(), length=len(dump), bytes=dump)

                self._commands.append(cmd)

            elif item[0] == Command.T_STATE_CHECK:
                dump = bytes(item[4:])
                dump = pickle.loads(dump)

                self._parent.add_state_to_check(item[3], dump['number'], dump)

            elif item[0] == Command.T_MV_CHECK:
                mv_check_list.append(item[2:])

            else:
                n_item = list(item)
                n_item[1] = Command.get_sort_num()
                cn_list.append(Command().append_tuple(tuple(n_item)))

        if len(mv_check_list) > 0:
            self._parent.add_moveables_to_check(mv_check_list)
