import pickle
import select
import socket
import threading
from collections import OrderedDict

from deepdiff import DeepDiff
from game.config import Config
from game.gamestat.gamespec import GameSpec
from game.helper.frametimer import FrameTimer
from game.logger import Logger
from game.network.command import Command
from game.network.serverconnection import ServerConnection
from game.gamestat.gamecontext import GameContext


class Server:
    _thread = None
    _sck = None
    _lock = None
    _commands = None
    _logic_loop = False
    _connections = None
    _contexts = None
    _chat_messages = None
    _state_to_check = None
    _moveables_to_check = None

    _fr_timer = None
    _tick = 0
    _tick_action_ahead = 0

    def __init__(self):
        self._connections = []
        self._contexts = []
        self._chat_messages = []
        self._state_to_check = {}
        self._moveables_to_check = {}

    def start(self):
        self._lock = threading.Lock()
        self._commands = OrderedDict()

        self._sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sck.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sck.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self._sck.bind((Config.get().get_listen_host(), Config.get().get_port()))
        self._sck.listen()
        self._sck.setblocking(False)

        self._logic_loop = True

        self._thread = threading.Thread(target=Server.server_logic, args=(self,))
        self._thread.start()

    def server_logic(self):
        Logger.get().logm('(server) Server started at ' + Config.get().get_listen_host() + ':' + str(Config.get().get_port()))

        lst_read = [self._sck]
        lst_write = []
        lst_except = []

        self._fr_timer = FrameTimer(ticks_qa=FrameTimer.DEFAULT_TICKS // FrameTimer.DEFAULT_SERVER_TICKSTEP)
        self._fr_timer.update_sleep_minimal_avg()

        self._tick = 0
        self._tick_action_ahead = FrameTimer.DEFAULT_SERVER_TICKSTEP * 1

        use_int = 1 / 100

        while self._logic_loop:
            # !! r = select.select(lst_read, lst_write, lst_except, use_int)
            r = select.select(lst_read, lst_write, lst_except, 0)

            self._fr_timer.start()

            if r[0]:
                conn, addr = self._sck.accept()

                cn = ServerConnection(self, conn, addr)
                self.connection_add(cn)
                cn.start_wait()

            with self._lock:
                if len(self._connections) > 0:
                    self._tick += FrameTimer.DEFAULT_SERVER_TICKSTEP

                    # print('ack ' + str(self._tick))

                    for con in (c for c in self._connections if c.get_is_alive()):
                        cmd = Command()
                        cmd.append(Command.T_SYNC, Command.get_sort_num(), ticks=self._tick, time=self._fr_timer.get_time(), latency=con.get_send_latency())

                        if con.get_ctx_number() in self._commands and len(self._commands[con.get_ctx_number()]) > 0:
                            cmd.append(Command.T_ACTION, Command.get_sort_num(), ctx=con.get_ctx_number(), ticks=self._tick + self._tick_action_ahead)

                            for cn in self._commands[con.get_ctx_number()]:
                                tcn = Command()
                                tcn.append_buffer(cn.get_buffer())

                                for scn in tcn.pop_all():
                                    n_item = list(scn)
                                    n_item[1] = Command.get_sort_num()
                                    cmd.append_tuple(tuple(n_item))

                        cmd.append(Command.T_ENDSYNC, Command.get_sort_num(), ticks=self._tick)
                        con.add_command(cmd)

                    self._commands.clear()

                    for con in (c for c in self._connections if c.get_is_alive()):
                        for dt in self._chat_messages:
                            dump = pickle.dumps(dt)
                            con.add_command(Command().append(Command.T_CHAT, Command.get_sort_num(), length=len(dump), bytes=dump))

                        con.add_command_rest()

                    self._chat_messages.clear()

                elif len(self._connections) > 0:
                    # vsechny spojeni jeste nedostaly acknowledge od klientu
                    pass

            # !! use_int = self._fr_timer.end_sleep_get()
            self._fr_timer.end_sleep()

        self._sck.close()
        Logger.get().logm('(server) Server stop')

    def game_context_create(self, spec: GameSpec):
        with self._lock:
            Logger.get().logm('(server) Creating game context')

            ctx = GameContext(spec)

            self._contexts.append(ctx)

            return ctx

    def game_context_get(self, ctx_number, assign_conn=None):
        with self._lock:
            ctx = next((c for c in self._contexts if c.number == ctx_number and not c.get_started_flag()), None)

            if ctx is not None and assign_conn is not None:
                ctx.connection_assign(assign_conn)

            return ctx

    def game_context_list_get(self):
        return [c.get_info() for c in self._contexts]

    def add_commands_with_ack(self, ctx_number, cn_list):
        with self._lock:
            if ctx_number not in self._commands:
                self._commands[ctx_number] = []

            for cn in cn_list:
                self._commands[ctx_number].append(cn)

    def connection_add(self, cn):
        with self._lock:
            self._connections.append(cn)

    def connection_drop(self, cn):
        with self._lock:
            self._connections.remove(cn)

            for cx in [c for c in self._contexts]:
                cx.connection_leave(cn)

                if cx.get_is_dead():
                    self._contexts.remove(cx)

    def add_chat_message(self, dt):
        with self._lock:
            self._chat_messages.append(dt)

    def add_state_to_check(self, tick, ctx_number, state):
        with self._lock:
            if ctx_number not in self._state_to_check:
                self._state_to_check[ctx_number] = {}

            for bef_tick in [c for c in self._state_to_check[ctx_number].keys()]:
                if bef_tick < tick:
                    del self._state_to_check[ctx_number][bef_tick]

            if tick not in self._state_to_check[ctx_number]:
                self._state_to_check[ctx_number][tick] = []

            self._state_to_check[ctx_number][tick].append(state)

            if len(self._state_to_check[ctx_number][tick]) >= 2:

                states_match = True

                for idx in range(1, len(self._state_to_check[ctx_number][tick])):
                    dd = DeepDiff(self._state_to_check[ctx_number][tick][0], self._state_to_check[ctx_number][tick][idx])

                    if len(dd) > 0:
                        states_match = False
                        print(dd)

                if not states_match:
                    Logger.get().logm('(server) state check: MISMATCH!')
                else:
                    Logger.get().logm('(server) state check: match')

    def add_moveables_to_check(self, mv_list):
        with self._lock:
            ctx_number = mv_list[0][0]
            tick = mv_list[0][1]

            if ctx_number not in self._moveables_to_check:
                self._moveables_to_check[ctx_number] = {}

            for bef_tick in [c for c in self._moveables_to_check[ctx_number].keys()]:
                if bef_tick < tick:
                    del self._moveables_to_check[ctx_number][bef_tick]

            if tick not in self._moveables_to_check[ctx_number]:
                self._moveables_to_check[ctx_number][tick] = []

            self._moveables_to_check[ctx_number][tick].append(mv_list)

            if len(self._moveables_to_check[ctx_number][tick]) >= 2:
                for idx in range(1, len(self._moveables_to_check[ctx_number][tick])):
                    dd = DeepDiff(self._moveables_to_check[ctx_number][tick][0], self._moveables_to_check[ctx_number][tick][idx])

                    if len(dd) > 0:
                        Logger.get().logm('(server) mv state check: MISMATCH! Details: ' + str(dd))
                        ctx = next((c for c in self._contexts if c.number == ctx_number), None)

                        if ctx is not None:
                            ctx.set_desync()
