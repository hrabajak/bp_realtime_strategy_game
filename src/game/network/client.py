import pickle
import random
import select
import socket
import threading
import time
from collections import OrderedDict

from game.config import Config
from game.helper.singleton import Singleton
from game.interface.ievents import IEvents
from game.logger import Logger
from game.network.sendbuffer import SendBuffer
from game.network.command import Command
from game.network.recvbuffer import RecvBuffer


class Client(IEvents, metaclass=Singleton):
    _thread = None
    _sck = None
    _lock = None

    _commands = None
    _events = None
    _actions = None
    _cmds = None
    _logic_loop = False

    _is_connected = False
    _tick = 0
    _latency = 0
    _next_tick = 0

    def __init__(self):
        IEvents.__init__(self)

        self._lock = threading.Lock()
        self._commands = []
        self._events = []
        self._actions = []
        self._cmds = []

    def connect(self):
        c_host = Config.get().get_connect_host()
        c_port = Config.get().get_port()

        Logger.get().logm("(client) connecting to " + str(c_host) + ":" + str(c_port))

        self._sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sck.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self._sck.connect((c_host, c_port))
        self._sck.setblocking(False)

        self._thread = threading.Thread(target=Client.client_logic, args=(self,))
        self._thread.start()

    def disconnect(self):
        if self._thread is not None:
            self._logic_loop = False
            self._is_connected = False
            self._thread.join()
            self._thread = None
            self._commands.clear()

    def connect_wait(self):
        try:
            self.connect()

            time.sleep(1 / 30)

            while not self._is_connected:
                time.sleep(1 / 30)

            return True, ""

        except socket.error as msg:
            self._logic_loop = False
            self._is_connected = False
            return False, str(msg)

    def get_is_connected(self):
        return self._is_connected

    def get_tick(self):
        return self._tick

    def get_latency(self):
        return self._latency

    def add_command(self, c: Command):
        with self._lock:
            self._commands.append(c)

    def event_add(self, evt_type, evt_data):
        with self._lock:
            self._events.append((evt_type, evt_data))

    def event_get(self):
        with self._lock:
            if len(self._events) > 0:
                evt = self._events[0]
                self._events = self._events[1:]
                yield evt

        return None

    def get_actions(self, tick):
        cmds = None

        with self._lock:
            while True:
                if len(self._actions) == 0:
                    break

                elif self._actions[0]['tick'] < tick:
                    print("!! LATE ACTION TICK !! " + str(self._actions[0]['tick']) + ' | ' + str(tick))
                    self._actions = self._actions[1:]

                elif self._actions[0]['tick'] == tick:
                    for c in self._actions[0]['list']:
                        if cmds is None:
                            cmds = []

                        cmds.append(c)

                    self._actions = self._actions[1:]

                else:
                    break

            if cmds is not None:
                cmds.sort(key=lambda x: x[1])

        return cmds

    tst = 0

    def client_logic(self):

        sb = SendBuffer()
        rb = RecvBuffer()

        lst_read = [self._sck]
        lst_write = []
        lst_except = [self._sck]
        timeout = 0
        interval = 1 / 2
        is_error = False

        self.tst = random.randint(1, 666666)

        self._is_connected = False
        self._logic_loop = True

        while self._logic_loop:
            r = select.select(lst_read, lst_write, lst_except, interval)

            if r[2]:
                self._logic_loop = False
                self.event_add(Command.T_ERROR, {'error': 'disconnected from server'})
                is_error = True

            elif r[0]:
                if not rb.recv_from_socket(self._sck):
                    self._logic_loop = False
                    self.event_add(Command.T_ERROR, {'error': 'disconnected from server'})
                    is_error = True

                else:
                    with self._lock:
                        timeout = 0

                        for item in rb.pop_command_all():
                            if item[0] == Command.T_SYNC:
                                self._cmds.append(item)

                            elif item[0] == Command.T_ENDSYNC:
                                self._cmds.append(item)
                                self._realize_commands(sb)
                                self._cmds.clear()

                            else:
                                self._cmds.append(item)

                    sb.send_to_socket(self._sck)

            else:
                timeout += 1

                if timeout * interval > 5:
                    self._logic_loop = False

        self._is_connected = False

        if not is_error:
            cn = Command()
            cn.append(Command.T_QUIT, Command.get_sort_num())

            sb.append_command(cn)
            sb.send_to_socket(self._sck)

        Logger.get().logm("(client) disconnecting")

        self._sck.close()

    def _realize_commands(self, sb):
        evts = []
        add_ac = None

        for item in self._cmds:
            if item[0] == Command.T_SYNC:

                self._tick = item[2]
                self._latency = item[4]
                self._is_connected = True

                # print(self._tick)
                # print('Sync recv: ' + str(item))
                # time.sleep(1)

                cn = Command()
                cn.append(Command.T_ACK, Command.get_sort_num(), time=time.time())

                for add_cn in self._commands:
                    cn.append_buffer(add_cn.get_buffer())
                self._commands.clear()

                cn.append(Command.T_ENDACK, Command.get_sort_num())
                sb.append_command(cn)

            elif item[0] == Command.T_ERROR:
                evts.append((Command.T_ERROR, {'error': item[2]}))

            elif item[0] in [Command.T_GAME_START, Command.T_GAME_CONTEXT, Command.T_GAME_LIST]:
                dump = bytes(item[3:])
                dump = pickle.loads(dump)

                evts.append((item[0], {'dump': dump}))

            elif item[0] == Command.T_GAME_READY:
                evts.append((item[0], {'number': item[2], 'waiting_count': item[3]}))

            elif item[0] == Command.T_CHAT:
                evts.append((item[0], item[2]))

            elif item[0] == Command.T_ACTION:
                add_ac = {
                    'tick': item[3],
                    'list': []
                }

            elif item[0] == Command.T_ENDSYNC:

                if add_ac is not None:
                    self._actions.append(add_ac)

                add_ac = None

            elif add_ac is not None:
                add_ac['list'].append(item)

        [self._events.append(evt) for evt in evts]

        return evts
