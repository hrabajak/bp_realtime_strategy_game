import math
import random

from game.helper.distangle import DistAngle
from game.image.imagecollector import ImageCollector
from game.interface.iimages import IImages
from game.interface.iparticles import IParticles
from game.const.specialflags import SpecialFlags


class BuildSpec:

    _specs = {}
    _specs_by_num = {}
    _key = None
    _key_base = None
    _side_type = None
    _name = None
    _health = 0
    _special = None
    _special_places = None
    _action_keys = []
    _turret_keys = []
    _effects = None
    _tex = None
    _tex_damage1 = None
    _tex_damage2 = None
    _tex_below = None
    _tex_dead = None
    _tex_ico = None
    _tex_ico_round = None
    _tiles_width = None
    _tiles_height = None
    _safe_range = 1
    _place_range = 3
    _place_seek = True
    _is_turret = False
    _allow_nbr = []
    _allow_place_travel = False
    _allow_destroy = True
    _random_rotate = False
    _upgrade_max = 0
    _max_quantity = 0
    _target_priority = 1

    number = 0
    _number_total = 0

    def __init__(self, key):
        self._key = key
        self._key_base = key[3:]
        self._effects = []

        self.number = BuildSpec._number_total
        BuildSpec._number_total += 1

    def get_key(self):
        return self._key

    def get_key_base(self):
        return self._key_base

    def get_side_type(self):
        return self._side_type

    def get_name(self):
        return self._name

    def get_health(self):
        return self._health

    def get_special(self):
        return self._special

    def get_special_places(self):
        return self._special_places

    def get_action_keys(self):
        return self._action_keys

    def get_turret_keys(self):
        return self._turret_keys

    def get_effects(self):
        return self._effects

    def get_tex(self, color=None):
        return ImageCollector().get_image_color(self._tex, color)

    def get_tex_damage1(self, color=None):
        return ImageCollector().get_image_color(self._tex_damage1, color)

    def get_tex_damage2(self, color=None):
        return ImageCollector().get_image_color(self._tex_damage2, color)

    def get_tex_below(self, color=None):
        return ImageCollector().get_image_color(self._tex_below, color)

    def get_tex_dead(self):
        return ImageCollector().get_image_color(self._tex_dead)

    def get_tex_ico(self):
        return None if self._tex_ico is None else ImageCollector().get_image(self._tex_ico)

    def get_tex_ico_round(self):
        return None if self._tex_ico_round is None else ImageCollector().get_image(self._tex_ico_round)

    def get_tex_load(self, color):
        self.get_tex(color)
        self.get_tex_damage1(color)
        self.get_tex_damage2(color)
        self.get_tex_below(color)
        self.get_tex_dead()

    def get_tiles_width(self):
        return self._tiles_width

    def get_tiles_height(self):
        return self._tiles_height

    def get_safe_range(self):
        return self._safe_range

    def get_place_range(self):
        return self._place_range

    def get_place_seek(self):
        return self._place_seek

    def get_is_turret(self):
        return self._is_turret

    def get_allow_nbr(self):
        return self._allow_nbr

    def get_allow_place_travel(self):
        return self._allow_place_travel

    def get_allow_destroy(self):
        return self._allow_destroy

    def get_random_rotate(self):
        return self._random_rotate

    def get_upgrade_max(self):
        return self._upgrade_max

    def get_max_quantity(self):
        return self._max_quantity

    def get_target_priority(self):
        return self._target_priority

    @staticmethod
    def init_specs(imgs: IImages):
        # === side 1 ===

        s = BuildSpec("s1_main")
        s._side_type = "s1"
        s._name = "Construction base"
        s._health = 10000
        s._tex = "s1_main"
        s._tex_damage1 = "s1_main_damage1"
        s._tex_damage2 = "s1_main_damage2"
        s._tex_below = "s1_main_below"
        s._tex_dead = "s1_main_damage2_mono"
        s._tex_ico = "gui_b_main"
        s._tex_ico_round = "gui_round_main"
        s._action_keys = ["s1_refinery", "s1_barracks", "s1_factory", "s1_lab", "s1_turret", "s1_turret_can"]
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 2, -2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 2, 0, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 2, 2, {"angle_dist": DistAngle().set_diff(0, 1)}))
        s._special_places.append((SpecialFlags.SPECIAL_UPGRADE, -2, -2, {"angle_dist": DistAngle().set_diff(-1, 0)}))
        s._tiles_width = 2
        s._tiles_height = 2
        s._upgrade_max = 1
        s._allow_destroy = False

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s1_factory")
        s._side_type = "s1"
        s._name = "Factory"
        s._health = 10000
        s._tex = "s1_factory"
        s._tex_damage1 = "s1_factory_damage1"
        s._tex_damage2 = "s1_factory_damage2"
        s._tex_below = "s1_factory_below"
        s._tex_dead = "s1_factory_damage2_mono"
        s._tex_ico = "gui_b_factory"
        s._tex_ico_round = "gui_round_factory"
        s._action_keys = ["s1_jeep", "s1_plate", "s1_lighttank", "s1_medtank", "s1_rocket", "s1_miner"]
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 0, -2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 0, 0, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 0, 2, {"angle_dist": DistAngle().set_diff(0, 1)}))
        s._special_places.append((SpecialFlags.SPECIAL_UPGRADE, 4, -2, {"angle_dist": DistAngle().set_diff(1, 0)}))
        s._tiles_width = 2
        s._tiles_height = 2
        s._allow_place_travel = True
        s._upgrade_max = 3

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s1_barracks")
        s._side_type = "s1"
        s._name = "Barracks"
        s._health = 5000
        s._tex = "s1_barracks"
        s._tex_damage1 = "s1_barracks_damage1"
        s._tex_damage2 = "s1_barracks_damage2"
        s._tex_below = "s1_barracks_below"
        s._tex_dead = "s1_barracks_damage2_mono"
        s._tex_ico = "gui_b_barracks"
        s._tex_ico_round = "gui_round_barracks"
        s._action_keys = ["s1_troop", "s1_sniper", "s1_troop_cannon"]
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 0, -1, {"angle_dist": DistAngle().set_diff(1, 0), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 2, -1, {"angle_dist": DistAngle().set_diff(1, 0), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 3, 0, {"angle_dist": DistAngle().set_diff(1, 0), "inc_x": 5}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 3, 1, {"angle_dist": DistAngle().set_diff(0, 1)}))
        s._special_places.append((SpecialFlags.SPECIAL_UPGRADE, -2, 0, {"angle_dist": DistAngle().set_diff(-1, 0)}))
        s._tiles_width = 2
        s._tiles_height = 1
        s._allow_place_travel = True
        s._upgrade_max = 2

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s1_refinery")
        s._side_type = "s1"
        s._name = "Refinery"
        s._health = 10000
        s._tex = "s1_refinery"
        s._tex_damage1 = "s1_refinery_damage1"
        s._tex_damage2 = "s1_refinery_damage2"
        s._tex_below = "s1_refinery_below"
        s._tex_dead = "s1_refinery_damage2_mono"
        s._tex_ico = "gui_b_rafinery"
        s._tex_ico_round = "gui_round_rafinery"
        s._special = "refinery"
        s._special_places = [(SpecialFlags.SPECIAL_MINER, 2, 0, {"angle_dist": DistAngle().set_diff(0, 1)})]
        s._action_keys = []
        s._tiles_width = 2
        s._tiles_height = 2
        s._effects = []
        s._max_quantity = 3

        def _ptcallback1(particles: IParticles, x, y):
            pt = particles.add_particle(x, y, 'smoke3', -1, scale=1.1 - random.random() * 0.3)

        s._effects.append({"x": -15, "y": 30, "callback": _ptcallback1, "freq": 50, "freq_offset": 0, "freq_rand": 55})
        s._effects.append({"x": -15, "y": 0, "callback": _ptcallback1, "freq": 50, "freq_offset": 0, "freq_rand": 55})

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s1_lab")
        s._side_type = "s1"
        s._name = "Laboratory"
        s._health = 10000
        s._tex = "s1_lab"
        s._tex_damage1 = "s1_lab_damage1"
        s._tex_damage2 = "s1_lab_damage2"
        s._tex_below = "s1_lab_below"
        s._tex_dead = "s1_lab_damage2_mono"
        s._tex_ico = "gui_b_laboratory"
        s._tex_ico_round = "gui_round_laboratory"
        s._action_keys = ["s1_upgrade"]
        s._tiles_width = 2
        s._tiles_height = 2
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 2, -2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": -10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 2, 0, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": -10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 2, 2, {"angle_dist": DistAngle().set_diff(0, 1)}))

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s1_turret")
        s._side_type = "s1"
        s._name = "Minigun turret"
        s._health = 2500
        s._tex = "s1_tower"
        s._tex_damage1 = "s1_tower"
        s._tex_damage2 = "s1_tower"
        s._tex_below = None
        s._tex_dead = "s1_tower_mono"
        s._action_keys = []
        s._turret_keys = ["s1_minigun"]
        s._allow_nbr = ["s1_turret", "s1_turret_can"]
        s._special_places = []
        s._tiles_width = 1
        s._tiles_height = 1
        s._upgrade_max = 0
        s._target_priority = 2
        s._place_range = 5
        s._place_seek = False
        s._is_turret = True

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s1_turret_can")
        s._side_type = "s1"
        s._name = "Cannon turret"
        s._health = 3500
        s._tex = "s1_tower"
        s._tex_damage1 = "s1_tower"
        s._tex_damage2 = "s1_tower"
        s._tex_below = None
        s._tex_dead = "s1_tower_mono"
        s._action_keys = []
        s._turret_keys = ["s1_cannon"]
        s._allow_nbr = ["s1_turret", "s1_turret_can"]
        s._special_places = []
        s._tiles_width = 1
        s._tiles_height = 1
        s._upgrade_max = 0
        s._target_priority = 2
        s._place_range = 5
        s._place_seek = False
        s._is_turret = True

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        # === side 2 ===

        s = BuildSpec("s2_main")
        s._side_type = "s2"
        s._name = "Construction base"
        s._health = 10000
        s._tex = "s2_main"
        s._tex_damage1 = "s2_main_damage1"
        s._tex_damage2 = "s2_main_damage2"
        s._tex_below = "s2_main_below"
        s._tex_dead = "s2_main_damage2_mono"
        s._tex_ico = "gui_b_main"
        s._tex_ico_round = "gui_round_main"
        s._action_keys = ["s2_refinery", "s2_barracks", "s2_factory", "s2_lab", "s2_turret", "s2_turret_rock"]
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 0, -2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 15}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 0, 0, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 15}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 0, 2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 15}))
        s._special_places.append((SpecialFlags.SPECIAL_UPGRADE, -2, -2, {"angle_dist": DistAngle().set_diff(-1, 0)}))
        s._tiles_width = 2
        s._tiles_height = 2
        s._upgrade_max = 1
        s._allow_destroy = False

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s2_factory")
        s._side_type = "s2"
        s._name = "Factory"
        s._health = 10000
        s._tex = "s2_factory"
        s._tex_damage1 = "s2_factory_damage1"
        s._tex_damage2 = "s2_factory_damage2"
        s._tex_below = "s2_factory_below"
        s._tex_dead = "s2_factory_damage2_mono"
        s._tex_ico = "gui_b_factory"
        s._tex_ico_round = "gui_round_factory"
        s._action_keys = ["s2_basic", "s2_mini", "s2_rock", "s2_plasma", "s2_partilery", "s2_miner"]
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 0, -2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 0, 0, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 0, 2, {"angle_dist": DistAngle().set_diff(0, 1)}))
        s._special_places.append((SpecialFlags.SPECIAL_UPGRADE, 4, -2, {"angle_dist": DistAngle().set_diff(1, 0)}))
        s._tiles_width = 2
        s._tiles_height = 2
        s._allow_place_travel = True
        s._upgrade_max = 3

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s2_barracks")
        s._side_type = "s2"
        s._name = "Barracks"
        s._health = 5000
        s._tex = "s2_barracks"
        s._tex_damage1 = "s2_barracks_damage1"
        s._tex_damage2 = "s2_barracks_damage2"
        s._tex_below = "s2_barracks_below"
        s._tex_dead = "s2_barracks_damage2_mono"
        s._tex_ico = "gui_b_barracks"
        s._tex_ico_round = "gui_round_barracks"
        s._action_keys = ["s2_troop", "s2_troop_beam", "s2_troop_rocket"]
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 0, -2, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 0, 0, {"angle_dist": DistAngle().set_diff(0, 1), "inc_x": 10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, 1, 1, {"angle_dist": DistAngle().set_diff(0, 1)}))
        s._special_places.append((SpecialFlags.SPECIAL_UPGRADE, 0, -4, {"angle_dist": DistAngle().set_diff(0, -1)}))
        s._tiles_width = 1
        s._tiles_height = 2
        s._allow_place_travel = True
        s._upgrade_max = 2

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s2_refinery")
        s._side_type = "s2"
        s._name = "Refinery"
        s._health = 10000
        s._tex = "s2_refinery"
        s._tex_damage1 = "s2_refinery_damage1"
        s._tex_damage2 = "s2_refinery_damage2"
        s._tex_below = "s2_refinery_below"
        s._tex_dead = "s2_refinery_damage2_mono"
        s._tex_ico = "gui_b_rafinery"
        s._tex_ico_round = "gui_round_rafinery"
        s._special = "refinery"
        s._special_places = [(SpecialFlags.SPECIAL_MINER, 2, 0, {"angle_dist": DistAngle().set_diff(1, 0)})]
        s._action_keys = []
        s._tiles_width = 2
        s._tiles_height = 2
        s._max_quantity = 3

        def _ptcallback1(particles: IParticles, x, y):
            pt = particles.add_particle(x, y, 'smoke3', -1, scale=0.7 - random.random() * 0.1)

        s._effects.append({"x": -30, "y": -13, "callback": _ptcallback1, "freq": 150, "freq_offset": 0, "freq_rand": 0})
        s._effects.append({"x": -30, "y": -26, "callback": _ptcallback1, "freq": 150, "freq_offset": 50, "freq_rand": 0})
        s._effects.append({"x": -30, "y": -39, "callback": _ptcallback1, "freq": 150, "freq_offset": 100, "freq_rand": 0})

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s2_lab")
        s._side_type = "s2"
        s._name = "Laboratory"
        s._health = 10000
        s._tex = "s2_lab"
        s._tex_damage1 = "s2_lab_damage1"
        s._tex_damage2 = "s2_lab_damage2"
        s._tex_below = "s2_lab_below"
        s._tex_dead = "s2_lab_damage2_mono"
        s._tex_ico = "gui_b_laboratory"
        s._tex_ico_round = "gui_round_laboratory"
        s._action_keys = ["s2_upgrade"]
        s._tiles_width = 2
        s._tiles_height = 2
        s._special_places = []
        s._special_places.append((SpecialFlags.SPECIAL_CREATE, 2, -2, {"angle_dist": DistAngle().set_diff(1, 0), "inc_y": -10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_MOVE, 0, -2, {"angle_dist": DistAngle().set_diff(1, 0), "inc_y": -10}))
        s._special_places.append((SpecialFlags.SPECIAL_CREATE_FINAL, -2, -2, {"angle_dist": DistAngle().set_diff(1, 0)}))

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s2_turret")
        s._side_type = "s2"
        s._name = "Laser turret"
        s._health = 2500
        s._tex = "s2_tower"
        s._tex_damage1 = "s2_tower"
        s._tex_damage2 = "s2_tower"
        s._tex_below = None
        s._tex_dead = "s2_tower_mono"
        s._action_keys = []
        s._turret_keys = ["s2_laser"]
        s._allow_nbr = ["s2_turret", "s2_turret_rock"]
        s._special_places = []
        s._tiles_width = 1
        s._tiles_height = 1
        s._upgrade_max = 0
        s._target_priority = 2
        s._place_range = 5
        s._place_seek = False
        s._is_turret = True

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

        s = BuildSpec("s2_turret_rock")
        s._side_type = "s2"
        s._name = "Rocket turret"
        s._health = 3500
        s._tex = "s2_tower"
        s._tex_damage1 = "s2_tower"
        s._tex_damage2 = "s2_tower"
        s._tex_below = None
        s._tex_dead = "s2_tower_mono"
        s._action_keys = []
        s._turret_keys = ["s2_rocket"]
        s._allow_nbr = ["s2_turret", "s2_turret_rock"]
        s._special_places = []
        s._tiles_width = 1
        s._tiles_height = 1
        s._upgrade_max = 0
        s._target_priority = 2
        s._place_range = 5
        s._place_seek = False
        s._is_turret = True

        BuildSpec._specs[s.get_key()] = s
        BuildSpec._specs_by_num[s.number] = s

    @staticmethod
    def get(key):
        return BuildSpec._specs[key]

    @staticmethod
    def get_by_number(num):
        return BuildSpec._specs_by_num[num]

    @staticmethod
    def get_all_by_type(tp):
        list = []

        for key in BuildSpec._specs.keys():
            spec = BuildSpec.get(key)

            if spec.get_side_type() == tp:
                list.append(spec)

        return list
