from game.build.buildcollector import BuildCollector
from game.interface.ibuilds import IBuilds
from game.interface.iselectable import ISelectable
from game.interface.itilecollector import ITileCollector
from game.map.mapscanner import MapScanner


class BuildAction:

    _builds: IBuilds = None
    _map_scr: MapScanner = None

    _childs = None

    def __init__(self):
        self._builds = BuildCollector()
        self._map_scr = MapScanner(ITileCollector.get())

        self._childs = []

    def add(self, ref):
        if isinstance(ref, list):
            for s_ref in ref:
                if s_ref is not None and s_ref.get_selected_type() == ISelectable.SELECTED_BUILD:
                    self._childs.append(s_ref)

        elif ref is not None and ref.get_selected_type() == ISelectable.SELECTED_BUILD:
            self._childs.append(ref)

    def set_place_travel(self, place_travel):
        if place_travel is not None and len(self._childs) > 0:

            for bref in self._childs:
                bref.set_place_travel(place_travel)
