from collections import deque

from game.build.buildspec import BuildSpec
from game.interface.itilecollector import ITileCollector
from game.player.playerside import PlayerSide


cdef class BuildPlace:

    cdef:
        object _tile_col
        object _side
        object _spec

        long _x
        long _y

        object _build_tile
        object _build_tiles
        object _build_safe_tiles
        object _build_nb_tiles

    def __cinit__(self, side, spec):
        self._side = side
        self._spec = spec
        self._tile_col = ITileCollector.get()

        self._build_tile = None
        self._build_tiles = None
        self._build_safe_tiles = None
        self._build_nb_tiles = None

    def get_side(self):
        return self._side

    def get_spec(self):
        return self._spec

    def get_tile(self):
        return self._build_tile

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def set_tile(self, use_tile):
        cdef object stk, nb, tile
        cdef long use_flag

        if use_tile is None:
            pass

        elif use_tile is not self._build_tile:

            if self._build_tile is not None:
                for tl in self._build_tiles + self._build_safe_tiles:
                    tl.set_overlay(None)
                self._build_tile = None
                self._build_tiles = None

            self._build_tile = use_tile
            self._build_tiles = self._build_tile.get_rect_area(self._spec.get_tiles_width(), self._spec.get_tiles_height())
            self._build_safe_tiles = []
            self._build_nb_tiles = []

            self._x = 0
            self._y = 0

            for tile in self._build_tiles:
                self._x += tile.get_x()
                self._y += tile.get_y()

            self._x /= len(self._build_tiles)
            self._y /= len(self._build_tiles)

            use_flag = self._tile_col.get_find_flag(1)
            stk = deque()

            for tile in self._build_tiles:
                tile.back_steps = 0
                tile.gen_flag = use_flag
                stk.append(tile)

            while stk:
                tile = stk.popleft()
                tile.gen_flag = use_flag

                if tile.back_steps == 0:
                    pass
                elif tile.back_steps <= self._spec.get_safe_range():
                    self._build_safe_tiles.append(tile)
                elif tile.back_steps <= self._spec.get_place_range():
                    self._build_nb_tiles.append(tile)

                if tile.back_steps < self._spec.get_place_range():
                    for nb in tile.get_neighbors():
                        if nb.gen_flag != use_flag:
                            nb.gen_flag = use_flag
                            nb.back_steps = tile.back_steps + 1

                            stk.append(nb)

            self._tile_col.move_find_flag(1)

        return self

    def unset(self):
        cdef object tile

        if self._build_tile is not None:
            for tile in self._build_tiles + self._build_safe_tiles:
                tile.set_overlay(None)

            self._build_tile = None
            self._build_tiles = None
            self._build_safe_tiles = None

    def higlight(self):
        cdef object tl
        cdef int is_near

        if self._build_tile is not None:

            is_near = self.get_is_near_build() and not self.get_is_invalid()

            for tl in self._build_tiles:
                if tl.is_buildable() and is_near:
                    tl.set_overlay("place_ok", 220)
                else:
                    tl.set_overlay("place_deny", 220)

            for tl in self._build_safe_tiles:
                if tl.is_buildable_safe(self._spec) and is_near:
                    tl.set_overlay("place_ok", 120)
                else:
                    tl.set_overlay("place_deny", 120)

    def get_is_near_build(self):
        cdef object tl, bref

        for tl in self._build_nb_tiles + self._build_safe_tiles + self._build_tiles:
            bref = tl.get_build_ref()
            
            if bref is not None and bref.get_side() is self._side and bref.get_spec().get_place_seek():
                return True

        return False

    def get_is_invalid(self):
        cdef long len_a, len_b

        len_a = self._spec.get_tiles_width() * self._spec.get_tiles_height()
        len_b = (self._spec.get_tiles_width() + 2) * (self._spec.get_tiles_height() + 2) - len_a

        if len(self._build_tiles) != len_a:
            return True

        if len(self._build_safe_tiles) != len_b:
            return True

        return False

    def get_is_buildable(self):
        cdef object tl

        if self._build_tile is None:
            return False

        if self.get_is_invalid():
            return False

        for tl in self._build_tiles:
            if not tl.is_buildable():
                return False

        for tl in self._build_safe_tiles:
            if not tl.is_buildable_safe(self._spec):
                return False

        if not self.get_is_near_build():
            return False

        return True
