import math
import random

from game.build.buildturretspec import BuildTurretSpec
from game.graphic.graphbatches import GraphBatches
from game.helper.discrete import get_angle_from
from game.helper.vector cimport angle_conv_realdeg, angle_dist_side_deg, angle_inc_border_deg, angle_conv_deg
from game.interface.ibuilds import IBuilds
from game.interface.idrawable import IDrawable
from game.interface.iparticles import IParticles
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.map.maptilearea import MapTileArea
from game.particle.particlecollector import ParticleCollector
from game.player.playerside import PlayerSide
from game.shot.shotrealize import ShotRealize
from game.sound.soundcollector import SoundCollector


class BuildTurret(IDrawable):

    _builds: IBuilds = None
    _particles: IParticles = None
    _sounds: ISounds = None
    _batches = None

    _parent = None
    _side: PlayerSide = None
    _spec: BuildTurretSpec = None
    _tile_area = None
    _target: ITargetable = None

    _sprite_init = False
    _sprite_turret = None
    _sprite_canon = None

    _width = 0
    _height = 0
    _visible = False
    _tile = None
    _idle_timeout = 0
    _idle_seq = 0

    _look_angle = 0
    _look_angle_deg = 0
    _look_trg_angle_deg = 0

    _force_turret = 0
    _force_canon = 0

    _shots_count = 0
    _shots_timeout = 0
    _shots_wait_timeout = 0
    _sound_timeout = 0

    _look_seqs = [125, 625, 125, 250, 500, 0, 875, 375, 750, 0, 250, 375, 500, 750, 375, 750, 375, 500, 375, 625, 750, 250, 750, 875, 250, 375, 625, 500, 625, 250, 625, 875, 0, 500, 250, 875, 625, 375, 625, 875, 500, 250, 875, 250, 125, 0, 250, 500, 625, 500, 125, 750, 125, 500, 625, 500, 375, 0, 250, 875]

    def __init__(self, parent, side: PlayerSide, spec: BuildTurretSpec):
        IDrawable.__init__(self)

        self._builds = IBuilds.get()
        self._particles: IParticles = ParticleCollector()
        self._sounds: ISounds = SoundCollector()
        self._batches = GraphBatches()

        self._parent = parent
        self._side = side
        self._spec = spec
        self._shots_count = self._spec.get_shot_count()
        self._tile_area = None
        self._visible = True
        self._idle_timeout = 0
        self._idle_seq = 0

    def get_side(self):
        return self._side

    def get_spec(self):
        return self._spec

    def get_targetable(self):
        return self._parent

    def get_x(self):
        return self._tile.get_x()

    def get_y(self):
        return self._tile.get_y()

    def get_eff_x(self):
        return self._tile.get_x()

    def get_eff_y(self):
        return self._tile.get_y()

    def get_look_coords_angle_inc(self, value, rand=0):
        # TODO udelat diskretne
        return (
            self._tile.get_x() + math.sin(self._look_angle) * value + random.random() * rand - rand / 2,
            self._tile.get_y() + math.cos(self._look_angle) * value + random.random() * rand - rand / 2
        )

    def get_coords(self, rand=0):
        return (
            self._tile.get_x() + random.random() * rand - rand / 2,
            self._tile.get_y() + random.random() * rand - rand / 2
        )

    def get_use_coords(self):
        return (
            self._tile.get_x(),
            self._tile.get_y()
        )

    def get_sprite_x(self):
        return self._tile.get_x() * self._builds.get_zoom() + self._builds.get_x()

    def get_sprite_y(self):
        return self._tile.get_y() * self._builds.get_zoom() + self._builds.get_y()

    def get_tile(self):
        return self._tile

    def get_place(self):
        return self._parent.get_place()

    def get_is_alive(self):
        return self._parent.get_is_alive()

    def get_look_angle(self):
        return self._look_angle

    def get_look_angle_deg(self):
        return self._look_angle_deg

    def get_look_trg_angle_deg(self):
        return self._look_trg_angle_deg

    def set_look_snap(self, look_snap):
        pass

    def init(self, tile):

        self._tile = tile
        self._tile_area = MapTileArea(self, self._spec.get_range())
        self._tile_area.set_coords(self.get_x(), self.get_y())

        self._sprite_init = True
        self.may_draw = 1
        self._batches.add_drawable(self)

    def scan_targets(self):
        if self._target is None or self._target.get_target_priority() in [0, 1]:
            c = self._tile_area.scan_targetables_on_tiles(self._spec.get_range_tiles() + 1, lambda mm, result: BuildTurret._find_moveable(self, mm, result))

            if c is not None and c['ref'].dist_to_tile(self._tile) <= self._spec.get_range():
                self._target = c['ref']

        return self._target is not None

    @staticmethod
    def _find_moveable(tref, mm_loop: ITargetable, mm_found: ITargetable):
        if mm_loop is not tref and mm_loop.get_is_alive() and tref.get_side() is not mm_loop.get_side():

            dist = mm_loop.dist_to_tile(tref.get_tile())
            pri = mm_loop.get_target_priority()

            if mm_found is None or (mm_found['dist'] > dist and mm_found['pri'] == pri) or mm_found['pri'] < pri:
                mm_found = {
                    'ref': mm_loop,
                    'dist': dist,
                    'pri': pri
                }

        return mm_found

    def destroy(self):
        self._tile_area.destroy()
        self.may_draw = 2
        self._batches.add_sprite_delete([self._sprite_turret, self._sprite_canon])
        GraphBatches().kill_drawable(self)

    def draw_destroy(self):
        pass

    def update(self, tick):

        if self._shots_wait_timeout > 0:
            self._shots_wait_timeout -= 1

        elif self._shots_timeout > 0:
            self._shots_timeout -= 1

        if self._sound_timeout > 0:
            self._sound_timeout -= 1

        if self._force_canon != 0:
            self._force_canon += 0.25
            if self._force_canon >= 0:
                self._force_canon = 0

        if self._force_turret != 0:
            self._force_turret += 0.1
            if self._force_turret >= 0:
                self._force_turret = 0

        if self._target is not None:
            if not self._target.get_is_alive():
                self._target = None
                self._idle_timeout = 60
                self.scan_targets()

            if self._target is not None and self._target.dist_to_tile(self._tile) <= self._spec.get_range():

                c = self._target.get_target_coords(self.get_x(), self.get_y())
                self._look_trg_angle_deg = get_angle_from(c[0] - self.get_x(), c[1] - self.get_y())

                if (self._look_angle_deg - self._look_trg_angle_deg) != 0:
                    btw = angle_dist_side_deg(self._look_trg_angle_deg, self._look_angle_deg)

                    if min(btw.left, btw.right) <= self._spec.get_look_speed():
                        self._look_angle_deg = self._look_trg_angle_deg

                    elif btw.left < btw.right:
                        self._look_angle_deg = angle_inc_border_deg(self._look_angle_deg, self._spec.get_look_speed(), self._look_trg_angle_deg, self._spec.get_look_speed())

                    else:
                        self._look_angle_deg = angle_inc_border_deg(self._look_angle_deg, -self._spec.get_look_speed(), self._look_trg_angle_deg, self._spec.get_look_speed())

                    self._look_angle = angle_conv_deg(self._look_angle_deg)

                elif self._shots_count > 0 and self._shots_timeout == 0 and self._shots_wait_timeout == 0:
                    self._shot_to_target()

            else:
                self._target = None
                self._idle_timeout = 60

        elif self._idle_timeout > 0:
            self._idle_timeout -= 1

            if self._idle_timeout == 0:
                self._look_trg_angle_deg = BuildTurret._look_seqs[self._idle_seq]
                self._idle_seq += 1
                if self._idle_seq >= len(BuildTurret._look_seqs):
                    self._idle_seq = 0

        else:
            btw = angle_dist_side_deg(self._look_trg_angle_deg, self._look_angle_deg)

            if min(btw.left, btw.right) <= self._spec.get_look_speed() // 2:
                self._look_angle_deg = self._look_trg_angle_deg
                self._idle_timeout = 60

            elif btw.left < btw.right:
                self._look_angle_deg = angle_inc_border_deg(self._look_angle_deg, self._spec.get_look_speed() // 2, self._look_trg_angle_deg, self._spec.get_look_speed() // 2)

            else:
                self._look_angle_deg = angle_inc_border_deg(self._look_angle_deg, -self._spec.get_look_speed() // 2, self._look_trg_angle_deg, self._spec.get_look_speed() // 2)

            self._look_angle = angle_conv_deg(self._look_angle_deg)

        if tick % 30 == 0 and self._target is None:
            self.scan_targets()

    def _shot_to_target(self):
        sr = ShotRealize(self, self._target, self._spec)
        c = sr.make(self._shots_count)

        if "sound_timeout" in c:
            self._sound_timeout = c["sound_timeout"]

        self._force_canon = self._spec.get_shot_force_canon()
        self._force_turret = self._spec.get_shot_force_turret()
        self._shots_count -= 1
        self._shots_lock = True

        if self._shots_count == 0:
            self._shots_timeout = self._spec.get_shot_reload() + self._spec.get_shot_mid_reload()
            self._shots_count = self._spec.get_shot_count()
            self._shots_wait_timeout = self._spec.get_shot_cooldown()

        else:
            self._shots_timeout = self._spec.get_shot_mid_reload()

    def draw(self):
        if self.may_draw != 1:
            return False

        if self._parent.get_visible() != self._visible:
            self._visible = self._parent.get_visible()

            if self._visible:
                self._sprite_init = True
            else:
                self._batches.add_sprite_delete([self._sprite_turret, self._sprite_canon])

                self._sprite_turret = None
                self._sprite_canon = None

        if self._visible:
            if self._sprite_init:
                self._sprite_init = False

                self._sprite_turret = self._batches.create_sprite(img=self._spec.get_tex_turret(0, self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_build_third(), batch=self._batches.get_batch())
                self._sprite_turret.visible = self._visible

                self._sprite_canon = self._batches.create_sprite(img=self._spec.get_tex_canon(0, self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_build_second(), batch=self._batches.get_batch())
                self._sprite_canon.visible = self._visible

            self._sprite_canon.update(x=self.get_sprite_x() + math.sin(self._look_angle) * self._force_canon * self._builds.get_zoom(),
                                      y=self.get_sprite_y() + math.cos(self._look_angle) * self._force_canon * self._builds.get_zoom(),
                                      scale=self._builds.get_zoom(),
                                      rotation=angle_conv_realdeg(self._look_angle))

            self._sprite_turret.update(x=self.get_sprite_x() + math.sin(self._look_angle) * self._force_turret * self._builds.get_zoom(),
                                       y=self.get_sprite_y() + math.cos(self._look_angle) * self._force_turret * self._builds.get_zoom(),
                                       scale=self._builds.get_zoom(),
                                       rotation=angle_conv_realdeg(self._look_angle))
