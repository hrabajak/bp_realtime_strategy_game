from game.build.builditem import BuildItem
from game.build.buildspec import BuildSpec
from game.helper.singleton import Singleton
from game.interface.ibuilds import IBuilds
from game.interface.iplayersides import IPlayerSides
from game.interface.itilecollector import ITileCollector
from game.interface.iview import IView


class BuildCollector(IBuilds, metaclass=Singleton):

    _tiles: ITileCollector = None
    _sides: IPlayerSides = None
    _view: IView = None

    _childs = None

    _x = 0
    _y = 0
    _zoom = 1.0

    def __init__(self):
        IBuilds.__init__(self)
        IBuilds.set(self)

        self._childs = []

    def set_refs(self, tiles, sides, view):
        self._tiles = tiles
        self._sides = sides
        self._view = view

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_zoom(self):
        return self._zoom

    def add_build(self, side, spec: BuildSpec, x, y, data=None):
        mm = BuildItem(side, spec)
        mm.init(self._tiles.get_tile(x, y), data)

        self._childs.append(mm)

        return mm

    def add_build_tile(self, side, spec: BuildSpec, tile, data=None):
        mm = BuildItem(side, spec)
        mm.init(tile, data)

        self._childs.append(mm)

        return mm

    def get_build_at_coord(self, x: float, y: float, target_side=None):
        place = self._tiles.get_place_by_coords_scroll(x, y)

        if place is not None:
            mm = place.get_build_ref()

            if mm is not None and (target_side is None or mm.get_side() is target_side):
                return mm

        return None

    def get_build_at_range(self, sx: float, sy: float, ex: float, ey: float, target_side=None):
        out = []

        for mm in self._childs:
            sz = mm.get_sprite_size() / 2

            if mm.get_sprite_x() + sz >= sx and mm.get_sprite_x() - sz <= ex and mm.get_sprite_y() + sz >= sy and mm.get_sprite_y() - sz <= ey:
                if target_side is None or mm.get_side() is target_side:
                    out += [mm]

        return out

    def get_build_by_number(self, number: int):
        for mm in self._childs:
            if mm.number == number:
                return mm

        return None

    def get_childs(self):
        return self._childs

    def serialize(self):
        out = {
            'number_total': BuildItem.number_total,
            'list': []
        }

        for ch in self._childs:
            out['list'].append(ch.serialize())

        return out

    def unserialize(self, dt):
        for c in dt['list']:
            side = self._sides.get_side_by_number(c['side_number'])
            mm = side.add_build(self._tiles.get_tile(c['tile_x'], c['tile_y']), BuildSpec.get(c['spec']), {'ready': True})
            mm.number = c['number']

        BuildItem.number_total = dt['number_total']

    def clear(self):
        for ch in self._childs:
            ch.destroy()

        self._childs = []

    def update(self, tick):
        rem = []

        for ch in self._childs:
            if ch.update(tick):
                rem.append(ch)

        if len(rem) > 0:
            for ch in rem:
                ch.destroy()
                self._childs.remove(ch)

    def draw(self):
        self._x, self._y, self._zoom = self._view.get_view_vals()
