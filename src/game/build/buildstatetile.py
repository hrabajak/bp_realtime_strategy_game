import math

from game.graphic.graphbatches import GraphBatches
from game.image.imagecollector import ImageCollector
from game.interface.ibuilds import IBuilds
from game.interface.idrawable import IDrawable


class BuildStateTile(IDrawable):

    _builds = None
    _batches = None

    _parent = None
    _tile = None
    _state = 0
    _opacity = 0
    _visible = False
    _sprite_init = False
    _sprite = None
    _sprite_up = None

    number = 0
    number_total = 0

    def __init__(self, parent, tile):
        IDrawable.__init__(self)

        self._builds = IBuilds.get()
        self._batches = GraphBatches()

        self._parent = parent
        self._tile = tile
        self._state = 0
        self._opacity = 255
        self._sprite_init = True
        self._visible = True
        self.may_draw = 1
        self._batches.add_drawable(self)

        self.number = BuildStateTile.number_total
        BuildStateTile.number_total += 1

    def get_tile(self):
        return self._tile

    def get_place(self):
        return self._tile.get_place()

    def get_x(self):
        return self._tile.get_x()

    def get_y(self):
        return self._tile.get_y()

    def set_build(self):
        self._state = 1
        self._sprite_init = True

    def set_decay(self):
        self._state = 2

    def destroy(self):
        self.may_draw = 2
        self._batches.add_sprite_delete([self._sprite, self._sprite_up])
        GraphBatches().kill_drawable(self)

    def update(self):
        if self._state == 2:
            self._opacity = max(0, self._opacity - 10)
            return self._opacity == 0

        return False

    def draw_destroy(self):
        pass

    def draw(self):
        if self.may_draw != 1:
            return False

        if self._parent.get_visible() != self._visible:
            self._visible = self._parent.get_visible()

            if self._visible:
                self._sprite_init = True
            else:
                self._batches.add_sprite_delete([self._sprite, self._sprite_up])

                self._sprite = None
                self._sprite_up = None

        if self._visible:
            if self._sprite_init:
                if self._sprite is None:
                    self._sprite = self._batches.create_sprite(img=ImageCollector().get_image("build_cross"), x=self._tile.get_sprite_x(), y=self._tile.get_sprite_y(), group=self._batches.get_group_below(), batch=self._batches.get_batch())

                if self._state == 1 and self._sprite_up is None:
                    self._sprite_up = self._batches.create_sprite(img=ImageCollector().get_image("build_state_top", self.number % 4), x=self._tile.get_sprite_x(), y=self._tile.get_sprite_y(), group=self._batches.get_group_build_second(), batch=self._batches.get_batch())
                    self._sprite_up.rotation = 90 * ((self._tile.get_pos_x() + self._tile.get_pos_y()) % 7)
                    self._sprite.image = ImageCollector().get_image("build_state_below", (self._tile.get_pos_x() + self._tile.get_pos_y()) % 4)
                    self._sprite.rotation = 90 * ((self._tile.get_pos_x() + self._tile.get_pos_y()) % 4)

                self._sprite_init = False

            self._sprite.update(x=self._tile.get_sprite_x(), y=self._tile.get_sprite_y(), scale=self._builds.get_zoom())
            self._sprite.opacity = self._opacity

            if self._sprite_up is not None:
                self._sprite_up.update(x=self._tile.get_sprite_x(), y=self._tile.get_sprite_y(), scale=self._builds.get_zoom())
                self._sprite_up.opacity = self._opacity
