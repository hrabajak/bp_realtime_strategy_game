import random
from collections import deque

from game.build.buildplace import BuildPlace
from game.build.buildspec import BuildSpec
from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.vector import dist_to
from game.interface.itilecollector import ITileCollector
from game.player.playerside import PlayerSide


class BuildPlaceAnalyze:

    _tiles: ITileCollector = None

    _side = None
    _spec = None

    _buildable_tiles = None

    def __init__(self, side: PlayerSide, spec: BuildSpec):
        self._tiles = ITileCollector.get()

        self._side = side
        self._spec = spec

    def get_side(self):
        return self._side

    def get_spec(self):
        return self._spec

    def get_buildable_tiles(self):
        return self._buildable_tiles

    def get_buildable_tile(self):
        if len(self._buildable_tiles) > 0:
            self._buildable_tiles.sort(key=lambda c: c["dist_sort"], reverse=False)
            return self._buildable_tiles[0]["tile"]

        return None

    def get_buildable_tile_nearest(self, positions):
        min_tile = None
        min_dist = -1

        for bb in self._buildable_tiles:
            for c in positions:
                dist = bb["tile"].dist_to_coord_pos(c[0], c[1])

                if min_dist == -1 or dist < min_dist:
                    min_tile = bb["tile"]
                    min_dist = dist

        return min_tile

    def analyze(self):
        self._buildable_tiles = []

        max_range = self._spec.get_place_range() + max(self._spec.get_tiles_width(), self._spec.get_tiles_height()) - 1
        use_flag = self._tiles.get_find_flag()

        stk = deque()

        for b in self._side.get_build_childs():
            for tl in b.get_tiles():
                tl.back_steps = 0
                tl.flag = use_flag

                stk.append(tl)

        #PrimitiveCollector().clear()

        bp = BuildPlace(self._side, self._spec)

        while stk:
            tile = stk.popleft()

            if tile.is_buildable():
                bp.set_tile(tile)

                if bp.get_is_buildable():
                    dist_avg = 0

                    for bref in self._side.get_build_childs():
                        dist_avg += dist_to(bp.get_x(), bp.get_y(), bref.get_x(), bref.get_y())

                    dist_avg /= len(self._side.get_build_childs())

                    self._buildable_tiles.append({
                        "tile": tile,
                        "dist_avg": dist_avg,
                        "dist_sort": int(dist_avg // 20)
                    })

                    #PrimitiveCollector().add_circle(tile.get_x(), tile.get_y(), 5)

            if tile.back_steps < max_range:
                for nb in tile.get_neighbors():
                    if nb.flag != use_flag:
                        nb.flag = use_flag
                        nb.back_steps = tile.back_steps + 1

                        stk.append(nb)

        self._tiles.move_find_flag()

    def analyze_simple_get(self, min_back_steps=0):
        max_range = self._spec.get_place_range() + max(self._spec.get_tiles_width(), self._spec.get_tiles_height()) - 1
        use_flag = self._tiles.get_find_flag()

        result = []
        stk = deque()

        for b in self._side.get_build_childs():
            for tl in b.get_tiles():
                tl.back_steps = 0
                tl.flag = use_flag

                stk.append(tl)

        while stk:
            tile = stk.popleft()

            if tile.is_buildable() and tile.back_steps >= min_back_steps:
                result.append(tile)

            if tile.back_steps < max_range:
                for nb in tile.get_neighbors():
                    if nb.flag != use_flag:
                        nb.flag = use_flag
                        nb.back_steps = tile.back_steps + 1

                        stk.append(nb)

        self._tiles.move_find_flag()

        return result
