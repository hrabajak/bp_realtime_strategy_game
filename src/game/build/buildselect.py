from game.player.playerside import PlayerSide


class BuildSelect:

    _side = None
    _ac_spec = None
    _tiles = None
    _build_src_ref = None
    _build_ref = None

    def __init__(self, side: PlayerSide, ac_spec, build_src_ref=None):
        self._side = side
        self._ac_spec = ac_spec
        self._build_src_ref = build_src_ref

    def get_side(self):
        return self._side

    def get_ac_spec(self):
        return self._ac_spec

    def get_build_src_ref(self):
        return self._build_src_ref

    def get_build_ref(self):
        return self._build_ref

    def set_tile(self, tile):
        if self._tiles is not None:
            for tl in self._tiles:
                tl.set_overlay(None)
            self._tiles = None

        if tile is None:
            pass

        elif tile.get_build_ref() is not None:
            self._build_ref = tile.get_build_ref()
            self._tiles = self._build_ref.get_tiles()

        else:
            self._build_ref = None
            self._tiles = [tile]

    def unset(self):
        if self._tiles is not None:
            for tl in self._tiles:
                tl.set_overlay(None)
            self._tiles = None

        self._build_ref = None

    def higlight(self):
        if self._tiles is not None:

            is_valid = self.get_is_valid()

            for tl in self._tiles:
                if is_valid:
                    tl.set_overlay("place_ok", 220)
                else:
                    tl.set_overlay("place_deny", 220)

    def get_is_valid(self):
        return self._build_ref is not None and self._build_ref.get_side() is self._side and self._build_ref.get_spec().get_key() in self._ac_spec.get_upgrade_keys() and self._build_ref.get_may_upgrade()
