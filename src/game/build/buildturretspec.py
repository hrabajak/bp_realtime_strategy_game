import random

from game.image.imagecollector import ImageCollector
from game.interface.iimages import IImages
from game.shot.shotspec import ShotSpec


class BuildTurretSpec:

    _specs = {}
    _specs_by_num = {}
    _key = None
    _side_type = None
    _name = None
    _tex_turret = None
    _tex_canon = None
    _look_speed = 0.05
    _range = 150
    _shot_count = 1
    _shot_effect = "boom"
    _shot_spec = None
    _shot_reload = 60
    _shot_mid_reload = 15
    _shot_cooldown = 0
    _shot_force_canon = -10
    _shot_force_turret = -4
    _shot_canon_dist = 25

    number = 0
    _number_total = 0

    def __init__(self, key):
        self._key = key

        self.number = BuildTurretSpec._number_total
        BuildTurretSpec._number_total += 1

    def get_key(self):
        return self._key

    def get_side_type(self):
        return self._side_type

    def get_name(self):
        return self._name

    def get_tex_turret(self, key=0, color=None):
        return ImageCollector().get_image_color(self._tex_turret[key], color)

    def get_tex_canon(self, key=0, color=None):
        return ImageCollector().get_image_color(self._tex_canon[key], color)

    def get_tex_load(self, color):
        self.get_tex_turret(0, color)
        self.get_tex_turret(1, color)
        self.get_tex_canon(0, color)
        self.get_tex_canon(1, color)

    def get_look_speed(self):
        return self._look_speed

    def get_range(self):
        return self._range

    def get_range_tiles(self):
        return self._range // 50

    def get_shot_count(self):
        return self._shot_count

    def get_shot_effect(self):
        return self._shot_effect

    def get_shot_spec(self):
        return self._shot_spec

    def get_shot_reload(self):
        return self._shot_reload

    def get_shot_mid_reload(self):
        return self._shot_mid_reload

    def get_shot_cooldown(self):
        return self._shot_cooldown

    def get_shot_force_canon(self):
        return self._shot_force_canon

    def get_shot_force_turret(self):
        return self._shot_force_turret

    def get_shot_canon_dist(self):
        return self._shot_canon_dist

    def get_shot_canon_dist_rnd(self, rand):
        return self._shot_canon_dist + random.random() * rand - rand / 2

    @staticmethod
    def init_specs(imgs: IImages):

        s = BuildTurretSpec("s1_minigun")
        s._side_type = "s1"
        s._name = "Minigun"
        s._tex_turret = "s1_tower_minigun", "s1_tower_minigun_mono"
        s._tex_canon = "s1_tower_minigun_cannon", "s1_tower_minigun_cannon_mono"
        s._look_speed = 20
        s._range = 360
        s._shot_spec = ShotSpec.get("mini_jeep")
        s._shot_effect = "mini_jeep"
        s._shot_reload = 10
        s._shot_mid_reload = 0
        s._shot_force_canon = -6
        s._shot_force_turret = -3

        BuildTurretSpec._specs[s.get_key()] = s
        BuildTurretSpec._specs_by_num[s.number] = s

        s = BuildTurretSpec("s1_cannon")
        s._side_type = "s1"
        s._name = "Cannon"
        s._tex_turret = "s1_tower_can", "s1_tower_can_mono"
        s._tex_canon = "s1_tower_can_cannon", "s1_tower_can_cannon_mono"
        s._look_speed = 6
        s._range = 360
        s._shot_spec = ShotSpec.get("tank")
        s._shot_effect = "tank"
        s._shot_reload = 45
        s._shot_mid_reload = 0
        s._shot_force_canon = -10
        s._shot_force_turret = -2

        BuildTurretSpec._specs[s.get_key()] = s
        BuildTurretSpec._specs_by_num[s.number] = s

        s = BuildTurretSpec("s2_laser")
        s._side_type = "s2"
        s._name = "Laser"
        s._tex_turret = "s2_tower_laser", "s2_tower_laser_mono"
        s._tex_canon = "s2_tower_laser_cannon", "s2_tower_laser_cannon_mono"
        s._look_speed = 20
        s._range = 360
        s._shot_spec = ShotSpec.get("laser_beam")
        s._shot_effect = "laser_beam"
        s._shot_reload = 20
        s._shot_mid_reload = 0
        s._shot_count = 2
        s._shot_force_canon = -6
        s._shot_force_turret = -4

        BuildTurretSpec._specs[s.get_key()] = s
        BuildTurretSpec._specs_by_num[s.number] = s

        s = BuildTurretSpec("s2_rocket")
        s._side_type = "s2"
        s._name = "Rocket"
        s._tex_turret = "s2_tower_rocket", "s2_tower_rocket_mono"
        s._tex_canon = "s2_tower_rocket_cannon", "s2_tower_rocket_cannon_mono"
        s._look_speed = 6
        s._range = 360
        s._shot_spec = ShotSpec.get("minirocket")
        s._shot_effect = "minirocket"
        s._shot_reload = 110
        s._shot_mid_reload = 10
        s._shot_count = 4
        s._shot_force_canon = -6
        s._shot_force_turret = -6
        s._shot_canon_dist = 5

        BuildTurretSpec._specs[s.get_key()] = s
        BuildTurretSpec._specs_by_num[s.number] = s

    @staticmethod
    def get(key):
        return BuildTurretSpec._specs[key]

    @staticmethod
    def get_by_number(num):
        return BuildTurretSpec._specs_by_num[num]

    @staticmethod
    def get_all_by_type(tp):
        list = []

        for key in BuildTurretSpec._specs.keys():
            spec = BuildTurretSpec.get(key)

            if spec.get_side_type() == tp:
                list.append(spec)

        return list
