import math
import random

from game.action.actionitemspec import ActionItemSpec
from game.build.buildspec import BuildSpec
from game.build.buildstatetile import BuildStateTile
from game.build.buildturret import BuildTurret
from game.build.buildturretspec import BuildTurretSpec
from game.graphic.graphbatches import GraphBatches
from game.helper.discretemath import int_sqrt
from game.helper.vector import angle_conv_realdeg
from game.image.imagecollector import ImageCollector
from game.interface.ibuilds import IBuilds
from game.interface.idrawable import IDrawable
from game.interface.igui import IGui
from game.interface.iparticles import IParticles
from game.interface.iplayersides import IPlayerSides
from game.interface.iselectable import ISelectable
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.map.maptilearea import MapTileArea
from game.map.maptiletexture import MapTileTexture
from game.map.pathfinder import PathFinder
from game.particle.particlecollector import ParticleCollector
from game.player.playerside import PlayerSide
from game.const.specialflags import SpecialFlags
from game.moveable.unitspec import UnitSpec
from game.sound.soundcollector import SoundCollector


class BuildItem(ITargetable, ISelectable, IDrawable):

    _builds: IBuilds = None
    _particles: IParticles = None
    _sounds: ISounds = None
    _batches = None

    _side: PlayerSide = None
    _spec: BuildSpec = None
    _sprite_init = False
    _sprite = None
    _sprite_below = None
    _sprite_select = None
    _sprite_ico = None
    _travel_particle = None
    _travel_rect = None
    _selected = None
    _tile = None
    _tiles = None
    _state_tiles = None
    _places = None
    _tiles_around = None
    _places_around = None
    _places_special = None
    _places_special_dict = None
    _place_travel = None
    _action_item = None
    _tile_areas = None
    _turrets = None
    _callback_dead = None
    _callback_damage = None
    _callback_kill = None
    _is_ready_flag = False
    _quick_ready = False
    _visible = False
    _sprite_up_damage = 0
    _opacity = 0
    _angle = 0
    _x = 0
    _y = 0
    _width = 0
    _height = 0
    _health = 0
    _t_width = None
    _t_height = None
    _dmg_fire_tick = 0
    _dmg_smoke_tick = 0
    _dmg_hit_timeout = 0
    _effects = None
    _upgrades = None
    _upgrade_spec: ActionItemSpec = None
    _upgrade_mref = None

    number = 0
    number_total = 0

    def __init__(self, side: PlayerSide, spec: BuildSpec):
        ITargetable.__init__(self)
        ISelectable.__init__(self)
        IDrawable.__init__(self)

        self._builds = IBuilds.get()
        self._particles: IParticles = ParticleCollector()
        self._sounds: ISounds = SoundCollector()
        self._batches = GraphBatches()

        self._tile_areas = []
        self._turrets = None
        self._side = side
        self._spec = spec
        self._upgrade_spec = None
        self._selected = False
        self._health = self._spec.get_health()
        self._angle = 0
        self._upgrades = []
        self._pend_upgrades = []
        self._sprite_select = [[None, None], [None, None]]

        if self._spec.get_key() == "s1_barracks":
            self._upgrade_spec = ActionItemSpec.get("s1_upgrade")
        elif self._spec.get_key() == "s1_factory":
            self._upgrade_spec = ActionItemSpec.get("s1_upgrade")
        elif self._spec.get_key() == "s1_main":
            self._upgrade_spec = ActionItemSpec.get("s1_upgrade")
        elif self._spec.get_key() == "s2_barracks":
            self._upgrade_spec = ActionItemSpec.get("s2_upgrade")
        elif self._spec.get_key() == "s2_factory":
            self._upgrade_spec = ActionItemSpec.get("s2_upgrade")
        elif self._spec.get_key() == "s2_main":
            self._upgrade_spec = ActionItemSpec.get("s2_upgrade")

        self.number = BuildItem.number_total
        BuildItem.number_total += 1

    def get_side(self):
        return self._side

    def get_target_type(self):
        return 1

    def get_target(self):
        return None

    def get_spec(self):
        return self._spec

    def get_upgrade_spec(self):
        return self._upgrade_spec

    def get_selected_type(self):
        if self._is_ready_flag >= 1:
            return ISelectable.SELECTED_BUILD
        else:
            return ISelectable.SELECTED_NONE

    def get_targetable(self):
        return self

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_use_x(self):
        return self._x

    def get_use_y(self):
        return self._y

    def get_selected(self):
        return self._selected

    def get_action_item(self):
        return self._action_item

    def get_is_alive(self):
        return self._health > 0

    def get_is_damaged(self):
        return self._health / self._spec.get_health() <= 0.6666

    def get_is_heavy_damaged(self):
        return self._health / self._spec.get_health() <= 0.3333

    def get_is_ready(self):
        return self._is_ready_flag == 2

    def get_is_ready_flag(self):
        return self._is_ready_flag

    def get_is_human(self):
        return False

    def get_sprite_x(self):
        return (self._tile.get_x() + self._t_width) * self._builds.get_zoom() + self._builds.get_x()

    def get_sprite_y(self):
        return (self._tile.get_y() - self._t_height) * self._builds.get_zoom() + self._builds.get_y()

    def get_sprite_sel_x(self, y, x):
        return (self._tile.get_x() + (self._width - 50) * x) * self._builds.get_zoom() + self._builds.get_x()

    def get_sprite_sel_y(self, y, x):
        return (self._tile.get_y() - (self._height - 50) * y) * self._builds.get_zoom() + self._builds.get_y()

    def get_ico_x(self):
        return (self._tile.get_x()) * self._builds.get_zoom() + self._builds.get_x()

    def get_ico_y(self):
        return (self._tile.get_y()) * self._builds.get_zoom() + self._builds.get_y()

    def get_sprite_size(self):
        return 50.0 * self._spec.get_tiles_width() * self._builds.get_zoom()

    def get_coords(self, rand=0):
        return (
            self._x + random.random() * rand - rand / 2,
            self._y + random.random() * rand - rand / 2
        )

    def get_target_place(self, from_x, from_y):
        return self.get_nearest_place(from_x, from_y)

    def get_target_coords(self, from_x, from_y):
        place = self.get_nearest_place(from_x, from_y)
        return place.get_x(), place.get_y()

    def get_target_colide_coords(self, ref):
        tl = self.get_nearest_place(ref.get_use_x(), ref.get_use_y())
        rand = 40

        return (
            tl.get_x() + random.random() * rand - rand / 2,
            tl.get_y() + random.random() * rand - rand / 2
        )

    def get_target_colide_area(self):
        return 30

    def get_target_priority(self):
        return self._spec.get_target_priority()

    def get_target_colide_check(self, cx, cy):
        place = ITileCollector.get().get_place_by_coords(cx, cy)
        return place is not None and place.get_build_ref() is self

    def get_nearest_place(self, from_x, from_y):
        return min(self._places, key=lambda tl: tl.dist_to_coord(from_x, from_y))

    def get_place(self):
        return self._places[0]

    def get_places(self):
        return self._places

    def get_places_around(self):
        return self._places_around

    def get_places_special(self, special):
        if special in self._places_special_dict:
            return self._places_special_dict[special]
        else:
            return None

    def get_is_on_place(self, place):
        return place in self._places
    
    def get_tile(self):
        return self._tiles[0]

    def get_tiles(self):
        return self._tiles

    def get_state_tiles(self):
        return self._state_tiles

    def get_may_upgrade(self):
        return len(self._upgrades) < self._spec.get_upgrade_max() and self._upgrade_mref is None

    def get_upgrade_level(self):
        return len(self._upgrades)

    def get_may_apply_action(self, ac_spec):
        if len(ac_spec.get_man_builds()) > 0:
            if not next((True for key in ac_spec.get_man_builds() if len(self._side.get_builds_by_key(key)) > 0), False):
                return False

        return self._is_ready_flag >= 1 and ac_spec.get_key() in self._spec.get_action_keys() and ac_spec.get_level() <= len(self._upgrades)

    def get_visible(self):
        return self._visible

    def get_state_map(self):
        sm = {}
        return sm

    def dist_to_moveable(self, ref):
        tl = min(self._tiles, key=lambda tl: tl.set_dist(int_sqrt((tl.get_x() - ref.get_use_x()) ** 2 + (tl.get_y() - ref.get_use_y()) ** 2)))
        return tl.dist

    def set_selected(self, selected):
        self._selected = selected
        self._update_place_travel()

    def set_place_travel(self, place_travel):
        if self._spec.get_allow_place_travel() and place_travel.is_accessible_no_build_only_b(): # !! and place_travel.dist_to_coord(self.get_x(), self.get_y()) < 1000
            self._place_travel = place_travel
            self._update_place_travel()

    def set_callback_dead(self, callback):
        self._callback_dead = callback

    def set_callback_damage(self, callback):
        self._callback_damage = callback

    def set_callback_kill(self, callback):
        self._callback_kill = callback

    def set_upgrade_mref(self, mref):
        self._upgrade_mref = mref

    def unset_upgrade_mref(self, mref):
        if mref is self._upgrade_mref:
            self._upgrade_mref = None

    def _update_place_travel(self):
        if self._travel_particle is not None:
            self._travel_particle.destroy()
            self._travel_particle = None
            self._travel_rect.discard()
            self._travel_rect = None

        if self._selected and self._place_travel is not None:
            self._travel_particle = self._particles.add_particle(self._place_travel.get_x(), self._place_travel.get_y(), 'treff', 0, scale=1, group=GraphBatches().get_group_gui(), batch=GraphBatches().get_batch_black())

            to_x = self.get_use_x()
            to_y = self.get_use_y()

            if SpecialFlags.SPECIAL_CREATE_FINAL in self._places_special_dict:
                pl = self._places_special_dict[SpecialFlags.SPECIAL_CREATE_FINAL][0][0]
                to_x = pl.get_x()
                to_y = pl.get_y()

            self._travel_rect = self._batches.get_rects().create(ImageCollector().get_image("travel_line"))
            self._travel_rect.set_opacity_kf(0.5)
            self._travel_rect.set_colors(255, 255, 255)
            self._travel_rect.set_coords_line(self._place_travel.get_x(), self._place_travel.get_y(), to_x, to_y, 2, 0.0, 0.9)

    def may_dmg_hit(self, timeout, shot_spec):
        if shot_spec.get_key() in ["mini_vehicle", "mini_jeep", "mini_troop"]:

            if self._dmg_hit_timeout == 0:
                self._dmg_hit_timeout = timeout
                return True
            else:
                return False

        else:
            return True

    def deal_damage(self, dx, dy, owner, shot_spec, angle, koef=1000):

        bef_heavy_damaged = self.get_is_heavy_damaged()
        bef_damaged = self.get_is_damaged()

        dmg = (shot_spec.get_damage() * koef) // 1000

        self._health -= dmg

        if dmg > 80:
            pt = self._particles.add_particle(dx, dy, 'destroy', -1, scale=0.7 - random.random() * 0.1)
            pt.set_force(angle + math.pi + (math.pi / 2) * (0.5 - random.random()), 3 + random.random() * 2, 0.4)
            pt.set_force_rotate(math.pi / 4 + random.random(), 0.2)
            pt.set_fadeout(40, 10)

        if self._health <= 0:
            self._side.event_destroy_build(self)
            self._side.rem_from_selected(self)

            self._sounds.play_sound("ex1", self._x, self._y, 0.5 + random.random() * 0.1)
            self._particles.add_particle(self._x, self._y, "exp3", -1, scale=1.1 - random.random() * 0.2)

            for i in range(0, 4):
                c = self.get_coords(round(self._width / 1.5))
                self._particles.add_particle(c[0], c[1], "exp1", -1, i * 20, scale=0.7 - random.random() * 0.2)

            for i in range(0, 5):
                c = self.get_coords(round(self._width / 1.5))
                self._particles.add_particle(c[0], c[1], "smoke", -1, i * 15, scale=1.0 - random.random() * 0.2)

            if self._is_ready_flag > 0:
                if self._spec.get_tex_below() is not None:
                    pt = self._particles.add_particle(self._x, self._y, self._spec.get_tex_below(), 0, group=self._batches.get_group_below(), batch=self._batches.get_batch())
                    pt.set_fadeout(300, 50)

                pt = self._particles.add_particle(self._x, self._y, self._spec.get_tex_dead(), 0, group=self._batches.get_group_below_up(), batch=self._batches.get_batch())
                pt.set_fadeout(300, 50)

            if owner is not None:
                pass

            return True

        elif self.get_is_heavy_damaged() and not bef_heavy_damaged:
            self._sprite_up_damage = 1
            self._sounds.play_sound("ex2", self._x, self._y, 0.7 + random.random() * 0.1)

        elif self.get_is_damaged() and not bef_damaged:
            self._sprite_up_damage = 2
            self._sounds.play_sound("ex2", self._x, self._y, 0.7 + random.random() * 0.1)

        return False

    def init(self, tile, data=None):

        quick_ready = (data is not None and "ready" in data and data["ready"]) or IPlayerSides.get().get_worker().get_quick_build()

        self._is_ready_flag = 0
        self._tile = tile
        self._tiles = tile.get_rect_area(self._spec.get_tiles_width(), self._spec.get_tiles_height())
        self._places = []
        [[self._places.append(pl) for pl in c.get_places()] for c in self._tiles]
        [c.set_type_update(MapTileTexture.T_DUST) for c in self._tiles if c.get_type() != MapTileTexture.T_FIELD]
        self._t_width = (self._spec.get_tiles_width() - 1) * 25
        self._t_height = (self._spec.get_tiles_height() - 1) * 25
        self._places_special = []
        self._places_special_dict = {}

        [tl.set_special(SpecialFlags.SPECIAL_BUILD) for tl in self._tiles]

        self._tiles_around = []
        [[self._tiles_around.append(pl) for pl in c.get_neighbors_around() if pl not in self._tiles] for c in self._tiles]

        for tl in self._tiles_around:
            if tl.get_type() != MapTileTexture.T_FIELD and next((True for c in tl.get_neighbors_around() if c.get_is_build_except(self)), False):
                tl.set_type_update(MapTileTexture.T_DUST)

        self._places_around = []

        plc = self._tile.get_places()
        st_y = plc[2].get_pos_y()
        st_x = plc[2].get_pos_x()

        for fy in range(st_y - self._spec.get_tiles_height() * 2, st_y + 2):
            for fx in range(st_x - 1, st_x + self._spec.get_tiles_width() * 2 + 1):
                pl = ITileCollector.get().get_place(fx, fy)

                if pl is not None and pl not in self._places:
                    self._places_around.append(pl)

        self._width = self._spec.get_tiles_width() * 50
        self._height = self._spec.get_tiles_height() * 50

        sx = self._tiles[0].get_x()
        sy = self._tiles[0].get_y()
        ex = self._tiles[0].get_x()
        ey = self._tiles[0].get_y()

        for tl in self._tiles:
            sx = min(sx, tl.get_x())
            sy = min(sy, tl.get_y())
            ex = max(ex, tl.get_x())
            ey = max(ey, tl.get_y())

            area = MapTileArea(self, 250)
            area.set_coords(tl.get_x(), tl.get_y())

            self._tile_areas.append(area)

        for pl in self._places:
            pl.build_map(self)

        for pl in self._places:
            pl.update_acc_spread()

        self._x = (ex + sx) / 2
        self._y = (ey + sy) / 2

        self._effects = []

        if self._spec.get_random_rotate():
            self._angle = (self._tile.get_pos_x() + self._tile.get_pos_y()) % 4 * (math.pi / 2)
        else:
            self._angle = 0

        self._state_tiles = []

        for tile in self._tiles:
            self._state_tiles.append(BuildStateTile(self, tile))

        self._sprite_init = True
        self.may_draw = 1
        self._batches.add_drawable(self)

        if quick_ready:
            self.set_ready(data)

    def set_ready(self, data=None):
        if self._is_ready_flag > 0:
            return True

        [tl.set_special(SpecialFlags.SPECIAL_NONE) for tl in self._tiles]

        tls = self._spec.get_special_places()

        if tls is not None:
            for c in tls:
                place = self._tile.get_place().get_place_add_xy(c[1], c[2])
                place.get_tile().set_special(c[0], c[3])

                self._places_special.append(place)

                if not c[0] in self._places_special_dict:
                    self._places_special_dict[c[0]] = []

                self._places_special_dict[c[0]].append((place, c[3]))

                if c[0] == SpecialFlags.SPECIAL_MINER:
                    unit_spec = UnitSpec.get(self._side.get_type() + "_miner")

                    if self._side.check_max_unit(unit_spec):
                        mm = self._side.add_unit(place, unit_spec)
                        mm.set_angle_deg(c[3]["angle_dist"].get_angle_deg())
                        mm.get_angle_dist().set_angle_dist(c[3]["angle_dist"])

                        if data is not None and "ai" in data:
                            data["ai"].update_unit_stat(mm.get_spec(), created=True)

        for tk_idx in range(0, len(self._spec.get_turret_keys())):
            tref = BuildTurret(self, self._side, BuildTurretSpec.get(self._spec.get_turret_keys()[tk_idx]))
            tref.init(self._tiles[tk_idx])

            if self._turrets is None:
                self._turrets = []

            self._turrets.append(tref)

        if self._spec.get_effects() is not None:
            for c in self._spec.get_effects():
                c = c.copy()
                c["tick"] = c["freq"] + int(random.random() * c["freq_rand"]) + c["freq_offset"]
                self._effects += [c]

        if self._state_tiles is not None:
            [st.set_decay() for st in self._state_tiles]

        if self._spec.get_upgrade_max() > 0:
            for idx in range(0, self._spec.get_upgrade_max()):
                px = self.get_use_x() - self._spec.get_tiles_width() * 25 + len(self._pend_upgrades) * 25 + 15
                py = self.get_use_y() - self._spec.get_tiles_height() * 25 - 8
                pt = ParticleCollector().add_particle(px, py, 'blank_star', 0, scale=0.4)

                self._pend_upgrades.append({
                    'pt': pt
                })

        SoundCollector().play_sound("create" + str(random.randint(1, 2)), self.get_tile().get_x(), self.get_tile().get_y(), 0.9 + random.random() * 0.2)

        self._is_ready_flag = 1
        self._side.event_finish_build(self)

        return True

    def destroy(self):
        self.may_draw = 2

        if self._travel_particle is not None:
            self._travel_particle.destroy()
            self._travel_particle = None

        if self._travel_rect is not None:
            self._travel_rect.discard()
            self._travel_rect = None

        for u in self._upgrades:
            u['pt'].destroy()

        for u in self._pend_upgrades:
            u['pt'].destroy()

        for area in self._tile_areas:
            area.destroy()

        self._side.rem_build(self)

        for place in self._places_special:
            place.get_tile().set_special(SpecialFlags.SPECIAL_NONE)

        for place in self._places:
            place.build_rem(self)

        sdel = [self._sprite_below, self._sprite, self._sprite_ico]

        for fy in range(0, 2):
            for fx in range(0, 2):
                sdel.append(self._sprite_select[fy][fx])

        self._batches.add_sprite_delete(sdel)

        if self._state_tiles is not None:
            [st.destroy() for st in self._state_tiles]

        if self._turrets is not None:
            [tref.destroy() for tref in self._turrets]

        GraphBatches().kill_drawable(self)

    def draw_destroy(self):
        pass

    def assign_working_action_item(self, action_item):
        if self._action_item is None or self._action_item is action_item:
            self._action_item = action_item
            return True

        else:
            return False

    def unset_working_action_item(self, action_item):
        if self._action_item is action_item:
            self._action_item = None
            return True

        else:
            return False

    def handle_action_item(self, action_item, data=None):
        if self._action_item is not action_item:
            return False

        if action_item.get_spec().get_build_spec() is not None:

            # stavba budovy

            if data is not None and "tile" in data:
                spec = action_item.get_spec().get_build_spec()
                bref = self._side.add_build(data["tile"], spec, data)

                if bref.get_is_ready_flag() == 0:
                    places_create = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE]]
                    places_move = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE_MOVE]]
                    places_final = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE_FINAL]]

                    spec = UnitSpec.get(self._spec.get_side_type() + "_builder")

                    path_fnd = PathFinder(ITileCollector.get())
                    place_list = path_fnd.path_places_follow(places_create + places_move + places_final)

                    mm = self._side.add_unit(places_create[0], spec)
                    mm.set_born(SpecialFlags.BORN_SPAWNING)
                    mm.goto_enforce_places(place_list, self._place_travel, force_step=True)
                    mm.get_builder().set_build_refs(self, bref)
                    mm.set_action_item(action_item)

                    self._side.event_create_build(action_item, bref)
                    self._action_item = None

                else:
                    self._action_item = None

                return True

        elif action_item.get_spec().get_unit_spec() is not None:

            # stavba jednotky

            spec = action_item.get_spec().get_unit_spec()

            places_create = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE]]
            places_move = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE_MOVE]]
            places_final = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE_FINAL]]

            if next((False for c in places_create + places_move if len(c.get_tile().get_moveables()) > 0), True):

                path_fnd = PathFinder(ITileCollector.get())
                place_list = path_fnd.path_places_follow(places_create + places_move + places_final)

                mm = self._side.add_unit(places_create[0], spec)
                mm.set_born(SpecialFlags.BORN_SPAWNING)
                mm.goto_enforce_places(place_list, self._place_travel)
                mm.set_action_item(action_item)

                self._side.event_create_unit(action_item, mm)
                self._action_item = None

                return True

        elif action_item.get_spec().get_upgrade_keys() is not None:

            # vylepseni budovy

            spec = UnitSpec.get(self._spec.get_side_type() + "_upgrade")

            places_create = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE]]
            places_move = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE_MOVE]]
            places_final = [c[0] for c in self._places_special_dict[SpecialFlags.SPECIAL_CREATE_FINAL]]

            path_fnd = PathFinder(ITileCollector.get())
            place_list = path_fnd.path_places_follow(places_create + places_move + places_final)

            mm = self._side.add_unit(places_create[0], spec)
            mm.set_born(SpecialFlags.BORN_SPAWNING)
            mm.goto_enforce_places(place_list, self._place_travel, force_step=True)
            mm.get_upgrade().set_build_refs(self, action_item.get_build_trg_ref())
            mm.set_action_item(action_item)

            self._side.event_upgrade(action_item, self)
            self._action_item = None

            return True

        return False

    def add_upgrade(self):
        if self.get_may_upgrade():
            if len(self._pend_upgrades) > 0:
                self._pend_upgrades[0]['pt'].destroy()
                del self._pend_upgrades[0]

            px = self.get_use_x() - self._spec.get_tiles_width() * 25 + len(self._upgrades) * 25 + 15
            py = self.get_use_y() - self._spec.get_tiles_height() * 25 - 8
            pt = ParticleCollector().add_particle(px, py, 'white_star', 0, scale=1, scale_to=0.4, scale_div=8)

            self._upgrades.append({
                'pt': pt
            })

            self._sounds.play_sound("upgrade", self.get_use_x(), self.get_use_y(), 0.9 - random.random() * 0.2)
            IGui.get().focus_back(self._side)
            return True

        else:
            return False

    def serialize(self):
        out = {
            'number': self.number,
            'side_number': self._side.number,
            'spec': self._spec.get_key(),
            'is_ready_flag': self._is_ready_flag,
            'tile_x': self.get_tile().get_pos_x(),
            'tile_y': self.get_tile().get_pos_y(),
            'place_x': self.get_place().get_pos_x(),
            'place_y': self.get_place().get_pos_y()
        }

        return out

    def update(self, tick):
        if not self.get_is_alive():
            return True

        if self._is_ready_flag == 1:
            self._opacity = min(self._opacity + 5, 255)

            if self._opacity == 10:
                IGui.get().focus_back(self._side)
            elif self._opacity == 255:
                self._is_ready_flag = 2

        if self._dmg_hit_timeout > 0:
            self._dmg_hit_timeout -= 1

        if self.get_is_damaged():
            if self._dmg_smoke_tick == 0:
                c = self.get_coords(round(self._width / 2))
                self._particles.add_particle(c[0], c[1], "smoke", -1, scale=1.1 - random.random() * 0.5)

                if self.get_is_heavy_damaged():
                    self._dmg_smoke_tick = 40 + random.randint(0, 50)
                else:
                    self._dmg_smoke_tick = 70 + random.randint(0, 60)

            else:
                self._dmg_smoke_tick -= 1

            if self._dmg_fire_tick == 0:
                flg = "fire1" if random.randint(0, 1) == 0 else "fire2"
                c = self.get_coords(round(self._width / 1.5))
                self._particles.add_particle(c[0], c[1], flg, -1, scale=1.1 - random.random() * 0.4)

                if self.get_is_heavy_damaged():
                    self._dmg_fire_tick = 20 + random.randint(0, 10)
                else:
                    self._dmg_fire_tick = 40 + random.randint(0, 15)

            else:
                self._dmg_fire_tick -= 1

        for c in self._effects:
            c["tick"] -= 1

            if c["tick"] == 0:
                c["callback"](self._particles, self._x + c["x"], self._y + c["y"])
                c["tick"] = c["freq"] + int(random.random() * c["freq_rand"])

        if self._state_tiles is not None:
            rem = []

            for st in self._state_tiles:
                if st.update():
                    rem.append(st)

            for st in rem:
                st.destroy()
                self._state_tiles.remove(st)

            if len(self._state_tiles) == 0:
                self._state_tiles = None

        if self._turrets is not None:
            for tref in self._turrets:
                tref.update(tick)

        return False

    def draw(self):
        if self.may_draw != 1:
            return False

        vis = False

        for tl in self._tiles:
            if tl.get_visible():
                vis = True
                break

        if vis != self._visible:
            self._visible = vis

            if self._visible:
                self._sprite_init = True
            else:
                sdel = [self._sprite_below, self._sprite, self._sprite_ico]

                for fy in range(0, 2):
                    for fx in range(0, 2):
                        sdel.append(self._sprite_select[fy][fx])

                self._batches.add_sprite_delete(sdel)

                self._sprite = None
                self._sprite_below = None
                self._sprite_ico = None
                self._sprite_select = [[None, None], [None, None]]

        if self._visible:
            if self._sprite_init:
                self._sprite_init = False

                imgs = ImageCollector()

                if self._spec.get_tex_below() is not None:
                    self._sprite_below = self._batches.create_sprite(img=self._spec.get_tex_below(self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_below(), batch=self._batches.get_batch())
                else:
                    self._sprite_below = None

                self._sprite = self._batches.create_sprite(img=self._spec.get_tex(self._side.get_color()), x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._batches.get_group_build(), batch=self._batches.get_batch())
                self._sprite_select[0][0] = self._batches.create_sprite(img=imgs.get_image("select_LT"), x=self.get_sprite_sel_x(0, 0), y=self.get_sprite_sel_y(0, 0), group=self._batches.get_group_particle(), batch=self._batches.get_batch())
                self._sprite_select[0][1] = self._batches.create_sprite(img=imgs.get_image("select_RT"), x=self.get_sprite_sel_x(0, 1), y=self.get_sprite_sel_y(0, 1), group=self._batches.get_group_particle(), batch=self._batches.get_batch())
                self._sprite_select[1][0] = self._batches.create_sprite(img=imgs.get_image("select_LD"), x=self.get_sprite_sel_x(1, 0), y=self.get_sprite_sel_y(1, 0), group=self._batches.get_group_particle(), batch=self._batches.get_batch())
                self._sprite_select[1][1] = self._batches.create_sprite(img=imgs.get_image("select_RD"), x=self.get_sprite_sel_x(1, 1), y=self.get_sprite_sel_y(1, 1), group=self._batches.get_group_particle(), batch=self._batches.get_batch())

                for fy in range(0, 2):
                    for fx in range(0, 2):
                        self._sprite_select[fy][fx].visible = False

                if self._spec.get_tex_ico_round() is not None:
                    self._sprite_ico = self._batches.create_sprite(img=self._spec.get_tex_ico_round(), x=self.get_ico_x(), y=self.get_ico_y(), group=self._batches.get_group_particle(), batch=self._batches.get_batch())

            if self._sprite_up_damage == 1:
                self._sprite.image = self._spec.get_tex_damage2(self._side.get_color())
                self._sprite_up_damage = 0

            elif self._sprite_up_damage == 2:
                self._sprite.image = self._spec.get_tex_damage1(self._side.get_color())
                self._sprite_up_damage = 0

            self._sprite.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._builds.get_zoom(), rotation=angle_conv_realdeg(self._angle))
            self._sprite.opacity = self._opacity

            if self._sprite_below is not None:
                self._sprite_below.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._builds.get_zoom(), rotation=angle_conv_realdeg(self._angle))
                self._sprite_below.opacity = self._opacity

            if self._sprite_ico is not None:
                self._sprite_ico.update(x=self.get_ico_x(), y=self.get_ico_y(), scale=self._builds.get_zoom())
                self._sprite_ico.visible = self._selected

            for fy in range(0, 2):
                for fx in range(0, 2):
                    if self._selected:
                        self._sprite_select[fy][fx].update(x=self.get_sprite_sel_x(fy, fx), y=self.get_sprite_sel_y(fy, fx), scale=self._builds.get_zoom())
                        self._sprite_select[fy][fx].visible = True
                    else:
                        self._sprite_select[fy][fx].visible = False

        if self._state_tiles is not None:
            [st.draw() for st in self._state_tiles]
