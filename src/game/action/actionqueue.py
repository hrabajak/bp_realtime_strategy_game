from game.action.actionitem import ActionItem
from game.action.actionitemspec import ActionItemSpec
from game.interface.igui import IGui


class ActionQueue:

    _side = None

    _action_queue = None
    _action_rem = None

    _event_avail: (any, any) = lambda self, data: None

    def __init__(self, side):
        self._side = side
        self._action_queue = []
        self._action_rem = []

    def get_action_queue(self):
        return self._action_queue

    def action_enqueue(self, action: ActionItemSpec, build_ref, build_trg_ref=None):
        it = ActionItem(action, self._side, build_ref, build_trg_ref)
        self._action_queue += [it]
        return it

    def get_spec_stacked_count(self, action_spec: ActionItemSpec):
        return sum([1 for ac in self._action_queue if ac.get_spec() is action_spec])

    def get_build_stacked_count(self, build_ref):
        return sum([1 for ac in self._action_queue if ac.get_build_ref() is build_ref])

    def get_build_spec_stacked_count(self, build_spec):
        return sum([1 for ac in self._action_queue if ac.get_spec().get_build_spec() is build_spec])

    def get_unit_spec_stacked_count(self, unit_spec):
        return sum([1 for ac in self._action_queue if ac.get_spec().get_unit_spec() is unit_spec])

    def get_item_by_number(self, number: int):
        for ac in self._action_queue:
            if ac.number == number:
                return ac

        return None

    def get_avail_action_item(self):
        return next((ac for ac in self._action_queue if ac.get_avail()), None)

    def action_avail(self, ac: ActionItem):
        ac.avail()
        IGui.get().action_item_notify(ac)

    def action_trigger(self, ac: ActionItem, data=None):
        return ac.trigger(data)

    def action_handle(self, ac: ActionItem, data=None):
        if ac.handle(data):
            self._action_queue.remove(ac)

            return True
        else:
            return False

    def action_cancel(self, ac: ActionItem):
        if ac in self._action_queue:
            ac.refund()
            self._action_queue.remove(ac)

    def action_refund(self, ac: ActionItem):
        if ac.refund():
            self._action_queue.remove(ac)

    def serialize(self):
        out = {
            'action_queue': [c.serialize() for c in self._action_queue]
        }

        return out

    def update(self):
        for ac in [c for c in self._action_queue]:
            if ac.update() and ac.handle():
                self._action_queue.remove(ac)
