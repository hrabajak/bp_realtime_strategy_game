from game.build.buildspec import BuildSpec
from game.config import Config
from game.interface.iplayersides import IPlayerSides
from game.moveable.unitspec import UnitSpec


class ActionItemSpec:

    _specs = {}
    _specs_by_num = {}
    _key = None
    _key_base = None
    _name = None
    _delay = 0
    _money_cost = 0
    _image_name = None
    _unit_spec: UnitSpec = None
    _build_spec: BuildSpec = None
    _man_builds = []
    _is_turret = False
    _level = 0
    _upgrade_keys = None

    number = 0
    _number_total = 0

    def __init__(self, key):
        self._key = key
        self._key_base = key[3:]

        self.number = ActionItemSpec._number_total
        ActionItemSpec._number_total += 1

    def get_key(self):
        return self._key

    def get_key_base(self):
        return self._key_base

    def get_name(self):
        return self._name

    def get_delay(self):
        return 1 if IPlayerSides.get().get_worker().get_quick_progress() else self._delay

    def get_money_cost(self):
        return self._money_cost

    def get_image_name(self):
        return self._image_name

    def get_unit_spec(self):
        return self._unit_spec

    def get_build_spec(self):
        return self._build_spec

    def get_is_turret(self):
        return self._is_turret

    def get_man_builds(self):
        return self._man_builds

    def get_level(self):
        return self._level

    def get_upgrade_keys(self):
        return self._upgrade_keys

    @staticmethod
    def init_specs():
        # === side 1 ===

        s = ActionItemSpec("s1_main")
        s._name = "Build main"
        s._delay = 60 * 5
        s._money_cost = 3000
        s._build_spec = BuildSpec.get("s1_main")
        s._image_name = "gui_b_main"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_barracks")
        s._name = "Build barracks"
        s._delay = 60 * 2
        s._money_cost = 1000
        s._build_spec = BuildSpec.get("s1_barracks")
        s._man_builds = ["s1_refinery"]
        s._image_name = "gui_b_barracks"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_factory")
        s._name = "Build factory"
        s._delay = 60 * 5
        s._money_cost = 1500
        s._build_spec = BuildSpec.get("s1_factory")
        s._man_builds = ["s1_refinery"]
        s._image_name = "gui_b_factory"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_refinery")
        s._name = "Build refinery"
        s._delay = 60 * 5
        s._money_cost = 1500
        s._build_spec = BuildSpec.get("s1_refinery")
        s._image_name = "gui_b_rafinery"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_lab")
        s._name = "Build laboratory"
        s._delay = 60 * 5
        s._money_cost = 1200
        s._build_spec = BuildSpec.get("s1_lab")
        s._man_builds = ["s1_factory", "s1_barracks"]
        s._image_name = "gui_b_laboratory"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_upgrade")
        s._name = "Tech. upgrade"
        s._delay = 60 * 5
        s._money_cost = 1200
        s._upgrade_keys = ["s1_factory", "s1_barracks", "s1_main"]
        s._image_name = "gui_b_upgrade"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_turret")
        s._name = "Minigun turret"
        s._delay = 60 * 5
        s._money_cost = 1000
        s._build_spec = BuildSpec.get("s1_turret")
        s._is_turret = True
        s._man_builds = ["s1_refinery"]
        s._image_name = "gui_b_turret"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_turret_can")
        s._name = "Cannon turret"
        s._delay = 60 * 5
        s._money_cost = 1500
        s._build_spec = BuildSpec.get("s1_turret_can")
        s._level = 1
        s._is_turret = True
        s._man_builds = ["s1_refinery"]
        s._image_name = "gui_b_turret_sec"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_troop")
        s._name = "Trooper"
        s._delay = 60
        s._money_cost = 100
        s._unit_spec = UnitSpec.get("s1_troop")
        s._image_name = "gui_s1_troop"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_troop_cannon")
        s._name = "Cannon trooper"
        s._delay = 60
        s._money_cost = 200
        s._unit_spec = UnitSpec.get("s1_troop_cannon")
        s._level = 1
        s._image_name = "gui_s1_troop_cannon"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_sniper")
        s._name = "Sniper"
        s._delay = 60
        s._money_cost = 300
        s._unit_spec = UnitSpec.get("s1_sniper")
        s._level = 2
        s._image_name = "gui_s1_sniper"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_jeep")
        s._name = "Jeep"
        s._delay = 120
        s._money_cost = 300
        s._unit_spec = UnitSpec.get("s1_jeep")
        s._image_name = "gui_s1_jeep"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_plate")
        s._name = "Plated tank"
        s._delay = 183
        s._money_cost = 750
        s._unit_spec = UnitSpec.get("s1_plate")
        s._level = 1
        s._image_name = "gui_s1_plate"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_lighttank")
        s._name = "Light tank"
        s._delay = 211
        s._money_cost = 1000
        s._unit_spec = UnitSpec.get("s1_lighttank")
        s._level = 1
        s._image_name = "gui_s1_lighttank"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_medtank")
        s._name = "Medium tank"
        s._delay = 324
        s._money_cost = 2000
        s._unit_spec = UnitSpec.get("s1_medtank")
        s._level = 2
        s._image_name = "gui_s1_medtank"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_rocket")
        s._name = "Rocket lnch."
        s._delay = 113
        s._money_cost = 1000
        s._unit_spec = UnitSpec.get("s1_rocket")
        s._level = 3
        s._image_name = "gui_s1_rocket"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s1_miner")
        s._name = "Harvester"
        s._delay = 240
        s._money_cost = 1000
        s._unit_spec = UnitSpec.get("s1_miner")
        s._level = 0
        s._image_name = "gui_s1_miner"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        # === side 2 ===

        s = ActionItemSpec("s2_main")
        s._name = "Build main"
        s._delay = 60 * 5
        s._money_cost = 3000
        s._build_spec = BuildSpec.get("s2_main")
        s._image_name = "gui_b_main"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_factory")
        s._name = "Build factory"
        s._delay = 60 * 5
        s._money_cost = 1500
        s._build_spec = BuildSpec.get("s2_factory")
        s._man_builds = ["s2_refinery"]
        s._image_name = "gui_b_factory"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_barracks")
        s._name = "Build barracks"
        s._delay = 60 * 2
        s._money_cost = 1000
        s._build_spec = BuildSpec.get("s2_barracks")
        s._man_builds = ["s2_refinery"]
        s._image_name = "gui_b_barracks"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_refinery")
        s._name = "Build refinery"
        s._delay = 60 * 5
        s._money_cost = 1500
        s._build_spec = BuildSpec.get("s2_refinery")
        s._image_name = "gui_b_rafinery"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_lab")
        s._name = "Build laboratory"
        s._delay = 60 * 5
        s._money_cost = 1200
        s._build_spec = BuildSpec.get("s2_lab")
        s._man_builds = ["s2_factory", "s2_barracks"]
        s._image_name = "gui_b_laboratory"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_upgrade")
        s._name = "Tech. upgrade"
        s._delay = 60 * 5
        s._money_cost = 1000
        s._upgrade_keys = ["s2_factory", "s2_barracks", "s2_main"]
        s._image_name = "gui_b_upgrade"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_turret")
        s._name = "Laser turret"
        s._delay = 60 * 5
        s._money_cost = 1000
        s._build_spec = BuildSpec.get("s2_turret")
        s._is_turret = True
        s._man_builds = ["s2_refinery"]
        s._image_name = "gui_b_turret"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_turret_rock")
        s._name = "Rocket turret"
        s._delay = 60 * 5
        s._money_cost = 1500
        s._build_spec = BuildSpec.get("s2_turret_rock")
        s._level = 1
        s._is_turret = True
        s._man_builds = ["s2_refinery"]
        s._image_name = "gui_b_turret_sec"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_troop")
        s._name = "Trooper"
        s._delay = 60
        s._money_cost = 110
        s._unit_spec = UnitSpec.get("s2_troop")
        s._image_name = "gui_s2_troop"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_troop_rocket")
        s._name = "Trooper rocket"
        s._delay = 60
        s._money_cost = 200
        s._unit_spec = UnitSpec.get("s2_troop_rocket")
        s._level = 1
        s._image_name = "gui_s2_troop_rocket"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_troop_beam")
        s._name = "Trooper beam"
        s._delay = 60
        s._money_cost = 250
        s._unit_spec = UnitSpec.get("s2_troop_beam")
        s._level = 2
        s._image_name = "gui_s2_troop_beam"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_basic")
        s._name = "Basic"
        s._delay = 85
        s._money_cost = 300
        s._unit_spec = UnitSpec.get("s2_basic")
        s._image_name = "gui_s2_basic"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_mini")
        s._name = "Minigun veh."
        s._delay = 127
        s._money_cost = 600
        s._unit_spec = UnitSpec.get("s2_mini")
        s._level = 1
        s._image_name = "gui_s2_mini"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_rock")
        s._name = "Rocket veh."
        s._delay = 183
        s._money_cost = 1100
        s._unit_spec = UnitSpec.get("s2_rock")
        s._level = 1
        s._image_name = "gui_s2_rock"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_plasma")
        s._name = "Plasma tank"
        s._delay = 255
        s._money_cost = 1400
        s._unit_spec = UnitSpec.get("s2_plasma")
        s._level = 2
        s._image_name = "gui_s2_plasma"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_partilery")
        s._name = "Artilery tank"
        s._delay = 113
        s._money_cost = 1000
        s._unit_spec = UnitSpec.get("s2_partilery")
        s._level = 3
        s._image_name = "gui_s2_partilery"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        s = ActionItemSpec("s2_miner")
        s._name = "Harvester"
        s._delay = 240
        s._money_cost = 1000
        s._unit_spec = UnitSpec.get("s2_miner")
        s._level = 0
        s._image_name = "gui_s2_miner"

        ActionItemSpec._specs[s.get_key()] = s
        ActionItemSpec._specs_by_num[s.number] = s

        for s in ActionItemSpec._specs.values():
            if s._unit_spec is not None:
                s._unit_spec.set_build_money_cost(s._money_cost)
                s._unit_spec.set_build_delay(s._delay)

        #UnitSpec.log_table()

    @staticmethod
    def get(key):
        return ActionItemSpec._specs[key] if key in ActionItemSpec._specs else None

    @staticmethod
    def get_by_number(num):
        return ActionItemSpec._specs_by_num[num]
