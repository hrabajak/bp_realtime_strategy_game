from game.action.actionitemspec import ActionItemSpec


class ActionItem:

    _spec: ActionItemSpec = None
    _side = None
    _build_ref = None
    _build_trg_ref = None
    _tick = 0
    _flag = False
    _avail = False
    _handled = False
    _hash_key = None

    cust_data = None
    number = 0
    _number_total = 0

    def __init__(self, spec: ActionItemSpec, side, build_ref, build_trg_ref=None):
        self._spec = spec
        self._side = side
        self._build_ref = build_ref
        self._build_trg_ref = build_trg_ref
        self._tick = 0
        self._flag = False
        self._avail = False
        self._handled = False
        self._hash_key = spec.get_key()

        if build_ref is not None:
            self._hash_key += "~" + str(build_ref.number)
        if build_trg_ref is not None:
            self._hash_key += "~T" + str(build_trg_ref.number)

        self.number = ActionItem._number_total
        ActionItem._number_total += 1

    def get_spec(self):
        return self._spec

    def get_side(self):
        return self._side

    def get_build_ref(self):
        return self._build_ref

    def get_build_trg_ref(self):
        return self._build_trg_ref

    def get_flag(self):
        return self._flag

    def get_avail(self):
        return self._avail

    def get_name(self, count=1):
        if self._flag:
            return self._spec.get_name() + ("" if count <= 1 else (" x " + str(count)))
        else:
            return self._spec.get_name() + ("" if count <= 1 else (" x " + str(count))) + "\n(" + str(round((self._tick / self._spec.get_delay()) * 100)) + " %)"

    def get_hash_key(self):
        return self._hash_key

    def serialize(self):
        return {
            'number': self.number,
            'spec': self._spec.get_key(),
            'tick': self._tick,
            'flag': self._flag,
            'avail': self._avail
        }

    def avail(self):
        self._side.event_action_avail(self)
        self._avail = True

    def trigger(self, data=None):
        if self._flag and self._build_ref.get_is_alive():
            if self._spec.get_build_spec() is not None:
                data["gui"].focus_building(self._spec.get_build_spec(), self)
                return True

        return False

    def refund(self):
        if not self._build_ref.get_is_alive() or not self._build_ref.unset_working_action_item(self):
            return True

        self._side.money_add(self._spec.get_money_cost())
        return True

    def handle(self, data=None):
        if not self._build_ref.get_is_alive():
            return True

        elif not self._flag:
            return False

        elif self._build_ref.handle_action_item(self, data):
            return True

        elif self._spec.get_unit_spec() is not None:
            self._flag = False

        return False

    def update(self):
        if not self._build_ref.get_is_alive() or not self._build_ref.assign_working_action_item(self):
            return True

        elif self._flag:
            return False

        elif self._tick < self._spec.get_delay():
            self._tick += 1
            return False

        elif not self._flag:
            self._flag = True
            self._side.get_actions().action_avail(self)
            return True
