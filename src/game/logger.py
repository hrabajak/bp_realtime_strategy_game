

class Logger:

    _ref = None

    def __init__(self):
        pass

    def logm(self, value):
        print(value)

    def log(self, value):
        print(value)

    @staticmethod
    def get():
        if Logger._ref is None:
            Logger._ref = Logger()

        return Logger._ref
