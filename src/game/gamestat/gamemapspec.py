

class GameMapSpec:

    _seed = 0
    _blank = False
    _map_width = 0
    _map_height = 0

    def __init__(self):
        self._seed = 0
        self._blank = False

    def get_seed(self):
        return self._seed

    def get_blank(self):
        return self._blank

    def get_map_width(self):
        return self._map_width

    def get_map_height(self):
        return self._map_height

    def get_readable_caption(self):
        if self._blank:
            return 'blank'
        else:
            return 'seed ' + str(self._seed)

    def set_seed(self, seed):
        self._seed = seed

    def set_blank(self, blank):
        self._blank = blank

    def set_map_size(self, map_width, map_height):
        self._map_width = map_width
        self._map_height = map_height

    def serialize(self):
        return {
            'seed': self._seed,
            'blank': self._blank,
            'map_width': self._map_width,
            'map_height': self._map_height
        }

    def unserialize(self, dt):
        self._seed = dt['seed']
        self._blank = dt['blank']
        self._map_width = dt['map_width']
        self._map_height = dt['map_height']

        return self
