

class GamePlayerSpec:

    _side_type = 's1'
    _ai = False
    _money = 2000
    _color = None

    def __init__(self, side_type='s1', ai='human', money=2000, color=None):
        self._side_type = side_type
        self._ai = ai
        self._money = money
        self._color = color

    def get_side_type(self):
        return self._side_type

    def get_ai(self):
        return self._ai

    def get_money(self):
        return self._money

    def get_color(self):
        return self._color

    def serialize(self):
        return {
            'side_type': self._side_type,
            'ai': self._ai,
            'money': self._money,
            'color': self._color
        }

    def unserialize(self, dt):
        self._side_type = dt['side_type']
        self._ai = dt['ai']
        self._money = dt['money']
        self._color = dt['color']

        return self
