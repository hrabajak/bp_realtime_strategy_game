import threading

from game.build.buildcollector import BuildCollector
from game.config import Config
from game.gamestat.gamespec import GameSpec
from game.interface.ievents import IEvents
from game.logger import Logger
from game.map.mapbase import MapBase
from game.moveable.moveablecollector import MoveableCollector
from game.network.command import Command
from game.player.playersidecollector import PlayerSideCollector


class GameContext:

    _thread = None
    _evt = None

    _spec: GameSpec = None
    _map: MapBase = None
    _moveables: MoveableCollector = None
    _builds: BuildCollector = None
    _sides: PlayerSideCollector = None
    _ready_flag = False
    _started_flag = False
    _desync_flag = False
    _connections = None

    number = 0
    _number_total = 1

    def __init__(self, spec: GameSpec):
        self.number = GameContext._number_total
        GameContext._number_total += 1

        self._spec = spec
        self._spec.set_ctx_number(self.number)

        self._ready_flag = False
        self._started_flag = False
        self._desync_flag = False
        self._connections = []

    def get_spec(self):
        return self._spec

    def get_ready_flag(self):
        return self._ready_flag

    def get_started_flag(self):
        return self._started_flag

    def get_ready_count(self):
        return len([c for c in self._connections if c['ready']])

    def get_waiting_count(self):
        return len(self._spec.get_players()) - self.get_ready_count()

    def get_is_desync(self):
        return self._desync_flag

    def get_is_corrupted(self):
        return self._started_flag and (len(self._connections) < len(self._spec.get_players()) or self._desync_flag)

    def get_is_dead(self):
        return self._started_flag and len(self._connections) == 0

    def get_readable_caption(self):
        out = self._spec.get_readable_caption()

        if self._started_flag:
            out += ', started (' + str(len(self._connections)) + ' / ' + str(len(self._spec.get_players())) + ')'
        elif self._ready_flag:
            out += ', ready to connect (' + str(len(self._connections)) + ' / ' + str(len(self._spec.get_players())) + ')'
        else:
            out += ', preparing ...'

        return out

    def get_info(self):
        return {
            'spec': self._spec.serialize(),
            'ready_flag': self._ready_flag,
            'readable': self.get_readable_caption(),
            'ctx_number': self.number
        }

    def get_side_by_connection(self, conn):
        return self._sides.get_side_by_index(self.connection_get_index(conn))

    def set_desync(self):
        self._desync_flag = True

    def connection_assign(self, conn):
        if not self._started_flag and len(self._connections) < len(self._spec.get_players()) and self.connection_get_index(conn) == -1:
            self._connections.append({
                'conn': conn,
                'ready': False
            })

    def connection_set_ready(self, conn):
        idx = self.connection_get_index(conn)
        if idx != -1:
            self._connections[idx]['ready'] = True

            if self.get_waiting_count() == 0:
                self._started_flag = True

            return True
        else:
            return False

    def connection_get_index(self, conn):
        for idx in range(0, len(self._connections)):
            if self._connections[idx]['conn'] is conn:
                return idx
        return -1

    def connection_leave(self, conn):
        idx = self.connection_get_index(conn)
        if idx != -1:
            del self._connections[idx]

    def init(self, evt: IEvents):
        self._evt = evt
        self._ready_flag = False

        self._thread = threading.Thread(target=GameContext.init_call, args=(self,))
        self._thread.start()

    def init_call(self):
        Logger.get().logm('(gamecontext) Game context init started')

        conf = Config.get()

        self._map = MapBase(new=True)
        self._map.build(50, 50)
        #self._map.build(100, 100)

        Logger.get().logm('(gamecontext) Generating map')

        if self._spec.get_map().get_blank():
            self._map.generate_blank(len(self._spec.get_players()))
        else:
            self._map.regenerate_b(self._spec.get_map().get_seed(), len(self._spec.get_players()))

        self._sides = PlayerSideCollector(new=True)

        self._moveables = MoveableCollector(new=True)
        self._moveables.set_refs(self._map, self._sides, None)

        self._builds = BuildCollector(new=True)
        self._builds.set_refs(self._map, self._sides, None)

        self._sides.set_refs(self._moveables, self._builds, self._map, None)

        for pl in self._spec.get_players():
            self._sides.add_side(pl)

        self._sides.build_units()

        Logger.get().logm('(gamecontext) Game context ' + str(self.number) + ' initialized')

        self._ready_flag = True
        self._evt.event_add(Command.T_GAME_START, {'ctx_number': self.number})

    def serialize(self, conn):
        dt = {
            'number': self.number,
            'side_number': -1,
            'map': self._map.serialize(),
            'sides': self._sides.serialize(),
            'moveables': self._moveables.serialize(),
            'builds': self._builds.serialize(),
            'spec': self._spec.serialize()
        }

        side = self.get_side_by_connection(conn)

        if side is not None:
            dt['side_number'] = side.number

        return dt
