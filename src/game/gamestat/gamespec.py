from game.gamestat.gamemapspec import GameMapSpec
from game.gamestat.gameplayerspec import GamePlayerSpec


class GameSpec:

    TYPE_NORMAL = 0
    TYPE_ONLINE = 1
    TYPE_BATTLE = 2

    _ctx_number = 0
    _game_type = 0
    _players = None
    _server_game = False
    _map: GameMapSpec = None
    _fog_type = True
    _quick_progress = False
    _quick_build = False
    _quick_mine = False

    def __init__(self):
        self._ctx_number = 0
        self._game_type = GameSpec.TYPE_NORMAL
        self._players = []
        self._server_game = False
        self._fog_type = 0
        self._quick_progress = False
        self._quick_build = False
        self._quick_mine = False
        self._map = GameMapSpec()

    def get_ctx_number(self):
        return self._ctx_number

    def get_game_type(self):
        return self._game_type

    def get_players(self):
        return self._players

    def get_server_game(self):
        return self._server_game

    def get_fog_type(self):
        return self._fog_type

    def get_quick_progress(self):
        return self._quick_progress

    def get_quick_build(self):
        return self._quick_build

    def get_quick_mine(self):
        return self._quick_mine

    def get_map(self) -> GameMapSpec:
        return self._map

    def get_readable_caption(self):
        out = '#' + str(self._ctx_number) + ', '
        out += 'players: ' + str(len(self._players)) + 'x, '
        out += 'map: ' + self._map.get_readable_caption()

        return out

    def set_ctx_number(self, ctx_number):
        self._ctx_number = ctx_number

    def set_game_type(self, game_type):
        self._game_type = game_type

    def set_server_game(self, server_game):
        self._server_game = server_game

    def set_fog_type(self, fog_type):
        self._fog_type = fog_type

    def set_quick_progress(self, quick_progress):
        self._quick_progress = quick_progress

    def set_quick_build(self, quick_build):
        self._quick_build = quick_build

    def set_quick_mine(self, quick_mine):
        self._quick_mine = quick_mine

    def add_player(self, pl: GamePlayerSpec):
        self._players.append(pl)

    def serialize(self):
        return {
            'ctx_number': self._ctx_number,
            'players': [c.serialize() for c in self._players],
            'server_game': self._server_game,
            'fog_type': self._fog_type,
            'quick_progress': self._quick_progress,
            'quick_build': self._quick_build,
            'quick_mine': self._quick_mine,
            'map': self._map.serialize()
        }

    def unserialize(self, dt):
        try:
            self._ctx_number = dt['ctx_number']
            self._players = [GamePlayerSpec().unserialize(c) for c in dt['players']]
            self._server_game = dt['server_game']
            self._fog_type = dt['fog_type']
            self._quick_progress = dt['quick_progress']
            self._quick_build = dt['quick_build']
            self._quick_mine = dt['quick_mine']
            self._map = GameMapSpec().unserialize(dt['map'])

        except KeyError:
            pass
        except ValueError:
            pass

        return self
