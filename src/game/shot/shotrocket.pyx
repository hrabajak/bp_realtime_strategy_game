import random

from libc.math cimport sin, cos, M_PI

from game.graphic.graphbatches import GraphBatches
from game.helper.discrete cimport angle_coords, get_diff_from_angle, get_angle_from
from game.helper.distangle import DistAngle
from game.helper.line cimport line_point, make_line_step
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.helper.vector cimport angle_to, angle_conv, angle_conv_realdeg
from game.shot.shotbase cimport ShotBase
from game.shot.shotsplash import ShotSplash


cdef class ShotRocket(ShotBase):

    cdef:
        object _sprite
        int _sprite_init

        object _light
        int _light_inc

        object _owner
        object _target
        object _target_place
        long _bef_x
        long _bef_y
        long _x
        long _y
        long _st_x
        long _st_y
        long _speed
        long _acc
        long _steps
        long _ignition_steps
        object _trg_coords
        double _angle
        long _dilation
        long _dilation_step

        long _tick
        int _smoke

    def __cinit__(self, shots, particles, sounds, shot_spec, shot_add):
        ShotBase.__init__(self, shots, particles, sounds, shot_spec, shot_add)

        self._sprite_init = False
        self._light_inc = 0
        self._bef_x = 0
        self._bef_y = 0
        self._x = 0
        self._y = 0
        self._st_x = 0
        self._st_y = 0
        self._steps = 0
        self._angle = 0
        self._dilation = 0
        self._dilation_step = 0
        self._tick = 0
        self._smoke = False

    def set_moveables(self, owner, target: ITargetable):
        cdef angle_coords c
        cdef long diff_x, diff_y, ang_deg, steps
        cdef object cd

        self._owner = owner

        cd = self._owner.get_use_coords()
        self._x = cd[0]
        self._y = cd[1]
        self._st_x = cd[0]
        self._st_y = cd[1]
        self._bef_x = self._x
        self._bef_y = self._y

        self._target = target
        self._target_place = self._target.get_target_place(self._st_x, self._st_y)

        diff_x = self._target_place.get_pos_x() - self._owner.get_place().get_pos_x()
        diff_y = self._target_place.get_pos_y() - self._owner.get_place().get_pos_y()

        ang_deg = get_angle_from(diff_x, diff_y)

        steps = (self._owner.get_spec().get_range() // 25) + 4
        c = get_diff_from_angle(ang_deg, steps)

        self._trg_coords = [0, 0]
        self._trg_coords[0] = (c.ox + self._owner.get_place().get_pos_x()) * 25.0 + (25.0 / 2)
        self._trg_coords[1] = (c.oy + self._owner.get_place().get_pos_y()) * 25.0 + (25.0 / 2)

        self._angle = angle_to(self._x, self._y, self._trg_coords[0], self._trg_coords[1])

    def get_sprite_x(self):
        return self._x * self._shots.get_zoom() + self._shots.get_x()

    def get_sprite_y(self):
        return self._y * self._shots.get_zoom() + self._shots.get_y()

    def init(self, shot_num):
        if self._shot_spec.get_key() in ["rocket"]:
            self._speed = 100
            self._acc = 20
            self._ignition_steps = 40

        elif self._shot_spec.get_key() in ["blueshot"]:
            self._speed = 300
            self._acc = 10
            self._dilation = 15
            self._ignition_steps = 3

        self._sprite_init = True
        self.may_draw = 1

        GraphBatches().add_drawable(self)

    def update(self, tile_col: ITileCollector):
        cdef int ignite
        cdef line_point lp

        if self._steps < 550:
            self._bef_x, self._bef_y = self._x, self._y
            ignite = False

            if self._ignition_steps > 0:
                self._ignition_steps -= 1
                lp = make_line_step(self._x, self._y, self._trg_coords[0], self._trg_coords[1], 1)
                self._x, self._y = lp.px, lp.py
                self._steps += 1

                if self._ignition_steps == 0:
                    ignite = True

            else:
                lp = make_line_step(self._x, self._y, self._trg_coords[0], self._trg_coords[1], self._speed // 100)
                self._x, self._y = lp.px, lp.py

                if self._bef_x == self._x and self._bef_y == self._y:
                    self._collide_effect(self._collide_check())
                    return True

                self._steps += self._speed // 100
                self._speed += self._acc

            if ignite:
                if self._shot_spec.get_key() in ["rocket"]:
                    self._light = self._particles.add_light(self._x, self._y, opacity=100, color=(245, 255, 110), size=10)
                    self._light_inc = -10
                    self._smoke = True

                    self._particles.add_particle(self._x, self._y, "boom", -1, scale=1.1 - random.random() * 0.5)
                    self._sounds.play_sound("rocket", self._x, self._y, 1.0 + random.random() * 0.2)

                elif self._shot_spec.get_key() in ["blueshot"]:
                    self._light = self._particles.add_light(self._x, self._y, opacity=50, color=(79, 169, 236), size=40)
                    self._light_inc = -10

            if self._collide_check():
                self._collide_effect(True)
                return True

        else:
            self._collide_effect(False)
            return True

        if self._light is not None:
            if self._light_inc:
                self._light.set_coords(self._x + sin(self._angle) * self._light_inc, self._y + cos(self._angle) * self._light_inc)
            else:
                self._light.set_coords(self._x, self._y)

        if self._dilation > 0:
            self._dilation_step = self._dilation_step + 1

            if self._dilation_step >= self._dilation:
                self._dilation_step = 0

        if self._smoke and self._tick % 3 == 0:
            self._particles.add_particle(self._x, self._y, "smoke2", -1, random.randint(0, 8), scale=1.1 - random.random() * 0.5)

        self._tick += 1

        return False

    def _collide_check(self):
        cdef object place, mm

        place = ITileCollector.get().get_place_by_coords(self._x, self._y)

        if place is not None:
            for mm in place.get_targetables():
                if mm.get_side() is not self._owner.get_side() and mm.get_target_colide_check(self._x, self._y):
                    self._target = mm
                    return True

        return False

    def draw(self):
        if self.may_draw != 1:
            return False

        if self._sprite_init:
            self._sprite_init = False
            self._sprite = GraphBatches().create_sprite(img=self._shot_spec.get_tex(), x=self.get_sprite_x(), y=self.get_sprite_y(), group=GraphBatches().get_group_particle(), batch=GraphBatches().get_batch())
            self._sprite.update(scale=self._shots.get_zoom(), rotation=angle_conv_realdeg(self._angle))

        if self._sprite is not None:
            self._sprite.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._shots.get_zoom(), rotation=angle_conv_realdeg(self._angle))

            if self._dilation > 0:
                self._sprite.update(scale_x=sin((self._dilation_step / self._dilation) * M_PI * 2) * 0.2 + 0.8, scale_y=cos((self._dilation_step / self._dilation) * M_PI * 2) * 0.2 + 0.8)

    def _collide_effect(self, is_hit):
        cdef double cx, cy
        cdef object spl, place

        cx = self._bef_x + sin(self._angle) * (10 + random.random() * 15.0)
        cy = self._bef_y + cos(self._angle) * (10 + random.random() * 15.0)

        if self._shot_spec.get_key() in ["rocket"]:
            self._sounds.play_sound("explosion0", cx, cy, 1.6 + random.random() * 0.3)
            self._particles.add_particle(cx, cy, "exp2", -1, scale=1.1 - random.random() * 0.5)

        elif self._shot_spec.get_key() in ["blueshot"]:
            self._sounds.play_sound("blueshot_hit", cx, cy, 0.4 + random.random() * 0.1)
            self._particles.add_particle(cx, cy, "blueboom", -1, scale=1.0 + random.random() * 0.3)

            self._particles.add_light(self._x, self._y, opacity=50, tick_value=(20, 5, 15), color=(79, 169, 236), size=50)

        if self._shot_spec.get_splash_range() > 0:
            place = ITileCollector.get().get_place_by_coords(self._x, self._y)

            if place is not None:
                spl = ShotSplash(place, self._shot_spec.get_splash_range(), self._owner)
                spl.set_koefs(self._shot_spec.get_splash_koefs())
                spl.set_affect(self._shot_spec.get_splash_affect())
                spl.make(lambda moveable_ref, koef: moveable_ref.deal_damage(self._x, self._y, self._owner, self._shot_spec, self._angle, koef))

    def draw_destroy(self):
        pass

    def destroy(self):
        self.may_draw = 2

        if self._owner.get_is_alive():
            self._owner.set_look_snap(None)

        if self._light is not None:
            self._light.destroy()

        GraphBatches().add_sprite_delete(self._sprite)
        GraphBatches().kill_drawable(self)
