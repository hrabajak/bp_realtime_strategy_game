import random

from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.particle.particlecollector import ParticleCollector
from game.shot.shotcollector import ShotCollector
from game.sound.soundcollector import SoundCollector


class ShotRealize:

    _shots: IShots = None
    _particles: IParticles = None
    _sounds: ISounds = None

    _owner = None
    _target = None
    _spec = None

    def __init__(self, owner, target, spec):
        self._shots: IShots = ShotCollector()
        self._particles: IParticles = ParticleCollector()
        self._sounds: ISounds = SoundCollector()

        self._owner = owner
        self._target = target
        self._spec = spec

    def make(self, shots_count, sound_timeout=0):
        sh = self._shots.add_shot(self._spec.get_shot_spec(), self._owner, self._target, self._spec.get_shot_canon_dist(), self._spec.get_shot_count() - shots_count)
        ret = {}

        if self._spec.get_shot_effect() in ["bigboom"]:
            self._sounds.play_sound("plasma", self._owner.get_eff_x(), self._owner.get_eff_y(), 0.9 - random.random() * 0.3)

            dst = self._spec.get_shot_canon_dist()

            self._particles.add_particle(0, 0, self._spec.get_shot_effect(), self._owner.get_look_angle(), snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=50, tick_value=(20, 15, 18), color=(255, 255, 0), size=40, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["blueboom"]:
            self._sounds.play_sound("blueshot", self._owner.get_eff_x(), self._owner.get_eff_y(), 0.9 - random.random() * 0.3)

            dst = self._spec.get_shot_canon_dist_rnd(7)

            pt = self._particles.add_particle(0, 0, "blueboom", snap=(self._owner, dst))
            pt.set_random_angle()

            self._particles.add_light(0, 0, opacity=60, tick_value=(25, 5, 20), color=(79, 169, 236), size=50, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["bigboom_small"]:
            self._sounds.play_sound("plasma_small", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.0 - random.random() * 0.1)

            dst = self._spec.get_shot_canon_dist()

            self._particles.add_particle(0, 0, "bigboom", self._owner.get_look_angle(), snap=(self._owner, dst), scale=0.5)
            self._particles.add_light(0, 0, opacity=50, tick_value=(20, 15, 18), color=(255, 255, 0), size=20, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["miniboom"]:
            self._sounds.play_sound("tank_fire", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.5 - random.random() * 0.3)

            self._particles.add_particle(0, 0, self._spec.get_shot_effect(), self._owner.get_look_angle(), snap=(self._owner, self._spec.get_shot_canon_dist()))
            self._particles.add_light(0, 0, opacity=80, tick_value=(25, 8, 9), color=(255, 219, 110), size=25, snap=(self._owner, self._spec.get_shot_canon_dist() + 10))

        elif self._spec.get_shot_effect() in ["hit3"]:
            self._particles.add_particle(0, 0, self._spec.get_shot_effect(), self._owner.get_look_angle(), snap=(self._owner, self._spec.get_shot_canon_dist_rnd(6)))

        elif self._spec.get_shot_effect() in ["lasereff"]:
            self._sounds.play_sound("laser_shot2", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.1 - random.random() * 0.2)

            dst = self._spec.get_shot_canon_dist_rnd(6)

            self._particles.add_particle(0, 0, self._spec.get_shot_effect(), self._owner.get_look_angle(), snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=60, tick_value=(30, 2, 3), color=(161, 231, 255), size=15, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["laser_beam"]:
            self._sounds.play_sound("laser_shot2", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.1 - random.random() * 0.2)

            dst = self._spec.get_shot_canon_dist_rnd(6)

            self._particles.add_light(0, 0, opacity=40, tick_value=(30, 2, 3), color=(161, 231, 255), size=15, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["rocket"]:
            self._sounds.play_sound("launch", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.1 - random.random() * 0.2)

            self._particles.add_particle(0, 0, "miniboom", self._owner.get_look_angle(), snap=(self._owner, self._spec.get_shot_canon_dist()))
            self._particles.add_light(0, 0, opacity=60, tick_value=(25, 10, 11), color=(245, 255, 110), size=40, snap=(self._owner, self._spec.get_shot_canon_dist()))

            c = self._owner.get_look_coords_angle_inc(self._spec.get_shot_canon_dist() * 1.5, 10)
            pt = self._particles.add_particle(c[0], c[1], "fire1", self._owner.get_look_angle())

        elif self._spec.get_shot_effect() in ["minirocket"]:
            self._sounds.play_sound("mini_rocket", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.1 + random.random() * 0.2)

            self._particles.add_particle(0, 0, "miniboom", self._owner.get_look_angle(), snap=(self._owner, self._spec.get_shot_canon_dist()))
            self._particles.add_light(0, 0, opacity=40, tick_value=(25, 3, 4), color=(245, 255, 110), size=30, snap=(self._owner, self._spec.get_shot_canon_dist()))

        elif self._spec.get_shot_effect() in ["tank"]:
            self._sounds.play_sound("tank_fire", self._owner.get_eff_x(), self._owner.get_eff_y(), 0.8 - random.random() * 0.2)

            dst = self._spec.get_shot_canon_dist_rnd(7)

            self._particles.add_particle(0, 0, "boom", snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=50, tick_value=(20, 2, 3), color=(255, 255, 0), size=40, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["mini_vehicle"]:
            if sound_timeout == 0:
                self._sounds.play_sound("machinegun", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.0 - random.random() * 0.1)
                ret["sound_timeout"] = 60

            dst = self._spec.get_shot_canon_dist_rnd(7)

            self._particles.add_particle(0, 0, "mini", snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=30, tick_value=(20, 3, 4), color=(255, 255, 100), size=20 + random.random() * 6 - 3, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["mini_jeep"]:
            self._sounds.play_sound("minigun", self._owner.get_eff_x(), self._owner.get_eff_y(), 1.1 - random.random() * 0.2)

            dst = self._spec.get_shot_canon_dist_rnd(7)

            self._particles.add_particle(0, 0, "mini", snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=30, tick_value=(20, 3, 4), color=(255, 255, 100), size=20 + random.random() * 6 - 3, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["mini_troop"]:
            if sound_timeout == 0:
                self._sounds.play_sound("burst", self._owner.get_eff_x(), self._owner.get_eff_y(), 0.8 - random.random() * 0.1)
                ret["sound_timeout"] = 40

            dst = self._spec.get_shot_canon_dist_rnd(3)

            self._particles.add_particle(0, 0, "mini", snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=30, tick_value=(10, 1, 2), color=(255, 255, 100), size=15 + random.random() * 6 - 3, snap=(self._owner, dst))

        elif self._spec.get_shot_effect() in ["sniper"]:
            self._sounds.play_sound("sniper", self._owner.get_eff_x(), self._owner.get_eff_y(), 0.8 - random.random() * 0.1)

            dst = self._spec.get_shot_canon_dist_rnd(3)

            self._particles.add_particle(0, 0, "miniboom", snap=(self._owner, dst))
            self._particles.add_light(0, 0, opacity=80, tick_value=(10, 1, 2), color=(255, 255, 100), size=25 + random.random() * 6 - 3, snap=(self._owner, dst))

        else:
            #self._particles.add_particle(c[0], c[1], self._spec.get_shot_effect(), snap=(self._owner, self._spec.get_shot_canon_dist_rnd(7)))
            pass

        return ret
