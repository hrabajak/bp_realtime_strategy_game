from game.interface.idrawable import IDrawable
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector


cdef class ShotBase: # interfaces: (IDrawable)
    def __cinit__(self, shots, particles, sounds, shot_spec, shot_add):
        #ifc: IDrawable.__init__(self)

        self._shots = shots
        self._particles = particles
        self._sounds = sounds
        self._shot_spec = shot_spec
        self._shot_add = shot_add

    def set_moveables(self, owner, target):
        raise NotImplementedError

    def init(self, shot_num):
        raise NotImplementedError

    def update(self, tile_col):
        raise NotImplementedError

    def draw_destroy(self):
        raise NotImplementedError

    def draw(self):
        raise NotImplementedError

    def destroy(self):
        raise NotImplementedError
