import random

from libc.math cimport sin, cos, M_PI

from game.graphic.graphbatches import GraphBatches
from game.helper.discrete cimport angle_coords, get_diff_from_angle
from game.helper.distangle import DistAngle
from game.helper.line cimport line_point, make_line_step
from game.helper.vector cimport angle_to, angle_conv, angle_conv_realdeg
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.shot.shotbase cimport ShotBase
from game.shot.shotsplash import ShotSplash


cdef class ShotLine(ShotBase):

    cdef:
        object _sprite
        int _sprite_init

        object _light
        int _light_inc

        object _owner
        object _target
        object _target_place
        long _bef_x
        long _bef_y
        long _x
        long _y
        long _st_x
        long _st_y
        object _trg_coords
        double _angle
        long _speed
        long _tick
        long _tick_max
        int _smoke
        int _homing

    def __cinit__(self, shots, particles, sounds, shot_spec, shot_add):
        ShotBase.__init__(self, shots, particles, sounds, shot_spec, shot_add)

        self._sprite_init = False
        self._light_inc = 0
        self._bef_x = 0
        self._bef_y = 0
        self._x = 0
        self._y = 0
        self._st_x = 0
        self._st_y = 0
        self._angle = 0
        self._speed = 0
        self._tick = 0
        self._tick_max = 0
        self._smoke = False
        self._homing = False

    def set_moveables(self, owner, target: ITargetable):
        cdef angle_coords c
        cdef object cd

        self._owner = owner

        cd = self._owner.get_use_coords()
        self._x = cd[0]
        self._y = cd[1]

        c = get_diff_from_angle(self._owner.get_look_angle_deg(), self._owner.get_spec().get_shot_canon_dist() + 5)
        self._x += c.ox
        self._y += c.oy

        self._st_x = self._x
        self._st_y = self._y
        self._bef_x = self._x
        self._bef_y = self._y

        self._target = target
        self._target_place = self._target.get_target_place(self._st_x, self._st_y)
        self._trg_coords = self._target.get_target_coords(self._st_x, self._st_y)

        if self._shot_spec.get_look_snap():
            self._owner.set_look_snap(self._trg_coords)

        self._angle = angle_to(self._x, self._y, self._trg_coords[0], self._trg_coords[1])

    def get_sprite_x(self):
        return self._x * self._shots.get_zoom() + self._shots.get_x()

    def get_sprite_y(self):
        return self._y * self._shots.get_zoom() + self._shots.get_y()

    def init(self, shot_num):
        self._speed = 1500
        self._tick_max = 100

        #primitives.get().clear()
        #make_line(int(self._x), int(self._y), int(self._trg_coords[0]), int(self._trg_coords[1]), lambda cx, cy: primitives.get().add_circle(cx * 1 + 0.5, cy * 1 + 0.5, 1))
        #primitives.get().add_circle(int(self._st_x), int(self._st_y), 5)

        if self._shot_spec.get_key() in ["laser"]:
            self._light = self._particles.add_light(self._x, self._y, opacity=120, color=(105, 210, 245), size=8)

        self._sprite_init = True
        self.may_draw = 1

        self._tick = self._tick_max

        GraphBatches().add_drawable(self)

    def update(self, tile_col: ITileCollector):
        cdef line_point lp

        self._tick -= 1

        if self._tick >= 0:
            self._bef_x, self._bef_y = self._x, self._y
            lp = make_line_step(self._x, self._y, self._trg_coords[0], self._trg_coords[1], self._speed // 100)
            self._x, self._y = lp.px, lp.py

            if self._bef_x == self._x and self._bef_y == self._y:
                self._collide_effect(self._collide_check())
                return True

            elif self._collide_check():
                self._collide_effect(True)
                return True

        else:
            self._collide_effect(self._collide_check())
            return True

        if self._light is not None:
            if self._light_inc:
                self._light.set_coords(self._x + sin(self._angle) * self._light_inc, self._y + cos(self._angle) * self._light_inc)
            else:
                self._light.set_coords(self._x, self._y)

        if self._smoke and self._tick % 3 == 0:
            self._particles.add_particle(self._x, self._y, "smoke2", -1, random.randint(0, 8), scale=1.1 - random.random() * 0.5)

        return False

    def draw(self):
        if self.may_draw != 1:
            return False

        if self._sprite_init:
            self._sprite_init = False
            self._sprite = GraphBatches().create_sprite(img=self._shot_spec.get_tex(), x=self.get_sprite_x(), y=self.get_sprite_y(), group=GraphBatches().get_group_particle(), batch=GraphBatches().get_batch())
            self._sprite.update(scale=self._shots.get_zoom(), rotation=angle_conv_realdeg(self._angle))

        if self._sprite is not None:
            self._sprite.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._shots.get_zoom(), rotation=angle_conv_realdeg(self._angle))

    def _collide_check(self):
        cdef object place, mm

        #place = ITileCollector.get().get_place_by_coords(self._x, self._y)
        #mm = place.get_moveable_ref()

        if self._target.get_is_alive() and self._target.get_target_colide_check(self._x, self._y):
            return True
            #return next((pl for pl in places if self._target.get_is_on_place(pl)), False)

        return False

    def _collide_effect(self, is_hit):
        cdef object c, spl

        if is_hit:
            c = self._target.get_target_colide_coords(self._owner)
        else:
            c = self._x, self._y

        if self._shot_spec.get_key() == "laser":

            if not is_hit:
                pass
            elif self._target.get_is_human():
                self._particles.add_particle(c[0], c[1], "blood_hit1", -1, scale=1.1 - random.random() * 0.3)
            else:
                self._particles.add_particle(c[0], c[1], "laserexp", -1, scale=1.1 - random.random() * 0.5)

            self._particles.add_light(c[0], c[1], opacity=60, tick_value=(30, 2, 3), color=(161, 231, 255), size=15)

        elif self._shot_spec.get_key() == "heat_shot":
            # !! self._sounds.play_sound("hit", c[0], c[1], 0.4 + random.random() * 0.2)

            if is_hit:
                self._particles.add_particle(c[0], c[1], "hit2", -1, scale=1.1 - random.random() * 0.5)
                self._particles.add_light(c[0], c[1], opacity=120, tick_value=(20, 6, 7), color=(255, 219, 110), size=20)
            else:
                self._particles.add_particle(c[0], c[1], "smoke4", -1, scale=1.1 - random.random() * 0.5)

        elif self._shot_spec.get_key() == "tank":
            # !! self._sounds.play_sound("hit2", c[0], c[1], 0.6 + random.random() * 0.2)

            if is_hit:
                self._particles.add_particle(c[0], c[1], "hit2", -1, scale=1.1 - random.random() * 0.5)
                self._particles.add_light(c[0], c[1], opacity=70, tick_value=(20, 2, 3), color=(255, 255, 97), size=20)
            else:
                self._particles.add_particle(c[0], c[1], "smoke4", -1, scale=1.1 - random.random() * 0.5)

        else:
            self._particles.add_particle(c[0], c[1], "hit2" if is_hit else "smoke4", -1, scale=1.1 - random.random() * 0.5)

        if self._shot_spec.get_splash_range() > 0:
            spl = ShotSplash(self._target_place, self._shot_spec.get_splash_range(), self._owner)
            spl.set_koefs(self._shot_spec.get_splash_koefs())
            spl.set_affect(self._shot_spec.get_splash_affect())
            spl.make(lambda moveable_ref, koef: moveable_ref.deal_damage(self._x, self._y, self._owner, self._shot_spec, self._angle, koef))

        elif is_hit and self._target.get_is_alive():
            self._target.deal_damage(self._x, self._y, self._owner, self._shot_spec, self._angle)

    def draw_destroy(self):
        pass

    def destroy(self):
        self.may_draw = 2

        if self._owner.get_is_alive():
            self._owner.set_look_snap(None)

        if self._light is not None:
            self._light.destroy()

        GraphBatches().add_sprite_delete(self._sprite)
        GraphBatches().kill_drawable(self)
