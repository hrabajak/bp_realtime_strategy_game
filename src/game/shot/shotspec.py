from game.interface.iimages import IImages
from game.shot.shotsplash import ShotSplash


class ShotSpec:

    _specs = {}
    _key = None
    _name = None
    _tex = None
    _damage = 0
    _rect_width = 0
    _rect_height = 0
    _splash_range = False
    _splash_koefs = [1000, 500, 300, 200, 100, 100, 100]
    _splash_affect = [ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_ALL]
    _splash_info = ""
    _look_snap = False

    def __init__(self, key):
        self._key = key

    def get_key(self):
        return self._key

    def get_tex(self):
        return self._tex

    def get_damage(self):
        return self._damage

    def get_rect_width(self):
        return self._rect_width

    def get_rect_height(self):
        return self._rect_height

    def get_splash_range(self):
        return self._splash_range

    def get_splash_koefs(self):
        return self._splash_koefs

    def get_splash_affect(self):
        return self._splash_affect

    def get_splash_info(self):
        return self._splash_info

    def get_look_snap(self):
        return self._look_snap

    @staticmethod
    def init_specs(imgs: IImages):

        s = ShotSpec("mini_vehicle")

        s._name = "minigun shot"
        s._tex = imgs.get_image("mini_shoot")
        s._rect_width = 0.6
        s._rect_height = 5
        s._damage = 25

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("mini_jeep")

        s._name = "minigun shot"
        s._tex = imgs.get_image("mini_shoot")
        s._rect_width = 0.6
        s._rect_height = 5
        s._damage = 25

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("mini_troop")

        s._name = "minigun shot"
        s._tex = imgs.get_image("mini_shoot")
        s._rect_width = 0.7
        s._rect_height = 2
        s._damage = 10

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("sniper")

        s._name = "sniper shot"
        s._tex = imgs.get_image("mini_shoot")
        s._rect_width = 0.8
        s._rect_height = 2
        s._damage = 60

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("laser")

        s._name = "laster shot"
        s._tex = imgs.get_image("laser_shoot")
        s._damage = 40

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("laser_beam")

        s._name = "laster beam"
        s._tex = imgs.get_image("laser_shoot_rot")
        s._rect_width = 0.3
        s._rect_height = 4
        s._damage = 30

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("beam_shot")

        s._name = "beam shot"
        s._tex = imgs.get_image("mazec_shoot_rot")
        s._damage = 350
        s._rect_height = 20
        s._look_snap = True

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("beam_shot_small")

        s._name = "beam shot small"
        s._tex = imgs.get_image("mazec_shoot_rot")
        s._damage = 175
        s._rect_height = 10
        s._look_snap = True

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("rocket")

        s._name = "rocket"
        s._tex = imgs.get_image("shoot_artilery")
        s._damage = 450
        s._splash_range = 3
        s._splash_info = "+3"

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("minirocket")

        s._name = "mini rocket"
        s._tex = imgs.get_image("shoot_minirocket")
        s._damage = 180
        s._splash_range = 1
        s._splash_koefs = [1000, 400]
        s._splash_info = "+1"

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("blueshot")

        s._name = "blueshot"
        s._tex = imgs.get_image("blueshot")
        s._damage = 225
        s._splash_range = 3
        s._splash_info = "+3"

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("heat_shot")

        s._name = "heat shot"
        s._tex = imgs.get_image("shoot_yellow")
        s._damage = 130
        s._look_snap = True

        ShotSpec._specs[s.get_key()] = s

        s = ShotSpec("tank")

        s._name = "tank shot"
        s._tex = imgs.get_image("shoot_tank")
        s._damage = 170
        s._look_snap = True
        s._splash_range = 1
        s._splash_koefs = [1000, 200]
        s._splash_affect = [ShotSplash.AFFECT_ALL, ShotSplash.AFFECT_TROOP]
        s._splash_info = "+1 vs troops"

        ShotSpec._specs[s.get_key()] = s

    @staticmethod
    def get(key):
        return ShotSpec._specs[key]
