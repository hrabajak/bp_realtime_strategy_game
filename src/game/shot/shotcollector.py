from lib.orderedset import OrderedSet
from game.helper.singleton import Singleton
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.interface.iview import IView
from game.particle.particlecollector import ParticleCollector
from game.shot.shotminirocket import ShotMiniRocket
from game.shot.shotrocket import ShotRocket
from game.shot.shotline import ShotLine
from game.shot.shotrect import ShotRect
from game.sound.soundcollector import SoundCollector


class ShotCollector(IShots, metaclass=Singleton):

    _view: IView = None
    _particles: IParticles = None
    _sounds: ISounds = None
    _tile_col: ITileCollector = None

    _items = None

    _x = 0
    _y = 0
    _zoom = 1.0

    def __init__(self):
        IShots.__init__(self)

        self._view = IView.get()
        self._particles = ParticleCollector()
        self._sounds = SoundCollector()
        self._tile_col = ITileCollector.get()

        self._items = OrderedSet()

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_zoom(self):
        return self._zoom

    def add_shot(self, shot_spec, owner, target: ITargetable, move_val=0, shot_num=0):
        sh = None

        shot_add = {
            "mid_move_type": 1 if owner.get_spec().get_shot_count() == 1 else 0,
            "mid_reload": owner.get_spec().get_shot_mid_reload()
        }

        if shot_spec.get_key() in ["beam_shot", "beam_shot_small", "mini_vehicle", "mini_jeep", "mini_troop", "sniper", "laser_beam"]:
            sh = ShotRect(self, self._particles, self._sounds, shot_spec, shot_add)
        elif shot_spec.get_key() in ["laser", "heat_shot", "tank"]:
            sh = ShotLine(self, self._particles, self._sounds, shot_spec, shot_add)
        elif shot_spec.get_key() in ["blueshot"]:
            sh = ShotRocket(self, self._particles, self._sounds, shot_spec, shot_add)
        elif shot_spec.get_key() in ["rocket"]:
            sh = ShotRocket(self, self._particles, self._sounds, shot_spec, shot_add)
        elif shot_spec.get_key() in ["minirocket"]:
            sh = ShotMiniRocket(self, self._particles, self._sounds, shot_spec, shot_add)

        sh.set_moveables(owner, target)
        sh.init(shot_num)

        self._items.add(sh)

        return sh

    def clear(self):
        for pt in self._items:
            pt.destroy()

        self._items = OrderedSet()

    def update(self):
        rem = []

        for pt in self._items:
            if pt.update(self._tile_col):
                rem.append(pt)

        if len(rem) > 0:
            for pt in rem:
                pt.destroy()
                self._items.remove(pt)

    def draw(self):
        self._x, self._y, self._zoom = IView.get().get_view_vals()
