import math
import random

from game.graphic.graphbatches import GraphBatches
from game.helper.discrete cimport angle_coords, get_diff_from_angle
from game.helper.distangle import DistAngle
from game.helper.vector import angle_to, angle_inc_deg
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.shot.shotbase cimport ShotBase


cdef class ShotRect(ShotBase):

    cdef:
        object _rect
        int _rect_init
        double _rect_opacity
        int _rect_he_type

        object _line
        object _owner
        object _target
        object _target_place
        long _st_x
        long _st_y
        object _trg_coords
        int _trg_update
        double _angle
        double _ang_inc
        long _tick
        long _tick_max
        long _tick_colide
        int _colide_flag

    def __cinit__(self, shots, particles, sounds, shot_spec, shot_add):
        ShotBase.__init__(self, shots, particles, sounds, shot_spec, shot_add)

        self._rect_init = False
        self._rect_opacity = 1.0
        self._rect_he_type = 0
        self._st_x = 0
        self._st_y = 0
        self._trg_update = False
        self._angle = 0
        self._ang_inc = 0
        self._tick = 0
        self._tick_max = 0
        self._tick_colide = 0
        self._colide_flag = False

    def set_moveables(self, owner, target: ITargetable):
        self._owner = owner

        cd = self._owner.get_use_coords()
        self._st_x = cd[0]
        self._st_y = cd[1]

        self._target = target
        self._target_place = self._target.get_target_place(self._st_x, self._st_y)
        self._trg_coords = self._target.get_target_coords(self._st_x, self._st_y)

        if self._shot_spec.get_look_snap():
            self._owner.set_look_snap(self._trg_coords)

        self._angle = angle_to(self._st_x, self._st_y, self._trg_coords[0], self._trg_coords[1])

    def init(self, shot_num):
        self._rect_init = True

        if self._shot_spec.get_key() in ["beam_shot"]:
            self._tick_max = 30
            self._rect_he_type = 0
            self.may_draw = 1
            self._collide()

        elif self._shot_spec.get_key() in ["beam_shot_small"]:
            self._tick_max = 20
            self._rect_he_type = 0
            self.may_draw = 1
            self._collide()

        elif self._shot_spec.get_key() in ["mini_vehicle", "mini_jeep", "mini_troop"]:
            self._rect_init = True
            self._tick_max = 5
            self._rect_opacity = 0.5
            self._rect_he_type = 1
            self.may_draw = 1
            self._collide()

        elif self._shot_spec.get_key() in ["mini_troop"]:
            self._rect_init = True
            self._tick_max = 8
            self._rect_opacity = 0.3
            self._rect_he_type = 2
            self.may_draw = 1
            self._collide()

        elif self._shot_spec.get_key() in ["sniper"]:
            self._rect_init = True
            self._tick_max = 10
            self._rect_opacity = 0.8
            self._rect_he_type = 2
            self.may_draw = 1
            self._collide()

        elif self._shot_spec.get_key() in ["laser_beam"]:
            self._rect_init = True
            self._tick_max = 20
            self._tick_colide = 6
            self._rect_opacity = 0.7
            self._rect_he_type = 2
            self._trg_update = True
            self.may_draw = 1

            if self._shot_add["mid_reload"] > 0:
                self._ang_inc = 0
            else:
                self._ang_inc = 50 if shot_num == 0 else -50

        self._tick = self._tick_max

        GraphBatches().add_drawable(self)

    def _shot_step(self):
        pass

    def update(self, tile_col: ITileCollector):
        self._tick -= 1

        if self._tick <= self._tick_colide:
            self._collide()
            return True

        else:
            if self._trg_update:
                self._trg_coords = self._target.get_target_coords(self._st_x, self._st_y)

    def draw(self):
        cdef double kf, kf_from, kf_to, tx, ty, rhe
        cdef object cf
        cdef angle_coords c

        if self.may_draw != 1:
            return False

        if self._rect_init:
            self._rect_init = False
            self._rect = GraphBatches().get_rects().create(self._shot_spec.get_tex())
            self._rect.set_opacity(0)
            self._rect.set_colors(255, 255, 255)

        if self._rect is not None:
            tx = 0
            ty = 0

            if self._ang_inc == 0:
                cf = self._owner.get_look_coords_angle_inc(self._owner.get_spec().get_shot_canon_dist() + 5)
            else:
                angle_deg = angle_inc_deg(self._owner.get_look_angle_deg(), self._ang_inc)
                c = get_diff_from_angle(angle_deg, self._owner.get_spec().get_shot_canon_dist())
                cf = (self._st_x + c.ox, self._st_y + c.oy)
                tx, ty = c.ox, c.oy

            kf_from = 0.0
            kf_to = 1.0

            if self._shot_spec.get_rect_width() > 0:
                kf = (self._tick / self._tick_max) * (1.0 + self._shot_spec.get_rect_width())
                kf_from = max(1.0 - kf, 0.0)
                kf_to = min(1.0 - kf + self._shot_spec.get_rect_width(), 1.0)

            if self._rect_he_type == 0:
                rhe = math.sin(self._tick / self._tick_max) * self._shot_spec.get_rect_height()
            elif self._rect_he_type == 1:
                rhe = random.random() * self._shot_spec.get_rect_height()
            else:
                rhe = self._shot_spec.get_rect_height()

            self._rect.set_coords_line(self._trg_coords[0] + tx, self._trg_coords[1] + ty, cf[0], cf[1], rhe, kf_from, kf_to)
            self._rect.set_opacity_kf(math.sin((self._tick / self._tick_max) * math.pi) * self._rect_opacity)

    def _collide(self):
        if not self._colide_flag:
            self._colide_flag = True

            if self._shot_spec.get_key() in ["beam_shot", "beam_shot_small"]:
                c = int(self._target_place.get_x()), int(self._target_place.get_y())
            else:
                c = self._target.get_target_colide_coords(self._owner)

            self._trg_coords = c

            if self._shot_spec.get_key() in ["mini_vehicle", "mini_jeep"]:

                if not self._target.get_is_human():
                    if random.randint(0, 5) == 0 and self._target.may_dmg_hit(20, self._shot_spec):
                        self._sounds.play_sound("mini_hit", c[0], c[1], 1.2 + random.random() * 0.3)

                    self._particles.add_particle(c[0], c[1], "hit1", -1, scale=1.1 - random.random() * 0.3)

            elif self._shot_spec.get_key() in ["mini_troop", "sniper"]:

                if random.randint(0, 5) == 0 and self._target.may_dmg_hit(20, self._shot_spec):
                    self._sounds.play_sound("mini_hit", c[0], c[1], 1.2 + random.random() * 0.3)

                if not self._target.get_is_human():
                    self._particles.add_particle(c[0], c[1], "hit1", -1, scale=1.1 - random.random() * 0.3)

            elif self._shot_spec.get_key() in ["beam_shot"]:

                for i in range(0, 4):
                    flg = "fire1" if random.randint(0, 1) == 0 else "fire2"
                    c = self._target.get_target_colide_coords(self._owner)
                    self._particles.add_particle(c[0], c[1], flg, -1, i * 10, scale=1.1 - random.random() * 0.4)

            elif self._shot_spec.get_key() in ["beam_shot_small"]:

                for i in range(0, 2):
                    flg = "fire1" if random.randint(0, 1) == 0 else "fire2"
                    c = self._target.get_target_colide_coords(self._owner)
                    self._particles.add_particle(c[0], c[1], flg, -1, i * 10, scale=1.1 - random.random() * 0.4)

            elif self._shot_spec.get_key() in ["laser_beam"]:

                c = self._target.get_target_colide_coords(self._owner)

                if self._target.get_is_human():
                    self._particles.add_particle(c[0], c[1], "blood_hit1", -1, scale=1.1 - random.random() * 0.3)

                self._particles.add_light(c[0], c[1], opacity=40, tick_value=(30, 2, 3), color=(161, 231, 255), size=15)

            else:
                self._particles.add_particle(c[0], c[1], "hit2", -1, scale=1.1 - random.random() * 0.5)

            if self._target.get_is_alive():
                self._target.deal_damage(self._trg_coords[0], self._trg_coords[1], self._owner, self._shot_spec, self._angle)

    def draw_destroy(self):
        pass

    def destroy(self):
        self.may_draw = 2

        if self._owner.get_is_alive():
            self._owner.set_look_snap(None)

        if self._rect is not None:
            self._rect.discard()

        GraphBatches().kill_drawable(self)
