
cdef class ShotBase: # interfaces: (IDrawable)
    cdef public:
        int may_draw

    cdef:
        object _shots
        object _particles
        object _sounds
        object _shot_spec
        object _shot_add
