import random

from libc.math cimport sin, cos, M_PI

from lib.orderedset import OrderedSet
from game.graphic.graphbatches import GraphBatches
from game.helper.discrete cimport angle_coords, get_diff_from_angle
from game.helper.distangle import DistAngle
from game.helper.line cimport line_point, make_line_step
from game.interface.iparticles import IParticles
from game.interface.ishots import IShots
from game.interface.isounds import ISounds
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.helper.vector cimport angle_to, angle_conv, angle_inc_deg, angle_conv_realdeg
from game.map.mapbase import MapBase
from game.shot.shotbase cimport ShotBase
from game.shot.shotsplash import ShotSplash


cdef class ShotMiniRocket(ShotBase):

    cdef:
        object _sprite
        int _sprite_init

        object _light
        int _light_inc

        object _owner
        object _target
        object _target_place
        long _bef_x
        long _bef_y
        long _x
        long _y
        long _st_x
        long _st_y
        object _trg_coords
        double _angle

        long _mid_x
        long _mid_y
        int _mid_move
        int _homing

        long _tick
        object _places
        int _smoke

    def __cinit__(self, shots, particles, sounds, shot_spec, shot_add):
        ShotBase.__init__(self, shots, particles, sounds, shot_spec, shot_add)

        self._sprite_init = False
        self._light_inc = 0
        self._bef_x = 0
        self._bef_y = 0
        self._x = 0
        self._y = 0
        self._st_x = 0
        self._st_y = 0
        self._angle = 0
        self._mid_x = 0
        self._mid_y = 0
        self._mid_move = False
        self._homing = False
        self._tick = 0
        self._smoke = False

    def set_moveables(self, owner, target: ITargetable):
        self._owner = owner

        cd = self._owner.get_use_coords()
        self._x = cd[0]
        self._y = cd[1]
        self._st_x = cd[0]
        self._st_y = cd[1]
        self._bef_x = self._x
        self._bef_y = self._y

        self._target = target
        self._target_place = self._target.get_target_place(self._st_x, self._st_y)
        self._trg_coords = self._target.get_target_coords(self._st_x, self._st_y)

        self._angle = angle_to(self._x, self._y, self._trg_coords[0], self._trg_coords[1])

    def get_sprite_x(self):
        return self._x * self._shots.get_zoom() + self._shots.get_x()

    def get_sprite_y(self):
        return self._y * self._shots.get_zoom() + self._shots.get_y()

    def init(self, shot_num):
        cdef angle_coords c

        if self._owner.get_spec().get_key() == "s2_troop_rocket":
            angle_deg = angle_inc_deg(self._owner.get_look_angle_deg(), 375)
            c = get_diff_from_angle(angle_deg, 20)

        elif self._owner.get_spec().get_key() == "s2_rocket":
            angle_deg = angle_inc_deg(self._owner.get_look_angle_deg(), 278 if shot_num % 2 == 0 else -278)
            c = get_diff_from_angle(angle_deg, 30)

        else:
            angle_deg = angle_inc_deg(self._owner.get_look_angle_deg(), 139 if shot_num % 2 == 0 else -139)
            c = get_diff_from_angle(angle_deg, 40)

        self._mid_x = c.ox + self._x
        self._mid_y = c.oy + self._y
        self._mid_move = True

        self._smoke = False
        self._sprite_init = True
        self.may_draw = 1
        self._tick = 0

        GraphBatches().add_drawable(self)

    def update(self, tile_col: ITileCollector):
        cdef line_point lp

        if self._homing and self._target.get_is_alive():
            self._target_place = self._target.get_target_place(self._st_x, self._st_y)
            self._trg_coords = self._target.get_target_coords(self._st_x, self._st_y)

            if self._shot_spec.get_look_snap():
                self._owner.set_look_snap(self._trg_coords)

        self._angle = angle_to(self._x, self._y, self._trg_coords[0], self._trg_coords[1])

        if self._mid_move:
            lp = make_line_step(self._x, self._y, self._mid_x, self._mid_y, 2)
            self._x, self._y = lp.px, lp.py

            if self._x == self._mid_x and self._y == self._mid_y:
                self._mid_move = False
                self._smoke = True

                self._particles.add_particle(self._x, self._y, "boom", -1, scale=1.1 - random.random() * 0.5)
                self._particles.add_light(self._x, self._y, opacity=60, tick_value=(25, 3, 4), color=(245, 255, 110), size=30)

                self._light = self._particles.add_light(self._x, self._y, opacity=100, color=(245, 255, 110), size=8)
                self._light_inc = -7

        else:
            self._bef_x, self._bef_y = self._x, self._y
            lp = make_line_step(self._x, self._y, self._trg_coords[0], self._trg_coords[1], 5)
            self._x, self._y = lp.px, lp.py

            if self._bef_x == self._x and self._bef_y == self._y:
                self._collide_effect(self._collide_check())
                return True

            elif self._collide_check():
                self._collide_effect(True)
                return True

            elif self._x == self._trg_coords[0] and self._y == self._trg_coords[1]:
                self._collide_effect(False)
                return True

        if self._light is not None:
            if self._light_inc:
                self._light.set_coords(self._x + sin(self._angle) * self._light_inc, self._y + cos(self._angle) * self._light_inc)
            else:
                self._light.set_coords(self._x, self._y)

        if self._smoke and self._tick % 3 == 0:
            self._particles.add_particle(self._x, self._y, "smoke2", -1, random.randint(0, 8), scale=1.1 - random.random() * 0.5)

        self._tick += 1

        return False

    def _collide_check(self):
        cdef object place, mm

        place = ITileCollector.get().get_place_by_coords(self._x, self._y)

        if place is not None:
            for mm in place.get_targetables():
                if mm.get_side() is not self._owner.get_side() and mm.get_target_colide_check(self._x, self._y):
                    self._target = mm
                    return True

        return False

    def draw(self):
        if self.may_draw != 1:
            return False

        if self._sprite_init:
            self._sprite_init = False
            self._sprite = GraphBatches().create_sprite(img=self._shot_spec.get_tex(), x=self.get_sprite_x(), y=self.get_sprite_y(), group=GraphBatches().get_group_particle(), batch=GraphBatches().get_batch())
            self._sprite.update(scale=self._shots.get_zoom(), rotation=angle_conv_realdeg(self._angle))

        if self._sprite is not None:
            self._sprite.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._shots.get_zoom(), rotation=angle_conv_realdeg(self._angle))

    def _collide_effect(self, is_hit):
        cdef double cx, cy
        cdef object spl, place

        cx = self._bef_x + sin(self._angle) * (5 + random.random() * 10.0)
        cy = self._bef_y + cos(self._angle) * (5 + random.random() * 10.0)

        self._particles.add_particle(cx, cy, "exp2", -1, scale=0.7 - random.random() * 0.3)
        self._particles.add_light(cx, cy, opacity=100, tick_value=(27, 22, 25), color=(245, 255, 110), size=20)

        if self._shot_spec.get_splash_range() > 0:
            place = ITileCollector.get().get_place_by_coords(self._x, self._y)

            if place is not None:
                spl = ShotSplash(place, self._shot_spec.get_splash_range(), self._owner)
                spl.set_koefs(self._shot_spec.get_splash_koefs())
                spl.set_affect(self._shot_spec.get_splash_affect())
                spl.make(lambda moveable_ref, koef: moveable_ref.deal_damage(self._x, self._y, self._owner, self._shot_spec, self._angle, koef))

    def draw_destroy(self):
        pass

    def destroy(self):
        self.may_draw = 2

        if self._owner.get_is_alive():
            self._owner.set_look_snap(None)

        if self._light is not None:
            self._light.destroy()

        GraphBatches().add_sprite_delete([self._sprite])
        GraphBatches().kill_drawable(self)
