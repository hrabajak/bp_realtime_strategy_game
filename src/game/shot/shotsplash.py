from collections import OrderedDict

from game.map.mapbase import MapBase
from game.map.mapscanner import MapScanner


class ShotSplash:

    AFFECT_ALL = 0
    AFFECT_TROOP = 1

    _place = None
    _steps = 0
    _owner = None
    _hash = None
    _koefs = [1000, 500, 300, 200, 100, 100, 100]
    _affect = [AFFECT_ALL, AFFECT_ALL, AFFECT_ALL, AFFECT_ALL, AFFECT_ALL, AFFECT_ALL, AFFECT_ALL]

    def __init__(self, place, steps, owner):
        self._place = place
        self._steps = steps
        self._owner = owner
        self._hash = OrderedDict()

    def set_koefs(self, koefs):
        self._koefs = koefs

    def set_affect(self, affect):
        self._affect = affect

    def make(self, damage_callback: (any, int)):
        scr = MapScanner(MapBase())
        scr.scan_tiles(self._place, self._steps, self._splash_step)

        for mm in self._hash.keys():
            steps = self._hash[mm]
            koef = self._koefs[steps] if steps < len(self._koefs) else 0

            damage_callback(mm, koef)

    def _splash_step(self, place, result):

        # primitives.get().add_circle(place.get_x(), place.get_y(), 1 + place.back_steps)

        for mm in place.get_targetables():
            if mm.get_is_alive() and mm.get_side() is not self._owner.get_side():
                steps = place.back_steps // 10
                affect_flag = False

                if self._affect[steps] == ShotSplash.AFFECT_ALL:
                    affect_flag = True
                elif self._affect[steps] == ShotSplash.AFFECT_TROOP:
                    affect_flag = mm.get_target_type() == 0 and mm.get_spec().get_is_troop()

                if affect_flag:
                    if mm not in self._hash:
                        self._hash[mm] = steps
                    elif self._hash[mm] > steps:
                        self._hash[mm] = steps

        return None
