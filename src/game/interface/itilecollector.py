

class ITileCollector:

    _ref = None

    @staticmethod
    def get():
        return ITileCollector._ref

    @staticmethod
    def set(ref):
        ITileCollector._ref = ref

    def get_x(self):
        raise NotImplementedError

    def get_y(self):
        raise NotImplementedError

    def get_view_width(self):
        raise NotImplementedError

    def get_view_height(self):
        raise NotImplementedError

    def get_zoom(self):
        raise NotImplementedError

    def get_fog_type(self):
        raise NotImplementedError

    def get_fog_enabled(self):
        raise NotImplementedError

    def get_cur_tilesize(self):
        raise NotImplementedError

    def get_find_flag(self, tp=0):
        raise NotImplementedError

    def move_find_flag(self, tp=0):
        raise NotImplementedError

    def get_tile(self, x, y):
        raise NotImplementedError

    def get_tile_by_coords(self, x, y):
        raise NotImplementedError

    def get_tile_by_coords_scroll(self, x, y):
        raise NotImplementedError

    def get_tiles(self):
        raise NotImplementedError

    def get_tiles_width(self):
        raise NotImplementedError

    def get_tiles_height(self):
        raise NotImplementedError

    def get_garea(self):
        raise NotImplementedError

    def pop_first_tile(self):
        raise NotImplementedError

    def clear_tiles_flags(self):
        raise NotImplementedError

    def fog_tile_add(self, tile):
        raise NotImplementedError

    def overlay_tile_add(self, tile):
        raise NotImplementedError

    def get_place(self, x, y):
        raise NotImplementedError

    def get_place_by_coords(self, x, y):
        raise NotImplementedError

    def get_place_by_coords_scroll(self, x, y):
        raise NotImplementedError

    def get_places(self):
        raise NotImplementedError

    def get_edge(self, x, y):
        raise NotImplementedError
