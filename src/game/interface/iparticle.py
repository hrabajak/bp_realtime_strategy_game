
class IParticle:
    def destroy(self):
        raise NotImplementedError

    def update(self):
        raise NotImplementedError
