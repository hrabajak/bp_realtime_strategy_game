
class ICoordSnap:
    def get_look_coords_angle_inc(self, value, rand=0):
        raise NotImplementedError

    def get_coords(self, rand=0):
        raise NotImplementedError

    def get_look_angle(self):
        raise NotImplementedError
