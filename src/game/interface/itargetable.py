
class ITargetable:
    def get_x(self):
        raise NotImplementedError

    def get_y(self):
        raise NotImplementedError

    def get_use_x(self):
        raise NotImplementedError

    def get_use_y(self):
        raise NotImplementedError

    def get_side(self):
        raise NotImplementedError

    def get_target_type(self):
        raise NotImplementedError

    def get_target(self):
        raise NotImplementedError

    def get_tile(self):
        raise NotImplementedError

    def get_place(self):
        raise NotImplementedError

    def get_places(self):
        raise NotImplementedError

    def get_places_around(self):
        raise NotImplementedError

    def get_is_on_place(self, place):
        raise NotImplementedError

    def get_is_alive(self):
        raise NotImplementedError

    def get_is_human(self):
        raise NotImplementedError

    def dist_to_moveable(self, ref):
        raise NotImplementedError

    def get_target_place(self, from_x, from_y):
        raise NotImplementedError

    def get_target_coords(self, from_x, from_y):
        raise NotImplementedError

    def get_target_colide_coords(self, ref):
        raise NotImplementedError

    def get_target_colide_area(self):
        raise NotImplementedError

    def get_target_priority(self):
        raise NotImplementedError

    def get_target_colide_check(self, cx, cy):
        raise NotImplementedError

    def get_nearest_tile(self, from_x, from_y):
        raise NotImplementedError

    def may_dmg_hit(self, timeout, shot_spec):
        raise NotImplementedError

    def deal_damage(self, dx, dy, owner, shot_spec, angle, koef=1.0):
        raise NotImplementedError
