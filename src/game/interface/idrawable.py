
class IDrawable:

    may_draw = 0

    def draw_destroy(self):
        raise NotImplementedError

    def draw(self):
        raise NotImplementedError
