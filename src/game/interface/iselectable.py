
class ISelectable:

    SELECTED_NONE = 0
    SELECTED_MOVEABLE = 1
    SELECTED_BUILD = 2

    def get_side(self):
        raise NotImplementedError

    def get_is_alive(self):
        raise NotImplementedError

    def get_selected_type(self):
        raise NotImplementedError

    def set_selected(self, selected):
        raise NotImplementedError

    def get_state_map(self):
        raise NotImplementedError
