from game.moveable.unitspec import UnitSpec


class IMoveables:

    _ref = None

    @staticmethod
    def get():
        return IMoveables._ref

    @staticmethod
    def set(ref):
        IMoveables._ref = ref

    def set_refs(self, tiles, sides, view):
        raise NotImplementedError

    def add_moveable(self, side, spec: UnitSpec, x: int, y: int, set_number=-1):
        raise NotImplementedError

    def add_moveable_place(self, side, spec: UnitSpec, tile, set_number=-1):
        raise NotImplementedError

    def rem_moveable(self, ref):
        raise NotImplementedError

    def get_moveable_at_coord(self, x: float, y: float, target_side=None):
        raise NotImplementedError

    def get_moveable_at_range(self, sx: float, sy: float, ex: float, ey: float, target_side=None):
        raise NotImplementedError

    def get_moveable_by_number(self, number: int):
        raise NotImplementedError

    def get_x(self):
        raise NotImplementedError

    def get_y(self):
        raise NotImplementedError

    def get_zoom(self):
        raise NotImplementedError
