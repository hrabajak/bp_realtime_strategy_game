from game.build.buildspec import BuildSpec


class IBuilds:

    _ref = None

    @staticmethod
    def get():
        return IBuilds._ref

    @staticmethod
    def set(ref):
        IBuilds._ref = ref

    def set_refs(self, tiles, sides, view):
        raise NotImplementedError

    def add_build(self, side, spec: BuildSpec, x, y, data=None):
        raise NotImplementedError

    def add_build_tile(self, side, spec: BuildSpec, tile):
        raise NotImplementedError

    def get_build_at_coord(self, x: float, y: float, target_side=None):
        raise NotImplementedError

    def get_build_at_range(self, sx: float, sy: float, ex: float, ey: float, target_side=None):
        raise NotImplementedError

    def get_build_by_number(self, number: int):
        raise NotImplementedError

    def get_x(self):
        raise NotImplementedError

    def get_y(self):
        raise NotImplementedError

    def get_zoom(self):
        raise NotImplementedError
