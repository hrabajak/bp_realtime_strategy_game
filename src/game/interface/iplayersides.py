
class IPlayerSides:

    _ref = None

    @staticmethod
    def get():
        return IPlayerSides._ref

    @staticmethod
    def set(ref):
        IPlayerSides._ref = ref

    def set_refs(self, moveables, builds, tile_col, client):
        raise NotImplementedError

    def get_worker(self):
        raise NotImplementedError

    def get_sides(self):
        raise NotImplementedError

    def get_side_by_number(self, num):
        raise NotImplementedError

    def get_focus_side(self, any=False):
        raise NotImplementedError

    def get_playable_sides(self):
        raise NotImplementedError

    def get_random_enemy(self, self_side):
        raise NotImplementedError

    def determine_wictory_side(self):
        raise NotImplementedError
