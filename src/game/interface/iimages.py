

class IImages:

    _ref = None

    @staticmethod
    def get():
        return IImages._ref

    @staticmethod
    def set(ref):
        IImages._ref = ref

    def get_image(self, key, var=0):
        raise NotImplementedError

    def get_image_color(self, key, color=None):
        raise NotImplementedError

    def get_image_rnd(self, key, rndval=0):
        raise NotImplementedError

    def get_animation(self, key):
        raise NotImplementedError

    def get_image_texture(self, hash, data_map, def_type, number, alternative):
        raise NotImplementedError
