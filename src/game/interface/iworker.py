from game.gamestat.gamespec import GameSpec


class IWorker:

    _ref = None

    @staticmethod
    def get():
        return IWorker._ref

    @staticmethod
    def set(ref):
        IWorker._ref = ref

    event_game_clear: (any, any) = lambda self, data: None
    event_game_start: (any, any) = lambda self, data: None
    event_game_list: (any, any) = lambda self, data: None
    event_game_enter: (any, any) = lambda self, data: None
    event_error: (any, any) = lambda self, error: None

    def event_reset(self):
        self.event_game_clear = self._evt_blank
        self.event_game_start = self._evt_blank
        self.event_game_list = self._evt_blank
        self.event_game_enter = self._evt_blank
        self.event_error = self._evt_blank

    def _evt_blank(self, data):
        pass

    def get_quick_progress(self):
        raise NotImplementedError

    def get_quick_build(self):
        raise NotImplementedError

    def get_quick_mine(self):
        raise NotImplementedError

    def get_is_immed(self):
        raise NotImplementedError

    def check_victory_side(self):
        raise NotImplementedError

    def chat_message(self, message):
        raise NotImplementedError

    def game_clear(self):
        raise NotImplementedError

    def game_connect(self):
        raise NotImplementedError

    def game_disconnect(self):
        raise NotImplementedError

    def game_start(self, spec: GameSpec):
        raise NotImplementedError

    def game_list(self):
        raise NotImplementedError

    def game_enter(self, number):
        raise NotImplementedError

    def game_prepare(self):
        raise NotImplementedError

    def game_quit(self):
        raise NotImplementedError
