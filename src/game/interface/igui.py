from game.build.buildspec import BuildSpec


class IGui:
    _ref = None

    @staticmethod
    def get():
        return IGui._ref

    @staticmethod
    def set(ref):
        IGui._ref = ref

    def set_side(self, side):
        raise NotImplementedError

    def create_modal_message(self, caption):
        raise NotImplementedError

    def drop_modal_messages(self):
        raise NotImplementedError

    def chat_message(self, side, message):
        raise NotImplementedError

    def drop_menu(self):
        raise NotImplementedError

    def create_menu(self, tp):
        raise NotImplementedError

    def get_side(self):
        raise NotImplementedError

    def create_notify(self, message):
        raise NotImplementedError

    def action_item_notify(self, action):
        raise NotImplementedError

    def focus_remove(self):
        raise NotImplementedError

    def focus_moveables(self, side, moveables):
        raise NotImplementedError

    def focus_builds(self, side, builds):
        raise NotImplementedError

    def focus_building(self, spec: BuildSpec, ac=None):
        raise NotImplementedError

    def focus_upgrade(self, side, build_src_ref, ac_spec):
        raise NotImplementedError

    def focus_back(self, side):
        raise NotImplementedError
