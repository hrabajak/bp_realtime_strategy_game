
class IParticles:
    def add_particle(self, x, y, key, angle, delay, **kwargs):
        raise NotImplementedError

    def add_light(self, x, y, **kwargs):
        raise NotImplementedError

    def get_x(self):
        raise NotImplementedError

    def get_y(self):
        raise NotImplementedError

    def get_zoom(self):
        raise NotImplementedError
