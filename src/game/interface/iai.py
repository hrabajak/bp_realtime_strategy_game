from game.action.actionitemspec import ActionItemSpec


class IAi:
    def get_side(self):
        raise NotImplementedError

    def get_tick(self):
        raise NotImplementedError

    def is_action_stacked(self, key):
        raise NotImplementedError

    def may_stack_action(self, key, bref, money_slot):
        raise NotImplementedError

    def get_suitable_factory(self):
        raise NotImplementedError

    def add_group(self, grp_ref):
        raise NotImplementedError

    def get_group(self, **kwargs):
        raise NotImplementedError

    def add_eco_level(self, eco_ref):
        raise NotImplementedError

    def get_unit_stat(self, key=None):
        raise NotImplementedError

    def update_unit_stat(self, spec, **kwargs):
        raise NotImplementedError

    def get_is_occupied(self):
        raise NotImplementedError

    def get_defend_tiles(self, count):
        raise NotImplementedError

    def trigger_damaged(self, ref, ref_by):
        raise NotImplementedError

    def moveable_occupy_enter(self, tile, ref):
        raise NotImplementedError

    def moveable_occupy_leave(self, tile, ref):
        raise NotImplementedError

    def log(self, value):
        raise NotImplementedError
