from game.interface.itargetable import ITargetable


class IShots:
    def add_shot(self, shot_spec, owner, target: ITargetable, move_val=0, shot_num=0):
        raise NotImplementedError

    def get_x(self):
        raise NotImplementedError

    def get_y(self):
        raise NotImplementedError

    def get_zoom(self):
        raise NotImplementedError
