
class ITrajectory:
    def set_speed(self, speed):
        raise NotImplementedError

    def set_angle_speed(self, angle_speed):
        raise NotImplementedError

    def set_aft_point(self, aft_to_x, aft_to_y):
        raise NotImplementedError

    def set_is_leaf(self, is_leaf):
        raise NotImplementedError

    def get_is_any(self):
        raise NotImplementedError

    def get_len(self):
        raise NotImplementedError

    def get_koef(self):
        raise NotImplementedError

    def get_pts(self):
        raise NotImplementedError

    def pop(self):
        raise NotImplementedError
