from game.interface.iview import IView


class ISounds:
    def play_sound(self, media, x, y, pitch, use_coords=True):
        raise NotImplementedError
