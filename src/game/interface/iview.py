

class IView:

    _ref = None

    @staticmethod
    def get():
        return IView._ref

    @staticmethod
    def set(ref):
        IView._ref = ref

    def wnd_update(self):
        raise NotImplementedError

    def update_view(self):
        raise NotImplementedError

    def sc_to(self, x, y, zoom=-1):
        raise NotImplementedError

    def get_zoom(self) -> float:
        raise NotImplementedError

    def get_cur_tilesize(self) -> float:
        raise NotImplementedError

    def get_x(self) -> float:
        raise NotImplementedError

    def get_y(self) -> float:
        raise NotImplementedError

    def get_width(self) -> int:
        raise NotImplementedError

    def get_height(self) -> int:
        raise NotImplementedError

    def get_view_rect(self):
        raise NotImplementedError

    def get_view_vals(self):
        raise NotImplementedError

    def get_wnd(self):
        raise NotImplementedError

    def get_mat(self):
        raise NotImplementedError

    def get_trans_mat(self):
        raise NotImplementedError
