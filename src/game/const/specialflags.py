
class SpecialFlags:

    SPECIAL_NONE = 0
    SPECIAL_MINER = 1
    SPECIAL_UPGRADE = 2
    SPECIAL_BUILD = 3
    SPECIAL_CREATE = 4
    SPECIAL_CREATE_MOVE = 5
    SPECIAL_CREATE_FINAL = 6

    BORN_SPAWNING = 0
    BORN_READY = 1
    BORN_VAPORIZE = 2

    def __init__(self):
        pass
