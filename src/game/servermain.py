from game.action.actionitemspec import ActionItemSpec
from game.build.buildspec import BuildSpec
from game.build.buildturretspec import BuildTurretSpec
from game.config import Config
from game.helper.discrete import build_discrete_map
from game.helper.singleton import Singleton
from game.image.imagecollector import ImageCollector
from game.image.imagecollectordummy import ImageCollectorDummy
from game.moveable.unitspec import UnitSpec
from game.network.server import Server
from game.shot.shotspec import ShotSpec
from game.sound.soundcollectordummy import SoundCollectorDummy


class ServerMain(metaclass=Singleton):

    _srv = None

    def __init__(self):
        pass

    def init(self):
        build_discrete_map()

        Singleton.set_cls_override("ImageCollector", ImageCollectorDummy)
        Singleton.set_cls_override("SoundCollector", SoundCollectorDummy)

        conf = Config.get()
        conf.load()

        #conf.test_only_map()
        conf.test_srv()
        conf.server()

        ShotSpec.init_specs(ImageCollector())
        UnitSpec.init_specs(ImageCollector())
        BuildTurretSpec.init_specs(ImageCollector())
        BuildSpec.init_specs(ImageCollector())
        ActionItemSpec.init_specs()

        self._srv = Server()
        self._srv.start()
