from game.gamestat.gamespec import GameSpec
from game.interface.iworker import IWorker


class WorkerDummy(IWorker):

    _quick_progress = False
    _quick_build = False
    _quick_mine = False

    def __init__(self):
        IWorker.__init__(self)
        IWorker.set(self)

    def set_quick_progress(self, quick_progress):
        self._quick_progress = quick_progress

    def set_quick_build(self, quick_build):
        self._quick_build = quick_build

    def set_quick_mine(self, quick_mine):
        self._quick_mine = quick_mine

    def get_quick_progress(self):
        return self._quick_progress

    def get_quick_build(self):
        return self._quick_build

    def get_quick_mine(self):
        return self._quick_mine

    def get_is_immed(self):
        return True

    def check_victory_side(self):
        return None

    def chat_message(self, message):
        pass

    def game_clear(self):
        pass

    def game_connect(self):
        pass

    def game_disconnect(self):
        pass

    def game_start(self, spec: GameSpec):
        pass

    def game_list(self):
        pass

    def game_enter(self, number):
        pass

    def game_prepare(self):
        pass

    def game_quit(self):
        pass
