import pyglet

from game.window.windowelement import WindowElement


class Panel(WindowElement):

    _rect = None
    _rectb = None
    _rectu = None
    _batch = None
    _group = None
    _group_text = None
    _color = (142, 142, 142)
    _color_back = (0, 0, 0)
    _color_bg = (110, 110, 110)
    _color_brd = (100, 90, 90)
    _padd_x = 0
    _padd_y = 0

    def __init__(self, x, y, width, height, padd_x=0, padd_y=0):
        WindowElement.__init__(self, x, y, width, height)

        self._padd_x = padd_x
        self._padd_y = padd_y

    def raise_to_top(self, modal=False):
        self.get_parent().raise_element(self, modal)

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

    def get_batch(self):
        return self._batch

    def get_group(self):
        return self._group

    def get_group_text(self):
        return self._group_text

    def get_is_drawable(self):
        return self._batch is not None

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                if self._batch is not None:
                    self._batch.invalidate()
                    self._batch = None

                self._group = None
                self._group_text = None
                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True

            self._batch = pyglet.graphics.Batch()
            self._group = pyglet.graphics.OrderedGroup(8)
            self._group_text = pyglet.graphics.OrderedGroup(9)
            self._rectu = pyglet.shapes.Rectangle(*self._pos.get(), color=self._color_back, batch=self.get_batch(), group=self.get_group())
            self._rectb = pyglet.shapes.Rectangle(*self._pos.get(), color=self._color_bg, batch=self.get_batch(), group=self.get_group())
            self._rectu.opacity = 100
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color, border_color=self._color_brd, border=1, batch=self.get_batch(), group=self.get_group())

        if self._graph_update:
            self._rect.visible = self._visible
            #self._rectu.x, self._rectu.y, self._rectu.width, self._rectu.height = self._pos.get(-self._padd_x, -self._padd_y, -10, 10)
            self._rectu.x, self._rectu.y, self._rectu.width, self._rectu.height = self._pos.get(-self._padd_x - 2, -self._padd_y - 2, -10, 10)
            self._rectb.x, self._rectb.y, self._rectb.width, self._rectb.height = self._pos.get(-self._padd_x - 2, -self._padd_y - 2)
            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get(-self._padd_x, -self._padd_y)

            self._graph_update = False
