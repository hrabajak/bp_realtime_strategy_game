import pyglet

from game.window.listboxitem import ListBoxItem
from game.window.windowelement import WindowElement


class ListBox(WindowElement):

    _rect = None
    _padd = 5

    _color_brd_normal = (70, 70, 70)
    _color_brd_hover = (90, 90, 90)
    _color_brd_focus = (0, 94, 14)
    _color_back = (255, 255, 255)
    _color_back_focus = (232, 255, 235)

    _event_select: (any, any) = lambda self, data: None
    _event_select_data = None

    def __init__(self, x, y, width, height):
        WindowElement.__init__(self, x, y, width, height)

    def set_event_select(self, func, data):
        self._event_select = func
        self._event_select_data = data

    def event_select(self, arg):
        def decorator(func):
            self._event_select = func
            self._event_select_data = arg

        return decorator

    def item_add(self, key, value):
        it = self.item_get(key)

        if it is None:
            it = ListBoxItem(0, 0, key, value)
            it.init(self)
            it.get_pos().set(self._padd, 0, self.get_pos().get_width() - self._padd * 2, 25)
            self.organize_y(0, self._padd)
            self._graph_update = True

        else:
            it.set_value(value)

        return it

    def item_get(self, key=None):
        if key is None:
            return next((c for c in self._childs if c.get_active()), None)
        else:
            return next((c for c in self._childs if c.get_key() == key), None)

    def item_select(self, key):
        ac_item = self.item_get()

        if ac_item is None or ac_item.get_key() != key:
            [c.set_active(False) for c in self._childs if c.get_active()]
            [c.set_active(True) for c in self._childs if c.get_key() == key]
            self._event_select(self._event_select_data)

    def item_remove(self, key):
        it = self.item_get(key)
        if it is not None:
            it.destroy()
            self.organize_y(0, self._padd)
            self._graph_update = True

    def item_set_dict(self, items):
        rem_keys = [c.get_key() for c in self._childs]

        for key in items.keys():
            it = self.item_get(key)

            if it is None:
                self.item_add(key, items[key])
            else:
                it.set_value(items[key])
                rem_keys.remove(key)

        for key in rem_keys:
            self.item_remove(key)

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color_back, batch=self.get_batch(), group=self.get_group(), border_color=self._color_brd_normal, border=4)

        if self._graph_update:
            self._rect.visible = self._visible
            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get()

            self._graph_update = False

            if self.get_in_focus():
                self._rect._brgb = self._color_brd_focus
                self._rect.color = self._color_back_focus
            elif self._hover:
                self._rect._brgb = self._color_brd_hover
                self._rect.color = self._color_back
            else:
                self._rect._brgb = self._color_brd_normal
                self._rect.color = self._color_back
