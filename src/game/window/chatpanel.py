import pyglet

from game.window.windowelement import WindowElement


class ChatPanel(WindowElement):

    _messages = None
    _to_drop = None

    def __init__(self, x, y, width, height):
        WindowElement.__init__(self, x, y, width, height)

        self._messages = []
        self._to_drop = []

    def add_message(self, color, message):
        self._messages.append({
            'tick': 0,
            'color': (color[0], color[1], color[2], 255),
            'message': message,
            'doc': None,
            'label': None
        })
        self._graph_update = True

    def tickstep(self):
        for m in [c for c in self._messages]:
            m['tick'] += 1

            if m['tick'] > 10:
                self._messages.remove(m)
                self._to_drop.append(m)
                self._graph_update = True

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                for m in self._messages + self._to_drop:
                    if m['label'] is not None:
                        m['label'].delete()
                        m['label'] = None
                        m['doc'] = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self._graph_init = None
            self._graph_update = True

        if self._graph_update:
            if len(self._to_drop) > 0:
                for m in self._to_drop:
                    if m['label'] is not None:
                        m['label'].delete()
                        m['label'] = None
                        m['doc'] = None

                self._to_drop.clear()

            for m in self._messages:
                if m['label'] is None:
                    m['doc'] = pyglet.text.document.UnformattedDocument(m['message'])
                    m['doc'].set_style(0, len(m['doc'].text), dict(font_name=self._mng.get_font(), font_size=self._mng.get_font_size(), color=m['color']))

                    m['label'] = pyglet.text.layout.TextLayout(m['doc'], width=self._pos.get_width(), height=30, multiline=True, batch=self.get_batch(), group=self.get_group_text())
                    m['label'].anchor_x = 'left'
                    m['label'].anchor_y = 'bottom'

            idx = 0

            for m in self._messages:
                c = self._pos.get()
                m['label'].x, m['label'].y = c[0], c[1] - idx * 20
                idx += 1

            self._graph_update = False
