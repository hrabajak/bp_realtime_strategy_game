import math
import time
import pyglet
from pyglet import font

from game.graphic.graphbatches import GraphBatches
from game.helper.singleton import Singleton
from game.window.position import Position


class WindowManager(metaclass=Singleton):

    _batches: GraphBatches = None
    _childs = None
    _child_modal = None
    _focused = None
    _selrect = None
    _keys = None

    _batch = None
    _group = None
    _group_text = None

    _pos: Position = None

    _wnd_width = 0
    _wnd_height = 0

    _mouse_x = 0
    _mouse_y = 0
    _mouse_btn = set()

    _drag_flag = False
    _drag_bt = 0
    _drag_time = 0
    _drag_time_bt = -1
    _drag_x = 0
    _drag_y = 0
    _drag_sx = 0
    _drag_sy = 0
    _drag_dx = 0
    _drag_dy = 0
    _drag_dst = 0
    _drag_kf = 0
    _drag_trv = 0
    _drag_trv_min = 35

    event_mouse: (any, int, int) = lambda self, x, y: None
    event_click: (any, int, int, int) = lambda self, x, y, button: None
    event_double_click: (any, int, int, int) = lambda self, x, y, button: None
    event_drag_area: (any, int, int, int, int, int) = lambda self, sx, sy, ex, ey, button: None
    event_drag_move: (any, int, int) = lambda self, dx, dy: None

    def __init__(self):
        self._batches = GraphBatches()
        self._batch = self._batches.get_batch_gui()
        self._group = self._batches.get_group_gui()
        self._group_text = self._batches.get_group_gui_text()

        font.add_file("resources/font/basicff.ttf")
        font.load("basicff")

        self._pos = Position()

        self._childs = []
        self._childs_draw = []
        self._focused = None
        self._selrect = []
        self._keys = set()

        for i in range(0, 4):
            ln = pyglet.shapes.Line(0, 0, 0, 0, width=2, color=(255, 255, 255), group=self._group, batch=self._batch)
            ln.visible = False
            self._selrect += [ln]

    def get_batch(self):
        return self._batch

    def get_group(self):
        return self._group

    def get_group_text(self):
        return self._group_text

    def get_font(self):
        return 'basicff'

    def get_font_size(self):
        return 10

    def get_focused(self):
        return self._focused

    def get_mouse_x(self):
        return self._mouse_x

    def get_mouse_y(self):
        return self._mouse_y

    def get_is_pan(self):
        return not self._drag_flag and self._drag_kf > 0 and self._drag_bt == 1 and self._drag_trv > self._drag_trv_min

    def get_pos(self):
        return self._pos

    def get_is_key(self, *args):
        return next((k for k in args if k in self._keys), False)

    def get_is_shift(self):
        return self.get_is_key(pyglet.window.key.LSHIFT, pyglet.window.key.RSHIFT)

    def set_size(self, wnd_width, wnd_height):
        self._pos.set(0, 0, wnd_width, wnd_height)
        [c.pos_update() for c in self._childs]

    def pop_drag_coords(self):
        kf = self._drag_kf

        self._drag_kf = self._drag_kf * 0.8 - 0.005
        if self._drag_kf <= 0.01:
            self._drag_kf = 0

        return self._drag_dx * kf, self._drag_dy * kf

    def add_element(self, ref):
        self._childs.append(ref)
        self.mouse_touch()

    def rem_element(self, ref):
        if ref in self._childs:
            self._childs.remove(ref)

        if ref is self._child_modal:
            self._child_modal = None

        self.mouse_touch()

    def raise_element(self, ref, modal=False):
        self._childs.remove(ref)
        self._childs = [ref] + self._childs

        if modal:
            self._child_modal = ref
            self.focus_element(None)

        self.mouse_touch()

    def focus_element(self, ref):
        if self._focused is not None:
            self._focused.focus_leave()
            self._focused = None

        if ref is not None:
            self._focused = ref
            self._focused.focus_enter()

        self.mouse_touch()

    def mouse_touch(self):
        self.mouse_motion(self._mouse_x, self._mouse_y)

    def mouse_motion(self, x, y):
        self._mouse_x = x
        self._mouse_y = y

        if self._drag_flag:
            self._drag_snap(x, y)

        elif self._child_modal is not None:
            self._child_modal.mouse_motion(x, y)

        else:
            [c.mouse_motion(x, y) for c in self._childs]

        self.event_mouse(x, y)

    def mouse_press(self, x, y, button):
        self._mouse_x = x
        self._mouse_y = y

        self.focus_element(None)

        if self._child_modal is not None:
            if self._child_modal.mouse_press(x, y, button):
                return False

        elif next((c for c in self._childs if c.mouse_press(x, y, button)), None) is not None:
            return False

        if button & pyglet.window.mouse.RIGHT or button & pyglet.window.mouse.LEFT:
            self._drag_x = x
            self._drag_y = y
            self._drag_sx = x
            self._drag_sy = y
            self._drag_dst = 0
            self._drag_trv = 0
            self._drag_flag = True
            self._drag_bt = 0 if (button & pyglet.window.mouse.LEFT) else 1

        if button & pyglet.window.mouse.LEFT:
            self._mouse_btn.add(pyglet.window.mouse.LEFT)

        if button & pyglet.window.mouse.RIGHT:
            self._mouse_btn.add(pyglet.window.mouse.RIGHT)

    def mouse_release(self, x, y, button):
        self._mouse_x = x
        self._mouse_y = y

        if self._child_modal is not None:
            if self._child_modal.mouse_release(x, y, button):
                return False

        elif next((c for c in self._childs if c.mouse_release(x, y, button)), None) is not None:
            return False

        if self._drag_flag:
            self._drag_snap(x, y)
            self._drag_flag = False

            if self._drag_trv <= 18.0:
                if self._drag_time > 0 and self._drag_time_bt == self._drag_bt and (time.time() - self._drag_time) < 0.3:
                    self._drag_time = 0
                    self._drag_time_bt = -1
                    self.event_double_click(x, y, self._drag_bt)

                else:
                    self._drag_time = time.time()
                    self._drag_time_bt = self._drag_bt
                    self.event_click(x, y, self._drag_bt)

            elif self._drag_bt == 0:
                sx = min(self._drag_sx, self._drag_x)
                sy = min(self._drag_sy, self._drag_y)
                ex = max(self._drag_sx, self._drag_x)
                ey = max(self._drag_sy, self._drag_y)

                self.event_drag_area(sx, sy, ex, ey, self._drag_bt)

            else:
                #print("drag click")
                pass

        for ln in self._selrect:
            ln.visible = False

        if button & pyglet.window.mouse.LEFT and pyglet.window.mouse.LEFT in self._mouse_btn:
            self._mouse_btn.remove(pyglet.window.mouse.LEFT)

        if button & pyglet.window.mouse.RIGHT and pyglet.window.mouse.RIGHT in self._mouse_btn:
            self._mouse_btn.remove(pyglet.window.mouse.RIGHT)

    def _drag_snap(self, x, y):
        diff_x = x - self._drag_x
        diff_y = y - self._drag_y
        self._drag_trv += math.sqrt(diff_x ** 2 + diff_y ** 2)

        if diff_x != 0 or diff_y != 0:
            self._drag_dx = diff_x
            self._drag_dy = diff_y
            self._drag_dst = math.sqrt(diff_x ** 2 + diff_y ** 2)
            self._drag_kf = 3

            if self._drag_bt == 1:

                if self._drag_trv > self._drag_trv_min:
                    self.event_drag_move(diff_x, diff_y)

            else:
                sx = self._drag_sx
                sy = self._drag_sy
                ex = self._drag_x
                ey = self._drag_y

                self._selrect[0].position = (sx, sy, ex, sy)
                self._selrect[0].visible = True

                self._selrect[1].position = (sx, sy, sx, ey)
                self._selrect[1].visible = True

                self._selrect[2].position = (sx, ey, ex, ey)
                self._selrect[2].visible = True

                self._selrect[3].position = (ex, sy, ex, ey)
                self._selrect[3].visible = True

        self._drag_x = x
        self._drag_y = y

    def on_text(self, text):
        if self._focused is not None:
            self._focused.on_text(text)

    def on_text_motion(self, motion):
        if self._focused is not None:
            self._focused.on_text_motion(motion)

    def keypress(self, symbol):
        self._keys.add(symbol)
        return True

    def keyrelease(self, symbol):
        result = True

        if self._focused is not None and self._focused.get_is_focus_block():
            result = False

        if symbol in self._keys:
            self._keys.remove(symbol)

        return result

    def update(self):
        for ref in self._childs:
            ref.update()

    def draw(self):
        [c.get_batch().draw() for c in reversed(self._childs) if c.get_is_drawable()]
