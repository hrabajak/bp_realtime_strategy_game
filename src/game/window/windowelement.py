import pyglet

from game.graphic.graphbatches import GraphBatches
from game.interface.idrawable import IDrawable
from game.window.position import Position
from game.window.windowmanager import WindowManager


class WindowElement(IDrawable):

    _mng = None
    _parent = None
    _pos: Position = None
    _childs = None
    _graph_init = False
    _graph_update = False
    _visible = True
    _disabled = False
    _hover = False
    _press = 0

    _event_hover: (any, any, int) = lambda self, data, flag: None
    _event_hover_data = None

    def __init__(self, x, y, width, height):
        IDrawable.__init__(self)

        self._pos = Position()
        self._pos.set(x, y, width, height)
        self._pos.event_update = self._pos_update
        self._childs = []
        self._visible = True
        self._disabled = False
        self._hover = False
        self._press = 0

    def set_event_hover(self, func, data):
        self._event_hover = func
        self._event_hover_data = data

    def event_hover(self, arg):
        def decorator(func):
            self._event_hover = func
            self._event_hover_data = arg

        return decorator

    def init(self, parent=None):
        self._mng = WindowManager()
        self._parent = self._mng if parent is None else parent
        self._graph_init = True
        self._graph_update = True
        self.may_draw = 1
        self._pos.set_parent(self._parent.get_pos())
        self._parent.add_element(self)

        GraphBatches().add_drawable(self)

    def destroy(self):
        for ref in [c for c in self._childs]:
            ref.destroy()

        self._childs = []
        self.may_draw = 2
        self._parent.rem_element(self)
        GraphBatches().kill_drawable(self)

    def get_mng(self):
        return self._mng

    def get_batch(self):
        return self._parent.get_batch()

    def get_group(self):
        return self._parent.get_group()

    def get_group_text(self):
        return self._parent.get_group_text()

    def add_element(self, ref):
        self._childs.append(ref)

    def rem_element(self, ref):
        self._childs.remove(ref)

    def raise_element(self, ref):
        self._childs.remove(ref)
        self._childs = [ref] + self._childs

    def draw_destroy(self):
        pass

    def get_pos(self):
        return self._pos

    def get_parent(self):
        return self._parent

    def get_visible(self):
        return self._visible

    def get_hover(self):
        return self._hover

    def get_in_focus(self):
        return self == self._mng.get_focused()

    def get_is_focus_block(self):
        return False

    def get_is_drawable(self):
        return False

    def set_disabled(self, disabled):
        self._disabled = disabled

    def set_visible(self, visible):
        self._visible = visible
        self._graph_update = True

    def _pos_update(self):
        self._graph_update = True

    def mouse_motion(self, x, y):
        hv = self._pos.get_is_inside(x, y) and not self._disabled

        if hv != self._hover:
            self._press = 0
            self._hover = hv
            self._graph_update = True
            self._event_hover(self._event_hover_data, self._hover)

        for ref in self._childs:
            ref.mouse_motion(x, y)

    def mouse_press(self, x, y, button):
        if self._hover and button & pyglet.window.mouse.LEFT and not self._disabled:
            if next((c for c in self._childs if c.get_hover() and c.mouse_press(x, y, button)), False):
                return True

            self._press = 1
            self._graph_update = True
            self._mng.focus_element(self)
            return True

        return False

    def mouse_release(self, x, y, button):
        for ref in self._childs:
            if ref.get_hover():
                ref.mouse_release(x, y, button)

        if self._press == 1 and not self._disabled:
            self._press = 2
            self._graph_update = True
            return True

        return False

    def on_text(self, text):
        pass

    def on_text_motion(self, motion):
        pass

    def focus_enter(self):
        self._graph_update = True

    def focus_leave(self):
        self._graph_update = True

    def organize_x(self, margin=0, start=0):
        pos_x = margin + start

        for ref in self._childs:
            ref.get_pos().set_x(pos_x)
            pos_x += ref.get_pos().get_width() + margin

        return pos_x

    def organize_y(self, margin=0, start=0):
        pos_y = margin + start

        for ref in self._childs:
            ref.get_pos().set_y(pos_y)
            pos_y += ref.get_pos().get_height() + margin

        return pos_y

    def pos_update(self):
        self.get_pos().update()

        for ref in self._childs:
            ref.pos_update()

    def update(self):
        for ref in self._childs:
            ref.update()

    def draw(self):
        if self.may_draw != 1:
            return False

        if self._graph_init:
            self._graph_init = False

        if self._graph_update:
            self._graph_update = False
