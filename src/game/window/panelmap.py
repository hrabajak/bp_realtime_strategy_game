import pyglet

from game.image.imagecollector import ImageCollector
from game.interface.itilecollector import ITileCollector
from game.map.mapsnapshot import MapSnapshot
from game.window.windowelement import WindowElement


class PanelMap(WindowElement):

    _rect = None
    _img = None
    _batch = None
    _group = None
    _group_img = None
    _color = (142, 142, 142)
    _color_brd = (100, 90, 90)
    _draw_tick = 0
    _map_update = 0
    _sn_ref = None
    _tile_col = None

    _event_point: (any, any, int, int) = lambda self, data, mx, my: None
    _event_point_data = None

    def __init__(self, x, y, width, height):
        WindowElement.__init__(self, x, y, width, height)

        self._draw_tick = 0
        self._map_update = 0
        self._sn_ref = MapSnapshot(2)
        self._tile_col = ITileCollector.get()

    def get_batch(self):
        return self._batch

    def get_group(self):
        return self._group

    def get_is_drawable(self):
        return self._batch is not None

    def get_mouse_map_point(self, x, y):
        mx = ((x - self.get_pos().get_x()) / self.get_pos().get_width()) * self._tile_col.get_tiles_width() * 2 * 25.0
        my = ((y - self.get_pos().get_y()) / self.get_pos().get_height()) * self._tile_col.get_tiles_height() * 2 * 25.0

        return mx, my

    def map_update(self, view):
        self._sn_ref.set_view(view)
        self._map_update = 1
        self._graph_update = True

    def raise_to_top(self):
        self.get_parent().raise_element(self)

    def mouse_motion(self, x, y):
        hv = self._pos.get_is_inside(x, y) and not self._disabled

        if hv != self._hover:
            self._press = 0
            self._hover = hv
            self._graph_update = True
            self._event_hover(self._event_hover_data, self._hover)

        if self._hover and self._press == 1:
            c = self.get_mouse_map_point(x, y)
            self._event_point(self._event_point_data, c[0], c[1])

        for ref in self._childs:
            ref.mouse_motion(x, y)

    def mouse_release(self, x, y, button):
        if self._press == 1:
            self._press = 2

            c = self.get_mouse_map_point(x, y)
            self._event_point(self._event_point_data, c[0], c[1])
            return True

        return False

    def set_event_point(self, func, data):
        self._event_point = func
        self._event_point_data = data

    def event_point(self, arg):
        def decorator(func):
            self._event_point = func
            self._event_point_data = arg

        return decorator

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                if self._img is not None:
                    self._img.delete()
                    self._img = None

                if self._batch is not None:
                    self._batch.invalidate()
                    self._batch = None

                self._group = None
                self._group_img = None
                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True

            self._batch = pyglet.graphics.Batch()
            self._group = pyglet.graphics.OrderedGroup(8)
            self._group_img = pyglet.graphics.OrderedGroup(9)
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color, border_color=self._color_brd, border=1, batch=self.get_batch(), group=self.get_group())

            self._img = pyglet.sprite.Sprite(img=ImageCollector().get_gui_image("test"), group=self._group_img, batch=self._batch)

        if self._graph_update:
            self._rect.visible = self._visible
            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get(-2, -2)

            if self._map_update == 1:
                self._map_update = 2

                self._sn_ref.update_map(self._tile_col)
                self._img.image = self._sn_ref.get_img()

            m = self._pos.get_mid()
            scale_x = 1 / (self._img.image.width / self._pos.get_width())
            scale_y = 1 / (self._img.image.height / self._pos.get_height())

            self._img.visible = self._visible and self._map_update == 2
            self._img.update(x=m[0], y=m[1], scale_x=scale_x, scale_y=scale_y)

            self._graph_update = False
