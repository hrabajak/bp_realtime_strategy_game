from game.window.listbox import ListBox
from game.window.position import Position


class SelectBoxList(ListBox):

    _select = None

    def __init__(self, select):
        self._select = select

        p_wd = self._select.get_pos().get_width() - self._select.get_capt_width()
        p_he = 25 * len(self._select.item_get_all()) + 10

        px = self._select.get_pos().get_x() + self._select.get_capt_width()
        py = self._select.get_pos().get_y() - p_he

        ListBox.__init__(self, px, py, p_wd, p_he)
        self.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_BOTTOM)

    def get_select(self):
        return self._select

    def init_select(self):
        self.init(parent=None)
        ac_item = self._select.item_get()

        for it in self._select.item_get_all():
            it = self.item_add(it.get_key(), it.get_value())

            if ac_item is not None and ac_item.get_key() == it.get_key():
                it.set_active(True)

        self.get_parent().raise_element(self, modal=True)

    def item_select(self, key):
        super().item_select(key)
        self._select.item_select(key)
