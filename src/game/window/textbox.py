import pyglet

from game.window.windowelement import WindowElement


class TextBox(WindowElement):

    _rect = None
    _doc = None
    _layout = None
    _caret = None
    _value = ""
    _capt_label = None
    _capt_width = 0
    _capt = None
    _is_numeric = False

    _color_brd_normal = (70, 70, 70)
    _color_brd_hover = (90, 90, 90)
    _color_brd_focus = (0, 94, 14)
    _color_back = (255, 255, 255)
    _color_back_focus = (232, 255, 235)

    _event_change: (any, any) = lambda self, data: None
    _event_change_data = None

    def __init__(self, x, y, width, height=30, value=''):
        WindowElement.__init__(self, x, y, width, height)

        self._value = value
        self._is_numeric = False

    def set_event_change(self, func, data):
        self._event_change = func
        self._event_change_data = data

    def event_change(self, arg):
        def decorator(func):
            self._event_change = func
            self._event_change_data = arg
        return decorator

    def get_value(self):
        return self._value

    def get_value_int(self):
        try:
            i_val = int(self._value)
        except ValueError:
            i_val = 0

        return i_val

    def get_is_focus_block(self):
        return True

    def set_numeric(self, is_numeric):
        self._is_numeric = is_numeric

    def set_value(self, value):
        self._value = str(value)

    def set_caption(self, capt, capt_width):
        self._capt = capt
        self._capt_width = capt_width
        self._graph_update = True

    def on_text(self, text):
        if self._caret is None:
            pass
        elif text in ('\r', '\n'):
            self._mng.focus_element(None)
        elif text in ('\t'):
            pass
        elif self._is_numeric:
            if text.isnumeric():
                self._caret.on_text(text)
                self._value = self._doc.text
                self._event_change(self._event_change_data)

        else:
            self._caret.on_text(text)
            self._value = self._doc.text
            self._event_change(self._event_change_data)

    def on_text_motion(self, motion):
        if self._caret is not None:
            self._caret.on_text_motion(motion)
            self._value = self._doc.text

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

        if self._caret is not None:
            self._caret.delete()

        if self._layout is not None:
            self._layout.delete()

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                if self._layout is not None:
                    self._layout.delete()
                    self._layout = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color_back, batch=self.get_batch(), group=self.get_group(), border_color=self._color_brd_normal, border=4)

            self._doc = pyglet.text.document.UnformattedDocument(self._value)
            self._doc.set_style(0, len(self._value), dict(font_name=self._mng.get_font(), font_size=self._mng.get_font_size(), color=(0, 0, 0, 255)))

            self._layout = pyglet.text.layout.IncrementalTextLayout(self._doc, width=self._pos.get_width(), height=self._pos.get_height(), multiline=False, batch=self.get_batch(), group=self.get_group_text())
            self._layout.x = self._pos.get_x()
            self._layout.y = self._pos.get_y()

            self._caret = pyglet.text.caret.Caret(self._layout, color=(0, 0, 0))
            self._caret.position = len(self._value)
            self._caret.visible = False

        if self._graph_update:
            if self._capt_label is None and self._capt is not None:
                self._capt_label = pyglet.text.Label(self._capt, x=0, y=0, width=self._capt_width, anchor_y="center", align="left", multiline=True, font_name=self._mng.get_font(), font_size=self._mng.get_font_size(), color=(255, 255, 255, 255), batch=self.get_batch(), group=self.get_group_text())

            self._layout.visible = self._visible
            self._rect.visible = self._visible
            self._caret.visible = self.get_in_focus()

            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get(padd_left=self._capt_width, add_width=-self._capt_width)
            self._layout.x, self._layout.y, self._layout.width, self._layout.height = self._pos.get(padd_left=self._capt_width, padd_x=8, padd_top=-6, add_width=-self._capt_width)

            if self._capt_label is not None:
                self._capt_label.visible = self._visible
                self._capt_label.x = self._pos.get_x()
                self._capt_label.y = self._pos.get_y() + self._pos.get_height() / 2

            self._graph_update = False

            if self.get_in_focus():
                self._rect._brgb = self._color_brd_focus
                self._rect.color = self._color_back_focus

            elif self._hover:
                self._rect._brgb = self._color_brd_hover
                self._rect.color = self._color_back

            else:
                self._rect._brgb = self._color_brd_normal
                self._rect.color = self._color_back
