from game.window.button import Button
from lib.orderedset import OrderedSet


class ButtonAction(Button):

    _action_item = None
    _rest_items = None
    _hash_key = None

    def __init__(self, x, y, width, height, caption, action_item):
        Button.__init__(self, x, y, width, height, caption)

        self._action_item = action_item
        self._hash_key = action_item.get_hash_key()
        self._rest_items = OrderedSet()
        self._rest_items.add(action_item)
        self._color_normal = (77, 77, 77)
        self._color_border = (43, 43, 43)
        self._disabled = False

    def get_action_item(self):
        return self._action_item

    def get_hash_key(self):
        return self._hash_key

    def add_action_item(self, action_item):
        if action_item not in self._rest_items:
            self._rest_items.add(action_item)

    def check_action_queue(self, actions):
        for ac in [c for c in self._rest_items]:
            if ac not in actions.get_action_queue():
                self._rest_items.remove(ac)

        self._action_item = next((c for c in self._rest_items), None)
        return self._action_item is not None

    def _toggle_avail(self):
        self._color_normal = (0, 105, 7)
        self._color_hover = (0, 137, 10)
        self._color_down = (0, 176, 207)
        self._color_border = (13, 71, 17)
        self._disabled = False
        self._graph_update = True

    def update(self):
        self._caption = self._action_item.get_name(len(self._rest_items))
        self._graph_update = True

        if self._action_item.get_flag():
            self._toggle_avail()
