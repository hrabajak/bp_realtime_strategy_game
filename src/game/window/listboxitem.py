import pyglet

from game.window.windowelement import WindowElement


class ListBoxItem(WindowElement):

    _rect = None
    _label = None
    _key = None
    _value = None
    _active = False

    _color_normal = (255, 255, 255)
    _color_hover = (209, 228, 255)
    _color_press = (168, 202, 255)
    _color_selected = (232, 255, 235)

    def __init__(self, x, y, key, value):
        WindowElement.__init__(self, x, y, 50, 25)

        self._key = key
        self._value = value
        self._active = False

    def get_is_focus_block(self):
        return True

    def get_key(self):
        return self._key

    def get_value(self):
        return self._value

    def get_active(self):
        return self._active

    def set_value(self, value):
        self._value = value
        self._graph_update = True

    def set_active(self, active):
        self._active = active
        self._graph_update = True

    def mouse_release(self, x, y, button):
        if self._press == 1:
            self._press = 2
            self._parent.item_select(self._key)
            return True

        return False

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

        if self._label is not None:
            self._label.delete()

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                if self._label is not None:
                    self._label.delete()
                    self._label = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color_normal, batch=self.get_batch(), group=self.get_group(), border=0)
            self._label = pyglet.text.Label(self._value, x=0, y=0, anchor_y="center", font_name=self._mng.get_font(), font_size=self._mng.get_font_size(), color=(0, 0, 0, 255), batch=self.get_batch(), group=self.get_group_text())

        if self._graph_update:
            self._graph_update = False
            self._label.visible = self._visible
            self._rect.visible = self._visible

            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get()

            self._label.text = self._value
            self._label.x, self._label.y, self._label.width, self._label.height = self.get_pos().get_align(0, 0, 8, self._mng.get_font_size())

            if not self._visible:
                self._rect.visible = False

            elif self._press == 1:
                self._rect.color = self._color_press

            elif self._active:
                self._rect.color = self._color_selected

            elif self._hover:
                self._rect.color = self._color_hover

            else:
                self._rect.color = self._color_normal
