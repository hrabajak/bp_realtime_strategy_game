

class SelectBoxItem:

    _key = None
    _value = None
    _active = False

    def __init__(self, key, value):
        self._key = key
        self._value = value
        self._active = False

    def get_key(self):
        return self._key

    def get_value(self):
        return self._value

    def get_active(self):
        return self._active
