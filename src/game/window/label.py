import pyglet

from game.graphic.graphbatches import GraphBatches
from game.image.imagecollector import ImageCollector
from game.window.position import Position
from game.window.windowelement import WindowElement


class Label(WindowElement):

    _label = None
    _caption = ""
    _font_size = None
    _align_koef = 0.1
    _align_str = 'left'
    _text_align_str = 'left'
    _opacity = 255
    _image_name = None
    _sprite_image = None
    _y_add = 0

    _event_click: (any, any) = lambda self, data: None
    _event_click_data = None

    def __init__(self, x, y, width, height, caption, align=Position.H_ALIGN_LEFT, text_align="left"):
        WindowElement.__init__(self, x, y, width, height)

        self.set_text(caption)
        self.set_align(align)
        self._text_align_str = text_align
        self._opacity = 255
        self._image_name = None

    def set_image_name(self, image_name):
        self._image_name = image_name
        self._graph_update = True

    def init(self, parent=None):
        super().init(parent)
        self._font_size = self._mng.get_font_size()

    def set_text(self, caption):
        self._caption = caption
        self._graph_update = True

    def set_font_size(self, font_size):
        self._font_size = font_size
        self._graph_update = True

    def set_y_add(self, y_add):
        self._y_add = y_add

    def set_align(self, align):
        self._align_koef = 0.0
        self._align_str = 'left'
        self._graph_update = True

        if align == Position.H_ALIGN_CENTER:
            self._align_koef = 0.5
            self._align_str = 'center'
        elif align == Position.H_ALIGN_RIGHT:
            self._align_koef = 1.0
            self._align_str = 'right'

    def set_opacity(self, opacity):
        self._opacity = opacity
        self._graph_update = True

    def draw_destroy(self):
        if self._label is not None:
            self._label.delete()
            self._label = None

    def mouse_release(self, x, y, button):
        if self._press == 1:
            self._press = 2
            self._event_click(self._event_click_data)
            return True

        return False

    def set_event_click(self, func, data):
        self._event_click = func
        self._event_click_data = data

    def event_click(self, arg):
        def decorator(func):
            self._event_click = func
            self._event_click_data = arg

        return decorator

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._label is not None:
                    self._label.delete()
                    self._label = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._label = pyglet.text.Label(self._caption, x=0, y=0, width=self.get_pos().get_width(), anchor_y="center", align=self._text_align_str, multiline=True, font_name=self._mng.get_font(), font_size=self._font_size, color=(255, 255, 255, self._opacity), batch=self.get_batch(), group=self.get_group_text())

        if self._graph_update:
            self._graph_update = False

            if self._image_name is None:
                pass
            elif self._sprite_image is None:
                GraphBatches().add_batch_group(self.get_batch(), self.get_group_text())
                self._sprite_image = GraphBatches().create_sprite(img=ImageCollector().get_image(self._image_name), batch=self.get_batch(), group=self.get_group_text())
            else:
                self._sprite_image.image = ImageCollector().get_image(self._image_name)

            self._label.text = self._caption
            self._label.visible = self._visible
            self._label.x, self._label.y, self._label.width, self._label.height = self.get_pos().get_align(h_koef=self._align_koef, padd_x=0 if self._sprite_image is None else self._sprite_image.width + 8, padd_y=self._font_size + self._y_add)
            self._label.anchor_x = self._align_str
            self._label.font_size = self._font_size
            self._label.color = (255, 255, 255, self._opacity)

            if self._sprite_image is not None:
                self._sprite_image.x, self._sprite_image.y = self._pos.get_x() + self._sprite_image.width / 2, self._pos.get_y() + self._pos.get_height() / 2
