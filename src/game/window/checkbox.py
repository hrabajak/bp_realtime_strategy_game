import pyglet

from game.window.position import Position
from game.window.windowelement import WindowElement


class CheckBox(WindowElement):

    _rect = None
    _box = None
    _label = None
    _caption = ""
    _font_size = None
    _align_koef = 0.1
    _align_str = 'left'
    _box_size = 20
    _checked = False

    _color_brd_normal = (70, 70, 70)
    _color_brd_hover = (90, 90, 90)
    _color_brd_focus = (0, 94, 14)
    _color_back = (255, 255, 255)
    _color_back_focus = (232, 255, 235)
    _color_box = (0, 0, 0)
    _color_box_press = (120, 120, 120)

    _event_change: (any, any) = lambda self, data: None
    _event_change_data = None

    def __init__(self, x, y, width, caption, height=30, align=Position.H_ALIGN_LEFT):
        WindowElement.__init__(self, x, y, width, height)

        self.set_text(caption)
        self.set_align(align)
        self._checked = False

    def get_checked(self):
        return self._checked

    def set_checked(self, checked):
        self._checked = checked

    def mouse_release(self, x, y, button):
        if self._press == 1:
            self._press = 2
            self._checked = not self._checked
            self._graph_update = True
            self._event_change(self._event_change_data)
            return True

        return False

    def set_event_change(self, func, data):
        self._event_change = func
        self._event_change_data = data

    def event_change(self, arg):
        def decorator(func):
            self._event_change = func
            self._event_change_data = arg
        return decorator

    def init(self, parent=None):
        super().init(parent)
        self._font_size = self._mng.get_font_size()

    def set_text(self, caption):
        self._caption = caption
        self._graph_update = True

    def set_font_size(self, font_size):
        self._font_size = font_size
        self._graph_update = True

    def set_align(self, align):
        self._align_koef = 0.0
        self._align_str = 'left'
        self._graph_update = True

        if align == Position.H_ALIGN_CENTER:
            self._align_koef = 0.5
            self._align_str = 'center'
        elif align == Position.H_ALIGN_RIGHT:
            self._align_koef = 1.0
            self._align_str = 'right'

    def get_box_pos(self, padd=0):
        return self._pos.get_x() + padd, self._pos.get_y() + (self._pos.get_height() - self._box_size) / 2 + padd, self._box_size - padd * 2, self._box_size - padd * 2

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

        if self._box is not None:
            self._box.delete()

        if self._label is not None:
            self._label.delete()

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._label is not None:
                    self._label.delete()
                    self._label = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._rect = pyglet.shapes.BorderedRectangle(*self.get_box_pos(), color=self._color_back, batch=self.get_batch(), group=self.get_group(), border_color=self._color_brd_normal, border=4)
            self._box = pyglet.shapes.BorderedRectangle(*self.get_box_pos(), color=self._color_box, batch=self.get_batch(), group=self.get_group())
            self._label = pyglet.text.Label(self._caption, x=0, y=0, width=self.get_pos().get_width(), anchor_y="center", align="left", multiline=True, font_name=self._mng.get_font(), font_size=self._font_size, color=(255, 255, 255, 255), batch=self.get_batch(), group=self.get_group_text())

        if self._graph_update:
            self._graph_update = False

            self._rect.visible = self._visible
            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self.get_box_pos()

            self._box.visible = (self._visible and self._checked) or self._press == 1
            self._box.x, self._box.y, self._box.width, self._box.height = self.get_box_pos(5)

            self._label.text = self._caption
            self._label.visible = self._visible
            self._label.x, self._label.y, self._label.width, self._label.height = self.get_pos().get_align(self._align_koef, 0, 30, self._font_size)
            self._label.anchor_x = self._align_str

            if self._press == 1:
                self._box.color = self._color_box_press
            else:
                self._box.color = self._color_box

            if self.get_in_focus():
                self._rect._brgb = self._color_brd_focus
                self._rect.color = self._color_back_focus
            elif self._hover:
                self._rect._brgb = self._color_brd_hover
                self._rect.color = self._color_back
            else:
                self._rect._brgb = self._color_brd_normal
                self._rect.color = self._color_back
