
class Position:

    H_ALIGN_LEFT = 0
    H_ALIGN_CENTER = 1
    H_ALIGN_RIGHT = 2

    V_ALIGN_BOTTOM = 0
    V_ALIGN_CENTER = 1
    V_ALIGN_TOP = 2

    _x = 0
    _y = 0
    _width = 0
    _height = 0
    _h_align = 0
    _v_align = 0

    _use_x = 0
    _use_y = 0
    _use_l_x = 0
    _use_l_y = 0

    _parent = None

    event_update: (any) = lambda self: None

    def __init__(self):
        self._h_align = Position.H_ALIGN_LEFT
        self._v_align = Position.V_ALIGN_TOP

    def get_is_inside(self, x, y):
        return x >= self._use_x and y >= self._use_y and x <= self._use_l_x and y <= self._use_l_y

    def get(self, padd_x=0, padd_y=0, padd_top=0, padd_left=0, add_width=0, add_height=0):
        return self._use_x + padd_x + padd_left, self._use_y + padd_y + padd_top, (self._width + add_width) - 2 * padd_x, (self._height + add_height) - 2 * padd_y

    def get_align(self, h_koef=0, v_koef=0, padd_x=0, padd_y=0):
        return self._use_x + h_koef * self._width + padd_x, self._use_y + v_koef * self._height + padd_y, self._width, self._height

    def get_mid(self):
        return self._use_x + self._width // 2, self._use_y + self._height // 2, self._width, self._height

    def get_x(self):
        return self._use_x

    def get_y(self):
        return self._use_y

    def get_width(self):
        return self._width

    def get_height(self):
        return self._height

    def get_origin(self, h_align, v_align):
        px = self._use_x
        py = self._use_y
        inc_x = 1
        inc_y = 1
        inc_d_x = 0
        inc_d_y = 0

        if h_align == Position.H_ALIGN_CENTER:
            px = self._use_x + self._width // 2
            inc_d_x = -0.5

        elif h_align == Position.H_ALIGN_RIGHT:
            px = self._use_x + self._width
            inc_x = -1
            inc_d_x = -1

        if v_align == Position.V_ALIGN_CENTER:
            py = self._use_y + self._height // 2
            inc_d_y = -0.5

        elif v_align == Position.V_ALIGN_TOP:
            py = self._use_y + self._height
            inc_y = -1
            inc_d_y = -1

        return px, py, inc_x, inc_y, inc_d_x, inc_d_y

    def get_parent(self):
        return self._parent

    def set_align(self, h_align, v_align):
        self._h_align = h_align
        self._v_align = v_align
        self.update()

    def set_x(self, x):
        self._x = x
        self.update()

    def set_y(self, y):
        self._y = y
        self.update()

    def set_width(self, width):
        self._width = width
        self.update()

    def set_height(self, height):
        self._height = height
        self.update()

    def set(self, x, y, width, height):
        self._x = x
        self._y = y
        self._width = width
        self._height = height
        self.update()

    def set_parent(self, parent):
        self._parent = parent
        self.update()

    def update(self):
        self._use_x = 0
        self._use_y = 0

        if self._parent is not None:
            self._parent.update()
            px, py, inc_x, inc_y, inc_d_x, inc_d_y = self._parent.get_origin(self._h_align, self._v_align)

            self._use_x = int(px + inc_x * self._x + inc_d_x * self._width)
            self._use_y = int(py + inc_y * self._y + inc_d_y * self._height)

        else:
            self._use_x = self._x
            self._use_y = self._y

        self._use_l_x = self._use_x + self._width
        self._use_l_y = self._use_y + self._height

        self.event_update()
