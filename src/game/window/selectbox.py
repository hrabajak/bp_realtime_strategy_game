import pyglet

from game.window.listbox import ListBox
from game.window.selectboxitem import SelectBoxItem
from game.window.selectboxlist import SelectBoxList
from game.window.windowelement import WindowElement


class SelectBox(WindowElement):

    _rect = None
    _arrow = None
    _label = None
    _items = None
    _item_active = None
    _font_size = None
    _listbox = None
    _capt_label = None
    _capt_width = 0
    _capt = None

    _color_brd_normal = (70, 70, 70)
    _color_brd_hover = (90, 90, 90)
    _color_brd_focus = (0, 94, 14)
    _color_back = (255, 255, 255)
    _color_back_focus = (232, 255, 235)

    _color_arrow = (120, 120, 120)
    _color_arrow_press = (180, 180, 180)
    _color_arrow_brd_normal = (82, 82, 82)
    _color_arrow_brd_hover = (105, 105, 105)

    _event_change: (any, any) = lambda self, data: None
    _event_change_data = None

    def __init__(self, x, y, width, height=30):
        WindowElement.__init__(self, x, y, width, height)

        self._items = []
        self._item_active = None
        self._listbox = None

    def item_add(self, key, value):
        it = self.item_get(key)

        if it is None:
            it = SelectBoxItem(key, value)
            self._items.append(it)
        else:
            it.set_value(value)

        if self._item_active is None:
            self._item_active = it

        self._graph_update = True

        return it

    def item_get(self, key=None):
        if key is None:
            return self._item_active
        else:
            return next((c for c in self._items if c.get_key() == key), None)

    def item_get_all(self):
        return self._items

    def item_select(self, key):
        if self._item_active is None or self._item_active.get_key() != key:
            self._item_active = next((c for c in self._items if c.get_key() == key), None)
            self._event_change(self._event_change_data)

        if self._listbox is not None:
            self._listbox.destroy()
            self._listbox = None

        self._graph_update = True

    def item_remove(self, key):
        it = self.item_get(key)

        if it is not None and self._item_active is it:
            self._item_active = None
            self._graph_update = True

    def item_set_dict(self, items):
        rem_keys = [c.get_key() for c in self._items]

        for key in items.keys():
            it = self.item_get(key)

            if it is None:
                self.item_add(key, items[key])
            else:
                it.set_value(items[key])
                rem_keys.remove(key)

        for key in rem_keys:
            self.item_remove(key)

    def mouse_release(self, x, y, button):
        if self._press == 1:
            self._press = 2
            self._graph_update = True
            self.open_list()
            return True

        return False

    def set_event_change(self, func, data):
        self._event_change = func
        self._event_change_data = data

    def event_change(self, arg):
        def decorator(func):
            self._event_change = func
            self._event_change_data = arg

        return decorator

    def set_caption(self, capt, capt_width):
        self._capt = capt
        self._capt_width = capt_width
        self._graph_update = True

    def init(self, parent=None):
        super().init(parent)
        self._font_size = self._mng.get_font_size()

    def open_list(self):
        if self._listbox is not None:
            self._listbox.destroy()
            self._listbox = None

        self._listbox = SelectBoxList(self)
        self._listbox.init_select()

    def get_box_pos(self):
        return self._pos.get_x() + self._capt_width, self._pos.get_y(), self._pos.get_height(), self._pos.get_height()

    def get_capt_width(self):
        return self._capt_width

    def get_selected_key(self):
        return "" if self._item_active is None else self._item_active.get_key()

    def set_selected_key(self, key):
        it = self.item_get(key)
        if it is not None:
            self._item_active = it
            self._graph_update = True

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

        if self._arrow is not None:
            self._arrow.delete()

        if self._label is not None:
            self._label.delete()

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._arrow = pyglet.shapes.BorderedRectangle(*self.get_box_pos(), color=self._color_arrow, batch=self.get_batch(), group=self.get_group(), border_color=self._color_arrow_brd_normal, border=4)
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color_back, batch=self.get_batch(), group=self.get_group(), border_color=self._color_brd_normal, border=4)
            self._label = pyglet.text.Label("", x=0, y=0, width=self.get_pos().get_width(), anchor_y="center", align="left", multiline=True, font_name=self._mng.get_font(), font_size=self._font_size, color=(0, 0, 0, 255), batch=self.get_batch(), group=self.get_group_text())

        if self._graph_update:
            if self._capt_label is None and self._capt is not None:
                self._capt_label = pyglet.text.Label(self._capt, x=0, y=0, width=self._capt_width, anchor_y="center", align="left", multiline=True, font_name=self._mng.get_font(), font_size=self._mng.get_font_size(), color=(255, 255, 255, 255), batch=self.get_batch(), group=self.get_group_text())

            self._arrow.visible = self._visible
            self._arrow.x, self._arrow.y, self._arrow.width, self._arrow.height = self.get_box_pos()

            self._rect.visible = self._visible
            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get(padd_left=self._pos.get_height() + self._capt_width, add_width=-(self._pos.get_height() + self._capt_width))

            self._label.text = "" if self._item_active is None else self._item_active.get_value()
            self._label.visible = self._visible
            self._label.x, self._label.y, self._label.width, self._label.height = self.get_pos().get_align(padd_x=self._pos.get_height() + 10 + self._capt_width, padd_y=self._font_size)

            if self._capt_label is not None:
                self._capt_label.visible = self._visible
                self._capt_label.x = self._pos.get_x()
                self._capt_label.y = self._pos.get_y() + self._pos.get_height() / 2

            self._graph_update = False

            if self._press == 1:
                self._arrow.color = self._color_arrow_press
            else:
                self._arrow.color = self._color_arrow

            if self.get_in_focus():
                self._rect._brgb = self._color_brd_focus
                self._rect.color = self._color_back_focus
                self._arrow.border_color = self._color_arrow_brd_normal
            elif self._hover:
                self._rect._brgb = self._color_brd_hover
                self._rect.color = self._color_back
                self._arrow.border_color = self._color_arrow_brd_hover
            else:
                self._rect._brgb = self._color_brd_normal
                self._rect.color = self._color_back
                self._arrow.border_color = self._color_arrow_brd_normal
