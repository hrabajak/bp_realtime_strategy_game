import pyglet

from game.graphic.graphbatches import GraphBatches
from game.image.imagecollector import ImageCollector
from game.window.windowelement import WindowElement


class Button(WindowElement):

    _rect = None
    _text = None
    _caption = ""
    _image_name = None
    _sprite_image = None

    _color_normal = (77, 77, 77)
    _color_hover = (91, 91, 91)
    _color_down = (122, 122, 122)
    _color_border = (50, 50, 50)

    _event_click: (any, any) = lambda self, data: None
    _event_click_data = None

    def __init__(self, x, y, width, height, caption):
        WindowElement.__init__(self, x, y, width, height)

        self._caption = caption
        self._image_name = None

    def set_image_name(self, image_name):
        self._image_name = image_name
        self._graph_update = True

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.delete()

        if self._text is not None:
            self._text.delete()

        GraphBatches().add_sprite_delete(self._sprite_image)

    def mouse_release(self, x, y, button):
        if self._press == 1:
            self._press = 2
            self._graph_update = True
            self._event_click(self._event_click_data)
            return True

        return False

    def set_event_click(self, func, data):
        self._event_click = func
        self._event_click_data = data

    def event_click(self, arg):
        def decorator(func):
            self._event_click = func
            self._event_click_data = arg
        return decorator

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                if self._rect is not None:
                    self._rect.delete()
                    self._rect = None

                if self._text is not None:
                    self._text.delete()
                    self._text = None

                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._rect = pyglet.shapes.BorderedRectangle(*self._pos.get(), color=self._color_normal, batch=self.get_batch(), group=self.get_group(), border_color=self._color_border)

            doc = pyglet.text.document.UnformattedDocument(self._caption)
            doc.set_style(0, len(doc.text), dict(font_name=self._mng.get_font(), font_size=self._mng.get_font_size(), color=(255, 255, 255, 255)))

            self._text = pyglet.text.layout.TextLayout(doc, width=self._pos.get_width(), height=self._pos.get_height(), multiline=True, batch=self.get_batch(), group=self.get_group_text())
            self._text.anchor_x = 'left'
            self._text.anchor_y = 'bottom'
            self._text.content_valign = 'center'

        if self._graph_update:
            self._graph_update = False

            if self._image_name is None:
                pass
            elif self._sprite_image is None:
                GraphBatches().add_batch_group(self.get_batch(), self.get_group_text())
                self._sprite_image = GraphBatches().create_sprite(img=ImageCollector().get_image(self._image_name), batch=self.get_batch(), group=self.get_group_text())
            else:
                self._sprite_image.image = ImageCollector().get_image(self._image_name)

            self._text.document.text = self._caption
            self._text.visible = self._visible
            self._rect.visible = self._visible

            self._rect.x, self._rect.y, self._rect.width, self._rect.height = self._pos.get()
            self._text.x, self._text.y, self._text.width, self._text.height = self._pos.get(padd_x=15, padd_left=0 if self._sprite_image is None else self._sprite_image.width)

            if self._sprite_image is not None:
                self._sprite_image.x, self._sprite_image.y = self._rect.x + self._sprite_image.width / 2 + 8, self._rect.y + self._rect.height / 2

            if self._press == 1:
                self._rect.color = self._color_down
            elif self._hover:
                self._rect.color = self._color_hover
            else:
                self._rect.color = self._color_normal
