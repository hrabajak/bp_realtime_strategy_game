from game.graphic.graphbatches import GraphBatches
from game.image.imagecollector import ImageCollector
from game.window.windowelement import WindowElement


class ImagePanel(WindowElement):

    _sprite = None
    _image_name = None

    def __init__(self, x, y, width, height, image_name):
        WindowElement.__init__(self, x, y, width, height)

        self._image_name = image_name

    def set_image_name(self, image_name):
        self._image_name = image_name
        self._graph_update = True

    def draw_destroy(self):
        GraphBatches().add_sprite_delete(self._sprite)

    def draw(self):
        if self.may_draw != 1:
            if self.may_draw == 2:
                self.may_draw = 3

            return False

        if self._graph_init:
            self.draw_destroy()
            self._graph_init = None
            self._graph_update = True
            self._sprite = GraphBatches().create_sprite(img=ImageCollector().get_image(self._image_name), batch=self.get_batch())

        if self._graph_update:
            self._graph_update = False
            self._sprite.image = ImageCollector().get_image(self._image_name)

            self._sprite.x, self._sprite.y, wd, he = self._pos.get()
