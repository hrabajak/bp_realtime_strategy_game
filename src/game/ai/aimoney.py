from collections import OrderedDict

from game.ai.aimoneyslot import AiMoneySlot


class AiMoney:

    _slots = None

    def __init__(self):
        self._slots = OrderedDict()
        self._slots[AiMoneySlot.SLOT_RESOURCES] = AiMoneySlot(AiMoneySlot.SLOT_RESOURCES, 1, 1500)
        self._slots[AiMoneySlot.SLOT_BUILD] = AiMoneySlot(AiMoneySlot.SLOT_BUILD, 1)
        self._slots[AiMoneySlot.SLOT_DEFENSE] = AiMoneySlot(AiMoneySlot.SLOT_DEFENSE, 1)
        self._slots[AiMoneySlot.SLOT_UPGRADE] = AiMoneySlot(AiMoneySlot.SLOT_UPGRADE, 1)
        self._slots[AiMoneySlot.SLOT_UNITS] = AiMoneySlot(AiMoneySlot.SLOT_UNITS, 1)

    def get_money(self, tp):
        return self._slots[tp].get_money()

    def dec_slot(self, tp, value):
        if value <= self._slots[tp].get_money():
            self._slots[tp].add_money(-value)
        else:
            value -= self._slots[tp].get_money()
            self._slots[tp].set_money(0)
            self.add_money(-value)

    def inc_slot(self, tp, value):
        self._slots[tp].add_money(value)

    def add_money(self, value):
        w_total = 0
        tp_last = -1

        for tp in self._slots.keys():
            if self._slots[tp].get_weight() > 0:
                w_total += self._slots[tp].get_weight()
                tp_last = tp

        for tp in self._slots.keys():
            if self._slots[tp].get_weight() > 0:
                if tp == tp_last:
                    to_add = value
                else:
                    to_add = (value * 1000 * self._slots[tp].get_weight()) // (w_total * 1000)
                    w_total -= self._slots[tp].get_weight()

                if self._slots[tp].get_max_money() > 0 and self._slots[tp].get_money() + to_add > self._slots[tp].get_max_money():
                    to_add = max(self._slots[tp].get_max_money() - self._slots[tp].get_money(), 0)

                value -= to_add
                self._slots[tp].add_money(to_add)

        if value > 0 and tp_last != -1:
            self._slots[tp_last].add_money(value)

    def set_weights(self, refill_when_changed, *args):
        tps = set()
        any_changed = False

        if len(args) > 0:
            [tps.add(tp) for tp in self._slots.keys()]

            for i in range(0, len(args), 2):
                tp = args[i]
                we = args[i + 1]

                if self._slots[tp].set_weight(we):
                    any_changed = True

                tps.remove(tp)
        else:
            for tp in self._slots.keys():
                if self._slots[tp].set_weight(1):
                    any_changed = True

        rest_money = 0

        for tp in tps:
            rest_money += self._slots[tp].pop_all_money()
            if self._slots[tp].set_weight(0):
                any_changed = True

        if any_changed and refill_when_changed:
            # redistribute all money
            for tp in self._slots.keys():
                rest_money += self._slots[tp].pop_all_money()

        if rest_money > 0:
            self.add_money(rest_money)

    def set_weight(self, tp, weight):
        self._slots[tp].set_weight(weight)

    def clear(self):
        for tp in self._slots.keys():
            self._slots[tp].clear()

    def refill(self):
        money = 0

        for tp in self._slots.keys():
            money += self._slots[tp].pop_all_money()

        self.add_money(money)

    def debug(self):
        out = []

        for tp in self._slots.keys():
            out.append("M[" + self._slots[tp].get_caption() + ",w" + str(self._slots[tp].get_weight()) + "]: " + str(self._slots[tp].get_money()))

        print(out)
