from lib.orderedset import OrderedSet


class AiGroupUniv:

    _univs = [
        ["s1_troop", "s1_troop_cannon"],
        ["s1_sniper"],
        ["s1_jeep"],
        ["s1_plate"],
        ["s1_lighttank", "s1_medtank"],
        ["s1_rocket"],

        ["s2_troop", "s2_troop_rocket", "s2_troop_beam"],
        ["s2_basic"],
        ["s2_mini", "s2_rock"],
        ["s2_plasma"],
        ["s2_partilery"]
    ]

    @staticmethod
    def get_univ_from_spec(key):
        dc = OrderedSet()

        for uv in AiGroupUniv._univs:
            if key in uv:
                [dc.add(c) for c in uv]
                return dc

        dc.add(key)
        return dc
