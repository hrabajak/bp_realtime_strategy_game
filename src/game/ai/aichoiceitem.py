from game.action.actionitemspec import ActionItemSpec


class AiChoiceItem:

    UTP_TROOP = 0
    UTP_SCOUT = 1
    UTP_VEHICLE = 2
    UTP_ARTILERY = 3

    _parent = None
    _cont = None
    _side = None
    _spec = None
    _ac_spec = None
    _utp = 0
    _def_weight = 1
    _weight = 1
    _max_count = 10
    _builded_count = 0
    _builds = None

    def __init__(self, parent, spec, utp, def_weight, max_count):
        self._parent = parent
        self._cont = parent.get_parent()
        self._side = parent.get_side()

        self._spec = spec
        self._ac_spec = ActionItemSpec.get(self._spec.get_key())
        self._utp = utp
        self._def_weight = def_weight
        self._weight = def_weight
        self._max_count = max_count
        self._builded_count = 0
        self._builds = []

    def get_spec(self):
        return self._spec

    def get_ac_spec(self):
        return self._ac_spec

    def get_utp(self):
        return self._utp

    def get_level(self):
        return self._ac_spec.get_level()

    def get_weight(self):
        return self._weight

    def get_created_count(self):
        return self._side.get_units_by_key_count(self._spec.get_key())

    def get_builded_count(self):
        return self._builded_count

    def get_max_count(self):
        return self._max_count

    def get_available_build(self):
        for bref in self._builds:
            if bref.get_may_apply_action(self._ac_spec) and self._side.get_actions().get_spec_stacked_count(self._ac_spec) == 0:
                return bref

        return None

    def get_is_full(self):
        return self._side.get_units_by_key_count(self._spec.get_key()) >= self._max_count

    def add_builded(self):
        self._builded_count += 1

    def try_map_build(self, bref):
        self._builds.append(bref)

    def clear(self):
        self._builds.clear()
