from game.ai.aicontainer import AiContainer
from game.helper.singleton import Singleton


class AiCollector(metaclass=Singleton):

    _childs = None

    def __init__(self):
        self._childs = []

    def add(self, side, flag):
        ref = AiContainer(side, flag)
        self._childs.append(ref)

    def get_have_ai(self, side):
        return next((ai for ai in self._childs if ai.get_side() is side), None)

    def get_childs(self):
        return self._childs
    
    def start(self):
        for ref in self._childs:
            ref.start()

    def clear(self):
        for ref in self._childs:
            ref.destroy()

        self._childs.clear()

    def update(self, tick):
        for ref in self._childs:
            ref.update(tick)
