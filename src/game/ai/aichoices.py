from game.ai.aichoiceitem import AiChoiceItem
from game.moveable.unitspec import UnitSpec


class AiChoices:

    _parent = None
    _side = None
    _items = None
    _item_scout = None
    _any_pending = False
    _created_count = 0
    _max_count = 0

    def __init__(self, parent, side):
        self._parent = parent
        self._side = side
        self._init()

    def _init(self):
        self._items = []
        self._item_scout = None

        if self._side.get_type() == "s1":
            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_troop"), AiChoiceItem.UTP_TROOP, 1, 20))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_troop_cannon"), AiChoiceItem.UTP_TROOP, 3, 10))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_sniper"), AiChoiceItem.UTP_TROOP, 5, 5))

            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_jeep"), AiChoiceItem.UTP_SCOUT, 2, 6))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_lighttank"), AiChoiceItem.UTP_VEHICLE, 3, 6))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_plate"), AiChoiceItem.UTP_VEHICLE, 3, 6))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_medtank"), AiChoiceItem.UTP_VEHICLE, 4, 6))

            self._items.append(AiChoiceItem(self, UnitSpec.get("s1_rocket"), AiChoiceItem.UTP_ARTILERY, 8, 3))

        elif self._side.get_type() == "s2":
            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_troop"), AiChoiceItem.UTP_TROOP, 1, 20))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_troop_rocket"), AiChoiceItem.UTP_TROOP, 3, 10))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_troop_beam"), AiChoiceItem.UTP_TROOP, 5, 5))

            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_basic"), AiChoiceItem.UTP_SCOUT, 2, 6))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_mini"), AiChoiceItem.UTP_VEHICLE, 3, 6))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_rock"), AiChoiceItem.UTP_VEHICLE, 3, 6))
            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_plasma"), AiChoiceItem.UTP_VEHICLE, 4, 6))

            self._items.append(AiChoiceItem(self, UnitSpec.get("s2_partilery"), AiChoiceItem.UTP_ARTILERY, 8, 3))

        self._item_scout = next((c for c in self._items if c.get_utp() == AiChoiceItem.UTP_SCOUT), None)

    def get_parent(self):
        return self._parent

    def get_side(self):
        return self._side

    def get_item_scout(self):
        return self._item_scout

    def get_any_pending(self):
        return self._any_pending

    def get_created_perc(self):
        return 0 if self._max_count == 0 else (self._created_count * 100) // self._max_count

    def update_builds(self):
        for it in self._items:
            it.clear()

            for bref in self._side.get_build_childs():
                if bref.get_is_alive() and bref.get_spec().get_key_base() in ["factory", "barracks"]:
                    it.try_map_build(bref)

    def get_item_to_create(self):
        self._any_pending = False
        self._created_count = 0
        self._max_count = 0

        min_cost = -1
        min_item = None
        min_build = None

        for it in self._items:
            bref = it.get_available_build()

            if bref is not None:
                self._max_count += it.get_max_count()
                self._created_count += it.get_created_count()

                if not it.get_is_full():
                    cost = it.get_weight() * it.get_builded_count()

                    if min_cost == -1 or cost < min_cost:
                        min_cost = cost
                        min_item = it
                        min_build = bref
                        self._any_pending = True

        return min_item, min_build
