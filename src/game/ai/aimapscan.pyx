import random

from libc.stdlib cimport malloc, free
from collections import deque, OrderedDict

from game.build.buildplaceanalyze import BuildPlaceAnalyze
from game.build.buildspec import BuildSpec
from game.const.specialflags import SpecialFlags
from game.graphic.primitivecollector import PrimitiveCollector
from game.interface.itilecollector import ITileCollector
from game.map.maptiletexture import MapTileTexture
from lib.orderedset import OrderedSet
from lib.heap import BinaryHeap

include "aimapscan.pxi"


cdef class AiMapScan:

    cdef:
        AiMapScanField * _a
        object _parent
        object _side
        object _tile_col

        object _stk
        long _use_flag
        int _phase
        long _t_idx
        long _last_area_id

        object _build_stk
        long _build_phase
        long _build_use_flag

        long _refresh_x
        long _refresh_y

        object _enemy_stk

        object _field_areas
        object _to_scan_tiles
        object _enemy_builds
        object _defend_tiles
        object _defend_tiles_proc
        object _defend_tiles_proc_sec
        object _affect_mvs
        object _affect_mvs_proc
        object _enemy_areas
        object _enemy_mvs_proc
        object _enemy_mvs

    def __cinit__(self, parent, side):
        cdef size_t a_ptr
        cdef long idx, midx

        self._parent = parent
        self._side = side
        self._tile_col = ITileCollector.get()

        self._stk = None
        self._use_flag = 0
        self._phase = 0

        self._build_stk = None
        self._build_use_flag = 0
        self._build_phase = 0

        self._refresh_x = 0
        self._refresh_y = 0

        self._enemy_stk = None

        self._field_areas = OrderedDict()
        self._to_scan_tiles = OrderedSet()
        self._enemy_builds = OrderedSet()
        self._defend_tiles = []
        self._defend_tiles_proc = None
        self._defend_tiles_proc_sec = None
        self._enemy_mvs = OrderedSet()
        self._enemy_mvs_proc = OrderedSet()
        self._affect_mvs = []
        self._affect_mvs_proc = OrderedSet()
        self._enemy_areas = OrderedDict()

        midx = self._tile_col.get_tiles_width() * self._tile_col.get_tiles_height()
        a_ptr = <size_t> malloc(midx * sizeof(AiMapScanField))

        self._a = <AiMapScanField *> a_ptr

        for idx in range(0, midx):
            self._a[idx].flag = 0
            self._a[idx].tp = 0
            self._a[idx].area_id = 0
            self._a[idx].scan = 0
            self._a[idx].build_flag = 0
            self._a[idx].build = 0
            self._a[idx].turret = 0
            self._a[idx].enemy_value = 0
            self._a[idx].enemy_tile = 0
            self._a[idx].enemy_tick = 0
            self._a[idx].refresh_flag = 0

    def __dealloc__(self):
        if self._a != NULL:
            free(self._a)
            self._a = NULL

    def get_a_ptr(self):
        return <size_t>self._a

    def get_fields_positions(self):
        return [(self._field_areas[c]['px'], self._field_areas[c]['py']) for c in self._field_areas.keys()]

    def get_to_scan_tiles(self):
        return self._to_scan_tiles

    def get_defend_tiles(self):
        return self._defend_tiles

    def get_enemy_builds(self):
        return self._enemy_builds

    def get_enemy_mvs(self):
        return self._enemy_mvs

    def get_enemy_build_dist(self, tile):
        cdef long number, dist, min_dist = -1
        cdef object bref

        for bref in self._enemy_builds:
            dist = tile.dist_to(bref.get_tile())

            if min_dist == -1 or dist < min_dist:
                min_dist = dist

        return min_dist

    def get_enemy_build_attack(self):
        out = OrderedDict()
        sides = []

        for bref in self._enemy_builds:
            if bref.get_side() not in out:
                out[bref.get_side()] = []
                sides.append(bref.get_side())

            out[bref.get_side()].append(bref)

        use_side = sides[random.randint(0, len(sides) - 1)]
        return out[use_side][random.randint(0, len(out[use_side]) - 1)]

    def get_enemy_build_list(self):
        cdef object out, side

        out = OrderedDict()

        for bref in self._enemy_builds:
            side = bref.get_side()

            if side not in out:
                out[side] = {
                    'px': 0,
                    'py': 0,
                    'cnt': 0
                }

            out[side]['px'] += bref.get_tile().get_pos_x()
            out[side]['py'] += bref.get_tile().get_pos_y()
            out[side]['cnt'] += 1

        for side in out.keys():
            out[side]['px'] = out[side]['px'] // out[side]['cnt']
            out[side]['py'] = out[side]['py'] // out[side]['cnt']

        return out

    def get_affect_mvs(self):
        return self._affect_mvs

    def get_enemy_areas(self):
        return self._enemy_areas

    def get_min_scan_tile(self):
        cdef long px, py, bpx, bpy
        cdef long t_width, t_height, score, min_score
        cdef object tile, min_tile

        t_width = int(self._tile_col.get_tiles_width())
        t_height = int(self._tile_col.get_tiles_height())
        
        bpx = self._parent.get_build_pos()[0]
        bpy = self._parent.get_build_pos()[1]
        min_score = 100000
        min_tile = None

        for py in range(0, t_height):
            for px in range(0, t_width):

                tile = self._tile_col.get_tile(px, py)

                if tile.is_accessible_simple() and self._a[py * t_width + px].enemy_value <= 100:
                    score = abs(py - bpy) + abs(px - bpx) + self._a[py * t_width + px].scan

                    if score < min_score:
                        min_score = score
                        min_tile = tile

        return min_tile

    def _update_fields(self):
        cdef long area_id

        for area_id in self._field_areas.keys():
            self._field_areas[area_id]['px'] = 0
            self._field_areas[area_id]['py'] = 0

            if len(self._field_areas[area_id]['tiles']) > 0:
                for tile in self._field_areas[area_id]['tiles']:
                    self._field_areas[area_id]['px'] += tile.get_pos_x()
                    self._field_areas[area_id]['py'] += tile.get_pos_y()

                self._field_areas[area_id]['px'] //= len(self._field_areas[area_id]['tiles'])
                self._field_areas[area_id]['py'] //= len(self._field_areas[area_id]['tiles'])

            #PrimitiveCollector().add_circle(self._field_areas[area_id]['px'] * 50, self._field_areas[area_id]['py'] * 50, 5)

    def scan_step(self, tick, steps):
        cdef object cur, sbc, c, tile, bref, mref
        cdef long idx, uev
        cdef int is_any
        cdef long max_scan = 1000
        cdef long max_enemy_value = 1000

        if self._phase == 0:
            # iniciacni faze

            self._phase = 1
            self._use_flag += 1
            self._t_idx = 0
            self._last_area_id = 0
            self._stk = deque()
            self._enemy_stk = deque()

            #PrimitiveCollector().clear()

        elif self._phase == 1:
            # stack budov

            builds = self._side.get_build_childs()

            while self._t_idx < len(builds) and steps > 0:
                steps -= 1

                if builds[self._t_idx].get_is_alive():
                    for tile in builds[self._t_idx].get_tiles():
                        self._a[tile.number].flag = self._use_flag
                        self._stk.append(tile)

                self._t_idx += 1

            if self._t_idx >= len(builds):
                self._phase = 2
                self._t_idx = 0

        elif self._phase == 2:
            # stack jednotek

            units = self._side.get_units()

            while self._t_idx < len(units) and steps > 0:
                steps -= 1

                if units[self._t_idx].get_is_alive():
                    tile = units[self._t_idx].get_tile()

                    if self._a[tile.number].flag != self._use_flag:
                        self._a[tile.number].flag = self._use_flag
                        self._stk.append(tile)

                self._t_idx += 1

            if self._t_idx >= len(units):
                self._phase = 3
                self._t_idx = 0

        elif self._phase == 3:
            # pruchod viditelne oblasti a skenovani objektu (nepratel, zdroju apod.)

            is_any = False

            while self._stk and steps > 0:
                cur = self._stk.popleft()
                self._a[cur.number].flag = self._use_flag
                self._a[cur.number].scan = max_scan
                steps -= 1

                if cur in self._to_scan_tiles:
                    self._to_scan_tiles.remove(cur)

                if cur.get_type() == MapTileTexture.T_FIELD:
                    if self._a[cur.number].area_id == 0:
                        self._last_area_id += 1
                        self._a[cur.number].area_id = self._last_area_id
                        self._field_areas[self._last_area_id] = {
                            'tiles': OrderedSet(),
                            'px': 0,
                            'py': 0
                        }

                elif self._a[cur.number].area_id != 0:
                    # field tile not here anymore

                    if cur in self._field_areas[self._a[cur.number].area_id]['tiles']:
                        self._field_areas[self._a[cur.number].area_id]['tiles'].remove(cur)

                    self._a[cur.number].area_id = 0

                if self._a[cur.number].area_id != 0 and cur not in self._field_areas[self._a[cur.number].area_id]['tiles']:
                    self._field_areas[self._a[cur.number].area_id]['tiles'].add(cur)
                    is_any = True
                    #PrimitiveCollector().add_circle(cur.get_x(), cur.get_y(), 5)

                if next((True for c in cur.get_moveables() if c.get_side() is not self._side), False):
                    self._enemy_stk.append(cur)

                elif cur.get_build_ref() is not None and cur.get_build_ref().get_side() is not self._side:
                    self._enemy_stk.append(cur)

                elif self._a[cur.number].enemy_value > 0:
                    self._a[cur.number].enemy_value = max(self._a[cur.number].enemy_value - 50, 0)

                for sbc in cur.get_neighbors():
                    if not sbc.get_accessible():
                        pass

                    elif sbc.get_fog_side(self._side):
                        # visible tile

                        if self._a[sbc.number].flag != self._use_flag:
                            self._a[sbc.number].flag = self._use_flag
                            self._a[sbc.number].scan = max_scan
                            self._stk.append(sbc)

                            if sbc in self._to_scan_tiles:
                                self._to_scan_tiles.remove(sbc)

                        if self._a[cur.number].area_id != 0 and sbc.get_type() == cur.get_type() and self._a[sbc.number].area_id != self._a[cur.number].area_id:
                            # expand field area
                            self._a[sbc.number].area_id = self._a[cur.number].area_id

            if is_any:
                self._update_fields()

            if not self._stk:
                self._phase = 4
                self._use_flag += 1

                for sbc in self._enemy_stk:
                    self._a[sbc.number].flag = self._use_flag
                    self._a[sbc.number].enemy_value = max_enemy_value
                    self._a[sbc.number].enemy_tile = sbc.number
                    self._a[sbc.number].enemy_tick = tick

        elif self._phase == 4:
            # dalsi iterace skenovani nepratel

            while self._enemy_stk and steps > 0:
                cur = self._enemy_stk.popleft()
                self._a[cur.number].flag = self._use_flag
                steps -= 1

                if self._a[cur.number].enemy_value > 0:
                    #PrimitiveCollector().add_circle(cur.get_x(), cur.get_y(), self._a[cur.number].enemy_value / 100)

                    for sbc in cur.get_neighbors():
                        if sbc.is_accessible_simple():
                            uev = self._a[cur.number].enemy_value - max_enemy_value // 20

                            if self._a[sbc.number].flag != self._use_flag:
                                self._a[sbc.number].enemy_value = uev
                                self._a[sbc.number].enemy_tile = self._a[cur.number].enemy_tile
                                self._a[sbc.number].enemy_tick = self._a[cur.number].enemy_tick - 2
                                self._a[sbc.number].flag = self._use_flag
                                self._enemy_stk.append(sbc)

                            elif uev > self._a[sbc.number].enemy_value:
                                self._a[sbc.number].enemy_value = uev
                                self._a[sbc.number].enemy_tile = self._a[cur.number].enemy_tile

                else:
                    for sbc in cur.get_neighbors():
                        if self._a[sbc.number].flag != self._use_flag and sbc.is_accessible_simple() and self._a[sbc.number].enemy_value > 0:
                            self._a[sbc.number].enemy_value = 0
                            self._a[sbc.number].flag = self._use_flag
                            self._enemy_stk.append(sbc)

            if not self._enemy_stk:
                self._phase = 0

    def build_step(self, steps):
        cdef object cur, sbc, c, tile, bref, mref, hs, stk, defend_tmp
        cdef long idx, cost
        cdef long long score, score_enemy, score_tmp
        cdef int is_any

        if self._build_phase == 0:
            # iniciacni faze skenovani okoli baze

            self._build_phase = 1
            self._build_use_flag += 1
            self._build_stk = deque()

            #PrimitiveCollector().clear()

            for bref in self._side.get_build_childs():
                if not bref.get_spec().get_is_turret():
                    for tile in bref.get_tiles():
                        self._a[tile.number].build_flag = self._build_use_flag
                        self._a[tile.number].build = 1
                        tile.b_back_steps = 0

                        self._build_stk.append(tile)

        elif self._build_phase == 1:
            # pruchod okoli baze a skenovani detailu

            while self._build_stk and steps > 0:
                cur = self._build_stk.popleft()
                self._a[cur.number].build_flag = self._build_use_flag
                steps -= 1

                if self._a[cur.number].build == 1 and cur.get_build_ref() is None:
                    self._a[cur.number].build = 0

                for c in cur.get_neighbors_with_cost():
                    cost = c[0]
                    sbc = c[1]

                    if self._a[sbc.number].build_flag != self._build_use_flag and (cur.b_back_steps + cost <= 160 or self._a[sbc.number].build > 0):
                        self._a[sbc.number].build_flag = self._build_use_flag
                        sbc.b_back_steps = cur.b_back_steps + cost
                        self._build_stk.append(sbc)

                        if self._a[cur.number].build > 0 and sbc.b_back_steps <= 140 and sbc.get_build_ref() is None:
                            self._a[sbc.number].build = self._a[cur.number].build + 1
                        else:
                            self._a[sbc.number].build = 0

            if not self._build_stk:
                self._build_phase = 2
                self._build_use_flag += 1
                self._build_stk = deque()

                for bref in self._side.get_build_childs():
                    if bref.get_spec().get_is_turret():
                        for tile in bref.get_tiles():
                            self._a[tile.number].build_flag = self._build_use_flag
                            self._a[tile.number].turret = 1
                            tile.b_back_steps = 0

                            self._build_stk.append(tile)

        elif self._build_phase == 2:
            # pruchod okoli vezi baze

            while self._build_stk and steps > 0:
                cur = self._build_stk.popleft()
                self._a[cur.number].build_flag = self._build_use_flag
                steps -= 1

                if self._a[cur.number].turret == 1 and cur.get_build_ref() is None:
                    self._a[cur.number].turret = 0

                for c in cur.get_neighbors_with_cost():
                    cost = c[0]
                    sbc = c[1]

                    if self._a[sbc.number].build_flag != self._build_use_flag and (cur.b_back_steps + cost <= 120 or self._a[sbc.number].turret > 0):
                        self._a[sbc.number].build_flag = self._build_use_flag
                        sbc.b_back_steps = cur.b_back_steps + cost
                        self._build_stk.append(sbc)

                        if self._a[cur.number].turret > 0 and sbc.b_back_steps <= 100 and sbc.get_build_ref() is None:
                            self._a[sbc.number].turret = self._a[cur.number].turret + 1
                        else:
                            self._a[sbc.number].turret = 0

            if not self._build_stk:
                self._build_phase = 3
                self._build_use_flag += 1
                self._build_stk = deque()
                self._defend_tiles_proc = deque()

                for bref in self._side.get_build_childs():
                    for tile in bref.get_tiles():
                        self._a[tile.number].build_flag = self._build_use_flag
                        self._build_stk.append(tile)

        elif self._build_phase == 3:
            # nalez policek pro obranu

            while self._build_stk and steps > 0:
                cur = self._build_stk.popleft()
                self._a[cur.number].build_flag = self._build_use_flag
                steps -= 1
                
                if self._a[cur.number].build > 5 and self._a[cur.number].build <= 7 and cur.is_accessible_simple() and cur.get_type() == MapTileTexture.T_GRASS:
                    self._defend_tiles_proc.append(cur)

                for sbc in cur.get_neighbors():
                    if self._a[sbc.number].build_flag != self._build_use_flag and self._a[sbc.number].build > 0:
                        self._a[sbc.number].build_flag = self._build_use_flag
                        self._build_stk.append(sbc)

            if not self._build_stk:
                self._build_phase = 4
                self._defend_tiles_proc_sec = BinaryHeap()

        elif self._build_phase == 4:
            # analyza okoli policek pro obranu

            stk = deque()

            while self._defend_tiles_proc and steps > 0:
                use_flag = self._tile_col.get_find_flag(1)

                tile = self._defend_tiles_proc.popleft()
                tile.flag = use_flag
                tile.b_back_steps = 0

                score = 0
                score_enemy = 0

                stk.clear()
                stk.append(tile)

                while stk:
                    cur = stk.popleft()
                    cur.flag = use_flag
                    steps -= 1

                    score += 1
                    score_enemy = max(score_enemy, self._a[cur.number].enemy_tick)

                    for sbc in cur.get_neighbors():
                        if sbc.flag != use_flag and cur.is_accessible_simple() and cur.b_back_steps + 1 <= 4:
                            sbc.b_back_steps = cur.b_back_steps + 1
                            sbc.flag = use_flag
                            stk.append(sbc)

                self._defend_tiles_proc_sec.push(10000 - score, (10000 - score, score_enemy, tile))

                self._tile_col.move_find_flag(1)

            if not self._defend_tiles_proc:

                #for tl in self._defend_tiles:
                    #tl.snap_color = None

                self._build_phase = 0
                self._defend_tiles = []

                defend_tmp = BinaryHeap()
                hs = set()

                while self._defend_tiles_proc_sec and len(defend_tmp) < 25:
                    c = self._defend_tiles_proc_sec.pop_ref()
                    idx = (c[2].get_pos_y() // 2) * 10000 + c[2].get_pos_x() // 2

                    if idx not in hs:
                        defend_tmp.push(c[1] * -1, c[2])
                        hs.add(idx)

                while defend_tmp:
                    self._defend_tiles.append(defend_tmp.pop_ref())

                #for tl in self._defend_tiles:
                    #tl.snap_color = (255, 0, 0)

    def enemy_scan(self):
        cdef object stk, cur, sbc, mref, tile, mvs, dc_idx
        cdef long use_flag, idx

        dc_idx = {}
        mvs = OrderedSet()
        use_flag = 0

        if len(self._affect_mvs) > 0:
            # affect_mvs - jednotky na nepratelsky oznacenem uzemi

            use_flag = self._tile_col.get_find_flag()

            stk = deque()
            tls = self._tile_col.get_tiles()
            idx = 1

            for mref in self._affect_mvs:
                if mref.get_is_alive():
                    tile = mref.get_tile()

                    if self._a[tile.number].enemy_value > 0:
                        cur = tls[self._a[tile.number].enemy_tile]
                        cur.flag = use_flag
                        cur.df_back_steps = 0
                        cur.df_index = idx
                        stk.append(cur)

                        dc_idx[idx] = set()
                        dc_idx[idx].add(idx)
                        idx += 1

            while stk:
                cur = stk.popleft()
                cur.flag = use_flag

                for mref in cur.get_moveables():
                    if mref.get_side() is not self._side and mref not in mvs:
                        mvs.add(mref)

                for sbc in cur.get_neighbors():
                    if sbc.is_accessible_simple() and cur.df_back_steps + 1 <= 5 and self._a[sbc.number].enemy_value > 0:
                        if sbc.flag != use_flag:
                            sbc.flag = use_flag
                            sbc.df_back_steps = cur.df_back_steps + 1
                            sbc.df_index = cur.df_index
                            stk.append(sbc)

                        elif sbc.df_index != cur.df_index and sbc.df_index in dc_idx:
                            dc_idx[sbc.df_index].add(cur.df_index)
                            dc_idx[cur.df_index].add(sbc.df_index)

            self._tile_col.move_find_flag()

        #for idx in self._enemy_areas.keys():
            #for tile in self._enemy_areas[idx]['tls']:
                #tile.snap_color = None

        self._enemy_areas.clear()

        for mref in mvs:
            tile = mref.get_tile()

            if tile.flag == use_flag and tile.df_index in dc_idx:
                idx = min(dc_idx[tile.df_index])

                if idx not in self._enemy_areas:
                    self._enemy_areas[idx] = {
                        'px': 0,
                        'py': 0,
                        'inbase': 0,
                        'is_inbase': False,
                        'mass': 0,
                        'mvs': []
                        #'tls': []
                    }

                self._enemy_areas[idx]['px'] += tile.get_pos_x()
                self._enemy_areas[idx]['py'] += tile.get_pos_y()
                self._enemy_areas[idx]['mass'] += mref.get_spec().get_mass_third()
                self._enemy_areas[idx]['mvs'].append(mref)

                #tile = mref.get_tile()
                #if self._side.get_type() == 's1':
                    #tile.snap_color = (0, 0, 255)
                #else:
                    #tile.snap_color = (255, 0, 0)
                #self._enemy_areas[idx]['tls'].append(tile)

                if self._a[tile.number].build > 0:
                    self._enemy_areas[idx]['inbase'] += 1

        for idx in self._enemy_areas.keys():
            self._enemy_areas[idx]['px'] //= len(self._enemy_areas[idx]['mvs'])
            self._enemy_areas[idx]['py'] //= len(self._enemy_areas[idx]['mvs'])
            self._enemy_areas[idx]['is_inbase'] = self._enemy_areas[idx]['inbase'] > 0
            # !! self._enemy_areas[idx]['is_inbase'] = ((self._enemy_areas[idx]['inbase'] * 100) // len(self._enemy_areas[idx]['mvs'])) >= 50

    def travel_place_scan(self, bref):
        cdef object places, tile, stk, cur, sbc, max_place
        cdef long use_flag, score, max_score

        max_score = -1
        max_place = None

        places = bref.get_places_special(SpecialFlags.SPECIAL_CREATE_FINAL)

        if places is not None:
            use_flag = self._tile_col.get_find_flag()

            tile = places[0][0].get_tile()
            tile.flag = use_flag
            tile.back_steps = 0

            stk = deque()
            stk.append(tile)

            while stk:
                cur = stk.popleft()
                cur.flag = use_flag

                if self._a[cur.number].build > 0:
                    score = self._a[cur.number].build

                    if cur.get_type() == MapTileTexture.T_GRASS:
                        score += 20

                    for mref in cur.get_moveables():
                        score -= mref.get_spec().get_mass_third()

                    if max_score == -1 or score > max_score:
                        max_score = score
                        max_place = cur.get_place()

                for sbc in cur.get_neighbors():
                    if sbc.flag != use_flag and sbc.is_accessible_simple() and cur.back_steps + 1 <= 4:
                        sbc.flag = use_flag
                        sbc.back_steps = cur.back_steps + 1
                        stk.append(sbc)

            self._tile_col.move_find_flag()

        return max_place

    def refresh_step(self, steps):
        cdef long midx = self._tile_col.get_tiles_width() * self._tile_col.get_tiles_height()
        cdef long t_width = self._tile_col.get_tiles_width()
        cdef long t_height = self._tile_col.get_tiles_height()
        cdef long idx, number
        cdef object tile, sbc, c

        while steps > 0:
            idx = self._refresh_y * t_width + self._refresh_x

            if self._a[idx].scan > 0:
                tile = self._tile_col.get_tile(self._refresh_x, self._refresh_y)
                self._a[idx].scan -= 1

            if self._a[idx].enemy_value > 0:
                tile = self._tile_col.get_tile(self._refresh_x, self._refresh_y)

            if self._a[idx].enemy_value > 100:
                tile = self._tile_col.get_tile(self._refresh_x, self._refresh_y)
                bref = tile.get_build_ref()

                if bref is not None and bref.get_side() is not self._side and bref not in self._enemy_builds:
                    self._enemy_builds.add(bref)

            if self._a[idx].enemy_value > 0:
                for mref in self._tile_col.get_tile(self._refresh_x, self._refresh_y).get_moveables():
                    if mref.get_side() is self._side and mref not in self._affect_mvs_proc:
                        self._affect_mvs_proc.add(mref)

            if self._a[idx].build > 0 and self._a[idx].enemy_value > 0:
                for mref in self._tile_col.get_tile(self._refresh_x, self._refresh_y).get_moveables():
                    if mref.get_side() is not self._side:
                        self._enemy_mvs_proc.add(mref)

            #if self._a[idx].turret > 0:
                #tile = self._tile_col.get_tile(self._refresh_x, self._refresh_y)
                #PrimitiveCollector().update_circle(tile.get_x(), tile.get_y(), 5)

            self._refresh_x += 1

            if self._refresh_x >= t_width:
                self._refresh_x = 0
                self._refresh_y += 1

                if self._refresh_y >= t_height:
                    self._refresh_y = 0

                    # scanned enemy bases

                    for bref in [c for c in self._enemy_builds]:
                        if not bref.get_is_alive():
                            self._enemy_builds.remove(bref)

                    # scanned defend moveables

                    self._affect_mvs.clear()
                    [self._affect_mvs.append(c) for c in self._affect_mvs_proc]
                    self._affect_mvs_proc = OrderedSet()

                    # enemy on base

                    self._enemy_mvs = self._enemy_mvs_proc
                    self._enemy_mvs_proc = OrderedSet()

            steps -= 1
