

class AiMoneySlot:

    SLOT_RESOURCES = 0
    SLOT_BUILD = 1
    SLOT_DEFENSE = 2
    SLOT_UPGRADE = 3
    SLOT_UNITS = 4

    _tp = 0
    _money = 0
    _weight = 0
    _max_money = 0
    _tp_dict = {
        SLOT_RESOURCES: "resources",
        SLOT_BUILD: "build",
        SLOT_DEFENSE: "defense",
        SLOT_UPGRADE: "upgrade",
        SLOT_UNITS: "units"
    }

    def __init__(self, tp, weight, max_money=0):
        self._tp = tp
        self._money = 0
        self._weight = weight
        self._max_money = max_money

    def get_tp(self):
        return self._tp

    def get_money(self):
        return self._money

    def get_weight(self):
        return self._weight

    def get_max_money(self):
        return self._max_money

    def get_caption(self):
        return AiMoneySlot._tp_dict[self._tp]

    def set_money(self, money):
        self._money = money

    def set_weight(self, weight):
        if self._weight != weight:
            self._weight = weight
            return True
        else:
            return False

    def add_money(self, money):
        self._money += money

    def pop_all_money(self):
        out = self._money
        self._money = 0
        return out

    def clear(self):
        self._money = 0
