

cdef struct AiMapScanField:
    long flag
    long tp
    long area_id
    long scan
    long build_flag
    long build
    long turret
    long enemy_value
    long enemy_tile
    long long enemy_tick
    long refresh_flag
