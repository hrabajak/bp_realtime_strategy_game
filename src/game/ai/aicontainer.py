import random
from collections import deque, OrderedDict

from game.action.actionitemspec import ActionItemSpec
from game.ai.aichoices import AiChoices
from game.ai.aigroup import AiGroup
from game.ai.aimapscan import AiMapScan
from game.ai.aimoney import AiMoney
from game.ai.aimoneyslot import AiMoneySlot
from game.build.buildplace import BuildPlace
from game.build.buildplaceanalyze import BuildPlaceAnalyze
from game.const.specialflags import SpecialFlags
from game.graphic.primitivecollector import PrimitiveCollector
from game.interface.iai import IAi
from game.interface.iplayersides import IPlayerSides
from game.interface.itilecollector import ITileCollector
from game.map.mapscanner import MapScanner
from lib.orderedset import OrderedSet


class AiContainer(IAi):

    AIGROUP_NONE = 0
    AIGROUP_SCOUT = 1
    AIGROUP_DEFEND = 2
    AIGROUP_DEFEND_ATTACK = 3
    AIGROUP_ATTACK = 4

    _tile_col: ITileCollector = None
    _psides: IPlayerSides = None
    _map_scr: MapScanner = None
    _scan = None
    _money = None

    _side = None
    _flag = None
    _actions = None
    _alive = False
    _start_tick = 0
    _unit_groups = None
    _unit_stats = None
    _unit_assigned = None
    _group_scout = None
    _groups_defend = None
    _groups_defend_attack = None
    _groups_attack = None
    _choices = None
    _build_flag = False

    _mains = None
    _refineries = None
    _miners = None
    _barracks = None
    _factories = None
    _laboratories = None
    _turrets = None
    _build_pos = (0, 0)
    _attack_to = None
    _attack_tick = 0
    _attack_tick_max = 0

    _max_refineries = 3
    _max_miners = 4
    _retreat_mass = 10

    allow_build = True
    allow_scout = True
    allow_upgrades = True
    allow_turrets = True
    allow_units = True
    allow_attack = True
    allow_cost_effective = True
    allow_retreat = True

    number = 0
    _number_total = 0

    def __init__(self, side, flag):
        IAi.__init__(self)

        self.number = AiContainer._number_total
        AiContainer._number_total += 1

        self._tile_col = ITileCollector.get()
        self._psides = IPlayerSides.get()
        self._map_scr = MapScanner(self._tile_col)
        self._scan = None
        self._money = AiMoney()
        self._money.set_weights(False, AiMoneySlot.SLOT_RESOURCES, 1)

        self._side = side
        self._flag = flag
        self._actions = side.get_actions()
        self._alive = False
        self._unit_groups = []
        self._unit_stats = {}
        self._unit_assigned = []
        self._group_scout = None
        self._groups_defend = []
        self._groups_defend_attack = []
        self._groups_attack = []
        self._choices = AiChoices(self, self._side)
        self._build_flag = False
        self._attack_tick = 0
        self._attack_tick_max = 20

        if self._flag == 'ai_easy':
            self._max_refineries = 1
            self._max_miners = 1

        elif self._flag == 'ai_medium':
            self._max_refineries = 2
            self._max_miners = 3

        else:
            self._max_refineries = 3
            self._max_miners = 5

        self._refineries = []
        self._barracks = []
        self._factories = []
        self._laboratories = []
        self._turrets = [[], []]

        self._side.set_event_money(self._money_update)
        self._side.set_event_action_avail(self._action_avail)
        self._side.set_event_create_build(self._create_build)
        self._side.set_event_create_unit(self._create_unit)
        self._side.set_event_upgrade(self._upgrade)
        self._side.set_event_destroy_build(self._destroy_build)
        self._side.set_event_destroy_unit(self._destroy_unit)
        self._side.set_event_finish_build(self._finish_build)
        self._side.set_event_finish_unit(self._finish_unit)

    def get_side(self):
        return self._side

    def get_build_pos(self):
        return self._build_pos

    def _add_group(self, allow_any_child=False, max_mass=20, caption="", tp=AIGROUP_NONE):
        group_ref = AiGroup(self, self._scan, self._side, allow_any_child=allow_any_child, max_mass=max_mass, caption=caption, tp=tp)
        self._unit_groups.append(group_ref)
        return group_ref

    def _rem_group(self, group_ref):
        self._unit_groups.remove(group_ref)
        return group_ref

    def _get_defend_mass(self):
        mass = 0

        for group_ref in self._groups_defend:
            mass += group_ref.get_mass()

        for group_ref in self._groups_defend_attack:
            mass += group_ref.get_mass()

        return mass

    def _build_pos_update(self):
        pos_x = 0
        pos_y = 0

        for bref in self._side.get_build_childs():
            pos_x += bref.get_tile().get_pos_x()
            pos_y += bref.get_tile().get_pos_y()

        if len(self._side.get_build_childs()) == 0:
            self._build_pos = (0, 0)
        else:
            self._build_pos = (pos_x // len(self._side.get_build_childs()), pos_y // len(self._side.get_build_childs()))

    def _money_update(self, money, action_spec=None, money_slot=-1):
        if money < 0 and money_slot != -1:
            self._money.dec_slot(money_slot, abs(money))
            # !! self._money.debug()

        elif money_slot != -1:
            self._money.inc_slot(money_slot, money)
            # !! self._money.debug()

        else:
            self._money.add_money(money)
            # !! self._money.debug()

    def _action_avail(self, action_item):
        pass

    def _realize_action_avail(self, action_item):
        if action_item.get_spec().get_key_base() in ["refinery"]:

            pos = self._scan.get_fields_positions()

            if len(pos) > 0:
                an = BuildPlaceAnalyze(self._side, action_item.get_spec().get_build_spec())
                an.analyze()

                ptile = an.get_buildable_tile_nearest(pos)

                if ptile is not None:
                    self._side.action_build_realize(action_item, BuildPlace(self._side, action_item.get_spec().get_build_spec()).set_tile(ptile))

        elif action_item.get_spec().get_key_base() in ["barracks", "factory", "lab"]:

            an = BuildPlaceAnalyze(self._side, action_item.get_spec().get_build_spec())
            an.analyze()

            ptile = an.get_buildable_tile()

            if ptile is not None:
                self._side.action_build_realize(action_item, BuildPlace(self._side, action_item.get_spec().get_build_spec()).set_tile(ptile))

        elif action_item.get_spec().get_is_turret():

            an = BuildPlaceAnalyze(self._side, action_item.get_spec().get_build_spec())
            bp = BuildPlace(self._side, action_item.get_spec().get_build_spec())

            min_dist = -1
            min_tile = None

            for tile in an.analyze_simple_get(4):
                bp.set_tile(tile)

                if bp.get_is_buildable():
                    dist = self._scan.get_enemy_build_dist(tile)

                    if min_dist == -1 or dist < min_dist:
                        min_dist = dist
                        min_tile = tile

            if min_tile is not None:
                bp.set_tile(min_tile)
                self._side.action_build_realize(action_item, bp)

        self._build_pos_update()

    def _create_build(self, action_item, bref):
        if self._alive:
            self._build_flag = True

    def _create_unit(self, action_item, mref):
        if self._alive:
            pass

    def _finish_unit(self, mref):
        if self._alive:
            self._unit_assigned.append(mref)

    def _upgrade(self, action_item, bref):
        if self._alive:
            pass

    def _destroy_build(self, bref):
        if self._alive:
            self._build_flag = True

    def _finish_build(self, bref):
        if self._alive:
            self._build_flag = True

            travel_place = self._scan.travel_place_scan(bref)

            if travel_place is not None:
                self._side.set_selected([bref])
                self._side.selected_place_travel(travel_place)

    def _destroy_unit(self, mref):
        if self._alive:
            pass

    def start(self):
        self._alive = True
        self._start_tick = 100
        self._scan = AiMapScan(self, self._side)
        self._build_pos_update()
        self._group_scout = self._add_group(allow_any_child=False, max_mass=20, caption="scout", tp=AiContainer.AIGROUP_SCOUT)

        [self._unit_assigned.append(mref) for mref in self._side.get_units()]
        self._update_unit_assign()
        self._money.clear()
        self._money.add_money(self._side.get_money())

    def destroy(self):
        self._side.event_clear()

    def update(self, tick):
        if not self._alive:
            return False

        elif self._start_tick > 0:
            self._start_tick -= 1
            return False

        if tick % 50 == 0:

            self._mains = self._side.get_builds_by_key(self._side.get_type() + "_main")
            self._refineries = self._side.get_builds_by_key(self._side.get_type() + "_refinery")
            self._miners = self._side.get_units_by_key(self._side.get_type() + "_miner")
            self._barracks = self._side.get_builds_by_key(self._side.get_type() + "_barracks")
            self._factories = self._side.get_builds_by_key(self._side.get_type() + "_factory")
            self._laboratories = self._side.get_builds_by_key(self._side.get_type() + "_lab")

            self._turrets = [
                self._side.get_builds_by_key(self._side.get_type() + "_" + self._side.get_turret_spec_key(0)),
                self._side.get_builds_by_key(self._side.get_type() + "_" + self._side.get_turret_spec_key(1))
            ]

            if self._build_flag:
                self._build_flag = False
                self._choices.update_builds()
                self._choices.get_item_to_create()

            if len(self._mains) == 0 or not self.allow_build:
                pass

            else:
                wgs = []
                refill_flag = False
                use_defense = None
                upgrade_bref = None
                upgrade_level = 0
                is_enemy_critical = len(self._scan.get_enemy_mvs()) > 2 and len(self._scan.get_enemy_mvs()) > self._get_defend_mass() # vetsi masa nepratelskych jednotek

                if len(self._refineries) == 0:
                    wgs += [AiMoneySlot.SLOT_RESOURCES, 10]
                    refill_flag = True

                elif is_enemy_critical:
                    # primaryne stavet jednotky

                    if len(self._miners) == 0 and len(self._factories) > 0:
                        # zadni mineri
                        wgs += [AiMoneySlot.SLOT_RESOURCES, 60]
                        refill_flag = True

                    elif len(self._factories) > 0 or len(self._barracks) > 0:
                        # primarne stavet jednotky
                        wgs += [AiMoneySlot.SLOT_UNITS, 60]
                        refill_flag = True

                    else:
                        wgs += [AiMoneySlot.SLOT_BUILD, 10]
                        refill_flag = True

                else:
                    if len(self._barracks) == 0:
                        wgs += [AiMoneySlot.SLOT_BUILD, 10]
                        refill_flag = True

                    else:
                        if len(self._miners) == 0 and len(self._factories) > 0:
                            # zadni mineri
                            wgs += [AiMoneySlot.SLOT_RESOURCES, 60]
                            refill_flag = True

                        elif len(self._miners) < len(self._refineries) and len(self._factories) > 0:
                            # min mineru nez refinerii
                            wgs += [AiMoneySlot.SLOT_RESOURCES, 30]
                            refill_flag = True

                        elif len(self._refineries) == 1:
                            # pouze jedna refinerie
                            wgs += [AiMoneySlot.SLOT_RESOURCES, 30]

                        elif len(self._refineries) < self._max_refineries:
                            # stale je mozne stavet refinerie

                            if len(self._refineries) >= 2:
                                wgs += [AiMoneySlot.SLOT_RESOURCES, 2]
                            else:
                                wgs += [AiMoneySlot.SLOT_RESOURCES, 10]

                        elif len(self._miners) < self._max_miners and len(self._factories) > 0:
                            # postavit vice mineru
                            wgs += [AiMoneySlot.SLOT_RESOURCES, 10]

                        if len(self._factories) == 0 or len(self._laboratories) == 0:
                            wgs += [AiMoneySlot.SLOT_BUILD, 10]

                    if self.allow_units and self._choices.get_any_pending() and (len(self._barracks) > 0 or len(self._factories) > 0):
                        wgs += [AiMoneySlot.SLOT_UNITS, 10]

                if not is_enemy_critical:
                    if self.allow_upgrades and len(self._laboratories) > 0 and len(self._refineries) >= self._max_refineries:
                        upgrade_bref, upgrade_level = self._get_upgrade_build()

                        if upgrade_bref is not None and upgrade_level == 0:
                            wgs += [AiMoneySlot.SLOT_UPGRADE, 10]
                        elif upgrade_bref is not None:
                            wgs += [AiMoneySlot.SLOT_UPGRADE, 2]

                    if self.allow_turrets and len(self._refineries) >= self._max_refineries: # and len(self._scan.get_enemy_builds()) > 0: # !! and False
                        if len(self._turrets[0]) < 3:
                            if self._choices.get_created_perc() >= 50:
                                wgs += [AiMoneySlot.SLOT_DEFENSE, 10 - len(self._turrets[0]) * 3]
                                use_defense = self._side.get_turret_spec_key(0)

                        elif len(self._turrets[1]) < 2:
                            if self.is_action_available(self._side.get_turret_spec_key(1), self._mains[0]):
                                wgs += [AiMoneySlot.SLOT_DEFENSE, 10 - len(self._turrets[1]) * 5]
                                use_defense = self._side.get_turret_spec_key(1)

                if len(wgs) == 0 and self.allow_units:
                    wgs += [AiMoneySlot.SLOT_UNITS, 10]
                    refill_flag = True

                self._money.set_weights(refill_flag, *wgs)
                # !! self._money.debug()

                if len(self._refineries) < self._max_refineries:
                    if self.may_stack_action("refinery", self._mains[0], AiMoneySlot.SLOT_RESOURCES):
                        self._side.action_enqueue_key("refinery", self._mains[0], money_slot=AiMoneySlot.SLOT_RESOURCES)

                elif len(self._miners) < self._max_miners and len(self._factories) > 0:
                    if self.may_stack_action("miner", self._factories[0], AiMoneySlot.SLOT_RESOURCES):
                        self._side.action_enqueue_key("miner", self._factories[0], money_slot=AiMoneySlot.SLOT_RESOURCES)

                if len(self._barracks) == 0:
                    if self.may_stack_action("barracks", self._mains[0], AiMoneySlot.SLOT_BUILD):
                        self._side.action_enqueue_key("barracks", self._mains[0], money_slot=AiMoneySlot.SLOT_BUILD)

                elif len(self._factories) == 0:
                    if self.may_stack_action("factory", self._mains[0], AiMoneySlot.SLOT_BUILD):
                        self._side.action_enqueue_key("factory", self._mains[0], money_slot=AiMoneySlot.SLOT_BUILD)

                elif len(self._laboratories) == 0:
                    if self.may_stack_action("lab", self._mains[0], AiMoneySlot.SLOT_BUILD):
                        self._side.action_enqueue_key("lab", self._mains[0], money_slot=AiMoneySlot.SLOT_BUILD)

                if use_defense is not None:
                    if self.may_stack_action(use_defense, self._mains[0], AiMoneySlot.SLOT_DEFENSE):
                        self._side.action_enqueue_key(use_defense, self._mains[0], money_slot=AiMoneySlot.SLOT_DEFENSE)

                if upgrade_bref is not None and self.may_stack_action(upgrade_bref.get_upgrade_spec(), self._laboratories[0], AiMoneySlot.SLOT_UPGRADE, True):
                    self._side.action_enqueue(upgrade_bref.get_upgrade_spec(), self._laboratories[0], build_trg_ref=upgrade_bref, money_slot=AiMoneySlot.SLOT_UPGRADE)

                action_item = self._actions.get_avail_action_item()

                if action_item is not None:
                    self._realize_action_avail(action_item)

        if (tick + 25) % 50 == 0:
            self._update_unit_assign()
            self._unit_actions()
            self._make_units()

        if tick % 100 == 0:
            self._unit_actions_defend()

        if tick % 5 == 0:
            self._scan.scan_step(tick, 100)

        if (tick + 2) % 50 == 0:
            self._scan.enemy_scan()
            self._react_to_enemy()
            self._perform_attack()

        if tick % 20 == 0:
            self._scan.build_step(100)

        self._scan.refresh_step(200)

    def _update_unit_assign(self):
        if len(self._unit_assigned) > 0:
            self._group_scout.update()

            if self._group_scout.get_is_empty():
                for mref in [c for c in self._unit_assigned if c.get_spec().get_is_scout() and c.get_is_alive()]:
                    self._group_scout.add_child(mref)
                    self._unit_assigned.remove(mref)
                    break

            is_any = False

            for mref in [c for c in self._unit_assigned if c.get_is_alive()]:
                if mref.get_spec().get_attack_able():
                    self._add_unit_to_defend(mref)
                    self._unit_assigned.remove(mref)
                    is_any = True

                else:
                    self._unit_assigned.remove(mref)

    def _add_unit_to_defend(self, mref):
        use_group_ref = None

        for group_ref in self._groups_defend:
            if group_ref.get_may_add_child(mref):
                use_group_ref = group_ref
                break

        if use_group_ref is None:
            use_group_ref = self._add_group(allow_any_child=False, max_mass=20, caption="defend", tp=AiContainer.AIGROUP_DEFEND)
            self._groups_defend.append(use_group_ref)

        use_group_ref.add_child(mref)
        return use_group_ref

    def _add_unit_to_attack(self, mref):
        use_group_ref = None

        for group_ref in self._groups_attack:
            if group_ref.get_may_add_child(mref):
                use_group_ref = group_ref
                break

        if use_group_ref is None:
            use_group_ref = self._add_group(allow_any_child=True, max_mass=26, caption="attack", tp=AiContainer.AIGROUP_ATTACK)
            self._groups_attack.append(use_group_ref)

        use_group_ref.add_child(mref)
        return use_group_ref

    def _unit_actions(self):
        [group_ref.update() for group_ref in self._unit_groups]

        if self.allow_scout:
            if not self._group_scout.get_is_empty():
                if self._group_scout.get_is_moving():
                    self._group_scout.check_scout()
                else:
                    self._group_scout.scan_scout()

    def _unit_actions_defend(self):
        tiles = self._scan.get_defend_tiles().copy()
        idx = 0

        for group_ref in [c for c in self._groups_defend]:
            if group_ref.get_is_empty():
                self._groups_defend.remove(self._rem_group(group_ref))

            elif len(tiles) > 0:
                # !! tiles.sort(key=lambda tile: abs(tile.get_pos_x() - group_ref.get_px()) + abs(tile.get_pos_y() - group_ref.get_py()))
                group_ref.goto(tiles.pop(0))
                idx += 1

    def _make_units(self):
        if self.allow_units:
            it, bref = self._choices.get_item_to_create()

            if it is not None and self.may_stack_action(it.get_ac_spec(), bref, AiMoneySlot.SLOT_UNITS, True):
                it.add_builded()
                self._side.action_enqueue(it.get_ac_spec(), bref, money_slot=AiMoneySlot.SLOT_UNITS)

    def _get_upgrade_build(self):
        min_level = -1
        min_res_level = 0
        min_bref = None

        for c in [(0, self._barracks), (0, self._factories), (2, self._mains)]:
            for bref in c[1]:
                if bref.get_may_upgrade():
                    lv = bref.get_upgrade_level() + c[0]

                    if min_level == -1 or lv < min_level:
                        min_level = lv
                        min_res_level = bref.get_upgrade_level()
                        min_bref = bref

        return min_bref, min_res_level

    def _react_to_enemy(self):
        def_flag = False
        group_ref = None

        mvs_aff = self._scan.get_affect_mvs()
        areas = self._scan.get_enemy_areas()

        for group_ref in self._groups_defend_attack:
            for mref in group_ref.get_childs():
                mref.flag_ai = False

        area_react = OrderedDict()

        for idx in areas.keys():
            mvs_aff.sort(key=lambda mref: mref.get_tile().dist_to_coord_pos_manhattan(areas[idx]['px'], areas[idx]['py']) if mref.get_is_alive() else 10000, reverse=False)
            mvs_use = []
            mvs_mass = 0

            group_ref = None
            groups = OrderedSet()

            for mref in mvs_aff:
                if mref.get_is_alive():
                    if mref.group_ai is None or mref.group_ai.get_tp() == AiContainer.AIGROUP_SCOUT:
                        pass

                    else:
                        is_attack = mref.group_ai.get_tp() == AiContainer.AIGROUP_ATTACK
                        is_defend_attack = mref.group_ai.get_tp() == AiContainer.AIGROUP_DEFEND_ATTACK
                        is_defend = mref.group_ai.get_tp() == AiContainer.AIGROUP_DEFEND

                        if is_attack:
                            pass

                        elif (is_defend and areas[idx]['is_inbase']) or is_defend_attack:
                            if mvs_mass < areas[idx]['mass'] * 2 or is_defend_attack:
                                mvs_mass += mref.get_spec().get_mass_third()
                                mvs_use.append(mref)
                                mref.flag_ai = True

                                if not is_defend_attack:
                                    if group_ref is None:
                                        group_ref = self._add_group(allow_any_child=True, max_mass=10000, caption="defend_attack", tp=AiContainer.AIGROUP_DEFEND_ATTACK)
                                        self._groups_defend_attack.append(group_ref)

                                    group_ref.add_child(mref)

                                if mref.group_ai not in groups:
                                    groups.add(mref.group_ai)

                        if self.allow_cost_effective and not is_defend:
                            if idx not in area_react:
                                area_react[idx] = []

                            area_react[idx].append(mref)
                            mref.focus_ai = None
                            mref.focus_ai_dist = -1
                            mref.focus_ai_perc = 0
                            mref.focus_ai_perc_min = 0

            for group_ref in groups:
                if group_ref.check_timeout(5):
                    group_ref.attack_move(self._tile_col.get_tile(areas[idx]['px'], areas[idx]['py']), force=True)

        for group_ref in [c for c in self._groups_defend_attack]:
            for mref in [mm for mm in group_ref.get_childs()]:
                if not mref.flag_ai:
                    self._add_unit_to_defend(mref)
                    def_flag = True

            if group_ref.get_is_empty():
                self._groups_defend_attack.remove(self._rem_group(group_ref))

        if def_flag:
            self._unit_actions_defend()

        # cost effective reakce

        grps_used = set()

        for idx in area_react.keys():
            # !! area_react[idx] - vlastni jednotky
            # !! areas[idx]['mvs'] - cizi jednotky

            grps_used.clear()

            for mref in area_react[idx]:
                spec = mref.get_spec()
                add_to_enemy = False

                if mref.group_ai not in grps_used:
                    grps_used.add(mref.group_ai)
                    mref.group_ai.set_enemy_mass(0)
                    add_to_enemy = True

                for trg_mref in areas[idx]['mvs']:
                    #def_perc = round((trg_mref.get_spec().get_bonus_perc(spec) - 1.0) * 100.0)
                    def_perc = spec.get_max_dealt_damage(trg_mref.get_spec()) # !! - trg_mref.get_spec().get_max_dealt_damage(spec) // 3
                    dist = mref.dist_to_moveable(trg_mref)

                    if trg_mref.get_is_heavy_damaged():
                        def_perc = def_perc * 2

                    if add_to_enemy:
                        mref.group_ai.add_enemy_mass(trg_mref)

                    if dist > spec.get_range() + 40:
                        # prilis daleko pro presnou reakci
                        pass

                    elif mref.focus_ai is None:
                        mref.focus_ai = trg_mref
                        mref.focus_ai_dist = dist
                        mref.focus_ai_perc = def_perc
                        mref.focus_ai_perc_min = def_perc

                    elif mref.focus_ai_perc < def_perc or (mref.focus_ai_perc == def_perc and mref.focus_ai_dist > dist):
                        mref.focus_ai = trg_mref
                        mref.focus_ai_dist = dist
                        mref.focus_ai_perc = def_perc
                        mref.focus_ai_perc_min = min(mref.focus_ai_perc_min, def_perc)

                    else:
                        mref.focus_ai_perc_min = min(mref.focus_ai_perc_min, def_perc)

                    #print(spec.get_key() + " attack " + trg_mref.get_spec().get_key(), def_perc)

            att_dict = OrderedDict()

            for mref in area_react[idx]:
                if mref.focus_ai is not None: # and mref.focus_ai_perc_min < mref.focus_ai_perc:
                    # !! print(mref.get_spec().get_key() + " attack " + mref.focus_ai.get_spec().get_key(), mref.focus_ai_perc, mref.focus_ai_dist)

                    if mref.focus_ai not in att_dict:
                        att_dict[mref.focus_ai] = []

                    att_dict[mref.focus_ai].append(mref)

            for att_mref in att_dict.keys():
                self._side.set_selected(att_dict[att_mref])
                self._side.selected_goto_attack(att_mref)

    def _perform_attack(self):
        if self.allow_attack:
            if len(self._groups_attack) > 0:
                def_flag = False
                ret_groups = set()

                for group_ref in [c for c in self._groups_attack]:
                    if group_ref.get_is_empty():
                        self._groups_attack.remove(self._rem_group(group_ref))
                        self._attack_tick_max = 20
                        self._attack_tick = 0

                    elif self.allow_retreat and group_ref.get_is_more_enemy():
                        # retreat force
                        ret_groups.add(group_ref)
                        self._groups_attack.remove(group_ref)

                    elif self.allow_retreat and group_ref.get_is_min_mass() and not group_ref.get_is_any_attacking():
                        # retreat - back to defend
                        ret_groups.add(group_ref)
                        self._groups_attack.remove(group_ref)

                    elif group_ref.check_timeout(5) and not group_ref.get_is_moving(): # group_ref.get_is_any_waiting(): # timeout * 50
                        tile = group_ref.scan_attack_tile()

                        if tile is not None:
                            group_ref.attack_move(tile, force=True)
                        else:
                            # nowhere to come - back to defend
                            for mref in [mm for mm in group_ref.get_childs()]:
                                self._add_unit_to_defend(mref)
                                def_flag = True

                if len(ret_groups) > 0 and (len(self._groups_attack) > 0 or len(ret_groups) > 1):
                    # merge attack groups
                    grp_added = set()

                    for group_ref in ret_groups:
                        for mref in [mm for mm in group_ref.get_childs()]:
                            grp_added.add(self._add_unit_to_attack(mref))

                        self._rem_group(group_ref)

                    ret_groups = set()

                    for group_ref in grp_added:
                        group_ref.set_min_mass(max(group_ref.get_mass() // 2, self._retreat_mass))

                        if group_ref.get_is_min_mass():
                            ret_groups.add(group_ref)
                            self._groups_attack.remove(group_ref)

                if len(ret_groups) > 0:
                    self._attack_tick = 0
                    self._attack_tick_max = 30
                    def_flag = True

                    for group_ref in ret_groups:
                        for mref in [mm for mm in group_ref.get_childs()]:
                            self._add_unit_to_defend(mref)
                            def_flag = True

                        self._rem_group(group_ref)

                if def_flag:
                    self._unit_actions_defend()
                    self._attack_tick_max = 20
                    self._attack_tick = 0

            do_attack = False
            self._attack_tick += 1

            tmass = 0
            ebs = self._scan.get_enemy_builds()

            for group_ref in self._groups_defend:
                tmass += group_ref.get_mass()

            if len(self._groups_defend_attack) > 0 or tmass < 10 or len(ebs) == 0:
                self._attack_tick = 0
            elif self._attack_tick < self._attack_tick_max:
                pass
            else:
                do_attack = True

            if do_attack:
                self._attack_tick = 0
                self._attack_tick_max = 40

                grp_added = set()

                for group_ref in self._groups_defend:
                    for mref in [c for c in group_ref.get_childs()]:
                        grp_added.add(self._add_unit_to_attack(mref))

                for group_ref in grp_added:
                    group_ref.set_min_mass(max(group_ref.get_mass() // 2, self._retreat_mass))

    def is_action_stacked(self, key):
        spec = key if isinstance(key, ActionItemSpec) else ActionItemSpec.get(self._side.get_type() + "_" + key)
        return self._actions.get_spec_stacked_count(spec) > 0

    def may_stack_action(self, key, bref, money_slot, strict=False):
        spec = key if isinstance(key, ActionItemSpec) else ActionItemSpec.get(self._side.get_type() + "_" + key)
        if strict and bref.get_action_item() is not None:
            return False
        return bref.get_may_apply_action(spec) and self._money.get_money(money_slot) >= spec.get_money_cost() and self._actions.get_spec_stacked_count(spec) == 0

    def is_action_available(self, key, bref):
        spec = key if isinstance(key, ActionItemSpec) else ActionItemSpec.get(self._side.get_type() + "_" + key)
        return bref.get_may_apply_action(spec)

    def get_unit_stat(self, key=None):
        if key is None:
            return self._unit_stats

        elif key in self._unit_stats:
            return self._unit_stats[key]

        else:
            return {
                "created": 0,
                "death": 0,
                "killed": 0
            }

    def update_unit_stat(self, spec, **kwargs):
        if "created" in kwargs and kwargs["created"]:
            self._unit_stats[spec.get_key()]["created"] += 1

        if "death" in kwargs and kwargs["death"]:
            self._unit_stats[spec.get_key()]["death"] += 1

        if "killed" in kwargs and kwargs["killed"]:
            self._unit_stats[spec.get_key()]["killed"] += 1
