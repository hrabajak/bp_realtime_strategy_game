from collections import deque

from game.ai.aigroupuniv import AiGroupUniv
from game.graphic.primitivecollector import PrimitiveCollector
from game.interface.itilecollector import ITileCollector
from lib.orderedset import OrderedSet

include "aimapscan.pxi"


cdef class AiGroup:

    cdef:
        object _parent
        object _scan
        object _tile_col
        object _side
        object _childs
        object _childs_hash
        object _univ
        object _caption
        object _last_place
        int _tp
        long _last_action
        long _max_mass
        long _enemy_mass
        long _allow_any_child
        long _death_count
        long _mass
        long _min_mass
        long _px
        long _py
        long _timeout

    def __cinit__(self, parent, scan, side, allow_any_child=False, max_mass=10, caption="", tp=0):
        self._parent = parent
        self._scan = scan
        self._tile_col = ITileCollector.get()
        self._side = side
        self._childs = []
        self._childs_hash = OrderedSet()
        self._univ = None
        self._last_place = None
        self._last_action = -1
        self._allow_any_child = allow_any_child
        self._max_mass = max_mass
        self._caption = caption
        self._death_count = 0
        self._mass = 0
        self._min_mass = 0
        self._px = 0
        self._py = 0
        self._timeout = 0
        self._tp = tp
        self._enemy_mass = 0

    def get_childs(self):
        return self._childs

    def get_childs_count(self):
        return len(self._childs)

    def get_have_child(self, mref):
        return mref in self._childs_hash

    def get_is_empty(self):
        return len(self._childs) == 0

    def get_caption(self):
        return self._caption

    def get_mass(self):
        return self._mass

    def get_min_mass(self):
        return self._min_mass

    def get_enemy_mass(self):
        return self._enemy_mass

    def get_px(self):
        return self._px

    def get_py(self):
        return self._py

    def get_tp(self):
        return self._tp

    def get_is_moving(self):
        return next((True for ch in self._childs if ch.get_is_moving() and not ch.get_is_stuck()), False)

    def get_is_moving_all(self):
        m_count = 0

        for ch in self._childs:
            if ch.get_is_moving() and not ch.get_is_stuck():
                m_count += 1

        return m_count == len(self._childs)

    def get_is_any_waiting(self):
        for ch in self._childs:
            if (not ch.get_is_moving() or ch.get_is_stuck()) and not ch.get_is_targeting():
                return True

        return False

    def get_is_any_attacking(self):
        return next((True for ch in self._childs if ch.get_target() is not None), False)

    def get_is_min_mass(self):
        return self._mass <= self._min_mass

    def get_is_more_enemy(self):
        return (self._mass * 1500) // 1000 <= self._enemy_mass

    def set_min_mass(self, min_mass):
        self._min_mass = min_mass

    def set_enemy_mass(self, enemy_mass):
        self._enemy_mass = enemy_mass

    def add_enemy_mass(self, mref):
        self._enemy_mass += mref.get_spec().get_mass_sec()

    def check_timeout(self, max_timeout):
        if self._timeout == 0:
            self._timeout = max_timeout
            return True
        else:
            return False

    def get_may_add_child(self, child):
        return (self._univ is None or child.get_spec().get_key() in self._univ) and self._mass + child.get_spec().get_mass_sec() <= self._max_mass

    def add_child(self, child):
        if child.group_ai is not self:
            self._childs.append(child)
            self._childs_hash.add(child)
            self._mass += child.get_spec().get_mass_sec()
            self._last_place = None
            self._last_action = -1

            if self._allow_any_child:
                # any child allowed
                pass
            elif self._univ is None:
                self._univ = AiGroupUniv.get_univ_from_spec(child.get_spec().get_key())

            if child.group_ai is not None:
                child.group_ai.rem_child(child)

            child.group_ai = self

    def rem_child(self, child):
        if child.group_ai is self:
            self._childs.remove(child)
            self._childs_hash.remove(child)
            child.group_ai = None

            if len(self._childs) == 0:
                self._univ = None

    def update(self):
        cdef object child, tile, c

        self._timeout = max(self._timeout - 1, 0)
        self._px = 0
        self._py = 0
        self._mass = 0

        for child in [c for c in self._childs]:
            if not child.get_is_alive():
                self._childs.remove(child)
                self._childs_hash.remove(child)
                self._mass -= child.get_spec().get_mass_sec()
                self._death_count += 1
            else:
                tile = child.get_tile()
                self._px += tile.get_pos_x()
                self._py += tile.get_pos_y()
                self._mass += child.get_spec().get_mass_sec()

        if len(self._childs) > 0:
            self._px = self._px // len(self._childs)
            self._py = self._py // len(self._childs)

    def stop(self):
        cdef object ch

        for ch in self._childs:
            ch.stop()

    def check_scout(self):
        cdef size_t a_ptr = self._scan.get_a_ptr()
        cdef AiMapScanField * a = <AiMapScanField *> a_ptr
        cdef object ch, nb
        cdef int do_evade = False

        for ch in self._childs:
            if ch.get_target_entity().scan_target_check() is not None:
                do_evade = True
                break

        if do_evade:
            self.scan_scout()
            return True

        else:
            return False

    def goto(self, tile, force=False):
        cdef object final_place, use_childs

        final_place = tile.get_place()

        if final_place is not None and (force or self._last_place is not final_place or self._last_action != 0):
            self._side.set_selected(self._childs)
            self._side.selected_goto(final_place)

            self._last_place = final_place
            self._last_action = 0

    def attack_move(self, tile, force=False):
        final_place = tile.get_place()

        if final_place is not None and (force or self._last_place is not final_place or self._last_action != 1):
            self._side.set_selected(self._childs)
            self._side.selected_attack_move(final_place)

            self._last_place = final_place
            self._last_action = 1

    def scan_scout(self):
        cdef long use_flag, final_scan, bpx, bpy, score, final_score
        cdef object stk = deque()
        cdef object cur, sbc, final_place
        cdef size_t a_ptr = self._scan.get_a_ptr()
        cdef AiMapScanField * a = <AiMapScanField *> a_ptr
        cdef long check_places = 200

        bpx = self._parent.get_build_pos()[0]
        bpy = self._parent.get_build_pos()[1]

        use_flag = self._tile_col.get_find_flag()
        final_place = None
        final_score = -1

        for mref in self._childs:
            stk.append(mref.get_tile())

        while stk and check_places > 0:
            cur = stk.popleft()
            cur.flag = use_flag

            if cur.is_accessible_simple() and a[cur.number].enemy_value < 100 and a[cur.number].build == 0: # !! cur.get_accessible_place(None) is not None
                score = abs(cur.get_pos_x() - bpy) + abs(cur.get_pos_y() - bpx) + a[cur.number].scan

                if final_score == -1 or score < final_score:
                    final_place = cur.get_accessible_place(self._childs[0])
                    final_score = score

                check_places -= 1

            for sbc in cur.get_neighbors_plus():
                if sbc.is_accessible_simple() and sbc.flag != use_flag:
                    sbc.flag = use_flag
                    stk.append(sbc)

        self._tile_col.move_find_flag()

        if final_place is not None:
            self._side.set_selected(self._childs)
            self._side.selected_goto(final_place)

    def scan_attack_tile(self):
        cdef long use_flag, final_scan, score, use_score, final_score
        cdef object stk = deque()
        cdef object cur, sbc, final_tile, side, blist
        cdef size_t a_ptr = self._scan.get_a_ptr()
        cdef AiMapScanField * a = <AiMapScanField *> a_ptr
        cdef long check_places = 300

        blist = self._scan.get_enemy_build_list()
        use_flag = self._tile_col.get_find_flag()
        final_tile = None
        final_score = -1

        for mref in self._childs:
            stk.append(mref.get_tile())

        while stk and check_places > 0:
            cur = stk.popleft()
            cur.flag = use_flag

            if cur.is_accessible_simple() and a[cur.number].enemy_tick > 0:
                use_score = -1

                if len(blist.keys()) > 0:
                    for side in blist.keys():
                        score = 10000 - (abs(cur.get_pos_x() - blist[side]['px']) + abs(cur.get_pos_y() - blist[side]['py'])) + a[cur.number].enemy_value

                        if use_score == -1 or score > use_score:
                            use_score = score

                elif a[cur.number].enemy_value > 100:
                    use_score = a[cur.number].enemy_value

                if use_score != -1 and (final_score == -1 or use_score > final_score):
                    final_tile = cur
                    final_score = use_score

                check_places -= 1

            for sbc in cur.get_neighbors_plus():
                if sbc.is_accessible_simple() and sbc.flag != use_flag:
                    sbc.flag = use_flag
                    stk.append(sbc)

        self._tile_col.move_find_flag()

        return final_tile

    def scan_goto_defend(self):
        cdef long use_flag, final_scan, bpx, bpy, score, final_score, final_e_score
        cdef object stk = deque()
        cdef object cur, sbc, tile, bref, final_place
        cdef size_t a_ptr = self._scan.get_a_ptr()
        cdef AiMapScanField * a = <AiMapScanField *> a_ptr

        bpx = self._parent.get_build_pos()[0]
        bpy = self._parent.get_build_pos()[1]

        use_flag = self._tile_col.get_find_flag()
        final_place = None
        final_score = -1
        final_e_score = -1

        for bref in self._side.get_build_childs():
            for tile in bref.get_tiles():
                tile.flag = use_flag
                stk.append(tile)

        while stk:
            cur = stk.popleft()
            cur.flag = use_flag

            if cur.get_accessible_place(None) is not None and a[cur.number].build > 3 and a[cur.number].build <= 6:

                if a[cur.number].enemy_tick > 0:
                    score = a[cur.number].enemy_tick

                    if final_e_score == -1 or score > final_e_score:
                        final_place = cur.get_accessible_place(None)
                        final_e_score = score

                elif final_e_score == -1:
                    score = abs(cur.get_pos_x() - bpy) + abs(cur.get_pos_y() - bpx)

                    if final_score == -1 or score < final_score:
                        final_place = cur.get_accessible_place(None)
                        final_score = score

            for sbc in cur.get_neighbors_plus():
                if sbc.flag != use_flag and a[sbc.number].build > 0:
                    sbc.flag = use_flag
                    stk.append(sbc)

        self._tile_col.move_find_flag()

        if final_place is not None:
            self._side.set_selected(self._childs)
            self._side.selected_attack_move(final_place)
