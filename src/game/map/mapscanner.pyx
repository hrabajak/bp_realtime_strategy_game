from collections import deque

from game.interface.itilecollector import ITileCollector


cdef class MapScanner:

    cdef object _tile_col

    def __cinit__(self, tile_col: ITileCollector):
        self._tile_col = tile_col

    def scan_tiles(self, tile_from, max_steps, callback, step_callback=None, instant=False):
        cdef long use_flag
        cdef object stk, result

        use_flag = self._tile_col.get_find_flag()
        max_steps *= 10

        stk = deque()

        if isinstance(tile_from, list):
            for tile in tile_from:
                tile.back_steps = 0
                tile.flag = use_flag
                stk.append(tile)

        else:
            tile_from.back_steps = 0
            tile_from.flag = use_flag
            stk.append(tile_from)

        result = None

        while stk:
            tile = stk.popleft()
            tile.flag = use_flag

            result = callback(tile, result)

            if instant and result is not None:
                return result

            for sb in tile.get_neighbors_with_cost():
                sub_tile = sb[1]
                cost = sb[0]

                if sub_tile.flag != use_flag and tile.back_steps + 10 <= max_steps and (step_callback is None or step_callback(sub_tile)):
                    sub_tile.flag = use_flag
                    sub_tile.back_steps = tile.back_steps + cost
                    stk.append(sub_tile)

        self._tile_col.move_find_flag()

        return result

    def scan_targetables_on_tiles(self, tile_from, max_steps, callback):
        def _fnd(tile, result):
            cdef object mm, bref

            for mm in tile.get_moveables():
                result = callback(mm, result)
            bref = tile.get_build_ref()
            if bref is not None:
                result = callback(bref, result)

            return result

        return self.scan_tiles(tile_from, max_steps, _fnd)

    def scan_avail_place(self, moveable_ref, place_from):
        cdef long use_flag
        cdef object stk, place, sub_place

        if place_from.is_accessible(moveable_ref):
            return place_from

        use_flag = self._tile_col.get_find_flag()
        place_from.back_steps = 0

        stk = deque()
        stk.append(place_from)

        while stk:
            place = stk.popleft()

            if place.is_accessible(moveable_ref):
                self._tile_col.move_find_flag()
                return place

            place.flag = use_flag

            for sb in place.get_neighbors_with_cost():
                sub_place = sb[1]

                if sub_place.flag != use_flag:
                    sub_place.flag = use_flag
                    stk.append(sub_place)

        self._tile_col.move_find_flag()

        return None

    def scan_avail_places(self, place_from, places_count):
        cdef long use_flag, sec_flag
        cdef object stk, out, place

        use_flag = self._tile_col.get_find_flag()
        sec_flag = self._tile_col.get_find_flag(1)

        stk = deque()
        stk += place_from if isinstance(place_from, list) else [place_from]
        out = []

        for tt in stk:
            tt.back_steps = 0

        while stk and len(out) < places_count:
            place = stk.popleft()

            if place.is_accessible_simple():
                out.append(place)
                if len(out) == places_count:
                    break

            place.flag = use_flag

            for sub_place in place.get_neighbors_around():
                if sub_place.flag != use_flag:
                    sub_place.flag = use_flag
                    stk.append(sub_place)

        self._tile_col.move_find_flag()
        self._tile_col.move_find_flag(1)

        return out
