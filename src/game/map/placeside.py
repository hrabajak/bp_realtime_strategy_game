

class PlaceSide:
    _side_map = {}

    @staticmethod
    def build():
        vals = [
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
            (1, 0),
            (1, -1),
            (0, -1)
        ]

        for idx in range(0, len(vals)):
            idx_left = idx - 1
            idx_right = idx + 1

            if idx_left < 0:
                idx_left = len(vals) - 1
            if idx_right >= len(vals):
                idx_right = 0

            h = vals[idx][0] * 10 + vals[idx][1]
            PlaceSide._side_map[h] = (vals[idx_left], vals[idx_right])
