from libc.stdlib cimport malloc, free
from collections import deque
from lib.orderedset import OrderedSet
include "pathflowmapdef.pxi"


cdef object pf_maps = deque()
cdef long pf_maps_size = 0

cdef class PathFlowMap:

    cdef:
        PathFlowMapField * _a
        object _keys
        object _parent
        object _tile_col
        object _goto_group
        object _stk
        object _conv_key_idx
        int _conv_slot

    def __cinit__(self, parent, tile_col):
        cdef size_t a_ptr

        self._parent = parent
        self._tile_col = tile_col
        self._keys = []
        self._stk = deque()
        self._goto_group = None
        self._conv_key_idx = 0
        self._conv_slot = 0

        cdef long idx, midx = self._tile_col.get_tiles_width() * 2 * self._tile_col.get_tiles_height() * 2
        global pf_maps_size

        if midx != pf_maps_size:
            while pf_maps:
                a_ptr = pf_maps.popleft()
                free(<void *>a_ptr)

            pf_maps_size = midx

        if pf_maps:
            a_ptr = pf_maps.popleft()
        else:
            a_ptr = <size_t>malloc(midx * sizeof(PathFlowMapField))

        self._a = <PathFlowMapField *> a_ptr
        self.clear()

    def set_goto_group(self, goto_group):
        self._goto_group = goto_group

    def get_a_ptr(self):
        return <size_t>self._a

    def add_key(self, key):
        self._keys.append(key)

    def clear(self):
        cdef long idx, midx = self._tile_col.get_tiles_width() * 2 * self._tile_col.get_tiles_height() * 2

        for idx in range(0, midx):
            self._a[idx].flag = 0
            self._a[idx].cost[0] = COST_NA
            self._a[idx].cost[1] = COST_NA
            self._a[idx].finish = FINISH_NONE
            self._a[idx].steps = 0

        self._keys.clear()

    def invalidate_finish(self, places):
        for pl in places:
            if self._a[pl.number].finish > 0:
                self._a[pl.number].finish = FINISH_INVALID

    def release(self):
        cdef size_t a_ptr = <size_t>self._a
        pf_maps.append(a_ptr)
        self._a = NULL

    def add_place_targets(self, place_targets, int is_used=False):
        cdef long number

        for pl in place_targets:
            number = pl.number

            self._a[number].cost[0] = 0
            self._a[number].cost[1] = 0
            self._a[number].finish = FINISH_USED if is_used else FINISH_TRAVEL

            self._keys.append(number)

    def add_targetables(self, targetables):
        cdef long number

        for place in [pl for c in targetables for pl in c.get_places_around() if c.get_is_alive()]:
            number = place.number

            if self._a[number].finish == FINISH_NONE:
                self._a[number].cost[0] = 0
                self._a[number].cost[1] = 0
                self._a[number].finish = FINISH_TEMP

                self._keys.append(number)

    def rem_targetable(self, trg_ref):
        cdef long number

        for place in [pl for pl in trg_ref.get_places()]:
            number = place.number

            if self._a[number].finish > 0:
                self._a[number].finish = FINISH_NONE

    def update_costs_prepare(self, place_targets, clear=True):
        cdef long number

        if clear:
            self._stk.clear()

        for pl in place_targets:
            number = pl.number

            if self._a[number].finish in [FINISH_TRAVEL, FINISH_TEMP]:
                pl.cost = self._a[number].cost[0]
                pl.cost_2 = self._a[number].cost[1]
                pl.cost_inc = 0

                if pl.cost != COST_NA:
                    self._stk.append(pl)

    def _check_moveable(self, mref):
        if mref is None:
            return True

        elif mref in self._parent.get_block_moveables():
            return False

        elif mref.get_goto_group() is self._goto_group and (not mref.get_is_moving() or self._parent.get_entity(mref).get_evade_check()):
            return False

        return True

    def update_costs(self, long use_flag, long max_steps):
        cdef long number, sub_number
        cdef long steps = 0

        cdef long sub_cost
        cdef object sub_place
        cdef object sub_sides
        cdef int allow_2

        while self._stk and steps < max_steps:
            cur = self._stk.popleft()
            number = cur.number

            self._a[number].flag = use_flag

            steps += 1

            for c in cur.get_neighbors_with_cost():
                sub_cost = c[0]
                sub_place = c[1]
                sub_sides = c[2]
                #sub_cost = 1
                sub_cost = sub_cost * ZOOM_CONV // 10 # bez konvoluce tady musi byt odpovidajici hodnota
                sub_number = sub_place.number

                if self._a[sub_number].cost[0] == COST_NA or self._a[sub_number].finish > 0:
                    # nedostupne policko pro velikost 1 implikuje nedostupnost i pro velikost 2
                    pass

                elif sub_place.is_accessible_no_build_only_b() and self._check_moveable(sub_place.get_moveable_ref()):

                    allow_2 = cur.cost_2 != COST_NA

                    if allow_2 and sub_place.get_acc() < 2:
                        if sub_place.get_acc_ref() is None:
                            allow_2 = False

                        elif not self._check_moveable(sub_place.get_acc_ref()):
                            allow_2 = False

                    if self._a[sub_number].flag != use_flag:
                        sub_place.cost = cur.cost + sub_cost
                        sub_place.cost_inc = sub_cost
                        self._a[sub_number].flag = use_flag
                        self._a[sub_number].cost[0] = sub_place.cost

                        if allow_2:
                            sub_place.cost_2 = cur.cost_2 + sub_cost
                            self._a[sub_number].cost[1] = sub_place.cost_2
                        else:
                            sub_place.cost_2 = COST_NA
                            self._a[sub_number].cost[1] = COST_NA

                        self._stk.append(sub_place)

                    else:
                        if cur.cost + sub_cost < sub_place.cost:
                            sub_place.cost = cur.cost + sub_cost
                            sub_place.cost_inc = sub_cost
                            self._a[sub_number].cost[0] = cur.cost + sub_cost

                        if allow_2 and cur.cost_2 + sub_cost < sub_place.cost_2:
                            sub_place.cost_2 = cur.cost_2 + sub_cost
                            self._a[sub_number].cost[1] = sub_place.cost_2

                    #PrimitiveCollector().add_line(sub_place.get_x(), sub_place.get_y(), cur.get_x(), cur.get_y())

                else:
                    self._a[sub_number].cost[0] = COST_NA
                    self._a[sub_number].cost[1] = COST_NA
                    self._a[sub_number].finish = FINISH_NONE
                    sub_place.cost = COST_NA
                    sub_place.cost_2 = COST_NA

                    #PrimitiveCollector().add_circle(sub_place.get_x(), sub_place.get_y(), 5)

        #for idx in range(0, self._tile_col.get_tiles_width() * 2 * self._tile_col.get_tiles_height() * 2):
            #if self._a[idx].cost[0] > COST_NA:
                #print("BLBOST-A!")
            #if self._a[idx].cost[1] > COST_NA:
                #print("BLBOST-B!")

        return steps < max_steps, steps

    def convolution_prepare(self, int use_slot):
        self._conv_key_idx = 0
        self._conv_slot = use_slot

    def convolution(self, long max_steps):

        cdef long steps = 0, fx, fy, number, nb_number, k_len = len(self._keys)
        cdef unsigned long long cost_sum, cost_cnt
        cdef long long conv_mtx[3][3]

        # pozor - gausovske rozostreni neni nakonec idealni - nektere vektory mohou prijit o informaci o smeru
        #conv_mtx[0][:] = [1, 2, 1]
        #conv_mtx[1][:] = [2, 4, 2]
        #conv_mtx[2][:] = [1, 2, 1]

        # nakonec lehka modifikace box blur
        conv_mtx[0][:] = [1, 1, 1]
        conv_mtx[1][:] = [1, 2, 1]
        conv_mtx[2][:] = [1, 1, 1]

        # self._conv_mtx.append((1, 4, 6, 4, 1))
        # self._conv_mtx.append((4, 16, 24, 16, 4))
        # self._conv_mtx.append((6, 24, 36, 24, 6))
        # self._conv_mtx.append((4, 16, 24, 16, 4))
        # self._conv_mtx.append((1, 4, 6, 4, 1))

        places = self._tile_col.get_places()

        while self._conv_key_idx < k_len and steps < max_steps:
            cur = places[self._keys[self._conv_key_idx]]
            number = cur.number
            self._conv_key_idx += 1

            if self._a[number].cost[self._conv_slot] == COST_NA:
                self._a[number].flag = COST_NA

            else:
                cost_sum = 0
                cost_cnt = 0

                steps += 1

                for fy in range(0, 3):
                    for fx in range(0, 3):
                        nb = cur.get_neighbor(fy - 1, fx - 1)

                        if nb is not None:
                            nb_number = nb.number

                            if self._a[nb_number].cost[self._conv_slot] != COST_NA:
                                cost_sum += <long long>self._a[nb_number].cost[self._conv_slot] * conv_mtx[fy][fx]
                                cost_cnt += conv_mtx[fy][fx]

                if cost_cnt == 0:
                    self._a[number].flag = self._a[number].cost[self._conv_slot]
                else:
                    #print(cost_sum, cost_cnt, cost_sum // cost_cnt)
                    self._a[number].flag = (cost_sum * <unsigned long long>ZOOM_CONV) // cost_cnt

        return steps < max_steps, steps

    def convolution_finish(self):
        cdef long key

        for key in self._keys:
            self._a[key].cost[self._conv_slot] = self._a[key].flag
