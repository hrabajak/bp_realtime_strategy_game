import math
from collections import deque

from game.helper.distangle import DistAngle
from game.interface.itilecollector import ITileCollector
from lib.heap import BinaryHeap


cdef class PathFinder:

    cdef object _tile_col

    def __cinit__(self, tile_col: ITileCollector):
        self._tile_col = tile_col

    def back_list(self, tile_from, tile_to):
        cdef object out, loop

        out = []
        loop = tile_from

        if loop.back_ref is None:
            out = [loop]
            return out

        while True:
            out.append(loop)
            loop = loop.back_ref

            if loop is None:
                break
            elif loop is tile_to:
                out.append(loop)
                break

        return out

    def path_find_tile(self, tile_from, callback_found, callback_step=lambda tile: True):
        cdef long use_flag, cost
        cdef object stk, tile, tile_to, sub_title

        use_flag = self._tile_col.get_find_flag()

        stk = deque()
        stk.append(tile_from)

        tile_from.back_steps = 0
        tile_from.back_ref = None
        tile_to = None

        while stk and tile_to is None:
            tile = stk.popleft()
            tile.flag = use_flag

            for sb in tile.get_neighbors_with_cost():
                cost = sb[0]
                sub_tile = sb[1]

                if callback_step(sub_tile):

                    if sub_tile.flag != use_flag:
                        sub_tile.back_ref = None
                        stk.append(sub_tile)

                    if sub_tile.back_ref is None or tile.back_steps + cost <= sub_tile.back_steps:
                        sub_tile.back_ref = tile
                        sub_tile.back_steps = tile.back_steps + cost
                        sub_tile.flag = use_flag

                if callback_found(sub_tile):

                    if sub_tile.back_ref is None or tile.back_steps + cost <= sub_tile.back_steps:
                        sub_tile.back_ref = tile
                        sub_tile.back_steps = tile.back_steps + cost
                        sub_tile.flag = use_flag

                    if tile_to is None or sub_tile.back_steps <= tile_to.back_steps:
                        tile_to = sub_tile

        self._tile_col.move_find_flag()

        return tile_to

    def path_find_place_for_moveable(self, moveable_ref, place_from, place_to, to_range=0, max_len=-1, acc_tolerance=250, strict=False, exclude=None):
        cdef int find_loop
        cdef long use_flag, use_to_flag, max_len_mul, cost, to_x, to_y, steps
        cdef object stk, place, sub_place, sb

        # oceneni cilovych poli podle rozsahu

        if place_from is place_to:
            place_to.back_steps = 0
            place_to.back_ref = None
            return place_to

        use_flag = self._tile_col.get_find_flag(0)
        use_to_flag = self._tile_col.get_find_flag(1)
        to_range *= 10
        max_len_mul = max_len * 10

        #for pl in self._tile_col.get_places(): pl.test_flag = 0

        stk = deque()
        stk.append(place_to)
        place_to.to_flag = use_to_flag
        place_to.to_cost = 0

        while stk:

            place = stk.popleft()
            place.to_flag = use_to_flag
            #place.test_flag = 2
            place.back_ref = None

            if place.to_cost < to_range:
                for sb in place.get_neighbors_with_cost():
                    cost = sb[0] // 2
                    sub_place = sb[1]
                    sub_place.back_ref = None

                    if sub_place.to_flag != use_to_flag:
                        sub_place.to_cost = place.to_cost + cost
                        sub_place.to_flag = use_to_flag
                        #sub_place.test_flag = 2
                        stk.append(sub_place)

        # hledani cesty

        place_from.back_steps = 0
        place_from.back_ref = None
        place_from.at_angle = moveable_ref.get_angle_dist()
        place_from.dist = 0

        to_x = place_to.get_pos_x()
        to_y = place_to.get_pos_y()

        place_found = []
        find_loop = True
        steps = 0

        if exclude is not None:
            for place in exclude:
                place.to_flag = use_to_flag - 1
                place.flag = use_flag

        stk = BinaryHeap()
        stk.push(0, place_from)

        while stk and find_loop:
            place = stk.pop_ref()
            place.flag = use_flag

            steps += 1

            if steps > max_len * 3:
                # blbost
                find_loop = False
                break

            for sb in place.get_neighbors_with_cost():
                cost = sb[0]
                sub_place = sb[1]
                #sub_place.dist = math.ceil(sub_place.dist_to(place_to) / 2)
                #sub_place.dist = math.ceil(sub_place.dist_to(place_to) / 3)
                sub_place.dist = (abs(to_x - sub_place.get_pos_x()) + abs(to_y - sub_place.get_pos_y())) * 10

                if sub_place.get_moveable_focus() > 0:
                    cost += sub_place.get_moveable_focus() * 3

                if place is place_from:
                    sub_place.at_angle = DistAngle()
                    sub_place.at_angle.set_places(place, sub_place)

                    btw = place.at_angle.angle_btw_deg(sub_place.at_angle)

                    if btw <= 45:
                        pass
                    elif btw <= 90:
                        cost += 20
                    elif btw <= 135:
                        cost += 30
                    else:
                        cost += 40

                if max_len != -1 and place.back_steps + cost > max_len_mul:
                    pass

                else:
                    if sub_place.flag != use_flag:
                        sub_place.back_ref = None

                    if sub_place.flag != use_flag and sub_place.is_accessible_tolerance(moveable_ref, acc_tolerance):

                        if sub_place.back_ref is None or place.back_steps + cost < sub_place.back_steps:
                            sub_place.back_ref = place
                            sub_place.back_steps = place.back_steps + cost

                            # optimalizace vzdalenosti
                            stk.push(sub_place.dist, sub_place)

                    if sub_place.to_flag == use_to_flag and (sub_place.is_accessible(moveable_ref) or strict):
                        # je cilovym polem - drzeni nekolika nejcenejsich poli

                        if sub_place.back_ref is None or place.back_steps + cost < sub_place.back_steps:
                            sub_place.back_ref = place
                            sub_place.back_steps = place.back_steps + cost

                        place_found.append(sub_place)
                        place_found.sort(key=lambda tl: tl.to_cost, reverse=False)

                        if len(place_found) > 15:
                            del place_found[15]

                        if sub_place.to_cost == 0:
                            # pole s nejnizsi cenou nalezeno - nema smysl nic dalsiho
                            find_loop = False
                            break

                    sub_place.flag = use_flag

        self._tile_col.move_find_flag(0)
        self._tile_col.move_find_flag(1)

        #print("STEPS: " + str(steps) + " | " + str(max_len))

        if len(place_found) > 0 and place_found[0].to_cost == 0:
            # jedeme na pole s nejnizsi cenou
            return place_found[0]

        elif place_from in place_found:
            return place_from

        elif len(place_found) > 0:
            # vybereme to pole do ktereho se da dostat s nejmene kroky
            place_found.sort(key=lambda tl: tl.back_steps, reverse=False)

            return place_found[0]

        elif place_from.to_flag == use_to_flag:
            # stacilo stat na miste
            return place_from

        else:
            # za nastavenych podminek nevede cesta
            return None

    def path_places_follow(self, place_list):
        cdef long use_flag, idx
        cdef object stk, place, trg_place, sub_place, c

        # oceneni cilovych poli podle rozsahu

        use_flag = self._tile_col.get_find_flag(0)

        stk = deque()
        stk.append(place_list[0])
        idx = 1

        while idx < len(place_list):
            place = stk.popleft()
            place.flag = use_flag

            trg_place = next((c for c in place.get_neighbors() if c == place_list[idx]), None)

            if trg_place is not None:
                trg_place.flag = use_flag
                trg_place.back_ref = place
                trg_place.back_steps = place.back_steps + 10

                stk = deque()
                stk.append(trg_place)
                idx += 1

            else:
                for c in place.get_neighbors_with_cost():
                    sub_place = c[1]

                    if sub_place.flag != use_flag:
                        sub_place.flag = use_flag
                        sub_place.back_ref = place
                        sub_place.back_steps = place.back_steps + 10

                        stk.append(sub_place)

        self._tile_col.move_find_flag(0)

        return self.back_list(place_list[len(place_list) - 1], place_list[0])

    def tile_list_to_place_list(self, tile_list):
        cdef long near_dist
        cdef object out, place, near_place, nb

        out = []

        while len(tile_list) > 0:
            place = tile_list[0].get_place()
            del tile_list[0]

            out.append(place)

            if len(tile_list) > 0:
                to_place = tile_list[0].get_place()
                near_place = None
                near_dist = 0

                for nb in place.get_neighbors():
                    dist = nb.dist_to(to_place)

                    if near_place is None or dist < near_dist:
                        near_place = nb
                        near_dist = dist

                if near_place is not None:
                    out.append(near_place)

        return out
