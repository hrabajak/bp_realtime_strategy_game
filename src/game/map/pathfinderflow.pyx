from collections import OrderedDict, deque

from lib.orderedset import OrderedSet
from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.discretemath cimport int_sqrt
from game.helper.heapqueue import HeapQueue
from game.interface.itilecollector import ITileCollector
from game.map.pathflowmap import PathFlowMap
from lib.heap import BinaryHeap

include "pathflowmapdef.pxi"


cdef class PathFinderFlow:

    REALIZE_NONE = 0
    REALIZE_GOTO = 1
    REALIZE_ATTACK = 2
    REALIZE_ATTACK_MOVE = 3

    cdef:
        object _tile_col
        object _goto_group
        object _entities
        object _entities_hash

        int _ready
        int _processing

        object _place_to
        object _place_targets
        object _place_targets_used
        object _targetables
        object _proc_costs
        object _costs

        int _mass_sum
        int _mass_size
        object _avail_size
        int _diff_x
        int _diff_y
        int _avg_x
        int _avg_y
        long _loop_flag

        object _block_places
        object _block_moveables

        int _count_finish
        int _count_moving
        int _realize_type
        object _realize_rel_object
        int _realize_sect
        object _realize_sizes
        int _realize_force
        int _realize_prior
        int _use_flag

        object _pth_stk_steps
        object _pth_stk_heap
        object _pth_stk
        object _pth_to_set
        object _pth_group_prep

        int _do_draw_preview
        
    def __cinit__(self, tile_col: ITileCollector, goto_group):
        self._tile_col = tile_col
        self._goto_group = goto_group
        self._place_targets = OrderedSet()
        self._place_targets_used = OrderedSet()
        self._targetables = OrderedDict()
        self._block_places = OrderedSet()
        self._block_moveables = OrderedSet()
        self._costs = None
        self._proc_costs = None
        self._avail_size = OrderedDict()
        self._avail_size[1] = False
        self._avail_size[2] = False
        self._entities = []
        self._entities_hash = OrderedDict()

        self._ready = True
        self._processing = False
        self._do_draw_preview = 0
        self._mass_sum = 0
        self._mass_size = 0
        self._diff_x = 0
        self._diff_y = 0
        self._avg_x = 0
        self._avg_y = 0
        self._loop_flag = 0
        self._count_finish = 0
        self._count_moving = 0
        self._use_flag = 0

        self._realize_type = PathFinderFlow.REALIZE_NONE
        self._realize_rel_object = None
        self._realize_sect = -1
        self._realize_force = 0
        self._realize_prior = False

        self._pth_stk_steps = deque()
        self._pth_stk = deque()
        self._pth_stk_heap = BinaryHeap()

    def set_moveables(self, moveables):
        self._entities.clear()
        self._entities_hash.clear()

        for mm in moveables:
            ent = mm.get_path().get_flow_entity()
            ent.reset(self)

            self._entities.append(ent)
            self._entities_hash[mm.number] = ent

    def add_moveable(self, mref):
        if mref.number not in self._entities_hash:
            ent = mref.get_path().get_flow_entity()
            ent.reset(self)

            self._entities.append(ent)
            self._entities_hash[mref.number] = ent

    def rem_moveable(self, mref):
        if mref.number in self._entities_hash:
            ent = self._entities_hash[mref.number]
            self._entities.remove(ent)
            del self._entities_hash[mref.number]

        if len(self._entities) == 0 and self._costs is not None:
            self._costs.release()
            self._costs = None

    def rem_entities(self, entities):
        for ent in entities:
            if ent.get_mref().number in self._entities_hash:
                self._entities.remove(ent)
                del self._entities_hash[ent.get_mref().number]

    def set_realize_force(self, value=1):
        self._realize_force = value

    def add_targetable(self, trg_ref):
        if trg_ref.get_is_alive() and trg_ref not in self._targetables:
            self._targetables[trg_ref] = trg_ref.get_place()
            return True
        else:
            return False

    def rem_targetable(self, trg_ref):
        if trg_ref in self._targetables:
            del self._targetables[trg_ref]

    def clear_targetables(self):
        self._targetables.clear()

    def check_targetables_change(self):
        for trg_ref in self._targetables.keys():
            if trg_ref.get_is_alive() and trg_ref.get_place() != self._targetables[trg_ref]:
                return True
        return False

    def get_entities(self):
        return self._entities

    def get_entity(self, mref):
        return self._entities_hash[mref.number] if mref.number in self._entities_hash else None

    def get_entity_score(self, mref):
        if mref.number in self._entities_hash:
            return self._entities_hash[mref.number].get_score()
        else:
            return 999999

    def get_realize_type(self):
        return self._realize_type

    def get_realize_rel_object(self):
        return self._realize_rel_object

    def get_moveables(self):
        return [ent.get_mref() for ent in self._entities]

    def get_place_to(self):
        if self._place_to is not None:
            return self._place_to
        elif len(self._targetables) > 0:
            for trg_ref in self._targetables.keys():
                return trg_ref.get_place()
            return None
        else:
            return None

    def get_place_targets(self):
        return self._place_targets

    def get_place_targets_used(self):
        return self._place_targets_used

    def get_targetables(self):
        return self._targetables.keys()

    def get_block_moveables(self):
        return self._block_moveables

    def get_costs(self) -> PathFlowMap:
        return self._costs

    def get_loop_flag(self) -> long:
        flag = self._loop_flag
        self._loop_flag += 1
        return flag

    def get_is_ready(self):
        return self._ready

    def get_may_move(self):
        return self._costs is not None

    def add_place_target(self, place):
        if place not in self._place_targets:
            self._place_targets.add(place)

    def add_place_target_used(self, place):
        if place not in self._place_targets_used:
            self._place_targets_used.add(place)

            if place not in self._place_targets:
                self._place_targets.add(place)

    def add_block_places(self, place):
        cdef long use_flag, steps
        cdef object stk

        if not place.is_accessible_goto_group(self._goto_group) and place not in self._block_places:

            use_flag = self._tile_col.get_find_flag()
            stk = deque()
            stk.append(place)

            steps = 0

            while stk and steps < 60:
                place = stk.popleft()
                place.flag = use_flag

                self._block_places.add(place)

                for sub_place in place.get_neighbors():
                    if sub_place.flag != use_flag and not sub_place.is_accessible_goto_group(self._goto_group) and sub_place.get_moveable_ref() is not None:
                        sub_place.flag = use_flag
                        stk.append(sub_place)

                steps += 1

            self._tile_col.move_find_flag()
            return True

        else:
            return False

    def clear_finish(self):
        [ent.clear_finish() for ent in self._entities]

    def invalidate_finish(self):
        if self._costs is not None:
            self._costs.invalidate_finish(self._place_targets)
            self._costs.invalidate_finish(self._place_targets_used)

        self._place_targets.clear()
        self._place_targets_used.clear()

    def prepare(self, place_to, keep_ready=False):
        if len(self._entities) == 0:
            return True

        if keep_ready:
            self._ready = True
        else:
            self._ready = False

        #PrimitiveCollector().clear()

        self._realize_force = 0
        self._place_to = place_to
        self._place_targets.clear()
        self._place_targets_used.clear()

        self._mass_sum = sum(ent.get_size() ** 2 for ent in self._entities)
        self._mass_size = max(1, int_sqrt(self._mass_sum))

        if self._mass_sum >= 4:
            self._mass_size = max(3, self._mass_size)

        self._avail_size[1] = False
        self._avail_size[2] = False

        avg_x = 0
        avg_y = 0

        for ent in self._entities:
            avg_x += ent.get_mref().get_place().get_pos_x()
            avg_y += ent.get_mref().get_place().get_pos_y()
            self._avail_size[ent.get_size()] = True

        if place_to is not None:
            self._diff_x = place_to.get_pos_x() - (avg_x // len(self._entities))
            self._diff_y = place_to.get_pos_y() - (avg_y // len(self._entities))

            use_range = []

            if self._diff_x != 0 and self._diff_y != 0 and abs(1000 - abs((self._diff_x * 1000) // self._diff_y)) < 600:
                # diagonala

                for idx in range(- self._mass_size // 2, self._mass_size // 2):
                    if self._diff_x > 0 and self._diff_y > 0:
                        use_range.append((idx * -1, idx))
                    elif self._diff_x > 0:
                        use_range.append((idx, idx))
                    elif self._diff_y > 0:
                        use_range.append((idx, idx))
                    else:
                        use_range.append((idx * -1, idx))

            elif abs(self._diff_x) > abs(self._diff_y):
                # vertikala
                use_range = [(0, c) for c in range(- self._mass_size // 2, self._mass_size // 2)]

            else:
                # horizontala
                use_range = [(c, 0) for c in range(- self._mass_size // 2, self._mass_size // 2)]

            use_flag = self._tile_col.get_find_flag()
            places_num = 0
            stk = deque()

            for coords in use_range:
                sec_place = place_to.get_place_add_xy(coords[0], coords[1])

                if sec_place is not None:
                    sec_place.flag = use_flag
                    stk.append(sec_place)
                    places_num += 1

            # nalez nejblizsich dostupnych policek

            while stk and places_num > 0:
                place = stk.popleft()
                place.flag = use_flag

                if place.is_accessible_no_build_only():
                    self._place_targets.add(place)
                    places_num -= 1

                for sub_place in place.get_neighbors():
                    if sub_place.flag != use_flag:
                        sub_place.flag = use_flag
                        stk.append(sub_place)

            self._tile_col.move_find_flag()

            """
            for coords in use_range:
                sec_place = place_to.get_place_add_xy(coords[0], coords[1])

                if sec_place is not None:
                    self._place_targets.add(sec_place)
            """

    def realize_light(self, realize_type=-1, realize_rel_object=None, prior=False):
        if realize_type == -1 or realize_type == self._realize_type:
            self._realize_force = 2
            self._realize_rel_object = realize_rel_object
        else:
            self.realize(realize_type, realize_rel_object, prior)

    def realize(self, realize_type=-1, realize_rel_object=None, prior=False):
        if len(self._entities) == 0 or self._processing or self._realize_prior:
            return True

        if realize_type != -1:
            self._realize_type = realize_type

        self._realize_rel_object = realize_rel_object

        #print("flow realize `" + str(self._realize_type) + "`")

        if self._proc_costs is None:
            self._proc_costs = PathFlowMap(self, self._tile_col)
        else:
            self._proc_costs.clear()

        n_targetables = OrderedDict()

        for trg_ref in self._targetables.keys():
            if trg_ref.get_is_alive():
                n_targetables[trg_ref] = trg_ref.get_place()

        self._targetables = n_targetables

        self._proc_costs.set_goto_group(self._goto_group)
        self._proc_costs.add_place_targets(self._place_targets)
        self._proc_costs.add_place_targets(self._place_targets_used, is_used=True)
        self._proc_costs.add_targetables(self._targetables.keys())

        if prior:
            for ent in self._entities:
                ent.reset(self)

            self._block_places.clear()
            self._block_moveables.clear()

        else:
            self._block_moveables.clear()
            to_rem = []

            for place in self._block_places:
                mref = place.get_moveable_ref()
                if mref is not None:
                    self._block_moveables.add(mref)
                else:
                    to_rem.append(place)

            [self._block_places.remove(c) for c in to_rem]

        self._use_flag = 1
        self._pth_stk_steps.clear()
        self._pth_stk.clear()
        self._pth_stk_heap.clear()
        self._realize_sect = 0
        self._realize_sizes = []
        self._realize_prior = prior

        for size in self._avail_size.keys():
            if self._avail_size[size]:
                self._realize_sizes.append(size)

        self._processing = True
        self._ready = False

        return True

    def _realize_step(self, max_steps):

        if self._realize_sect == 0:
            self._path_find_prepare(self._proc_costs)
            self._realize_sect = 1

        if self._realize_sect == 1:
            c = self._path_find(self._proc_costs, max_steps)
            max_steps -= c[1]

            if c[0]:
                self._path_find_final(self._proc_costs)
                self._realize_sect = 2
            else:
                return False, max_steps

        if self._realize_sect == 2:
            c = self._path_expand(self._proc_costs, max_steps)
            max_steps -= c[1]

            if c[0]:
                self._realize_sect = 3
                self._proc_costs.update_costs_prepare(self._place_targets, clear=True)
                self._proc_costs.update_costs_prepare([pl for c in self._targetables.keys() for pl in c.get_places_around()], clear=False)
            else:
                return False, max_steps

        if self._realize_sect == 3:
            c = self._proc_costs.update_costs(self._use_flag, max_steps)
            max_steps -= c[1]

            if c[0]:
                #size = self._realize_sizes[0]
                #self._proc_costs.convolution_prepare(1 if size == 2 else 0)
                #self._realize_sect = 4
                pass

            else:
                return False, max_steps

        """
        if self._realize_sect == 4:
            c = self._proc_costs.convolution(max_steps)
            max_steps -= c[1]

            if c[0]:
                self._realize_sect = 5
                self._proc_costs.convolution_finish()
                del self._realize_sizes[0]

                if len(self._realize_sizes) > 0:
                    size = self._realize_sizes[0]
                    self._proc_costs.convolution_prepare(1 if size == 2 else 0)

            else:
                return False, max_steps

        if self._realize_sect == 5 and len(self._realize_sizes) > 0:
            c = self._proc_costs.convolution(max_steps)
            max_steps -= c[1]

            if c[0]:
                self._realize_sect = 6
                self._proc_costs.convolution_finish()
                del self._realize_sizes[0]

            else:
                return False, max_steps
        """

        return True, max_steps

    def _realize_finish(self):
        if self._costs is not None:
            self._costs.release()
            self._costs = None

        self._costs = self._proc_costs
        self._proc_costs = None

        self.update_counts()
        self.update_score_all()

        self._realize_prior = False
        self._processing = False
        self._ready = True

        for ent in self._entities:
            ent.clear_timeout()

        if self._do_draw_preview:
            #PrimitiveCollector().clear()
            self._draw_preview()

    def _path_find_prepare(self, use_costs: PathFlowMap):
        if self._do_draw_preview:
            PrimitiveCollector().clear()
            pass

        self._pth_to_set = OrderedSet()
        self._pth_stk_heap.clear()
        self._pth_group_prep = {}

        for ent in self._entities:
            ent.prepare(self)

    def _path_find(self, use_costs: PathFlowMap, max_steps):
        cdef long steps = 0, number, c_flag, cost
        cdef long to_x = 0
        cdef long to_y = 0
        cdef object place, mm
        cdef long t_len = 0

        for place in self._place_targets:
            to_x += place.get_pos_x()
            to_y += place.get_pos_y()
            t_len += 1

        for mm in self._targetables.keys():
            if mm.get_is_alive():
                to_x += mm.get_place().get_pos_x()
                to_y += mm.get_place().get_pos_y()
                t_len += 1

        cdef size_t a_ptr = use_costs.get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        cdef object sub_place
        cdef object sub_sides
        cdef int is_any
        cdef int is_finish

        if t_len == 0:
            return True, steps

        to_x = to_x // t_len
        to_y = to_y // t_len

        for group_key in self._goto_group.get_group_keys():

            is_any = False
            is_finish = False
            path_steps = 0

            if group_key in self._pth_group_prep:
                # jiz zpracovavana skupina
                is_any = self._pth_group_prep[group_key]['is_any']
                is_finish = self._pth_group_prep[group_key]['is_finish']
                path_steps = self._pth_group_prep[group_key]['path_steps']

            else:
                # priprava nove skupiny ke zpracovani
                self._pth_stk_heap.clear()
                self._pth_to_set.clear()

                [self._pth_to_set.add(place) for place in self._place_targets if place not in self._place_targets_used]
                [self._pth_to_set.add(place) for c in self._targetables.keys() for place in c.get_places_around()]

                for mref in self._goto_group.get_group_moveables(group_key):
                    ent = self._entities_hash[mref.number]

                    if ent.get_mref().get_is_alive():
                        # !! and ent.get_mref().get_is_moving(): !! blbost - vlozit i kdyz jsou stopped
                        place = ent.get_mref().get_place()
                        place.back_steps = 0
                        place.cost = 0
                        place.back_ref = None
                        a[place.number].flag = self._use_flag

                        self._pth_stk_heap.push(place.dist, place)
                        is_any = True

                if is_any:
                    self._use_flag += 1

                path_steps = (self._tile_col.get_tiles_width() * self._tile_col.get_tiles_height() * 2)
                self._pth_group_prep[group_key] = {'is_any': is_any, 'is_finish': False, 'use_flag': self._use_flag, 'path_steps': path_steps}

            if is_any and not is_finish:
                # nalez cesty

                while self._pth_stk_heap and self._pth_to_set and steps < max_steps and path_steps > 0:
                    place = self._pth_stk_heap.pop_ref()
                    c_flag = a[place.number].flag
                    steps += 1
                    path_steps -= 1

                    if self._do_draw_preview:
                        PrimitiveCollector().add_circle(place.get_x(), place.get_y(), 5)

                    for sb in place.get_neighbors_with_cost():
                        sub_place = sb[1]
                        sub_sides = sb[2]
                        sub_place.dist = ( abs(to_x - sub_place.get_pos_x()) + abs(to_y - sub_place.get_pos_y()) ) * 10
                        cost = sb[0]

                        if sub_place in self._block_places or next((True for sp in sub_sides if not sp.is_accessible_no_build_only() or sp in self._block_places), False):
                            if sub_place in self._pth_to_set:
                                self._pth_to_set.remove(sub_place)

                        else:
                            if sub_place.is_accessible_goto_group_flw(self._goto_group):

                                if a[sub_place.number].flag != c_flag:
                                    sub_place.back_ref = None

                                if sub_place.back_ref is None or place.back_steps + cost <= sub_place.back_steps:
                                    sub_place.back_ref = place
                                    sub_place.back_steps = place.back_steps + cost

                                if a[sub_place.number].flag != c_flag:
                                    a[sub_place.number].flag = c_flag
                                    sub_place.cost = sub_place.back_steps + sub_place.dist
                                    self._pth_stk_heap.push(sub_place.dist, sub_place)

                            if sub_place in self._pth_to_set:
                                self._pth_to_set.remove(sub_place)

                self._pth_group_prep[group_key]['path_steps'] = path_steps

                if path_steps == 0:
                    self._realize_finish()
                    return False, steps

                if steps >= max_steps:
                    return False, steps

                # zpetny pruchod cesty

                [self._pth_stk.append(place) for place in self._place_targets if place not in self._place_targets_used]
                [self._pth_stk.append(place) for c in self._targetables.keys() for place in c.get_places_around()]

                while self._pth_stk:
                    place = self._pth_stk.popleft()
                    number = place.number

                    if a[number].cost[0] == COST_NA:
                        a[number].cost[0] = 0
                        a[number].cost[1] = 0
                        a[number].steps = 0
                        use_costs.add_key(number)

                    self._pth_stk_steps.append(place)

                    if place.back_ref is not None:
                        self._pth_stk.append(place.back_ref)
                        place.back_ref = None

                self._pth_group_prep[group_key]['is_finish'] = True

        return steps < max_steps, steps

    def _path_find_final(self, use_costs: PathFlowMap):
        cdef long number
        cdef object place
        cdef size_t a_ptr = use_costs.get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        for ent in self._entities:
            if ent.get_mref().get_is_alive():
                place = ent.get_mref().get_place()
                number = place.number

                if a[number].cost[0] == COST_NA:
                    a[number].cost[0] = 0
                    a[number].cost[1] = 0
                    a[number].steps = 0
                    use_costs.add_key(number)
                    self._pth_stk_steps.append(place)

        self._use_flag += 1

    def _path_expand(self, use_costs: PathFlowMap, max_steps):
        cdef size_t a_ptr = use_costs.get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        cdef long number, sub_number, steps, cur_steps, sub_cost
        cdef int use_mass_size = self._mass_size * 15
        cdef object sub_place
        cdef object place

        steps = 0

        # expanze cesty

        while self._pth_stk_steps and steps < max_steps:
            place = self._pth_stk_steps.popleft()
            number = place.number
            steps += 1

            cur_steps = a[number].steps

            for c in place.get_neighbors_with_cost():
                sub_cost = c[0]
                sub_place = c[1]
                sub_number = sub_place.number

                if a[sub_number].cost[0] == COST_NA and cur_steps + sub_cost < use_mass_size and sub_place.is_accessible_goto_group_flw(self._goto_group):

                    a[sub_number].cost[0] = 0
                    a[sub_number].cost[1] = 0
                    a[sub_number].steps = cur_steps + sub_cost
                    use_costs.add_key(sub_number)
                    self._pth_stk_steps.append(sub_place)

        for pl in self._block_places:
            number = pl.number
            a[number].cost[0] = COST_NA
            a[number].cost[1] = COST_NA

        return steps < max_steps, steps

    def _draw_preview(self):
        #PrimitiveCollector().clear()

        if self._costs is None:
            return False

        cdef size_t a_ptr = self._costs.get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        # size2 = 0, size1 = 1
        draw_slot = self._do_draw_preview - 1

        for place in self._tile_col.get_places():
            if a[place.number].cost[draw_slot] != COST_NA and a[place.number].finish == FINISH_USED:
                PrimitiveCollector().add_circle(place.get_x(), place.get_y(), 4, color=(255, 255, 255))

            elif a[place.number].cost[draw_slot] != COST_NA:

                if a[place.number].finish == FINISH_TRAVEL:
                    PrimitiveCollector().add_circle(place.get_x(), place.get_y(), 4, color=(0, 0, 255))

                elif a[place.number].finish == FINISH_TEMP:
                    PrimitiveCollector().add_circle(place.get_x(), place.get_y(), 4, color=(0, 255, 255))

                if a[place.number].cost[draw_slot] > 0:
                    step_place = None
                    step_place_cost = 0
                    step_place_fc = 0

                    for c in place.get_neighbors_with_cost():

                        sub_cost = c[0]
                        sub_place = c[1]

                        if a[sub_place.number].cost[draw_slot] != COST_NA and a[sub_place.number].finish != FINISH_USED:
                            test_cost = a[sub_place.number].cost[draw_slot]

                            if step_place is None or test_cost < step_place_cost or (sub_cost < step_place_fc):
                                step_place = sub_place
                                step_place_cost = test_cost
                                step_place_fc = sub_cost

                    if step_place is not None and (step_place_cost < 15 or True):
                        #print(self._costs.a[place.number][PathFlowMap.SLOT_VECTOR])
                        #vec = self._costs.a[place.number][PathFlowMap.SLOT_VECTOR]

                        #if vec is not None:
                            #PrimitiveCollector().add_line(place.get_x(), place.get_y(), place.get_x() + vec[0] * 15, place.get_y() + vec[1] * 15, color=(0, 255, 0))
                            #PrimitiveCollector().add_circle(place.get_x(), place.get_y(), 2)

                        PrimitiveCollector().add_line(place.get_x(), place.get_y(), step_place.get_x(), step_place.get_y())
                        #PrimitiveCollector().add_text(place.get_x(), place.get_y(), str(round(a[place.number].cost[draw_slot], 2)))

    def update_targets(self):
        cdef size_t a_ptr = self._costs.get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr
        cdef long number, sub_number, use_flag, wg_x, wg_y, dist, dist_min, test_number
        cdef long step_place_cost, step_place_fc
        cdef object cur
        cdef object sub_place

        if len(self._place_targets) == 0:
            return False

        stk = deque()
        use_flag = self._tile_col.get_find_flag()

        wg_x = 0
        wg_y = 0

        for pl in self._place_targets:
            pl.flag = use_flag
            stk.append(pl)

            wg_x += pl.get_pos_x()
            wg_y += pl.get_pos_y()

        for pl in [pl for c in self._targetables.keys() for pl in c.get_places_around()]:
            stk.append(pl)

        wg_x = wg_x // len(self._place_targets)
        wg_y = wg_y // len(self._place_targets)

        dist_min = max(int_sqrt(len(self._place_targets_used)), 1)

        while stk:
            cur = stk.popleft()

            cur.flag = use_flag
            number = cur.number

            if a[number].finish != FINISH_USED:
                step_place = None
                step_place_cost = 0
                step_place_fc = 0

                for sbc in cur.get_neighbors_with_cost():

                    sub_cost = sbc[0]
                    sub_place = sbc[1]
                    sub_number = sub_place.number

                    if a[sub_number].cost[0] != COST_NA:
                        test_cost = a[sub_number].cost[0]

                        if step_place is None:
                            step_place = sub_place
                            step_place_cost = test_cost
                            step_place_fc = sub_cost

                        elif test_cost < step_place_cost or (test_cost == step_place_cost and sub_cost < step_place_fc):
                            step_place = sub_place
                            step_place_cost = test_cost
                            step_place_fc = sub_cost

                if step_place is not None and a[step_place.number].finish == FINISH_USED:
                    dist = cur.dist_to_pos_coord(wg_x, wg_y)

                    #print("DIST: " + str(dist) + " | " + str(dist_min))

                    if dist <= dist_min:
                        a[number].finish = FINISH_TRAVEL
                        self.add_place_target(cur)

            if a[number].finish == FINISH_USED:
                for nb in cur.get_neighbors():
                    sub_number = nb.number

                    if a[sub_number].cost[0] != COST_NA:
                        if nb.flag != use_flag:
                            nb.flag = use_flag
                            stk.append(nb)

        self._tile_col.move_find_flag()
        self.update_counts()

        if self._do_draw_preview:
            PrimitiveCollector().clear()
            self._draw_preview()

    def update_counts(self):
        self._count_finish = 0
        self._count_moving = 0

        for ent in self._entities:
            if ent.get_finish():
                self._count_finish += 1
            else:
                self._count_moving += 1

    def get_is_finish(self, mref):
        if mref.number not in self._entities_hash:
            return False

        ent = self._entities_hash[mref.number]

        if ent.get_finish():
            return True

        if ent.test_finish():
            return True

        return False

    def update_score_all(self):
        [ent.update_score() for ent in self._entities]

    def map_focus_place(self, mref, focus_place, before_place):
        if mref.number in self._entities_hash:
            ent = self._entities_hash[mref.number]
            ent.map_focus_place(focus_place, before_place)

    def get_focus_place(self, mref):
        if mref.number not in self._entities_hash:
            return None

        ent = self._entities_hash[mref.number]

        if self.get_is_finish(mref):
            return None

        return ent.get_focus_place()

    def get_focus_place_next(self, mref):
        return None

    def pop_stacked_entities(self):
        ents = sorted((ent for ent in self._entities if ent.get_stacked_count() > 3 and ent.get_stacked_timeout() > 10), key=lambda ent: ent.get_stacked_count(), reverse=True)

        if len(ents) > 0:
            return ents[0].get_stacked_all(with_self=True)
        else:
            return None

    def update_realize(self, max_steps):
        if not self._ready:
            c = self._realize_step(max_steps)

            if c[0]:
                self._realize_finish()

            return c[1]
        else:
            return max_steps

    def update(self, tick):
        cdef size_t a_ptr
        cdef PathFlowMapField * a

        if self._ready:
            if tick % 10 == 0:
                rem = []

                for place in self._block_places:
                    if place.is_accessible_goto_group(self._goto_group):
                        rem.append(place)

                if len(rem) > 0:
                    for place in rem:
                        self._block_places.remove(place)

            if tick % 60 == 0:
                if self._realize_force:
                    if self._realize_type == PathFinderFlow.REALIZE_ATTACK:

                        if self._realize_rel_object is not None and self._realize_rel_object.get_is_alive():
                            # !! self.clear_finish()
                            # !! self.invalidate_finish()

                            if self._realize_force == 2:
                                self.prepare(None)
                                self.realize(PathFinderFlow.REALIZE_ATTACK, self._realize_rel_object, True)
                            else:
                                self.realize()

                            for ent in self._entities:
                                mref = ent.get_mref()
                                mref.get_target_entity().scan_targets()
                                mref.get_path().goto(self.get_place_to(), path_flow=self, check_target=True)

                    elif self._realize_type == PathFinderFlow.REALIZE_ATTACK_MOVE:
                        # !! self.clear_finish()
                        # !! self.invalidate_finish()

                        if self._realize_force == 2:
                            self.prepare(self._realize_rel_object)
                            self.realize(PathFinderFlow.REALIZE_ATTACK_MOVE, self._realize_rel_object, True)
                        else:
                            self.realize()

                        for ent in self._entities:
                            mref = ent.get_mref()
                            mref.get_target_entity().scan_targets()
                            mref.get_path().goto(self.get_place_to(), path_flow=self, check_target=True)

                    elif self._realize_type == PathFinderFlow.REALIZE_GOTO:

                        if self._realize_force == 2:
                            self.prepare(self._realize_rel_object)
                            self.realize(PathFinderFlow.REALIZE_GOTO, self._realize_rel_object, True)
                        else:
                            self.realize()

                    self._realize_force = 0

                else:
                    ents = sorted((ent for ent in self._entities if ent.get_stacked_count() > 3 or ent.get_stacked_timeout() > 10), key=lambda ent: ent.get_stacked_count(), reverse=True)

                    if len(ents) > 0:
                        self.update_targets()
                        self.realize()
