
class MapTileOrganize:

    _type = None
    _mtx = None
    _nb = None
    _type_blank = None
    _type_fill = None

    def __init__(self, type, mtx):
        self._type = type
        self._mtx = mtx
        self._nb = [
            ["000.000.000", 13],
            ["111.111.111", 4],

            # ["*0*.111.*1*", 1],
            # ["*1*.111.*0*", 7],
            # ["*1*.011.*1*", 3],
            # ["*1*.110.*1*", 5],

            ["011.111.110", 17],
            ["110.111.011", 16],

            ["*0*.111.111", 1], ["0*0.111.111", 1],
            ["111.111.*0*", 7], ["111.111.0*0", 7],
            ["*11.011.*11", 3], ["011.*11.011", 3],
            ["11*.110.11*", 5], ["110.11*.110", 5],

            ["*0*.011.*11", 0],
            ["11*.110.*0*", 8],
            ["*11.011.*0*", 6],
            ["*0*.110.11*", 2],

            ["011.111.111", 12],
            ["110.111.111", 11],
            ["111.111.011", 10],
            ["111.111.110", 9],

            ["000.011.111", 0],
            ["000.110.111", 2],
            ["111.110.000", 8],
            ["111.011.000", 6],

            ["***.*11.*11", 0],
            ["*11.*11.***", 6],
            ["***.11*.11*", 2],
            ["11*.11*.***", 8]

            # ["001.011.011", 0],
            # ["011.011.001", 6],
            # ["100.110.110", 2],
            # ["110.110.100", 8]
        ]

        if self._type == 1:
            self._type_fill = [1, 2, 3]
            self._type_blank = [0]

        elif self._type == 2:
            self._type_fill = [2]
            self._type_blank = [0, 1, 3]

        else:
            self._nb = []

    def organize(self):
        organize_key = -1

        for c in self._nb:
            lines = c[0].split('.')

            if lines[1][1] == '1':
                match = True
                fy = 0
                fx = 0

                while fy <= 2 and match:
                    if lines[fy][fx] == '*':
                        # muze byt cokoliv
                        pass
                    elif self._mtx[fy][fx] is None:
                        # neni soused - muze byt cokoliv
                        pass
                    elif lines[fy][fx] == '0':
                        match = self._mtx[fy][fx].get_type() in self._type_blank
                    elif lines[fy][fx] == '1':
                        match = self._mtx[fy][fx].get_type() in self._type_fill

                    fx += 1
                    if fx > 2:
                        fx = 0
                        fy += 1

                if match:
                    organize_key = c[1]
                    break

        return organize_key
