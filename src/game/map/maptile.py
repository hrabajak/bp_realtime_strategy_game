import math
import random

from game.config import Config
from game.const.specialflags import SpecialFlags
from game.graphic.envrect import EnvRect
from game.graphic.graphbatches import GraphBatches
from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.discretemath import int_sqrt
from game.image.imagecollector import ImageCollector
from game.interface.iplayersides import IPlayerSides
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector
from game.map.mapscanner import MapScanner
from game.map.maptileorganize import MapTileOrganize
from game.map.maptiletexture import MapTileTexture


class MapTile:

    _psides: IPlayerSides = None
    _play_sides = None
    _tile_col: ITileCollector = None
    _map_scr: MapScanner = None

    _x = 0
    _y = 0
    _img_key = None
    _img_var = 0
    _img = None
    _env_item = None
    _overlay_img_key = None
    _overlay_opacity = 0
    _overlay_flag = 0
    _sprite_overlay = None
    _visible = False
    _type = 0
    _seed = 0
    _resources = 0
    _accessible = False
    _blocked = False
    _special = 0
    _special_data = None
    _tex = None
    _left = None
    _right = None
    _top = None
    _bottom = None
    _places = None
    _moveables = None
    _build_ref = None
    _moveable_focus = None
    _targetable_sight = None
    _fog = None
    _fog_count = 0
    _fog_visible = False
    _fog_visible_nbr = False
    _fog_type = 0
    _fog_koef = 0

    _nbr = None
    _nbr_with_cost = None
    _nbr_with_cost_hash = None
    _nbr_around = None
    _nbr_plus = None
    _nbr_x = None

    _edges = None

    _scan_cache = None
    _scan_cache_back_steps = None
    _occupy = None

    number = 0
    flag = 0
    area_id = 0
    back_ref = None
    back_steps = 0
    b_back_steps = 0
    df_back_steps = 0
    df_index = 0
    gen_flag = 0
    test_flag = 0

    rnd_pts = None

    pt_idx = 0
    col_disp_idx = 0
    black_col_idx = 0
    black_op_idx = 0
    snap_color = None

    def __init__(self, x, y, map_scr: MapScanner):
        self._tile_col = ITileCollector.get()
        self._map_scr = map_scr

        self._type = 0
        self._seed = random.randint(0, 999999)
        self._x = x
        self._y = y
        self._places = []
        self._moveables = []
        self._build_ref = None
        self._targetable_sight = set()
        self._sprite_overlay = None
        self._overlay_img_key = None

        self._fog_visible = not self._tile_col.get_fog_enabled()
        self._fog_visible_nbr = not self._tile_col.get_fog_enabled()
        self._fog = {}
        self._edges = [None, None, None, None]
        self._tex = MapTileTexture(self)

    def get_visible(self):
        return self._visible and self._fog_visible_nbr

    def set_visible(self, visible):
        if self._visible != visible:
            self._visible = visible

            if self._visible and self._env_item is None:

                if self._type in [MapTileTexture.T_TREE, MapTileTexture.T_DEEP_TREE, MapTileTexture.T_WATER_TREE]:
                    tp = EnvRect.TYPE_TREE

                    if self._type == MapTileTexture.T_DEEP_TREE:
                        tp = EnvRect.TYPE_DEEP_TREE

                    self._env_item = GraphBatches().get_envs().create(tp, self._seed)
                    self._env_item.set_pos(self.get_x(), self.get_y())
                    self._env_item.create_sub()

            elif not self._visible and self._env_item is not None:
                self._env_item.discard()
                self._env_item = None

    def get_type(self):
        return self._type

    def set_type(self, type):
        self._tex.set_type(type)
        self._type = type
        self._touch_accessible()
        [p.update() for p in self._places]

    def set_type_update(self, type):
        self.set_type(type)
        self.build()
        [tl.build() for tl in self.get_neighbors()]

        if self._tile_col.get_garea() is not None:
            self._tile_col.get_garea().force_update()

    def set_blocked(self, blocked):
        self._blocked = blocked
        self._touch_accessible()
        [p.update() for p in self._places]

    def set_seed(self, seed=-1):
        self._seed = random.randint(0, 999999) if seed == -1 else seed

    def _touch_accessible(self):
        self._accessible = self._type in [MapTileTexture.T_GRASS, MapTileTexture.T_WATERGRASS, MapTileTexture.T_CONCGROUND, MapTileTexture.T_FIELD, MapTileTexture.T_DUST] and not self._blocked

    def get_tex(self):
        return self._tex

    def get_accessible(self):
        return self._accessible

    def get_fog_koef(self):
        return self._fog_koef

    def get_fog_visible(self):
        return self._fog_visible

    def get_fog_type(self):
        return self._fog_type

    def get_fog_side(self, side):
        return self._fog[side]

    def get_resources(self):
        return self._resources

    def mine_resources(self, value):
        if value > self._resources:
            value = self._resources

        self._resources -= value

        if self._resources == 0:
            self.set_type_update(MapTileTexture.T_GRASS)

        return value

    def get_special(self):
        return self._special

    def get_special_data(self):
        return self._special_data

    def get_img_key(self):
        return self._img_key

    def get_img_var(self):
        return self._img_var

    def get_img(self):
        if self._img is None:
            self._img = self._tex.get_image()
        return self._img

    def set_special(self, special, data=None):
        self._special = special
        self._special_data = data
        self._touch_accessible()
        [p.update() for p in self._places]

    def set_dist(self, dist):
        self.dist = dist
        return dist

    def add_place(self, place):
        self._places.append(place)

    def set_gen_flag(self, gen_flag):
        self.gen_flag = gen_flag

    def serialize(self):
        out = {
            'x': self._x,
            'y': self._y,
            'type': self._type,
            'seed': self._seed
        }

        return out

    def update_sides(self, psides: IPlayerSides):
        self._psides = psides
        self._play_sides = [] # psides.get_playable_sides()

        if len(self._play_sides) == 0 and psides.get_focus_side(True) is not None:
            self._play_sides = [psides.get_focus_side(True)]

        for side in psides.get_sides():
            if side not in self._fog:
                self._fog[side] = False

            if side not in self._play_sides:
                self._fog[side] = False

        self.fog_update()

    def destroy(self):
        if self._env_item is not None:
            self._env_item.discard()
            self._env_item = None

        self._left = None
        self._right = None
        self._top = None
        self._bottom = None
        self._places = None
        self._moveables = None
        self._build_ref = None
        self._moveable_focus = None
        self._targetable_sight = None
        self._fog = None

        self._nbr = None
        self._nbr_with_cost = None
        self._nbr_with_cost_hash = None
        self._nbr_around = None
        self._nbr_plus = None
        self._nbr_x = None

        self._edges = None

        self._scan_cache = None
        self._scan_cache_back_steps = None
        self._occupy = None

    def build(self):
        self._tex.set_type(self._type)
        self._img_key = None
        self._img_var = 0
        self._img = None

        if self._type == MapTileTexture.T_FIELD:
            self._resources = 1750

        [pl.update_acc() for pl in self._places]

    def get_x(self):
        return self._x * 50 + 50 // 2

    def get_y(self):
        return self._y * 50 + 50 // 2

    def get_pos_x(self):
        return self._x

    def get_pos_y(self):
        return self._y

    def get_place(self, idx=2):
        return self._places[idx]

    def get_places(self):
        return self._places

    def get_left(self):
        return self._left

    def get_right(self):
        return self._right

    def get_top(self):
        return self._top

    def get_bottom(self):
        return self._bottom

    def get_sprite_x(self):
        return self._x * self._tile_col.get_cur_tilesize() + self._tile_col.get_x() + self._tile_col.get_cur_tilesize() / 2

    def get_sprite_y(self):
        return self._y * self._tile_col.get_cur_tilesize() + self._tile_col.get_y() + self._tile_col.get_cur_tilesize() / 2

    def get_tile_add_xy(self, x, y):
        loop = self

        if x > 0:
            while x > 0 and loop is not None:
                loop = loop.get_right()
                x -= 1
        else:
            while x < 0 and loop is not None:
                loop = loop.get_left()
                x += 1

        if y > 0:
            while y > 0 and loop is not None:
                loop = loop.get_top()
                y -= 1
        else:
            while y < 0 and loop is not None:
                loop = loop.get_bottom()
                y += 1

        return loop

    def get_accessible_place(self, moveable_ref):
        return next((place for place in self._places if place.is_accessible_no_build_only(moveable_ref)), None)

    def dist_to(self, tile):
        return int_sqrt((self._x - tile._x) ** 2 + (self._y - tile._y) ** 2)

    def dist_to_coord(self, x, y):
        return int_sqrt((self.get_x() - x) ** 2 + (self.get_y() - y) ** 2)

    def dist_to_coord_pos(self, x, y):
        return int_sqrt((self._x - x) ** 2 + (self._y - y) ** 2)

    def dist_to_moveable(self, ref):
        return int_sqrt((self.get_x() - ref.get_use_x()) ** 2 + (self.get_y() - ref.get_use_y()) ** 2)

    def dist_to_coord_pos_manhattan(self, x, y):
        return abs(self._x - x) + abs(self._y - y)

    def is_accessible_simple(self):
        if not self._accessible:
            return False

        if self._build_ref is not None:
            return False

        return True

    def is_buildable(self):
        if not self._accessible or self._type not in [MapTileTexture.T_GRASS, MapTileTexture.T_DARKGRASS, MapTileTexture.T_DUST]:
            return False

        if self._build_ref is not None or len(self._moveables) > 0:
            return False

        return True

    def is_buildable_safe(self, build_spec):
        if not self._accessible:
            return False

        if self._build_ref is not None and self._build_ref.get_spec().get_key() not in build_spec.get_allow_nbr():
            return False

        return True

    def moveable_map(self, ref):
        self._moveables.append(ref)

        if self._occupy is not None:
            for ai in self._occupy.keys():
                self._occupy[ai]["entry_count"] += 1
                ai.moveable_occupy_enter(self, ref)

    def moveable_rem(self, ref):
        self._moveables.remove(ref)

        if self._occupy is not None:
            rem = []

            for ai in self._occupy.keys():
                ai.moveable_occupy_leave(self, ref)

                if self._occupy[ai]["temporary"]:
                    rem.append(ai)

            if len(rem) > 0:
                for ai in rem:
                    del self._occupy[ai]

    def targetable_map(self, ref: ITargetable):
        if ref not in self._targetable_sight:
            self._targetable_sight.add(ref)
            self.fog_update()

    def targetable_rem(self, ref: ITargetable):
        if ref in self._targetable_sight:
            self._targetable_sight.remove(ref)
            self.fog_update()

    def occupy_add(self, ai, steps, temporary=False):
        if self._occupy is None:
            self._occupy = {}

        self._occupy[ai] = {
            "back_steps": steps,
            "temporary": temporary,
            "entry_count": 0
        }

    def is_occupied_by(self, ai):
        return self._occupy is not None and ai in self._occupy

    def is_edge(self):
        return self._x == 0 or self._y == 0 or self._x == self._tile_col.get_tiles_width() - 1 or self._y == self._tile_col.get_tiles_height() - 1

    def fog_update(self):
        sides = set()

        for trg in self._targetable_sight:
            if trg.get_side() not in sides:
                sides.add(trg.get_side())
                
        for side in self._fog.keys():
            if side in sides and not self._fog[side]:
                self._fog[side] = True
                self._fog_count += 1
                self._tile_col.fog_tile_add(self)

            elif side not in sides and self._fog[side]:
                self._fog[side] = False
                self._fog_count -= 1
                self._tile_col.fog_tile_add(self)

    def fog_process(self):
        if next((True for side in self._fog.keys() if self._fog[side] and side in self._play_sides), False) or (self._tile_col.get_fog_type() == Config.FOG_ALL and self._fog_count > 0) or self._tile_col.get_fog_type() == Config.FOG_NONE:
            # viditelne

            self._fog_koef = min(self._fog_koef + 0.5, 1.0)

            if not self._fog_visible:
                self._fog_visible = True
                return 1

        else:
            # neviditelne

            self._fog_koef = max(self._fog_koef - 0.05, 0)

            if self._fog_koef == 0:
                self._fog_visible = False
                return 2

        return 0

    def fog_sec_process(self):
        if self._fog_visible:
            self._fog_visible_nbr = True

        else:
            self._fog_visible_nbr = False

            for fy in range(-2, 3):
                for fx in range(-2, 3):
                    if fx != 0 or fy != 0:
                        tl = self._tile_col.get_tile(self.get_pos_x() + fx, self.get_pos_y() + fy)

                        if tl is not None and tl.get_fog_visible():
                            self._fog_visible_nbr = True
                            break

    def fog_after_process(self, level=0):
        out = []

        for nb in [c for c in (self.get_neighbors() + [self])]:
            if level == 0:
                tp = 0

                if not nb.get_fog_visible():
                    for subc in nb.get_neighbors():
                        if subc.get_fog_visible():
                            tp = 1

                if tp != 0 or nb._fog_type != tp:
                    nb._fog_type = tp
                    out.append(nb)

            elif level == 1 and nb.get_fog_type() != 1:
                tp = 0

                if not nb.get_fog_visible():
                    for subc in nb.get_neighbors():
                        if not subc.get_fog_visible() and subc.get_fog_type() == 1:
                            tp = 2

                if tp != 0 or nb._fog_type != tp:
                    nb._fog_type = tp
                    out.append(nb)

        return out

    def set_moveable_focus(self, ref):
        self._moveable_focus = ref

    def get_moveable_focus(self):
        return self._moveable_focus

    def check_moveable_focus(self, ref):
        return self._moveable_focus is None or self._moveable_focus is ref

    def build_map(self, ref):
        self._build_ref = ref

    def build_rem(self, ref):
        self._build_ref = None

    def get_moveables(self):
        return self._moveables

    def get_build_ref(self):
        return self._build_ref

    def get_is_build_except(self, exc):
        return self._build_ref is not None and self._build_ref is not exc

    def is_moveable_spec(self, ref, spec):
        return next((False for p in self._places if not p.is_moveable_spec(ref, spec)), True)

    def is_accessible(self, moveable_ref):
        return next((False for p in self._places if not p.is_accessible(moveable_ref)), True)

    def is_accessible_no_build_only(self):
        return next((False for p in self._places if not p.is_accessible_no_build_only()), True)

    def is_accessible_tolerance(self, moveable_ref, tolerance=250):
        return next((False for p in self._places if not p.is_accessible_tolerance(moveable_ref, tolerance)), True)

    def get_rect_area(self, width, height):
        out = []
        loop_y = self

        for py in range(0, height):
            loop_x = loop_y

            for px in range(0, width):
                out += [loop_x]
                loop_x = loop_x.get_right()
                if loop_x is None:
                    break

            loop_y = loop_y.get_bottom()
            if loop_y is None:
                break

        return out

    def set_overlay(self, img_key, opacity=255):
        if img_key is None:
            self._overlay_img_key = None
            self._overlay_flag = 0

        else:
            self._overlay_img_key = img_key
            self._overlay_opacity = opacity
            self._overlay_flag = 2

            self._tile_col.overlay_tile_add(self)

    def get_overlay_flag(self):
        return self._overlay_flag

    def all_neighbor_are(self, check_callback):
        for tile in self.get_neighbors():
            if not check_callback(tile):
                return False

        return True

    def get_neighbor(self, off_x, off_y):
        return self._tile_col.get_place(self._x + off_x, self._y + off_y)

    def get_neighbors(self):
        return self._nbr

    def get_neighbors_with_cost(self):
        return self._nbr_with_cost

    def get_neighbors_around(self):
        return self._nbr_around

    def get_neighbors_plus(self):
        return self._nbr_plus

    def get_neighbors_x(self):
        return self._nbr_x

    def set_neightbor(self, tile_col: ITileCollector):
        self._left = tile_col.get_tile(self._x - 1, self._y)
        self._right = tile_col.get_tile(self._x + 1, self._y)
        self._top = tile_col.get_tile(self._x, self._y + 1)
        self._bottom = tile_col.get_tile(self._x, self._y - 1)

        self._nbr = []

        for fy in range(-1, 2):
            for fx in range(-1, 2):
                tile = tile_col.get_tile(self._x + fx, self._y + fy)

                if (fy != 0 or fx != 0) and tile is not None:
                    self._nbr.append(tile)

        self._nbr_with_cost = []
        self._nbr_with_cost_hash = {}

        for c in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            tile = tile_col.get_tile(self._x + c[0], self._y + c[1])

            if tile is not None:
                self._nbr_with_cost.append((10, tile, c))
                self._nbr_with_cost_hash[c] = (10, tile)

        for c in [(-1, -1), (1, 1), (-1, 1), (1, -1)]:
            tile = tile_col.get_tile(self._x + c[0], self._y + c[1])

            if tile is not None:
                # !! out.append((1.5, tile))
                self._nbr_with_cost.append((12, tile, c))
                self._nbr_with_cost_hash[c] = (12, tile)

        self._nbr_around = []

        for c in [(-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1)]:
            tile = tile_col.get_tile(self._x + c[0], self._y + c[1])

            if tile is not None:
                self._nbr_around.append(tile)

        self._nbr_plus = []

        for c in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            tile = tile_col.get_tile(self._x + c[0], self._y + c[1])

            if tile is not None:
                self._nbr_plus.append(tile)

        self._nbr_x = []

        for c in [(-1, -1), (1, 1), (-1, 1), (1, -1)]:
            tile = tile_col.get_tile(self._x + c[0], self._y + c[1])

            if tile is not None:
                self._nbr_x.append(tile)

    def get_edges(self):
        return self._edges

    def get_edge(self, idx):
        return self._edges[idx]

    def set_edge(self, idx, edge):
        self._edges[idx] = edge

    def scan_cache_create(self):
        max_steps = 13

        self._scan_cache = []
        self._scan_cache_back_steps = []

        self._map_scr.scan_tiles(self, max_steps, lambda tl, result: MapTile._scan_step(self, tl, result))

    def get_scan_cache(self):
        if self._scan_cache is None:
            self.scan_cache_create()

        return self._scan_cache

    def get_scan_cache_back_steps(self):
        if self._scan_cache_back_steps is None:
            self.scan_cache_create()

        return self._scan_cache_back_steps

    @staticmethod
    def _scan_step(self, tile_loop, result):
        self._scan_cache.append(tile_loop)
        self._scan_cache_back_steps.append(tile_loop.back_steps // 10)

    def is_in_view(self) -> bool:
        x_tile = self._x * self._tile_col.get_cur_tilesize()
        y_tile = self._y * self._tile_col.get_cur_tilesize()

        return \
            x_tile + self._tile_col.get_x() >= - self._tile_col.get_cur_tilesize() and \
            y_tile + self._tile_col.get_y() >= - self._tile_col.get_cur_tilesize() and \
            x_tile + self._tile_col.get_x() <= self._tile_col.get_view_width() and \
            y_tile + self._tile_col.get_y() <= self._tile_col.get_view_height()

    def draw(self):

        if self._overlay_flag > 0:
            if self._sprite_overlay is None:
                self._sprite_overlay = GraphBatches().create_sprite(img=ImageCollector().get_image(self._overlay_img_key), x=self.get_sprite_x(), y=self.get_sprite_y(), batch=GraphBatches().get_batch_black(), subpixel=False)
                self._sprite_overlay.update(scale=self._tile_col.get_zoom())
                self._sprite_overlay.opacity = self._overlay_opacity

            elif self._overlay_flag == 2:
                self._sprite_overlay.image = ImageCollector().get_image(self._overlay_img_key)
                self._sprite_overlay.opacity = self._overlay_opacity
                self._overlay_flag = 1

            self._sprite_overlay.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._tile_col.get_zoom())

        elif self._sprite_overlay is not None:
            GraphBatches().add_sprite_delete(self._sprite_overlay)
            self._overlay_flag = 0
            self._sprite_overlay = None

        """
        if self.test_flag > 0: # or len(self._targetable_sight) > 0):
            self._sprite.color = (100 + self.test_flag, 100 + self.test_flag, 100 + self.test_flag)
        else:
            self._sprite.color = (255, 255, 255)
        """

        """
        if (self._occupy is not None):
            ln = len(self._occupy) * 80
            self._sprite.color = (255 - ln, 255 - ln, 255 - ln)
        """

        """
        if (self._fog is not None and self._fog[self._psides.get_focus_side()]["flag"]):
            self._sprite.color = (255, 255, 255)
        else:
            self._sprite.color = (100 + self.test_flag, 100 + self.test_flag, 100 + self.test_flag)
        """
