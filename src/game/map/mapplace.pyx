from collections import deque

from lib.orderedset import OrderedSet
from game.const.specialflags import SpecialFlags
from game.helper.discretemath cimport int_sqrt
from game.helper.distangle import DistAngle
from game.interface.itilecollector import ITileCollector
from game.map.placeside import PlaceSide
include "mapplacedef.pxi"

cdef class MapPlace:

    cdef:
        object _tile_col
        object _flag

        long _back_steps
        long _back_cost
        long _x
        long _y
        int _type
        int _acc
        int _accessible

        object _acc_ref
        object _acc_moveables

        object _special
        object _tile
        object _left
        object _right
        object _top
        object _bottom
        object _back_for

        object _moveable_ref
        object _moveables
        object _moveables_union
        object _targetables
        object _build_ref
        int _moveable_focus

        object _nbr
        object _nbr_x
        object _nbr_with_cost
        object _nbr_around
        object _nbr_sz

    cdef public:
        long number
        long flag
        int temp_acc
        object back_ref
        long back_steps
        object at_angle
        long cost
        long cost_2
        long cost_inc
        long to_cost
        long to_flag
        long dist
        int test_flag

    def __cinit__(self, x, y, tile):
        self._x = x
        self._y = y
        self._type = 0
        self._tile = tile
        self._tile.add_place(self)
        self._acc = 2
        self._back_for = {}
        self._flag = False
        self._back_steps = 0
        self._back_cost = 0
        self._acc = 0
        self._accessible = False

        self._moveable_focus = 0
        self._acc_moveables = OrderedSet()
        self._moveables = OrderedSet()
        self._moveables_union = OrderedSet()
        self._targetables = OrderedSet()
        self.update()

        self.number = 0
        self.flag = 0
        self.temp_acc = 0
        self.back_ref = None
        self.back_steps = 0
        self.at_angle = None
        self.cost = 0
        self.cost_2 = 0
        self.cost_inc = 0
        self.to_cost = 0
        self.to_flag = 0
        self.dist = 0
        self.test_flag = 0

    def get_tile(self):
        return self._tile

    def get_visible(self):
        return self._tile.get_visible()

    def get_special(self):
        return self._special

    def get_acc(self):
        return self._acc

    def get_acc_count(self):
        return len(self._acc_moveables)

    def get_acc_ref(self):
        return self._acc_ref

    def get_x(self):
        return self._x * 25 + 25 // 2

    def get_y(self):
        return self._y * 25 + 25 // 2

    def get_pos_x(self):
        return self._x

    def get_pos_y(self):
        return self._y

    def get_left(self):
        return self._left

    def get_right(self):
        return self._right

    def get_top(self):
        return self._top

    def get_bottom(self):
        return self._bottom

    def get_place_add_xy(self, x, y):
        return self._tile_col.get_place(self._x + x, self._y + y)

    def get_trg_coords(self, moveable_ref):
        if self._special in [SpecialFlags.SPECIAL_MINER, SpecialFlags.SPECIAL_CREATE, SpecialFlags.SPECIAL_CREATE_MOVE]:
            inc_x = 0
            inc_y = 0

            if self._tile.get_special_data() is not None and "inc_x" in self._tile.get_special_data():
                inc_x = self._tile.get_special_data()["inc_x"]

            if self._tile.get_special_data() is not None and "inc_y" in self._tile.get_special_data():
                inc_x = self._tile.get_special_data()["inc_y"]

            return self.get_x() * DistAngle.ZOOM_VALUE + inc_x, self.get_y() * DistAngle.ZOOM_VALUE + inc_y
        else:
            return self.get_x() * DistAngle.ZOOM_VALUE, self.get_y() * DistAngle.ZOOM_VALUE
            # tady NESMI byt random !!
            #return self.get_x() + (self._x * self._y * 7536 % 4) - 2, self.get_y() + (self._x * self._y * 1793 % 4) - 2

    def dist_to(self, place) -> long:
        return int_sqrt((self._x - place.get_pos_x()) ** 2 + (self._y - place.get_pos_y()) ** 2)

    def dist_to_full(self, place) -> long:
        return int_sqrt((self.get_x() - place.get_x()) ** 2 + (self.get_y() - place.get_y()) ** 2)

    def dist_to_coord(self, x, y) -> long:
        return int_sqrt((self.get_x() - x) ** 2 + (self.get_y() - y) ** 2)

    def dist_to_pos_coord(self, x, y) -> long:
        return int_sqrt((self.get_pos_x() - x) ** 2 + (self.get_pos_y() - y) ** 2)

    def dist_to_moveable(self, ref) -> long:
        return int_sqrt((self.get_x() - ref.get_use_x()) ** 2 + (self.get_y() - ref.get_use_y()) ** 2)

    def is_accessible_simple(self) -> int:
        if not self._accessible:
            return False

        if self._build_ref is not None or self._moveable_ref is not None:
            return False

        return True

    def is_accessible_no_build_only(self, moveable_ref=None) -> int:
        if not self._accessible:
            return False

        if self._build_ref is not None:
            return False

        if moveable_ref is not None and self._acc < moveable_ref.get_spec().get_size() and self._acc_ref is not moveable_ref:
            return False

        return True

    def is_accessible_no_build_only_b(self) -> int:
        if not self._accessible:
            return False

        if self._build_ref is not None:
            return False

        return True

    def is_accessible_goto_group(self, goto_group) -> int:
        if self._moveable_ref is None:
            pass
        elif self._moveable_ref.get_goto_group() is not goto_group:
            return False

        return True

    def is_accessible_goto_group_flw(self, goto_group) -> int:
        if not self._accessible:
            return False

        if self._build_ref is not None:
            return False

        if self._moveable_ref is None:
            pass
        elif self._moveable_ref.get_goto_group() is goto_group and not self._moveable_ref.get_is_moving():
            return False

        return True

    def is_accessible_goto_group_only_b(self, moveable_ref, av_set) -> int:
        if not self._accessible:
            return False

        if self._build_ref is not None:
            return False

        if self._acc < moveable_ref.get_spec().get_size():
            if self._acc_ref is None:
                return False
            elif self._acc_ref.get_goto_group() is moveable_ref.get_goto_group():
                return True
            elif self._acc_ref in av_set:
                return False

        return True

    def is_accessible_size(self, int size) -> int:
        if not self._accessible:
            return False

        if self._build_ref is not None or self._moveable_ref is not None:
            return False

        if self._acc < size:
            return False

        return True

    def is_accessible(self, moveable_ref) -> int:
        if not self._accessible:
            return False

        if self._special in moveable_ref.get_spec().get_specials():
            #return True # !!
            pass
        elif self._build_ref is not None:
            return False

        if self._acc < moveable_ref.get_spec().get_size() and self._acc_ref is not moveable_ref:
            return False

        if self._moveable_ref is None:
            pass
        elif self._moveable_ref is not moveable_ref:
            return False

        return True

    def is_accessible_tolerance(self, moveable_ref, int tolerance=250) -> int:
        if not self._accessible:
            return False

        if self._special in moveable_ref.get_spec().get_specials():
            #return True # !!
            pass
        elif self._build_ref is not None:
            return False
        
        if tolerance == 0:
            if len(self._acc_moveables) == 0 is None and self._acc < moveable_ref.get_spec().get_size():
                return False

            return True

        if self._acc < moveable_ref.get_spec().get_size():
            if self._acc_ref is None:
                return False

            elif moveable_ref is self._acc_ref:
                pass

            elif self._acc_ref.get_goto_group() == moveable_ref.get_goto_group() and self._acc_ref.get_is_moving() and moveable_ref.get_max_speed() - self._acc_ref.get_max_speed() <= 0.05:
                pass

            else:
                return False

                """
                for mref in self._acc_moveables:
                    if mref.get_goto_group() == moveable_ref.get_goto_group() and mref.get_is_moving() and moveable_ref.get_max_speed() - mref.get_max_speed() <= 0.05:

                        #if mref.get_focus_place() is self:
                            # jednotka sem uz jede
                            #return False

                        pass

                    elif mref is not moveable_ref:
                        #return False

                        tol = tolerance if mref.get_is_moving() else tolerance
                        if mref.dist_to_moveable(moveable_ref) < tol:
                            return False
                """

        if self._moveable_ref is None:
            pass

        elif self._moveable_ref.get_goto_group() == moveable_ref.get_goto_group() and self._moveable_ref.get_is_moving() and moveable_ref.get_max_speed() - self._moveable_ref.get_max_speed() <= 0.05:
            pass

        elif self._moveable_ref is not moveable_ref:
            tol = tolerance if self._moveable_ref.get_is_moving() else tolerance
            if self._moveable_ref.dist_to_moveable(moveable_ref) < tol:
                return False

        return True

    def moveable_map(self, ref):
        self._moveables.add(ref)
        self._moveables_union.add(ref)
        self._tile.moveable_map(ref)
        self._targetables.add(ref)
        self._moveable_ref = ref

    def moveable_rem(self, ref):
        self._moveables.remove(ref)
        self._moveables_union.remove(ref)
        self._tile.moveable_rem(ref)
        self._targetables.remove(ref)
        self._moveable_ref = None

    def build_map(self, ref):
        self._tile.build_map(ref)
        self._targetables.add(ref)
        self._build_ref = ref

    def build_rem(self, ref):
        self._tile.build_rem(ref)
        self._targetables.remove(ref)
        self._build_ref = None

    def get_accessible(self):
        return self._accessible

    def get_moveables(self):
        return self._moveables

    def get_moveable_ref(self):
        return self._moveable_ref

    def get_build_ref(self):
        return self._build_ref

    def get_targetables(self):
        return self._targetables

    def get_moveable_focus(self):
        return self._moveable_focus

    def add_moveable_focus(self, int value):
        self._moveable_focus += value

    def unset_moveable_focus(self, ref):
        for c in self._nbr_around[ref.get_spec().get_size()]:
            c.add_moveable_focus(-1)

    def set_moveable_focus(self, ref):
        for c in self._nbr_around[ref.get_spec().get_size()]:
            c.add_moveable_focus(1)

    def get_moveables_size(self, size=1):
        return self._moveables_union if size > self._acc else self._moveables

    def is_moveable_slower(self, ref) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and mm.get_is_moving() and ref.get_max_speed() - mm.get_max_speed() > 0.05:
                return True
        return False

    def is_moveable_attacking(self, ref) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and mm.get_is_attacking():
                return True
        return False

    def is_moveable_goto_group(self, ref, goto_group) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and mm.get_is_moving() and not mm.get_is_attacking() and mm.get_goto_group() is goto_group:
                return True
        return False

    def is_moveable_special(self, ref, special_flag) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and special_flag in mm.get_spec().get_specials():
                return True
        return False

    def is_moveable_standing(self, ref, side=None) -> int:
        for mref in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if not mref.get_is_moving() and (side is None or mref.get_side() is side):
                return True
        return False

    def is_moveable_standing_stuck(self, ref, side=None) -> int:
        for mref in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if (not mref.get_is_moving() or mref.get_is_stuck()) and (side is None or mref.get_side() is side):
                return True
        return False

    def is_moveable_stuck(self, ref) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and ref.get_is_stuck():
                return True
        return False

    def is_moveable_spec(self, ref, spec) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and ref.get_spec() is spec:
                return True
        return False

    def is_moveable_any(self, ref) -> int:
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref:
                return True
        return False

    def get_moveable_goto_group_simple(self, ref, goto_group):
        for mm in (self._moveables_union if ref.get_spec().get_size() > self._acc else self._moveables):
            if mm is not ref and mm.get_goto_group() is goto_group:
                return mm
        return None

    def destroy(self):
        self._acc_ref = None
        self._acc_moveables = None
        self._tile = None
        self._left = None
        self._right = None
        self._top = None
        self._bottom = None
        self._moveables = None
        self._moveables_union = None
        self._moveable_ref = None

        self._nbr = None
        self._nbr_with_cost = None
        self._nbr_around = None
        self._nbr_sz = None

    def update(self):
        self._type = self._tile.get_type()
        self._accessible = self._tile.get_accessible()
        self._special = self._tile.get_special()

    def update_acc_spread(self):
        self.update_acc()
        [c.update_acc() for c in self._nbr]

    def update_acc(self):
        self._acc = 2

        [self._moveables_union.remove(c) for c in self._acc_moveables if c not in self._moveables]
        self._acc_moveables.clear()

        if len(self._nbr_sz[2]) != 4 or not self._accessible:
            self._acc = 1
            self._acc_ref = None

        else:
            is_build = False

            for pl in self._nbr_sz[2]:
                if not pl.get_accessible() or (pl.get_build_ref() is not None and pl.get_special() != SpecialFlags.SPECIAL_MINER and pl.get_special() != SpecialFlags.SPECIAL_BUILD):
                    self._acc = 1
                    is_build = True
                else:
                    for c in pl.get_moveables():
                        if c not in self._moveables:
                            self._acc = 1
                            if c not in self._acc_moveables:
                                self._acc_moveables.add(c)

            if is_build:
                self._acc_ref = None
                self._acc_moveables.clear()

            else:
                self._acc_ref = next(iter(self._acc_moveables)) if len(self._acc_moveables) == 1 else None

                [self._moveables_union.add(c) for c in self._acc_moveables if c not in self._moveables]

    def all_neighbor_are(self, check_callback):
        for place in self.get_neighbors():
            if not check_callback(place):
                return False

        return True

    def get_neighbor(self, off_x, off_y):
        return self._tile_col.get_place(self._x + off_x, self._y + off_y)

    def get_neighbors(self):
        return self._nbr

    def get_neighbors_x(self):
        return self._nbr_x

    def get_neighbors_with_cost(self):
        return self._nbr_with_cost

    def get_neighbors_around(self, size=1):
        return self._nbr_around[size]

    def get_neighbors_size_max(self):
        return self._nbr_sz[2]

    def get_neighbors_size(self, size):
        return self._nbr_sz[size]

    def get_neighbors_around_backs(self, moveable_ref, get_all=False):
        out = []

        for c in self._nbr_around[moveable_ref.get_spec().get_size()]:
            loop = c
            steps = []

            while loop is not self:
                if (loop.is_accessible_no_build_only(moveable_ref) and loop.get_special() == SpecialFlags.SPECIAL_NONE) or get_all:
                    steps.append(loop)
                    #loop = loop._back_for[self]
                    loop = loop.get_back_for(self)
                else:
                    steps = None
                    break

            if steps is not None:
                out.append(steps)

        return out

    def get_side_places(self, diff_x, diff_y):
        tpl = PlaceSide._side_map[diff_x * 10 + diff_y]
        return [c for c in [self._tile_col.get_place(self._x + tpl[0][0], self._y + tpl[0][1]), self._tile_col.get_place(self._x + tpl[1][0], self._y + tpl[1][1])] if c is not None]

    def get_flag(self):
        return self._flag

    def get_back_steps(self):
        return self._back_steps

    def get_back_cost(self):
        return self._back_cost

    def get_back_for(self, back_for):
        return self._back_for[back_for]

    def set_flag(self, flag):
        self._flag = flag

    def _back_reset(self):
        self._flag = False
        self._back_steps = 0
        self._back_cost = 20

    def _back_set(self, back_cost, back_steps):
        self._back_cost = back_cost
        self._back_steps = back_steps

    def _back_for_set(self, p_from, p_to):
        self._back_for[p_from] = p_to

    def set_neightbor(self, tile_col: ITileCollector):
        cdef long fx, fy, idx
        cdef object place, stk, c
        cdef NbCoords nbc, coords

        self._tile_col = tile_col

        for fy in range(-2, 3):
            for fx in range(-2, 3):
                place = tile_col.get_place(self._x + fx, self._y - fy)

                if place is not None:
                    place._back_reset()

        self._left = tile_col.get_place(self._x - 1, self._y)
        self._right = tile_col.get_place(self._x + 1, self._y)
        self._top = tile_col.get_place(self._x, self._y + 1)
        self._bottom = tile_col.get_place(self._x, self._y - 1)

        self._nbr = []
        self._nbr_x = []

        self._nbr_with_cost = []

        #for c in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        for idx in range(0, 4):
            nbc = nb_coords_a[idx]
            place = tile_col.get_place(self._x + nbc.cx, self._y + nbc.cy)

            if place is not None:
                self._nbr_with_cost.append((10, place, self.get_side_places(nbc.cx, nbc.cy)))
                self._nbr.append(place)
                self._nbr_x.append(place)

        #for c in [(-1, -1), (1, 1), (-1, 1), (1, -1)]:
        for idx in range(0, 4):
            nbc = nb_coords_b[idx]
            place = tile_col.get_place(self._x + nbc.cx, self._y + nbc.cy)

            if place is not None:
                #self._nbr_with_cost.append((12, place, c))
                self._nbr_with_cost.append((12, place, self.get_side_places(nbc.cx, nbc.cy)))
                self._nbr.append(place)

        self._nbr_around = {
            1: [],
            2: []
        }

        stk = deque()
        stk.append(NbCoords_c(self._x, self._y))

        self._back_cost = 0

        while stk:
            coords = stk.popleft()
            c = tile_col.get_place(coords.cx, coords.cy)

            if c.get_back_steps() in [1, 2]:
                self._nbr_around[c.get_back_steps()].append(c)

            c.set_flag(True)

            if c.get_back_steps() < 2:
                #for cd in [(-1, -1, 1.2), (-1, 0, 1), (-1, 1, 1.2), (0, 1, 1), (1, 1, 1.2), (1, 0, 1), (1, -1, 1.2), (0, -1, 1)]:
                for idx in range(0, 8):
                    sbc = nb_coords_around[idx]
                    sub = tile_col.get_place(coords.cx + sbc.cx, coords.cy + sbc.cy)

                    if sub is not None:
                        if sub.get_back_cost() > c.get_back_cost() + sbc.cost:
                            sub._back_set(c.get_back_cost() + sbc.cost, c.get_back_steps() + 1)
                            sub._back_for_set(self, c)

                        if not sub.get_flag():
                            sub.set_flag(True)
                            stk.append(NbCoords_c(coords.cx + sbc.cx, coords.cy + sbc.cy))

        self._nbr_sz = {
            1: [self],
            2: []
        }

        for idx in range(0, 4):
            nbc = nb_coords_c[idx]
            place = tile_col.get_place(self._x + nbc.cx, self._y + nbc.cy)

            if place is not None:
                self._nbr_sz[2].append(place)
