from lib.orderedset import OrderedSet

from game.graphic.primitivecollector import PrimitiveCollector
from game.interface.itargetable import ITargetable
from game.interface.itilecollector import ITileCollector


cdef class MapTileArea:

    cdef:
        object _tile_col
        object _trg_ref
        object _tiles
        long _range
        long _min_back_steps
        long _bef_x
        long _bef_y
        long _x
        long _y

    def __cinit__(self, trg_ref: ITargetable, long range_value):
        self._tile_col = ITileCollector.get()
        self._trg_ref = trg_ref

        self._range = range_value
        self._min_back_steps = range_value // 50
        self._tiles = OrderedSet()

        self._bef_x = 0
        self._bef_y = 0
        self._x = -100
        self._y = -100

    def set_coords(self, x, y):
        self._x = x
        self._y = y

        if abs(self._bef_x - self._x) > 20 or abs(self._bef_y - self._y) > 20:
            self._bef_x = self._x
            self._bef_y = self._y

            self._update()

    def destroy(self):
        for tl in self._tiles:
            tl.targetable_rem(self._trg_ref)

    def _update(self):
        cdef long max_steps, back_steps
        cdef object tile, tile_loop, sc, scb

        tile = self._tile_col.get_tile_by_coords(self._x, self._y)
        max_steps = (self._range // 50) + 2

        sc = tile.get_scan_cache()
        scb = tile.get_scan_cache_back_steps()

        for idx in range(0, len(sc)):
            tile_loop = sc[idx]
            back_steps = scb[idx]

            if back_steps > max_steps + 2:
                break

            elif back_steps <= max_steps:
                if back_steps <= self._min_back_steps or tile_loop.dist_to_coord(self._x, self._y) <= self._range:
                    if tile_loop not in self._tiles:
                        self._tiles.add(tile_loop)
                        tile_loop.targetable_map(self._trg_ref)

                elif tile_loop in self._tiles:
                    self._tiles.remove(tile_loop)
                    tile_loop.targetable_rem(self._trg_ref)

    def scan_targetables_on_tiles(self, long max_steps, callback):
        cdef long back_steps
        cdef object tile, tile_loop, result, sc, scb

        def _fnd(tile, result):
            cdef object mm, bref

            for mm in tile.get_moveables():
                result = callback(mm, result)
            bref = tile.get_build_ref()
            if bref is not None:
                result = callback(bref, result)

            return result

        tile = self._tile_col.get_tile_by_coords(self._x, self._y)
        result = None

        sc = tile.get_scan_cache()
        scb = tile.get_scan_cache_back_steps()

        for idx in range(0, len(sc)):
            tile_loop = sc[idx]
            back_steps = scb[idx]

            if back_steps > max_steps + 2:
                break

            elif back_steps <= max_steps:
                result = _fnd(tile_loop, result)

        return result
