
cdef struct PathFlowMapField:
    long flag
    long cost[2]
    int finish
    long steps

cdef long ZOOM_CONV = 10000
cdef long COST_NA = 2100000000 #99999 * ZOOM_CONV
cdef long FINISH_NONE = 0
cdef long FINISH_TRAVEL = 1
cdef long FINISH_TEMP = 2
cdef long FINISH_INVALID = 10
cdef long FINISH_USED = 20
