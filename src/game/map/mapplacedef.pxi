
cdef struct NbCoords:
    long cx
    long cy
    double cost

cdef NbCoords NbCoords_c(long x, long y, double c=1.0):
    cdef NbCoords n

    n.cx = x
    n.cy = y
    n.cost = c

    return n

cdef NbCoords * nb_coords_a = [NbCoords_c(-1, 0), NbCoords_c(1, 0), NbCoords_c(0, -1), NbCoords_c(0, 1)]
cdef NbCoords * nb_coords_b = [NbCoords_c(-1, -1), NbCoords_c(1, 1), NbCoords_c(-1, 1), NbCoords_c(1, -1)]
cdef NbCoords * nb_coords_c = [NbCoords_c(0, 0), NbCoords_c(1, 0), NbCoords_c(0, -1), NbCoords_c(1, -1)]
cdef NbCoords * nb_coords_around = [
    NbCoords_c(-1, -1, 1.2),
    NbCoords_c(-1, 0, 1),
    NbCoords_c(-1, 1, 1.2),
    NbCoords_c(0, 1, 1),
    NbCoords_c(1, 1, 1.2),
    NbCoords_c(1, 0, 1),
    NbCoords_c(1, -1, 1.2),
    NbCoords_c(0, -1, 1)
]
