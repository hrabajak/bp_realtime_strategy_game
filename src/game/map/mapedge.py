from game.interface.itilecollector import ITileCollector


class MapEdge:

    _tile_col: ITileCollector = None

    _x = 0
    _y = 0
    _tiles = None
    _nbr = None

    opacity = 0

    # 0 - left, bottom
    # 1 - right, bottom
    # 2 - right, top
    # 3 - left, top

    def __init__(self, x, y):
        self._tile_col = ITileCollector.get()

        self._x = x
        self._y = y
        self._tiles = [
            self._tile_col.get_tile(self._x - 1, self._y - 1),
            self._tile_col.get_tile(self._x, self._y - 1),
            self._tile_col.get_tile(self._x, self._y),
            self._tile_col.get_tile(self._x - 1, self._y)
        ]

        if self._tiles[0] is not None:
            self._tiles[0].set_edge(2, self)

        if self._tiles[1] is not None:
            self._tiles[1].set_edge(3, self)

        if self._tiles[2] is not None:
            self._tiles[2].set_edge(0, self)

        if self._tiles[3] is not None:
            self._tiles[3].set_edge(1, self)

    def get_tiles(self):
        return self._tiles

    def get_neighbors(self):
        return self._nbr

    def set_neightbor(self, map: ITileCollector):
        self._nbr = []

        for fy in range(-1, 2):
            for fx in range(-1, 2):
                edge = map.get_edge(self._x + fx, self._y + fy)

                if (fy != 0 or fx != 0) and edge is not None:
                    self._nbr.append(edge)

    def destroy(self):
        self._tiles = None
        self._nbr = None
