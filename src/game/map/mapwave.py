from collections import deque

from game.interface.itilecollector import ITileCollector


class MapWave:
    _tile_col = None

    def __init__(self, tile_col: ITileCollector):
        self._tile_col = tile_col

    def wave_find(self, tile_from, find_callback, loop_callback=lambda tile: True, nb_callback=lambda tile: tile.get_neighbors_plus()):
        self._tile_col.clear_tiles_flags()

        stk = deque()
        stk.append(tile_from)

        while stk:
            tile = stk.popleft()
            tile.flag = 1

            if find_callback(tile):
                return tile

            for subTile in [x for x in nb_callback(tile) if x.flag == 0 and loop_callback(x)]:
                subTile.flag = 1
                stk.append(subTile)

        return None

    def wave_find_edges(self, tile_from, is_edge_callback):
        self._tile_col.clear_tiles_flags()

        stk = deque()
        stk.append(tile_from)
        edges = []

        while stk:
            tile = stk.popleft()
            tile.flag = 1
            have_nb = False

            for subTile in tile.get_neighbors():
                if is_edge_callback(subTile):
                    have_nb = True

                elif subTile.flag == 0:
                    subTile.flag = 1
                    stk.append(subTile)

            if have_nb:
                edges.append(tile)

        return edges

    def find_nearest_tile_from_tiles(self, tiles, found_callback, step_callback=lambda tile: True):
        stk = deque()
        stk += tiles

        tile_found = None
        tile_found_dist = 0

        while stk:
            tile = stk.popleft()
            tile.flag = 1

            for subTile in tile.get_neighbors():
                if subTile.flag == 0 and step_callback(subTile):
                    subTile.flag = 1
                    stk.append(subTile)

                if found_callback(subTile) and (tile_found is None or subTile.dist_to(tile) < tile_found_dist):
                    tile_found = subTile
                    tile_found_dist = subTile.dist_to(tile)

        return tile_found
