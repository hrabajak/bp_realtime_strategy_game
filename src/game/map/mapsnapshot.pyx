import pyglet

from game.map.maptiletexture import MapTileTexture


cdef class MapSnapshot:

    cdef:
        long _scale
        object _img
        long _map_width
        long _map_height
        long _image_width
        long _image_height

        int _use_view
        long _scX
        long _scY
        long _toX
        long _toY
        double _zoom_use
        double _rect_size

    def __cinit__(self, scale=10):
        self._scale = scale
        self._img = None
        self._map_width = 0
        self._map_height = 0
        self._zoom_use = 1.0
        self._rect_size = 25.0

    def get_img(self):
        return self._img

    def set_view(self, view):
        cdef long sx, sy, width, height

        self._use_view = True

        sx, sy, width, height, self._zoom_use = view.get_view_rect()

        self._rect_size = 25.0 * self._zoom_use
        self._scX = int((sx * -1) / self._rect_size)
        self._scY = int((sy * -1) / self._rect_size)
        self._toX = int(self._scX + (width / self._rect_size))
        self._toY = int(self._scY + (height / self._rect_size))

    def update_map(self, mref):
        cdef long fx, fy
        cdef object bts, place, tile

        if mref.get_tiles_width() != self._map_width or mref.get_tiles_height() != self._map_height:
            self._map_width = mref.get_tiles_width()
            self._map_height = mref.get_tiles_height()

            self._image_width = self._map_width * self._scale
            self._image_height = self._map_height * self._scale

            if self._img is not None:
                self._img.destroy()
                self._img = None

            self._img = pyglet.image.create(self._image_width, self._image_height)
            self._img.anchor_x = self._image_width // 2
            self._img.anchor_y = self._image_height // 2

        bts = list(self._img.get_image_data().get_data('RGBA', 4 * self._image_width))

        if self._scale == 2:
            for fy in range(0, self._map_height * 2):
                for fx in range(0, self._map_width * 2):

                    place = mref.get_place(fx, fy)
                    tile = place.get_tile()

                    if self._use_view and (((fx == self._scX or fx == self._toX) and fy >= self._scY and fy <= self._toY) or ((fy == self._scY or fy == self._toY) and fx >= self._scX and fx <= self._toX)):
                        self._map_col_single(bts, fx, fy, (100, 100, 100))

                    elif tile.snap_color is not None:
                        self._map_col_single(bts, fx, fy, tile.snap_color)

                    elif tile.get_fog_visible():
                        bref = place.get_build_ref()

                        if bref is not None:
                            self._map_col_single(bts, fx, fy, bref.get_side().get_color().get_map_color())

                        elif place.get_moveable_ref() is not None:
                            mm = place.get_moveable_ref()

                            self._map_col_single(bts, fx, fy, mm.get_side().get_color().get_map_color())

                        else:
                            self._map_col_single(bts, fx, fy, MapTileTexture.get_type_color(tile.get_type()))

                    else:
                        self._map_col_single(bts, fx, fy, (0, 0, 0))

        else:
            for fy in range(0, self._map_height):
                for fx in range(0, self._map_width):

                    tile = mref.get_tile(fx, fy)
                    col = MapTileTexture.get_type_color(tile.get_type())

                    for im_fy in range(0, self._scale):
                        for im_fx in range(0, self._scale):
                            idx = (fy * self._scale + im_fy) * self._image_width * 4 + (fx * self._scale + im_fx) * 4

                            bts[idx + 0] = int(col[0])
                            bts[idx + 1] = int(col[1])
                            bts[idx + 2] = int(col[2])
                            bts[idx + 3] = 255

        self._img.get_image_data().set_data('RGBA', 4 * self._image_width, bytes(bts))

    def _map_col(self, bts, fx, fy, size, col):
        cdef long im_fx, im_fy, idx

        for im_fy in range(0, size):
            for im_fx in range(0, size):
                idx = (fy + im_fy) * self._image_width * 4 + (fx + im_fx) * 4

                bts[idx + 0] = col[0]
                bts[idx + 1] = col[1]
                bts[idx + 2] = col[2]
                bts[idx + 3] = 255

    def _map_col_single(self, bts, fx, fy, col):
        cdef long idx = fy * self._image_width * 4 + fx * 4

        bts[idx + 0] = col[0]
        bts[idx + 1] = col[1]
        bts[idx + 2] = col[2]
        bts[idx + 3] = 255
