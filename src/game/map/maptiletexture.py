import random

from game.interface.iimages import IImages


class MapTileTexture:

    T_WATER = 0
    T_DUST = 2
    T_CONCGROUND = 3
    T_WATERGRASS = 4
    T_GRASS = 5
    T_DARKGRASS = 6
    T_FIELD = 7
    T_TREE = 8
    T_DEEP_TREE = 9
    T_WATER_TREE = 10
    T_BLANK = 99

    _spread_sort = [
        T_WATER,
        T_DUST,
        T_CONCGROUND,
        T_TREE,
        T_DEEP_TREE,
        T_DARKGRASS,
        T_GRASS,
        T_WATER_TREE,
        T_WATERGRASS,
        T_FIELD,
        T_BLANK
    ]
    _tile = None
    _type = 0
    _map = None
    _hash = 0
    _number = 0
    _alternative = 0

    def __init__(self, tile, type=T_WATER):
        self._tile = tile
        self._type = type
        self._map = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]

    def set_type(self, type):
        self._type = type
        self.update()

    def _type_transform(self, type):
        if type in [MapTileTexture.T_TREE, MapTileTexture.T_DEEP_TREE]:
            return MapTileTexture.T_DARKGRASS
        elif type in [MapTileTexture.T_WATER_TREE]:
            return MapTileTexture.T_WATERGRASS
        else:
            return type

    def update(self):
        self._hash = ''

        for fy in range(0, 3):
            for fx in range(0, 3):
                self._map[fy][fx] = self._type if fy == 1 and fx == 1 else -1

        if self._type in [MapTileTexture.T_TREE, MapTileTexture.T_DEEP_TREE] and False:
            # !! NE
            for fy in range(0, 3):
                for fx in range(0, 3):
                    self._map[fy][fx] = self._type

        else:
            sort_idx = MapTileTexture._spread_sort.index(self._type)

            # self._map[1][0] = MapTileTexture.T_GRASS  # x=0 je VLEVO
            # self._map[1][2] = MapTileTexture.T_GRASS  # x=2 je VPRAVO
            # self._map[0][1] = MapTileTexture.T_GRASS # y=0 je NAHORE
            # self._map[2][1] = MapTileTexture.T_GRASS # y=2 je DOLE

            for coord in [(1, 0), (-1, 0), (0, -1), (0, 1)]:
                n_tile = self._tile.get_tile_add_xy(coord[0], coord[1])

                if n_tile is not None:
                    n_idx = MapTileTexture._spread_sort.index(n_tile.get_type())
                    use_coords = []

                    if coord[1] == 0:
                        for c in range(-1, 2):
                            use_coords.append((1 - c, 1 + coord[0]))
                            #self._map[1 - c][1 + coord[0]] = n_tile.get_type()
                    else:
                        for c in range(-1, 2):
                            use_coords.append((1 - coord[1], 1 + c))
                            #self._map[1 - coord[1]][1 + c] = n_tile.get_type()

                    if sort_idx > n_idx:
                        for c in use_coords:
                            if self._map[c[0]][c[1]] == -1 or MapTileTexture._spread_sort.index(self._map[c[0]][c[1]]) > n_idx:
                                self._map[c[0]][c[1]] = self._type_transform(n_tile.get_type())

            for coord in [(1, 1), (-1, -1), (1, -1), (-1, 1)]:
                n_tile = self._tile.get_tile_add_xy(coord[0], coord[1])

                if n_tile is not None:
                    n_idx = MapTileTexture._spread_sort.index(n_tile.get_type())

                    if sort_idx > n_idx and (self._map[1 - coord[1]][1 + coord[0]] == -1 or MapTileTexture._spread_sort.index(self._map[1 - coord[1]][1 + coord[0]]) > n_idx):
                        self._map[1 - coord[1]][1 + coord[0]] = self._type_transform(n_tile.get_type())

            tp_counts = {}

            for tp in MapTileTexture._spread_sort:
                tp_counts[tp] = 0

            for fy in range(0, 3):
                for fx in range(0, 3):
                    if self._map[fy][fx] == -1:
                        self._map[fy][fx] = self._type
                    tp_counts[self._map[fy][fx]] += 1

            if tp_counts[self._type] == 4: #or tp_counts[self._type] == 2:
                self._map[1][1] = max(tp_counts, key=lambda key: tp_counts[key])

        for fy in range(0, 3):
            for fx in range(0, 3):
                self._hash += '.' + str(self._map[fy][fx])

        # self._number = (self._tile.get_pos_x() + self._tile.get_pos_y()) % 10
        self._number = random.randint(0, 12)

        if self._type == MapTileTexture.T_DUST:
            self._alternative = random.randint(0, 1)
        else:
            self._alternative = 1 if random.randint(0, 10) == 0 else 0

        self._hash += '-' + str(self._number) + '-' + str(self._alternative)

    def get_image(self):
        return IImages.get().get_image_texture(self._hash, self._map, self._type_transform(self._type), self._number, self._alternative).get_region(1, 1, 50, 50)

    def print_overview(self):
        print("== tex: " + str(self._type) + " [" + str(self._tile.get_pos_x()) + ", " + str(self._tile.get_pos_y()) + "] " + self._hash)

        for fy in range(0, 3):
            print(self._map[fy])

    @staticmethod
    def get_type_color(tp):
        if tp == MapTileTexture.T_WATER:
            return 76, 143, 252

        elif tp == MapTileTexture.T_DUST:
            return 166, 113, 43

        elif tp == MapTileTexture.T_CONCGROUND:
            return 102, 92, 80

        elif tp == MapTileTexture.T_GRASS:
            return 38, 128, 49

        elif tp == MapTileTexture.T_WATERGRASS:
            return 109, 179, 84

        elif tp == MapTileTexture.T_FIELD:
            return 189, 159, 13

        elif tp in [MapTileTexture.T_TREE, MapTileTexture.T_WATER_TREE]:
            return 15, 77, 21

        elif tp == MapTileTexture.T_DEEP_TREE:
            return 6, 46, 10

        else:
            return 46, 46, 46
