from collections import deque
from threading import Lock

from game.config import Config
from game.helper.singleton import Singleton
from game.interface.iplayersides import IPlayerSides
from game.interface.iview import IView
from game.logger import Logger
from game.map.mapedge import MapEdge
from game.map.mapgenerator import MapGenerator
from game.map.mapplace import MapPlace
from game.map.mapscanner import MapScanner
from game.map.maptile import MapTile
from game.interface.itilecollector import ITileCollector
from game.map.maptiletexture import MapTileTexture
from game.map.placeside import PlaceSide


class MapBase(ITileCollector, metaclass=Singleton):

    _view: IView = IView.get()

    _lock = None
    _width = 0
    _height = 0
    _tiles = None
    _tiles_avail = None
    _tiles_fog = None
    _tiles_overlay = None
    _fog_type = 0
    _fog_enabled = True
    _grid = None
    _edges = None
    _edge_grid = None
    _places = None
    _place_grid = None
    _first_tiles = None
    _loop_flag = 0
    _find_flag = None
    _tick = 0
    _set_flag = 0

    _garea = None

    _bef_x = -1
    _bef_y = -1
    _bef_width = -1
    _bef_height = -1
    _bef_zoom = -1
    _cur_tilesize = 0

    def __init__(self):
        ITileCollector.__init__(self)
        ITileCollector.set(self)

        self._lock = Lock()
        self._tiles_avail = set()
        self._tiles_fog = deque()
        self._tiles_overlay = set()

        self._tiles = []
        self._grid = []
        self._edges = []
        self._edge_grid = []
        self._places = []
        self._place_grid = []
        self._first_tiles = []

        self._set_flag = 0
        self._bef_x = -1
        self._bef_y = -1
        self._bef_width = -1
        self._bef_height = -1
        self._bef_zoom = -1
        self._cur_tilesize = 0

        self._view = IView.get()
        self._loop_flag = {
            0: 1,
            1: 1
        }
        self._tick = 0

    def get_x(self):
        return self._bef_x

    def get_y(self):
        return self._bef_y

    def get_zoom(self):
        return self._bef_zoom

    def get_fog_type(self):
        return self._fog_type

    def get_fog_enabled(self):
        return self._fog_enabled

    def get_view_width(self):
        return self._bef_width

    def get_view_height(self):
        return self._bef_height

    def get_cur_tilesize(self):
        return self._cur_tilesize

    def get_width(self) -> int:
        return self._width

    def get_height(self) -> int:
        return self._height

    def get_set_flag(self):
        return self._set_flag

    def get_find_flag(self, tp=0):
        return self._loop_flag[tp]

    def move_find_flag(self, tp=0):
        self._loop_flag[tp] += 1

    def get_tile(self, x, y):
        if x >= 0 and y >= 0 and x < self._width and y < self._height:
            return self._grid[y][x]

        return None

    def get_tile_by_coords(self, x, y):
        px = max(min(int(x / 50.0), self._width - 1), 0)
        py = max(min(int(y / 50.0), self._height - 1), 0)

        return self.get_tile(px, py)

    def get_tile_by_coords_scroll(self, x, y):
        px = int((self._view.get_x() - x) * -1 / self._view.get_cur_tilesize())
        py = int((self._view.get_y() - y) * -1 / self._view.get_cur_tilesize())

        return self.get_tile(px, py)

    def get_tile_list_by_coords(self, x, y, size):
        out = []

        px = int((self._view.get_x() - x) * -1 / self._view.get_cur_tilesize())
        py = int((self._view.get_y() - y) * -1 / self._view.get_cur_tilesize())

        for fy in range(py - size, py + size + 1):
            for fx in range(px - size, px + size + 1):
                tile = self.get_tile(fx, fy)
                if tile is not None:
                    out.append(tile)

        return out

    def get_tiles(self):
        return self._tiles

    def get_tiles_width(self):
        return self._width

    def get_tiles_height(self):
        return self._height

    def get_garea(self):
        return self._garea

    def pop_first_tile(self):
        if len(self._first_tiles) > 0:
            tl = self._first_tiles[0]
            del self._first_tiles[0]
            return tl
        else:
            return None

    def clear_tiles_flags(self):
        for tile in self._tiles:
            tile.flag = 0

    def fog_tile_add(self, tile):
        if self._fog_enabled:
            self._tiles_fog.append(tile)

    def overlay_tile_add(self, tile):
        if tile not in self._tiles_overlay:
            self._tiles_overlay.add(tile)

    def get_place(self, x, y):
        if x >= 0 and y >= 0 and x < self._width * 2 and y < self._height * 2:
            return self._place_grid[y][x]

        return None

    def get_place_by_coords(self, x, y):
        px = x // 25
        py = y // 25

        return self.get_place(px, py)

    def get_place_by_coords_scroll(self, x, y):
        px = int((self._view.get_x() - x) * -1 / (self._view.get_cur_tilesize() / 2))
        py = int((self._view.get_y() - y) * -1 / (self._view.get_cur_tilesize() / 2))

        return self.get_place(px, py)

    def get_places(self):
        return self._places

    def get_edge(self, x, y):
        if x >= 0 and y >= 0 and x < self._width + 1 and y < self._height + 1:
            return self._edge_grid[y][x]

        return None

    def init_grid_area(self):
        from game.graphic.gridarea import GridArea
        self._garea = GridArea(self._view, self)

    def clear(self):
        if self._edges is not None:
            for eg in self._edges:
                eg.destroy()

        if self._places is not None:
            for pl in self._places:
                pl.destroy()

        if self._tiles is not None:
            for tl in self._tiles:
                tl.destroy()

        self._tiles = []
        self._tiles_avail = set()
        self._tiles_fog = deque()
        self._tiles_overlay = set()
        self._grid = []
        self._edges = []
        self._edge_grid = []
        self._places = []
        self._place_grid = []
        self._first_tiles = []
        self._width = 0
        self._height = 0

        self._set_flag = 0
        self._tick = 0
        self._bef_x = -1
        self._bef_y = -1
        self._bef_width = -1
        self._bef_height = -1
        self._bef_zoom = -1

        self.force_update()

    def build(self, width, height):

        self._width = width
        self._height = height

        PlaceSide.build()

        map_scr = MapScanner(self)

        Logger.get().logm("(map) Creating tiles ...")

        for fy in range(0, self._height):
            grid_line = []

            for fx in range(0, self._width):
                tile = MapTile(fx, fy, map_scr)
                tile.number = len(self._tiles)
                self._tiles.append(tile)
                grid_line.append(tile)

            self._grid.append(grid_line)

        Logger.get().logm("(map) Creating edges ...")

        for fy in range(0, self._height + 1):
            edge_line = []

            for fx in range(0, self._width + 1):
                edge = MapEdge(fx, fy)
                self._edges.append(edge)
                edge_line.append(edge)

            self._edge_grid.append(edge_line)

        Logger.get().logm("(map) Creating places ...")

        for fy in range(0, self._height * 2):
            place_line = []

            for fx in range(0, self._width * 2):
                place = MapPlace(fx, fy, self.get_tile(fx // 2, fy // 2))
                place.number = len(self._places)
                self._places.append(place)
                place_line.append(place)

            self._place_grid.append(place_line)

        Logger.get().logm("(map) Setting neightbors tiles ...")

        for c in self._tiles:
            c.set_neightbor(self)

        Logger.get().logm("(map) Setting neightbors places ...")

        for pl in self._places:
            pl.set_neightbor(self)

        Logger.get().logm("(map) Setting neightbors edges ...")

        for ee in self._edges:
            ee.set_neightbor(self)

        Logger.get().logm("(map) Done")

    def clear_fill(self, tp):
        for fy in range(0, self._height):
            for fx in range(0, self._width):
                tile = self._grid[fy][fx]
                tile.set_seed()
                tile.flag = 0
                tile.area_id = 0
                tile.gen_flag = 0

                if fy > 0 and fx > 0 and fy < self._height - 1 and fx < self._width - 1:
                    tile.set_type(tp)
                else:
                    tile.set_type(MapTileTexture.T_BLANK)

    def regenerate(self, seed):
        self.clear_fill(MapTileTexture.T_WATER)

        m_gen = MapGenerator(self)
        m_gen.generate(seed)

        self._first_tiles = m_gen.get_first_tiles()

        for c in self._tiles:
            c.build()

        for c in self._places:
            c.update_acc()

        self.force_update()
        self._set_flag = 1

    def regenerate_b(self, seed, player_count):
        self.clear_fill(MapTileTexture.T_WATER)

        m_gen = MapGenerator(self)
        m_gen.generate_b(seed, player_count)

        self._first_tiles = m_gen.get_first_tiles()

        for c in self._tiles:
            c.build()

        for c in self._places:
            c.update_acc()

        self.force_update()
        self._set_flag = 1

    def generate_blank(self, count):
        self._first_tiles = []
        self.clear_fill(MapTileTexture.T_GRASS)

        m_gen = MapGenerator(self)
        dst = 4

        tiles = [
            self.get_tile(dst, dst),
            self.get_tile(self._width - 1 - dst, self._height - 1 - dst),
            self.get_tile(dst, self._height - 1 - dst),
            self.get_tile(self._width - 1 - dst, dst)
        ]

        for idx in range(0, count):
            tile = tiles[idx]

            #field_qa = 500
            #field_tiles = m_gen.generate_fields(m_gen.generate_area(tile, field_qa), field_qa)

            #fd_x = sum((c.get_x() for c in field_tiles)) / len(field_tiles)
            #fd_y = sum((c.get_y() for c in field_tiles)) / len(field_tiles)
            #self._first_tiles.append(self.get_tile_by_coords(fd_x, fd_y))
            self._first_tiles.append(tile)

        field_qa = (self._width * self._height) // 4
        m_gen.generate_area(self.get_tile(self._width // 2, self._height // 2), field_qa, MapTileTexture.T_FIELD)

        for c in self._tiles:
            c.build()

        for c in self._places:
            c.update_acc()

        self.force_update()
        self._set_flag = 1

    def serialize(self):
        out = {
            'width': self._width,
            'height': self._height,
            'first_tiles': [],
            'grid': []
        }

        for tile in self._first_tiles:
            out['first_tiles'].append(tile.serialize())

        for fy in range(0, self._height):
            row = []

            for fx in range(0, self._width):
                row.append(self._grid[fy][fx].serialize())

            out['grid'].append(row)

        return out

    def unserialize(self, dt):
        self.build(dt['width'], dt['height'])

        self._first_tiles = []

        for item in dt['first_tiles']:
            self._first_tiles.append(self.get_tile(item['x'], item['y']))

        for fy in range(0, self._height):
            for fx in range(0, self._width):
                item = dt['grid'][fy][fx]
                self._grid[fy][fx].set_type(item['type'])
                self._grid[fy][fx].set_seed(item['seed'])

        for c in self._tiles:
            c.build()

        for c in self._places:
            c.update_acc()

        self.force_update()
        self._set_flag = 1

    def set_lines(self, lines):

        lines = lines.split('|')

        for fy in range(0, self._height):
            for fx in range(0, self._width):
                char = lines[fy][fx]
                self._grid[fy][fx].set_type(MapTileTexture.T_WATER if char == 'x' else MapTileTexture.T_GRASS)

    def set_fog_type(self, fog_type):
        self._fog_type = fog_type
        self._fog_enabled = self._fog_type in [Config.FOG_ALLOW, Config.FOG_ALL]

    def update_sides(self, psides: IPlayerSides):
        for tl in self._tiles:
            tl.update_sides(psides)

    def rebuild_tile(self, tile: MapTile):
        tile.build()

        for c in tile.get_neighbors():
            c.build()

    def force_update(self):
        for tile in self._tiles:
            self._tiles_fog.append(tile)

        if self._garea is not None:
            self._garea.force_update()

    def load_images(self):
        Logger.get().logm('Loading map textures ...')

        for tl in self._tiles:
            tl.get_img()

    def draw_update(self):
        if self._set_flag > 0:

            self._bef_x, self._bef_y, self._bef_width, self._bef_height, self._bef_zoom = self._view.get_view_rect()
            self._cur_tilesize = self._view.get_cur_tilesize()
            self._set_flag = 2

            tls = self._garea.update()

            if len(tls) > 0:
                n_avail = set()

                for tl in tls:
                    if tl in self._tiles_avail:
                        self._tiles_avail.remove(tl)
                    else:
                        tl.set_visible(True)

                    n_avail.add(tl)

                for tl in self._tiles_avail:
                    tl.set_visible(False)

                self._tiles_avail = n_avail

            if len(self._tiles_overlay) > 0:
                for tl in [c for c in self._tiles_overlay]:

                    tl.draw()

                    if tl.get_overlay_flag() == 0:
                        self._tiles_overlay.remove(tl)

            if self._tick % 10 == 0:

                proc = []
                add = []

                while self._tiles_fog:
                    c = self._tiles_fog.popleft()
                    pres = c.fog_process()

                    if pres == 2:
                        proc.append(c)

                    elif pres == 1:
                        proc.append(c)
                        add.append(c)

                    else:
                        add.append(c)

                [self._tiles_fog.append(c) for c in add]

                if len(proc) > 0:
                    for subc in [c for c in proc]:
                        proc.extend(subc.fog_after_process())

                    for subc in [c for c in proc]:
                        proc.extend(subc.fog_after_process(1))

                    for c in proc:
                        c.fog_sec_process()

                    edges = set()

                    for c in proc:
                        for ee in c.get_edges():
                            if ee not in edges:
                                edges.add(ee)

                                for sub_ee in ee.get_neighbors():
                                    if sub_ee not in edges:
                                        edges.add(sub_ee)

                    edge_tiles = set()

                    for ee in edges:
                        self._garea.update_edge(ee)

                        for c in ee.get_tiles():
                            if c is not None and c not in edge_tiles:
                                edge_tiles.add(c)

                    for c in edge_tiles:
                        if c in self._tiles_avail:
                            self._garea.update_tile(c)

            self._tick += 1

    def draw(self):
        if self._set_flag == 2:
            self._garea.draw()

    def draw_black(self):
        if self._set_flag == 2:
            self._garea.draw_black()
