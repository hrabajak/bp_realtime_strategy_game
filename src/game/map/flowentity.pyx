import math
from collections import deque

import cython
from lib.orderedset import OrderedSet

from game.graphic.primitivecollector import PrimitiveCollector
from game.helper.discretemath cimport int_sqrt
from game.interface.itilecollector import ITileCollector
from game.map.pathflowmap import PathFlowMap
include "pathflowmapdef.pxi"


cdef struct ConvVec:
    long long vx
    long long vy
    #double xx
    #double yy

cdef class FlowEntity:

    cdef:
        object _parent
        object _tile_col
        object _mref
        int _size
        long _score
        object _vector
        object _before_place
        int _use_slot

        int _finish
        int _free_way

        object _stacked_ents
        int _stacked_timeout
        int _stacked_count
        long _loop_flag

        object _prim
        object _prim_b

    def __cinit__(self, mref):
        self._parent = None
        self._tile_col = ITileCollector.get()

        self._mref = mref
        self._size = mref.get_spec().get_size()
        self._stacked_ents = OrderedSet()

        self._finish = False
        self._free_way = False
        self._score = 0
        self._loop_flag = 0
        self._stacked_count = 0
        self._use_slot = 1 if self._size == 2 else 0

    def reset(self, parent, check_target=False):
        self._parent = parent
        self._stacked_ents.clear()
        self._vector = None
        self._finish = False
        self._free_way = False
        self._before_place = None
        self._score = 0
        self._loop_flag = 0
        self._stacked_count = 0
        self._stacked_timeout = 0

    def prepare(self, parent):
        self._parent = parent
        self._vector = None
        self._loop_flag = 0

    def clear_finish(self):
        self._finish = False
        self._before_place = None

    def clear_timeout(self):
        if self._mref.get_is_alive():
            self._mref.get_path().unset_timeouts()

    def get_mref(self):
        return self._mref

    def get_size(self):
        return self._size

    def get_score(self):
        return self._score

    def get_loop_flag(self):
        return self._loop_flag

    def get_finish(self):
        return self._finish

    def get_stacked_ents(self):
        return self._stacked_ents

    def get_stacked_count(self):
        return self._stacked_count

    def get_stacked_timeout(self):
        return self._stacked_timeout

    def get_evade_check(self):
        # dulezite - musi tu byt horni mez, aby se opet otevrela moznost k naplanovani cesty
        return self._stacked_timeout > 10 and self._stacked_timeout < 14

    def set_loop_flag(self, loop_flag):
        self._loop_flag = loop_flag

    def update_score(self):
        cdef long number, nb_number, max_steps
        cdef size_t a_ptr
        cdef PathFlowMapField * a

        self._score = 0
        self._free_way = True

        if not self._parent.get_may_move():
            return False

        elif self._finish or not self._mref.get_is_alive():
            return True

        a_ptr = self._parent.get_costs().get_a_ptr()
        a = <PathFlowMapField *> a_ptr

        place = self._mref.get_place()
        loop_place = place
        number = loop_place.number
        max_steps = 2

        if a[number].cost[self._use_slot] != COST_NA:
            while a[number].finish != FINISH_TRAVEL and max_steps > 0:
                max_steps -= 1
                step_place = None

                for nb in loop_place.get_neighbors():
                    nb_number = nb.number

                    if a[nb_number].cost[self._use_slot] != COST_NA and (step_place is None or a[nb_number].cost[self._use_slot] < a[step_place.number].cost[self._use_slot]):
                        step_place = nb

                if step_place is not None:
                    loop_place = step_place
                    self._score += a[step_place.number].cost[self._use_slot]

                    if loop_place.is_moveable_goto_group(self._mref, self._mref.get_goto_group()):
                        self._score += len(self._parent.get_entities())
                        self._free_way = False

                #PrimitiveCollector().add_circle(loop_place.get_x(), loop_place.get_y(), 3, color=(200, 0, 0))

            #PrimitiveCollector().add_text(place.get_x(), place.get_y(), str(self._score))

    def map_focus_place(self, focus_place, before_place):
        self.clean_stacked()
        self.update_score()
        self._before_place = before_place

    def test_finish(self):
        cdef long number
        cdef size_t a_ptr
        cdef PathFlowMapField * a

        if not self._parent.get_may_move():
            return False

        elif self._finish:
            return True

        else:
            a_ptr = self._parent.get_costs().get_a_ptr()
            a = <PathFlowMapField *> a_ptr
            place = self._mref.get_place()

            fin_places = [pl for pl in place.get_neighbors_size(self._size) if a[pl.number].cost[self._use_slot] != COST_NA and a[pl.number].finish == FINISH_TRAVEL]

            if len(fin_places) > 0:
                for pl in place.get_neighbors_size(self._size):
                    number = pl.number

                    a[number].cost[self._use_slot] = 0
                    a[number].finish = FINISH_USED
                    self._parent.add_place_target_used(pl)

                self._finish = True
                self._parent.update_targets()

                for pl in fin_places:
                    number = pl.number
                    a[number].cost[self._use_slot] += 20

                self._parent.set_realize_force()

                return True

        return False

    def _nearest_accessible(self, from_place):
        cdef size_t a_ptr = self._parent.get_costs().get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        cdef long number, sub_number
        cdef object step_place = None
        cdef long step_place_cost = 0
        cdef long step_place_fc = 0
        cdef int step_finish = False

        for sbc in from_place.get_neighbors_with_cost():

            sub_cost = sbc[0]
            sub_place = sbc[1]
            sub_number = sub_place.number

            if sub_place is self._before_place:
                pass

            elif a[sub_number].cost[self._use_slot] != COST_NA and a[sub_number].finish != FINISH_USED and sub_place.is_accessible_goto_group_only_b(self._mref, self._parent.get_block_moveables()):
                test_cost = a[sub_number].cost[self._use_slot]
                test_finish = a[sub_number].finish == FINISH_TRAVEL

                if step_place is None or (not step_finish and test_finish):
                    step_place = sub_place
                    step_place_cost = test_cost
                    step_place_fc = sub_cost
                    step_finish = test_finish

                elif test_cost < step_place_cost or (test_cost == step_place_cost and sub_cost < step_place_fc) and test_finish == step_finish:
                    step_place = sub_place
                    step_place_cost = test_cost
                    step_place_fc = sub_cost
                    step_finish = test_finish

        return step_place

    @cython.cdivision(True)
    def _nearest_around(self, from_place):
        cdef size_t a_ptr = self._parent.get_costs().get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        cdef long pl_x = from_place.get_pos_x()
        cdef long pl_y = from_place.get_pos_y()
        cdef long min_cost = -1, nb_number, to_x, to_y
        cdef object nb, test_place, step_place

        step_place = None

        for nb in from_place.get_neighbors_around(2):

            nb_number = nb.number

            if a[nb_number].cost[self._use_slot] != COST_NA and a[nb_number].finish != FINISH_USED:

                to_x = (nb.get_pos_x() - pl_x) // 2 + pl_x
                to_y = (nb.get_pos_y() - pl_y) // 2 + pl_y

                test_place = self._tile_col.get_place(to_x, to_y)

                if a[nb_number].cost[self._use_slot] == COST_NA:
                    pass

                elif test_place is not None and (min_cost == -1 or min_cost > a[nb_number].cost[self._use_slot]) and test_place.is_accessible_size(self._size):
                    # and test_place.is_accessible(self._mref):
                    # and test_place.is_accessible_goto_group_only_b(self._mref, self._parent.get_block_moveables()):
                    min_cost = a[nb_number].cost[self._use_slot]
                    step_place = test_place

        return step_place

    @cython.cdivision(True)
    cdef ConvVec _convolution_vector(self, from_place):
        cdef size_t a_ptr = self._parent.get_costs().get_a_ptr()
        cdef PathFlowMapField * a = <PathFlowMapField *> a_ptr

        cdef long number, d_vec_size
        cdef long use_cost, t_cost
        cdef long min_cost = -1
        cdef ConvVec d_vec
        cdef long long min_x = 0
        cdef long long min_y = 0
        cdef int min_use = False
        cdef double b_cost

        d_vec.vx = 0
        d_vec.vy = 0

        #d_vec.xx = 0
        #d_vec.yy = 0

        for nb in from_place.get_neighbors():
            number = nb.number

            if a[number].cost[self._use_slot] != COST_NA:
                if a[number].cost[self._use_slot] == 0:
                    pass
                else:
                    use_cost = a[number].cost[self._use_slot]

                    if min_cost == -1 or use_cost < min_cost:
                        min_x = nb.get_pos_x() - from_place.get_pos_x()
                        min_y = nb.get_pos_y() - from_place.get_pos_y()
                        min_cost = use_cost

                    #b_cost = use_cost / ZOOM_CONV
                    #d_vec.xx += (1 / b_cost) * (nb.get_pos_x() - from_place.get_pos_x())
                    #d_vec.yy += (1 / b_cost) * (nb.get_pos_y() - from_place.get_pos_y())

                    t_cost = max(use_cost // ZOOM_CONV, 1)

                    # spocitani diskretnich tisicin konvolucniho vektoru
                    d_vec.vx += ((nb.get_pos_x() - from_place.get_pos_x()) * ZOOM_CONV) // t_cost
                    d_vec.vy += ((nb.get_pos_y() - from_place.get_pos_y()) * ZOOM_CONV) // t_cost

                    #print(nb.get_pos_x() - from_place.get_pos_x(), nb.get_pos_y() - from_place.get_pos_y(), t_cost, use_cost)

            else:
                min_use = True

        if min_use:
            # okoli je nekompletni - konvoluce by vracela nesmysl - vratime minimum
            d_vec.vx = min_x * ZOOM_CONV
            d_vec.vy = min_y * ZOOM_CONV

            return d_vec

        elif d_vec.vx == 0 and d_vec.vy == 0:
            return d_vec

        else:
            d_vec_size = int_sqrt(d_vec.vx * d_vec.vx + d_vec.vy * d_vec.vy)

            # diskretni normalizace - vraceni vektoru - z tisicinasobku
            d_vec.vx = (d_vec.vx * ZOOM_CONV) // d_vec_size
            d_vec.vy = (d_vec.vy * ZOOM_CONV) // d_vec_size

            #vec_size = math.sqrt(d_vec.xx ** 2 + d_vec.yy ** 2)
            #d_vec.xx = 0 if vec_size == 0 else d_vec.xx / vec_size
            #d_vec.yy = 0 if vec_size == 0 else d_vec.yy / vec_size

            return d_vec

    def get_focus_place(self):
        cdef long number
        cdef size_t a_ptr
        cdef PathFlowMapField * a

        cdef object place = self._mref.get_place()
        cdef object new_step_place = None
        cdef object test_step_place = None
        cdef int is_stuck = False
        cdef ConvVec c_vec
        cdef int u_vec_x
        cdef int u_vec_y

        if not self._parent.get_may_move():
            return None

        else:
            a_ptr = self._parent.get_costs().get_a_ptr()
            a = <PathFlowMapField *> a_ptr
            number = place.number

            """
            if self._prim is not None:
                self._prim.drop()
                self._prim = None

            if self._prim_b is not None:
                self._prim_b.drop()
                self._prim_b = None

            c_vec = self._convolution_vector(place)
            #self._prim_b = PrimitiveCollector().add_line(place.get_x(), place.get_y(), place.get_x() + c_vec.xx * 30, place.get_y() + c_vec.yy * 30, color=(255, 255, 0))
            self._prim = PrimitiveCollector().add_line(place.get_x(), place.get_y(), place.get_x() + (c_vec.vx / ZOOM_CONV) * 45, place.get_y() + (c_vec.vy / ZOOM_CONV) * 45, color=(0, 0, 255))
            """

            if a[number].finish == FINISH_INVALID:
                # at invalidated finish place
                return None

            elif a[number].cost[self._use_slot] != COST_NA:
                new_step_place = self._nearest_accessible(place)

                if new_step_place is not None and not (a[new_step_place.number].cost[self._use_slot] < a[number].cost[self._use_slot]):
                    new_step_place = None

                # !! new_step_place = None # !!!

                if new_step_place is None or not new_step_place.is_accessible(self._mref):
                    c_vec = self._convolution_vector(place)

                    if c_vec.vx < - 6666:
                        u_vec_x = -1
                    elif c_vec.vx < 6666:
                        u_vec_x = 0
                    else:
                        u_vec_x = 1

                    if c_vec.vy < - 6666:
                        u_vec_y = -1
                    elif c_vec.vy < 6666:
                        u_vec_y = 0
                    else:
                        u_vec_y = 1

                    test_step_place = place.get_place_add_xy(u_vec_x, u_vec_y)

                    if test_step_place is self._before_place or not test_step_place.is_accessible_goto_group_only_b(self._mref, self._parent.get_block_moveables()):
                        pass

                    elif test_step_place is not None and a[test_step_place.number].cost[self._use_slot] != COST_NA:
                        if a[test_step_place.number].cost[self._use_slot] < a[place.number].cost[self._use_slot] and a[test_step_place.number].finish != FINISH_USED:
                            new_step_place = test_step_place

                """
                if self._prim is not None:
                    self._prim.drop()
                    self._prim = None
        
                if self._prim_b is not None:
                    self._prim_b.drop()
                    self._prim_b = None
        
                if new_step_place is not None:
                    if not new_step_place.is_accessible(self._mref):
                        self._prim = PrimitiveCollector().add_line(place.get_x(), place.get_y(), new_step_place.get_x(), new_step_place.get_y(), color=(0, 0, 255))
                    else:
                        self._prim = PrimitiveCollector().add_line(place.get_x(), place.get_y(), new_step_place.get_x(), new_step_place.get_y(), color=(0, 255, 0))
        
                if len(self._stacked_ents) > 0:
                    f_ents = self.get_stacked_all()
                    self._prim_b = PrimitiveCollector().add_text(place.get_x(), place.get_y(), str(len(f_ents)) + " | " + str(self._stacked_timeout))
                """

        if new_step_place is None:
            test_step_place = self._nearest_around(place)

            if test_step_place is self._before_place:
                pass

            elif test_step_place is not None:
                new_step_place = test_step_place

        if new_step_place is None:
            self._stacked_timeout += 1
            is_stuck = True

        elif not new_step_place.is_accessible(self._mref):
            self.add_stacked(new_step_place)
            is_stuck = True

        if is_stuck:
            for pl in self._mref.get_path().get_places_around():
                if self._parent.add_block_places(pl):
                    break

        else:
            self.clean_stacked()

        return new_step_place

    def clean_stacked(self):
        self._stacked_ents.clear()
        self._stacked_timeout = 0
        self._stacked_count = 0

    def add_stacked(self, place):
        count_update = False

        sub_mref = place.get_moveable_ref()

        if sub_mref is None:
            sub_mref = place.get_acc_ref()

        if sub_mref is not None:
            ent = self._parent.get_entity(sub_mref)

            if ent is not None and self not in ent.get_stacked_ents():
                ent.get_stacked_ents().add(self)
                count_update = True

        self._stacked_timeout += 1

        if count_update:
            self._stacked_count = len(self.get_stacked_all())

    def get_stacked_all(self, with_self=False):
        final_ents = OrderedSet()
        stk = deque()
        flag = self._parent.get_loop_flag()

        for sub_ent in self._stacked_ents:
            sub_ent.set_loop_flag(flag)
            stk.append(sub_ent)

        if with_self:
            final_ents.add(self)

        while stk:
            ent = stk.popleft()

            final_ents.add(ent)

            for sub_ent in ent.get_stacked_ents():
                if sub_ent.get_loop_flag() != flag:
                    sub_ent.set_loop_flag(flag)
                    stk.append(sub_ent)

        return final_ents
