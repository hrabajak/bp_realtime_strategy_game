import random
import math

from game.config import Config
from game.helper.line import make_line
from game.interface.itilecollector import ITileCollector
from game.map.mapsnapshot import MapSnapshot
from game.map.maptiletexture import MapTileTexture
from game.map.mapwave import MapWave
from game.map.pathfinder import PathFinder


class MapGenerator:

    _tile_col = None
    _path_fnd = None
    _map_wave = None
    _first_tiles = None

    def __init__(self, tile_col: ITileCollector):
        self._tile_col = tile_col
        self._path_fnd = PathFinder(self._tile_col)
        self._map_wave = MapWave(self._tile_col)

    def get_first_tiles(self):
        return self._first_tiles

    def generate(self, seed):
        self._first_tiles = []

        random.seed(seed)

        for tile in self._tile_col.get_tiles():
            tile.areaId = 0

        idx_list_count = 5
        idx_list = []

        for idx in range(1, idx_list_count + 1):
            idx_list.append({
                'idx': idx,
                'firstTile': None
            })

        for c in idx_list:
            while True:
                px = random.randint(1, self._tile_col.get_tiles_width() - 2)
                py = random.randint(1, self._tile_col.get_tiles_height() - 2)

                if self._tile_col.get_tile(px, py).area_id == 0:
                    break

            size = random.randint(500, 800)

            area_tiles = self.generate_area(self._tile_col.get_tile(px, py), size)
            tree_size = int(math.sqrt(size) * (1.0 + random.random()))
            fields_size = size // 3

            while tree_size > 5:
                self.generate_trees(area_tiles, tree_size)
                tree_size = tree_size // 2

            field_tiles = self.generate_fields(area_tiles, fields_size)

            fd_x = sum((c.get_x() for c in field_tiles)) / len(field_tiles)
            fd_y = sum((c.get_y() for c in field_tiles)) / len(field_tiles)

            self._first_tiles.append(self._tile_col.get_tile_by_coords(fd_x, fd_y))

            for tile in area_tiles:
                tile.area_id = c['idx']

            c['firstTile'] = area_tiles[0]

        for c in idx_list:
            tiles_final = None

            for tc in idx_list:
                if c['idx'] != tc['idx'] and not self.is_areas_connected(c['firstTile'], tc['idx']):

                    tiles = self.join_two_areas(c['firstTile'], c['idx'], tc['idx'])

                    if tiles is not None and (tiles_final is None or len(tiles_final) > len(tiles)):
                        tiles_final = tiles

            if tiles_final is not None:
                for tile in tiles_final:
                    [self._join_step_tile(x) for x in tile.get_neighbors()]

    def generate_b(self, seed, p_count):

        random.seed(seed)

        self._first_tiles = []

        p_list = []

        mid_x = self._tile_col.get_tiles_width() // 2
        mid_y = self._tile_col.get_tiles_height() // 2

        ang = math.pi * 2 * random.random()
        ang_step = (math.pi * 2) / p_count

        radius_x = (self._tile_col.get_tiles_width() // 2) - 4
        radius_y = (self._tile_col.get_tiles_height() // 2) - 4

        for idx in range(0, p_count):
            c_ang = ang + ang_step * idx
            pt_x = round(mid_x + math.cos(c_ang) * radius_x)
            pt_y = round(mid_y + math.sin(c_ang) * radius_y)

            p_list.append({
                'start_x': pt_x,
                'start_y': pt_y,
                'bef_x': pt_x,
                'bef_y': pt_y,
                'ang': c_ang
            })

            tile = self._tile_col.get_tile(pt_x, pt_y)
            tls = self.generate_area(tile, 120, MapTileTexture.T_GRASS, 0, lambda tl: tl.gen_flag == 0)
            [c.set_gen_flag(1) for c in tls]

            self._first_tiles.append(tile)

            for fc in range(0, 2):
                # fields generation
                while self._tile_col.get_tile(pt_x, pt_y).gen_flag != 0:
                    c_ang += 0.01 * (-1) ** (fc + 1)
                    pt_x = round(mid_x + math.cos(c_ang) * (self._tile_col.get_tiles_width() // 2 - 4))
                    pt_y = round(mid_y + math.sin(c_ang) * (self._tile_col.get_tiles_height() // 2 - 4))

                tile = self._tile_col.get_tile(pt_x, pt_y)
                tls = self.generate_area(tile, 60, MapTileTexture.T_FIELD, 0, lambda tl: tl.gen_flag == 0)
                [c.set_gen_flag(2) for c in tls]

            for ec in range(0, 2):
                # edge outline
                tile = self._tile_col.get_tile(p_list[idx]['start_x'], p_list[idx]['start_y'])
                tls = self.fill_edges(tile, MapTileTexture.T_GRASS, lambda tl: tl.gen_flag != 0)
                [c.set_gen_flag(3 + ec) for c in tls]

        while radius_x > 0 and radius_y > 0:
            ang_inc = (math.pi / 2) * random.random() - math.pi / 4

            radius_x = max(radius_x - 10, 0)
            radius_y = max(radius_y - 10, 0)

            for idx in range(0, p_count):
                c_ang = p_list[idx]['ang'] + ang_inc
                pt_x = round(mid_x + math.cos(c_ang) * radius_x)
                pt_y = round(mid_y + math.sin(c_ang) * radius_y)

                make_line(p_list[idx]['bef_x'], p_list[idx]['bef_y'], pt_x, pt_y, self._line_step)

                p_list[idx]['bef_x'] = pt_x
                p_list[idx]['bef_y'] = pt_y

        # random noise grass

        for fy in range(1, self._tile_col.get_tiles_height() - 1):
            for fx in range(1, self._tile_col.get_tiles_width() - 1):
                tile = self._tile_col.get_tile(fx, fy)

                if random.randint(0, 100) <= 55 and tile.gen_flag == 0:
                    tile.set_type(MapTileTexture.T_GRASS)

        for i in range(0, 2):
            self._cell_iter_land()

        # random noise tree

        for fy in range(1, self._tile_col.get_tiles_height() - 1):
            for fx in range(1, self._tile_col.get_tiles_width() - 1):
                tile = self._tile_col.get_tile(fx, fy)

                if random.randint(0, 100) <= 50 and tile.gen_flag == 0 and tile.get_type() == MapTileTexture.T_GRASS:
                    tile.set_type(MapTileTexture.T_TREE)

        for i in range(0, 1):
            self._cell_iter_tree()

        # update deep tree and water edges

        for fy in range(1, self._tile_col.get_tiles_height() - 1):
            for fx in range(1, self._tile_col.get_tiles_width() - 1):
                tile = self._tile_col.get_tile(fx, fy)

                if tile.get_type() == MapTileTexture.T_TREE:
                    trees_num = sum((1 for c in tile.get_neighbors() if c.get_type() in [MapTileTexture.T_TREE, MapTileTexture.T_DEEP_TREE]))
                    water_num = sum((1 for c in tile.get_neighbors() if c.get_type() in [MapTileTexture.T_WATER]))

                    if water_num > 0:
                        tile.set_type(MapTileTexture.T_WATER_TREE)

                    elif trees_num >= 6:
                        tile.set_type(MapTileTexture.T_DEEP_TREE)

                elif tile.get_type() == MapTileTexture.T_GRASS:
                    water_num = sum((1 for c in tile.get_neighbors() if c.get_type() in [MapTileTexture.T_WATER]))

                    if water_num > 0:
                        tile.set_type(MapTileTexture.T_WATERGRASS)

    def _line_step(self, ux, uy):
        for s_fy in range(-2, 3):
            for s_fx in range(-2, 3):
                tile = self._tile_col.get_tile(ux + s_fx, uy + s_fy)

                if tile is not None and not tile.is_edge() and tile.gen_flag == 0:
                    tile.set_type(MapTileTexture.T_GRASS)
                    tile.gen_flag = 100

    def _cell_iter_land(self):
        for fy in range(1, self._tile_col.get_tiles_height() - 1):
            for fx in range(1, self._tile_col.get_tiles_width() - 1):
                cur_tile = self._tile_col.get_tile(fx, fy)

                if cur_tile.gen_flag == 0:
                    w_count = 0

                    for s_fy in range(-1, 2):
                        for s_fx in range(-1, 2):
                            tile = self._tile_col.get_tile(fx + s_fx, fy + s_fy)

                            if tile is None or tile.get_type() in [MapTileTexture.T_WATER, MapTileTexture.T_BLANK]:
                                w_count += 1

                    if w_count >= 5:
                        cur_tile.set_type(MapTileTexture.T_WATER)
                    else:
                        cur_tile.set_type(MapTileTexture.T_GRASS)

    def _cell_iter_tree(self):
        for fy in range(1, self._tile_col.get_tiles_height() - 1):
            for fx in range(1, self._tile_col.get_tiles_width() - 1):
                cur_tile = self._tile_col.get_tile(fx, fy)

                if cur_tile.gen_flag == 0 and cur_tile.get_type() in [MapTileTexture.T_TREE, MapTileTexture.T_GRASS]:
                    w_count = 0

                    for s_fy in range(-1, 2):
                        for s_fx in range(-1, 2):
                            tile = self._tile_col.get_tile(fx + s_fx, fy + s_fy)

                            if tile is None or tile.get_type() in [MapTileTexture.T_WATER, MapTileTexture.T_BLANK, MapTileTexture.T_TREE]:
                                w_count += 1

                    if w_count >= 5:
                        cur_tile.set_type(MapTileTexture.T_TREE)
                    else:
                        cur_tile.set_type(MapTileTexture.T_GRASS)

    def _join_step_tile(self, tl):
        self.generate_area(tl, random.randint(2, 5))

    def generate_area(self, tile, quantity, tp_set=MapTileTexture.T_GRASS, alg=0, check_callback=lambda tl: True):
        self._tile_col.clear_tiles_flags()

        stk = [tile]
        out = []

        while len(stk) > 0 and quantity > 0:
            if alg == 0:
                idx = random.randint(0, len(stk) - 1)
                tile = stk[idx]
                del stk[idx]

            else:
                tile = stk[0]
                del stk[0]

            if tile.get_type() != tp_set:
                tile.set_type(tp_set)
                out += [tile]
                quantity -= 1

            for sub_tile in tile.get_neighbors():
                if sub_tile.area_id == 0 and not sub_tile.is_edge() and check_callback(sub_tile) and sub_tile.get_type() != tp_set:
                    sub_tile.set_type(tp_set)
                    out += [sub_tile]
                    quantity -= 1

            tile.flag = 1

            for sub_tile in tile.get_neighbors():
                if sub_tile.flag == 0 and sub_tile.area_id == 0 and check_callback(sub_tile) and not sub_tile.is_edge():
                    stk.append(sub_tile)
                    sub_tile.flag = 1

        return out

    def generate_trees(self, tiles, quantity):
        return self.generate_any(tiles, quantity, MapTileTexture.T_TREE, [MapTileTexture.T_GRASS])

    def generate_fields(self, tiles, quantity):
        return self.generate_any(tiles, quantity, MapTileTexture.T_FIELD, [MapTileTexture.T_GRASS, MapTileTexture.T_TREE, MapTileTexture.T_DUST])

    def generate_any(self, tiles, quantity, gen_type, nb_types):
        self._tile_col.clear_tiles_flags()

        av_tiles = [] + tiles
        av_tile = None

        while len(av_tiles) > 0:
            av_idx = random.randint(0, len(av_tiles) - 1)
            av_tile = av_tiles[av_idx]

            if av_tile.all_neighbor_are(lambda tile: tile.get_type() == 1):
                break
            else:
                del av_tiles[av_idx]

        stk = []
        out = []

        if av_tile is not None:
            stk.append(av_tile)

        while len(stk) > 0 and quantity > 0:
            idx = random.randint(0, len(stk) - 1)
            tile = stk[idx]
            del stk[idx]

            tile.set_type(gen_type)
            tile.flag = 1
            quantity -= 1
            out.append(tile)

            for sub_tile in tile.get_neighbors():
                if sub_tile.flag == 0 and not sub_tile.is_edge() and sub_tile.area_id == av_tile.area_id and sub_tile.all_neighbor_are(lambda tile: tile.get_type() in nb_types or tile.flag == 1):
                    stk.append(sub_tile)

        return out

    def fill_edges(self, tile, tp_set, step_callback=lambda tl: True):
        self._tile_col.clear_tiles_flags()

        stk = [tile]
        out = []

        while len(stk) > 0:
            tile = stk[0]
            del stk[0]

            for nb in tile.get_neighbors():
                if not nb.is_edge() and not step_callback(nb):
                    nb.set_type(tp_set)
                    out.append(nb)

            tile.flag = 1

            for sub_tile in tile.get_neighbors():
                if sub_tile.flag == 0 and step_callback(sub_tile):
                    stk.append(sub_tile)
                    sub_tile.flag = 1

        return out

    def is_areas_connected(self, tile_from, idx_to):
        return self._map_wave.wave_find(tile_from, lambda tile: tile.area_id == idx_to, lambda tile: tile.get_type() in [1, 3])

    def join_two_areas(self, tile_from, idx_from, idx_to):
        # nalez hran oblasti
        nbs = self._map_wave.wave_find_edges(tile_from, lambda tile: tile.area_id != idx_from)

        # nalez nejblizsiho pole od hran
        tile_to = self._map_wave.find_nearest_tile_from_tiles(nbs, lambda tile: tile.area_id == idx_to, lambda tile: tile.area_id == 0)

        if tile_to is not None:
            # cesta od nejblizsiho pole zpet k nejake hrane
            tile_from = self._path_fnd.path_find_tile(tile_to, lambda tile: tile.area_id == idx_from, lambda tile: tile.area_id == 0)

            if tile_from is not None:
                return self._path_fnd.back_list(tile_from, tile_to)

        return None
