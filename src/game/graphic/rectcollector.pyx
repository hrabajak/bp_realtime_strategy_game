import pyglet

from game.graphic.quadcollector cimport QuadCollector
from game.graphic.rect import Rect
from game.image.imagecollector import ImageCollector
from game.interface.iview import IView
from lib import pyshaders


cdef class RectCollector(QuadCollector):

    cdef:
        object _view

        object _rects
        object _rects_dis
        long _rebuild_tick

        object _tex_id

    def __init__(self, view: IView):
        QuadCollector.__init__(self)

        self._view = view

        self._rects = []
        self._rects_dis = []

        self._rebuild_tick = 0
        self._tex_id = ImageCollector().get_images_all()[0].id

        vertex_shader = """
        #version 130
        in vec4 vert;
        in vec3 colors;
        in vec3 uvs;
        in float op;

        out vec3 vert_colors;
        out vec3 vert_uvs;
        out float vert_op;

        uniform mat4 projection = mat4(1.0, 0, 0, 0,  0, 1.0, 0, 0,  0, 0, 1.0, 0,  0, 0, 0, 1.0);
        uniform mat4 trans;

        void main()
        {
          gl_Position = projection * trans * vert;

          vert_colors = colors;
          vert_uvs = uvs;
          vert_op = op;
        }
        """

        fragment_shader = """
        #version 130

        in vec3 vert_colors;
        in vec3 vert_uvs;
        in float vert_op;

        out vec4 color_frag;
        
        uniform sampler2D tex;

        void main()
        {
          //mat2 mymat = mat2(0, -1, 1, 0);
          //mat2 mymat = mat2(1, 0, 0, 1);
          //float sin_factor = sin(0.01);
          //float cos_factor = cos(0.01);
          //mat2 mymat = mat2(cos_factor, sin_factor, -sin_factor, cos_factor);
                     
          color_frag = texture2D(tex, vec2(vert_uvs)) * vec4(vert_colors, vert_op);
        }
        """

        pyshaders.transpose_matrices(False)
        self._shad = pyshaders.from_string(vertex_shader, fragment_shader)

    def get_rects(self):
        return self._rects

    def _extend(self, downsize=False):
        self._quad_extend(len(self._rects), downsize)
        [rc.update_arrays() for rc in self._rects]

        return True

    def create(self, img=None):
        self._rebuild_tick = 60

        if len(self._rects_dis) > 0:
            rect = self._rects_dis.pop(0)
            rect.init(img)

        else:
            while len(self._rects) + 1 > self._len:
                self._extend()

            rect = Rect(self, len(self._rects))
            rect.init(img)

        self._rects.append(rect)

        return rect

    def discard(self, rect):
        self._rects.remove(rect)
        self._rects_dis.append(rect)
        self._rebuild_tick = 60

    def rebuild(self):
        self._rects.sort(key=lambda rc: rc.get_index(), reverse=False)

        for idx in range(0, len(self._rects)):
            self._rects[idx].set_index(idx)

        self._rects_dis.clear()
        self._extend(True)

    def draw_logic(self):
        if self._rebuild_tick > 0:
            self._rebuild_tick -= 1

            if self._rebuild_tick == 0:
                self._rebuild_tick = 0
                self.rebuild()

        self.rebuild()

    def draw(self):
        if len(self._pts) > 0:
            pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
            pyglet.gl.glEnable(pyglet.gl.GL_TEXTURE_2D)
            pyglet.gl.glBindTexture(pyglet.gl.GL_TEXTURE_2D, self._tex_id)

            with self._shad.using():

                self._shad.uniforms.projection = [self._view.get_mat()]
                self._shad.uniforms.trans = [self._view.get_trans_mat()]
                self._shad.uniforms.tex = self._tex_id

                self._shad.enable_all_attributes()

                if 'vert' in self._shad.attributes:
                    self._shad.attributes['vert'].point_to(self._pts, pyglet.gl.GL_FLOAT, 2)

                if 'colors' in self._shad.attributes:
                    self._shad.attributes['colors'].point_to(self._cols, pyglet.gl.GL_FLOAT, 3)

                if 'uvs' in self._shad.attributes:
                    self._shad.attributes['uvs'].point_to(self._uvs, pyglet.gl.GL_FLOAT, 3)

                if 'op' in self._shad.attributes:
                    self._shad.attributes['op'].point_to(self._ops, pyglet.gl.GL_FLOAT, 1)

                pyglet.gl.glDrawArrays(pyglet.gl.GL_QUADS, 0, len(self._pts) // 2)

                self._shad.disable_all_attributes()

            pyglet.gl.glDisable(pyglet.gl.GL_BLEND)
            pyglet.gl.glDisable(pyglet.gl.GL_TEXTURE_2D)
