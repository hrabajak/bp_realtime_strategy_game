import pyglet

from game.graphic.graphbatches import GraphBatches


class Primitive:

    PRIM_CIRCLE = 0
    PRIM_LINE = 1
    PRIM_TEXT = 2

    _draw_init = False
    _ref = None
    _parent = None
    _tp = 0
    _x = 0
    _y = 0
    _dt = None
    _alive = False

    def __init__(self, parent, tp, x, y, dt):
        self._draw_init = True
        self._ref = None
        self._parent = parent
        self._tp = tp
        self._x = x
        self._y = y
        self._dt = dt
        self._alive = True

    def drop(self):
        self._alive = False

    def destroy(self):
        self._ref.delete()
        self._ref = None

    def get_is_match(self, tp, x, y):
        return self._tp == tp and self._x == x and self._y == y

    def get_sprite_x(self):
        return self._x * self._parent.get_zoom() + self._parent.get_x()

    def get_sprite_y(self):
        return self._y * self._parent.get_zoom() + self._parent.get_y()

    def get_sprite_to_x(self):
        return self._dt['to_x'] * self._parent.get_zoom() + self._parent.get_x()

    def get_sprite_to_y(self):
        return self._dt['to_y'] * self._parent.get_zoom() + self._parent.get_y()

    def set_coords(self, x, y):
        self._x = x
        self._y = y

    def set_data(self, dt):
        self._dt = dt

    def draw(self):
        if self._draw_init:
            self._draw_init = False

            if self._tp == Primitive.PRIM_CIRCLE:
                self._ref = pyglet.shapes.Circle(self.get_sprite_x(), self.get_sprite_y(), self._dt['radius'] * self._parent.get_zoom(), color=self._dt['color'], batch=GraphBatches().get_batch_gui(), group=GraphBatches().get_group_gui())

            elif self._tp == Primitive.PRIM_LINE:
                self._ref = pyglet.shapes.Line(self.get_sprite_x(), self.get_sprite_y(), self.get_sprite_to_x(), self.get_sprite_to_y(), width=2, color=self._dt['color'], batch=GraphBatches().get_batch_gui(), group=GraphBatches().get_group_gui())

            elif self._tp == Primitive.PRIM_TEXT:
                self._ref = pyglet.text.Label(self._dt['text'], font_name='Times New Roman', font_size=9, x=self.get_sprite_x(), y=self.get_sprite_y(), anchor_x='center', anchor_y='center', batch=GraphBatches().get_batch_gui(), group=GraphBatches().get_group_gui_text())

        if self._tp == Primitive.PRIM_CIRCLE:
            self._ref.x = self.get_sprite_x()
            self._ref.y = self.get_sprite_y()
            self._ref.radius = self._dt['radius'] * self._parent.get_zoom()

        elif self._tp == Primitive.PRIM_LINE:
            self._ref.x = self.get_sprite_x()
            self._ref.y = self.get_sprite_y()
            self._ref.x2 = self.get_sprite_to_x()
            self._ref.y2 = self.get_sprite_to_y()

        elif self._tp == Primitive.PRIM_TEXT:
            self._ref.text = self._dt['text']
            self._ref.x = self.get_sprite_x()
            self._ref.y = self.get_sprite_y()

        return not self._alive
