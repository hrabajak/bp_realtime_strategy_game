from libc.math cimport cos, sin, M_PI

from game.graphic.rect cimport Rect
from game.helper.vector import angle_to, dist_to
from game.image.imagecollector import ImageCollector


cdef class EnvRect(Rect):

    TYPE_TREE = 0
    TYPE_DEEP_TREE = 1
    TYPE_BUSH = 2
    TYPE_DEEP_BUSH = 3

    cdef:
        double _rc_x
        double _rc_y
        double _rc_sz
        double _d_sz
        long _tp
        long _zindex
        long _seed
        long _num
        long _scalar
        double _angle
        object _subs

    def __cinit__(self, parent, idx):
        Rect.__init__(self, parent, idx)

        self._rc_x = 0
        self._rc_y = 0
        self._rc_sz = 0
        self._d_sz = 0
        self._tp = 0
        self._zindex = 0
        self._seed = 0
        self._num = 0
        self._scalar = 0
        self._angle = 0
        self._subs = None

    def discard(self):
        if self._subs is not None:
            for b in self._subs:
                b.discard()

            self._subs = None

        super().discard()

    def get_zindex(self):
        return self._zindex

    def set_type(self, tp, seed=0):
        self._tp = tp
        self._seed = seed
        self._angle = (self._seed / 40) * (M_PI * 2)

        if tp in [EnvRect.TYPE_TREE, EnvRect.TYPE_DEEP_TREE]:
            self._num = self._seed % 5
            self._zindex = 1

            self.init(ImageCollector().get_env_image("envtree", self._num))
            self._rc_sz = 55 if tp == EnvRect.TYPE_DEEP_TREE else 40

        elif tp in [EnvRect.TYPE_BUSH, EnvRect.TYPE_DEEP_BUSH]:
            self._num = self._seed % 4
            self._zindex = 20

            self.init(ImageCollector().get_env_image("envbush", self._num))
            self._rc_sz = 24 + self._seed % 7 if tp == EnvRect.TYPE_DEEP_BUSH else 18 + self._seed % 7
            self._d_sz = 2

    def create_sub(self):
        cdef double b_px, b_py, ang, dist, orig_dist
        cdef object bushes, b

        if self._tp in [EnvRect.TYPE_TREE, EnvRect.TYPE_DEEP_TREE]:
            bushes = []

            if self._num == 0:
                bushes.append((13, 10))
                bushes.append((8, 37))
                bushes.append((23, 51))
                bushes.append((47, 37))
                bushes.append((37, 10))

            elif self._num == 1:
                bushes.append((14, 14))
                bushes.append((34, 9))
                bushes.append((38, 39))
                bushes.append((17, 44))

            elif self._num == 2:
                bushes.append((10, 17))
                bushes.append((27, 8))
                bushes.append((42, 21))
                bushes.append((35, 44))
                bushes.append((15, 42))

            elif self._num == 3:
                bushes.append((6, 11))
                bushes.append((28, 6))
                bushes.append((42, 23))
                bushes.append((38, 42))
                bushes.append((11, 40))

            elif self._num == 4:
                bushes.append((7, 9))
                bushes.append((28, 11))
                bushes.append((35, 34))
                bushes.append((15, 38))

            if self._tp == EnvRect.TYPE_DEEP_TREE:
                bushes.append((self._img.width / 2, self._img.height / 2))

            self._subs = []
            orig_dist = dist_to(0, 0, self._img.width / 2, self._img.height / 2)

            b_idx = 0

            for c in bushes:
                use_x = c[0] + (self._scalar + b_idx) % 4 - 2
                use_y = c[1] + (self._scalar + b_idx) % 4 - 2

                ang = angle_to(self._img.width / 2, self._img.height / 2, use_x, self._img.height - use_y)
                dist = dist_to(self._img.width / 2, self._img.height / 2, use_x, self._img.height - use_y)
                b_px = self._rc_x + sin(ang + self._angle) * (dist / orig_dist) * self._rc_sz
                b_py = self._rc_y + cos(ang + self._angle) * (dist / orig_dist) * self._rc_sz

                b = self._parent.create(EnvRect.TYPE_DEEP_BUSH if self._tp == EnvRect.TYPE_DEEP_TREE else EnvRect.TYPE_BUSH)
                b.set_pos(b_px, b_py)

                self._subs.append(b)
                b_idx += 1

    def set_pos(self, x, y):
        self._rc_x = x
        self._rc_y = y
        self.update()

    def update(self):
        self._scalar += 1

        # 0 - left, bottom
        # 1 - right, bottom
        # 2 - right, top
        # 3 - left, top

        if self._tp == EnvRect.TYPE_TREE or self._tp == EnvRect.TYPE_DEEP_TREE:
            self._pts[self._idx * self._rect_pts_size + 0] = self._rc_x + sin(M_PI + M_PI / 4 + self._angle) * self._rc_sz
            self._pts[self._idx * self._rect_pts_size + 1] = self._rc_y + cos(M_PI + M_PI / 4 + self._angle) * self._rc_sz

            self._pts[self._idx * self._rect_pts_size + 2] = self._rc_x + sin(M_PI / 2 + M_PI / 4 + self._angle) * self._rc_sz
            self._pts[self._idx * self._rect_pts_size + 3] = self._rc_y + cos(M_PI / 2 + M_PI / 4 + self._angle) * self._rc_sz

            self._pts[self._idx * self._rect_pts_size + 4] = self._rc_x + sin(M_PI / 4 + self._angle) * self._rc_sz
            self._pts[self._idx * self._rect_pts_size + 5] = self._rc_y + cos(M_PI / 4 + self._angle) * self._rc_sz

            self._pts[self._idx * self._rect_pts_size + 6] = self._rc_x + sin(M_PI + M_PI / 2 + M_PI / 4 + self._angle) * self._rc_sz
            self._pts[self._idx * self._rect_pts_size + 7] = self._rc_y + cos(M_PI + M_PI / 2 + M_PI / 4 + self._angle) * self._rc_sz

        else:
            self._pts[self._idx * self._rect_pts_size + 0] = self._rc_x + sin(M_PI + M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 20) * self._d_sz)
            self._pts[self._idx * self._rect_pts_size + 1] = self._rc_y + cos(M_PI + M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 20) * self._d_sz)

            self._pts[self._idx * self._rect_pts_size + 2] = self._rc_x + sin(M_PI / 2 + M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 25) * self._d_sz)
            self._pts[self._idx * self._rect_pts_size + 3] = self._rc_y + cos(M_PI / 2 + M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 25) * self._d_sz)

            self._pts[self._idx * self._rect_pts_size + 4] = self._rc_x + sin(M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 15) * self._d_sz)
            self._pts[self._idx * self._rect_pts_size + 5] = self._rc_y + cos(M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 15) * self._d_sz)

            self._pts[self._idx * self._rect_pts_size + 6] = self._rc_x + sin(M_PI + M_PI / 2 + M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 30) * self._d_sz)
            self._pts[self._idx * self._rect_pts_size + 7] = self._rc_y + cos(M_PI + M_PI / 2 + M_PI / 4 + self._angle) * (self._rc_sz + sin(self._scalar / 30) * self._d_sz)

        return False
