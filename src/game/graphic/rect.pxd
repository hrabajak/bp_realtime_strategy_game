

cdef class Rect:
    cdef:
        object _parent
        object _idx
        object _img

        double _opacity
        double _col_r
        double _col_g
        double _col_b

        int _pts_size
        int _rect_pts_size

        object _pts
        object _cols
        object _uvs
        object _ops
