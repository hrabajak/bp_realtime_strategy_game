import pyglet


cdef class QuadCollector:

    def __cinit__(self):
        self._len = 0
        self._pts_size = 2
        self._pts = (pyglet.gl.GLfloat * 0)(*[])
        self._cols = (pyglet.gl.GLfloat * 0)(*[])
        self._uvs = (pyglet.gl.GLfloat * 0)(*[])
        self._ops = (pyglet.gl.GLfloat * 0)(*[])

    def get_arrays(self):
        return self._pts, self._cols, self._uvs, self._ops

    def get_pts_size(self):
        return self._pts_size

    def _quad_extend(self, long quads_count, int downsize=False):
        cdef long n_len, sb, i, ib

        _pts = list(self._pts)
        _cols = list(self._cols)
        _uvs = list(self._uvs)
        _ops = list(self._ops)

        if downsize:
            n_len = 2

            while n_len < quads_count:
                n_len = n_len * 2

        elif self._len == 0:
            n_len = 2
        else:
            n_len = self._len * 2

        if downsize:
            for i in range(quads_count * 4, self._len * 4):
                for sb in range(0, 2):
                    _pts[i * self._pts_size + sb] = 0

                for sb in range(0, 3):
                    _cols[i * 3 + sb] = 0
                    _uvs[i * 3 + sb] = 0

                _ops[i] = 0

        for i in range(0, (n_len - self._len) * 4):
            for ib in range(0, self._pts_size):
                _pts.extend([0])

            _cols.extend([0, 0, 0])
            _uvs.extend([0, 0, 0])
            _ops.extend([0])

        del _pts[n_len * 4 * self._pts_size:]
        del _cols[n_len * 4 * 3:]
        del _uvs[n_len * 4 * 3:]
        del _ops[n_len * 4 * 2:]

        self._pts = (pyglet.gl.GLfloat * len(_pts))(*_pts)
        self._cols = (pyglet.gl.GLfloat * len(_cols))(*_cols)
        self._uvs = (pyglet.gl.GLfloat * len(_uvs))(*_uvs)
        self._ops = (pyglet.gl.GLfloat * len(_ops))(*_ops)
        self._len = n_len

        return True
