import math


cdef class Rect:

    def __cinit__(self, parent, idx):
        self._parent = parent
        self._idx = idx
        self._pts_size = self._parent.get_pts_size()
        self._rect_pts_size = 4 * self._pts_size
        self._opacity = 1.0
        self._col_r = 1.0
        self._col_g = 1.0
        self._col_b = 1.0

    def init(self, img=None):
        self.update_arrays()
        self._img = img
        self.update_data()

    def update_arrays(self):
        self._pts, self._cols, self._uvs, self._ops = self._parent.get_arrays()

    def update_data(self):
        cdef long i

        for i in range(0, 4):
            self._cols[self._idx * 12 + i * 3 + 0] = self._col_r
            self._cols[self._idx * 12 + i * 3 + 1] = self._col_g
            self._cols[self._idx * 12 + i * 3 + 2] = self._col_b

        for i in range(0, 4):
            self._ops[self._idx * 4 + i] = self._opacity

        if self._img is not None:
            cd = self._img.tex_coords

            for i in range(0, 4):
                tx = cd[i * 3 + 0]
                ty = cd[i * 3 + 1]

                self._uvs[self._idx * 12 + i * 3 + 0] = tx
                self._uvs[self._idx * 12 + i * 3 + 1] = ty
                self._uvs[self._idx * 12 + i * 3 + 2] = cd[i * 3 + 2]

    def get_index(self):
        return self._idx

    def set_index(self, idx):
        cdef long i

        if self._idx != idx:
            for i in range(0, self._rect_pts_size):
                self._pts[idx * self._rect_pts_size + i] = self._pts[self._idx * self._rect_pts_size + i]

            self._idx = idx
            self.update_data()

    def discard(self):
        cdef long i

        for i in range(0, self._rect_pts_size):
            self._pts[self._idx * self._rect_pts_size + i] = 0.0

        for i in range(0, 4):
            self._ops[self._idx * 4 + i] = 0.0

        self._parent.discard(self)

    def set_coords_rect(self, double x, double y, double sz):

        # 0 - left, bottom
        # 1 - right, bottom
        # 2 - right, top
        # 3 - left, top

        self._pts[self._idx * self._rect_pts_size + 0] = x - sz
        self._pts[self._idx * self._rect_pts_size + 1] = y - sz

        self._pts[self._idx * self._rect_pts_size + 2] = x + sz
        self._pts[self._idx * self._rect_pts_size + 3] = y - sz

        self._pts[self._idx * self._rect_pts_size + 4] = x + sz
        self._pts[self._idx * self._rect_pts_size + 5] = y + sz

        self._pts[self._idx * self._rect_pts_size + 6] = x - sz
        self._pts[self._idx * self._rect_pts_size + 7] = y + sz

    def set_coords_rect_two(self, double x, double y, double width, double height):

        # 0 - left, bottom
        # 1 - right, bottom
        # 2 - right, top
        # 3 - left, top

        self._pts[self._idx * self._rect_pts_size + 0] = x - width
        self._pts[self._idx * self._rect_pts_size + 1] = y - height

        self._pts[self._idx * self._rect_pts_size + 2] = x + width
        self._pts[self._idx * self._rect_pts_size + 3] = y - height

        self._pts[self._idx * self._rect_pts_size + 4] = x + width
        self._pts[self._idx * self._rect_pts_size + 5] = y + height

        self._pts[self._idx * self._rect_pts_size + 6] = x - width
        self._pts[self._idx * self._rect_pts_size + 7] = y + height

    def set_coords_rect_sizes(self, double x, double y, double size0, double size1, double size2, double size3):

        # 0 - left, bottom
        # 1 - right, bottom
        # 2 - right, top
        # 3 - left, top

        self._pts[self._idx * self._rect_pts_size + 0] = x - size0
        self._pts[self._idx * self._rect_pts_size + 1] = y - size0

        self._pts[self._idx * self._rect_pts_size + 2] = x + size1
        self._pts[self._idx * self._rect_pts_size + 3] = y - size1

        self._pts[self._idx * self._rect_pts_size + 4] = x + size2
        self._pts[self._idx * self._rect_pts_size + 5] = y + size2

        self._pts[self._idx * self._rect_pts_size + 6] = x - size3
        self._pts[self._idx * self._rect_pts_size + 7] = y + size3

    def set_coords_line(self, double x1, double y1, double x2, double y2, double thick, double koef_from=0.0, double koef_to=1.0):

        # x1, y1 - to
        # x2, y2 - from

        cdef double vec_x = x1 - x2
        cdef double vec_y = y1 - y2
        cdef double vec_dst = max(1.0, math.sqrt(vec_x ** 2 + vec_y ** 2))
        cdef double n_vec_x = vec_y / vec_dst
        cdef double n_vec_y = - vec_x / vec_dst

        self._pts[self._idx * self._rect_pts_size + 0] = x2 + vec_x * koef_to + n_vec_x * thick
        self._pts[self._idx * self._rect_pts_size + 1] = y2 + vec_y * koef_to + n_vec_y * thick

        self._pts[self._idx * self._rect_pts_size + 2] = x2 + vec_x * koef_from + n_vec_x * thick
        self._pts[self._idx * self._rect_pts_size + 3] = y2 + vec_y * koef_from + n_vec_y * thick

        self._pts[self._idx * self._rect_pts_size + 4] = x2 + vec_x * koef_from - n_vec_x * thick
        self._pts[self._idx * self._rect_pts_size + 5] = y2 + vec_y * koef_from - n_vec_y * thick

        self._pts[self._idx * self._rect_pts_size + 6] = x2 + vec_x * koef_to - n_vec_x * thick
        self._pts[self._idx * self._rect_pts_size + 7] = y2 + vec_y * koef_to - n_vec_y * thick

    def set_coords_list(self, coords):
        cdef long i

        for i in range(0, self._rect_pts_size):
            self._pts[self._idx * self._rect_pts_size + i] = coords[i]

    def set_opacity(self, double opacity, double div=255):
        cdef long i

        self._opacity = opacity / div

        for i in range(0, 4):
            self._ops[self._idx * 4 + i] = self._opacity

    def set_opacity_kf(self, double opacity):
        cdef long i

        self._opacity = opacity

        for i in range(0, 4):
            self._ops[self._idx * 4 + i] = self._opacity

    def set_colors(self, double r, double g, double b, double div=255):
        cdef long i

        self._col_r = r / div
        self._col_g = g / div
        self._col_b = b / div

        for i in range(0, 4):
            self._cols[self._idx * 12 + i * 3 + 0] = self._col_r
            self._cols[self._idx * 12 + i * 3 + 1] = self._col_g
            self._cols[self._idx * 12 + i * 3 + 2] = self._col_b
