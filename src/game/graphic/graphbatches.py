from threading import Lock
import pyglet

from game.graphic.envcollector import EnvCollector
from game.graphic.groundcollector import GroundCollector
from game.graphic.rectcollector import RectCollector
from game.helper.singleton import Singleton
from game.interface.idrawable import IDrawable
from game.interface.iview import IView


class GraphBatches(metaclass=Singleton):

    _lock_sprite = None
    _sprite_delete = None
    _sprite_unset = None
    _sprite_recycle = None

    _lock_drw = None
    _drawables = None
    _dead_drawables = None
    _dead_flag = False
    _rects = None
    _grounds = None
    _envs = None

    _batch_map = None
    _batch = None
    _batch_black = None
    _batch_gui = None

    _group_map = None
    _group_below = None
    _group_below_up = None
    _group_first = None
    _group_second = None
    _group_third = None
    _group_build = None
    _group_build_second = None
    _group_build_third = None
    _group_particle = None
    _group_gui = None
    _group_gui_text = None

    _grp = None

    _force_rebuild = False
    _rebuild_tick = 0
    _tick = 0

    def __init__(self):
        self._lock_sprite = Lock()
        self._sprite_delete = []
        self._sprite_unset = []
        self._sprite_recycle = {}

        self._lock_drw = Lock()
        self._drawables = []
        self._dead_drawables = []
        self._dead_flag = True

        self._batch_map = pyglet.graphics.Batch()
        self._batch = pyglet.graphics.Batch()
        self._batch_black = pyglet.graphics.Batch()
        self._batch_gui = pyglet.graphics.Batch()

        btch = [
            self._batch_map,
            self._batch,
            self._batch_black,
            self._batch_gui
        ]

        self._group_below = pyglet.graphics.OrderedGroup(1)
        self._group_below_up = pyglet.graphics.OrderedGroup(2)
        self._group_first = pyglet.graphics.OrderedGroup(3)
        self._group_second = pyglet.graphics.OrderedGroup(4)
        self._group_third = pyglet.graphics.OrderedGroup(5)
        self._group_build = pyglet.graphics.OrderedGroup(6)
        self._group_build_second = pyglet.graphics.OrderedGroup(7)
        self._group_build_third = pyglet.graphics.OrderedGroup(8)
        self._group_particle = pyglet.graphics.OrderedGroup(9)
        self._group_gui = pyglet.graphics.OrderedGroup(10)
        self._group_gui_text = pyglet.graphics.OrderedGroup(11)

        grps = [
            self._group_below,
            self._group_below_up,
            self._group_first,
            self._group_second,
            self._group_third,
            self._group_build,
            self._group_build_second,
            self._group_build_third,
            self._group_particle,
            self._group_gui,
            None
        ]

        for b in btch:
            self._sprite_recycle[b] = {}

            for g in grps:
                self._sprite_recycle[b][g] = []

        self._force_rebuild = False
        self._rebuild_tick = 0
        self._tick = 0

    def init(self, view: IView):
        self._rects = RectCollector(view)
        self._grounds = GroundCollector(view)
        self._envs = EnvCollector(view)

    def get_batch(self):
        return self._batch

    def get_batch_map(self):
        return self._batch_map

    def get_batch_black(self):
        return self._batch_black

    def get_batch_gui(self):
        return self._batch_gui

    def get_group_below(self):
        return self._group_below

    def get_group_below_up(self):
        return self._group_below_up

    def get_group_first(self):
        return self._group_first

    def get_group_second(self):
        return self._group_second

    def get_group_third(self):
        return self._group_third

    def get_group_build(self):
        return self._group_build

    def get_group_build_second(self):
        return self._group_build_second

    def get_group_build_third(self):
        return self._group_build_third

    def get_group_particle(self):
        return self._group_particle

    def get_group_gui(self):
        return self._group_gui

    def get_group_gui_text(self):
        return self._group_gui_text

    def get_rects(self):
        return self._rects

    def get_grounds(self):
        return self._grounds

    def get_envs(self):
        return self._envs

    def set_force_rebuild(self):
        self._force_rebuild = True

    def add_sprite_delete(self, ref):
        with self._lock_sprite:
            if isinstance(ref, list):
                self._sprite_delete.extend([c for c in ref if c is not None])
            elif ref is not None:
                self._sprite_delete.append(ref)

    def add_batch_group(self, bb, gg):
        if bb not in self._sprite_recycle:
            self._sprite_recycle[bb] = {}

        if gg not in self._sprite_recycle[bb]:
            self._sprite_recycle[bb][gg] = []

    def create_sprite(self, **kwargs):
        with self._lock_sprite:
            batch = kwargs["batch"]
            group = None

            if "group" in kwargs:
                group = kwargs["group"]

            if len(self._sprite_recycle[batch][group]) == 0:
                if "subpixel" not in kwargs:
                    kwargs["subpixel"] = False # !!

                return pyglet.sprite.Sprite(**kwargs)

            else:
                # to recyklovani spritu v ramci batch+group ma nejaky dopad na vykon

                img = kwargs["img"]
                del kwargs["img"]

                if "batch" in kwargs:
                    del kwargs["batch"]

                if "group" in kwargs:
                    del kwargs["group"]

                if "subpixel" in kwargs:
                    del kwargs["subpixel"]

                kwargs["rotation"] = 0
                kwargs["scale_x"] = 1.0
                kwargs["scale_y"] = 1.0

                spr = self._sprite_recycle[batch][group].pop()
                spr.update(**kwargs)
                spr.image = img
                spr.opacity = 255
                spr.visible = True

                return spr

    def add_drawable(self, item: IDrawable):
        with self._lock_drw:
            self._drawables.append(item)

    def kill_drawable(self, item: IDrawable):
        with self._lock_drw:
            self._dead_drawables.append(item)
            self._dead_flag = True

    def rebuild(self):
        with self._lock_sprite:
            [spr.delete() for spr in self._sprite_unset]
            self._sprite_unset.clear()

    def draw_logic(self):
        """
        if self._tick % 120 == 0:
            print("drawables", len(self._drawables))
            print("sprite_delete", len(self._sprite_delete))
            print("sprite_unset", len(self._sprite_unset))
        """

        if self._rebuild_tick > 0:
            self._rebuild_tick -= 1

            if self._rebuild_tick == 0 or len(self._sprite_unset) > 100:
                self._rebuild_tick = 0
                self.rebuild()

        if len(self._sprite_delete) > 0:
            with self._lock_sprite:
                for spr in self._sprite_delete:
                    spr.visible = False
                    self._sprite_unset.append(spr)
                    #self._sprite_recycle[spr.batch][spr.group].append(spr)

                self._sprite_delete.clear()
                self._rebuild_tick = 60

        if self._dead_flag:
            # vyhazeni mrtvejch drawables

            with self._lock_drw:
                self._dead_flag = False

                for d in self._dead_drawables:
                    if d.may_draw >= 2:
                        d.draw_destroy()
                        self._drawables.remove(d)

                self._dead_drawables.clear()

                # Logger.get().log("Total drawables count: " + str(len(self._drawables)))

        for d in self._drawables:
            d.draw()

        self._grounds.draw_logic()
        self._envs.draw_logic()
        self._rects.draw_logic()

        self._tick += 1

        if self._force_rebuild:
            self._force_rebuild = False
            self.rebuild()
            self._rects.rebuild()
            self._grounds.rebuild()
            self._envs.rebuild()

    def draw(self):
        self._batch_map.draw()
        self._grounds.draw()
        self._batch.draw()
        self._envs.draw()
        self._rects.draw()
        self._batch_black.draw()

    def draw_simple(self):
        for d in self._drawables:
            d.draw()

        self._batch_map.draw()
        self._batch.draw()
        self._batch_black.draw()

    def draw_gui(self):
        self._batch_gui.draw()
