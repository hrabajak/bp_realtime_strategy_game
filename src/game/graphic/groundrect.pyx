import math

from game.graphic.rect cimport Rect


cdef class GroundRect(Rect):

    cdef:
        double _opacity_base

        long _decay
        long _decay_max

    def __cinit__(self, parent, idx):
        Rect.__init__(self, parent, idx)

        self._opacity_base = 1.0
        self._decay = 0
        self._decay_max = 0

    def set_decay(self, decay_max, opacity_base):
        self._decay_max = decay_max
        self._decay = decay_max
        self._opacity_base = opacity_base

    def update(self):
        if self._decay_max > 0:
            self._decay -= 1
            self.set_opacity_kf((self._decay / self._decay_max) * self._opacity_base)

            if self._decay == 0:
                return True

        return False
