from game.interface.idrawable import IDrawable


class DrawableBlank(IDrawable):
    def __init__(self):
        IDrawable.__init__(self)
        self.may_draw = 2

    def draw_destroy(self):
        pass

    def draw(self):
        pass
