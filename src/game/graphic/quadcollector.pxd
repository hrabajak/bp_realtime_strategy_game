

cdef class QuadCollector:
    cdef:
        long _len
        int _pts_size
        object _pts
        object _cols
        object _uvs
        object _ops
        object _shad
