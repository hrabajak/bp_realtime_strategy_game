import threading

from game.graphic.primitive import Primitive
from game.helper.singleton import Singleton
from game.interface.iview import IView


class PrimitiveCollector(metaclass=Singleton):

    _lock = None
    _items = []
    _x = 0
    _y = 0
    _zoom = 1.0

    def __init__(self):
        self._lock = threading.Lock()

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_zoom(self):
        return self._zoom

    def add_circle(self, x, y, radius, color=(255, 0, 0)):
        with self._lock:
            it = Primitive(self, Primitive.PRIM_CIRCLE, x, y, {'radius': radius, 'color': color})
            self._items.append(it)

        return it

    def update_circle(self, x, y, radius, color=(255, 0, 0)):
        with self._lock:
            is_any = False

            for it in [c for c in self._items]:
                if it.get_is_match(Primitive.PRIM_CIRCLE, x, y):
                    it.set_data({'radius': radius, 'color': color})
                    is_any = True

            if not is_any:
                it = Primitive(self, Primitive.PRIM_CIRCLE, x, y, {'radius': radius, 'color': color})
                self._items.append(it)

        return it

    def drop_circle(self, x, y):
        with self._lock:
            for it in [c for c in self._items]:
                if it.get_is_match(Primitive.PRIM_CIRCLE, x, y):
                    it.drop()

    def add_line(self, x, y, x2, y2, color=(255, 0, 0)):
        with self._lock:
            it = Primitive(self, Primitive.PRIM_LINE, x, y, {'to_x': x2, 'to_y': y2, 'color': color})
            self._items.append(it)

        return it

    def drop_text(self, x, y):
        with self._lock:
            for it in [c for c in self._items]:
                if it.get_is_match(Primitive.PRIM_TEXT, x, y):
                    it.drop()

    def add_text(self, x, y, text):
        with self._lock:
            it = Primitive(self, Primitive.PRIM_TEXT, x, y, {'text': text})
            self._items.append(it)

        return it

    def update_text(self, x, y, text):
        with self._lock:
            is_any = False

            for it in [c for c in self._items]:
                if it.get_is_match(Primitive.PRIM_TEXT, x, y):
                    it.set_data({'text': text})
                    is_any = True

            if not is_any:
                it = Primitive(self, Primitive.PRIM_TEXT, x, y, {'text': text})
                self._items.append(it)

        return it

    def clear(self):
        for it in self._items:
            it.drop()

    def draw(self):
        self._x, self._y, self._zoom = IView.get().get_view_vals()

        to_rem = set()

        for it in self._items:
            if it.draw():
                to_rem.add(it)

        if len(to_rem) > 0:
            with self._lock:
                for it in to_rem:
                    it.destroy()
                    self._items.remove(it)
