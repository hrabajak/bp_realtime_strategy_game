import math
import pyglet

from libc.stdlib cimport malloc, free

from game.image.imagecollector import ImageCollector
from game.interface.itilecollector import ITileCollector
from game.interface.iview import IView
from lib import pyshaders


cdef class GridArea:

    cdef:
        object _view
        object _tile_col
        object _images

        object _vlist_disp
        object _grid
        object _tex_ids

        int _allow_disp

        object _map_shad
        object _map_pts
        object _map_uvs
        object _map_cols
        object _map_op_hash

        object _black_shad
        object _black_pts
        object _black_cols
        object _black_bary
        object _black_op

        double _x
        double _y
        double _width
        double _height
        double _zoom
        double _tile_size

        int _do_update_flag
        long _bef_off_x
        long _bef_off_y
        long _bef_grid_width
        long _bef_grid_height
        long _bef_pt_count

    def __cinit__(self, view: IView, tile_col: ITileCollector):
        self._view = view
        self._tile_col = tile_col
        self._images = ImageCollector()

        self._allow_disp = False
        self._do_update_flag = False

        self._x = 0
        self._y = 0
        self._width = 0
        self._height = 0
        self._zoom = 1.0
        self._tile_size = 50.0
        self._tex_ids = set()

        self._do_update_flag = False
        self._bef_off_x = -1
        self._bef_off_y = -1
        self._bef_grid_width = 0
        self._bef_grid_height = 0
        self._bef_pt_count = 0

        vertex_shader = """
        #version 130
        in vec4 vert;
        in vec3 colors;
        in vec3 uvs;
        in float op;

        out vec3 vert_colors;
        out vec3 vert_uvs;
        out float vert_op;

        uniform mat4 projection = mat4(1.0, 0, 0, 0,  0, 1.0, 0, 0,  0, 0, 1.0, 0,  0, 0, 0, 1.0);
        uniform mat4 trans;

        void main()
        {
          gl_Position = projection * trans * vert;
          //gl_Position = projection * vert;

          vert_colors = colors;
          vert_uvs = uvs;
          vert_op = op;
        }
        """

        fragment_shader = """
        #version 130

        in vec3 vert_colors;
        in vec3 vert_uvs;
        in float vert_op;

        out vec4 color_frag;

        uniform sampler2D tex;

        void main()
        {
          color_frag = texture2D(tex, vec2(vert_uvs)) * vec4(vert_colors, vert_op);
        }
        """

        pyshaders.transpose_matrices(False)
        self._map_shad = pyshaders.from_string(vertex_shader, fragment_shader)

        vertex_shader = """
        #version 130
        in vec4 vert;
        in vec3 colors;
        in vec2 bary;
        in vec3 op;

        out vec3 vert_colors;
        out vec2 vert_bary;
        out vec3 vert_op;
        
        uniform mat4 projection = mat4(1.0, 0, 0, 0,  0, 1.0, 0, 0,  0, 0, 1.0, 0,  0, 0, 0, 1.0);
        uniform mat4 trans;

        void main()
        {
          gl_Position = projection * trans * vert;
          
          vert_colors = colors;
          vert_bary = bary;
          vert_op = op;
        }
        """

        fragment_shader = """
        #version 130

        in vec3 vert_colors;
        in vec2 vert_bary;
        in vec3 vert_op;
        
        out vec4 color_frag;
  
        void main()
        {
          float opa = vert_op.x - vert_bary.x * vert_bary.y * vert_op.y;
          color_frag = vec4(vert_colors, opa);
        }
        """

        pyshaders.transpose_matrices(False)
        self._black_shad = pyshaders.from_string(vertex_shader, fragment_shader)

    def force_update(self):
        self._do_update_flag = True

    def update(self):
        cdef size_t v_ptr
        cdef int do_update
        cdef long off_x, off_y, grid_width, grid_height, pt_count, fy, fx, idx, sub_idx, tex_id, c_tex_id
        cdef double st_x, st_y, en_x, en_y, sz
        cdef object tiles, tx, pts, uv, pts_disp, col_disp, black_pts, black_cols, black_bary, black_op

        self._x = self._view.get_x()
        self._y = self._view.get_y()
        self._zoom = self._view.get_zoom()
        self._tile_size = self._view.get_cur_tilesize()

        off_x = int(math.floor(self._x / self._tile_size))
        off_y = int(math.floor(self._y / self._tile_size))
        grid_width = math.ceil(self._view.get_width() / self._tile_size)
        grid_height = math.ceil(self._view.get_height() / self._tile_size)

        tiles = set()

        do_update = self._do_update_flag

        if grid_width != self._bef_grid_width or grid_height != self._bef_grid_height or self._allow_disp or do_update:

            do_update = True

            self._bef_grid_width = grid_width
            self._bef_grid_height = grid_height
            self._do_update_flag = False

            if self._vlist_disp is not None:
                self._vlist_disp.delete()

            pt_count = 0
            pts = []
            uv = []
            cols = []
            ops = []

            pts_disp = []
            col_disp = []

            black_pts = []
            black_cols = []
            black_bary = []
            black_op = []

            for fy in range(-1, grid_height + 1):
                for fx in range(-1, grid_width + 1):

                    pts.extend([0, 0])
                    pts.extend([0, 0])
                    pts.extend([0, 0])
                    pts.extend([0, 0])

                    if self._allow_disp:
                        for i in range(0, 4):
                            pts_disp.extend([0, 0])
                            pts_disp.extend([0, 0])
                            pts_disp.extend([0, 0])
                            pts_disp.extend([0, 0])

                            col_disp.extend([1, 1, 1, 0.5] * 4)

                    uv.extend([0, 0, 0])
                    uv.extend([0, 0, 0])
                    uv.extend([0, 0, 0])
                    uv.extend([0, 0, 0])

                    for i in range(0, 4):
                        cols.extend([1.0, 1.0, 1.0])
                        ops.append(1.0)

                    black_pts.extend([0, 0])
                    black_pts.extend([0, 0])
                    black_pts.extend([0, 0])
                    black_pts.extend([0, 0])

                    black_op.extend([0, 0, 0])
                    black_op.extend([0, 0, 0])
                    black_op.extend([0, 0, 0])
                    black_op.extend([0, 0, 0])

                    black_cols.extend([0, 0, 0])
                    black_cols.extend([0, 0, 0])
                    black_cols.extend([0, 0, 0])
                    black_cols.extend([0, 0, 0])

                    # 0 - left, bottom
                    # 1 - right, bottom
                    # 2 - right, top
                    # 3 - left, top

                    black_bary.extend([0, 0])  # interpolace v ramci x
                    black_bary.extend([1, 0])
                    black_bary.extend([0, 0])  # interpolace v ramci y
                    black_bary.extend([0, 1])

                    pt_count += 4

            self._map_pts = (pyglet.gl.GLfloat * len(pts))(*pts)
            self._map_uvs = (pyglet.gl.GLfloat * len(uv))(*uv)
            self._map_cols = (pyglet.gl.GLfloat * len(cols))(*cols)
            self._map_op_hash = {}

            for tex_id in self._images.get_texture_ids():
                self._map_op_hash[tex_id] = (pyglet.gl.GLfloat * len(ops))(*ops.copy())

            if self._allow_disp:
                self._vlist_disp = pyglet.graphics.vertex_list(len(pts_disp) // 2, ('v2f', pts_disp), ('c4f', col_disp))

            self._black_pts = (pyglet.gl.GLfloat * len(black_pts))(*black_pts)
            self._black_cols = (pyglet.gl.GLfloat * len(black_cols))(*black_cols)
            self._black_bary = (pyglet.gl.GLfloat * len(black_bary))(*black_bary)
            self._black_op = (pyglet.gl.GLfloat * len(black_op))(*black_op)

        if off_x != self._bef_off_x or off_y != self._bef_off_y or do_update or self._allow_disp:

            self._bef_off_x = off_x
            self._bef_off_y = off_y
            self._tex_ids.clear()

            idx = 0
            sz = 50.0

            for fy in range(-1, grid_height + 1):
                for fx in range(-1, grid_width + 1):

                    tile = self._tile_col.get_tile(fx - off_x, fy - off_y)

                    if tile is not None:
                        tiles.add(tile)

                        st_x = fx * sz
                        st_y = fy * sz + sz
                        en_x = fx * sz + sz
                        en_y = fy * sz

                        tile.pt_idx = idx * 8

                        self._map_pts[idx * 8 + 0] = st_x
                        self._map_pts[idx * 8 + 1] = en_y

                        self._map_pts[idx * 8 + 2] = en_x
                        self._map_pts[idx * 8 + 3] = en_y

                        self._map_pts[idx * 8 + 4] = en_x
                        self._map_pts[idx * 8 + 5] = st_y

                        self._map_pts[idx * 8 + 6] = st_x
                        self._map_pts[idx * 8 + 7] = st_y

                        tex_id = tile.get_img().id

                        self._tex_ids.add(tex_id)

                        tx = tile.get_img().tex_coords

                        for c_tex_id in self._map_op_hash.keys():
                            if c_tex_id == tex_id:
                                for i in range(0, 4):
                                    self._map_op_hash[c_tex_id][idx * 4 + i] = 1.0
                            else:
                                for i in range(0, 4):
                                    self._map_op_hash[c_tex_id][idx * 4 + i] = 0

                        for i in range(0, 12):
                            self._map_uvs[idx * 12 + i] = tx[i]

                        if self._allow_disp:
                            szh = sz / 2
                            sub_idx = 0

                            tile.col_disp_idx = idx * 64

                            for s_fy in [1, 0]:
                                for s_fx in range(0, 2):
                                    ust_x = st_x + s_fx * szh
                                    ust_y = st_y - s_fy * szh
                                    uen_x = ust_x + szh
                                    uen_y = ust_y - szh

                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 0] = ust_x
                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 1] = uen_y

                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 2] = uen_x
                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 3] = uen_y

                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 4] = uen_x
                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 5] = ust_y

                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 6] = ust_x
                                    self._vlist_disp.vertices[idx * 32 + sub_idx * 8 + 7] = ust_y

                                    sub_idx += 1

                                    # 0 - left top
                                    # 1 - right top
                                    # 2 - left bottom
                                    # 3 - right bottom

                        tile.black_op_idx = idx * 12
                        tile.black_col_idx = idx * 12

                        self._black_pts[idx * 8 + 0] = st_x
                        self._black_pts[idx * 8 + 1] = en_y

                        self._black_pts[idx * 8 + 2] = en_x
                        self._black_pts[idx * 8 + 3] = en_y

                        self._black_pts[idx * 8 + 4] = en_x
                        self._black_pts[idx * 8 + 5] = st_y

                        self._black_pts[idx * 8 + 6] = st_x
                        self._black_pts[idx * 8 + 7] = st_y

                        self.update_tile(tile)

                    else:
                        for i in range(0, 8):
                            self._map_pts[idx * 8 + i] = 0
                            self._black_pts[idx * 8 + i] = 0

                        if self._allow_disp:
                            for i in range(0, 32):
                                self._vlist_disp.vertices[idx * 32 + i] = 0

                    idx += 1

        return tiles

    def update_edge(self, edge):

        cdef long tot_count = 0
        cdef long vis_count = 0
        cdef long blur_count = 0
        cdef long blank_count = 0

        for tile in edge.get_tiles():
            if tile is not None:
                tot_count += 1

                if tile.get_fog_visible():
                    vis_count += 1
                elif tile.get_fog_type() == 1:
                    blur_count += 1
                else:
                    blank_count += 1

        if vis_count == tot_count:
            edge.opacity = 0

        elif blank_count == tot_count:
            edge.opacity = 1.0

        elif vis_count > 0:

            if vis_count == 3:
                edge.opacity = -1
            elif vis_count == 2:
                edge.opacity = -0.5
            else:
                edge.opacity = 0

        elif blur_count > 0:

            if blur_count == 3:
                edge.opacity = 150 / 255 - 0.5
            elif blur_count == 2:
                edge.opacity = 150 / 255 - 0.25
            else:
                edge.opacity = 150 / 255

    def update_tile(self, tile):

        for idx in range(0, 4):
            if not tile.get_fog_visible() and tile.get_fog_type() == 0:
                self._black_op[tile.black_op_idx + idx * 3 + 0] = 1.0

            else:
                self._black_op[tile.black_op_idx + idx * 3 + 0] = tile.get_edge(idx).opacity

        tot_op = - self._black_op[tile.black_op_idx + 0 + 0] + self._black_op[tile.black_op_idx + 3 + 0] - self._black_op[tile.black_op_idx + 6 + 0] + self._black_op[tile.black_op_idx + 9 + 0]

        for idx in range(0, 4):
            self._black_op[tile.black_op_idx + idx * 3 + 1] = tot_op

        # 0 - left, bottom
        # 1 - right, bottom
        # 2 - right, top
        # 3 - left, top

        if self._allow_disp:
            for idx in range(0, 4):
                place = tile.get_place(idx)

                col = [0, 0, 0, 0.5]

                if place.test_flag == 1:
                    col = [1, 0, 0, 0.8]
                elif place.test_flag == 2:
                    col = [1, 0.5, 0.5, 0.8]
                elif place.get_acc() == 2:
                    col = [1, 1, 1, 0.5]
                elif place.get_acc() == 1:
                    if place.get_acc_count() > 1:
                        col = [0, 1, 1, 0.5]
                    else:
                        col = [0, 0, 1, 0.5]

                if len(place.get_moveables()) > 0:
                    col[3] = 1

                for sub in range(0, 4):
                    self._vlist_disp.colors[tile.col_disp_idx + idx * 16 + sub * 4 + 0] = col[0]
                    self._vlist_disp.colors[tile.col_disp_idx + idx * 16 + sub * 4 + 1] = col[1]
                    self._vlist_disp.colors[tile.col_disp_idx + idx * 16 + sub * 4 + 2] = col[2]
                    self._vlist_disp.colors[tile.col_disp_idx + idx * 16 + sub * 4 + 3] = col[3]

    def draw(self):
        cdef long tex_id

        trans_mat = pyglet.math.Mat4().translate(self._x - self._bef_off_x * self._tile_size, self._y - self._bef_off_y * self._tile_size, 0.0).scale(self._zoom, self._zoom, self._zoom)

        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
        pyglet.gl.glEnable(pyglet.gl.GL_TEXTURE_2D)

        with self._map_shad.using():

            self._map_shad.uniforms.projection = [self._view.get_mat()]
            self._map_shad.uniforms.trans = [trans_mat]

            self._map_shad.enable_all_attributes()

            if 'vert' in self._map_shad.attributes:
                self._map_shad.attributes['vert'].point_to(self._map_pts, pyglet.gl.GL_FLOAT, 2)

            if 'colors' in self._map_shad.attributes:
                self._map_shad.attributes['colors'].point_to(self._map_cols, pyglet.gl.GL_FLOAT, 3)

            if 'uvs' in self._map_shad.attributes:
                self._map_shad.attributes['uvs'].point_to(self._map_uvs, pyglet.gl.GL_FLOAT, 3)

            for tex_id in self._tex_ids:
                # TODO - dodelat texture arrays
                if 'op' in self._map_shad.attributes:
                    self._map_shad.attributes['op'].point_to(self._map_op_hash[tex_id], pyglet.gl.GL_FLOAT, 1)

                pyglet.gl.glBindTexture(pyglet.gl.GL_TEXTURE_2D, tex_id)
                self._map_shad.uniforms.tex = tex_id

                pyglet.gl.glDrawArrays(pyglet.gl.GL_QUADS, 0, len(self._map_pts) // 2)

            self._map_shad.disable_all_attributes()

        pyglet.gl.glDisable(pyglet.gl.GL_BLEND)
        pyglet.gl.glDisable(pyglet.gl.GL_TEXTURE_2D)

        #pyglet.gl.glPopMatrix()

    def draw_black(self):
        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)

        with self._black_shad.using():

            mat = pyglet.math.Mat4().translate(self._x - self._bef_off_x * self._tile_size, self._y - self._bef_off_y * self._tile_size, 0.0)
            mat = mat.scale(self._zoom, self._zoom, self._zoom)

            self._black_shad.uniforms.projection = [self._view.get_mat()]
            self._black_shad.uniforms.trans = [mat]

            self._black_shad.enable_all_attributes()

            if 'vert' in self._black_shad.attributes:
                self._black_shad.attributes['vert'].point_to(self._black_pts, pyglet.gl.GL_FLOAT, 2)

            if 'colors' in self._black_shad.attributes:
                self._black_shad.attributes['colors'].point_to(self._black_cols, pyglet.gl.GL_FLOAT, 3)

            if 'bary' in self._black_shad.attributes:
                self._black_shad.attributes['bary'].point_to(self._black_bary, pyglet.gl.GL_FLOAT, 2)

            if 'op' in self._black_shad.attributes:
                self._black_shad.attributes['op'].point_to(self._black_op, pyglet.gl.GL_FLOAT, 3)

            pyglet.gl.glDrawArrays(pyglet.gl.GL_QUADS, 0, len(self._black_pts) // 2)

            self._black_shad.disable_all_attributes()

        pyglet.gl.glDisable(pyglet.gl.GL_BLEND)

    """
    def _draw(self):
        cdef long tex_id, use_opa, i

        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslatef(self._x - self._bef_off_x * self._tile_size, self._y - self._bef_off_y * self._tile_size, 0.0)
        #pyglet.gl.glTranslatef(self._x, self._y, 0.0)
        #pyglet.gl.glRotatef(self._angle, 0.0, 0.0, 1.0)
        pyglet.gl.glScalef(self._zoom, self._zoom, self._zoom)

        pyglet.gl.glEnable(pyglet.gl.GL_TEXTURE_2D)
        #pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MAG_FILTER, pyglet.gl.GL_NEAREST)
        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)

        self._vlist.draw(pyglet.gl.GL_QUADS)

        pyglet.gl.glDisable(pyglet.gl.GL_BLEND)
        pyglet.gl.glDisable(pyglet.gl.GL_TEXTURE_2D)

        if self._allow_disp:
            pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
            self._vlist_disp.draw(pyglet.gl.GL_QUADS)

        #self._vlist.draw(pyglet.gl.GL_LINES)

        pyglet.gl.glPopMatrix()
    """
