from game.action.actionitemspec import ActionItemSpec
from game.action.actionqueue import ActionQueue
from game.build.buildspec import BuildSpec
from game.build.buildturretspec import BuildTurretSpec
from game.image.imagecollector import ImageCollector
from game.interface.ibuilds import IBuilds
from game.interface.igui import IGui
from game.interface.imoveables import IMoveables
from game.interface.iselectable import ISelectable
from game.interface.itilecollector import ITileCollector
from game.logger import Logger
from game.moveable.unitspec import UnitSpec
from game.network.command import Command
from game.sound.soundcollector import SoundCollector


class PlayerSide:

    _moveables: IMoveables = None
    _builds: IBuilds = None
    _tile_col: ITileCollector = None
    _worker = None

    _type = None
    _color = None
    _order = 0
    _tick = 0
    _money = 0
    _money_rate = 0
    _money_stack = None
    _mark_selected = True

    _childs = None
    _childs_hash = None

    _build_childs = None
    _build_childs_hash = None

    _selected = None
    _actions = None

    _event_money: (any, int, any, int) = lambda self, money, action_spec, money_slot: None
    _event_action_avail: (any, any) = lambda self, data: None
    _event_create_build: (any, any, any) = lambda self, action_item, bref: None
    _event_create_unit: (any, any, any) = lambda self, action_item, mref: None
    _event_upgrade: (any, any, any) = lambda self, action_item, bref: None
    _event_destroy_unit: (any, any) = lambda self, mref: None
    _event_destroy_build: (any, any) = lambda self, bref: None
    _event_finish_build: (any, any) = lambda self, bref: None
    _event_finish_unit: (any, any) = lambda self, mref: None

    number = 0
    number_total = 0

    def __init__(self, type, ai, order, color):
        self._type = type
        self._ai = ai
        self._order = order
        self._money = 0
        self._money_rate = 0
        self._money_stack = []
        self._childs = []
        self._childs_hash = {}
        self._build_childs = []
        self._build_childs_hash = {}
        self._selected = []
        self._mark_selected = not self.get_is_ai()
        self._actions = ActionQueue(self)
        self._color = ImageCollector.color_get_by_key(color)

        if self._color is None:
            self._color = ImageCollector.color_sorted[self._order]

        self.number = PlayerSide.number_total
        PlayerSide.number_total += 1

    def set_refs(self, moveables, builds, tile_col, worker):
        self._moveables = moveables
        self._builds = builds
        self._tile_col = tile_col
        self._worker = worker

    def get_type(self):
        return self._type

    def get_money(self):
        return self._money

    def get_money_rate(self):
        return self._money_rate

    def get_color(self):
        return self._color

    def get_is_ai(self):
        return self._ai != 'human'

    def get_actions(self):
        return self._actions

    def get_is_any_selected(self, type=None):
        if type is None:
            return len(self._selected) > 0
        else:
            for mm in self._selected:
                if mm.get_selected_type() == type:
                    return True
            return False

    def get_selected(self):
        return self._selected

    def get_selected_moveables(self):
        return list(mm for mm in self.get_selected() if mm.get_selected_type() == ISelectable.SELECTED_MOVEABLE)

    def get_selected_builds(self):
        return list(mm for mm in self.get_selected() if mm.get_selected_type() == ISelectable.SELECTED_BUILD)

    def get_turret_spec_key(self, level):
        if self._type == "s1":
            if level == 0:
                return "turret"
            elif level == 1:
                return "turret_can"

        elif self._type == "s2":
            if level == 0:
                return "turret"
            elif level == 1:
                return "turret_rock"

        return None

    def add_to_selected(self, mref: ISelectable):
        if mref.get_side() == self and mref.get_is_alive() and mref not in self._selected and mref.get_selected_type() != ISelectable.SELECTED_NONE:
            self._selected.append(mref)

            if self._mark_selected:
                mref.set_selected(True)

    def rem_from_selected(self, mref: ISelectable):
        if mref.get_side() == self and mref in self._selected:
            self._selected.remove(mref)
            mref.set_selected(False)

    def toggle_selected(self, mref: ISelectable):
        if mref.get_side() == self:
            if mref in self._selected:
                self.rem_from_selected(mref)
            else:
                self.add_to_selected(mref)

    def clear_selected(self):
        for mref in self._selected:
            mref.set_selected(False)
        self._selected = []

    def set_selected(self, childs):
        self.clear_selected()
        for mref in childs:
            self.add_to_selected(mref)

    def get_units(self):
        return self._childs

    def get_units_by_key(self, key):
        return self._childs_hash[key] if (key in self._childs_hash) else []

    def get_units_by_key_count(self, key):
        return len(self._childs_hash[key]) if (key in self._childs_hash) else 0

    def get_build_childs(self):
        return self._build_childs

    def get_build_count(self, key):
        return len(self._build_childs_hash[key]) if (key in self._build_childs_hash) else 0

    def get_builds_by_key(self, key):
        return self._build_childs_hash[key] if (key in self._build_childs_hash) else []

    def get_is_alive(self):
        return len(self._build_childs) > 0 or len(self._childs) > 0

    def set_event_money(self, func):
        self._event_money = func

    def set_event_action_avail(self, func):
        self._event_action_avail = func

    def set_event_create_build(self, func):
        self._event_create_build = func

    def set_event_create_unit(self, func):
        self._event_create_unit = func

    def set_event_upgrade(self, func):
        self._event_upgrade = func

    def set_event_destroy_unit(self, func):
        self._event_destroy_unit = func

    def set_event_destroy_build(self, func):
        self._event_destroy_build = func

    def set_event_finish_build(self, func):
        self._event_finish_build = func

    def set_event_finish_unit(self, func):
        self._event_finish_unit = func

    def event_action_avail(self, action_item):
        self._event_action_avail(action_item)

    def event_create_build(self, action_item, bref):
        self._event_create_build(action_item, bref)

    def event_create_unit(self, action_item, mref):
        self._event_create_unit(action_item, mref)

    def event_upgrade(self, action_item, bref):
        self._event_upgrade(action_item, bref)

    def event_destroy_unit(self, bref):
        self._event_destroy_unit(bref)

    def event_destroy_build(self, mref):
        self._event_destroy_build(mref)

    def event_finish_build(self, bref):
        self._event_finish_build(bref)

    def event_finish_unit(self, mref):
        self._event_finish_unit(mref)

    def event_clear(self):
        self._event_money: (any, int, any, int) = lambda self, money, action_spec, money_slot: None
        self._event_action_avail: (any, any) = lambda self, data: None
        self._event_create_build: (any, any, any) = lambda self, action_item, bref: None
        self._event_create_unit: (any, any, any) = lambda self, action_item, mref: None
        self._event_upgrade: (any, any, any) = lambda self, action_item, bref: None
        self._event_destroy_unit: (any, any) = lambda self, mref: None
        self._event_destroy_build: (any, any) = lambda self, bref: None
        self._event_finish_build: (any, any) = lambda self, bref: None
        self._event_finish_unit: (any, any) = lambda self, mref: None

    def money_set(self, money):
        self._money = money

    def money_add(self, money, add_stack=False):
        self._money += money

        if add_stack:
            use_tick = self._tick // 50

            if len(self._money_stack) > 0 and self._money_stack[len(self._money_stack) - 1][0] == use_tick:
                self._money_stack[len(self._money_stack) - 1] = (use_tick, self._money_stack[len(self._money_stack) - 1][1] + money)
            else:
                self._money_stack.append((use_tick, money))

        self._event_money(money, None, -1)

    def money_pay(self, money, action_spec=None, money_slot=-1):
        if self._money >= money:
            self._money -= money
            self._event_money(money * -1, action_spec, money_slot)

            return True
        else:
            return False

    def get_targetable_at_coords(self, x, y):
        mref = self._moveables.get_moveable_at_coord(x, y)
        if mref is not None and mref.get_side() is self:
            mref = None

        if mref is None:
            mref = self._builds.get_build_at_coord(x, y)
            if mref is not None and mref.get_side() is self:
                mref = None

        return mref

    def destroy(self):
        pass

    def build_units(self):

        tile = self._tile_col.pop_first_tile()

        if self._type == "s1":
            self.add_build(tile, BuildSpec.get("s1_main"), {"ready": True})

            self.add_unit(tile.get_right().get_right().get_place(), UnitSpec.get("s1_jeep"))

            # self.add_build_coord(25, 5, BuildSpec.get("s1_main"))

            # self.add_units(22, 1, ["s1_lighttank", "s1_medtank", "s1_jeep", "s1_plate", "s1_rocket"], 2)
            # self.add_units(21, 1, ["s1_miner"], 1)
            # self.add_units(3, 3, ["s1_lighttank", "s1_medtank", "s1_jeep", "s1_plate"], 3)
            # self.add_units(3, 3, ["s1_medtank"], 2)
            # self.add_units(40, 3, ["s1_jeep"], 15)
            # self.add_units(27, 1, ["s1_lighttank"], 3)

        if self._type == "s2":
            self.add_build(tile, BuildSpec.get("s2_main"), {"ready": True})

            self.add_unit(tile.get_right().get_right().get_place(), UnitSpec.get("s2_basic"))

            # self.add_build_coord(39, 28, BuildSpec.get("s2_main"))

            # self.add_units(15, 25, ["s2_basic", "s2_mini", "s2_plasma", "s2_rock", "s2_rock"], 2)
            # self.add_units(3, 15, ["s2_rock"], 3)
            # self.add_units(3, 20, ["s2_rock"], 4)
            # self.add_units(3, 40, ["s2_basic"], 15)
            # self.add_units(3, 20, ["s2_rock"], 7)

    def serialize(self):
        out = {
            'number': self.number,
            'order': self._order,
            'type': self._type,
            'ai': self._ai,
            'money': self._money,
            'color': self._color.get_key(),
            'actions': self._actions.serialize()
        }

        return out

    def add_unit(self, place, spec, set_number=-1):
        mm = self._moveables.add_moveable_place(self, spec, place, set_number)
        self._childs += [mm]

        if spec.get_key() not in self._childs_hash:
            self._childs_hash[spec.get_key()] = []

        self._childs_hash[spec.get_key()].append(mm)
        return mm

    def add_units(self, x, y, spec_keys, size=1):
        px = x
        py = y

        for i in range(0, len(spec_keys)):
            for rx in range(0, size):
                for ry in range(0, size):
                    spec = UnitSpec.get(spec_keys[i])
                    mm = self._moveables.add_moveable(self, spec, px + rx, py + ry)

                    self._childs += [mm]

                    if spec.get_key() not in self._childs_hash:
                        self._childs_hash[spec.get_key()] = []

                    self._childs_hash[spec.get_key()].append(mm)

            px += size

    def add_unit_coord(self, x, y, spec, tile_coords=False):
        if tile_coords:
            tile = self._tile_col.get_tile(x, y)
        else:
            tile = self._tile_col.get_tile_by_coords_scroll(x, y)

        mm = None

        if tile is not None:
            for place in tile.get_places():
                if place.is_accessible_size(spec.get_size()):
                    mm = self._moveables.add_moveable_place(self, spec, place)
                    self._childs += [mm]

                    if spec.get_key() not in self._childs_hash:
                        self._childs_hash[spec.get_key()] = []

                    self._childs_hash[spec.get_key()].append(mm)

        return mm

    def add_unit_coord_command(self, cx, cy, spec):
        tile = self._tile_col.get_tile_by_coords_scroll(cx, cy)

        if tile is not None:
            cm = Command()
            cm.append(Command.T_ADD_UNIT_COORD, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), x=tile.get_pos_x(), y=tile.get_pos_y(), spec_number=spec.number, side_number=self.number)

            self._worker.add_command(cm)

    def rem_unit(self, ref):
        self._childs_hash[ref.get_spec().get_key()].remove(ref)
        self._childs.remove(ref)

    def add_build(self, tile, spec, data=None):
        b = self._builds.add_build_tile(self, spec, tile, data)
        self._build_childs += [b]

        if spec.get_key() not in self._build_childs_hash:
            self._build_childs_hash[spec.get_key()] = []

        self._build_childs_hash[spec.get_key()].append(b)
        return b

    def add_build_coord(self, x, y, spec, data=None):
        b = self._builds.add_build(self, spec, x, y, data)
        self._build_childs += [b]

        if spec.get_key() not in self._build_childs_hash:
            self._build_childs_hash[spec.get_key()] = []

        self._build_childs_hash[spec.get_key()].append(b)
        return b

    def rem_build(self, ref):
        self._build_childs_hash[ref.get_spec().get_key()].remove(ref)
        self._build_childs.remove(ref)

    def get_builds_coords(self):
        x = 0
        y = 0

        if len(self._build_childs) > 0:
            for b in self._build_childs:
                x += b.get_x()
                y += b.get_y()

            x /= len(self._build_childs)
            y /= len(self._build_childs)

        return x, y

    def selected_goto(self, place_to):
        sel = self.get_selected_moveables()

        if len(sel) == 0:
            return False

        elif place_to is not None:
            cm = Command()
            cm.append(Command.T_MV_GOTO, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), x=place_to.get_pos_x(), y=place_to.get_pos_y(), count=len(sel))

            for un in sel:
                cm.append(Command.T_NUMBER, Command.get_sort_num(), type=0, number=un.number)

            self._worker.add_command(cm)
            return True

        else:
            return False

    def selected_attack_move(self, place_to):
        sel = self.get_selected_moveables()

        if len(sel) == 0:
            return False

        elif place_to is not None:
            cm = Command()
            cm.append(Command.T_MV_ATTGOTO, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), x=place_to.get_pos_x(), y=place_to.get_pos_y(), count=len(sel))

            for un in sel:
                cm.append(Command.T_NUMBER, Command.get_sort_num(), type=0, number=un.number)

            self._worker.add_command(cm)
            return True

        else:
            return False

    def selected_goto_attack(self, attack_mref):
        sel = self.get_selected_moveables()

        if attack_mref is None or attack_mref.get_side() is self or len(sel) == 0:
            return False

        else:
            cm = Command()
            cm.append(Command.T_MV_ATTACK, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), type=attack_mref.get_target_type(), number=attack_mref.number, count=len(sel))

            for un in sel:
                cm.append(Command.T_NUMBER, Command.get_sort_num(), type=0, number=un.number)

            self._worker.add_command(cm)
            return True

    def selected_hold(self):
        sel = self.get_selected_moveables()

        if len(sel) == 0:
            return False

        else:
            cm = Command()
            cm.append(Command.T_MV_HOLD, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), count=len(sel))

            for un in sel:
                cm.append(Command.T_NUMBER, Command.get_sort_num(), type=0, number=un.number)

            self._worker.add_command(cm)
            return True

    def selected_stop(self):
        sel = self.get_selected_moveables()

        if len(sel) == 0:
            return False

        else:
            cm = Command()
            cm.append(Command.T_MV_STOP, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), count=len(sel))

            for un in sel:
                cm.append(Command.T_NUMBER, Command.get_sort_num(), type=0, number=un.number)

            self._worker.add_command(cm)
            return True

    def selected_place_travel(self, place_travel):
        sel = self.get_selected_builds()

        if len(sel) == 0:
            return False

        else:
            cm = Command()
            cm.append(Command.T_TRAVEL_PLACE, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), x=place_travel.get_pos_x(), y=place_travel.get_pos_y(), count=len(sel))

            for un in sel:
                cm.append(Command.T_NUMBER, Command.get_sort_num(), type=0, number=un.number)

            self._worker.add_command(cm)
            return True

    def destroy_build(self, build_ref):
        cm = Command()
        cm.append(Command.T_BUILD_DESTROY, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), number=build_ref.number)

        self._worker.add_command(cm)
        return True

    def check_max_build(self, build_spec):
        if build_spec.get_max_quantity() == 0:
            return True

        cnt = self.get_build_count(build_spec.get_key()) + self._actions.get_build_spec_stacked_count(build_spec)
        return cnt < build_spec.get_max_quantity()

    def check_max_unit(self, unit_spec):
        if unit_spec.get_max_quantity() == 0:
            return True

        cnt = self.get_units_by_key_count(unit_spec.get_key()) + self._actions.get_unit_spec_stacked_count(unit_spec)
        return cnt < unit_spec.get_max_quantity()

    def action_enqueue_key(self, key, build_ref, build_trg_ref=None, money_slot=-1, immed=False):
        spec = ActionItemSpec.get(self.get_type() + "_" + key)
        return self.action_enqueue(spec, build_ref, build_trg_ref, money_slot, immed)

    def action_enqueue(self, action: ActionItemSpec, build_ref, build_trg_ref=None, money_slot=-1, immed=False):
        if immed or self._worker.get_is_immed():
            if build_ref is not None and build_ref.get_is_alive() and build_ref.get_may_apply_action(action):

                if action.get_build_spec() is not None and not self.check_max_build(action.get_build_spec()):
                    IGui.get().create_notify("Maximum number of " + str(action.get_build_spec().get_name()) + " reached!")
                    SoundCollector().play_sound("error", 0, 0, 1.0, use_coords=False)

                elif action.get_unit_spec() is not None and not self.check_max_unit(action.get_unit_spec()):
                    IGui.get().create_notify("Maximum number of " + str(action.get_unit_spec().get_name()) + " reached!")
                    SoundCollector().play_sound("error", 0, 0, 1.0, use_coords=False)

                elif self.money_pay(action.get_money_cost(), action, money_slot):
                    return self._actions.action_enqueue(action, build_ref, build_trg_ref)

                else:
                    if action.get_unit_spec() is not None:
                        IGui.get().create_notify("Not enough money to create unit " + str(action.get_unit_spec().get_name()) + "!")
                    elif action.get_build_spec() is not None:
                        IGui.get().create_notify("Not enough money to create building " + str(action.get_build_spec().get_name()) + "!")
                    else:
                        IGui.get().create_notify("Not enough money for " + str(action.get_name()) + "!")

                    SoundCollector().play_sound("error", 0, 0, 1.0, use_coords=False)

            return None

        else:
            cm = Command()
            cm.append(Command.T_ACTION_ENQUEUE, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), side_number=self.number, action_number=action.number, build_number=build_ref.number, build_trg_number=0 if build_trg_ref is None else build_trg_ref.number, money_slot=money_slot)

            self._worker.add_command(cm)

    def action_build_realize(self, action_item, build_place, immed=False):
        if immed or self._worker.get_is_immed():
            self._actions.action_handle(action_item, {"tile": build_place.get_tile()})

        else:
            spec = build_place.get_spec()
            tile = build_place.get_tile()

            cm = Command()
            cm.append(Command.T_ACTION_BUILD, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), side_number=self.number, action_item_number=action_item.number, build_spec_number=spec.number, x=tile.get_pos_x(), y=tile.get_pos_y())

            self._worker.add_command(cm)

    def action_cancel(self, action_item, immed=False):
        if immed or self._worker.get_is_immed():
            self._actions.action_cancel(action_item)

        else:
            cm = Command()
            cm.append(Command.T_ACTION_CANCEL, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), side_number=self.number, action_item_number=action_item.number)

            self._worker.add_command(cm)

    def action_refund(self, action_item, immed=False):
        if immed or self._worker.get_is_immed():
            self._actions.action_refund(action_item)

        else:
            cm = Command()
            cm.append(Command.T_ACTION_REFUND, Command.get_sort_num(), ctx=self._worker.get_ctx_number(), side_number=self.number, action_item_number=action_item.number)

            self._worker.add_command(cm)

    def load_images(self):
        Logger.get().logm('Loading player #' + str(self._order) + ' (' + str(self._type) + ') ' + self._color.get_key() + ' sprites ...')
        [spec.get_tex_load(self._color) for spec in UnitSpec.get_all_by_type(self.get_type())]
        [spec.get_tex_load(self._color) for spec in BuildSpec.get_all_by_type(self.get_type())]
        [spec.get_tex_load(self._color) for spec in BuildTurretSpec.get_all_by_type(self.get_type())]

    def update(self, tick):
        self._tick = tick

        if self._tick % 50 == 0:
            while len(self._money_stack) > 0 and self._money_stack[0][0] + 30 < self._tick // 50:
                del self._money_stack[0]

            if len(self._money_stack) > 1:
                min_tick = self._money_stack[0][0]
                max_tick = self._tick // 50
                idx = 0
                self._money_rate = 0

                for tick in range(min_tick, max_tick + 1):
                    while idx < len(self._money_stack) and self._money_stack[idx][0] < tick:
                        idx += 1

                    if idx < len(self._money_stack) and self._money_stack[idx][0] == tick:
                        self._money_rate += self._money_stack[idx][1]

                self._money_rate = self._money_rate / (max_tick - min_tick)

        self._actions.update()
