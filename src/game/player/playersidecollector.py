import random

from game.ai.aicollector import AiCollector
from game.helper.singleton import Singleton
from game.interface.ibuilds import IBuilds
from game.interface.imoveables import IMoveables
from game.interface.iplayersides import IPlayerSides
from game.interface.iselectable import ISelectable
from game.interface.itilecollector import ITileCollector
from game.logger import Logger
from game.player.playerside import PlayerSide


class PlayerSideCollector(IPlayerSides, metaclass=Singleton):

    _moveables: IMoveables = None
    _builds: IBuilds = None
    _tile_col: ITileCollector = None
    _worker = None

    _sides = None
    _focus_side = None
    _use_playable_sides = None

    def __init__(self):
        IPlayerSides.__init__(self)
        IPlayerSides.set(self)

        self._sides = []
        self._focus_side = None
        self._use_playable_sides = None

    def set_refs(self, moveables, builds, tile_col, worker):
        self._moveables = moveables
        self._builds = builds
        self._tile_col = tile_col
        self._worker = worker

        for side in self._sides:
            side.set_refs(moveables, builds, tile_col, worker)

    def add_side(self, pl):
        side = PlayerSide(pl.get_side_type(), pl.get_ai(), len(self._sides), pl.get_color())
        side.set_refs(self._moveables, self._builds, self._tile_col, self._worker)

        self._sides.append(side)

        if pl.get_ai() != 'human':
            AiCollector().add(side, pl.get_ai())

        side.money_add(pl.get_money())

        return side

    def build_units(self):
        for side in self._sides:
            side.build_units()

    def serialize(self):
        out = {
            'number_total': PlayerSide.number_total,
            'list': []
        }

        for side in self._sides:
            out['list'].append(side.serialize())

        return out

    def unserialize(self, dt):
        for c in dt['list']:
            side = PlayerSide(c['type'], c['ai'], c['order'], c['color'])
            side.set_refs(self._moveables, self._builds, self._tile_col, self._worker)
            side.number = c['number']

            self._sides.append(side)

            side.money_add(c['money'])

        PlayerSide.number_total = dt['number_total']

    def get_worker(self):
        return self._worker

    def get_sides(self):
        return self._sides

    def get_side_by_index(self, idx):
        return self._sides[idx] if (idx >= 0 and idx < len(self._sides)) else None

    def get_side_by_number(self, num):
        for side in self._sides:
            if side.number == num:
                return side

        return None

    def get_focus_side(self, any=False):
        if self._focus_side is not None:
            return self._focus_side

        else:
            side = next((sd for sd in self._sides if AiCollector().get_have_ai(sd) is None), None)

            if any and side is None and len(self._sides) > 0:
                side = self._sides[0]

            return side

    def get_playable_sides(self):
        if self._use_playable_sides is not None:
            return self._use_playable_sides
        else:
            return list([sd for sd in self._sides if AiCollector().get_have_ai(sd) is None])

    def set_playable_sides(self, playable_sides):
        self._use_playable_sides = playable_sides

    def set_focus_side(self, focus_side):
        self._focus_side = focus_side

    def get_random_enemy(self, self_side):
        enemies = list([s for s in self._sides if s is not self_side])
        return enemies[random.randint(0, len(enemies) - 1)]

    def select_one(self, side, coord_x, coord_y):
        mm = self._moveables.get_moveable_at_coord(coord_x, coord_y)

        if mm is not None and mm.get_side() is side and mm.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
            mm.get_side().toggle_selected(mm)

        else:
            mm = self._builds.get_build_at_coord(coord_x, coord_y, side)

            if mm is not None and mm.get_side() is side:
                mm.get_side().toggle_selected(mm)

        return mm

    def select_at_range(self, side, sx, sy, ex, ey, spec_key=None):
        for mm in self._moveables.get_moveable_at_range(sx, sy, ex, ey, side):
            if spec_key is None or spec_key == mm.get_spec().get_key() and mm.get_selected_type() == ISelectable.SELECTED_MOVEABLE:
                mm.get_side().add_to_selected(mm)

    def select_builds_at_range(self, side, sx, sy, ex, ey, spec_key=None):
        for mm in self._builds.get_build_at_range(sx, sy, ex, ey, side):
            if spec_key is None or spec_key == mm.get_spec().get_key():
                mm.get_side().add_to_selected(mm)

    def determine_wictory_side(self):
        w_sides = []

        for c in self._sides:
            if c.get_is_alive():
                w_sides.append(c)

        return None if len(w_sides) != 1 else w_sides[0]

    def load_images(self):
        Logger.get().logm('Loading player colored sprites ...')
        [c.load_images() for c in self._sides]

    def clear(self):
        for c in self._sides:
            c.destroy()

        self._sides = []

    def update(self, tick):
        for side in self._sides:
            side.update(tick)
