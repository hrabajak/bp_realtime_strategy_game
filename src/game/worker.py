import pickle
import threading
import time
#import cProfile
import pyglet

from game.action.actionitemspec import ActionItemSpec
from game.ai.aicollector import AiCollector
from game.build.buildaction import BuildAction
from game.build.buildcollector import BuildCollector
from game.build.buildplace import BuildPlace
from game.build.buildspec import BuildSpec
from game.config import Config
from game.gamestat.gamespec import GameSpec
from game.graphic.graphbatches import GraphBatches
from game.gui.guibase import GuiBase
from game.helper.frametimer import FrameTimer
from game.interface.igui import IGui
from game.interface.iview import IView
from game.interface.iworker import IWorker
from game.logger import Logger
from game.map.mapbase import MapBase
from game.moveable.gotogroup import GotoGroup
from game.moveable.moveablecollector import MoveableCollector
from game.moveable.moveabletravel import MoveableTravel
from game.moveable.unitspec import UnitSpec
from game.network.client import Client
from game.network.command import Command
from game.particle.particlecollector import ParticleCollector
from game.player.playersidecollector import PlayerSideCollector
from game.shot.shotcollector import ShotCollector
from game.shot.shotspec import ShotSpec
from game.window.windowmanager import WindowManager


class Worker(IWorker):

    _parent = None
    _ctx_number = -1
    _ctx_spec = None
    _game_state = 0
    _game_ready_flag = False
    _victory_side = None
    _tick = 0

    _gui = None
    _map: MapBase = None
    _moveables: MoveableCollector = None
    _builds: BuildCollector = None
    _particles: ParticleCollector = None
    _shots: ShotCollector = None
    _client = None
    _sides: PlayerSideCollector = None
    _ais: AiCollector = None
    _windows = None
    _single_spec = None

    _lock = None
    _use_client = False
    _use_actions = None

    _prof = None
    _prof_flag = 0

    _thread_fr_timer = None
    _thread = None
    _thread_loop = 0

    def __init__(self):
        IWorker.__init__(self)
        IWorker.set(self)

        self._ctx_number = -1
        self._ctx_spec = None
        self._tick = 0
        self._victory_side = None

        self._lock = threading.Lock()
        self._gui = GuiBase()
        self._map = MapBase()
        self._client = Client()
        self._moveables = MoveableCollector()
        self._builds = BuildCollector()
        self._particles = ParticleCollector()
        self._shots = ShotCollector()
        self._sides = PlayerSideCollector()
        self._ais = AiCollector()
        self._windows = WindowManager()

    def get_is_alive(self):
        return self._thread_loop > 0

    def get_is_stopped(self):
        return self._thread_loop == -1

    def get_in_game(self):
        return self._thread_loop > 0 and self._game_state == 3

    def get_ctx_number(self):
        return self._ctx_number

    def get_quick_progress(self):
        return False if self._ctx_spec is None else self._ctx_spec.get_quick_progress()

    def get_quick_build(self):
        return False if self._ctx_spec is None else self._ctx_spec.get_quick_build()

    def get_quick_mine(self):
        return False if self._ctx_spec is None else self._ctx_spec.get_quick_mine()

    def get_is_immed(self):
        return False

    def get_game_state(self):
        return self._game_state

    def set_parent(self, parent):
        self._parent = parent

    def add_command(self, cm):
        if self._use_client:
            self._client.add_command(cm)
        else:
            with self._lock:
                if self._use_actions is None:
                    self._use_actions = []

                [self._use_actions.append(c) for c in cm.pop_all()]

    def check_victory_side(self):
        victory_side = self._sides.determine_wictory_side()

        if victory_side is not self._victory_side:
            self._victory_side = victory_side
            return self._victory_side
        else:
            return None

    def chat_message(self, message):
        if self._gui.get_side() is None:
            pass

        elif self._game_state >= 2 and self._use_client:
            dump = pickle.dumps({'num': self._gui.get_side().number, 'msg': message})

            cmd = Command()
            cmd.append(Command.T_CHAT, Command.get_sort_num(), length=len(dump), bytes=dump)

            self._client.add_command(cmd)

        elif self._game_state >= 2:
            self._gui.chat_message(self._gui.get_side(), message)

    def game_clear(self):
        if self._ctx_number > 0:
            self._ctx_number = -1
            self._ctx_spec = None
            self._game_state = 0
            self._game_ready_flag = False
            self._gui.set_side(None)
            self._particles.clear()
            self._moveables.clear()
            self._builds.clear()
            self._shots.clear()
            self._sides.clear()
            self._map.clear()
            self._ais.clear()
            GotoGroup.clear()
            GraphBatches().set_force_rebuild()
            self.event_game_clear(None)

            return True

        else:
            return False

    def game_connect(self):
        if not self._client.get_is_connected():
            c = self._client.connect_wait()

            self._use_client = self._client.get_is_connected()
            self._tick = self._client.get_tick()

            if not c[0]:
                self.event_error("Cannot connect to server: " + c[1])

        return self._client.get_is_connected()

    def game_disconnect(self):
        if self._client.get_is_connected():
            self._client.disconnect()
            self._use_client = False

    def game_start(self, spec: GameSpec):
        self._use_client = spec.get_server_game()

        if spec.get_server_game() and not self._client.get_is_connected():
            if not self.game_connect():
                return False

        if self._use_client:
            dump = pickle.dumps({'game_spec': spec.serialize()})

            cmd = Command()
            cmd.append(Command.T_GAME_START, Command.get_sort_num(), length=len(dump), bytes=dump)

            self._single_spec = None
            self._client.add_command(cmd)

        else:
            self._client.event_add(Command.T_GAME_START_SINGLE, spec)

    def game_list(self):
        dump = pickle.dumps({'filter': 'smt'})

        cmd = Command()
        cmd.append(Command.T_GAME_LIST, Command.get_sort_num(), length=len(dump), bytes=dump)

        self._client.add_command(cmd)

    def game_enter(self, number):
        self.game_clear()

        cmd = Command()
        cmd.append(Command.T_GAME_GET, Command.get_sort_num(), number=number)

        self._client.add_command(cmd)

    def game_prepare(self):
        if self._game_state == 1:
            IGui.get().drop_menu()
            IGui.get().create_modal_message("Connecting ...")
            IView.get().wnd_update()

            if self._use_client:
                if not self._game_ready_flag:
                    # send ready flag to server
                    Logger.get().logm('(multiplayer) Sending ready flag')
                    self._client.add_command(Command().append(Command.T_GAME_READY, Command.get_sort_num(), ctx=self._ctx_number, waiting_count=0))

            else:
                # singleplayer is allways ready
                self._game_ready_flag = True

            self._game_state = 2

    def game_run(self):
        if self._game_state == 2 and self._game_ready_flag:
            IGui.get().drop_menu()
            IView.get().wnd_update()
            self._ais.start()
            self._game_state = 3
            self._victory_side = None

    def game_quit(self):
        self.stop()

    def start(self):
        self._thread_fr_timer = FrameTimer(ticks_qa=50)
        # self._thread_fr_timer = FrameTimer(ticks_qa=20)

        self._thread = threading.Thread(target=Worker.worker_update, args=(self,))
        self._thread.start()

    def stop(self):
        self._client.disconnect()

        if self._thread is not None:
            self._thread_loop = -1
            self._thread.join(5)

    def worker_update(self):
        #self._prof = cProfile.Profile()
        #self._prof.disable()
        #self._prof.runcall(self._worker_update)
        #self._prof.print_stats(sort="tottime")
        #self._prof.disable()

        self._worker_update()

    def _worker_update(self):

        if self._use_client:
            self._tick = self._client.get_tick()
        else:
            self._tick = 0

        if self._thread_loop == 0:
            self._thread_loop = 1

        while self._thread_loop > 0:
            self._thread_fr_timer.start()

            if not self._use_client:
                self._process_events()
                self._logic_step()
                self._realize_actions()

                self._tick += 1

            else:
                self._process_events()
                self._logic_step()
                self._realize_actions()

                self._tick += 1

                if self._tick % FrameTimer.DEFAULT_SERVER_TICKSTEP == 0 and self._client.get_is_connected():
                    tick_diff = self._client.get_tick() - self._tick

                    #if self._tick % 30 == 0:
                        #print(str(tick_diff) + " | " + str(self._client.get_latency()))

                    if tick_diff > 1:
                        while tick_diff > 0 and self._client.get_is_connected():
                            self._process_events()
                            self._logic_step()
                            self._realize_actions()
                            self._tick += 1
                            tick_diff = self._client.get_tick() - self._tick

                    if tick_diff > 0:
                        # klient je pozadu - dobihani
                        self._thread_fr_timer.dec_time_ticks()

                    else:
                        # klient je casem napred - cekani do plneho sync
                        while tick_diff < 0 and self._client.get_is_connected(): # !! -FrameTimer.DEFAULT_SERVER_TICKSTEP:
                            time.sleep(self._thread_fr_timer.get_fixed_int() / 2.0)
                            tick_diff = self._client.get_tick() - self._tick

            self._thread_fr_timer.end_sleep()
            #use_int = self._thread_fr_timer.end()
            #time.sleep(max(1.0 / 100, use_int))

    def _logic_step(self):
        if self._ctx_number >= 0:
            if self._tick % 50 == 0:
                self._send_state_to_check()

            GraphBatches().get_grounds().update()
            GraphBatches().get_envs().update()
            self._shots.update()
            self._particles.update()
            GotoGroup.update(self._tick, self._tick % 10 == 0)
            self._moveables.update(self._tick)
            self._builds.update(self._tick)
            self._sides.update(self._tick)
            self._gui.logic_update(self._tick)
            self._ais.update(self._tick)

            # !! IView.get().update_view()

        else:
            self._gui.logic_update(self._tick)

            if self._tick > 0 and self._tick % 60 == 0:
                self.game_list()

    def _game_start(self, ctx):
        IGui.get().create_modal_message("Loading ...")
        Logger.get().logm('(multiplayer) Starting')
        Config.get().set_client()

        self._ctx_number = ctx['number']
        self._ctx_spec = GameSpec().unserialize(ctx['spec'])

        self._map.set_fog_type(self._ctx_spec.get_fog_type())
        self._map.unserialize(ctx['map'])
        self._sides.unserialize(ctx['sides'])
        self._moveables.unserialize(ctx['moveables'])
        self._builds.unserialize(ctx['builds'])

        for c in ctx['sides']['list']:
            if c['number'] == ctx['side_number'] and c['ai'] != 'human':
                # pouze sitovy hrac ktery hraje muze byt ai
                self._ais.add(self._sides.get_side_by_number(c['number']), c['ai'])
                break

        focus_side = self._sides.get_side_by_number(ctx['side_number'])
        self._sides.set_focus_side(focus_side)
        self._gui.set_side(focus_side)
        self._map.update_sides(self._sides)
        self._game_state = 1

        if focus_side is not None:
            cd = focus_side.get_builds_coords()
            self._parent.sc_to(cd[0], cd[1], zoom=1.0)

    def _game_start_spec(self, spec):
        IGui.get().create_modal_message("Loading ...")
        Logger.get().logm('(singleplayer) Starting')
        Config.get().set_standalone()

        self._ctx_number = 1
        self._ctx_spec = spec

        self._map.set_fog_type(spec.get_fog_type())
        self._map.build(spec.get_map().get_map_width() + 2, spec.get_map().get_map_height() + 2)

        if spec.get_map().get_blank():
            self._map.generate_blank(len(spec.get_players()))
        else:
            self._map.regenerate_b(spec.get_map().get_seed(), len(spec.get_players()))

        self._sides.clear()
        self._moveables.clear()
        self._builds.clear()

        for pl in spec.get_players():
            self._sides.add_side(pl)

        self._sides.build_units()

        focus_side = None

        for side in self._sides.get_sides():
            if not side.get_is_ai():
                focus_side = side
                break

        if focus_side is None:
            focus_side = self._sides.get_sides()[0]

        self._sides.set_focus_side(focus_side)
        self._gui.set_side(focus_side)
        self._map.update_sides(self._sides)

        if focus_side is not None:
            cd = focus_side.get_builds_coords()
            self._parent.sc_to(cd[0], cd[1], zoom=1.0)

        self._game_state = 1

    def _process_events(self):
        for evt in self._client.event_get():
            if evt[0] == Command.T_GAME_CONTEXT:
                self._game_start(evt[1]['dump'])
                self._tick = self._client.get_tick()
                self.event_game_enter(None)

                if self._prof is not None:
                    print("profiling!")
                    self._prof.enable()

            elif evt[0] == Command.T_GAME_START_SINGLE:
                self._game_start_spec(evt[1])
                self.event_game_enter(None)

            elif evt[0] == Command.T_GAME_START:
                self.event_game_start(evt[1]['dump'])

            elif evt[0] == Command.T_GAME_LIST:
                self.event_game_list(evt[1]['dump'])

            elif evt[0] == Command.T_GAME_READY:
                IGui.get().create_modal_message("Connecting (waiting for " + str(evt[1]['waiting_count']) + " players) ...")

                if evt[1]['waiting_count'] == 0:
                    self._game_ready_flag = True

            elif evt[0] == Command.T_CHAT:
                side = self._sides.get_side_by_number(evt[1]['num'])

                if side is not None:
                    self._gui.chat_message(side, evt[1]['msg'])

            elif evt[0] == Command.T_ERROR:
                self.event_error(evt[1]['error'])

    def _realize_actions(self):
        if self._use_client:
            cmds = self._client.get_actions(self._tick)
        else:
            with self._lock:
                cmds = self._use_actions
                self._use_actions = None

        if cmds is not None:

            c_len = len(cmds)
            c_idx = 0

            while c_idx < c_len:
                item = cmds[c_idx]

                if item[0] in [Command.T_MV_GOTO, Command.T_MV_ATTGOTO]:
                    place_to = self._map.get_place(item[3], item[4])
                    cnt = item[5]
                    mvs = []

                    while cnt > 0:
                        c_idx += 1
                        mvs.append(self._moveables.get_moveable_by_number(cmds[c_idx][3]))
                        cnt -= 1

                    tr = MoveableTravel()
                    tr.add(mvs)

                    if item[0] == Command.T_MV_ATTGOTO:
                        tr.attack_move(place_to)
                    else:
                        tr.goto(place_to)

                    c_idx += 1

                elif item[0] == Command.T_MV_ATTACK:

                    attack_mref = None

                    if item[3] == 0:
                        attack_mref = self._moveables.get_moveable_by_number(item[4])
                    elif item[3] == 1:
                        attack_mref = self._builds.get_build_by_number(item[4])

                    cnt = item[5]
                    mvs = []

                    while cnt > 0:
                        c_idx += 1
                        mvs.append(self._moveables.get_moveable_by_number(cmds[c_idx][3]))
                        cnt -= 1

                    tr = MoveableTravel()
                    tr.add(mvs)
                    tr.goto_attack(attack_mref)

                    c_idx += 1

                elif item[0] in [Command.T_TRAVEL_PLACE]:
                    place_travel = self._map.get_place(item[3], item[4])
                    cnt = item[5]
                    bld = []

                    while cnt > 0:
                        c_idx += 1
                        bld.append(self._builds.get_build_by_number(cmds[c_idx][3]))
                        cnt -= 1

                    tr = BuildAction()
                    tr.add(bld)
                    tr.set_place_travel(place_travel)

                    c_idx += 1

                elif item[0] in [Command.T_MV_HOLD, Command.T_MV_STOP]:

                    cnt = item[3]
                    mvs = []

                    while cnt > 0:
                        c_idx += 1
                        mvs.append(self._moveables.get_moveable_by_number(cmds[c_idx][3]))
                        cnt -= 1

                    if item[0] == Command.T_MV_HOLD:
                        [c.hold() for c in mvs]
                    else:
                        [c.stop() for c in mvs]

                    c_idx += 1

                elif item[0] == Command.T_BUILD_DESTROY:
                    build_ref = self._builds.get_build_by_number(item[3])

                    if build_ref is not None:
                        build_ref.deal_damage(build_ref.get_use_x(), build_ref.get_use_y(), None, ShotSpec.get("tank"), 0, 1000000)

                    c_idx += 1

                elif item[0] == Command.T_ADD_UNIT_COORD:
                    side = self._sides.get_side_by_number(item[6])
                    side.add_unit_coord(item[3], item[4], UnitSpec.get_by_number(item[5]), tile_coords=True)
                    c_idx += 1

                elif item[0] == Command.T_ACTION_ENQUEUE:
                    side = self._sides.get_side_by_number(item[3])
                    build_ref = self._builds.get_build_by_number(item[5])
                    build_trg_ref = self._builds.get_build_by_number(item[6])
                    money_slot = item[7]
                    side.action_enqueue(ActionItemSpec.get_by_number(item[4]), build_ref, build_trg_ref, money_slot, immed=True)
                    c_idx += 1

                elif item[0] == Command.T_ACTION_BUILD:
                    side = self._sides.get_side_by_number(item[3])
                    ac = side.get_actions().get_item_by_number(item[4])

                    build_place = BuildPlace(side, BuildSpec.get_by_number(item[5]))
                    build_place.set_tile(self._map.get_tile(item[6], item[7]))

                    if not build_place.get_is_invalid():
                        side.action_build_realize(ac, build_place, immed=True)

                    c_idx += 1

                elif item[0] == Command.T_ACTION_REFUND:
                    side = self._sides.get_side_by_number(item[3])
                    ac = side.get_actions().get_item_by_number(item[4])

                    side.action_refund(ac, immed=True)
                    c_idx += 1

                elif item[0] == Command.T_ACTION_CANCEL:
                    side = self._sides.get_side_by_number(item[3])
                    ac = side.get_actions().get_item_by_number(item[4])

                    side.action_cancel(ac, immed=True)
                    c_idx += 1

                else:
                    c_idx += 1

    def _send_state_to_check(self):
        if Config.get().get_client_full_state_check():
            # odeslani ulozeneho stavu pro kontrolu (pri debug modu)

            dump = pickle.dumps({
                'number': self._ctx_number,
                'mvs': self._moveables.serialize(),
                'sds': self._sides.serialize()
            })

            cmd = Command()
            cmd.append(Command.T_STATE_CHECK, Command.get_sort_num(), length=len(dump), tick=self._tick, bytes=dump)

            self._client.add_command(cmd)

        else:
            childs = self._moveables.get_childs()

            for idx in range(0, 5):
                mref = childs[(self._tick + idx) % len(childs)]

                if mref is not None and mref.get_is_alive():
                    cmd = Command()
                    cmd.append(Command.T_MV_CHECK, Command.get_sort_num(), ctx=self._ctx_number, tick=self._tick, number=mref.number, x=mref.get_place().get_pos_x(), y=mref.get_place().get_pos_y())

                    self._client.add_command(cmd)
