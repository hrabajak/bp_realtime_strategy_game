import colorsys


class ImageColor:

    _key = None
    _r = 0
    _g = 0
    _b = 0

    _map_r = 0
    _map_g = 0
    _map_b = 0

    _rep_h = 0
    _rep_s = 0
    _rep_v = 0

    _inc_h = 0
    _inc_s = 0
    _inc_v = 0

    def __init__(self, key):
        self._key = key
        self._r = 0
        self._g = 0
        self._b = 0

    def set_rgb(self, r, g, b):
        self._r = r
        self._g = g
        self._b = b
        self.update()
        return self

    def set_hsv(self, h, s, v):
        self._r, self._g, self._b = colorsys.hsv_to_rgb(h, s, v)
        self._r = int(self._r)
        self._g = int(self._g)
        self._b = int(self._b)
        self.update()
        return self

    def set_inc(self, inc_h=0, inc_s=0, inc_v=0):
        self._inc_h = inc_h
        self._inc_s = inc_s
        self._inc_v = inc_v
        self.update()
        return self

    def update(self):
        self._rep_h, self._rep_s, self._rep_v = colorsys.rgb_to_hsv(self._r, self._g, self._b)

        if self._key == "white":
            self._map_r, self._map_g, self._map_b = colorsys.hsv_to_rgb(self._rep_h, self._rep_s, self._rep_v)
        else:
            self._map_r, self._map_g, self._map_b = colorsys.hsv_to_rgb(self._rep_h, 1.0, 255)

        self._map_r = int(self._map_r)
        self._map_g = int(self._map_g)
        self._map_b = int(self._map_b)

    def get_key(self):
        return self._key

    def get_name(self):
        return self._key

    def get_r(self):
        return self._r

    def get_g(self):
        return self._g

    def get_b(self):
        return self._b

    def get_rep_h(self):
        return self._rep_h

    def get_rep_s(self):
        return self._rep_s

    def get_rep_v(self):
        return self._rep_v

    def get_inc_h(self):
        return self._inc_h

    def get_inc_s(self):
        return self._inc_s

    def get_inc_v(self):
        return self._inc_v

    def get_map_color(self):
        return self._map_r, self._map_g, self._map_b
