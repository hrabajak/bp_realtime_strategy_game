import os.path

import pyglet
import random

from game.helper.singleton import Singleton
from game.image.imagecolor import ImageColor
from game.image.imagetransform import ImageTransform
from game.image.texturebuilder import TextureBuilder
from game.interface.iimages import IImages
from game.logger import Logger


class ImageCollector(IImages, metaclass=Singleton):

    _images = {}
    _images_all = []
    _bin = None
    _atlas_tex = None
    _copy_tex = None
    _env_tex = None
    _gui_tex = None
    _mask_tex = None
    _path = None

    color_green = ImageColor("green").set_hsv(0.37, 0.72, 255)
    color_white = ImageColor("white").set_hsv(0, 0, 255)
    color_red = ImageColor("red").set_hsv(0, 0.72, 255)
    color_pink = ImageColor("pink").set_hsv(0.8388, 0.72, 255)
    color_yellow = ImageColor("yellow").set_hsv(0.2050, 0.72, 255)
    color_orange = ImageColor("orange").set_hsv(0.0861, 0.72, 255)
    color_purple = ImageColor("purple").set_hsv(0.7805, 0.72, 255)
    color_teal = ImageColor("teal").set_hsv(0.5844, 0.72, 255)

    color_sorted = [
        color_green,
        color_white,
        color_teal,
        color_yellow,
        color_red,
        color_orange,
        color_pink,
        color_purple
    ]

    def __init__(self):
        IImages.__init__(self)
        IImages.set(self)

    def init(self):
        Logger.get().logm("Initializing bitmaps ...")

        self._path = 'resources/'
        self._bin = pyglet.image.atlas.TextureBin()

        self._atlas_tex = {
            "bin": pyglet.image.atlas.TextureBin(),
            "ids": [],
            "list": [],
            "data": [],
            "hash": {}
        }

        self._copy_tex = {
            "bin": pyglet.image.atlas.TextureBin(),
            "ids": [],
            "list": [],
            "data": [],
            "hash": {}
        }

        self._env_tex = {
            "bin": pyglet.image.atlas.TextureBin(),
            "ids": [],
            "list": [],
            "data": [],
            "hash": {}
        }

        self._gui_tex = {
            "bin": pyglet.image.atlas.TextureBin(),
            "ids": [],
            "list": [],
            "data": [],
            "hash": {}
        }

        self._mask_tex = {
            "bin": pyglet.image.atlas.TextureBin(),
            "ids": [],
            "list": [],
            "data": [],
            "hash": {}
        }

        self.add_image("tex/black.png", app=self._atlas_tex, add_border=1)

        self.add_image("tex/black.png", app=self._copy_tex)
        self.add_image("tex/grass.png", vars=9, app=self._copy_tex)
        self.add_image("tex/grassrock.png", vars=7, app=self._copy_tex)
        self.add_image("tex/field.png", vars=9, app=self._copy_tex)
        self.add_image("tex/dust.png", vars=9, app=self._copy_tex)
        self.add_image("tex/dustrock.png", vars=7, app=self._copy_tex)
        self.add_image("tex/water.png", vars=9, app=self._copy_tex)
        self.add_image("tex/watergrass.png", vars=9, app=self._copy_tex)
        self.add_image("tex/darkgrass.png", vars=9, app=self._copy_tex)
        self.add_image("tex/tree.png", vars=12, app=self._copy_tex)
        self.add_image("tex/concground.png", vars=9, app=self._copy_tex)

        self.add_image("tex/test.png", app=self._env_tex)
        self.add_image("tex/envtree.png", vars=5, app=self._env_tex)
        self.add_image("tex/envbush.png", vars=4, app=self._env_tex)

        self.add_image("tex/test.png", app=self._gui_tex)
        self.add_image("menu/cvutfit.png")

        self.add_image("units/side1/lighttank_pad.png", load_mask=True)
        self.add_image("units/side1/lighttank_damage.png", load_mask=True)
        self.add_image("units/side1/lighttank_turret.png", load_mask=True)
        self.add_image("units/side1/lighttank_cannon.png")

        self.add_image("units/side1/medtank_pad.png", load_mask=True)
        self.add_image("units/side1/medtank_damage.png", load_mask=True)
        self.add_image("units/side1/medtank_turret.png", load_mask=True)
        self.add_image("units/side1/medtank_cannon.png")

        self.add_image("units/side1/jeep_pad.png", load_mask=True)
        self.add_image("units/side1/jeep_damage.png", load_mask=True)
        self.add_image("units/side1/jeep_turret.png")
        self.add_image("units/side1/jeep_cannon.png")

        self.add_image("units/side1/plate_pad.png", load_mask=True)
        self.add_image("units/side1/plate_damage.png", load_mask=True)
        self.add_image("units/side1/plate_turret.png", load_mask=True)
        self.add_image("units/side1/plate_cannon.png")

        self.add_image("units/side1/rocket_pad.png", load_mask=True)
        self.add_image("units/side1/rocket_damage.png", load_mask=True)
        self.add_image("units/side1/rocket_turret.png")
        self.add_image("units/side1/rocket_cannon.png")

        self.add_image("units/side1/s1_miner_pad.png", load_mask=True)
        self.add_image("units/side1/s1_miner_pad_full.png", load_mask=True)

        self.add_image("units/side1/s1_upgrade_pad.png", load_mask=True)
        self.add_image("units/side1/s1_upgrade_pad_full.png", load_mask=True)

        self.add_image("units/side1/s1_builder_pad.png", load_mask=True)
        self.add_image("units/side1/s1_builder_turret.png")

        self.add_image("units/side1/s1_troop0.png", load_mask=True)
        self.add_image("units/side1/s1_troop1.png", load_mask=True)
        self.add_image("units/side1/s1_troop2.png", load_mask=True)
        self.add_image("units/side1/s1_troop3.png", load_mask=True)

        self.add_image("units/side1/s1_sniper0.png", load_mask=True)
        self.add_image("units/side1/s1_sniper1.png", load_mask=True)
        self.add_image("units/side1/s1_sniper2.png", load_mask=True)
        self.add_image("units/side1/s1_sniper3.png", load_mask=True)

        self.add_image("units/side1/s1_troop_cannon0.png", load_mask=True)
        self.add_image("units/side1/s1_troop_cannon1.png", load_mask=True)
        self.add_image("units/side1/s1_troop_cannon2.png", load_mask=True)
        self.add_image("units/side1/s1_troop_cannon3.png", load_mask=True)

        self.add_image("units/side1/troop_dead.png")

        self.add_image("units/side2/basic_pad.png", load_mask=True)
        self.add_image("units/side2/basic_damage.png", load_mask=True)
        self.add_image("units/side2/basic_turret.png", load_mask=True)
        self.add_image("units/side2/basic_cannon.png")

        self.add_image("units/side2/mini_pad.png", load_mask=True)
        self.add_image("units/side2/mini_damage.png", load_mask=True)
        self.add_image("units/side2/mini_turret.png", load_mask=True)
        self.add_image("units/side2/mini_cannon.png")

        self.add_image("units/side2/plasma_pad.png", load_mask=True)
        self.add_image("units/side2/plasma_damage.png", load_mask=True)
        self.add_image("units/side2/plasma_turret.png", load_mask=True)
        self.add_image("units/side2/plasma_cannon.png", load_mask=True)

        self.add_image("units/side2/rock_pad.png", load_mask=True)
        self.add_image("units/side2/rock_damage.png", load_mask=True)
        self.add_image("units/side2/rock_turret.png", load_mask=True)
        self.add_image("units/side2/rock_cannon.png", load_mask=True)

        self.add_image("units/side2/partilery_pad.png", load_mask=True)
        self.add_image("units/side2/partilery_damage.png", load_mask=True)
        self.add_image("units/side2/partilery_turret.png", load_mask=True)
        self.add_image("units/side2/partilery_cannon.png", load_mask=True)

        self.add_image("units/side2/s2_troop0.png", load_mask=True)
        self.add_image("units/side2/s2_troop1.png", load_mask=True)
        self.add_image("units/side2/s2_troop2.png", load_mask=True)
        self.add_image("units/side2/s2_troop3.png", load_mask=True)

        self.add_image("units/side2/s2_troop_beam0.png", load_mask=True)
        self.add_image("units/side2/s2_troop_beam1.png", load_mask=True)
        self.add_image("units/side2/s2_troop_beam2.png", load_mask=True)
        self.add_image("units/side2/s2_troop_beam3.png", load_mask=True)

        self.add_image("units/side2/s2_troop_rocket0.png", load_mask=True)
        self.add_image("units/side2/s2_troop_rocket1.png", load_mask=True)
        self.add_image("units/side2/s2_troop_rocket2.png", load_mask=True)
        self.add_image("units/side2/s2_troop_rocket3.png", load_mask=True)

        self.add_image("units/side2/s2_miner_pad.png", load_mask=True)
        self.add_image("units/side2/s2_miner_pad_full.png", load_mask=True)

        self.add_image("units/side2/s2_upgrade_pad.png", load_mask=True)
        self.add_image("units/side2/s2_upgrade_pad_full.png", load_mask=True)

        self.add_image("units/side2/s2_builder_pad.png", load_mask=True)
        self.add_image("units/side2/s2_builder_turret.png")

        self.add_image("units/side1/build/s1_main.png", load_mask=True)
        self.add_image("units/side1/build/s1_main_damage1.png", load_mask=True, mask_path="units/side1/build/s1_main_mask.png")
        self.add_image("units/side1/build/s1_main_damage2.png", load_mask=True, mask_path="units/side1/build/s1_main_mask.png")
        self.add_image("units/side1/build/s1_main_below.png")

        self.add_image("units/side1/build/s1_factory.png", load_mask=True)
        self.add_image("units/side1/build/s1_factory_damage1.png", load_mask=True, mask_path="units/side1/build/s1_factory_mask.png")
        self.add_image("units/side1/build/s1_factory_damage2.png", load_mask=True, mask_path="units/side1/build/s1_factory_mask.png")
        self.add_image("units/side1/build/s1_factory_below.png")

        self.add_image("units/side1/build/s1_barracks.png", load_mask=True)
        self.add_image("units/side1/build/s1_barracks_damage1.png", load_mask=True, mask_path="units/side1/build/s1_barracks_mask.png")
        self.add_image("units/side1/build/s1_barracks_damage2.png", load_mask=True, mask_path="units/side1/build/s1_barracks_mask.png")
        self.add_image("units/side1/build/s1_barracks_below.png")

        self.add_image("units/side1/build/s1_refinery.png", load_mask=True)
        self.add_image("units/side1/build/s1_refinery_damage1.png", load_mask=True, mask_path="units/side1/build/s1_refinery_mask.png")
        self.add_image("units/side1/build/s1_refinery_damage2.png", load_mask=True, mask_path="units/side1/build/s1_refinery_mask.png")
        self.add_image("units/side1/build/s1_refinery_below.png")

        self.add_image("units/side1/build/s1_lab.png", load_mask=True)
        self.add_image("units/side1/build/s1_lab_damage1.png", load_mask=True, mask_path="units/side1/build/s1_lab_mask.png")
        self.add_image("units/side1/build/s1_lab_damage2.png", load_mask=True, mask_path="units/side1/build/s1_lab_mask.png")
        self.add_image("units/side1/build/s1_lab_below.png")

        self.add_image("units/side1/build/s1_tower.png", load_mask=True)
        self.add_image("units/side1/build/s1_tower_minigun.png")
        self.add_image("units/side1/build/s1_tower_minigun_cannon.png")
        self.add_image("units/side1/build/s1_tower_can.png")
        self.add_image("units/side1/build/s1_tower_can_cannon.png")

        self.add_image("units/side2/build/s2_main.png", load_mask=True)
        self.add_image("units/side2/build/s2_main_damage1.png", load_mask=True, mask_path="units/side2/build/s2_main_mask.png")
        self.add_image("units/side2/build/s2_main_damage2.png", load_mask=True, mask_path="units/side2/build/s2_main_mask.png")
        self.add_image("units/side2/build/s2_main_below.png")

        self.add_image("units/side2/build/s2_factory.png", load_mask=True)
        self.add_image("units/side2/build/s2_factory_damage1.png", load_mask=True, mask_path="units/side2/build/s2_factory_mask.png")
        self.add_image("units/side2/build/s2_factory_damage2.png", load_mask=True, mask_path="units/side2/build/s2_factory_mask.png")
        self.add_image("units/side2/build/s2_factory_below.png")

        self.add_image("units/side2/build/s2_barracks.png", load_mask=True)
        self.add_image("units/side2/build/s2_barracks_damage1.png", load_mask=True, mask_path="units/side2/build/s2_barracks_mask.png")
        self.add_image("units/side2/build/s2_barracks_damage2.png", load_mask=True, mask_path="units/side2/build/s2_barracks_mask.png")
        self.add_image("units/side2/build/s2_barracks_below.png")

        self.add_image("units/side2/build/s2_refinery.png", load_mask=True)
        self.add_image("units/side2/build/s2_refinery_damage1.png", load_mask=True, mask_path="units/side2/build/s2_refinery_mask.png")
        self.add_image("units/side2/build/s2_refinery_damage2.png", load_mask=True, mask_path="units/side2/build/s2_refinery_mask.png")
        self.add_image("units/side2/build/s2_refinery_below.png")

        self.add_image("units/side2/build/s2_lab.png", load_mask=True)
        self.add_image("units/side2/build/s2_lab_damage1.png", load_mask=True, mask_path="units/side2/build/s2_lab.png")
        self.add_image("units/side2/build/s2_lab_damage2.png", load_mask=True, mask_path="units/side2/build/s2_lab.png")
        self.add_image("units/side2/build/s2_lab_below.png")

        self.add_image("units/side2/build/s2_tower.png", load_mask=True)
        self.add_image("units/side2/build/s2_tower_laser.png")
        self.add_image("units/side2/build/s2_tower_laser_cannon.png")
        self.add_image("units/side2/build/s2_tower_rocket.png")
        self.add_image("units/side2/build/s2_tower_rocket_cannon.png")

        self.add_image("gui/gui_cancel.png")
        self.add_image("gui/gui_ok.png")
        self.add_image("gui/gui_back.png")
        self.add_image("gui/gui_settings.png")
        self.add_image("gui/gui_game.png")
        self.add_image("gui/gui_online.png")
        self.add_image("gui/gui_connect.png")
        self.add_image("gui/gui_destroy.png")
        self.add_image("gui/gui_info.png")
        self.add_image("gui/gui_overview.png")

        self.add_image("gui/gui_acc_attmove.png")
        self.add_image("gui/gui_acc_go.png")
        self.add_image("gui/gui_acc_hold.png")
        self.add_image("gui/gui_acc_stop.png")

        self.add_image("gui/gui_b_main.png")
        self.add_image("gui/gui_b_rafinery.png")
        self.add_image("gui/gui_b_laboratory.png")
        self.add_image("gui/gui_b_factory.png")
        self.add_image("gui/gui_b_barracks.png")
        self.add_image("gui/gui_b_turret.png")
        self.add_image("gui/gui_b_turret_sec.png")
        self.add_image("gui/gui_b_upgrade.png")

        self.add_image("gui/gui_round_main.png")
        self.add_image("gui/gui_round_rafinery.png")
        self.add_image("gui/gui_round_laboratory.png")
        self.add_image("gui/gui_round_factory.png")
        self.add_image("gui/gui_round_barracks.png")

        self.add_image("gui/gui_s1_troop.png")
        self.add_image("gui/gui_s1_troop_cannon.png")
        self.add_image("gui/gui_s1_sniper.png")
        self.add_image("gui/gui_s1_jeep.png")
        self.add_image("gui/gui_s1_lighttank.png")
        self.add_image("gui/gui_s1_medtank.png")
        self.add_image("gui/gui_s1_plate.png")
        self.add_image("gui/gui_s1_rocket.png")
        self.add_image("gui/gui_s1_miner.png")

        self.add_image("gui/gui_s2_basic.png")
        self.add_image("gui/gui_s2_miner.png")
        self.add_image("gui/gui_s2_mini.png")
        self.add_image("gui/gui_s2_partilery.png")
        self.add_image("gui/gui_s2_plasma.png")
        self.add_image("gui/gui_s2_rock.png")
        self.add_image("gui/gui_s2_troop.png")
        self.add_image("gui/gui_s2_troop_beam.png")
        self.add_image("gui/gui_s2_troop_rocket.png")

        self.add_image("gui/gui_stat_up.png")
        self.add_image("gui/gui_stat_down.png")

        self.add_image("eff/select.png")
        self.add_image("eff/select_small.png")
        self.add_image("eff/select_hold.png")
        self.add_image("eff/select_hold_small.png")
        self.add_image("eff/select_LD.png")
        self.add_image("eff/select_LT.png")
        self.add_image("eff/select_RD.png")
        self.add_image("eff/select_RT.png")

        self.add_image("eff/white_star.png")
        self.add_image("eff/blank_star.png")
        self.add_image("eff/build_cross.png")
        self.add_image("eff/build_state_below.png", vars=7)
        self.add_image("eff/build_state_top.png", vars=4)
        self.add_image("eff/place_ok.png")
        self.add_image("eff/place_deny.png")

        self.add_image("eff/shoot_tank.png")
        self.add_image("eff/laser_shoot.png")
        self.add_image("eff/laser_shoot_rot.png")
        self.add_image("eff/mazec_shoot.png")
        self.add_image("eff/mazec_shoot_rot.png")
        self.add_image("eff/travel_line.png")
        self.add_image("eff/mini_shoot.png")
        self.add_image("eff/shoot_yellow.png")
        self.add_image("eff/shoot_minirocket.png")
        self.add_image("eff/shoot_artilery.png")
        self.add_image("eff/blueshot.png")

        self.add_image("eff/light1.png")
        self.add_image("eff/track.png")
        self.add_image("eff/track2.png")
        self.add_image("eff/spark1.png")
        self.add_image("eff/spark2.png")
        self.add_image("eff/spark3.png")
        self.add_image("eff/spark4.png")
        self.add_image("eff/boom.png", vars=6, anim_duration=0.1)
        self.add_image("eff/bigboom.png", vars=8, anim_duration=0.08)
        self.add_image("eff/blood1.png", vars=5, anim_duration=0.1)
        self.add_image("eff/blood2.png", vars=6, anim_duration=0.1)
        self.add_image("eff/die1.png", vars=7, anim_duration=0.1)
        self.add_image("eff/blood_hit1.png", vars=5, anim_duration=0.1)
        self.add_image("eff/bullet.png", vars=6, anim_duration=0.05)
        self.add_image("eff/exp1.png", vars=10, anim_duration=0.1)
        self.add_image("eff/exp2.png", vars=9, anim_duration=0.1)
        self.add_image("eff/exp3.png", vars=12, anim_duration=0.1)
        self.add_image("eff/fire1.png", vars=12, anim_duration=0.1)
        self.add_image("eff/fire2.png", vars=8, anim_duration=0.1)
        self.add_image("eff/hit1.png", vars=3, anim_duration=0.1)
        self.add_image("eff/hit2.png", vars=7, anim_duration=0.05)
        self.add_image("eff/hit3.png", vars=5, anim_duration=0.1)
        self.add_image("eff/lasereff.png", vars=7, anim_duration=0.1)
        self.add_image("eff/laserexp.png", vars=5, anim_duration=0.1)
        self.add_image("eff/mini.png", vars=7, anim_duration=0.1)
        self.add_image("eff/miniboom.png", vars=7, anim_duration=0.05)
        self.add_image("eff/smoke.png", vars=9, anim_duration=0.1)
        self.add_image("eff/smoke2.png", vars=4, anim_duration=0.1)
        self.add_image("eff/smoke3.png", vars=9, anim_duration=0.1)
        self.add_image("eff/smoke4.png", vars=6, anim_duration=0.1)
        self.add_image("eff/destroy.png", vars=7)
        self.add_image("eff/goeff.png", vars=6, anim_duration=0.1)
        self.add_image("eff/ateff.png", vars=7, anim_duration=0.1)
        self.add_image("eff/atmeff.png", vars=6, anim_duration=0.1)
        self.add_image("eff/blueboom.png", vars=9, anim_duration=0.075)

        ims = []
        [ims.append(c) for c in self.get_images("goeff")]
        [ims.append(c) for c in reversed(self.get_images("goeff"))]

        self.add_image_animation("treff", ims, anim_duration=0.1, anim_loop=True)

        monos = [
            "lighttank_damage", "lighttank_turret", "lighttank_cannon",
            "medtank_damage", "medtank_turret", "medtank_cannon",
            "jeep_damage", "jeep_turret", "jeep_cannon",
            "plate_damage", "plate_turret", "plate_cannon",
            "rocket_damage", "rocket_turret", "rocket_cannon",
            "basic_damage", "basic_turret", "basic_cannon",
            "mini_damage", "mini_turret", "mini_cannon",
            "plasma_damage", "plasma_turret", "plasma_cannon",
            "rock_damage", "rock_turret", "rock_cannon",
            "partilery_damage", "partilery_turret", "partilery_cannon",
            "s1_miner_pad", "s1_upgrade_pad", "s1_builder_pad", "s1_builder_turret",
            "s2_miner_pad", "s2_upgrade_pad", "s2_builder_pad", "s2_builder_turret",

            "s1_main_damage2", "s1_factory_damage2", "s1_barracks_damage2", "s1_refinery_damage2", "s1_lab_damage2", "s1_tower",
            "s2_main_damage2", "s2_factory_damage2", "s2_barracks_damage2", "s2_refinery_damage2", "s2_lab_damage2", "s2_tower",
            
            "s1_tower_minigun", "s1_tower_minigun_cannon",
            "s1_tower_can", "s1_tower_can_cannon",
            "s2_tower_laser", "s2_tower_laser_cannon",
            "s2_tower_rocket", "s2_tower_rocket_cannon"
        ]

        for m in monos:
            self.add_image_spec(m + "_mono", ImageTransform.image_to_monocrhome(self.get_image(m)))

    def get_atlas_tex_id(self):
        return self._atlas_tex["list"][0].id

    def get_atlas_tex_len(self, key):
        return len(self._atlas_tex["hash"][key])

    def get_atlas_tex(self, key, var=0):
        return self._atlas_tex["list"][self._atlas_tex["hash"][key][var]]

    def get_atlas_tex_random(self):
        return self._atlas_tex["list"][random.randint(0, len(self._atlas_tex["list"]) - 1)]

    def get_copy_tex_color(self, x, y, key, var=0):
        img = self._copy_tex["list"][self._copy_tex["hash"][key][var]]
        data = self._copy_tex["data"][self._copy_tex["hash"][key][var]]
        idx = y * img.width * 4 + x * 4
        return data[idx + 0], data[idx + 1], data[idx + 2], data[idx + 3]

    def get_images_all(self):
        return self._images_all

    def get_image(self, key, var=0):
        lst = self._images[key]["list"]

        if key == 'rnd':
            keys = list(lst.keys())
            return lst[keys[random.randint(0, len(keys) - 1)]][var]
        else:
            return lst[var]

    def get_image_color(self, key, color=None):
        if key is None:
            return None

        if color is None:
            return self.get_image(key)
        else:
            c_key = key + "_" + color.get_key()

            if c_key in self._images:
                return self._images[c_key]["list"][0]

            self._images[c_key] = {
                "list": [],
                "anim": None,
                "loop": False
            }

            im = self.get_image(key)
            im_mask = self.get_mask_image(key)

            if im_mask is None:
                self._images[c_key]["list"].append(im)
            else:
                im = ImageTransform.image_colorize(im, im_mask, color)
                self._images[c_key]["list"].append(im)

            return im

    def get_image_rnd(self, key, rndval=0):
        lst = self._images[key]["list"]

        if rndval == 0:
            rndval = random.randint(0, 99999)

        return lst[rndval % len(lst)]

    def get_images(self, key):
        return self._images[key]["list"]

    def get_images_len(self, key):
        return len(self._images[key]["list"])

    def get_animation(self, key):
        return self._images[key]["anim"]

    def get_animation_loop(self, key):
        return self._images[key]["loop"]

    def get_env_id(self):
        return self._env_tex["list"][0].id

    def get_env_image(self, key, var=0):
        return self._env_tex["list"][self._env_tex["hash"][key][var]]

    def get_env_bin(self):
        return self._env_tex["bin"]

    def get_gui_id(self):
        return self._gui_tex["list"][0].id

    def get_gui_image(self, key, var=0):
        return self._gui_tex["list"][self._gui_tex["hash"][key][var]]

    def get_gui_bin(self):
        return self._gui_tex["bin"]

    def get_mask_image(self, key, var=0):
        if key in self._mask_tex["hash"]:
            return self._mask_tex["list"][self._mask_tex["hash"][key][var]]
        else:
            return None

    def get_image_texture(self, hash, data_map, def_type, number, alternative):
        key = "tex_" + str(hash)

        if key not in self._atlas_tex["hash"]:
            bld = TextureBuilder(self)
            bld.prepare()
            bld.build(data_map, def_type, number, alternative)

            img = bld.get_texture()
            img = ImageTransform.image_add_border(img, 1)
            # img.save('ee_' + hash + '.png')

            added = self.add_image_spec(key, img, app=self._atlas_tex, add_border=0)

            #for atlas in self._atlas_tex["bin"].atlases:
                #print("USAGE", len(self._atlas_tex["bin"].atlases), atlas.allocator.get_usage(), "ID", added.id)

        return self.get_atlas_tex(key)

    def get_texture_bin(self):
        return self._atlas_tex["bin"]

    def get_texture_ids(self):
        return self._atlas_tex["ids"]

    def add_image_spec(self, key, img, app=None, add_border=1):
        if app is not None:
            atlas = app["bin"].add(img.get_image_data(), border=add_border)

            app["list"].append(atlas)
            app["hash"][key] = [len(app["list"]) - 1]

            if atlas.id not in app["ids"]:
                app["ids"].append(atlas.id)

            return atlas

        else:
            if key not in self._images:
                self._images[key] = {
                    "list": [],
                    "anim": None
                }

            reg = self._bin.add(img.get_image_data(), border=add_border)
            reg.anchor_x = img.anchor_x
            reg.anchor_y = img.anchor_y

            self._images[key]["list"].append(reg)
            self._images_all.append(reg)

            return reg

    def add_image(self, path, vars=None, anim_duration=0.0, anim_loop=False, app=None, add_border=0, use_key=None, load_mask=False, mask_path=None):
        if use_key is not None:
            key = use_key
        else:
            idx = path.rindex("/")

            if (idx >= 0):
                key = path[idx + 1:]
            else:
                key = path

            idx = key.rindex(".")

            if idx >= 0:
                key = key[:idx]

        if app is None:
            self._images[key] = {
                "list": [],
                "anim": None,
                "loop": False
            }

        if vars is None:
            img = pyglet.image.load(self._path + path)
            img = img.get_texture()

            if add_border > 0:
                img = ImageTransform.image_add_border(img, add_border)

            img.anchor_x = img.width // 2
            img.anchor_y = img.height // 2

            if app is not None:
                aimg = app["bin"].add(img.get_image_data(), border=1)
                aimg.anchor_x = img.width // 2
                aimg.anchor_y = img.height // 2

                app["list"].append(aimg)
                app["data"].append(img.get_image_data().get_data('RGBA', 4 * img.width))
                app["hash"][key] = [len(app["list"]) - 1]

            else:
                reg = self._bin.add(img.get_image_data(), border=1)
                reg.anchor_x = img.anchor_x
                reg.anchor_y = img.anchor_y

                self._images[key]["list"].append(reg)
                self._images_all.append(reg)

        else:
            idx = path.rindex(".")
            ext = path[idx + 1:]
            path = path[:idx]

            for var in range(0, vars):
                vst = str(var)
                if len(vst) == 1:
                    vst = '0' + vst

                img = pyglet.image.load(self._path + path + '_' + vst + '.' + ext)
                img = img.get_texture()

                if add_border > 0:
                    img = ImageTransform.image_add_border(img, add_border)

                img.anchor_x = img.width // 2
                img.anchor_y = img.height // 2

                if app is not None:
                    aimg = app["bin"].add(img.get_image_data(), border=1)
                    aimg.anchor_x = img.width // 2
                    aimg.anchor_y = img.height // 2

                    app["list"].append(aimg)
                    app["data"].append(img.get_image_data().get_data('RGBA', 4 * img.width))

                    if key not in app["hash"]:
                        app["hash"][key] = []

                    app["hash"][key].append(len(app["list"]) - 1)

                else:
                    reg = self._bin.add(img.get_image_data(), border=1)
                    reg.anchor_x = img.anchor_x
                    reg.anchor_y = img.anchor_y

                    self._images[key]["list"].append(reg)
                    self._images_all.append(reg)

        if anim_duration > 0:
            self._images[key]["anim"] = (self._images[key]["list"], anim_duration, anim_loop)
            self._images[key]["loop"] = anim_loop

        if load_mask:
            if mask_path is None:
                pts = os.path.splitext(path)
                mask_path = pts[0] + "_mask" + pts[1]

            self.add_image(mask_path, app=self._mask_tex, use_key=key)

    def add_image_animation(self, key, image_list, anim_duration=0.0, anim_loop=False):
        self._images[key] = {
            "list": image_list,
            "anim": (image_list, anim_duration, anim_loop),
            "loop": anim_loop
        }

    @staticmethod
    def color_get_by_key(color_key):
        for c in ImageCollector.color_sorted:
            if c.get_key() == color_key:
                return c

        return None
