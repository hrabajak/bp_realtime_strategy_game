from game.helper.singleton import Singleton
from game.interface.iimages import IImages


class ImageCollectorDummy(IImages, metaclass=Singleton):

    def __init__(self):
        IImages.__init__(self)

    def get_image(self, key, var=0):
        return None

    def get_image_color(self, key, color=None):
        return None

    def get_image_rnd(self, key, rndval=0):
        return None

    def get_animation(self, key):
        return None

    def get_image_texture(self, hash, data_map, def_type, number, alternative):
        return None
