import pyglet

from libc.stdlib cimport malloc, free

from game.helper.colors cimport color_hsv, color_rgb, rgb_to_hsv, hsv_to_rgb


class ImageTransform:

    @staticmethod
    def image_to_monocrhome(img):
        cdef long width, height, idx, v_min, v_max, v_total, v_use
        cdef int r, g, b, h, s, v
        cdef double kf
        cdef object v_qa, new_img, bytes
        cdef color_hsv hsv
        cdef color_rgb rgb

        width = img.width
        height = img.height

        bytes = list(img.get_image_data().get_data('RGBA', 4 * width))
        v_min = 255
        v_max = 0
        v_qa = {}
        v_total = 0

        for idx in range(0, width * height):
            alpha = bytes[idx * 4 + 3]

            if alpha > 0:
                hsv = rgb_to_hsv(bytes[idx * 4 + 0], bytes[idx * 4 + 1], bytes[idx * 4 + 2])
                hsv.s = 0
                rgb = hsv_to_rgb(hsv.h, hsv.s, hsv.v)
                bytes[idx * 4 + 0] = int(rgb.r)
                bytes[idx * 4 + 1] = int(rgb.g)
                bytes[idx * 4 + 2] = int(rgb.b)

                if alpha > 150:
                    v_qa[int(hsv.v)] = v_qa.get(int(hsv.v), 0) + 1
                    v_total += 1

        v_keys = list(v_qa.keys())
        v_keys.sort()

        # orez dolniho percentilu

        for idx in range(0, len(v_keys)):
            kf = (v_qa[v_keys[idx]] / v_total) * 100.0

            if kf > 1:
                v_min = v_keys[idx]
                break

        # orez horniho percentilu

        for idx in range(len(v_keys) - 1, -1, -1):
            kf = (v_qa[v_keys[idx]] / v_total) * 100.0

            if kf > 1:
                v_max = v_keys[idx]
                break

        v_use = 60

        if v_min < v_max:
            for idx in range(0, width * height):
                hsv = rgb_to_hsv(bytes[idx * 4 + 0], bytes[idx * 4 + 1], bytes[idx * 4 + 2])

                if hsv.v <= v_min:
                    hsv.v = 0
                elif hsv.v >= v_max:
                    hsv.v = v_use
                else:
                    kf = (hsv.v - v_min) / (v_max - v_min)
                    hsv.v = round(kf * v_use)

                rgb = hsv_to_rgb(hsv.h, hsv.s, hsv.v)
                bytes[idx * 4 + 0] = int(rgb.r)
                bytes[idx * 4 + 1] = int(rgb.g)
                bytes[idx * 4 + 2] = int(rgb.b)

        new_img = pyglet.image.create(width, height)
        new_img.anchor_x = width // 2
        new_img.anchor_y = height // 2

        new_img.get_image_data().set_data('RGBA', 4 * width, ''.join(map(chr, bytes)))

        return new_img.get_texture()

    @staticmethod
    def image_add_border(img, border):
        cdef long width, height, n_width, n_height, i
        cdef object bytes, n_bytes, new_img

        width = img.width
        height = img.height

        bytes = list(img.get_image_data().get_data('RGBA', 4 * width))

        n_width = width + border * 2
        n_height = height + border * 2

        new_img = pyglet.image.create(n_width, n_height)
        new_img.anchor_x = n_width // 2
        new_img.anchor_y = n_height // 2

        n_bytes = []

        for i in range(0, border):
            n_bytes += bytes[0:4] * border
            n_bytes += bytes[0:width * 4]
            n_bytes += bytes[(width - 1) * 4:width * 4] * border

        for i in range(0, height):
            n_bytes += bytes[i * width * 4:i * width * 4 + 4] * border
            n_bytes += bytes[i * width * 4:(i * width + width) * 4]
            n_bytes += bytes[(i * width + width - 1) * 4:(i * width + width - 1) * 4 + 4] * border

        for i in range(0, border):
            n_bytes += bytes[((height - 1) * width) * 4:((height - 1) * width) * 4 + 4] * border
            n_bytes += bytes[((height - 1) * width) * 4:((height - 1) * width) * 4 + width * 4]
            n_bytes += bytes[((height - 1) * width) * 4 + (width - 1) * 4:width * height * 4] * border

        new_img.get_image_data().set_data('RGBA', 4 * n_width, ''.join(map(chr, n_bytes)))

        return new_img.get_texture()

    @staticmethod
    def image_colorize(img, img_mask, color):
        cdef long fx, fy, idx, im_width, im_height
        cdef double s_total, s_count, v_total, v_count
        cdef double s_avg, v_avg, rep_h, rep_s, rep_v
        cdef object bts, bts_mask, new_img
        cdef int * bts_ptr = NULL
        cdef int * bts_mask_ptr = NULL
        cdef color_hsv hsv
        cdef color_rgb rgb

        im_width = img.width
        im_height = img.height

        bts = list(img.get_image_data().get_data('RGBA', 4 * img.width))
        bts_ptr = <int *>malloc(len(bts) * sizeof(int))

        for idx in range(0, len(bts)):
            bts_ptr[idx] = bts[idx]

        if img_mask is not None:
            bts_mask = list(img_mask.get_image_data().get_data('RGBA', 4 * img.width))
            bts_mask_ptr = <int *>malloc(len(bts_mask) * sizeof(int))

            for idx in range(0, len(bts_mask)):
                bts_mask_ptr[idx] = bts_mask[idx]

        s_total = 0
        s_count = 0

        v_total = 0
        v_count = 0

        for fy in range(0, im_height):
            for fx in range(0, im_width):
                idx = fy * im_width * 4 + fx * 4

                if bts_mask_ptr == NULL or bts_mask_ptr[idx + 3] > 0:
                    if bts_ptr[idx + 3] > 0: # alpha
                        hsv = rgb_to_hsv(bts_ptr[idx + 0], bts_ptr[idx + 1], bts_ptr[idx + 2])

                        s_total += hsv.s
                        s_count += 1

                        v_total += hsv.v
                        v_count += 1

        s_avg = (s_total / s_count)
        v_avg = (v_total / v_count)

        rep_h, rep_s, rep_v = color.get_rep_h(), color.get_rep_s(), color.get_rep_v()

        new_img = pyglet.image.create(im_width, im_height)
        new_img.anchor_x = im_width // 2
        new_img.anchor_y = im_height // 2

        for fy in range(0, im_height):
            for fx in range(0, im_width):
                idx = fy * im_width * 4 + fx * 4

                if bts_mask_ptr == NULL or bts_mask_ptr[idx + 3] > 0:
                    if bts_ptr[idx + 3] > 0: # alpha
                        hsv = rgb_to_hsv(bts_ptr[idx + 0], bts_ptr[idx + 1], bts_ptr[idx + 2])

                        hsv.h = rep_h
                        hsv.s = rep_s

                        #hsv.h += color.get_inc_h()
                        #hsv.s += color.get_inc_s()
                        #hsv.v += color.get_inc_v()

                        if v_avg > 180:
                            hsv.s -= 0.1
                            hsv.v -= 100

                        elif s_avg < 0.5:
                            hsv.s -= 0.30
                            hsv.v -= 10

                        else:
                            hsv.s -= 0.2

                        if hsv.h < 0:
                            hsv.h = 3.6 + hsv.h

                        rgb = hsv_to_rgb(hsv.h, max(0.0, min(hsv.s, 1.0)), max(0, min(hsv.v, 255)))

                        bts_ptr[idx + 0] = int(min(rgb.r, 255))
                        bts_ptr[idx + 1] = int(min(rgb.g, 255))
                        bts_ptr[idx + 2] = int(min(rgb.b, 255))

        new_img.get_image_data().set_data('RGBA', 4 * img.width, bytes([bts_ptr[idx] for idx in range(0, len(bts))]))

        free(bts_ptr)

        if bts_mask_ptr != NULL:
            free(bts_mask_ptr)

        return new_img.get_texture()
