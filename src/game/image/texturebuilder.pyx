import random
import pyglet

from game.map.maptiletexture import MapTileTexture


class TextureBuilder:

    _images = None
    _width = 50
    _height = 50
    _n_img = None
    _bytes = None
    _typemap = []
    _number = 0
    _alternative = 0

    def __init__(self, images):
        self._images = images

    def get_texture(self):
        return self._n_img.get_texture()

    def get_tex_color(self, type):
        col = (0, 0, 0)

        if type == 0:
            col = (0, 0, 255, 255)
        elif type == 1:
            col = (0, 255, 0, 255)
        elif type == 2:
            col = (156, 161, 26, 255)

        return col

    def get_mix_color(self, col_a, col_b, opacity):
        return (
            int(col_a[0] * (opacity / 255.0) + col_b[0] * (1.0 - (opacity / 255.0))),
            int(col_a[1] * (opacity / 255.0) + col_b[1] * (1.0 - (opacity / 255.0))),
            int(col_a[2] * (opacity / 255.0) + col_b[2] * (1.0 - (opacity / 255.0)))
        )

    def get_tex_color_coords(self, type, x, y):
        # !! return (255, 255, 255)

        if type == MapTileTexture.T_DUST:
            if self._alternative == 1:
                return self._images.get_copy_tex_color(x, y, "dustrock", var=self._number % 7)
            else:
                return self._images.get_copy_tex_color(x, y, "dust", var=self._number % 9)

        elif type == MapTileTexture.T_BLANK:
            return self._images.get_copy_tex_color(x, y, "black")

        elif type == MapTileTexture.T_CONCGROUND:
            return self._images.get_copy_tex_color(x, y, "concground", var=self._number % 9)

        elif type in [MapTileTexture.T_WATERGRASS, MapTileTexture.T_WATER_TREE]:
            return self._images.get_copy_tex_color(x, y, "watergrass", var=self._number % 9)

        elif type in [MapTileTexture.T_TREE, MapTileTexture.T_DEEP_TREE, MapTileTexture.T_DARKGRASS]:
            return self._images.get_copy_tex_color(x, y, "darkgrass", var=self._number % 9)

        elif type in [MapTileTexture.T_GRASS]:
            if self._alternative == 1:
                return self._images.get_copy_tex_color(x, y, "grassrock", var=self._number % 7)
            else:
                return self._images.get_copy_tex_color(x, y, "grass", var=self._number % 9)

        elif type == MapTileTexture.T_WATER:
            return self._images.get_copy_tex_color(x, y, "water", var=self._number % 9)

        elif type == MapTileTexture.T_FIELD:
            col = self._images.get_copy_tex_color(x, y, "field", var=self._number % 9)

            if col[3] < 255:
                if self._alternative == 1:
                    col_mix = self._images.get_copy_tex_color(x, y, "grassrock", var=self._number % 7)
                else:
                    col_mix = self._images.get_copy_tex_color(x, y, "grass", var=self._number % 9)

                col = self.get_mix_color(col, col_mix, col[3])

            return col

        else:
            return self.get_tex_color(type)

    def prepare(self):
        self._width = 50
        self._height = 50

        self._n_img = pyglet.image.create(self._width, self._height)
        self._n_img.anchor_x = self._width // 2
        self._n_img.anchor_y = self._height // 2

        self._typemap = []

        for fy in range(0, 3):
            ls = []

            for fx in range(0, 3):
                ls.append(None)

            self._typemap.append(ls)

        self._bytes = list(self._n_img.get_image_data().get_data('RGBA', 4 * self._width))

    def _put_pixel(self, x, y, col, alpha=255):
        idx = y * self._width * 4 + x * 4
        self._bytes[idx + 0] = int(col[0])
        self._bytes[idx + 1] = int(col[1])
        self._bytes[idx + 2] = int(col[2])
        self._bytes[idx + 3] = int(alpha)

    def build(self, data_map, def_type, number, alternative):
        cdef long fx, fy, p_fx, p_fy, df_x, df_y, trg_x, trg_y, idx
        cdef long from_x, from_y, to_x, to_y, tmp, fill_x, fill_y
        cdef long st_x, st_y, use_x, use_y
        cdef int set_type, tp

        self._number = number
        self._alternative = alternative

        for fy in range(0, 3):
            for fx in range(0, 3):

                typemap_slot = [-1, -1, -1, -1, -1, -1, -1, -1, -1]

                for p_fy in range(0, 3):
                    for p_fx in range(0, 3):

                        set_type = data_map[fy][fx]

                        df_x = p_fx - 1
                        df_y = p_fy - 1

                        if df_x < 0 and fx > 1:
                            df_x = -1
                        elif df_x > 0 and fx < 1:
                            df_x = 1
                        else:
                            df_x = 0

                        if df_y < 0 and fy < 1:
                            df_y = 1
                        elif df_y > 0 and fy > 1:
                            df_y = -1
                        else:
                            df_y = 0

                        if df_x != 0 or df_y != 0:

                            trg_x = fx + df_x
                            trg_y = fy + df_y

                            if trg_x >= 0 and trg_x < 3 and trg_y >= 0 and trg_y < 3 and data_map[trg_y][trg_x] != data_map[fy][fx]:
                                set_type = data_map[trg_y][trg_x]

                        idx = p_fy * 3 + p_fx
                        typemap_slot[idx] = set_type

                self._typemap[fy][fx] = typemap_slot

                """
                hh = ''

                for c in typemap_slot:
                    if c == 0:
                        hh += '.' + str(c)
                    else:
                        hh += '.~'

                if hh not in ImageTransform._typemap_cache:
                    ImageTransform._typemap_cache[hh] = True
                    print(ImageTransform._typemap_cache)
                """

        for fy in range(0, 3):
            for fx in range(0, 3):

                from_y = fy * 16
                from_x = fx * 16
                to_y = self._height if fy == 2 else from_y + 16
                to_x = self._width if fx == 2 else from_x + 16

                tmp = self._height - to_y
                to_y = self._height - from_y
                from_y = tmp

                typemap_slot = self._typemap[fy][fx]

                for p_fy in range(from_y, to_y):
                    for p_fx in range(from_x, to_x):

                        idx = ((p_fy - from_y) // 6) * 3 + (p_fx - from_x) // 6
                        set_type = typemap_slot[idx]

                        col = self.get_tex_color_coords(set_type, p_fx, p_fy)
                        self._put_pixel(p_fx, p_fy, col)

                typemap_slot_use = [0, 0, 0, 0, 0, 0, 0, 0, 0]

                coords_list = []
                coords_list.append([(1, 0), (-1, 0), (0, 1), (0, -1)])
                coords_list.append([(1, 1), (-1, -1), (-1, 1), (1, -1)])

                for coords in coords_list:
                    for t_fy in range(0, 3):
                        for t_fx in range(0, 3):

                            tp = typemap_slot[t_fy * 3 + t_fx]

                            for cd in coords:
                                trg_fx = t_fx + cd[0]
                                trg_fy = t_fy + cd[1]

                                if trg_fx >= 0 and trg_fx < 3 and trg_fy >= 0 and trg_fy < 3 and typemap_slot_use[trg_fy * 3 + trg_fx] == 0 and typemap_slot[trg_fy * 3 + trg_fx] < tp:

                                    typemap_slot_use[trg_fy * 3 + trg_fx] = 1

                                    st_x = min(from_x + trg_fx * 6, self._width - 1)
                                    st_y = min(from_y + trg_fy * 6, self._height - 1)

                                    if cd[0] == 1 and cd[1] == 0:
                                        for fill_y in range(0, 6):
                                            for fill_x in range(0, 6):
                                                use_x = min(min(st_x + fill_x, from_x + 15), self._width - 1)
                                                use_y = min(st_y + fill_y, self._height - 1)

                                                #if use_y % (fill_x // 2 + 1) == 0:
                                                if random.randint(0, fill_x // 2) == 0:
                                                    self._put_pixel(use_x, use_y, self.get_tex_color_coords(tp, use_x, use_y))

                                    elif cd[0] == -1 and cd[1] == 0:
                                        st_x -= 1

                                        for fill_y in range(0, 6):
                                            for fill_x in range(6, -1, -1):
                                                use_x = min(st_x + fill_x, self._width - 1)
                                                use_y = min(st_y + fill_y, self._height - 1)

                                                #if use_y % ((6 - fill_x) // 2 + 1) == 0:
                                                if random.randint(fill_x // 2, 6 // 2) == 6 // 2:
                                                    self._put_pixel(use_x, use_y, self.get_tex_color_coords(tp, use_x, use_y))

                                    elif cd[0] == 0 and cd[1] == -1:
                                        st_y -= 1

                                        for fill_y in range(6, -1, -1):
                                            for fill_x in range(0, 6):
                                                use_x = min(st_x + fill_x, self._width - 1)
                                                use_y = min(st_y + fill_y, self._height - 1)

                                                #if use_x % ((6 - fill_y) // 2 + 1) == 0:
                                                if random.randint(fill_y // 2, 6 // 2) == 6 // 2:
                                                    self._put_pixel(use_x, use_y, self.get_tex_color_coords(tp, use_x, use_y))

                                    elif cd[0] == 0 and cd[1] == 1:
                                        for fill_y in range(0, 6):
                                            for fill_x in range(0, 6):
                                                use_x = min(st_x + fill_x, self._width - 1)
                                                use_y = min(st_y + fill_y, self._height - 1)

                                                #if use_x % (fill_y // 2 + 1) == 0:
                                                if random.randint(0, fill_y // 2) == 0:
                                                    self._put_pixel(use_x, use_y, self.get_tex_color_coords(tp, use_x, use_y))

        self._n_img.get_image_data().set_data('RGBA', 4 * self._width, ''.join(map(chr, self._bytes)))

        """
        fnm = 'img'

        for fy in range(0, 3):
            for fx in range(0, 3):
                fnm += '.' + str(data_map[fy][fx])

        fnm += '.png'

        n_img.save(fnm)
        """
