from game.action.actionitemspec import ActionItemSpec
from game.moveable.unitspec import UnitSpec
from game.window.button import Button
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position
from game.window.windowelement import WindowElement


class GuiSpecView:

    _panel = None
    _lab_tit = None
    _bt_a = None
    _focus_spec = None
    _items = []

    def __init__(self, focus_spec, add_button):
        self._focus_spec = focus_spec

        ac_spec = ActionItemSpec.get(self._focus_spec.get_key())

        self._panel = Panel(0, 0, 300, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 50, self._focus_spec.get_name() + (" " + str(ac_spec.get_money_cost()) + " $" if ac_spec is not None else ""))
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        if ac_spec is not None:
            self._lab_tit.set_image_name(ac_spec.get_image_name())

        st_note = "Strength: " + str(round(self._focus_spec.get_damage_per_second()))

        if self._focus_spec.get_shot_spec() is not None and self._focus_spec.get_shot_spec().get_splash_range() > 0:
            st_note += " (splash " + self._focus_spec.get_shot_spec().get_splash_info() + ")"

        l1 = Label(10, 0, 300, 20, st_note)
        l1.init(self._panel)

        l2 = Label(10, 0, 300, 20, "Endurance: " + str(round(self._focus_spec.get_endurance_neutral())))
        l2.init(self._panel)

        l2 = Label(10, 0, 300, 20, "Range: " + str(round(self._focus_spec.get_range_tiles())))
        l2.init(self._panel)

        if self._focus_spec.get_attack_able():
            box = WindowElement(0, 15, 400, 10)
            box.init(self._panel)

            for spec in UnitSpec.get_all():
                if spec.get_attack_able():
                    ac_spec = ActionItemSpec.get(spec.get_key())

                    if ac_spec is not None and ac_spec.get_image_name() is not None:
                        def_perc = round((spec.get_bonus_perc(self._focus_spec) - 1.0) * 100.0)

                        if def_perc != 0:
                            box = WindowElement(0, 15, 400, 30)

                            la = Label(0, 0, 160, 30, "VS " + spec.get_name() + ": ")
                            la.set_image_name(ac_spec.get_image_name())
                            la.init(box)

                            lb = Label(0, 0, 120, 30, "-")
                            lb.init(box)
                            lb.set_image_name("gui_stat_down" if def_perc < 0 else "gui_stat_up")
                            lb.set_text(("-" if def_perc < 0 else "+") + str(abs(def_perc)) + " %")

                            box.organize_x(10)
                            box.init(self._panel)

                            self._items.append({
                                'spec': spec,
                                'ac_spec': ac_spec,
                                'box': box,
                                'la': la,
                                'lb': lb
                            })

        if add_button:
            box = WindowElement(0, 15, 400, 30)
            box.init(self._panel)

            self._bt_b = Button(10, 10, 200, 40, "Close")
            self._bt_b.init(self._panel)

            @self._bt_b.event_click(self)
            def _f(this):
                this.destroy()

        self._panel.get_pos().set_height(self._panel.organize_y(5))
        self._panel.pos_update()
        self._panel.raise_to_top()

    def destroy(self):
        for c in self._items:
            c['box'].destroy()

        self._items.clear()

        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
            self._bt_a = None
