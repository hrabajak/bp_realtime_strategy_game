from game.interface.igui import IGui
from game.window.button import Button
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position


class GuiAbout:

    _panel = None
    _lab_tit = None
    _lab = None
    _bt_a = None

    def __init__(self):
        self._panel = Panel(0, 0, 400, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)
        self._panel.pos_update()

        self._lab_tit = Label(10, 10, 330, 50, "About")
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        msg = ""
        msg += "The game was created as part of a bachelor's thesis for the Czech Technical University in Prague Faculty of Information Technology https://fit.cvut.cz/. \n\n"
        msg += "The author of the game code and graphics is Jakub Hrabálek (j-hrb@seznam.cz). \n\n"
        msg += "The sound was taken from the https://freesound.org/ portal. \n\n"
        msg += "The tools that were used to develop the game: \n\n"
        msg += "- Python optimized with Cython \n"
        msg += "- Libraries: Pyglet, PyShaders, OrderedSet, DeepDiff \n"
        msg += "- PyCharm IDE \n"
        msg += "- Audacity sound editor \n"
        msg += "- Aseprite pixel-art image editor \n"
        msg += "- GIMP graphic editor \n"
        msg += "- PyInstaller tool \n"

        self._lab = Label(10, 10, 380, 350, msg)
        self._lab.init(self._panel)
        self._lab.set_y_add(150)

        self._bt_a = Button(10, 10, 380, 40, "Back")
        self._bt_a.set_image_name("gui_back")
        self._bt_a.init(self._panel)

        @self._bt_a.event_click(self)
        def _(this):
            IGui.get().create_menu("menu")

        self._panel.get_pos().set_height(self._panel.organize_y(10))
        self._panel.pos_update()

    def destroy(self):
        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
