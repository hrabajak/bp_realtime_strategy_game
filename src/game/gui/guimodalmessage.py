from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position


class GuiModalMessage:

    _panel = None
    _lab_tit = None

    def __init__(self, caption):
        self._panel = Panel(0, 0, 450, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 450, 100, caption, text_align="center")
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._panel.get_pos().set_height(self._panel.organize_y(10))
        self._panel.pos_update()
        self._panel.raise_to_top(modal=True)

    def destroy(self):
        self._panel.destroy()
        self._panel = None
