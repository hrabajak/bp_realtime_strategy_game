from game.config import Config
from game.gamestat.gameplayerspec import GamePlayerSpec
from game.gamestat.gamespec import GameSpec
from game.gui.guimessagebox import GuiMessageBox
from game.gui.guistdgame import GuiStdGame
from game.interface.igui import IGui
from game.interface.iworker import IWorker
from game.window.button import Button
from game.window.label import Label
from game.window.listbox import ListBox
from game.window.panel import Panel
from game.window.position import Position


class GuiMenuMulti:

    _panel = None
    _lab_tit = None
    _bt_a = None
    _bt_b = None
    _bt_c = None
    _list_a = None

    _c_seed = 7

    def __init__(self):
        self._panel = Panel(0, 0, 400, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 50, "Online server game")
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._bt_a = Button(10, 10, 380, 40, "New server game")
        self._bt_a.set_image_name("gui_online")
        self._bt_a.init(self._panel)

        @self._bt_a.event_click(self)
        def _(this):
            GuiStdGame(is_multi=True)

        self._list_a = ListBox(10, 10, 380, 200)
        self._list_a.init(self._panel)

        self._bt_b = Button(10, 10, 380, 40, "Connect to game")
        self._bt_b.set_image_name("gui_connect")
        self._bt_b.init(self._panel)

        @self._bt_b.event_click(self)
        def _(this):
            it = this._list_a.item_get()

            if it is None:
                GuiMessageBox("Error", "No game selected!")
            else:
                this.destroy()

                try:
                    game_num = int(it.get_key())
                except ValueError:
                    game_num = 0

                IWorker.get().game_enter(game_num)

        self._bt_c = Button(10, 10, 380, 40, "Back")
        self._bt_c.set_image_name("gui_back")
        self._bt_c.init(self._panel)

        @self._bt_c.event_click(self)
        def _f(this):
            IWorker.get().game_disconnect()
            IWorker.get().event_reset()
            IGui.get().create_menu("menu")

        self._panel.get_pos().set_height(self._panel.organize_y(10))
        self._panel.pos_update()

        IWorker.get().event_reset()
        IWorker.get().event_game_start = self._event_game_start
        IWorker.get().event_game_list = self._event_game_list
        IWorker.get().event_game_enter = self._event_game_enter
        IWorker.get().event_error = self._event_error
        IWorker.get().game_connect()

    def _event_game_start(self, data):
        if data is not None:
            GuiMessageBox("New game", "Game with number started: " + str(data["ctx_number"]))

    def _event_game_list(self, data):
        if data is not None and self._list_a is not None:
            items = {}

            for c in data['game_context_list']:
                items[c['ctx_number']] = c['readable']

            self._list_a.item_set_dict(items)

    def _event_game_enter(self, data):
        self.destroy()

    def _event_error(self, error):
        ms = GuiMessageBox("Error", error)

        @ms.event_ok(self)
        def _(this):
            IWorker.get().game_clear()
            IGui.get().create_menu("menu")
                
    def destroy(self):
        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
            self._bt_a = None
            self._bt_b = None
            self._list_a = None
