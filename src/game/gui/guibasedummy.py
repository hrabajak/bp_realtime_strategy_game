from game.build.buildspec import BuildSpec
from game.helper.singleton import Singleton
from game.interface.igui import IGui


class GuiBaseDummy(IGui, metaclass=Singleton):

    def __init__(self):
        IGui.__init__(self)
        IGui.set(self)

    def set_side(self, side):
        pass

    def create_modal_message(self, caption):
        pass

    def drop_modal_messages(self):
        pass

    def chat_message(self, side, message):
        pass

    def create_menu(self, tp):
        pass

    def get_side(self):
        return None

    def action_item_notify(self, action):
        pass

    def focus_remove(self):
        pass

    def focus_moveables(self, side, moveables):
        pass

    def focus_builds(self, side, builds):
        pass

    def focus_building(self, spec: BuildSpec, ac=None):
        pass

    def focus_upgrade(self, side, build_src_ref, ac_spec):
        pass

    def focus_back(self, side):
        pass
