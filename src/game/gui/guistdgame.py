from game.config import Config
from game.gamestat.gameplayerspec import GamePlayerSpec
from game.gamestat.gamespec import GameSpec
from game.gui.guipromptbox import GuiPromptBox
from game.gui.guistdgameplayer import GuiStdGamePlayer
from game.interface.igui import IGui
from game.interface.iworker import IWorker
from game.window.button import Button
from game.window.checkbox import CheckBox
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position
from game.window.selectbox import SelectBox
from game.window.textbox import TextBox
from game.window.windowelement import WindowElement


class GuiStdGame:

    _panel = None
    _lab_tit = None
    _sba = None
    _sbb = None
    _seed = None
    _blank = None
    _bt_a = None
    _bt_b = None
    _sel_fog = None
    _ch_quickprogress = None
    _ch_quickbuild = None
    _ch_quickmine = None
    _area = None
    _is_multi = False
    _players = None

    def __init__(self, is_multi):
        self._is_multi = is_multi
        self._players = []

        self._panel = Panel(0, 0, 600, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 50, "New server game" if is_multi else "New standard game")
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._sba = SelectBox(10, 10, 300)
        self._sba.init(self._panel)
        self._sba.set_caption("Player count: ", 100)

        for i in range(2, 5):
            self._sba.item_add(str(i), str(i) + " players")

        @self._sba.event_change(self)
        def _(this):
            self.update_players()

        self._sbb = SelectBox(10, 0, 300)
        self._sbb.init(self._panel)
        self._sbb.set_caption("Map size: ", 100)
        self._sbb.item_add("52x52", "50 x 50")
        self._sbb.item_add("67x67", "65 x 65")
        self._sbb.item_add("102x102", "100 x 100")

        self._sel_fog = SelectBox(10, 10, 300)
        self._sel_fog.init(self._panel)
        self._sel_fog.set_caption("Fog: ", 100)
        self._sel_fog.item_set_dict(Config.get_fog_type_values(capt=True))

        self._blank = CheckBox(10, 0, 300, "Blank map area")
        self._blank.init(self._panel)

        self._seed = TextBox(10, 0, 200)
        self._seed.init(self._panel)
        self._seed.set_caption("Seed for generated map: ", 150)
        self._seed.set_value("10")
        self._seed.set_numeric(True)

        spc = WindowElement(0, 0, 20, 20)
        spc.init(self._panel)

        self._area = WindowElement(0, 0, 580, 0)
        self._area.init(self._panel)

        spc = WindowElement(0, 0, 20, 20)
        spc.init(self._panel)

        self._ch_quickprogress = CheckBox(10, 10, 380, "Quick progress")
        self._ch_quickprogress.init(self._panel)

        self._ch_quickbuild = CheckBox(10, 10, 380, "Quick build")
        self._ch_quickbuild.init(self._panel)

        self._ch_quickmine = CheckBox(10, 10, 380, "Quick harvest")
        self._ch_quickmine.init(self._panel)


        btp = WindowElement(0, 0, 580, 40)
        btp.init(self._panel)

        self._bt_a = Button(0, 0, 150, 40, "Back")
        self._bt_a.set_image_name("gui_back")
        self._bt_a.init(btp)

        @self._bt_a.event_click(self)
        def _(this):
            if self._is_multi:
                Config.get().set_game_spec_multi(this.get_game_spec())
                Config.get().save()
                self.destroy()

            else:
                IGui.get().create_menu("menu")
                Config.get().set_game_spec_single(this.get_game_spec())
                Config.get().save()

        self._bt_b = Button(0, 0, 150, 40, "Create game" if self._is_multi else "Start game")
        self._bt_b.set_image_name("gui_online" if self._is_multi else "gui_game")
        self._bt_b.init(btp)

        @self._bt_b.event_click(self)
        def _(this):
            if self._is_multi:
                spec = this.get_game_spec()

                Config.get().set_game_spec_multi(spec)
                Config.get().save()
                IWorker.get().game_start(spec)
                this.destroy()

            else:
                pb = GuiPromptBox("Standard game", "Ready to start standard game?", "No", "Yes, start")

                @pb.event_ok(this)
                def _(this):
                    Config.get().set_game_spec_single(this.get_game_spec())
                    Config.get().save()
                    this.destroy()
                    IWorker.get().game_start(this.get_game_spec())

        btp.get_pos().set_width(btp.organize_x(10))
        btp.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_TOP)

        self.organize()
        self._panel.raise_to_top(modal=False)

        if self._is_multi:
            gspec = Config.get().get_game_spec_multi()
        else:
            gspec = Config.get().get_game_spec_single()

        if gspec is None:
            self.update_players()
        else:
            self.set_game_spec(gspec)

    def update_players(self):
        pcnt = int(self._sba.get_selected_key())

        while len(self._players) < pcnt:
            self.add_player()

        while len(self._players) > pcnt:
            idx = len(self._players) - 1
            self._players[idx].destroy()
            del self._players[idx]

        self.organize()

    def fix_players(self, exc):
        [pl.fix() for pl in self._players if pl is not exc]

    def get_players(self):
        return self._players

    def get_is_multi(self):
        return self._is_multi

    def add_player(self):
        tt = GuiStdGamePlayer(self, len(self._players) + 1)
        tt.init(self._area)

        self._players.append(tt)

    def organize(self):
        self._area.get_pos().set_height(self._area.organize_y(10))
        self._area.pos_update()

        self._panel.get_pos().set_height(self._panel.organize_y(10))
        self._panel.pos_update()
        self._panel.pos_update()

    def get_game_spec(self):
        spec = GameSpec()

        if self._is_multi:
            spec.set_game_type(GameSpec.TYPE_ONLINE)
            spec.set_server_game(True)
        else:
            spec.set_game_type(GameSpec.TYPE_NORMAL)

        c = Config.get().get_fog_type_values(capt=False)[self._sel_fog.get_selected_key()]
        spec.set_fog_type(c)

        spec.set_quick_progress(self._ch_quickprogress.get_checked())
        spec.set_quick_build(self._ch_quickbuild.get_checked())
        spec.set_quick_mine(self._ch_quickmine.get_checked())

        spec.get_map().set_blank(self._blank.get_checked())
        spec.get_map().set_seed(self._seed.get_value())

        sz = self._sbb.get_selected_key().split('x')
        spec.get_map().set_map_size(int(sz[0]), int(sz[1]))

        for pl in self._players:
            spec.add_player(GamePlayerSpec(pl.get_side_type(), pl.get_ai(), pl.get_money(), pl.get_color()))

        return spec

    def set_game_spec(self, spec):
        self._blank.set_checked(spec.get_map().get_blank())
        self._seed.set_value(spec.get_map().get_seed())

        self._sel_fog.set_selected_key(Config.get().get_fog_type_values(capt=False, find_value=spec.get_fog_type()))

        self._ch_quickprogress.set_checked(spec.get_quick_progress())
        self._ch_quickbuild.set_checked(spec.get_quick_build())
        self._ch_quickmine.set_checked(spec.get_quick_mine())

        sz = str(spec.get_map().get_map_width()) + 'x' + str(spec.get_map().get_map_height())
        self._sbb.set_selected_key(sz)

        while len(self._players) > 0:
            self._players[0].destroy()
            del self._players[0]

        self._sba.set_selected_key(str(len(spec.get_players())))

        for pl in spec.get_players():
            tt = GuiStdGamePlayer(self, len(self._players) + 1)
            tt.init(self._area)

            self._players.append(tt)

            tt.set_side_type(pl.get_side_type())
            tt.set_ai(pl.get_ai())
            tt.set_color(pl.get_color())
            tt.set_money(pl.get_money())

        self.organize()

    def destroy(self):
        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
