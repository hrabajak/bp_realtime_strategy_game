from game.window.button import Button
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position
from game.window.textbox import TextBox


class GuiChatBox:

    _title = None
    _panel = None
    _lab_tit = None
    _txt = None
    _bt_cancel = None
    _bt_ok = None

    glob_ref = None

    _event_cancel: (any, any) = lambda self, data: None
    _event_cancel_data = None
    _event_ok: (any, any, str) = lambda self, data, message: None
    _event_ok_data = None

    def __init__(self, title, caption_cancel="Cancel", caption_ok="OK"):
        if GuiChatBox.glob_ref is not None:
            GuiChatBox.glob_ref.destroy()

        GuiChatBox.glob_ref = self

        self._title = title

        self._panel = Panel(0, 0, 450, 200, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 30, self._title)
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._txt = TextBox(10, 50, 430)
        self._txt.init(self._panel)
        self._txt.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._bt_cancel = Button(120, 10, 100, 40, caption_cancel)
        self._bt_cancel.init(self._panel)
        self._bt_cancel.set_image_name("gui_cancel")
        self._bt_cancel.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

        @self._bt_cancel.event_click(self)
        def _f(this):
            this._event_cancel(this._event_cancel_data)
            this.destroy()

        self._bt_ok = Button(10, 10, 100, 40, caption_ok)
        self._bt_ok.init(self._panel)
        self._bt_ok.set_image_name("gui_ok")
        self._bt_ok.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

        @self._bt_ok.event_click(self)
        def _f(this):
            this._event_ok(this._event_ok_data, this._txt.get_value())
            this.destroy()

        self._panel.raise_to_top(modal=True)
        self._panel.get_mng().focus_element(self._txt)

    def get_message(self):
        return self._txt.get_value()

    def event_cancel(self, arg):
        def decorator(func):
            self._event_cancel = func
            self._event_cancel_data = arg

        return decorator

    def event_ok(self, arg):
        def decorator(func):
            self._event_ok = func
            self._event_ok_data = arg

        return decorator

    def destroy(self):
        self._panel.destroy()
        GuiChatBox.glob_ref = None
