from game.window.button import Button
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position


class GuiPromptBox:

    _title = None
    _message = None
    _panel = None
    _lab_tit = None
    _lab = None
    _bt_cancel = None
    _bt_ok = None

    _glob_ref = None

    _event_cancel: (any, any) = lambda self, data: None
    _event_cancel_data = None
    _event_ok: (any, any) = lambda self, data: None
    _event_ok_data = None

    def __init__(self, title, message, caption_cancel="Cancel", caption_ok="OK"):
        if GuiPromptBox._glob_ref is not None:
            GuiPromptBox._glob_ref.destroy()

        GuiPromptBox._glob_ref = self

        self._title = title
        self._message = message

        self._panel = Panel(0, 0, 350, 200, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 30, self._title)
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._lab = Label(10, 50, 330, 25, self._message)
        self._lab.init(self._panel)
        self._lab.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._bt_cancel = Button(160, 10, 140, 40, caption_cancel)
        self._bt_cancel.init(self._panel)
        self._bt_cancel.set_image_name("gui_cancel")
        self._bt_cancel.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

        @self._bt_cancel.event_click(self)
        def _f(this):
            this._event_cancel(this._event_cancel_data)
            this.destroy()

        self._bt_ok = Button(10, 10, 140, 40, caption_ok)
        self._bt_ok.init(self._panel)
        self._bt_ok.set_image_name("gui_ok")
        self._bt_ok.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

        @self._bt_ok.event_click(self)
        def _f(this):
            this._event_ok(this._event_ok_data)
            this.destroy()

        self._panel.raise_to_top(modal=True)

    def event_cancel(self, arg):
        def decorator(func):
            self._event_cancel = func
            self._event_cancel_data = arg
        return decorator

    def event_ok(self, arg):
        def decorator(func):
            self._event_ok = func
            self._event_ok_data = arg

        return decorator

    def destroy(self):
        self._panel.destroy()
        GuiPromptBox._glob_ref = None
