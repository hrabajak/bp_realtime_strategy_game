from game.window.button import Button
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position


class GuiMessageBox:

    _title = None
    _message = None
    _panel = None
    _lab_tit = None
    _lab = None
    _bt = None

    _glob_ref = None

    _event_ok: (any, any) = lambda self, data: None
    _event_ok_data = None

    def __init__(self, title, message):
        if GuiMessageBox._glob_ref is not None:
            GuiMessageBox._glob_ref.destroy()

        GuiMessageBox._glob_ref = self

        self._title = title
        self._message = message

        self._panel = Panel(0, 0, 350, 200, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 30, self._title)
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._lab = Label(10, 50, 330, 25, self._message)
        self._lab.init(self._panel)
        self._lab.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._bt = Button(10, 10, 140, 40, "OK")
        self._bt.set_image_name("gui_ok")
        self._bt.init(self._panel)
        self._bt.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

        @self._bt.event_click(self)
        def _(this):
            this._event_ok(this._event_ok_data)
            this.destroy()

        self._panel.raise_to_top(modal=True)

    def event_ok(self, arg):
        def decorator(func):
            self._event_ok = func
            self._event_ok_data = arg

        return decorator

    def destroy(self):
        self._panel.destroy()
        GuiMessageBox._glob_ref = None
