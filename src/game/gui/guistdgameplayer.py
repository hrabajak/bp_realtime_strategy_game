from game.image.imagecollector import ImageCollector
from game.window.label import Label
from game.window.position import Position
from game.window.selectbox import SelectBox
from game.window.windowelement import WindowElement


class GuiStdGamePlayer:

    _pmenu = None
    _number = 0
    _panel = None
    _label = None
    _sba = None
    _sbb = None
    _sbc = None
    _sbd = None

    def __init__(self, pmenu, number):
        self._pmenu = pmenu
        self._number = number

    def init(self, parent):
        ais = set()
        colors = set()
        bef_side_type = None

        for pl in self._pmenu.get_players():
            ais.add(pl.get_ai())
            colors.add(pl.get_color())
            bef_side_type = pl.get_side_type()

        self._panel = WindowElement(0, 0, 580, 30)
        self._panel.init(parent)

        self._label = Label(0, 0, 20, 30, str(self._number) + ") ")
        self._label.init(self._panel)
        self._label.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._sba = SelectBox(0, 0, 130)
        self._sba.init(self._panel)
        self._sba.item_add("s1", "first type")
        self._sba.item_add("s2", "second type")

        if "s1" == bef_side_type:
            self._sba.set_selected_key("s2")
        else:
            self._sba.set_selected_key("s1")

        self._sbb = SelectBox(0, 0, 110)
        self._sbb.init(self._panel)
        self._sbb.item_add("human", "human")
        self._sbb.item_add("ai_easy", "AI easy")
        self._sbb.item_add("ai_medium", "AI medium")
        self._sbb.item_add("ai", "AI hard")
        
        if self._pmenu.get_is_multi():
            self._sbb.set_selected_key("human")
        elif "human" in ais:
            self._sbb.set_selected_key("ai")
        else:
            self._sbb.set_selected_key("human")

        self._sbc = SelectBox(0, 0, 120)
        self._sbc.init(self._panel)

        for color in ImageCollector().color_sorted:
            self._sbc.item_add(color.get_key(), color.get_name())

        for color in ImageCollector().color_sorted:
            if color.get_key() not in colors:
                self._sbc.set_selected_key(color.get_key())
                break

        @self._sbc.event_change(self)
        def _(this):
            self._pmenu.fix_players(self)

        self._sbd = SelectBox(0, 0, 120)
        self._sbd.init(self._panel)

        for mn in [2000, 5000, 10000, 15000, 30000, 100000]:
            self._sbd.item_add(str(mn), f"{mn:,}")

        self._panel.organize_x(10)
        self._panel.pos_update()

    def get_side_type(self):
        return self._sba.get_selected_key()

    def get_ai(self):
        return self._sbb.get_selected_key()

    def get_color(self):
        return self._sbc.get_selected_key()

    def get_money(self):
        try:
            i_val = int(self._sbd.get_selected_key())
        except ValueError:
            i_val = 0

        return i_val

    def set_side_type(self, side_type):
        self._sba.set_selected_key(side_type)

    def set_ai(self, ai):
        self._sbb.set_selected_key(ai)

    def set_color(self, color):
        self._sbc.set_selected_key(color)

    def set_money(self, money):
        self._sbd.set_selected_key(str(money))

    def fix(self):
        colors = set()
        [colors.add(pl.get_color()) for pl in self._pmenu.get_players() if pl is not self]

        if self._sbc.get_selected_key() in colors:
            for color in ImageCollector().color_sorted:
                if color.get_key() not in colors:
                    self._sbc.set_selected_key(color.get_key())
                    break

    def destroy(self):
        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
            self._label = None
            self._sba = None
            self._sbb = None
            self._sbc = None
            self._sbd = None
