from game.config import Config
from game.interface.igui import IGui
from game.interface.iview import IView
from game.window.button import Button
from game.window.checkbox import CheckBox
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position
from game.window.selectbox import SelectBox
from game.window.textbox import TextBox
from game.window.windowelement import WindowElement


class GuiSettings:

    _panel = None
    _lab_tit = None
    _sba = None
    _sbc = None
    _chkb = None
    _chkc = None
    _chke = None
    _srv = None
    _lst = None
    _bt_a = None

    def __init__(self):
        self._panel = Panel(0, 0, 400, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)
        self._panel.pos_update()

        self._lab_tit = Label(10, 10, 330, 50, "Settings")
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._sba = SelectBox(10, 10, 380)
        self._sba.init(self._panel)
        self._sba.set_caption("Resolution: ", 100)
        self._sba.item_set_dict(Config.get_resolution_values(capt=True))

        @self._sba.event_change(self)
        def _(this):
            c = Config.get().get_resolution_values(capt=False)[this._sba.get_selected_key()]
            Config.get().set_resolution(c)
            Config.get().save()
            IView.get().wnd_update()

        self._sbc = SelectBox(10, 10, 380)
        self._sbc.init(self._panel)
        self._sbc.set_caption("Sound volume: ", 100)

        for i in range(0, 21):
            self._sbc.item_add(str(i), str(int((i / 20.0) * 100)) + ' %')

        @self._sbc.event_change(self)
        def _(this):
            Config.get().set_master_volume(int(this._sbc.get_selected_key()) / 20)
            Config.get().save()

        spc = WindowElement(0, 0, 20, 20)
        spc.init(self._panel)

        self._chke = CheckBox(10, 10, 380, "Allow cheats")
        self._chke.init(self._panel)

        @self._chke.event_change(self)
        def _(this):
            Config.get().set_allow_cheats(this._chke.get_checked())
            Config.get().save()

        self._chkb = CheckBox(10, 10, 380, "Fullscreen")
        self._chkb.init(self._panel)

        @self._chkb.event_change(self)
        def _(this):
            Config.get().set_fullscreen(this._chkb.get_checked())
            Config.get().save()
            IView.get().wnd_update()

        self._chkc = CheckBox(10, 10, 380, "V-sync")
        self._chkc.init(self._panel)

        @self._chkc.event_change(self)
        def _(this):
            Config.get().set_vsync(this._chkc.get_checked())
            Config.get().save()
            IView.get().wnd_update()

        spc = WindowElement(0, 0, 20, 20)
        spc.init(self._panel)

        self._srv = TextBox(10, 10, 380)
        self._srv.set_caption("Server IP: ", 150)
        self._srv.init(self._panel)

        @self._srv.event_change(self)
        def _(this):
            Config.get().set_connect_host(this._srv.get_value())
            Config.get().save()

        self._lst = TextBox(10, 10, 380)
        self._lst.set_caption("Listen IP (for server): ", 150)
        self._lst.init(self._panel)

        @self._lst.event_change(self)
        def _(this):
            Config.get().set_listen_host(this._lst.get_value())
            Config.get().save()

        spc = WindowElement(0, 0, 20, 20)
        spc.init(self._panel)

        self._bt_a = Button(10, 10, 380, 40, "Back")
        self._bt_a.set_image_name("gui_back")
        self._bt_a.init(self._panel)

        @self._bt_a.event_click(self)
        def _(this):
            IGui.get().create_menu("menu")

        self._panel.get_pos().set_height(self._panel.organize_y(10))
        self._panel.pos_update()

        self.update_from_conf()

    def update_from_conf(self):
        self._sba.set_selected_key(Config.get().get_resolution_values(capt=False, find_value=Config.get().get_resolution()))
        self._sbc.set_selected_key(str(int(Config.get().get_master_volume() * 20)))

        self._chke.set_checked(Config.get().get_allow_cheats())
        self._chkb.set_checked(Config.get().get_fullscreen())
        self._chkc.set_checked(Config.get().get_vsync())

        self._srv.set_value(Config.get().get_connect_host())
        self._lst.set_value(Config.get().get_listen_host())

    def destroy(self):
        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
