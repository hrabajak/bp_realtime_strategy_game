import time
from collections import OrderedDict
import pyglet

from game.action.actionitemspec import ActionItemSpec
from game.build.buildplace import BuildPlace
from game.build.buildselect import BuildSelect
from game.build.buildspec import BuildSpec
from game.gui.guiabout import GuiAbout
from game.gui.guimodalmessage import GuiModalMessage
from game.gui.guimenu import GuiMenu
from game.gui.guimenumulti import GuiMenuMulti
from game.gui.guipromptbox import GuiPromptBox
from game.gui.guisettings import GuiSettings
from game.gui.guispecview import GuiSpecView
from game.gui.guistdgame import GuiStdGame
from game.helper.singleton import Singleton
from game.interface.igui import IGui
from game.interface.iselectable import ISelectable
from game.interface.iview import IView
from game.sound.soundcollector import SoundCollector
from game.window.chatpanel import ChatPanel
from game.window.imagepanel import ImagePanel
from game.window.label import Label
from game.window.button import Button
from game.window.buttonaction import ButtonAction
from game.window.panelmap import PanelMap
from game.window.position import Position


class GuiBase(IGui, metaclass=Singleton):

    STATE_NONE = 0
    STATE_FOCUS_MOVEABLES = 1
    STATE_FOCUS_BUILDS = 2
    STATE_TARGET = 3
    STATE_BUILDING = 4
    STATE_UPGRADE = 5

    _state = None
    _side = None
    _gui_group = None
    _tick = 0

    _lab = None
    _lab_money = None
    _lab_notify = None
    _logo = None
    _notify_tick = 0
    _chatbox = None
    _buttons = None
    _action_buttons = None
    _action_hash = None
    _modal_msg_ref = None
    _message_ref = None
    _map_box = None
    _map_tick = 50
    _menu_ref = None
    _specview_ref = None

    _action_ref = None

    _focus_side = None
    _focus_moveables = None
    _focus_builds = None
    _focus_target_type = None

    _build_place: BuildPlace = None
    _build_select: BuildSelect = None

    def __init__(self):
        IGui.__init__(self)
        IGui.set(self)

        self._state = GuiBase.STATE_NONE
        self._gui_group = pyglet.graphics.Group()
        self._buttons = []
        self._action_buttons = []
        self._action_hash = OrderedDict()
        self._modal_msg_ref = None
        self._msg_connect_ref = None
        self._menu_ref = None

    def get_state(self):
        return self._state

    def get_focus_target_type(self):
        return self._focus_target_type

    def set_side(self, side):
        self._side = side
        self.init()

    def get_side(self):
        return self._side

    def create_modal_message(self, caption):
        self.drop_modal_messages()
        self._modal_msg_ref = GuiModalMessage(caption)
        time.sleep(0.1)

    def drop_modal_messages(self):
        if self._modal_msg_ref is not None:
            self._modal_msg_ref.destroy()
            self._modal_msg_ref = None

    def chat_message(self, side, message):
        message = message.strip()

        if len(message) > 0:
            self._chatbox.add_message(side.get_color().get_map_color(), side.get_color().get_name() + ": " + message)
            SoundCollector().play_sound("message", 0, 0, 1.0, use_coords=False)

    def drop_menu(self):
        self.drop_modal_messages()

        if self._logo is not None:
            self._logo.destroy()
            self._logo = None

        if self._menu_ref is not None:
            self._menu_ref.destroy()
            self._menu_ref = None

    def create_menu(self, tp):
        self.drop_menu()

        if self._logo is None:
            self._logo = ImagePanel(155, 75, 250, 90, "cvutfit")
            self._logo.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_BOTTOM)
            self._logo.init()

        if tp == "menu":
            self._menu_ref = GuiMenu()
        elif tp == "stdgame":
            self._menu_ref = GuiStdGame(is_multi=False)
        elif tp == "menumulti":
            self._menu_ref = GuiMenuMulti()
        elif tp == "settings":
            self._menu_ref = GuiSettings()
        elif tp == "about":
            self._menu_ref = GuiAbout()

    def init(self):
        if self._chatbox is None:
            self._chatbox = ChatPanel(20, 60, 300, 50)
            self._chatbox.set_disabled(True)
            self._chatbox.init()
            self._chatbox.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        if self._side is None:
            if self._lab is not None:
                self._lab.destroy()

            if self._lab_money is not None:
                self._lab_money.destroy()

            if self._map_box is not None:
                self._map_box.destroy()

            if self._lab_notify is not None:
                self._lab_notify.destroy()

            self._lab = None
            self._lab_money = None
            self._lab_notify = None
            self._map_box = None

        else:
            if self._lab is None:
                self._lab = Label(15, 75, 1000, 25, "", text_align="right")
                self._lab.set_disabled(True)
                self._lab.init()
                self._lab.set_align(Position.H_ALIGN_RIGHT)
                self._lab.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

            if self._lab_money is None:
                self._lab_money = Label(15, 15, 150, 25, "")
                self._lab_money.set_disabled(True)
                self._lab_money.init()
                self._lab_money.set_font_size(20)
                self._lab_money.set_align(Position.H_ALIGN_RIGHT)
                self._lab_money.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_TOP)

            if self._map_box is None:
                self._map_box = PanelMap(20, 20, 200, 200)
                self._map_box.set_visible(False)
                self._map_box.init()
                self._map_box.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_BOTTOM)

                @self._map_box.event_point(self)
                def _(this, mx, my):
                    IView.get().sc_to(mx, my)

        self.focus_remove()

    def create_notify(self, message):
        if self._lab_notify is not None:
            self._lab_notify.destroy()
            self._lab_notify = None

        self._lab_notify = Label(0, 0, 500, 25, message)
        self._lab_notify.set_disabled(True)
        self._lab_notify.init()
        self._lab_notify.set_font_size(20)
        self._lab_notify.set_align(Position.H_ALIGN_CENTER)
        self._lab_notify.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)
        self._notify_tick = 250

    def action_item_notify(self, action):
        if action.get_side() is self._side:
            if action.get_spec().get_build_spec() is not None:
                self.create_notify("Building " + action.get_spec().get_build_spec().get_name() + " is ready")
                SoundCollector().play_sound("ready", 0, 0, 1.0, use_coords=False)

    def focus_remove(self):
        self._state = GuiBase.STATE_NONE
        self._focus_target_type = None
        self._action_ref = None
        self.focus_unit_spec(None)

        if self._specview_ref is not None:
            self._specview_ref.destroy()
            self._specview_ref = None

        if self._lab is not None:
            self._lab.set_text("")

        if self._build_place is not None:
            self._build_place.unset()
            self._build_place = None

        if self._build_select is not None:
            self._build_select.unset()
            self._build_select = None

        self._remove_buttons()
        self._update_actions()

    def focus(self, side):
        if side.get_is_any_selected(ISelectable.SELECTED_MOVEABLE):
            self.focus_moveables(side, side.get_selected_moveables())

        elif side.get_is_any_selected(ISelectable.SELECTED_BUILD):
            self.focus_builds(side, side.get_selected_builds())

    def focus_moveables(self, side, moveables):
        self.focus_remove()
        self._state = GuiBase.STATE_FOCUS_MOVEABLES
        self._focus_side = side
        self._focus_moveables = moveables

        hash = {}

        for mm in moveables:
            if mm.get_spec().get_name() in hash:
                hash[mm.get_spec().get_name()] += 1
            else:
                hash[mm.get_spec().get_name()] = 1

        text = "Units selected: "

        for name in hash.keys():
            text += name + " (" + str(hash[name]) + "x) "

        self._lab.set_text(text)

        self._remove_buttons()
        self._add_button("Cancel", {"side": side}, self._click_cancel, "gui_cancel")
        self._add_button("Stop (S)", {"side": side}, self._click_stop, "gui_acc_stop")
        self._add_button("Hold (H)", {"side": side}, self._click_hold, "gui_acc_hold")
        # !! self._add_button("Attack (S)", {"side": side}, self._click_attack)
        self._add_button("Go (G)", {"side": side}, self._click_go, "gui_acc_go")
        self._add_button("Attack (A)", {"side": side}, self._click_attack_move, "gui_acc_attmove")

    def focus_moveables_back(self):
        if self._focus_side is not None:
            self.focus_moveables(self._focus_side, [m for m in self._focus_moveables if m.get_is_alive()])

    def focus_builds(self, side, builds):
        self.focus_remove()
        self._state = GuiBase.STATE_FOCUS_BUILDS
        self._focus_side = side
        self._focus_builds = builds

        hash = {}

        for mm in builds:
            if mm.get_spec().get_name() in hash:
                hash[mm.get_spec().get_name()] += 1
            else:
                hash[mm.get_spec().get_name()] = 1

        text = "Buildings selected: "

        for name in hash.keys():
            text += name + " (" + str(hash[name]) + "x) "

        self._lab.set_text(text)

        self._remove_buttons()

        if len(builds) == 1 and builds[0].get_spec().get_allow_destroy():
            self._add_button("Destroy", {"side": side, "build_ref": builds[0]}, self._click_destroy, "gui_destroy")

        spcs = OrderedDict()

        for bref in builds:
            if not bref.get_spec().number in spcs:
                spcs[bref.get_spec().number] = {
                    'spec': bref.get_spec(),
                    'refs': []
                }

            spcs[bref.get_spec().number]['refs'].append(bref)

        for number in spcs.keys():
            spec = spcs[number]['spec']
            refs = spcs[number]['refs']

            for key in spec.get_action_keys():
                ac = ActionItemSpec.get(key)

                if next((True for bref in builds if bref.get_may_apply_action(ac)), False) and (len(builds) == 1 or ac.get_upgrade_keys() is None):
                    btn = self._add_button(ac.get_name() + "\n" + str(ac.get_money_cost()) + " $", {"side": side, "action": ac, "build_refs": refs}, self._click_action, ac.get_image_name())

                    @btn.event_hover((self, ac))
                    def _(c, flag):
                        if flag:
                            this, ac = c[0], c[1]

                            if ac.get_unit_spec() is not None:
                                this.focus_unit_spec(ac.get_unit_spec())

        self._add_button("Cancel", {"side": side}, self._click_cancel, "gui_cancel")

    def focus_builds_back(self):
        if self._focus_side is not None:
            self.focus_builds(self._focus_side, [b for b in self._focus_builds if b.get_is_alive()])

    def focus_back(self, side):
        if self._side is side:
            if self._state == GuiBase.STATE_FOCUS_BUILDS:
                self.focus_builds_back()
            elif self._state == GuiBase.STATE_FOCUS_MOVEABLES:
                self.focus_moveables_back()

    def focus_unit_spec(self, unit_spec):
        if self._specview_ref is not None:
            self._specview_ref.destroy()
            self._specview_ref = None

        if unit_spec is not None:
            self._specview_ref = GuiSpecView(unit_spec, False)

    def focus_target(self, target_type):
        self.focus_remove()
        self._state = GuiBase.STATE_TARGET
        self._focus_target_type = target_type

        if target_type == "go":
            self._lab.set_text("Select target to move")
        elif target_type == "attack":
            self._lab.set_text("Select target to attack")
        elif target_type == "attack_move":
            self._lab.set_text("Select destination to attack move")

        self._remove_buttons()
        self._add_button("Back", {"side": self._focus_side, "moveables": self._focus_moveables}, self._click_back_to_focus)

    def focus_building(self, spec: BuildSpec, ac=None):
        self.focus_remove()
        self._state = GuiBase.STATE_BUILDING
        self._build_place = BuildPlace(self._side, spec)
        self._action_ref = ac
        self.create_notify("Select building location ...")

        self._lab.set_text("Select building location")

        self._remove_buttons()
        self._add_button("Cancel", {}, self._click_cancel_build, "gui_cancel")
        self._add_button("Refund", {"side": self._side, "ac": ac}, self._click_refund)

    def focus_upgrade(self, side, build_src_ref, ac_spec):
        self.focus_remove()
        self._state = GuiBase.STATE_UPGRADE
        self._build_select = BuildSelect(self._side, ac_spec, build_src_ref)
        self.create_notify("Select building to upgrade ...")

        self._lab.set_text("Select building to upgrade")

        self._remove_buttons()
        self._add_button("Cancel", {}, self._click_cancel_build, "gui_cancel")

    def building_set_tile(self, tile):
        if self._build_place is not None:
            self._build_place.set_tile(tile)
            self._build_place.higlight()

        elif self._build_select is not None:
            self._build_select.set_tile(tile)
            self._build_select.higlight()

    def building_finish(self):
        if self._state == GuiBase.STATE_BUILDING:

            if self._build_place.get_is_buildable():
                if self._action_ref is not None:
                    self._side.action_build_realize(self._action_ref, self._build_place)
                    # self._side.get_actions().action_handle(self._action_ref, {"tile": self._build_place.get_tile()})
                    self._action_ref = None
                    SoundCollector().play_sound("place", 0, 0, 1.0, use_coords=False)

                self.focus_builds_back()

                return True
            else:
                self.create_notify("Cannot PLACE building here!")
                SoundCollector().play_sound("error", 0, 0, 1.0, use_coords=False)
                return False

        elif self._state == GuiBase.STATE_UPGRADE:

            if self._build_select.get_is_valid():
                self._build_select.get_side().action_enqueue(self._build_select.get_ac_spec(), self._build_select.get_build_src_ref(), self._build_select.get_build_ref())
                self.focus_builds_back()
                SoundCollector().play_sound("add", 0, 0, 1.0, use_coords=False)

                return True
            else:
                self.create_notify("Cannot UPGRADE this building!")
                SoundCollector().play_sound("error", 0, 0, 1.0, use_coords=False)
                return False

        else:
            return False

    def _remove_buttons(self):
        for btn in self._buttons:
            btn.destroy()

        self._buttons = []

    def _add_button(self, caption, data, callback, image_name=None):
        btn = Button(15 + len(self._buttons) * (140 + 1), 15, 140, 45, caption)
        btn.init()
        btn.set_event_click(callback, data)
        btn.get_pos().set_align(Position.H_ALIGN_RIGHT, Position.V_ALIGN_BOTTOM)

        if image_name is not None:
            btn.set_image_name(image_name)

        self._buttons.append(btn)

        return btn

    def _update_actions(self):
        if self._side is None:
            for gac in self._action_buttons:
                gac.destroy()

            self._action_buttons.clear()
            self._action_hash.clear()

        else:
            self._lab_money.set_text(str(self._side.get_money()) + ' $')

            for ac in [c for c in self._side.get_actions().get_action_queue()]:
                if ac.get_hash_key() not in self._action_hash:
                    gac = ButtonAction(15 + len(self._action_buttons) * (160 + 2), 15, 160, 45, ac.get_name(), ac)
                    gac.init()
                    gac.set_event_click(self._click_action_realize, {"side": self._side, "action_item": ac, "hash_key": ac.get_hash_key()})
                    gac.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)
                    gac.set_image_name(ac.get_spec().get_image_name())

                    self._action_buttons.append(gac)
                    self._action_hash[ac.get_hash_key()] = gac

                else:
                    self._action_hash[ac.get_hash_key()].add_action_item(ac)

            rem = []
            idx = 0

            for gac in self._action_buttons:
                if gac.check_action_queue(self._side.get_actions()):
                    gac.get_pos().set_x(15 + idx * (160 + 2))
                    idx += 1
                else:
                    rem.append(gac)

            for gac in rem:
                self._action_buttons.remove(gac)
                del self._action_hash[gac.get_hash_key()]
                gac.destroy()

            for gac in self._action_buttons:
                gac.update()

    def logic_update(self, tick):
        if tick % 50 == 0 and self._chatbox is not None:
            self._chatbox.tickstep()

    def scroll_update(self):
        self._map_tick = 20

    def update(self):
        if self._tick % 10 == 0:
            self._update_actions()

        if self._tick % self._map_tick == 0 and self._map_box is not None:
            self._map_box.set_visible(True)
            self._map_box.map_update(IView.get())
            self._map_tick = 50

        if self._notify_tick > 0 and self._lab_notify is not None:
            self._notify_tick -= 1

            if self._notify_tick == 0:
                self._lab_notify.destroy()
                self._lab_notify = None

            elif self._notify_tick < 100:
                self._lab_notify.set_opacity(int((self._notify_tick / 100.0) * 255))

        self._tick += 1

    def _click_go(self, data):
        self.focus_target("go")

    def _click_attack(self, data):
        self.focus_target("attack")

    def _click_attack_move(self, data):
        self.focus_target("attack_move")

    def _click_stop(self, data):
        data["side"].selected_stop()

    def _click_hold(self, data):
        data["side"].selected_hold()

    def _click_cancel(self, data):
        if "side" in data:
            data["side"].clear_selected()
        self.focus_remove()

    def _click_cancel_build(self, data):
        self.focus_builds_back()

    def _click_refund(self, data):
        data["side"].action_refund(data["ac"])
        self.focus_remove()

    def _click_back_to_focus(self, data):
        self.focus_moveables_back()

    def _click_destroy(self, data):
        build_ref = data["build_ref"]
        pb = GuiPromptBox("Destroy", "Do you really want to destroy the " + build_ref.get_spec().get_name() + " building?", "No", "Yes, destroy")

        @pb.event_ok(self)
        def _(this):
            side = data["side"]
            side.destroy_build(data["build_ref"])
            this.focus_remove()

    def _click_action(self, data):
        side = data["side"]
        build_refs = data["build_refs"]
        ac = data["action"]

        if ac.get_upgrade_keys() is not None:
            self.focus_upgrade(side, build_refs[0], ac)

        else:
            for build_ref in build_refs:
                if ac.get_build_spec() is not None:
                    if side.get_actions().get_build_stacked_count(build_ref) == 0:
                        side.action_enqueue(ac, build_ref)
                        SoundCollector().play_sound("add", 0, 0, 1.0, use_coords=False)
                    else:
                        self.create_notify("Main building allready producing!")
                        SoundCollector().play_sound("error", 0, 0, 1.0, use_coords=False)
                else:
                    side.action_enqueue(ac, build_ref)
                    SoundCollector().play_sound("add", 0, 0, 1.0, use_coords=False)

    def _click_action_realize(self, data):
        if data["side"].get_actions().action_trigger(data["action_item"], {"gui": self}):
            pass

        elif data["hash_key"] in self._action_hash:
            data["side"].action_cancel(self._action_hash[data["hash_key"]].get_action_item())
            SoundCollector().play_sound("undo", 0, 0, 1.0, use_coords=False)
