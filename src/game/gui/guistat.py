from game.action.actionitemspec import ActionItemSpec
from game.gui.guispecview import GuiSpecView
from game.moveable.unitspec import UnitSpec
from game.window.button import Button
from game.window.label import Label
from game.window.panel import Panel
from game.window.position import Position
from game.window.windowelement import WindowElement


class GuiStat:

    _panel = None
    _lab_tit = None
    _bt_a = None
    _bt_b = None
    _focus_spec = None
    _view_ref = None
    _items = []

    def __init__(self):
        self._panel = Panel(0, 0, 650, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 50, "")
        self._lab_tit.init(self._panel)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        for spec in UnitSpec.get_all():
            if spec.get_attack_able():
                ac_spec = ActionItemSpec.get(spec.get_key())

                if ac_spec is not None and ac_spec.get_image_name() is not None:
                    box = WindowElement(0, 15, 400, 30)

                    la = Label(0, 0, 140, 30, spec.get_name())
                    la.set_image_name(ac_spec.get_image_name())
                    la.init(box)

                    @la.event_click((self, len(self._items)))
                    def _(c):
                        this, idx = c[0], c[1]
                        this.item_focus(idx)

                    lb = Label(0, 0, 60, 30, str(round(spec.get_damage_per_second(), 2)))
                    lb.init(box)

                    lc = Label(0, 0, 60, 30, str(round(spec.get_damage_till_die(), 2)))
                    lc.init(box)

                    ld = Label(0, 0, 60, 30, str(round(spec.get_damage_per_cost(), 2)))
                    ld.init(box)

                    le = Label(0, 0, 120, 30, "-")
                    le.init(box)

                    lf = Label(0, 0, 120, 30, "-")
                    lf.init(box)

                    box.organize_x(10)
                    box.init(self._panel)

                    self._items.append({
                        'spec': spec,
                        'ac_spec': ac_spec,
                        'box': box,
                        'la': la,
                        'lb': lb,
                        'lc': lc,
                        'ld': ld,
                        'le': le,
                        'lf': lf
                    })

        box = WindowElement(0, 15, 400, 30)
        box.init(self._panel)

        self._bt_a = Button(self._panel.get_pos().get_width() - 10 - 200, 10, 200, 40, "Reset")
        self._bt_a.init(self._panel)

        @self._bt_a.event_click(self)
        def _f(this):
            this.item_focus(None)

        self._bt_b = Button(self._panel.get_pos().get_width() - 10 - 200, 10, 200, 40, "Close")
        self._bt_b.init(self._panel)

        @self._bt_b.event_click(self)
        def _f(this):
            this.destroy()

        self._panel.get_pos().set_height(self._panel.organize_y(5))
        self._panel.pos_update()
        self._panel.raise_to_top()
        self.update_focus()

    def item_focus(self, idx):
        if self._view_ref is not None:
            self._view_ref.destroy()
            self._view_ref = None

        if idx is None:
            self._focus_spec = None
            self.update_focus()

        else:
            it = self._items[idx]
            self._focus_spec = it['spec']
            self.update_focus()
            self._view_ref = GuiSpecView(self._focus_spec, True)

    def update_focus(self):
        if self._focus_spec is None:
            self._lab_tit.set_text("Focus: none")

        else:
            self._lab_tit.set_text("Focus: " + self._focus_spec.get_name() + " [" + self._focus_spec.get_key() + "]")

        for c in self._items:
            spec = c['spec']

            if self._focus_spec is None:
                c['le'].set_text('-')
                c['lf'].set_text('-')
            else:
                c['le'].set_text("A: " + str(round(self._focus_spec.get_endurance(spec), 2)) + " (" + str(round((self._focus_spec.get_bonus_perc(spec) - 1.0) * 100.0)) + " %)")
                c['lf'].set_text("D: " + str(round(spec.get_endurance(self._focus_spec), 2)) + " (" + str(round((spec.get_bonus_perc(self._focus_spec) - 1.0) * -100.0)) + " %)")

    def destroy(self):
        if self._view_ref is not None:
            self._view_ref.destroy()
            self._view_ref = None

        for c in self._items:
            c['box'].destroy()

        self._items.clear()

        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
            self._bt_a = None
            self._bt_b = None
