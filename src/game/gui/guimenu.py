import random

from game.config import Config
from game.gamestat.gameplayerspec import GamePlayerSpec
from game.gamestat.gamespec import GameSpec
from game.gui.guimenumulti import GuiMenuMulti
from game.gui.guimessagebox import GuiMessageBox
from game.gui.guipromptbox import GuiPromptBox
from game.gui.guistat import GuiStat
from game.interface.igui import IGui
from game.interface.iworker import IWorker
from game.window.button import Button
from game.window.label import Label
from game.window.listbox import ListBox
from game.window.panel import Panel
from game.window.position import Position


class GuiMenu:

    _panel = None
    _lab_tit = None
    _bt_a = None
    _bt_b = None
    _bt_c = None
    _bt_d = None
    _bt_e = None
    _bt_f = None
    _list_a = None

    def __init__(self):
        self._panel = Panel(0, 0, 400, 500, 10, 10)
        self._panel.init()
        self._panel.get_pos().set_align(Position.H_ALIGN_CENTER, Position.V_ALIGN_CENTER)

        self._lab_tit = Label(10, 10, 330, 50, "RTS revival")
        self._lab_tit.init(self._panel)
        self._lab_tit.set_font_size(15)
        self._lab_tit.get_pos().set_align(Position.H_ALIGN_LEFT, Position.V_ALIGN_TOP)

        self._bt_a = Button(10, 10, 380, 40, "Standard game")
        self._bt_a.set_image_name("gui_game")
        self._bt_a.init(self._panel)

        @self._bt_a.event_click(self)
        def _f(this):
            IGui.get().create_menu("stdgame")

        # !! self._bt_b = Button(10, 10, 380, 40, "Battle mode")
        # !! self._bt_b.init(self._panel)

        self._bt_c = Button(10, 10, 380, 40, "Online server game")
        self._bt_c.set_image_name("gui_online")
        self._bt_c.init(self._panel)

        @self._bt_c.event_click(self)
        def _(this):
            IWorker.get().event_reset()
            IGui.get().create_menu("menumulti")

        self._bt_d = Button(10, 10, 380, 40, "Settings")
        self._bt_d.set_image_name("gui_settings")
        self._bt_d.init(self._panel)

        @self._bt_d.event_click(self)
        def _(this):
            IGui.get().create_menu("settings")

        self._bt_f = Button(10, 10, 380, 40, "About")
        self._bt_f.set_image_name("gui_info")
        self._bt_f.init(self._panel)

        @self._bt_f.event_click(self)
        def _f(this):
            IGui.get().create_menu("about")

        self._bt_b = Button(10, 10, 380, 40, "Units overview")
        self._bt_b.set_image_name("gui_overview")
        self._bt_b.init(self._panel)

        @self._bt_b.event_click(self)
        def _f(this):
            GuiStat()

        self._bt_e = Button(10, 10, 380, 40, "Quit")
        self._bt_e.set_image_name("gui_cancel")
        self._bt_e.init(self._panel)

        @self._bt_e.event_click(self)
        def _(this):
            pb = GuiPromptBox("Quit game", "Do you want to quit game?", "No", "Yes, quit")

            @pb.event_ok(this)
            def _(this):
                this.destroy()
                IWorker.get().game_quit()

        self._panel.get_pos().set_height(self._panel.organize_y(10))
        self._panel.pos_update()

        IWorker.get().event_reset()
        IWorker.get().event_game_enter = self._event_game_enter
        IWorker.get().event_error = self._event_error
        IWorker.get().game_disconnect()

    def _event_game_enter(self, data):
        self.destroy()

    def _event_error(self, error):
        ms = GuiMessageBox("Error", error)

        @ms.event_ok(self)
        def _(self):
            if IWorker.get().game_clear():
                IGui.get().create_menu("menu")

    def destroy(self):
        if self._panel is not None:
            self._panel.destroy()
            self._panel = None
            self._bt_a = None
            self._bt_b = None
            self._bt_c = None
            self._bt_d = None
            self._bt_e = None
