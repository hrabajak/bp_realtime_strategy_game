import math

from game.helper.discrete import get_angle_from, get_angle_from_rad
from game.helper.discretemath cimport int_sqrt
from game.helper.vector cimport angle_to, angle_dist_side_deg


cdef class DistAngle:

    ZOOM_VALUE = 20
    PLACE_SIZE = 25 * ZOOM_VALUE
    PLACE_SIZE_HALF = PLACE_SIZE // 2

    cdef long _diff_x
    cdef long _diff_y
    cdef long _dist

    def __cinit__(self):
        self._diff_x = 0
        self._diff_y = 1
        self._dist = 1

    def get(self):
        return self._diff_x, self._diff_y, self._dist

    def get_diff_x(self) -> long:
        return self._diff_x

    def get_diff_y(self) -> long:
        return self._diff_y

    def get_dist(self) -> long:
        return self._dist
    
    def get_angle(self):
        return get_angle_from_rad(self._diff_x, self._diff_y)

    def get_angle_deg(self):
        return get_angle_from(self._diff_x, self._diff_y)

    def get_is_equal(self, ang) -> int:
        return self._diff_x == ang.get_diff_x() and self._diff_y == ang.get_diff_y()

    def set(self, long from_x, long from_y, long to_x, long to_y):
        self._diff_x = int(to_x - from_x)
        self._diff_y = int(to_y - from_y)
        self._dist = int_sqrt(self._diff_x ** 2 + self._diff_y ** 2)
        return self

    def set_diff(self, long diff_x, long diff_y):
        self._diff_x = int(diff_x)
        self._diff_y = int(diff_y)
        self._dist = int_sqrt(self._diff_x ** 2 + self._diff_y ** 2)
        return self

    def set_places(self, place_from, place_to):
        self._diff_x = int(place_to.get_pos_x() - place_from.get_pos_x())
        self._diff_y = int(place_to.get_pos_y() - place_from.get_pos_y())
        self._dist = int_sqrt(self._diff_x ** 2 + self._diff_y ** 2)
        return self
    
    def set_angle_dist(self, ang):
        self._diff_x, self._diff_y, self._dist = ang.get()
        return self

    def angle_btw(self, ang):
        # vraci cos uhlu
        return (self._diff_x * ang.get_diff_x() + self._diff_y * ang.get_diff_y()) / (self._dist * ang.get_dist())

    def angle_btw_deg(self, ang):
        d = angle_dist_side_deg(get_angle_from(self._diff_x, self._diff_y), get_angle_from(ang.get_diff_x(), ang.get_diff_y()))
        return min(d.left, d.right)

    def angle_btw_deg_val(self, ang_val):
        d = angle_dist_side_deg(get_angle_from(self._diff_x, self._diff_y), ang_val)
        return min(d.left, d.right)
