import time

from collections import deque
from game.logger import Logger


class FrameTimer:

    DEFAULT_TICKS = 50
    DEFAULT_SERVER_TICKSTEP = 2

    _print_ticks = False
    _start_time_bef = 0
    _start_time = 0
    _time_begin = 0
    _time = 0
    _time_in_frame = 0
    _time_ticks = 0
    _time_ticks_qa = 50
    _time_next = 0
    _use_int = 0
    _correct = 0
    _tm_save = 0
    
    _sleeps = None
    _min_sleep_sum = 0
    _min_sleep = 0
    _sleeps_in_frame = 1
    _use_sleeps = 0

    def __init__(self, ticks_qa=DEFAULT_TICKS, print_ticks=False):
        self._time_ticks_qa = ticks_qa
        self._print_ticks = print_ticks
        self._start_time_bef = 0
        self._start_time = 0
        self._time = time.perf_counter()
        self._time_begin = self._time
        self._time_in_frame = self._time
        self._time_ticks = 0
        self._correct = 0
        
        self._sleeps = deque()
        self._min_sleep_sum = 0
        self.sleep_and_update_minimal()
        self._use_sleeps = 0

    def get_time_ticks_qa(self):
        return self._time_ticks_qa

    def get_time(self):
        return self._time

    def get_fixed_int(self):
        return 1 / self._time_ticks_qa

    def start(self):
        self._start_time_bef = self._start_time
        self._start_time = time.perf_counter()
        self._use_sleeps += self._sleeps_in_frame

    def sleep_and_update_minimal(self):
        tm = time.perf_counter()
        time.sleep(0.001)
        tm = time.perf_counter() - tm
        
        self._sleeps.append(tm)
        self._min_sleep_sum += tm
        
        while len(self._sleeps) > 10:
            self._min_sleep_sum -= self._sleeps.popleft()
        
        self._min_sleep = self._min_sleep_sum / len(self._sleeps)
        self._sleeps_in_frame = (1.0 / self._time_ticks_qa) / self._min_sleep
        #print("MIN", self._min_sleep - (sum(self._sleeps) / len(self._sleeps)))

    def update_sleep_minimal_avg(self):
        for i in range(0, 20):
            self.sleep_and_update_minimal()

    def start_check(self):
        tm = time.perf_counter()

        if tm > self._tm_save:
            self._start_time = time.perf_counter()
            return True
        else:
            return False

    def dec_time_ticks(self):
        self._time_ticks -= 1

    def end(self):
        ctime = time.perf_counter()
        calc_time = ctime - self._start_time
        self._time = ctime
        self._time_ticks += 1

        if ctime - self._time_in_frame >= 1 or self._time_ticks > self._time_ticks_qa:

            # korekce kvuli nepresnemu thread.sleep

            if self._time_ticks < self._time_ticks_qa:
                self._correct = min(0.02, self._correct + 0.002)
            elif self._time_ticks > self._time_ticks_qa:
                self._correct = max(-0.02, self._correct - 0.002)

            if self._print_ticks:
                Logger.get().log("(timer) ticks: " + str(self._time_ticks) + ", correct: " + str(self._correct) + ", calc_time: " + str(calc_time) + ", time: " + str(time.perf_counter() - self._time_begin))

            self._time_in_frame = ctime
            self._time_ticks = 0

            use_int = max(1.0 / self._time_ticks_qa - calc_time - self._correct, 1 / 1000)

        else:
            # odecteni casu vypoctu pro smooth pohyb bohate staci
            
            use_int = max(1.0 / self._time_ticks_qa - calc_time - self._correct, 1 / 1000)

        return use_int

    def end_sleep(self):
        ctime = time.perf_counter()
        
        calc_time = ctime - self._start_time
        self._time = ctime
        self._time_ticks += 1
        
        if ctime - self._time_in_frame >= 1:
        
            if self._print_ticks:
                Logger.get().log("(timer) ticks: " + str(self._time_ticks) + ", correct: " + str(self._correct) + ", calc_time: " + str(calc_time) + ", time: " + str(time.perf_counter() - self._time_begin))

            self._time_in_frame = ctime
            self._time_ticks = 0
        
        calc_time = ctime - self._start_time
        self._use_sleeps -= calc_time / self._min_sleep
        
        while self._use_sleeps >= 1.0:
            self.sleep_and_update_minimal()
            self._use_sleeps -= 1.0

    def end_sleep_get(self):
        ctime = time.perf_counter()

        calc_time = ctime - self._start_time
        self._time = ctime
        self._time_ticks += 1

        if ctime - self._time_in_frame >= 1:

            if self._print_ticks:
                Logger.get().log("(timer) ticks: " + str(self._time_ticks) + ", correct: " + str(
                    self._correct) + ", calc_time: " + str(calc_time) + ", time: " + str(
                    time.perf_counter() - self._time_begin))

            self._time_in_frame = ctime
            self._time_ticks = 0

        calc_time = ctime - self._start_time
        self._use_sleeps -= calc_time / self._min_sleep

        use_int = 0

        while self._use_sleeps >= 1.0:
            use_int += self._min_sleep
            self._use_sleeps -= 1.0

        return use_int

    def end_save(self):
        self._tm_save = time.perf_counter() + self.end()
