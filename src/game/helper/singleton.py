

class Singleton(type):
    _instances = {}
    _cls_override = {}

    def __call__(cls, *args, **kwargs):
        if cls.__name__ in Singleton._cls_override:
            use_class = Singleton._cls_override[cls.__name__]
        else:
            use_class = cls

        if "new" in kwargs and kwargs["new"]:
            del kwargs["new"]
            # !! return super(Singleton, cls).__call__(*args, **kwargs)
            return super(Singleton, use_class).__call__(*args, **kwargs)

        elif cls not in cls._instances:
            # !! cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            cls._instances[cls] = super(Singleton, use_class).__call__(*args, **kwargs)

        return cls._instances[cls]

    @staticmethod
    def set_cls_override(name, class_ref):
        Singleton._cls_override[name] = class_ref
