from typing import Callable


cpdef make_line_segment_low(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None]):
    cdef long dx = x1 - x0
    cdef long dy = y1 - y0
    cdef long yi = 1

    if dy < 0:
        yi = -1
        dy = -dy

    cdef long dd = (2 * dy) - dx
    cdef long y = y0

    for x in range(x0, x1 + 1):
        callback(x, y)

        if dd > 0:
            y = y + yi
            dd = dd + (2 * (dy - dx))
        else:
            dd = dd + 2 * dy


cpdef make_line_segment_high(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None]):
    cdef long dx = x1 - x0
    cdef long dy = y1 - y0
    cdef long xi = 1

    if dx < 0:
        xi = -1
        dx = -dx

    cdef long dd = (2 * dx) - dy
    cdef long x = x0

    for y in range(y0, y1 + 1):
        callback(x, y)

        if dd > 0:
            x = x + xi
            dd = dd + (2 * (dx - dy))
        else:
            dd = dd + 2 * dx


cpdef make_line(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None]):
    if abs(y1 - y0) < abs(x1 - x0):
        if x0 > x1:
            make_line_segment_low(x1, y1, x0, y0, callback)
        else:
            make_line_segment_low(x0, y0, x1, y1, callback)

    else:
        if y0 > y1:
            make_line_segment_high(x1, y1, x0, y0, callback)
        else:
            make_line_segment_high(x0, y0, x1, y1, callback)


cpdef make_line_b(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None]):
    # zachovani poradi

    cdef long dx = abs(x1 - x0)
    cdef long sx = 1 if x0 < x1 else -1
    cdef long dy = - abs(y1 - y0)
    cdef long sy = 1 if y0 < y1 else -1
    cdef long err = dx + dy

    while True:
        callback(x0, y0)

        if x0 == x1 and y0 == y1:
            break
        e2 = 2 * err

        if e2 >= dy:
            err += dy
            x0 += sx

        if e2 <= dx:
            err += dx
            y0 += sy


cpdef make_line_callback(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None], long steps):
    cdef long dx = abs(x1 - x0)
    cdef long sx = 1 if x0 < x1 else -1
    cdef long dy = - abs(y1 - y0)
    cdef long sy = 1 if y0 < y1 else -1
    cdef long err = dx + dy

    while steps > 0:
        callback(x0, y0)

        if x0 == x1 and y0 == y1:
            break

        e2 = 2 * err

        if e2 >= dy:
            err += dy
            x0 += sx

        if e2 <= dx:
            err += dx
            y0 += sy

        steps -= 1


cpdef line_point make_line_step(long x0, long y0, long x1, long y1, long steps):
    cdef long dx = abs(x1 - x0)
    cdef long sx = 1 if x0 < x1 else -1
    cdef long dy = - abs(y1 - y0)
    cdef long sy = 1 if y0 < y1 else -1
    cdef long err = dx + dy
    cdef line_point o

    o.px = x0
    o.py = y0

    while steps > 0:
        if o.px == x1 and o.py == y1:
            break

        e2 = 2 * err

        if e2 >= dy:
            err += dy
            o.px += sx

        if e2 <= dx:
            err += dx
            o.py += sy

        steps -= 1

    return o
