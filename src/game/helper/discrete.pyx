import gzip
import Cython
import struct
import sys

from libc.math cimport atan2, M_PI, sqrt
from libc.stdlib cimport malloc, free
from libc.string cimport memset

from game.logger import Logger

cdef int dang_map_range = 500
cdef int dang_map_range_max = dang_map_range * 2
cdef int dang_map_range_rev_dist = 707
cdef int dang_map_angle_range = 1000
cdef int dang_map_do_generate = False
cdef int dang_map_is_initialized = False

dang_map_file_name = 'discrete_map.dat'

cdef int * c_dang_map
cdef double * c_dang_map_rad
cdef int * c_dang_map_rev_x
cdef int * c_dang_map_rev_y
cdef int c_height = dang_map_range * 2 + 2
cdef int c_width = dang_map_range * 2 + 2
cdef int c2_height = dang_map_range_rev_dist + 1
cdef int c2_width = dang_map_angle_range + 1

cpdef long get_vec_angle_diff(long a1, long a2, long b1, long b2):
    global dang_map_range, dang_map_range_max

    cdef int ang2 = c_dang_map[int(min(dang_map_range_max, max(0, b2 + dang_map_range))) * c_width + int(min(dang_map_range_max, max(0, b1 + dang_map_range)))]
    cdef int ang1 = c_dang_map[int(min(dang_map_range_max, max(0, a2 + dang_map_range))) * c_width + int(min(dang_map_range_max, max(0, a1 + dang_map_range)))]

    return ang2 - ang1

cpdef long get_angle_from(long diff_x, long diff_y):
    global c_dang_map, dang_map_range, dang_map_range_max
    return c_dang_map[int(min(dang_map_range_max, max(0, diff_y + dang_map_range))) * c_width + int(min(dang_map_range_max, max(0, diff_x + dang_map_range)))]

cpdef double get_angle_from_rad(long diff_x, long diff_y):
    global c_dang_map_rad, dang_map_range, dang_map_range_max
    return c_dang_map_rad[int(min(dang_map_range_max, max(0, diff_y + dang_map_range))) * c_width + int(min(dang_map_range_max, max(0, diff_x + dang_map_range)))]

cpdef angle_coords get_diff_from_angle(long ang_deg, long dist):
    global c_dang_map_rev_x, c_dang_map_rev_y, dang_map_range_rev_dist, dang_map_angle_range
    cdef angle_coords o

    o.ox = c_dang_map_rev_x[min(dang_map_range_rev_dist, max(0, dist)) * c2_width + min(dang_map_angle_range, max(0, ang_deg))]
    o.oy = c_dang_map_rev_y[min(dang_map_range_rev_dist, max(0, dist)) * c2_width + min(dang_map_angle_range, max(0, ang_deg))]

    return o

def build_discrete_map(force=False):
    global dang_map_range, dang_map_range_max, dang_map_range_rev_dist, dang_map_angle_range, dang_map_do_generate, dang_map_file_name, dang_map_is_initialized
    global c_dang_map, c_dang_map_rad, c_dang_map_rev_x, c_dang_map_rev_y

    cdef int ang_deg
    cdef double ang
    cdef long dist, fy, fx, idx, fro_idx, sub_idx, s_idx, from_x, from_y, to_x, to_y

    if "pytest" in sys.modules:
        dang_map_file_name = "../" + dang_map_file_name

    if not dang_map_is_initialized and not dang_map_do_generate and not force:

        Logger().get().log("Discrete map loading ...")

        with gzip.open(dang_map_file_name, 'rb') as fl:
            sz_a = struct.unpack('!l', fl.read(struct.calcsize('!l')))[0]
            sz_b = struct.unpack('!l', fl.read(struct.calcsize('!l')))[0]

            c_dang_map = <int *>malloc(sz_a * sizeof(int))
            c_dang_map_rad = <double *>malloc(sz_a * sizeof(double))
            c_dang_map_rev_x = <int *>malloc(sz_b * sizeof(int))
            c_dang_map_rev_y = <int *> malloc(sz_b * sizeof(int))

            for idx in range(0, sz_a):
                c_dang_map[idx] = struct.unpack('!i', fl.read(struct.calcsize('!i')))[0]

            for idx in range(0, sz_a):
                c_dang_map_rad[idx] = struct.unpack('!d', fl.read(struct.calcsize('!d')))[0]

            for idx in range(0, sz_b):
                c_dang_map_rev_x[idx] = struct.unpack('!i', fl.read(struct.calcsize('!i')))[0]

            for idx in range(0, sz_b):
                c_dang_map_rev_y[idx] = struct.unpack('!i', fl.read(struct.calcsize('!i')))[0]

            dang_map_is_initialized = True

        """
        for fy in range(-dang_map_range, dang_map_range + 1):
            for fx in range(-dang_map_range, dang_map_range + 1):
                ang = math.atan2(fx, fy)
                if ang < 0:
                    ang += math.pi * 2
                ang_deg = round((ang / math.pi) * (1000 / 2))

                got_ang = get_angle_from(fx, fy)
                if got_ang != ang_deg:
                    print("ANG[" + str(fx) + ", " + str(fy) + "]: " + str(got_ang) + " | " + str(ang_deg))
        """

        """
        for dist in range(1, dang_map_range_rev_dist + 1):
            for ang in range(0, dang_map_angle_range):
                diff_x = abs(dang_map_rev[dist][ang][0])
                diff_y = abs(dang_map_rev[dist][ang][1])

                if diff_x > dist or diff_y > dist:
                    print("error value [dist=" + str(dist) + ", ang=" + str(ang) + "] = [" + str(diff_x) + ", " + str(diff_y) + "]")
        """

    elif (not dang_map_is_initialized and dang_map_do_generate) or force:

        Logger().get().log("Discrete map generating ...")

        sz_a = c_height * c_width
        sz_b = c2_height * c2_width

        c_dang_map = <int *>malloc(sz_a * sizeof(int))
        c_dang_map_rad = <double *>malloc(sz_a * sizeof(double))
        c_dang_map_rev_x = <int *>malloc(sz_b * sizeof(int))
        c_dang_map_rev_y = <int *> malloc(sz_b * sizeof(int))

        memset(<void *>c_dang_map, 0, sz_a)
        memset(<void *>c_dang_map_rad, 0, sz_a)
        memset(<void *>c_dang_map_rev_x, 0, sz_b)
        memset(<void *>c_dang_map_rev_y, 0, sz_b)

        for fy in range(-dang_map_range, dang_map_range + 1):
            for fx in range(-dang_map_range, dang_map_range + 1):

                dist = round(sqrt(fx ** 2 + fy ** 2))

                if fx == 0 and fy == 0:
                    ang = 0
                    ang_deg = 0
                else:
                    # ang = angle_to(0, 0, fx, fy)
                    ang = atan2(fx, fy)
                    if ang < 0:
                        ang += M_PI * 2
                    ang_deg = round((ang / M_PI) * (1000 / 2))

                # print("[" + str(fx) + ", " + str(fy) + ", d: " + str(ang_deg) + "]")

                c_dang_map[(dang_map_range + fy) * c_width + dang_map_range + fx] = ang_deg
                c_dang_map_rad[(dang_map_range + fy) * c_width + dang_map_range + fx] = ang
                c_dang_map_rev_x[(dist * c2_width) + ang_deg] = fx
                c_dang_map_rev_y[(dist * c2_width) + ang_deg] = fy

        for dist in range(1, dang_map_range_rev_dist + 1):
            idx = 0

            while idx <= dang_map_angle_range:
                if c_dang_map_rev_x[(dist * c2_width) + idx] == 0 and c_dang_map_rev_y[(dist * c2_width) + idx] == 0:
                    fro_idx = idx
                    sub_idx = fro_idx

                    while sub_idx <= dang_map_angle_range and c_dang_map_rev_x[(dist * c2_width) + sub_idx] == 0 and c_dang_map_rev_y[(dist * c2_width) + sub_idx] == 0:
                        sub_idx += 1

                    from_x = c_dang_map_rev_x[(dist * c2_width) + fro_idx - 1]
                    from_y = c_dang_map_rev_y[(dist * c2_width) + fro_idx - 1]

                    to_x = c_dang_map_rev_x[(dist * c2_width) + (0 if sub_idx > dang_map_angle_range else sub_idx)]
                    to_y = c_dang_map_rev_y[(dist * c2_width) + (0 if sub_idx > dang_map_angle_range else sub_idx)]

                    idx_dist = (sub_idx - fro_idx) // 2

                    for s_idx in range(fro_idx, sub_idx):
                        if s_idx - fro_idx < idx_dist:
                            c_dang_map_rev_x[(dist * c2_width) + s_idx] = from_x
                            c_dang_map_rev_y[(dist * c2_width) + s_idx] = from_y
                        else:
                            c_dang_map_rev_x[(dist * c2_width) + s_idx] = to_x
                            c_dang_map_rev_y[(dist * c2_width) + s_idx] = to_y

                    idx = sub_idx + 1

                else:
                    idx += 1

        dang_map_is_initialized = True

        with gzip.open(dang_map_file_name, 'wb') as fl:
            fl.write(struct.pack('!l', sz_a))
            fl.write(struct.pack('!l', sz_b))

            [fl.write(struct.pack('!i', c_dang_map[idx])) for idx in range(0, sz_a)]
            [fl.write(struct.pack('!d', c_dang_map_rad[idx])) for idx in range(0, sz_a)]
            [fl.write(struct.pack('!i', c_dang_map_rev_x[idx])) for idx in range(0, sz_b)]
            [fl.write(struct.pack('!i', c_dang_map_rev_y[idx])) for idx in range(0, sz_b)]
