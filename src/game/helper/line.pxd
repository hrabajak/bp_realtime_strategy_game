from typing import Callable


cdef struct line_point:
    long px
    long py


cpdef make_line_segment_low(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None])
cpdef make_line_segment_high(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None])
cpdef make_line(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None])
cpdef make_line_b(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None])
cpdef make_line_callback(long x0, long y0, long x1, long y1, callback: Callable[[int, int], None], long steps)
cpdef line_point make_line_step(long x0, long y0, long x1, long y1, long steps)
