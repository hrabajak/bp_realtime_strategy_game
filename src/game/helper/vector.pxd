

cdef struct dist_side:
    double left
    double right

cdef struct dist_side_deg:
    long left
    long right

cpdef double angle_to(long from_x, long from_y, long to_x, long to_y)
cpdef double angle_conv(double inp)
cpdef double angle_conv_realdeg(double inp)
cpdef double angle_conv_deg(double inp)
cpdef double angle_conv_deg_to_realdeg(double inp)
cpdef int angle_conv_realdeg_to_deg(double inp)
cpdef dist_side angle_dist_side(double ang1, double ang2)
cpdef dist_side_deg angle_dist_side_deg(long ang1, long ang2)
cpdef double angle_inc(double ang, double add)
cpdef long angle_inc_deg(long ang, long add)
cpdef double angle_inc_border(double ang, double add, double border, double tolerance)
cpdef long angle_inc_border_deg(long ang, long add, long border, long tolerance)
cpdef double dist_to(double x1, double y1, double x2, double y2)
cpdef int line_in_circle(circle_x, circle_y, circle_rad, x, y, x2, y2)
cpdef long get_accelerate_ticks(acc, speed, dist)
