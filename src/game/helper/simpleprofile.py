import time


class SimpleProfile:

    _start_time = 0

    @staticmethod
    def start():
        SimpleProfile._start_time = time.time()

    @staticmethod
    def p(text):
        diff = time.time() - SimpleProfile._start_time
        SimpleProfile._start_time = time.time()
        print(text + ", diff: " + str(round(diff, 6)))
