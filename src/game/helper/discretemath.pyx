

cpdef long int_sqrt(long value):
    cdef long long left = 0
    cdef long long right = value + 1
    cdef long long mid

    while left != right - 1:
        mid = (left + right) // 2

        if mid * mid <= value:
            left = mid
        else:
            right = mid

    return left
