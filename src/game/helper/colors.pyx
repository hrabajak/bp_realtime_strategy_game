

cpdef color_hsv rgb_to_hsv(int r, int g, int b):
    cdef int maxc, minc, v
    cdef double s, rc, gc, bc
    cdef color_hsv out

    maxc = max(r, g, b)
    minc = min(r, g, b)

    out.v = maxc

    if minc == maxc:
        out.h = 0
        out.s = 0
        return out

    out.s = (maxc-minc) / maxc

    rc = (maxc - r) / (maxc - minc)
    gc = (maxc - g) / (maxc - minc)
    bc = (maxc - b) / (maxc - minc)

    if r == maxc:
        out.h = bc - gc
    elif g == maxc:
        out.h = 2.0 + rc - bc
    else:
        out.h = 4.0 + gc - rc

    out.h = (out.h / 6.0) % 1.0

    return out

cpdef color_rgb hsv_to_rgb(double h, double s, double v):
    cdef int i, f, p, q, t
    cdef color_rgb out

    if s == 0.0:
        out.r = int(v)
        out.g = int(v)
        out.b = int(v)
        return out

    i = int(h * 6.0) # XXX assume int() truncates!
    f = int((h * 6.0) - i)
    p = int(v * (1.0 - s))
    q = int(v * (1.0 - s * f))
    t = int(v * (1.0 - s * (1.0 - f)))
    i = int(i % 6)

    if i == 0:
        out.r, out.g, out.b = int(v), t, p
        return out
    if i == 1:
        out.r, out.g, out.b = q, int(v), p
        return out
    if i == 2:
        out.r, out.g, out.b = p, int(v), t
        return out
    if i == 3:
        out.r, out.g, out.b = p, q, int(v)
        return out
    if i == 4:
        out.r, out.g, out.b = t, p, int(v)
        return out
    if i == 5:
        out.r, out.g, out.b = int(v), p, q
        return out
