
cdef struct angle_coords:
    long ox
    long oy

cpdef long get_vec_angle_diff(long a1, long a2, long b1, long b2)
cpdef long get_angle_from(long diff_x, long diff_y)
cpdef double get_angle_from_rad(long diff_x, long diff_y)
cpdef angle_coords get_diff_from_angle(long ang_deg, long dist)
