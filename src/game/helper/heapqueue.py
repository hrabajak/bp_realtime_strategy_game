import heapq


class HeapQueue(object):

    _data = None
    _key = None
    _index = 0

    def __init__(self, key=lambda x: x):
        self._data = []
        self._key = key
        self._index = 0

    def __len__(self):
        return len(self._data)

    def push(self, item):
        heapq.heappush(self._data, (self._key(item), self._index, item))
        self._index += 1

    def clear(self):
        self._data.clear()

    def pop(self):
        return heapq.heappop(self._data)[2]

    def any(self):
        return len(self._data) > 0
