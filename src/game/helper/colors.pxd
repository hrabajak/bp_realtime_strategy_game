
cdef struct color_rgb:
    int r
    int g
    int b

cdef struct color_hsv:
    double h
    double s
    double v


cpdef color_hsv rgb_to_hsv(int r, int g, int b)
cpdef color_rgb hsv_to_rgb(double h, double s, double v)
