import cython
from libc.math cimport atan, M_PI, sqrt
import math


cpdef double angle_to(long from_x, long from_y, long to_x, long to_y):
    cdef long diff_x = to_x - from_x
    cdef long diff_y = from_y - to_y

    if diff_y < 0 and diff_x > 0:
        return atan(diff_x / abs(diff_y))

    elif diff_y < 0 and diff_x < 0:
        return M_PI + M_PI / 2 + atan(abs(diff_y) / abs(diff_x))

    elif diff_y > 0 and diff_x > 0:
        return M_PI / 2 + atan(diff_y / diff_x)

    elif diff_y > 0 and diff_x < 0:
        return M_PI + atan(abs(diff_x) / diff_y)

    elif diff_x == 0:
        return 0 if diff_y <= 0 else M_PI

    elif diff_y == 0:
        return (M_PI + M_PI / 2) if diff_x <= 0 else (M_PI / 2)


cpdef double angle_conv(double inp):
    return (inp / M_PI) * 500


cpdef double angle_conv_realdeg(double inp):
    return (inp / M_PI) * 180


cpdef double angle_conv_deg(double inp):
    return (inp / 500) * M_PI


cpdef double angle_conv_deg_to_realdeg(double inp):
    return (inp / 1000) * 360


cpdef int angle_conv_realdeg_to_deg(double inp):
    return int((inp / 180) * 500)


cpdef dist_side angle_dist_side(double ang1, double ang2):
    cdef dist_side out

    if ang1 < ang2:
        out.right = ang2 - ang1
        out.left = (M_PI * 2) - ang2 + ang1

    else:
        out.right = (M_PI * 2) - ang1 + ang2
        out.left = ang1 - ang2

    return out


cpdef dist_side_deg angle_dist_side_deg(long ang1, long ang2):
    cdef dist_side_deg out

    if ang1 < ang2:
        out.right = ang2 - ang1
        out.left = 1000 - ang2 + ang1

    else:
        out.right = 1000 - ang1 + ang2
        out.left = ang1 - ang2

    return out


cpdef double angle_inc(double ang, double add):
    cdef double f_ang = ang + add

    while f_ang > M_PI * 2:
        f_ang -= M_PI * 2
    while f_ang < 0:
        f_ang += M_PI * 2

    return f_ang


cpdef long angle_inc_deg(long ang, long add):
    cdef long f_ang = ang + add

    while f_ang > 1000:
        f_ang -= 1000
    while f_ang < 0:
        f_ang += 1000

    return f_ang


cpdef double angle_inc_border(double ang, double add, double border, double tolerance):
    cdef double f_ang = ang + add
    cdef dist_side tm

    while f_ang > M_PI * 2:
        f_ang -= M_PI * 2
    while f_ang < 0:
        f_ang += M_PI * 2

    tm = angle_dist_side(ang, border)

    if tm.left <= tolerance or tm.right <= tolerance:
        ang = border

    return ang


cpdef long angle_inc_border_deg(long ang, long add, long border, long tolerance):
    cdef long f_ang = ang + add
    cdef dist_side_deg tm

    while f_ang > 1000:
        f_ang -= 1000
    while f_ang < 0:
        f_ang += 1000

    tm = angle_dist_side_deg(ang, border)

    if tm.left <= tolerance or tm.right <= tolerance:
        f_ang = border

    return f_ang


cpdef double dist_to(double x1, double y1, double x2, double y2):
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))


cpdef int line_in_circle(circle_x, circle_y, circle_rad, x, y, x2, y2):
    # zjednodusena kontrola v segmentech linky

    cdef long segments
    cdef double tlen, kf, ptx, pty

    tlen = dist_to(x, y, x2, y2)
    segments = math.ceil(tlen / 5) + 1

    for idx in range(0, segments):
        kf = idx / (segments - 1)
        ptx = x + (x2 - x) * kf
        pty = y + (y2 - y) * kf

        if dist_to(circle_x, circle_y, ptx, pty) - 0.05 <= circle_rad:
            return True

    return False


cpdef long get_accelerate_ticks(acc, speed, dist):
    ticks = 0
    use_speed = 0
    d = 0

    while d < dist:
        use_speed += acc
        use_speed = min(use_speed, speed)

        d += use_speed
        ticks += 1

    return ticks
