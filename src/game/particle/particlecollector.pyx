import time

import pyglet

from game.graphic.graphbatches import GraphBatches
from game.helper.singleton import Singleton
from game.image.imagecollector import ImageCollector
from game.interface.iparticles import IParticles
from game.interface.iview import IView
from game.particle.light import Light
from game.particle.particleitem import ParticleItem


class ParticleCollector(IParticles, metaclass=Singleton):

    _batches: GraphBatches = None

    _items = None
    _group = None
    _group_below = None

    _x = 0
    _y = 0
    _wnd_width = 0
    _wnd_height = 0
    _zoom = 1.0

    def __init__(self):
        IParticles.__init__(self)

        self._batches = GraphBatches()

        self._items = set()
        self._group = self._batches.get_group_particle()
        self._group_below = self._batches.get_group_below()

    def get_x(self):
        return self._x

    def get_y(self):
        return self._y

    def get_zoom(self):
        return self._zoom

    def get_in_view(self, double x, double y):
        return x >= -50 and x <= self._wnd_width + 50 and y >= -50 and y <= self._wnd_height + 50

    def add_particle(self, x, y, key, angle=0, delay=0, **kwargs):
        pt = ParticleItem(self)
        pt.set_coords(x, y)

        if delay != 0:
            pt.set_delay(delay)

        if angle == -1:
            pt.set_random_angle()
        elif angle != 0:
            pt.set_angle(angle)

        if "scale" in kwargs:
            pt.set_scale(kwargs["scale"])

        if "scale_to" in kwargs:
            pt.set_scale_to(kwargs["scale_to"])

        if "scale_div" in kwargs:
            pt.set_scale_div(kwargs["scale_div"])

        if "snap" in kwargs:
            pt.set_coord_snap(*kwargs["snap"])

        if isinstance(key, pyglet.image.AbstractImage):
            pt.init_image(key, kwargs["group"] if "group" in kwargs else self._group_below, kwargs["batch"] if "batch" in kwargs else None)

        else:
            anim = ImageCollector().get_animation(key)

            if anim is not None:
                pt.init_anim(anim, kwargs["group"] if "group" in kwargs else self._group, ImageCollector().get_animation_loop(key), kwargs["batch"] if "batch" in kwargs else None)
            else:
                pt.init_image(ImageCollector().get_image(key), kwargs["group"] if "group" in kwargs else self._group_below, kwargs["batch"] if "batch" in kwargs else None)

        self._items.add(pt)

        return pt

    def add_light(self, x, y, **kwargs):
        lg = Light(self)
        lg.set_coords(x, y)
        lg.init()

        if "snap" in kwargs:
            lg.set_coord_snap(*kwargs["snap"])

        if "opacity" in kwargs:
            lg.set_opacity(kwargs["opacity"])

        if "color" in kwargs:
            lg.set_color(*kwargs["color"])

        if "tick_value" in kwargs:
            lg.set_tick_value(*kwargs["tick_value"])

        if "size" in kwargs:
            lg.set_size(kwargs["size"])

        self._items.add(lg)

        return lg

    def clear(self):
        for pt in self._items:
            pt.destroy()

        self._items = set()

    def update(self):
        rem = []
        tm = time.time()

        for pt in self._items:
            if pt.update(tm):
                rem.append(pt)

        if len(rem) > 0:
            for pt in rem:
                pt.destroy()
                self._items.remove(pt)

    def draw(self):
        self._x, self._y, self._wnd_width, self._wnd_height, self._zoom = IView.get().get_view_rect()
