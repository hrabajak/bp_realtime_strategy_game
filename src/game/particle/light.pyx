import math
import random

from libc.math cimport sin, cos, M_PI

from game.graphic.graphbatches import GraphBatches
from game.image.imagecollector import ImageCollector
from game.interface.icoordsnap import ICoordSnap
from game.interface.idrawable import IDrawable
from game.interface.iparticle import IParticle
from game.interface.iparticles import IParticles


cdef class Light: # interfaces: (IDrawable, IParticle)

    cdef public:
        int may_draw

    cdef:
        object _particles

        double _x
        double _y
        int _visible
        double _opacity
        int _col_r
        int _col_g
        int _col_b
        int _rect_init
        object _rect

        object _coord_snap
        double _coord_dist

        double _cur_size
        double _size

        long _tick
        long _eff_tick
        long _eff_max
        long _value_in
        long _value_out
        long _value_max

    def __cinit__(self, particles: IParticles):
        #ifc: IDrawable.__init__(self)
        #ifc: IParticle.__init__(self)

        self._particles = particles

        self._x = 0
        self._y = 0
        self._visible = False
        self._opacity = 255
        self._col_r = 255
        self._col_g = 255
        self._col_b = 255
        self._rect_init = False
        self._coord_dist = 0
        self._cur_size = 0
        self._size = 0
        self._tick = 0
        self._eff_tick = 0
        self._eff_max = 0
        self._value_in = 0
        self._value_out = 0
        self._value_max = 0

    def get_sprite_x(self):
        return self._x * self._particles.get_zoom() + self._particles.get_x()

    def get_sprite_y(self):
        return self._y * self._particles.get_zoom() + self._particles.get_y()

    def set_x(self, val):
        self._x = val

    def set_y(self, val):
        self._y = val

    def set_coords(self, x, y):
        self._x, self._y = x, y

    def set_coord_snap(self, coord_snap: ICoordSnap, coord_dist):
        self._coord_snap = coord_snap
        self._coord_dist = coord_dist
        self._x, self._y = coord_snap.get_look_coords_angle_inc(coord_dist)

    def init(self):
        self._rect_init = True
        self.may_draw = 1

        self._cur_size = 0
        self._size = 40.0

        self._tick = 0
        self._eff_tick = random.randint(0, 100)
        self._value_max = 0
        self._value_in = 0
        self._value_out = 0
        self._eff_max = 5
        GraphBatches().add_drawable(self)

    def set_tick_value(self, value_max, value_in, value_out):
        self._value_max = value_max
        self._value_in = value_in
        self._value_out = value_out
        self._eff_max = value_max / 5

    def set_opacity(self, opacity):
        self._opacity = opacity

    def set_color(self, col_r, col_g, col_b):
        self._col_r = col_r
        self._col_g = col_g
        self._col_b = col_b

    def set_size(self, size):
        self._size = size

    def update(self, tm):
        self._tick += 1
        self._eff_tick += 1

        if self._coord_snap is not None:
            self._x, self._y = self._coord_snap.get_look_coords_angle_inc(self._coord_dist)

        if self._value_max == 0:
            self._cur_size = self._size
            return False

        elif self._tick > self._value_max:
            return True

        else:
            if self._tick < self._value_in:
                self._cur_size = sin((self._tick / self._value_in) * (M_PI / 2)) * self._size
            elif self._tick < self._value_out:
                self._cur_size = self._size
            else:
                self._cur_size = cos(((self._tick - self._value_out) / (self._value_max - self._value_out)) * (M_PI / 2)) * self._size

            return False

    def draw(self):
        cdef double sx, sy, kf0, kf1, kf2, kf3

        if self.may_draw != 1:
            return False

        sx = self.get_sprite_x()
        sy = self.get_sprite_y()

        visible_flag = self._particles.get_in_view(sx, sy)

        if visible_flag != self._visible:
            self._visible = visible_flag

            if self._visible:
                self._rect_init = True
            else:
                self._rect.discard()
                self._rect = None

        if self._visible:
            if self._rect_init:
                self._rect_init = False
                self._rect = GraphBatches().get_rects().create(ImageCollector().get_image("light1"))
                self._rect.set_opacity(self._opacity)
                self._rect.set_colors(self._col_r, self._col_g, self._col_b)

            kf0 = 1.0 - 0.2 * sin(self._eff_tick / self._eff_max)
            kf1 = 1.0 - 0.2 * cos(self._eff_tick / self._eff_max)
            kf2 = 1.0 - 0.2 * sin(self._eff_tick / self._eff_max + M_PI / 2)
            kf3 = 1.0 - 0.2 * cos(self._eff_tick / self._eff_max + M_PI / 2)

            self._rect.set_coords_rect_sizes(self._x, self._y, self._cur_size * kf0, self._cur_size * kf1, self._cur_size * kf2, self._cur_size * kf3)

    def draw_destroy(self):
        if self._rect is not None:
            self._rect.discard()
            self._rect = None

    def destroy(self):
        if self.may_draw != 2:
            self.may_draw = 2
            GraphBatches().kill_drawable(self)
