import math
import random
import time

from libc.math cimport sin, cos, M_PI

from game.graphic.graphbatches import GraphBatches
from game.helper.baseobj import BaseObj
from game.interface.icoordsnap import ICoordSnap
from game.interface.idrawable import IDrawable
from game.interface.iparticle import IParticle
from game.interface.iparticles import IParticles
from game.helper.vector cimport angle_conv, angle_conv_realdeg


cdef class ParticleItem: # interfaces: (IDrawable, IParticle)

    cdef public:
        int may_draw

    cdef:
        object _particles

        object _group
        object _batch
        object _anim
        int _anim_frame
        int _anim_loop
        double _anim_time
        object _image
        int _sprite_init
        int _sprite_update
        object _sprite

        double _x
        double _y
        int _visible

        object _coord_snap
        double _coord_dist

        double _opacity
        double _angle
        double _scale
        double _scale_to
        double _scale_div
        long _delay
        double _force_angle
        double _force
        double _force_dec
        double _force_rotate
        double _force_rotate_dec
        long _fadeout_tick
        long _fadeout_decay

    def __cinit__(self, particles: IParticles):
        #ifc: IDrawable.__init__(self)
        #ifc: IParticle.__init__(self)

        self._particles = particles
        self._anim_frame = 0
        self._anim_loop = False
        self._sprite_init = False
        self._sprite_update = False

        self._x = 0
        self._y = 0
        self._visible = True

        self._coord_dist = 0

        self._opacity = 255
        self._angle = 0
        self._scale = 1.0
        self._scale_to = 0.0
        self._scale_div = 4.0
        self._delay = 0
        self._force_angle = 0
        self._force = 0
        self._force_dec = 0
        self._force_rotate = 0
        self._force_rotate_dec = 0
        self._fadeout_tick = 0
        self._fadeout_decay = 0

    def set_x(self, double val):
        self._x = val

    def set_y(self, double val):
        self._y = val

    def set_coords(self, double x, double y):
        self._x, self._y = x, y

    def set_coord_snap(self, object coord_snap, double coord_dist):
        self._coord_snap = coord_snap
        self._coord_dist = coord_dist
        self._x, self._y = coord_snap.get_look_coords_angle_inc(coord_dist)

    cpdef get_sprite_x(self):
        return self._x * self._particles.get_zoom() + self._particles.get_x()

    cpdef get_sprite_y(self):
        return self._y * self._particles.get_zoom() + self._particles.get_y()

    def set_angle(self, angle):
        self._angle = angle

    def set_delay(self, delay):
        self._delay = delay

    def set_random_angle(self):
        self._angle = math.pi * random.random() * 2.0

    def set_force(self, force_angle, force, force_dec):
        self._force_angle = force_angle
        self._force = force
        self._force_dec = force_dec

    def set_force_rotate(self, force_rotate, force_rotate_dec):
        self._force_rotate = force_rotate
        self._force_rotate_dec = force_rotate_dec

    def set_fadeout(self, fadeout_tick, fadeout_decay):
        self._fadeout_tick = fadeout_tick
        self._fadeout_decay = fadeout_decay

    def set_scale(self, scale):
        self._scale = scale

    def set_scale_to(self, scale_to):
        self._scale_to = scale_to

    def set_scale_div(self, scale_div):
        self._scale_div = scale_div

    def init_anim(self, anim, group, loop, batch=None):
        self._group = group
        self._batch = GraphBatches().get_batch() if batch is None else batch
        self._anim = anim
        self._anim_frame = 0
        self._anim_time = time.time() + self._anim[1]
        self._anim_loop = loop
        self._sprite_init = True
        self.may_draw = 1

        GraphBatches().add_drawable(self)

    def init_image(self, image, group, batch=None):
        self._group = group
        self._batch = GraphBatches().get_batch() if batch is None else batch
        self._image = image
        self._sprite_init = True
        self.may_draw = 1
        GraphBatches().add_drawable(self)

    def _sprite_create(self):
        if self._anim is not None:
            self._sprite = GraphBatches().create_sprite(img=self._anim[0][self._anim_frame], x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._group, batch=self._batch)
            self._sprite.x = self.get_sprite_x()
            self._sprite.y = self.get_sprite_y()
            self._sprite.scale = self._particles.get_zoom() * self._scale
            self._sprite.rotation = angle_conv(self._angle)

        elif self._image is not None:
            self._sprite = GraphBatches().create_sprite(img=self._image, x=self.get_sprite_x(), y=self.get_sprite_y(), group=self._group, batch=self._batch)
            self._sprite.x = self.get_sprite_x()
            self._sprite.y = self.get_sprite_y()
            self._sprite.scale = self._particles.get_zoom() * self._scale
            self._sprite.rotation = angle_conv(self._angle)

    def update(self, tm):
        cdef double diff

        if self._delay > 0:
            self._delay -= 1

        if self._fadeout_decay > 0 and self._fadeout_tick == 0:
            return True

        else:
            if self._coord_snap is not None:
                self._x, self._y = self._coord_snap.get_look_coords_angle_inc(self._coord_dist)
                self._angle = self._coord_snap.get_look_angle()

            if self._scale_to > 0:
                diff = self._scale_to - self._scale
                self._scale += diff / self._scale_div

                if abs(self._scale - self._scale_to) < 0.01:
                    self._scale = self._scale_to
                    self._scale_to = 0

            if self._force > 0:
                self._x += sin(self._force_angle) * self._force
                self._y += cos(self._force_angle) * self._force

                self._force -= self._force_dec
                if self._force <= 0:
                    self._force = 0

            if self._force_rotate > 0:
                self._angle += self._force_rotate

                self._force_rotate -= self._force_rotate_dec
                if self._force_rotate <= 0:
                    self._force_rotate = 0

            elif self._force_rotate < 0:
                self._angle += self._force_rotate

                self._force_rotate -= self._force_rotate_dec
                if self._force_rotate >= 0:
                    self._force_rotate = 0

            if self._fadeout_tick > 0:
                self._fadeout_tick -= 1

                if self._fadeout_tick < self._fadeout_decay:
                    self._opacity = (self._fadeout_tick / self._fadeout_decay) * 255.0

            if self._anim is not None and tm >= self._anim_time:
                self._anim_time = tm + self._anim[1]

                if self._anim_frame + 1 < len(self._anim[0]):
                    self._anim_frame += 1
                    self._sprite_update = True
                elif self._anim_loop:
                    self._anim_frame = 0
                    self._sprite_update = True
                else:
                    return True

        return False

    def draw(self):
        cdef double sx, sy

        if self.may_draw != 1:
            return False

        sx = self.get_sprite_x()
        sy = self.get_sprite_y()

        visible_flag = self._particles.get_in_view(sx, sy)

        if visible_flag != self._visible:
            self._visible = visible_flag

            if self._visible:
                self._sprite_init = True
            else:
                GraphBatches().add_sprite_delete(self._sprite)
                self._sprite = None

        if self._visible:
            if self._sprite_init:
                self._sprite_init = False
                self._sprite_create()

            if self._delay == 0:
                self._sprite.visible = True
                self._sprite.paused = False

            self._sprite.update(x=self.get_sprite_x(), y=self.get_sprite_y(), scale=self._particles.get_zoom() * self._scale, rotation=angle_conv_realdeg(self._angle))

            if self._sprite_update:
                self._sprite_update = False
                self._sprite.image = self._anim[0][self._anim_frame]

            if self._opacity < 255:
                self._sprite.opacity = self._opacity

    def draw_destroy(self):
        pass

    def destroy(self):
        if self.may_draw != 2:
            self.may_draw = 2
            GraphBatches().add_sprite_delete(self._sprite)
            GraphBatches().kill_drawable(self)
