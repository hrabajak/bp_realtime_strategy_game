import os
from Cython.Build import cythonize
from distutils.extension import Extension
import Cython.Compiler.Options


Cython.Compiler.Options.annotate = False
#Cython.Compiler.Options.embed = "main"

exts = []

e = Extension("gam_main", sources=["gam_main.pyx"])
e.cython_directives = {"language_level": "3"}
exts.append(e)

for root, dirs, files in os.walk("game"):
    path = root.split(os.sep)

    for filename in files:
        ext = os.path.splitext(filename)[1]
        full_path = root + "/" + filename

        if ext == ".py":
            module_name = root.replace("/", ".") + "." + os.path.splitext(filename)[0]

            print("module=" + module_name + ", source=" + full_path)

            e = Extension(module_name, sources=[full_path])
            e.cython_directives = {"language_level": "3"}
            exts.append(e)

#e = Extension("game", sources=["game/__init__.py", "game/tstmod.py"])
#e.cython_directives = {"language_level": "3"}
#exts.append(e)

cythonize(exts, compiler_directives={"language_level": "3"})

"""
setup(
    name="Ctest",
    cmdclass={'build_ext': build_ext},
    ext_modules=exts
    #embed="main",
    #annotate=True
    #package_dir={'game': 'game'},
    #ext_modules=cythonize(["gam_main.py"], annotate=True)
    #ext_modules=cythonize(["gam_main.py", "game/*.py"])
)
"""