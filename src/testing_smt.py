import colorsys
import random
import time

import pyglet
from lib.heap import BinaryHeap

from game.helper.heapqueue import HeapQueue
from game.image.imagecollector import ImageCollector
from game.image.imagetransform import ImageTransform
from game.logger import Logger
from game.map.mapbase import MapBase
from game.map.mapsnapshot import MapSnapshot


def main():
    """
    zoom = 100
    lst = [23, 22, 17, 13, 11, 7, 3, 1]

    sum_val = 0
    sum_cnt = 0

    for num in lst:
        sum_val += num * zoom
        sum_cnt += 1

    print(sum_val)
    print(sum_cnt)
    sum_val = sum_val * (1 / sum_cnt)

    print("VAL: " + str(sum_val))
    """

    """
    tm = time.time()
    bb = BinaryHeap()
    #bb = HeapQueue()

    #for idx in range(0, 1000000):
        #bb.push(random.randint(0, 10000))

    for i in range(1, 10):
        bb.push(i, ["a"])
        bb.push(i, ["b"])
        bb.push(i, ["c"])
        bb.push(i, ["d"])
        bb.push(i, ["e"])

    while bb:
        print(bb.pop())

    print(time.time() - tm)
    """

    """
    out = ""
    bef = -1
    cnum = -1
    for idx in range(0, 60):
        while cnum == bef:
            cnum = int(((random.randint(0, 7) * 45) / 180.0) * 500.0)
        out += str(cnum) + ", "
        bef = cnum

    print(out)
    """

    ImageCollector().init()

    #b_img = pyglet.image.load("resources/units/side2/plasma_pad.png")
    #b_img = pyglet.image.load("resources/units/side1/build/s1_main.png")
    #m_img = pyglet.image.load("resources/units/side1/build/s1_main_mask.png")

    #b_img = ImageCollector().get_image("s1_factory")
    #m_img = ImageCollector().get_mask_image("s1_factory")

    #img = ImageTransform.image_colorize(b_img, m_img, 224, 25, 25)

    img = ImageCollector().get_image_color("s2_factory", ImageCollector().color_green)
    img.save("teeeeest.png")

    img = ImageCollector().get_image_color("s2_factory", ImageCollector().color_teal)
    img.save("teeeeest2.png")


if __name__ == '__main__':
    main()
