#!/bin/bash
LINES=`ps -aux | grep gam_main.py | awk '{ print $2 }'`
for pid in $LINES
do
	echo "killing #"$pid" proc"
	kill -9 $pid
done
